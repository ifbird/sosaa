# SOSAA

### Intallation

1. Create a folder, e.g., SOSAA.

2. In the folder SOSAA, clone the repository sosaa from bitbucket to local. Now it is a private repo, so you need right from us to access the repo.

    ```
    $ git clone git@bitbucket.org:ifbird/sosaa.git
    ```
    or
    ```
    git clone https://ifbird@bitbucket.org/ifbird/sosaa.git
    ```

3. Prepare the chemistry folder for a specific scheme

  Create a chemistry folder in SOSAA with several other folders KPP, INPUT, LIB

  ```
  $ mkdir chemistry_sample
  ```

  Create a KPP folder here:

  ```
  $ cd chemistry_sample
  $ mkdir KPP INPUT LIB
  ```

  Search and mark species in [MCM website](http://mcm.leeds.ac.uk/MCMv3.3.1/home.htt).
  Then extract the file as "KPP, experimental KPP format" with two options
  (Include inorganic reactions and Include generic rate coefficients) selected below.
  Save the file as "chemistry_sample/KPP/second.def".
  Modify second.def: delete 'USE constants' and 'CALL mcm_constants(time, temp, M, N2, O2, RO2, H2O)',
  add '#INCLUDE inline.def' in the beginning and '#INCLUDE other_spec.def' in the end.

  Copy second.kpp, inline.def and other_spec.def from sosaa/TEMPLATE to chemistry_sample/KPP.
  Run kpp to generate second files then copy them to chemistry_sample.
  Notice that the $KPP_HOME should be set to ".../sosaa/KPP" beforehand.

  ```
  $ ../../sosaa/KPP/bin/kpp second.kpp
  ```

  Copy Chemistry_Mod.f90 from TEMPLATE to chemistry_sample.


4. Copy input data folder sosa_in and chemistry folders (e.g., chemistry_sample) to SOSAA, and create the case folder (e.g., cases). So now you have these folders in SOSAA:

    ```
    cases  chemistry_sample  sosaa  sosa_in
    ```


### Compilation

1. Enter the source code folder.

    ```
    $ cd sosaa
    ```

2. Create your own `M_DEF`, you can copy from the example file `TEMPLATE/M_DEF_template`.
    All the template files are now put in the TEMPLATE folder.
    You may need to modify the variables `WORK_DIR`, `CODE_DIR` and `CASE_DIR`.

3. If you are compiling the code for the first time, you need to clean the `.o`, `.mod` and `.a` files in the chemistry library folder with the command:

    ```
    $ make cleanchem CHEM=chemistry_simple CASE=TEST
    ```

    Otherwise, you just need to do the normal clean:

    ```
    $ make clean CHEM=chemistry_simple CASE=TEST
    ```

    Then you can create a Chemistry_Mod.f90 file (you may copy one from the template file) in the chemistry scheme foler.
    Finally, you can start to compile the code and the executable file `sosa.exe` will be moved to the folder `cases/TEST` automatically.

    ```
    $ make CHEM=chemistry_simple CASE=TEST
    ```

### Prepare the input data

1. Now the model is mainly used for SMEAR II at Hyytiälä, Finland. So the input data structure and the code are mostly written for the data downloaded from [AVAA](https://avaa.tdata.fi/web/smart) data server. But the users can write their own code to process the IO data in DATA/Sosa\_io.f90.

2. The users can use the python code PREPROCESS/download_data/main.py to download the SMEAR II data and ECMWF data as the input data.


### Run the code

1. Enter the case folder.

    ```
    $ cd cases/TEST
    ```

2. Create an initiation input file `sosa.init` (you can copy one from the template file). Then run the executable file as

    ```
    $ ./sosa.exe sosa.init
    ``` 
    The output folder with the name of `output` will be created in `cases/TEST`. All the output files are saved here. In this way, you can keep different settings and output folders for different cases.


### Model file structure

sosaa
|- MAIN
|- DATA
|- METEOROLOGY
|- CHEMISTRY
|- MEGAN
|- DEPOSITION
|- AEROSOL
|- KPP
|- PREPROCESS
|- TEMPLATE
|- BACKUP
|- LIB
|- DOCUMENTATION
|- Makefile
|- M_DEF
|- README.md

sosa_in
|- general
   |- mcm
      |- photolysis
|- station
   |- hyytiala
      |- info
         |- canopy.txt
         |- EF_day.txt
         |- swr_distribution.txt
         |- ...
      |- 2010
         |- M01
            |- ECMWF_uwind.txt
            |- SMEAR_SO2.txt
            |- ...
         |- M02
         |- ...
      |- 2013
         |- ...
      |- ...

chemistry
|- sample
   |- INPUT
   |- KPP
      |- second.kpp
      |- second.def
      |- inline.def
      |- other_spec.def
   |- LIB
   |- Chemistry_Mod.f90
   |- second_*.f90
|- esm
|- ...

cases
|- sample
   |- sosa.exe
   |- sosa.init
   |- output
      |- UUU.dat
      |- ...
|- ...


### Documentation
