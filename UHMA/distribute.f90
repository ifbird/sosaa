!oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
!
! Contains subroutines and functions to initialize the particle size distribution
!	and to calculate the water and ammonia content of the particles
!
!oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

module distribute

use uhma_datatypes
implicit none

private
integer, parameter :: sections=uhma_sections,&
                      cond=uhma_cond,&
                      compo=uhma_compo,&
                      modes=uhma_init_modes,&
                      real_x=uhma_real_x

real(real_x),parameter :: pi = 4.0_real_x*atan(1.0_real_x), & !pi = 3.1415927
                          avogadro = 6.0221d23, &! Avogadro number (#/mol)
                          molar_h2o = 18.016,&   ! molar mass of water (g/mol)
                          molar_nh3 = 17.03,&    !  - " -        ammonia
                          molec_h2o = molar_h2o*1.d-3/avogadro,&
                          molec_nh3 = molar_nh3*1.d-3/avogadro



public :: init_dist, equilibrium_size, use_dmps, use_dmps_special,read_dmps, get_conc, ms2fx

contains


!oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
!
! Subroutines INIT_DIST and NUMBER_CONCENTRATION solve the initial distribution and
!	divide the particles to size sections according to input data.
!   The routines give as an output the dry radius and initial number concentration
!	in each section as well as the volume concentrations of each compound in each section.
!
!	r_min, r_max		=	smallest and largest particle radius in distribution (m)
!	n_mode, sigma_mode, r_mode	=	number concentration (m^-3), std and mean radius (m)
!							of each log-normal mode
!	mfrac				=	mass fractions of different compounds in each mode
!
! Determines values for global variables:
!	rdry				=	dry radius of particle in each section (m)
!	n_conc				=	number conc.			-"-			 (m^-3)
!	vol_conc			=	mass conc. of each compound in each section (um^3/m^3)
!	core				=	volume of dry particles (m^3)
!
!oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

subroutine init_dist(initial_dist,particles,density)

implicit none

type(uhma_initial_dist),intent(in) :: initial_dist
type(uhma_particles),intent(inout) :: particles
real(real_x),dimension(cond),intent(in) :: density

real(real_x) :: r_min, r_max
real(real_x), dimension(modes) :: n_mode, sigma_mode, r_mode
real(real_x),dimension(compo) :: mfractions
real(real_x),dimension(sections) ::n_conc,rdry,midvol,lowervol,uppervol,core
real(real_x),dimension(sections,compo) :: vol_conc
integer :: jj, kk
real(real_x) :: log_rdry, vol, width, n_limit
real(real_x), dimension(modes) :: n_onemode, dens_part

! localize constant variables
r_min=initial_dist%r_min
r_max=initial_dist%r_max
n_mode=initial_dist%n_mode
sigma_mode=initial_dist%sigma_mode
r_mode=initial_dist%r_mode
mfractions=initial_dist%mfractions

n_limit=particles%n_limit


! checking that the sum of mass fractions in each mode equals one
call fracsum(modes,mfractions)
! density of particles in each log-normal mode (kg/m^3)
! NOTE: ideal solution assumed
dens_part = 1./(sum(mfractions/density))

width = (log10(r_max)-log10(r_min))/(sections-1)        ! width of section (log-scale)


n_conc = 0.
vol_conc = 0.
do kk = 1,sections                                                ! initializing sections

    log_rdry = log10(r_min)+width*(kk-1)
    rdry(kk) = 10**log_rdry                                ! dry radius of particle in section (m)
    core(kk) = 4.*pi/3.*rdry(kk)**3                ! dry volume -"- (m^3)
    midvol(kk) = core(kk)

    uppervol(kk) = 4.*pi/3.*(10**(log_rdry + 0.5*width))**3        ! highest possible volumes in sections
    lowervol(kk) = 4.*pi/3.*(10**(log_rdry - 0.5*width))**3        ! lowest possible volumes in sections

! number concentration contribution of each individual mode to size section (m^-3)
    call number_concentration(n_mode, sigma_mode, r_mode, log_rdry, width, n_onemode)

    n_conc(kk) = sum(n_onemode)                ! total number concentration in section (m^-3)

    if (n_conc(kk) < n_limit) then        ! check whether number concentration negligible
        n_conc(kk) = 0.
        vol_conc(kk,:) = 0.
        goto 100
    end if

! volume concentration of each compound in section (um^3/m^3)
    do jj = 1, modes
        vol_conc(kk,:) = vol_conc(kk,:) + mfractions*dens_part(jj)/density*core(kk)*n_onemode(jj)
    end do
    vol_conc(kk,:) = vol_conc(kk,:)*1.d6**3

    100                continue

    !save the set variables
    particles%rdry=rdry
    
    particles%rdry_orig=rdry
    particles%core=core
    particles%n_conc=n_conc
    particles%vol_conc=vol_conc
end do
end subroutine init_dist

!--------------------------------------------------------------------------------
!
!	Calculates the contribution of the input log-normal modes to a certain
!	 size section number concentration (numerical integration to find the area
!	 under the distribution curve dN/d(logDp) )
!
!	n_mode, sigma_mode, r_mode	=	number concentration (m^-3), std and mean radius (m)
!							of each log-normal mode
!	rdlog				=	logarithm of size section radius
!	width				=	width of the section
!	n_onemode			=	contribution of modes to size section particle
!							concentration(m^3)
!
!--------------------------------------------------------------------------------

subroutine number_concentration(n_mode, sigma_mode, r_mode, rdlog, width, n_onemode)

implicit none

real(real_x), dimension(modes), intent(in) :: n_mode, sigma_mode, r_mode
real(real_x), intent(in) :: rdlog, width
real(real_x), dimension(modes), intent(out) :: n_onemode

integer :: ii, jj, steps
real(real_x) :: dist, expo, low, r_i, conv, stepsize

conv = 10.        !needed for conversion: log10(step) = log(step)/log(10)

low = rdlog - 0.5*width                !lower boundary of the bin
stepsize = 1.d-5                        !size and number of integration steps
steps = nint(width/stepsize)

n_onemode=0.

do jj = 1, modes
    do ii = 1,steps

        r_i = low + (ii-1)*stepsize
        r_i = 10.**r_i                                !radius of size section in consideration

        expo = -( log(r_i/r_mode(jj)) )**2 / (2*log(sigma_mode(jj))**2)
        if (expo < -700.) expo = -700.                        ! can't handle < exp(-700)

! value of log-normal distribution dN/d(lnDp) at r_i
        dist = n_mode(jj)/( sqrt(2.*pi)*log(sigma_mode(jj)) )*exp(expo)

        n_onemode(jj) = n_onemode(jj) + stepsize*log(conv)*dist

    end do
end do

end subroutine number_concentration


!-------------------------------------------------------------------
!
! Subroutine FRACSUM checks that the sum of mass fractions is
!	equal to one in each mode.
!
!-------------------------------------------------------------------

subroutine fracsum(modes,mfractions)

implicit none

integer,intent(in) :: modes
real(real_x),dimension(:),intent(in) :: mfractions
integer :: ii, jj
real(real_x):: total

do ii = 1,modes

    total = sum(mfractions)

    if (total < 0.9999 .or. total > 1.0001) then
        write(unit=6,fmt=10) ii
        10 format ("Sum of mass fractions doesn't equal one in mode",3I2)
        stop
    end if
end do

end subroutine fracsum

!---------------------------------------------------------------------------------
! Read in DMPS data (SUM file) and set as distribution.
! Conserves total particle number conc.
! --------------------------------------------------------------------------------
subroutine use_dmps(filename,sections_meas,line,particles,density,mfractions)
implicit none
character (len=*),intent(in) :: filename !DMPS sum file
integer,intent(in) :: sections_meas,&   !number of channels in DMPS data
                      line              !desired meas. line to use from the sum file
type(uhma_particles) :: particles
real(real_x),dimension(:),intent(in) :: density,mfractions

integer :: ii, jj, kk , mm, irdry_min, irdry_max
real(real_x) :: dummy, total_dmps,dens_part,n_limit
real(real_x), dimension(sections_meas) :: diam_dmps, conc_dmps, rdry_meas, conc_cum_meas
real(real_x), dimension(sections) :: conc_cum,n_conc,core,rdry
real(real_x),dimension(sections,compo) :: vol_conc


! localize constant variables
n_limit=particles%n_limit
rdry=particles%rdry
core=particles%core

open(41,file=filename,status='old')
read(41,*) dummy, dummy, diam_dmps          ! first line of DMPS sum file: 0 0 diam
do jj=2,line+1                              ! choose the selected meas. line as initial state, skip line-1 lines
    read(41,*) dummy, total_dmps, conc_dmps ! read data from DMPS sum file: time, total conc, concs per channel
end do
close(41)

! density of particles (kg/m^3)
! NOTE: ideal solution assumed
dens_part = 1./(sum(mfractions/density))

n_conc = 0.
vol_conc = 0.
rdry_meas=diam_dmps/2.        ! dry radius of particle in section (m)

call dlog_to_dn(conc_dmps,rdry_meas,sections_meas) ! get absolute number concentrations

! Calculate cumulative particle distribution
do ii=1,sections_meas
    conc_cum_meas(ii)=sum(conc_dmps(1:ii))
enddo

! Interpolate the cumulative particle distribution to model sections
conc_cum=0.
jj=1
do ii=1,sections
    if (rdry(ii)>=rdry_meas(jj) .and. jj<sections_meas) then
        do while (rdry(ii)>=rdry_meas(jj+1))
            jj=jj+1
            if (jj==sections_meas) exit
        enddo
        if (jj==sections_meas) then
            conc_cum(ii)=conc_cum_meas(jj)
        else
            conc_cum(ii)=conc_cum_meas(jj)+(conc_cum_meas(jj+1)-conc_cum_meas(jj)) &
                    /(rdry_meas(jj+1)-rdry_meas(jj))*(rdry(ii)-rdry_meas(jj))    
        endif
    elseif (ii>1) then
        conc_cum(ii)=conc_cum(ii-1)
    endif
enddo

! Allocate particles so that the cumulative number conc is conserved
n_conc(1)=conc_cum(1)
do ii=2,sections
    n_conc(ii)=conc_cum(ii)-sum(n_conc(1:ii-1))
enddo

do kk=1,sections
    if (n_conc(kk) < n_limit) then        ! check whether number concentration negligible
        n_conc(kk) = 0.
        vol_conc(kk,:) = 0.
    else
! volume concentration of each compound in section (um^3/m^3)
        vol_conc(kk,:) = vol_conc(kk,:) + mfractions*dens_part/density*core(kk)*n_conc(kk)
        vol_conc(kk,:) = vol_conc(kk,:)*1.d6**3
    end if
end do

!save the set variables
particles%n_conc=n_conc
particles%vol_conc=vol_conc
end subroutine use_dmps

!---------------------------------------------------------------------------------
! Only read in >100 nm (set the value(cut_off_diameter) by yourself) DMPS data (SUM file) .
! 
! Conserves total particle number conc.
! --------------------------------------------------------------------------------
subroutine use_dmps_special(filename,sections_meas,line,cut_off_diameter,cut_off_bin,particles,density,mfractions)
implicit none
character (len=*),intent(in) :: filename !DMPS sum file
integer,intent(in) :: sections_meas,&   !number of channels in DMPS data
                      line              !desired meas. line to use from the sum file
type(uhma_particles) :: particles
real(real_x),dimension(:),intent(in) :: density,mfractions

integer :: ii, jj, kk , mm, irdry_min, irdry_max,cut_off_bin
real(real_x),intent(in):: cut_off_diameter
real(real_x) :: dummy, total_dmps,dens_part,n_limit
real(real_x), dimension(sections_meas) :: diam_dmps, conc_dmps, rdry_meas, conc_cum_meas
real(real_x), dimension(sections) :: conc_cum,n_conc,core,rdry
real(real_x),dimension(sections,compo) :: vol_conc


! localize constant variables
n_limit=particles%n_limit
rdry=particles%rdry
core=particles%core

open(41,file=filename,status='old')
read(41,*) dummy, dummy, diam_dmps          ! first line of DMPS sum file: 0 0 diam
do jj=2,line+1                              ! choose the selected meas. line as initial state, skip line-1 lines
    read(41,*) dummy, total_dmps, conc_dmps ! read data from DMPS sum file: time, total conc, concs per channel
end do
close(41)

! density of particles (kg/m^3)
! NOTE: ideal solution assumed
dens_part = 1./(sum(mfractions/density))

n_conc = 0.
vol_conc = 0.
rdry_meas=diam_dmps/2.        ! dry radius of particle in section (m)

call dlog_to_dn(conc_dmps,rdry_meas,sections_meas) ! get absolute number concentrations

! Calculate cumulative particle distribution
do ii=1,sections_meas
    conc_cum_meas(ii)=sum(conc_dmps(1:ii))
enddo

! Interpolate the cumulative particle distribution to model sections
conc_cum=0.
jj=1
do ii=1,sections
    if (rdry(ii)>=rdry_meas(jj) .and. jj<sections_meas) then
        do while (rdry(ii)>=rdry_meas(jj+1))
            jj=jj+1
            if (jj==sections_meas) exit
        enddo
        if (jj==sections_meas) then
            conc_cum(ii)=conc_cum_meas(jj)
        else
            conc_cum(ii)=conc_cum_meas(jj)+(conc_cum_meas(jj+1)-conc_cum_meas(jj)) &
                    /(rdry_meas(jj+1)-rdry_meas(jj))*(rdry(ii)-rdry_meas(jj))    
        endif
    elseif (ii>1) then
        conc_cum(ii)=conc_cum(ii-1)
    endif
enddo

! Allocate particles so that the cumulative number conc is conserved
n_conc(1)=conc_cum(1)
do ii=2,sections
    n_conc(ii)=conc_cum(ii)-sum(n_conc(1:ii-1))
enddo

do kk=1,sections
    if (n_conc(kk) < n_limit) then        ! check whether number concentration negligible
        n_conc(kk) = 0.
        vol_conc(kk,:) = 0.
    else
! volume concentration of each compound in section (um^3/m^3)
        vol_conc(kk,:) = vol_conc(kk,:) + mfractions*dens_part/density*core(kk)*n_conc(kk)
        vol_conc(kk,:) = vol_conc(kk,:)*1.d6**3
    end if
end do

!save the set variables
!this part is added by Qi
do kk=1,sections
  if (rdry(kk)>=cut_off_diameter/2.0)then 
cut_off_bin=kk-1
exit
endif
enddo
do ii=1,sections
 if (ii>=kk) then
   particles%n_conc(ii)=n_conc(ii)
   particles%vol_conc(ii,:)=vol_conc(ii,:)
 endif
enddo
end subroutine use_dmps_special
! -----------------------------------------
!   Approximative conversion from dN/dlogDp to dN
! -----------------------------------------
subroutine dlog_to_dn(conc_dmps,rdry_meas,sections_meas)
implicit none
integer :: jj
integer,intent(in) :: sections_meas
real(real_x), dimension(sections_meas),intent(inout) :: conc_dmps,rdry_meas
real(real_x), dimension(sections_meas) :: dp
real(real_x), dimension(sections_meas+1) :: middlepoints

! Calculate bin middlepoints
do jj=2,sections_meas
    middlepoints(jj)=rdry_meas(jj-1)+rdry_meas(jj)
end do
middlepoints(1)=4.*rdry_meas(1)-middlepoints(2)
middlepoints(sections_meas+1)=4.*rdry_meas(sections_meas)-middlepoints(sections_meas)
middlepoints=log10(middlepoints)

do jj=1,sections_meas
    dp(jj)=middlepoints(jj+1)-middlepoints(jj)
end do

conc_dmps=conc_dmps*dp*1.d6  +1. !cm^-3 -> m^-3

end subroutine dlog_to_dn

!---------------------------------------------------------------------------------
! Return DMPS data (SUM file)
! --------------------------------------------------------------------------------
subroutine read_dmps(filename,sections_meas,line,conc_dmps)
implicit none
character (len=*),intent(in) :: filename !DMPS sum file
integer,intent(in) :: sections_meas,& !number of channels in DMPS data
                      line            !desired meas. line to use from the sum file
real(real_x), dimension(sections_meas), intent(out) :: conc_dmps ! output abs. conc
integer :: jj
real(real_x) :: dummy, total_dmps
real(real_x), dimension(sections_meas) :: diam_dmps, rdry_meas

open(41,file=filename,status='old')
read(41,*) dummy, dummy, diam_dmps          ! first line of DMPS sum file: 0 0 diam
do jj=2,line+1                              ! choose the selected meas. line as initial state, skip line-1 lines
    read(41,*) dummy, total_dmps, conc_dmps ! read data from DMPS sum file: time, total conc, concs per channel
end do
close(41)

rdry_meas=diam_dmps/2.        ! dry radius of particle in section (m)
call dlog_to_dn(conc_dmps,rdry_meas,sections_meas) ! get absolute number concentrations (#/m^3)

end subroutine read_dmps


!---------------------------------------------------------------------------------
! Get concs from old UHMA output data
! --------------------------------------------------------------------------------
subroutine get_conc(line,conc_comp,vol_comp,compo)
implicit none
integer,intent(in) :: line !desired meas. line to use from the sum file
real(real_x), dimension(:), intent(out) :: conc_comp ! output conc
real(real_x), dimension(:,:), intent(out) :: vol_comp ! output vol_conc
real(real_x), dimension(4) :: dummy
integer,intent(in) :: compo
integer :: jj,kk

open(41,file='number.res',status='old')
do jj=1,line
    read(41,*) conc_comp  ! skip line-1 lines
enddo
close(41)

open(41,file='volume.res',status='old')
do jj=1,line 
    do kk=1,compo
        read(41,*) vol_comp(:,kk)  ! skip line-1 lines
    enddo
enddo
close(41)


end subroutine get_conc

!ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
!
!  Subroutines EQUILIBRIUM_SIZE, TERSIZE and BINSIZE equilibrate
!   the sulphuric acid portion of the particles to ambient water
!	and ammonia vapours by parameterising the Kohler curves.
!	The rest of the particle constituent compounds are assumed insoluble.
!
!	Determines values for global variables:
!		radius		=	radius of particle in each section (m)
!		mass		=	mass  -"- (kg)
!
!ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

subroutine equilibrium_size(particles,ambient,options)

implicit none

type(uhma_particles),intent(inout) :: particles
type(uhma_ambient),intent(inout) :: ambient
type(uhma_options),intent(in) :: options
integer :: ii, k

!where (particles%n_conc(:)<0.) particles%n_conc(:)=0.
!where (particles%vol_conc(:,:)<0.) particles%vol_conc(:,:)=0.

do ii =1, sections

    if (particles%n_conc(ii) < 0) then
        particles%n_conc(ii) = 0
        particles%vol_conc(ii,:) = 0
    end if

    do k=1, compo
        if(particles%vol_conc(ii,k) <0) then
            particles%vol_conc(ii,k) = 0
        end if

    end do

end do


if (options%equilibrate) then
    if (ambient%nh3_mix > 0.1) then      ! ternary case
        call tersize(particles,ambient)
    else                                ! binary case
        call binsize(particles,ambient)
    end if

else ! avoid the bug with equilibration parameterization
    particles%radius = particles%rdry
    do ii=1,sections
        if (particles%n_conc(ii)<particles%n_limit) then
            particles%mass(ii)=4.*pi/3.*particles%radius(ii)**(3.)*1000. ! (kg)
            !particles%mass=4.*pi/3.*particles%radius**(3.)*1000. ! (kg)
        else
            particles%mass(ii) = (sum(ambient%density(:)*particles%vol_conc(ii,:))/particles%n_conc(ii))/(1.d6**3) ! (kg)
            !particles%mass(ii) = sum(ambient%density(:)*particles%vol_conc(ii,:)/particles%n_conc(ii))/(1.d6**3) ! (kg) original
 
        endif
    enddo
endif

end subroutine equilibrium_size


!-----------------------------------------------------------------------
!
!	Calculates the total wet radius and mass of particles when ambient
!		ammonia concentration is above 0.1 ppt.
!
!	Takes advantage of parameterizations for wet
!	radius and number of acid and ammonia molecules in ternary
!	H2SO4-NH3-H2O case
!		(c) Ismo Napari 2003
!
!		Division of Atmospheric Sciences
!		Department of Physical Sciences
!		P.O. Box 64
!		FIN-00014 University of Helsinki
!		Finland
!
!		(ismo.napari@helsinki.fi)
!-----------------------------------------------------------------------

subroutine tersize(particles,ambient)

implicit none

type(uhma_particles),intent(inout) :: particles
type(uhma_ambient),intent(inout) :: ambient
integer :: ii,jj, i
real(real_x) :: nacid, nwater, nammo, vol_sa, vol_org, vol_tot
real(real_x) :: wet_rad, wet_mass
real(real_x) :: lrw,lrh,ln1,ln3
real(real_x) :: r_worg, r_dorg, norg,gf_org
real(real_x) :: n_limit,rh,temp,nh3_mix
real(real_x),dimension(sections) :: n_conc,rdry,radius,mass
real(real_x),dimension(sections,compo) :: vol_conc
real(real_x),dimension(compo) :: density,molecvol,molecmass
real(real_x) :: organicsum

n_limit=particles%n_limit
n_conc=particles%n_conc
rdry=particles%rdry
vol_conc=particles%vol_conc

density=ambient%density
gf_org=ambient%gf_org
rh=ambient%rh
temp=ambient%temp
nh3_mix=ambient%nh3_mix
molecvol=ambient%molecvol
molecmass=ambient%molecmass

call tervalidity(ambient)

do ii = 1,sections

organicsum = 0
!Calculating the sum of soluble organics
do jj=1,uhma_compo
  if(ambient%vapor_type(jj) == soluble_organic) then
    organicsum = organicsum + vol_conc(ii, jj)
  end if
end do

! if the section not empty and the sulphuric acid & water sol. organic contents are not negligible,
! calculate total radius and mass
    if (n_conc(ii) < n_limit) then
        radius(ii) = rdry(ii)
        mass(ii) = 4.*pi/3.*radius(ii)**(3.)*1000.                                ! (kg)
    elseif (organicsum+ vol_conc(ii,ambient%sulfuric_acid_index) < 0.01*sum(vol_conc(ii,:)) &
        .or. vol_conc(ii,ambient%sulfuric_acid_index)/(n_conc(ii)*1.d6**(3.)*molecvol(ambient%sulfuric_acid_index)) < 1.) then
        radius(ii) = rdry(ii)
        mass(ii) = sum(density(1:compo)*vol_conc(ii,1:compo)/n_conc(ii))/(1.d6**3) ! (kg)
    else
        vol_sa = vol_conc(ii,ambient%sulfuric_acid_index)/(n_conc(ii)*1.d6**3)        ! volume of sulphuric acid content in one particle (m^3)
        nacid = vol_sa/molecvol(ambient%sulfuric_acid_index)                                                ! number of acid molecules -"-

        lrw = -1.22986 + 0.44570*rh - 1.213689*rh**2. + 0.78022*rh**3. + &
        0.0024136*log(nh3_mix) + 0.33194*log(nacid) + &
        0.024503*rh*log(nacid) + 0.050236*rh**2*log(nacid) + &
        0.00018042*log(nacid)**2. - 0.0017155*rh*log(nacid)**2.

        wet_rad = exp(lrw)*1.d-9        ! radius of the particle's h2so4-h2o-nh3 content (m)

! for the water soluble organic species
        vol_org = sum_by_type(vol_conc(ii,:), ambient%vapor_type, soluble_organic)/(n_conc(ii)*1.d6**3.)!vol_conc(ii,2)/(n_conc(ii)*1.d6**3.)        ! volume of water soluble organic content

        norg = 0
        do i=1,compo
          if(ambient%vapor_type(i) == soluble_organic) then
            norg = norg + (vol_conc(ii,i)/(n_conc(ii)*1.d6**3.))/molecvol(i)
          end if
        end do
        
        !norg = vol_org/molecvol(2)
        r_dorg = (3.*vol_org/(4.*pi))**(1./3.)                ! dry radius of organic content (m)
        r_worg = gf_org*r_dorg                                ! wet radius -"-
        vol_tot = 4.*pi/3.*wet_rad**3. + 4.*pi/3.*r_worg**3. + &
             (sum_by_type(vol_conc(ii,:), ambient%vapor_type, generic_organic) + sum_by_type(vol_conc(ii,:), ambient%vapor_type, generic)) &
             /(n_conc(ii)*1.d6**3.)!sum(vol_conc(ii,3:compo))/(n_conc(ii)*1.d6**3.)    ! total volume (m^3)
        radius(ii) = (3.*vol_tot/(4.*pi))**(1./3.)              ! total radius (m)

! **** BUG: sometimes dry radius > wet radius *** WHY?!?!
        if (radius(ii) < rdry(ii)) then
            write(*,*) 'radius problem:', ii, rdry(ii),radius(ii)
           ! stop
           radius(ii)=rdry(ii)
        end if

! calculation of water and ammonia molecules in the particles because of equilibration
!	with sulphate content
        ln1 = -7.92524 + 10.3438*rh - 20.2406*rh**2 + &
        10.6466*rh**3 + 0.00044323*temp + 0.013745*rh*temp + 0.0053998*log(nh3_mix) + &
        2.03629*log(nacid) + 0.32495*rh*log(nacid) - &
        0.00061285*temp*log(nacid) - 0.056351*log(nacid)**2 - &
        0.0087760*rh*log(nacid)**2 + 0.0011431*log(nacid)**3

        ln3 = 0.33976 + 7.63687*rh - 0.87997*rh**2 + 0.099540*rh**3 - 0.0013829*temp - &
        0.025128*rh*temp - 0.66492*log(nh3_mix) + &
        0.18587*rh*log(nh3_mix) + 0.0024368*temp*log(nh3_mix) - &
        0.012224*log(nh3_mix)**2 + 1.12186*log(nacid) - &
        0.075124*rh*log(nacid) - 0.00023663*temp*log(nacid) + &
        0.0023043*log(nh3_mix)*log(nacid) - &
        0.0024878*log(nacid)**2 + &
        0.0026711*rh*log(nacid)**2 + 0.000022432*log(nacid)**3

        nwater = exp(ln1)                        ! number of water molecules in the particle
        nammo = exp(ln3)                        ! number of ammonia molecules -"-

        if(nwater < 0.) nwater=0.
        if(nammo < 0.) nammo=0.

! mass of sulphuric acid-water-ammonia content and the whole particle (kg)
        wet_mass = nacid*molecmass(ambient%sulfuric_acid_index) + nwater*molec_h2o + nammo*molec_nh3
        wet_mass = wet_mass + 4.*pi/3.*(r_worg**3. - r_dorg**3.)*1000.        ! mass of water due to organics
        mass(ii) = wet_mass !+ (sum_by_type(density, vol_conc, ambient%vapor_type, generic_organic) + &
        !sum_by_type(density, vol_conc, ambient%vapor_type, soluble_organic) + &
        !sum_by_type(density, vol_conc, ambient%vapor_type, generic)))/(n_conc(ii))/(1.d6**3.))! + sum(density(2:compo)*vol_conc(ii,2:compo)/n_conc(ii))/(1.d6**3.) ! (kg)

        do jj=1,compo
          if(jj /= ambient%sulfuric_acid_index) then
            mass(ii) = mass(ii) + density(jj)*vol_conc(ii,jj)/n_conc(ii)/(1.d6**3.) ! (kg)
          end if
        end do
        
    end if
end do

!Save the modified variables
particles%radius=radius
particles%mass=mass

end subroutine tersize

!---------------------------------------------------------------------------
!
!	Calculates the total wet radius and mass of particles when ambient
!		ammonia concentration is negligible.
!
!	Takes advantage of parameterizations for wet
!	radius and acid number concentration in binary H2SO4-H2O case
!		(c) Hanna Vehkam�ki 2003
!
!		Division of Atmospheric Sciences
!		Department of Physical Sciences
!		P.O. Box 64
!		FIN-00014 University of Helsinki
!		Finland
!
!		(hanna.vehkamaki@helsinki.fi)
!
!-----------------------------------------------------------------------

subroutine binsize(particles,ambient)

implicit none

type(uhma_particles),intent(inout) :: particles
type(uhma_ambient),intent(inout) :: ambient
integer :: ii
real(real_x) :: vol_sa, vol_tot, nacid, nwater, wet_rad, wet_mass, rd, z, &
vol_org, norg, r_dorg, r_worg,gf_org,n_limit,rh,temp
real(real_x),dimension(sections) :: n_conc,rdry,radius,mass
real(real_x),dimension(sections,compo) :: vol_conc
real(real_x),dimension(compo) :: density,molecvol,molecmass

n_limit=particles%n_limit
n_conc=particles%n_conc
rdry=particles%rdry
vol_conc=particles%vol_conc

density=ambient%density
gf_org=ambient%gf_org
rh=ambient%rh
temp=ambient%temp
molecvol=ambient%molecvol
molecmass=ambient%molecmass

call binvalidity(ambient)                ! temperature and rh limits of the parameterizations

do ii = 1,sections

! if the section not empty and the sulphuric acid content not negligible,
! calculate 'wet' radius and mass
    if (n_conc(ii) < n_limit) then
        radius(ii) = rdry(ii)
        mass(ii) = 4.*pi/3.*radius(ii)**3*1000.                                ! (kg)
    else if (vol_conc(ii,ambient%sulfuric_acid_index) < 0.005*sum(vol_conc(ii,:))) then
        radius(ii) = rdry(ii)
        mass(ii) = sum(density(:)*vol_conc(ii,:)/n_conc(ii))/(1.d6**3) ! (kg) !was from 2:compo, but adding sulfur shouldn't make a difference
    else
        vol_sa = vol_conc(ii,ambient%sulfuric_acid_index)/(n_conc(ii)*1.d6**3.)        ! volume of sulphuric acid content in one particle (m^3)
        nacid = vol_sa/molecvol(ambient%sulfuric_acid_index)                           ! number of acid molecules -"-

        if (nacid < 3.or.nacid > 2e11) stop &                        ! validity of parameterizations
        'number of sulphuric acid molecules in particles out of bounds'

! parameterization for particle 'wet' radius (accuracy: < +/- 10%)
        rd = (3*vol_sa/(4*pi))**(1./3.)*1.d9                ! radius of sulphuric acid content (nm)

        rd = log(rd)
        z = 0.3571062410312164 + log(rh)*0.1005557226829263 + rd*(1.072418249000919 - rd*0.007225150816512805)
        wet_rad = exp(z)*1.d-9                ! radius of particle containing sulphuric acid - water content (m)

! for the water soluble organic species
        vol_org = sum_by_type(vol_conc(ii,:), ambient%vapor_type, soluble_organic)/(n_conc(ii)*1.d6**3.) !sum(vol_conc(ii,2:cond))/(n_conc(ii)*1.d6**3.)        ! volume of water soluble organic content
        !norg = vol_org/molecvol(2)
        r_dorg = (3*vol_org/(4*pi))**(1./3.)                        ! dry radius of organic content (m)
        r_worg = gf_org*r_dorg                                                        ! wet radius -"-

        vol_tot = 4.*pi/3.*wet_rad**3 +        4.*pi/3.*r_worg**3. +                        &
        sum_by_type(vol_conc(ii,:), ambient%vapor_type, generic_organic)/(n_conc(ii)*1.d6**3)!sum(vol_conc(ii,cond+1:compo))/(n_conc(ii)*1.d6**3)        ! total volume (m^3)
        !Everyhing else is now considered organic
        radius(ii) = (3*vol_tot/(4*pi))**(1./3.)                ! total radius (m)


! parameterization for number of water molecules in the particle
! (accuracy: - 15%-+22%; for rh 0.20-0.90: -8%-+18%)
        nwater = exp( 0.7175349331751585 + 3.516681581495114*rh - 7.799949839852581*rh**2 &
        + 4.399706826114728*rh**3 - 0.003003960532620574*temp &
        + 0.003600567492582062*rh*temp + 1.168794974365219*log(nacid) + 0.07550157096542515*rh*log(nacid) &
        + 0.1230059777461879*rh**2*Log(nacid) - 0.00004695130053660258*temp*Log(nacid) &
        - 0.01003371715122718*Log(nacid)**2 - 0.005132252262920538*rh*Log(nacid)**2 &
        + 0.0002242756945069555*Log(nacid)**3)

! mass of sulphuric acid-water content and the whole particle (kg)
        wet_mass = nacid*molecmass(ambient%sulfuric_acid_index) + nwater*molec_h2o
        wet_mass = wet_mass + 4.*pi/3.*(r_worg**3 - r_dorg**3)*1000. ! adding mass of water due to organics
        mass(ii) = wet_mass + (sum_by_type(density(:), ambient%vapor_type, generic_organic, vol_conc(ii,:)) + &
        sum_by_type(density(:), ambient%vapor_type, soluble_organic, vol_conc(ii,:)))/n_conc(ii)/(1.d6**3) ! (kg)

    end if
end do

! save the variables
particles%mass=mass
particles%radius=radius

end subroutine binsize

! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
subroutine binvalidity(ambient)
implicit none
type(uhma_ambient),intent(inout) :: ambient

if (ambient%temp < 190.) then
    print *,'Warning: temperature < 190 K, using 190 K'
    ambient%temp = 190.
else if (ambient%temp > 330.) then
    write (*,*) 'Warning: temperature > 330 K, using 330 K'
    ambient%temp = 330.
end if
if (ambient%rh < 0.15) then
    write (*,*) 'Warning: rh < 15%, using 15%'
    ambient%rh = 0.15
else if (ambient%rh > 0.94) then
    write (*,*) 'Warning: rh > 94%, using 94%'
    ambient%rh = 0.94
end if

end subroutine binvalidity

! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

subroutine tervalidity(ambient)
implicit none
type(uhma_ambient), intent(inout) :: ambient

if(ambient%temp < 240.0) then
     write (*,*) 'Warning: temperature < 240 K, using 240 K'
    ambient%temp=240.0
else if (ambient%temp > 300.0) then
     write (*,*) 'Warning: temperature > 300 K, using 300 K'
    ambient%temp=300.0
end if
if(ambient%rh < 0.05) then
     write (*,*) 'Warning: relative humidity < 5 %, using 5%'
    ambient%rh=0.05
else if (ambient%rh > 0.95) then
     write (*,*) 'Warning: relative humidity > 95 %, using 95%'
    ambient%rh=0.95
end if
if(ambient%nh3_mix < 0.1) then
    write (*,*) 'Warning: ammonia mixing ratio < 0.1 ppt, using 0.1 ppt'
    ambient%nh3_mix=0.1
else if(ambient%nh3_mix > 100.0) then
    write (*,*) 'Warning: ammonia mixing ratio > 100 ppt, using 100 ppt'
    ambient%nh3_mix=100.0
end if

end subroutine tervalidity

!--- Moves the particle size distribution from a moving sectional to a fixed grid ----
! Not general! Implementation for MC purposes only
subroutine ms2fx(rdry_fixed,particles,density)
implicit none
type(uhma_particles),intent(inout) :: particles
real(real_x),dimension(sections),intent(in) :: rdry_fixed !the fixed sizes
real(real_x),dimension(:),intent(in) :: density
real(real_x),dimension(sections) :: n_conc_old, core_old, n_conc, core, rdry
real(real_x),dimension(sections,compo) :: vol_conc_old,vol_conc
real(real_x),dimension(compo) :: mfrac
integer :: ii,jj
real(real_x) :: fracup

n_conc_old=particles%n_conc
vol_conc_old=particles%vol_conc
n_conc=0.
vol_conc=0.
core_old=particles%core
rdry=rdry_fixed
core=4.*pi/3.*rdry_fixed**3.

do ii=1,sections
   if (sum(vol_conc_old(ii,:)).ne.0) then
       mfrac=density*vol_conc_old(ii,:)/(sum(density*vol_conc_old(ii,:)))
   else
       !mfrac=(/0.62,0.38,0.,0./)
       !change Anton
       mfrac = 0/(compo)
   endif
   if (core_old(ii)>=core(sections)) then
      n_conc(sections)=n_conc(sections)+n_conc_old(ii)
      vol_conc(sections,:)=vol_conc(sections,:)+n_conc_old(ii) &
           *core(sections)*mfrac/density/(sum(mfrac/density))
   else
      jj=1
      do while (jj<sections)
        if (core_old(ii)>=core(jj) .and. core_old(ii)<core(jj+1)) then
            fracup=(core_old(ii)-core(jj))/(core(jj+1)-core(jj)) !volume conserved
            n_conc(jj+1)=n_conc(jj+1)+fracup*n_conc_old(ii)
            n_conc(jj)=n_conc(jj)+(1.-fracup)*n_conc_old(ii) ! N conserved
            vol_conc(jj,:)=vol_conc(jj,:)+(1.-fracup)*n_conc_old(ii) &
                *core(jj)*mfrac/density/(sum(mfrac/density))
            vol_conc(jj+1,:)=vol_conc(jj+1,:)+fracup*n_conc_old(ii) &
                *core(jj+1)*mfrac/density/(sum(mfrac/density))
            jj=sections
        endif
        jj=jj+1
    enddo
   endif
enddo

vol_conc=vol_conc*1.d6**3.

! save the new distribution
particles%n_conc=n_conc
particles%vol_conc=vol_conc
particles%rdry=rdry
particles%core=core

end subroutine ms2fx


!CORRECT ALL CALLS
pure function sum_by_type(property_to_sum, vapor_types, type_to_sum, multiplicative_property)
  real(real_x), dimension(:), intent(in) :: property_to_sum
  real(real_x), dimension(:), intent(in), optional :: multiplicative_property
  integer, intent(in) :: type_to_sum
  integer, dimension(:), intent(in) :: vapor_types
  
  real(real_x) :: sum_by_type
  
  integer :: i, arraysize
  
  arraysize = size(property_to_sum,1)
  
  sum_by_type = 0
  
  if(present(multiplicative_property)) then
    do i=1,arraysize
      if(vapor_types(i)==type_to_sum) then
        sum_by_type = sum_by_type + property_to_sum(i)*multiplicative_property(i)
      end if
    end do
  else
    do i=1,arraysize
      if(vapor_types(i)==type_to_sum) then
        sum_by_type = sum_by_type + property_to_sum(i)
      end if
    end do
  end if
  
end function sum_by_type

end module distribute
