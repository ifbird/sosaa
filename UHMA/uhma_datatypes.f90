!Contains definitions for data types used by UHMA
!as well as some critical parameters.

module uhma_datatypes

  implicit none
  public

  !add by Qi.  Input flag for different type of Vapor_properties file
  ! 1: using Vapor_properties file for the old version UHMA. Vapor_properties file is supplied by Anton
  ! 0: using the same Vapor_properties file and same method of Pontus Roldin's model
  INTEGER :: input_flag=0

  !..............................................................

  ! UHMA-wide parameters that need to be constants
  integer,parameter :: uhma_sections= 100,&                  !size sections!60
    uhma_vbs_bins= 983,  &! 983, & !373,487,615, & ! 598, & !785, & !802, &!785,& !1291, &    !When input_flag=1: number of organic species (volatility basis set, Donahue et al., 2008) 316 11! luxis file is 189 and pontus is 221 or 220 !
    ! Qi: Pontus newest file is 654 ,plus 1 is INMAT(species for initial) and plus 1 for H2SO4, so uhma_vbs_bins=656 when input_flag=0

  uhma_cond=uhma_vbs_bins,&         ! number of condensable species (sulphuric acid + organics)
    uhma_noncond=0,&                    ! number of non-condensable species
    uhma_compo=uhma_cond+uhma_noncond,& ! number of composition classes
    uhma_init_modes=2                        ! number of initial particle modes

  !defines the precision of model variables
  !integer, parameter :: uhma_real_x = selected_real_kind (p=14,r=30)
  INTEGER, PARAMETER :: uhma_real_x = SELECTED_REAL_KIND(15,300)
  ! DLSODE is in f77 DOUBLE PRECISION, so here we try match that with this (will return 8, standard 64-bit real, on gfortran)

  type uhma_particles !contains variables related to particles
    real(uhma_real_x) :: n_limit,nuc_rate, cluster_vol
    real(uhma_real_x),dimension(uhma_sections) :: n_conc,radius,rdry,rdry_orig,core,mass,gr, original_radiis
    real(uhma_real_x),dimension(uhma_sections,uhma_compo) :: vol_conc ! um^3/m^3
    integer, dimension(uhma_sections) :: order
  end type uhma_particles

  type uhma_ambient !contains variables related to vapors and ambient conditions
    real(uhma_real_x) :: temp,pres,rh,vap_limit,gf_org,nh3,nh3_mix,nh3_cm3,so2_mix,oh_conc,boundaryheight,nuc_coeff
    real(uhma_real_x),dimension(uhma_compo) :: density,molarmass
    real(uhma_real_x),dimension(uhma_cond) :: c_sat,surf_ten,diff_vol,&
      alpha,molecvol,molecmass,vap_conc,sink,n_crit, vap_conc_min
    integer :: sulfuric_acid_index
    character(len=60), dimension(uhma_cond) :: vapor_names
    integer, dimension(uhma_compo) :: vapor_type, condensing_type, index_in_chemistry
    real(uhma_real_x), dimension(uhma_cond) :: parameter_a, parameter_b
  end type uhma_ambient

  type uhma_options !contains misc. variables and options
    real(uhma_real_x) :: meas_interval,latitude,longitude,day_of_year,nuc_number
    logical :: nucleation,condensation,coagulation,dry_deposition,&
      snow_scavenge,equilibrate,cluster_divide, vapor_chemistry, vapor_loss,BLH_dilution
    character (len=2) :: dist_approach
    character (len=3) :: nuc_approach,solver
    integer :: year, month, day
    integer, dimension(3) :: nuc_array
    logical :: quasistationary, raoult
  end type uhma_options

  type uhma_initial_dist !contains parameters for initial particle size distribution
    real(uhma_real_x) :: r_min,r_max
    real(uhma_real_x),dimension(uhma_init_modes) :: n_mode,sigma_mode,r_mode
    real(uhma_real_x),dimension(uhma_compo) :: mfractions
  end type uhma_initial_dist

  type gde_variables !Holds arrays that need to be solved in time
    !Depending on use this might contain RATE OR VALUES at an unspecified time
    !Thus do not use the values from this structure for output.
    real(uhma_real_x), dimension(uhma_sections) :: number_concentration, core_volume, growth_rate
    real(uhma_real_x), dimension(uhma_sections, uhma_compo) :: volume_concentration
    real(uhma_real_x), dimension(uhma_cond) :: vapor_concentration

  end type gde_variables

  interface assignment(=)
    module procedure assign_variable
  end interface

  interface operator(+)
    module procedure add_variables
  end interface

  interface operator(*)
    module procedure multiply_variable, multiply_variable_left, multiply_variable_single, multiply_variable_single_left
  end interface

  enum, bind(c)
    enumerator :: sulfuric_acid = 1
    enumerator :: soluble_organic = 2
    enumerator :: generic_organic = 3
    enumerator :: generic = 4
  end enum

  enum, bind(c)
    enumerator :: acid = 1
    enumerator :: kohler = 2, normal = 3
    enumerator :: not_available = 0
  end enum

  enum, bind(c)
    enumerator :: chemical_not_found = 0
  end enum


contains

  pure function add_variables(a,b) result(c)

    type(gde_variables), intent(in) :: a,b
    type(gde_variables) :: c

    c%number_concentration = a%number_concentration + b%number_concentration
    c%volume_concentration = a%volume_concentration + b%volume_concentration
    c%core_volume = a%core_volume + b%core_volume
    c%growth_rate = a%growth_rate + b%growth_rate

    c%vapor_concentration = a%vapor_concentration + b%vapor_concentration

  end function add_variables

  function multiply_variable_single(number, variable) result(new_variable)

    real(4), intent(in) :: number
    type(gde_variables), intent(in) :: variable
    type(gde_variables) :: new_variable

    real(uhma_real_x) :: rnumber

    rnumber = number * 1.0_uhma_real_x

    new_variable%number_concentration = rnumber*variable%number_concentration
    new_variable%volume_concentration = rnumber*variable%volume_concentration
    new_variable%core_volume = rnumber*variable%core_volume
    new_variable%growth_rate = number*variable%growth_rate

    new_variable%vapor_concentration = rnumber*variable%vapor_concentration

  end function multiply_variable_single

  function multiply_variable_single_left(variable, number) result(new_variable)

    type(gde_variables), intent(in) :: variable
    real(4), intent(in) :: number

    type(gde_variables) :: new_variable

    real(uhma_real_x) :: rnumber

    rnumber = number * 1.0_uhma_real_x

    new_variable%number_concentration = rnumber*variable%number_concentration
    new_variable%volume_concentration = rnumber*variable%volume_concentration
    new_variable%core_volume = rnumber*variable%core_volume
    new_variable%growth_rate = number*variable%growth_rate

    new_variable%vapor_concentration = rnumber*variable%vapor_concentration

  end function multiply_variable_single_left

  function multiply_variable(number, variable) result(new_variable)

    type(gde_variables), intent(in) :: variable
    real(uhma_real_x), intent(in) :: number
    type(gde_variables) :: new_variable

    new_variable%number_concentration = number*variable%number_concentration
    new_variable%volume_concentration = number*variable%volume_concentration
    new_variable%core_volume = number*variable%core_volume
    new_variable%growth_rate = number*variable%growth_rate

    new_variable%vapor_concentration = number*variable%vapor_concentration

  end function multiply_variable

  function multiply_variable_left(variable, number) result(new_variable)

    type(gde_variables), intent(in) :: variable
    real(uhma_real_x), intent(in) :: number
    type(gde_variables) :: new_variable

    new_variable%number_concentration = number*variable%number_concentration
    new_variable%volume_concentration = number*variable%volume_concentration
    new_variable%core_volume = number*variable%core_volume
    new_variable%growth_rate = number*variable%growth_rate

    new_variable%vapor_concentration = number*variable%vapor_concentration

  end function multiply_variable_left

  pure subroutine assign_variable(a,b)

    type(gde_variables), intent(in) :: b
    type(gde_variables), intent(inout) :: a

    a%number_concentration = b%number_concentration
    a%volume_concentration = b%volume_concentration
    a%core_volume = b%core_volume
    a%growth_rate = b%growth_rate

    a%vapor_concentration = b%vapor_concentration


  end subroutine assign_variable


end module uhma_datatypes
