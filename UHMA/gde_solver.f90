!**********************************************************************************************************************************************
!
! Contains the solvers, the general dynamic equation (GDE, specific processes in separate files) and the equations
! for updating vapor concentrations.
! Major changes 2014 Anton Rusanen:
! Only moving sections exists now, remap (option quasistationary) if you want a stable output grid.
! Using lsode for differential equation solving.
! DLSODE setup from Sampo Smolander's chemistry example on atmospheric modelling course
!
!**********************************************************************************************************************************************
!*********************
!Explain by QI:
! In gde solver,condensation is calculated first specially (apc_scheme()) and then other processes (rk4) are calculated together.
!rk4() includes the function gde(). also in rk4, all the changes in other processes are added into the ambient and particle.
!diameter change are calculated in the calculation_secondary() and moving structure is reflected in give_order()
!*******************
module gde_solver

  use uhma_datatypes
  use nucleate
  use condense
  use coagulate
  use deposit
  use distribute
  use uhma_misc
  use sorting

  implicit none

  private
  integer, parameter :: sections=uhma_sections,&
    cond=uhma_cond,&
    compo=uhma_compo,&
    real_x=uhma_real_x, &
    equations = compo+uhma_sections+uhma_sections*compo

  integer, parameter :: vapor_start = 1, vapor_end=compo, number_start = compo+1, number_end = compo+sections, volume_start = number_end+1

  !real(real_x), parameter :: pi = 3.1415927
  type(uhma_options) :: options_global
  type(uhma_particles) :: particles_global
  type(uhma_ambient) :: ambient_global

  ! stuff needed for DLSODE
  INTEGER, PARAMETER  :: neq   = equations   ! number of differential equations
  !!$INTEGER, PARAMETER  :: itol  = 1       ! so that atol is a scalar, not array
  !!$INTEGER, PARAMETER  :: itask = 1       ! for normal computation from t to tout
  !!$INTEGER, PARAMETER  :: iopt  = 0       ! for no optional inputs
  !!$INTEGER, PARAMETER  :: lrw   = 22+9*neq+neq**2 !* MAX(16, neq+9) ! real workspace size
  !!$INTEGER, PARAMETER  :: liw   = 20+neq  ! integer workspace size
  !!$INTEGER, PARAMETER  :: mf    =  22     ! stiff, no user-provided jacobian
  !!$REAL(uhma_real_x), PARAMETER :: rtol = 1d-5     ! relative tolerance
  !!$REAL(uhma_real_x), PARAMETER :: atol = 1d-3     ! absolute tolerance
  !!$REAL(uhma_real_x) :: rwork(lrw)   ! real workspace
  !!$INTEGER  :: iwork(liw)   ! integer workspace

  real(uhma_real_x), parameter :: pi = 4.0_real_x*atan(1.0_real_x), um3_to_m3= (1D-6)**3, Avogadro = 6.0221d23, apc_timestep = 0.5

  integer :: sc_c0, sc_c1, sc_cr, sc_cm

  public :: solve_gde, options_global, particles_global, ambient_global
contains



  !**********************************************************************************************************************************************
  !
  ! The differential equation (GDE) solver
  !
  !**********************************************************************************************************************************************

  subroutine solve_gde(time,timestep,particles,ambient,options,nuc_rate,ipr,Nuc_by_charge)

    implicit none

    real(real_x),intent(in) :: time,ipr
    real(real_x),intent(inout) :: timestep
    type(uhma_particles),intent(inout) :: particles
    type(uhma_ambient),intent(inout) :: ambient
    type(uhma_options),intent(in) :: options
    real(real_x),intent(out):: nuc_rate,Nuc_by_charge(3)
    real(real_x)::CS_H2SO4

    real(uhma_real_x), dimension(equations) :: vector, debug_derivative, guess, next_guess, guess_derivative

    integer :: istate
    real(uhma_real_x) :: time1b, time2, step
    !debug
    integer :: i
    !debug
    time1b = time
    istate = 1
    !optional input
    !IWORK(6) = 2000

    time2 = time + timestep

    !copying to 1d vector for solving, this is necessary for the outside interface to stay exactly the same.
    !If we make more radical changes this could be avoided.
    vector = -1
    vector(vapor_start:vapor_end) = ambient%vap_conc
    vector(number_start:number_end) = particles%n_conc
    vector(volume_start:) = reshape(particles%vol_conc, (/compo*sections/))

    !solving number concentration and volume concentration (other values of interest, i.e radius,mass,... can be derived from these two)

    !First do condensation
    ! Putian: why we need these 3 lines?
    ambient%vap_conc = vector(vapor_start:vapor_end)
    particles%n_conc = vector(number_start:number_end)
    particles%vol_conc = reshape(vector(volume_start:), (/sections, compo/))

    call calculate_secondaries(particles%n_conc, particles%vol_conc, particles%core, particles%rdry, particles%mass)


    !TEST DEBUG
    if(any(vector < 0)) then
      write(*,*) vector
      stop 'Negative values entering apc solver'
    end if

    call system_clock(sc_c0, sc_cr)
    if (options_global%condensation) then
      call apc_scheme(vector, particles%core, particles%rdry, particles%rdry, particles%mass, apc_timestep, timestep, particles, ambient,CS_H2SO4, options)
    end if
    call system_clock(sc_c1, sc_cr)
    ! write(*,*) 'apc_scheme: ', real(sc_c1-sc_c0)/real(sc_cr)

    !TEST DEBUG
    if(any(vector < 0)) then
      stop 'Negative values exiting apc solver'
    end if

    !decomposing back to particles and ambient
    ambient%vap_conc = vector(vapor_start:vapor_end)
    ambient%sink(ambient%sulfuric_acid_index)=CS_H2SO4
    particles%n_conc = vector(number_start:number_end)
    particles%vol_conc = reshape(vector(volume_start:), (/sections, compo/))
    call make_assumptions(vector)

    call calculate_secondaries(particles%n_conc, particles%vol_conc, particles%core, particles%rdry, particles%mass)



    !Then solve the other processes like coagulation, depositoin and ...
    particles_global = particles
    options_global = options
    ambient_global = ambient
    ! call system_clock(sc_c0, sc_cr)
    call rk4(timestep, vector, time1b, nuc_rate, IPR, Nuc_by_charge)
    !CALL DLSODE(gde, neq, vector, time1b, time2, itol, rtol, atol, itask, istate, iopt, rwork, lrw, iwork, liw, dummy, mf)
    ! call system_clock(sc_c1, sc_cr)
    ! write(*,*) 'rk4 after apc: ', real(sc_c1-sc_c0)/real(sc_cr)

    if(istate < 0) then
      write (*,*), 'ISTATE=', ISTATE, time1b
    end if

    !decomposing back to particles and ambient
    ambient%vap_conc = vector(vapor_start:vapor_end)
    particles%n_conc = vector(number_start:number_end)
    particles%vol_conc = reshape(vector(volume_start:), (/sections, compo/))
    !calculating radii and masses for output so user doesn't have to
    call calculate_secondaries(particles%n_conc, particles%vol_conc, particles%core, particles%rdry, particles%mass)

    particles%radius = particles%rdry
  end subroutine solve_gde


  !**********************************************************************************************************************************************
  !
  ! Calculates the differentials in the Grand Dynamic Equation and gas concentrations
  !
  !**********************************************************************************************************************************************

  subroutine gde(neq, time, values, derivatives, time_current,time_step,nuc_rate,IPR,Nuc_by_charge)

    implicit none

    INTEGER,  INTENT(in)  :: neq
    integer :: i,j
    REAL(uhma_real_x), INTENT(in)  :: time, time_current,time_step,IPR
    REAL(uhma_real_x), INTENT(out)  :: nuc_rate, Nuc_by_charge(3)
    REAL(uhma_real_x), INTENT(in), target  :: values(neq)
    REAL(uhma_real_x), INTENT(out), target :: derivatives(neq)

    real(uhma_real_x), dimension(:), pointer ::  vap_conc, n_conc, dvapor, dnumber
    real(uhma_real_x), dimension(:,:), pointer :: vol_conc, dvolume

    real(uhma_real_x), dimension(sections) :: core, mass, rdry, radius
    integer, dimension(sections) :: order

    real(uhma_real_x), target :: myvalues(neq)

    derivatives = 0

    myvalues = values
    !assumptions
    !call make_assumptions(myvalues)

    !Associate array segments with the correct variables.
    vap_conc(1:compo) => myvalues(vapor_start:vapor_end)
    n_conc(1:sections) => myvalues(number_start:number_end)
    vol_conc(1:sections, 1:compo) => myvalues(volume_start:)
    dvapor(1:compo) => derivatives(vapor_start:vapor_end)
    dnumber(1:sections) => derivatives(number_start:number_end)
    dvolume(1:sections, 1:compo) => derivatives(volume_start:)

    dvapor = 0
    dnumber = 0
    dvolume = 0

    !calculate core, mass, dry and (possibly) wet radii

    call calculate_secondaries(n_conc, vol_conc, core, rdry, mass)


    !Water vapor equilibrium is not implemented
    radius = rdry

    !Find out bin order
    call give_order(core, order)
    !----------- Go through selected aerosol dynamical processes ------------------
    call system_clock(sc_c0, sc_cr)
    if (options_global%nucleation) call nucleation(vap_conc, n_conc, vol_conc, core, rdry, radius, order, particles_global, ambient_global, options_global, dvapor, dnumber, dvolume, nuc_rate, IPR, Nuc_by_charge)
    call system_clock(sc_c1, sc_cr)
    ! write(*,*) 'nucleation: ', real(sc_c1-sc_c0)/real(sc_cr)

    !Not solved at the smae time anymore - now we use the subroutine apc-scheme in module condense
    !if (options_global%condensation) call condensation(vap_conc, n_conc, vol_conc, core, rdry, radius, mass, particles_global, ambient_global, options_global, dvapor, dvolume)

    call system_clock(sc_c0, sc_cr)
    if (options_global%coagulation) call coagulation(n_conc, vol_conc, core, rdry, radius, mass, particles_global, ambient_global, options_global, order, dnumber, dvolume)
    call system_clock(sc_c1, sc_cr)
    ! write(*,*) 'coagulation: ', real(sc_c1-sc_c0)/real(sc_cr)

    if (options_global%dry_deposition) call dry_deposition(n_conc, vol_conc, core, rdry, radius, mass, particles_global, ambient_global, options_global, dnumber, dvolume)

    if(options_global%BLH_dilution) call dilution(time_current,time_step,n_conc, vol_conc,vap_conc, core, rdry, radius, mass, particles_global, ambient_global, options_global, dvapor, dnumber, dvolume)

    if(.not. options_global%vapor_loss) dvapor = 0

    !remove these three if statement by QI
    !    if (negatives(dvapor)) dvapor = 0.
    !    if (negatives(dnumber)) dnumber = 0.
    !    if (negatives(dvolume)) dvolume = 0.
    !
    !   do i=1,sections
    !     do j=1,compo
    !      if ((vol_conc(i,j)+2.5*dvolume(i,j))<0) then
    !     write(*,*) 'Debug_Qi:',dvolume(i,j)
    !     write(*,*) 'Debug Qi1',vol_conc(i,j)
    !     write(*,*) 'i,j',i,j
    !     stop
    !      endif
    !enddo
    !enddo
    !    stop
  end subroutine gde

  ! ==============================================================================
  !
  ! Calculate the new particle volume (core), particle mass (mass) and particle
  ! dry radius (dry_radius) at each section according to volume concentration
  ! (vol_conc). Here dry radius is actually the total radius, maybe better to
  ! call it wet radius (needed to be confirmed in future).
  !
  subroutine calculate_secondaries(n_conc, vol_conc, core, dry_radius, mass)

    ! --- in/out ------------------------------

    real(uhma_real_x), dimension(sections), intent(in) :: n_conc
    real(uhma_real_x), dimension(sections,compo), intent(in) :: vol_conc

    real(uhma_real_x), dimension(sections), intent(inout) ::  core, dry_radius, mass

    ! --- local -------------------------------

    real(uhma_real_x), dimension(sections) :: volsum

    integer :: i

    ! --- begin ------------------------------

    !Assigning original core sizes, causing reuse in case the bin is empty

    core = ((4*pi)/3) * particles_global%original_radiis**3

    volsum = sum(vol_conc, 2)

    if(any(volsum < 0)) then
      write (*,*), 'Some volumes are below zero'
    end if

    mass = 0

    do i=1,sections
      if(n_conc(i) /= 0 .and. volsum(i) > 0) then
        mass(i) = (sum(vol_conc(i,:)*ambient_global%density)*um3_to_m3)/n_conc(i)
        core(i) = (volsum(i)*um3_to_m3)/n_conc(i)
        !write(*,*) i, volsum(i), n_conc(i), core(i)
      end if

      dry_radius(i) = ((3.0/(4.0*pi))*core(i))**(1.0/3.0)
    end do

  end subroutine calculate_secondaries


  subroutine make_assumptions(myvalues)

    real(uhma_real_x), intent(inout), target :: myvalues(neq)

    real(uhma_real_x), pointer, dimension(:) :: n_conc
    real(uhma_real_x), pointer, dimension(:,:) :: vol_conc

    real(uhma_real_x), dimension(sections) :: binsum

    integer :: i

    n_conc(1:sections) => myvalues(number_start:number_end)
    vol_conc(1:sections, 1:compo) => myvalues(volume_start:)

    do i=1,sections
      if (n_conc(i) < particles_global%n_limit) then
        n_conc(i) = 0
        vol_conc(i,:) = 0
      end if
    end do

    binsum = sum(vol_conc,2)

    !complete evaporation
    do i=1,sections
      if(binsum(i) == 0) then
        n_conc(i) = 0
      end if
    end do

    nullify(n_conc, vol_conc)

  end subroutine make_assumptions

  subroutine rk4(timestep, values, abs_time,nuc_rate,IPR,Nuc_by_charge)

    real(uhma_real_x), intent(out):: nuc_rate, Nuc_by_charge(3)
    real(uhma_real_x), intent(in) :: timestep, abs_time,IPR
    real(uhma_real_x), dimension(neq), intent(inout) :: values

    real(uhma_real_x), dimension(neq) :: k1, k2, k3, k4, intermediate_result, equilibrium_values

    real(uhma_real_x) :: time_now, time_end, step,time_input

    logical :: equil = .false.
    integer :: i
    time_now = 0
    time_end = timestep

    step = timestep

    !TEST DEBUG
    if(any(values < 0)) then
      stop 'Negative values enetring rk4 solver'
    end if




    do while(time_now < time_end)
      time_input=time_now+abs_time
      if(step <= 1E-11) then
        write (*,*), 'Just stop'
      end if
      !step = min(time_now-time_end, step)

      !k1
      call gde(neq, time_now, values, k1,time_input,step,nuc_rate,IPR,Nuc_by_charge)
      !DEBUG
      if(step < 1E-10) call make_checks(values, k1, 0.5*step, 'k1', .false.)
      !END DEBUG

      !add by Qi. Because for some cases,values is 0 but k1 is negative. It makes no science.
      do i=1,neq
        if ((values(i)==0) .and.  (k1(i)<0)) then
          k1(i)=0
        endif
      enddo
      !.........................
      intermediate_result = values+0.5*step*k1


      if(negatives(intermediate_result)) then
        step=step*0.5
        write (*,*), 'step now1:', step
        cycle
      end if


      !k2
      call gde(neq, time_now+step*0.5, intermediate_result, k2,time_input,step,nuc_rate,IPR,Nuc_by_charge)
      !DEBUG
      if(step < 1E-10) call make_checks(intermediate_result, k2, 0.5*step, 'k2', .true.)
      !END DEBUG

      !add by Qi. Because for some cases,values is 0 but k1 is negative. It makes no science.
      do i=1,neq
        if ((values(i)==0) .and.  (k2(i)<0)) then
          k2(i)=0
        endif
      enddo
      !.........................
      intermediate_result = values+0.5*step*k2



      if(negatives(intermediate_result)) then
        !......Debug.......................
        !  open(112,FILE='./Malte_out/2debug_k2.txt')
        !  do i=1,neq
        !       ! if (intermediate_result(i)<0) then
        !          write(112,*) 'i',i
        !          write(112,*) 'value',values(i)
        !          write(112,*) 'k2',k2(i)
        ! endif
        !    enddo
        !     stop
        !.....Debug...........................
        step=step*0.5
        write (*,*), 'step now2:', step
        cycle
      end if


      !k3
      call gde(neq, time_now+step*0.5, intermediate_result, k3, time_input,step,nuc_rate,IPR,Nuc_by_charge)
      !DEBUG
      if(step < 1E-10)  call make_checks(intermediate_result, k3, step, 'k3', .false.)
      !add by Qi. Because for some cases,values is 0 but k1 is negative. It makes no science.
      do i=1,neq
        if ((values(i)==0) .and. ( k3(i)<0)) then
          k3(i)=0
        endif
      enddo
      !.........................
      intermediate_result = values+step*k3



      if(negatives(intermediate_result)) then
        step=step*0.5
        !write(*,*) intermediate_result
        cycle
      end if

      !k4

      call gde(neq, time_now+step, intermediate_result, k4, time_input,step,nuc_rate,IPR,Nuc_by_charge)
      !DEBUG
      if(step < 1E-10)  call make_checks(values, (1/6.0_uhma_real_x)*(k1+2*k2+2*k3+k4), step, 'final', .true.) !note values
      !END DEBUG

      !add by Qi. Because for some cases,values is 0 but k1 is negative. It makes no science.
      do i=1,neq
        if ((values(i)==0) .and. ( k4(i)<0)) then
          k4(i)=0
        endif
      enddo
      !.........................
      intermediate_result = values + step*(1/6.0_uhma_real_x)*(k1+2*k2+2*k3+k4)

      if(negatives(intermediate_result)) then
        step=step*0.5
        write (*,*), 'step now4:', step
        cycle
      end if

      values = intermediate_result
      time_now = time_now + step
      !call gde(neq, time_now, values, k1)
    end do

  end subroutine rk4

  pure function negatives(array)

    real(uhma_real_x), dimension(neq), intent(in) :: array
    logical :: negatives

    negatives =  any(array < 0)

  end function negatives


  subroutine make_checks(values, derivates, step, tag, stopnow)
    implicit none

    real(uhma_real_x), dimension(neq), intent(in) :: values, derivates
    real(uhma_real_x), intent(in) :: step
    character(len=*), intent(in) :: tag
    logical, intent(in) :: stopnow

    real(uhma_real_x), dimension(neq) :: check

    integer :: i,j
    logical :: isopen

    write (*,*), tag
    check = 0

    where(values /= 0)
      check = derivates/values
    end where

    write (*,*), 'Largest relative change'
    write (*,*), 'Value:', maxval(abs(check))
    write (*,*),  'Location:', maxloc(abs(check))
    write (*,*), 'Orig.Value:', values(maxloc(abs(check)))
    write (*,*), 'Derivative:',derivates(maxloc(abs(check)))


    do i=1,neq
      check(i) = values(i) + step*derivates(i)
      if(check(i) < 0) then
        write (*,*), 'Variable',i,'is going negative'
        write (*,*), 'value:',values(i)
        write (*,*), 'derivative:',derivates(i)
        write (*,*), 'result:', check(i)
      end if
    end do

    write (*,*), 'End for tag:'//''//tag

    if(stopnow) then
      !!$     inquire(999778, opened=isopen)
      !!$
      !!$     if(.not.isopen) then
      !!$        open(999778, file='DATA_BEFORE_CRASH.txt', action='write')
      !!$     end if
      !!$
      !!$     write(999778,*) values
      !!$     write(999778,*) particles_global%original_radiis
      !!$     write(999778,*) ambient_global%c_sat
      !!$     write(999778,*) derivates
      !!$
      !!$     close(999778)

      stop 'Stop after checks have been made'
      !options_global%condensation = .false.
    end if

  end subroutine make_checks


  subroutine monitor(time, values, derivates, index)

    real(uhma_real_x), dimension(neq), intent(in) :: values, derivates
    real(uhma_real_x), intent(in) :: time
    integer, intent(in) :: index

    logical :: isopen


    inquire(999777, opened=isopen)

    if(.not.isopen) then
      open(999777, file='DEBUG_raoult_0001.txt', action='write')
      write(999777,*) 'time value derivative'
    end if

    write(999777,*) time, values(index), derivates(index)

  end subroutine monitor

  subroutine equilibriate(equilibrium_values, values)

    implicit none
    !Truncates extra condensation

    real(uhma_real_x), dimension(neq), target, intent(in) :: equilibrium_values
    real(uhma_real_x), dimension(neq), target, intent(inout) :: values

    real(uhma_real_x), dimension(:), pointer ::  vap_conc_e, n_conc_e
    real(uhma_real_x), dimension(:,:), pointer :: vol_conc_e

    real(uhma_real_x), dimension(:), pointer ::  vap_conc, n_conc
    real(uhma_real_x), dimension(:,:), pointer :: vol_conc

    real(uhma_real_x) :: difference

    integer :: ii, jj

    !Associate array segments with the correct variables.
    vap_conc(1:compo) => values(vapor_start:vapor_end)
    n_conc(1:sections) => values(number_start:number_end)
    vol_conc(1:sections, 1:compo) => values(volume_start:)
    vap_conc_e(1:compo) => equilibrium_values(vapor_start:vapor_end)
    n_conc_e(1:sections) => equilibrium_values(number_start:number_end)
    vol_conc_e(1:sections, 1:compo) => equilibrium_values(volume_start:)

    do jj=1,cond
      do ii=1,sections
        if(vol_conc_e(ii,jj) < vol_conc(ii,jj)) then
          difference = (vol_conc(ii,jj)-vol_conc_e(ii,jj))*um3_to_m3/ambient_global%molecvol(jj) !um^3/m^3 to m^-3
          vap_conc(jj) = vap_conc(jj) + difference
          if(vap_conc(jj) < 0) then
            stop 'Sum of two positive quantites is negative'
          end if
          vol_conc(ii,jj) = vol_conc_e(ii,jj)
        end if
      end do
    end do

  end subroutine equilibriate


  subroutine equilibrium(values, timestep)

    implicit none
    !Sees if equilibrium in condensation is reached in less time than the timestep.
    !If so sets the value to equilibrium

    real(uhma_real_x), dimension(neq), target :: values
    real(uhma_real_x), intent(in) :: timestep

    real(uhma_real_x), dimension(:), pointer ::  vap_conc, n_conc
    real(uhma_real_x), dimension(:,:), pointer :: vol_conc

    real(uhma_real_x), dimension(sections) :: core, mass, rdry, radius, kelvin_eff, collision
    real(uhma_real_x), dimension(compo) :: moles

    !real(uhma_real_x), dimension(neq) :: equilibrium_values
    !  real(uhma_real_x), dimension(neq) :: prev_values

    real(uhma_real_x), dimension(sections,compo) :: vol_change, change_to_equilibrium
    real(uhma_real_x) :: n_others, n_i, limit, solution, change_rate

    integer :: ii,jj

    !!$  prev_values = 0
    !!$
    !!$
    !!$  do while(.not. all(abs(prev_values - values) < 1E-1))
    !!$  !DEBUG try iteration
    !!$  prev_values = values

    !Associate array segments with the correct variables.
    vap_conc(1:compo) => values(vapor_start:vapor_end)
    n_conc(1:sections) => values(number_start:number_end)
    vol_conc(1:sections, 1:compo) => values(volume_start:)

    change_to_equilibrium = 0

    call calculate_secondaries(n_conc, vol_conc, core, rdry, mass)

    !Water vapor equilibrium is not implemented
    radius = rdry

    !Check change rate of one particle (initial condition)
    call calc_vol_change(vap_conc, n_conc, vol_conc, core, rdry, radius, mass, ambient_global, options_global, vol_change)

    do ii=1,cond
      !Equilibrium is not considered for non-organics
      if(ambient_global%vapor_type(ii) == 1 .or. ambient_global%vapor_type(ii) == 4) cycle

      !Calculate things assumed constant (could be optimized if combined with the vol_change calculation)
      call kelvin(ii, kelvin_eff,ambient_global%surf_ten,ambient_global%molecvol,ambient_global%temp,radius)
      collision = collision_rate(ii,radius,mass,ambient_global)

      do jj=1,sections
        if(n_conc(jj) <= 0) cycle

        moles = um3_to_m3*vol_conc(jj,:)/ambient_global%molecvol ! m^3/m^3 /m^3 -> m^-3
        !        moles = um3_to_m3*vol_conc(jj,:)/ambient_global%molecvol/Avogadro


        !First let's calculate true total of organics
        n_others = sum(moles,MASK=(ambient_global%vapor_type == 2 .or. ambient_global%vapor_type == 3))
        if(n_others <= 0) cycle !no organics in this bin

        n_i = moles(ii)
        n_others = n_others - n_i
        if(n_others == 0) cycle ! x_i is one
        if(vap_conc(ii) == 0 .and. n_i == 0) cycle !we are at equilbirium

        change_rate = vol_change(jj,ii)/ambient_global%molecvol(ii) !Try: m^3/s -> molecules/s Maybe this is wrong?: um^3/m^3 s^-1 -> molecules/m^3 s^-1
        !       change_rate = um3_to_m3*vol_change(jj,ii)/ambient_global%molecvol(ii)/Avogadro !um^3/m^3 s^-1 -> mol/m^3 s^-1

        limit = limit_equilibirium(change_rate, collision(jj), n_others, kelvin_eff(jj), ambient_global%c_sat(ii), vap_conc(ii))
        !limit = limit_reducing_equilibirium(n_others, vap_conc(ii)+n_i)

        !DEBUG test with Y(0)
        !solution = solution_at_time(n_i, collision(jj), n_others, kelvin_eff(jj), ambient_global%c_sat(ii), vap_conc(ii), timestep)
        !solution = solution_at_time(change_rate, collision(jj), n_others, kelvin_eff(jj), ambient_global%c_sat(ii), vap_conc(ii), timestep) ! molecules/m^3
        solution = gas_reducing_solution_at_time(change_rate, collision(jj), n_others, kelvin_eff(jj), ambient_global%c_sat(ii), vap_conc(ii)+n_i, timestep) ! molecules/m^3

        !DEBUG
        !!$        if(ii == 8 .and. timestep < 1E-20) then
        !!$
        !!$
        !!$        stop 'Check parameters'
        !!$        end if
        !END DEBUG

        change_to_equilibrium(jj,ii) = limit*ambient_global%molecvol(ii)/um3_to_m3 - vol_conc(jj,ii) ! molecules/m^3 -> um^3/m^3


        !!$        if(abs(limit-solution)/limit < 1E-2) then !molecules
        !!$           change_to_equilibrium(jj,ii) = solution*ambient_global%molecvol(ii)/um3_to_m3 - vol_conc(jj,ii) ! molecules/m^3 -> um^3/m^3
        !!$           !change_to_equilibrium(jj,ii) = solution*ambient_global%molecvol(ii)*Avogadro/um3_to_m3 - vol_conc(jj,ii) ! mol/m^3 -> um^3/m^3
        !!$        else
        !!$           cycle
        !!$        end if


      end do

    end do

    vol_conc = vol_conc + change_to_equilibrium
    !!$  do ii=1,cond
    !!$     vap_conc(ii) = vap_conc(ii) - sum(change_to_equilibrium(:,ii))*um3_to_m3/ambient_global%molecvol(ii) ! um^3/m^3 -> #/m^3
    !!$     !Sanity check, if this trips, we need to reconsider either the limit, or modify the differential equation
    !!$     if(vap_conc(ii) < 0) then
    !!$        stop
    !!$     end if
    !!$  end do

    !DEBUG try iteration
    !end do

  end subroutine equilibrium

  elemental function gas_reducing_solution_at_time(initial_value, collision_rate, n_others, kelvin_term, saturation, gas_and_particle_total, time) result(solution_at_time)

    real(uhma_real_x), intent(in) :: initial_value, collision_rate, n_others, kelvin_term, saturation, gas_and_particle_total, time
    real(uhma_real_x) :: solution_at_time


    !Y(0) (Annoyingly complex formula)

    ! Y'(0) = derivate (simpler one)
    solution_at_time = -(collision_rate*time*(N_others+(kelvin_term*saturation)))/N_others
    solution_at_time = N_others*(collision_rate*gas_and_particle_total - initial_value*exp(solution_at_time))
    solution_at_time = solution_at_time/(collision_rate*(N_others+(kelvin_term*saturation)))

  end function gas_reducing_solution_at_time

  elemental function limit_reducing_equilibirium(n_others, gas_and_particle_total) result(limit_equilibrium)

    real(uhma_real_x), intent(in) :: n_others, gas_and_particle_total
    real(uhma_real_x) :: limit_equilibrium

    !DEBUG test
    limit_equilibrium = N_others*gas_and_particle_total
    limit_equilibrium = limit_equilibrium/(N_others+gas_and_particle_total)

    !limit_equilibirium = n_others*(gas_phase_concentration - derivative/collision_rate)/(kelvin_term * saturation)

  end function limit_reducing_equilibirium


  elemental function solution_at_time(initial_value, collision_rate, n_others, kelvin_term, saturation, gas_phase_concentration, time)

    real(uhma_real_x), intent(in) :: initial_value, collision_rate, n_others, kelvin_term, saturation, gas_phase_concentration, time
    real(uhma_real_x) :: solution_at_time

    !Y(0)
    !solution_at_time = exp((-collision_rate*(kelvin_term*saturation)*time)/(n_others)) &
    !*(gas_phase_concentration*n_others*(exp((collision_rate*(kelvin_term*saturation)*time)/(n_others)) -1 )+(kelvin_term*saturation)*initial_value)
    !solution_at_time = solution_at_time/(kelvin_term*saturation)

    ! Y'(0) = derivate
    solution_at_time = (n_others/(kelvin_term * saturation)) * &
      (gas_phase_concentration -((initial_value*exp((-collision_rate*(kelvin_term * saturation)*time)/n_others))/(collision_rate)))

  end function solution_at_time

  elemental function limit_equilibirium(derivative, collision_rate, n_others, kelvin_term, saturation, gas_phase_concentration)

    real(uhma_real_x), intent(in) :: derivative, collision_rate, n_others, kelvin_term, saturation, gas_phase_concentration
    real(uhma_real_x) :: limit_equilibirium

    !DEBUG test
    limit_equilibirium = (gas_phase_concentration*n_others)/(kelvin_term*saturation)

    !limit_equilibirium = n_others*(gas_phase_concentration - derivative/collision_rate)/(kelvin_term * saturation)

  end function limit_equilibirium

  SUBROUTINE dummy
    ! does nothing
  END SUBROUTINE dummy



end module gde_solver
