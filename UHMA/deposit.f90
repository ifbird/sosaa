!------------------------------------------------------------
!Contains routines related to deposition
!------------------------------------------------------------

module deposit
use uhma_datatypes
implicit none

private
integer, parameter :: sections=uhma_sections,&
                      cond=uhma_cond,&
                      compo=uhma_compo,&
                      real_x=uhma_real_x

real(real_x), parameter :: pi = 3.1415927, &
pstand = 1.01325e5,     &          ! standard pressure (Pa)
boltzmann = 1.3807d-23,  &         ! Boltzmann constant (J/K)
gravitation = 9.81                 ! gravitational acceleration (m/s^2)

public :: dry_deposition, snow_scavenge,dilution

contains

!oooooooooooooooooooooooooo  DRY DEPOSITION  ooooooooooooooooooooooooooooooooooooooooooo
!
!  Calculates dry deposition velocities for particles -> loss of number and volume
!   concentration due to dry deposition
!
!  The parameterization and the parameter values used here are based on:
!   Rannik et al, Interpretation of aerosol particle fluxes over a pine forest:
!   Dry deposition and random errors,
!   J. Geophys. Res., Vol. 108, No. D17, 4544, 10.1029/2003JD003542
!
!ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

!!$subroutine dry_deposition(particles,ambient, n_depo, vol_depo)
!!$
!!$implicit none
!!$type(uhma_particles),intent(in) :: particles
!!$type(uhma_ambient),intent(in) :: ambient
!!$real(real_x), dimension(sections), intent(out) :: n_depo
!!$real(real_x), dimension(sections, compo), intent(out) :: vol_depo
!!$
!!$integer :: ii
subroutine dry_deposition(n_conc, vol_conc, core, rdry, radius, mass, particles, ambient, options, dnumber, dvolume)

  real(uhma_real_x), dimension(:), intent(in) :: n_conc, core, rdry, radius, mass ! current values
  real(uhma_real_x), dimension(:,:), intent(in) :: vol_conc
      
  !Values assumed to be constant in the short term (may change outside the solver)
  type(uhma_particles),intent(in) :: particles
  type(uhma_ambient),intent(in) :: ambient
  type(uhma_options),intent(in) :: options

  real(uhma_real_x), dimension(:), intent(inout) :: dnumber !change rates
  real(uhma_real_x), dimension(:,:), intent(inout) :: dvolume

  real(real_x) :: boundaryheight

  real(real_x), dimension(sections) ::  corr_coeff, settling_veloc, r_c, diff_part
  real(real_x), dimension(sections) :: schmidt, brownian, empirical, dep_veloc, eps

  real(real_x) :: wind_speed, fric_veloc, exponent, wind_canopy, gamma, collection, dmin
  real(real_x) :: air_mean_path, air_viscosity, air_density,n_limit,pres,temp
  real(real_x) :: r_a, prefactor, diff_dmin, schmidt_dmin, deriv_coeff, deriv_schmidt, coeff_dmin

  integer :: ii

  n_limit=particles%n_limit
  pres=ambient%pres
  temp=ambient%temp
  boundaryheight=ambient%boundaryheight

! the following parameter values are based on Hyyti�l� measurements
! they are not (necessarily) applicable to other sites!!!
wind_speed = 2.6                ! median wind speed [m/s]
fric_veloc = 0.4                ! median friction velocity at Hyyti�l� [m/s]
exponent = 2.5                        ! (from Rannik et al., figure 7; TRFE < 50%)
wind_canopy = 1.5                ! wind speed at canopy height [m/s]
gamma = 1.9                                ! coefficient of exponental decrease of wind speed inside canopy
collection = 2.9                ! collection efficiency factor (from fig. 7; TRFE < 50%)
dmin = 146.d-9                        ! diameter of minimum in collection efficiency (from fig. 7; TRFE < 50%)

! ambient air properties
air_mean_path = (6.73d-8*temp*(1.+110.4/temp))/(296.*pres/pstand*1.373)
air_viscosity = (1.832d-5*406.4*temp**1.5)/(5093*(temp+110.4))
air_density = 1.2929*273.15*pres/(temp*pstand)                ! [kg/m^3]

! Cunningham correction factor (based on Allen and Raabe (1982))
corr_coeff = 1. + air_mean_path/(2.*radius)*(2.514 + 0.8*exp(-0.55*(2.*radius/air_mean_path)))

! particle gravitational settling velocity [m/s]
settling_veloc = corr_coeff*mass*gravitation/(6.*pi*air_viscosity*radius)

! aerodynamic resistance [s/m]
r_a = (wind_speed - wind_canopy)/fric_veloc**2

! collection efficiency of particles due to Brownian diffusion
diff_part = boltzmann*temp*corr_coeff/(6.*pi*air_viscosity*radius)        ! diffusivity of particles [m^2/s]
schmidt = air_viscosity/(air_density*diff_part)                                                ! particle Schmidt number
brownian = 1./3.*schmidt**(-2./3.)

! calculating prefactor (as a function of dmin) for empirical collection efficiency of particles
coeff_dmin = 1. + air_mean_path/dmin*(2.514 + 0.8*exp(-0.55*dmin/air_mean_path))
diff_dmin = boltzmann*temp*coeff_dmin/(3.*pi*air_viscosity*dmin)
schmidt_dmin = air_viscosity/(air_density*diff_dmin)
deriv_coeff = -2.514*air_mean_path/dmin**2 - &        ! derivative of Cunninghan correction factor
(air_mean_path/dmin + 0.55)*0.8/dmin*exp(-0.55*dmin/air_mean_path)
deriv_schmidt = schmidt_dmin*(1./dmin - 1./coeff_dmin*deriv_coeff) ! deriv. of Schmidt number
prefactor = 2./9.*schmidt_dmin**(-5./3.)* &
deriv_schmidt/(exponent*dmin**(exponent-1.))

empirical = 4.*prefactor*radius**exponent                ! empirical collection efficiency of particles

eps = collection*(brownian + empirical)        ! total collection efficiency of particles
eps = sqrt(eps)

! canopy resistance [s/m]
r_c = wind_canopy/(fric_veloc**2*eps)*((1. + eps*tanh(gamma*eps))/(eps + tanh(gamma*eps)))

! deposition velocity [m/s]
dep_veloc = settling_veloc + 1./(r_a + r_c)

! number and volume concentration change rate in each bin
do ii = 1,sections
    if (n_conc(ii) > n_limit) then
        dnumber(ii) = dnumber(ii) -dep_veloc(ii)/boundaryheight*n_conc(ii)        ![#/m^3 s]
        dvolume(ii,:) = dvolume(ii,:) -dep_veloc(ii)/boundaryheight*vol_conc(ii,:)        ![um^3/m^3 s]
    endif
enddo

end subroutine dry_deposition


!-----------------------------------------------------------------------
!
! Boundary layer dilution parametrization by Ximeng Qi, 2016
! Brief method is from Pontus Roldin's idea
! Changed by Michael Boy February 2017
!
!-----------------------------------------------------------------------

subroutine dilution(timew,time_step,n_conc, vol_conc,vapor, core, rdry, radius, mass, particles, ambient, options, dvapor, dnumber, dvolume)

  real(uhma_real_x),intent(in):: timew,time_step
  real(uhma_real_x), dimension(:), intent(in) :: n_conc, core, rdry, radius, mass ! current values
  real(uhma_real_x), dimension(:,:), intent(in) :: vol_conc

  !Values assumed to be constant in the short term (may change outside the solver)
  type(uhma_particles),intent(in) :: particles
  type(uhma_ambient),intent(in) :: ambient
  type(uhma_options),intent(in) :: options

  real(uhma_real_x), dimension(:), intent(inout) :: dvapor
  real(uhma_real_x), dimension(:), intent(inout) :: dnumber !change rates
  real(uhma_real_x), dimension(:,:), intent(inout) :: dvolume
  real(uhma_real_x), dimension(compo)    :: svoc_upBL,elvoc_upBL
  real(uhma_real_x), dimension(sections)    :: n_conc_upBL 
  real(uhma_real_x), dimension(sections,compo)  :: vol_conc_upBL
  real(uhma_real_x), dimension(compo)::vapor,svoc_critical,elvoc_critical
  real(uhma_real_x),dimension(98),save :: BLH  !time: 00:00-24:00-00:00-1:00,49+1
   !variable boundary layer height(hourly data). different with the boundaryheight(consitant value) in subroutine depostion
  real(uhma_real_x):: BLH_current,BLH_later,start_time_par
  logical, save :: first_BLH_read=.true. !only read the file one time.
  integer :: time_hour,time_hour2,i
  if (first_BLH_read) then
    first_BLH_read=.false.
    !read the boundary layer height !!!NOTICE::change the filename of BLH file here!!!!
    open(100,file='./Malte_in/BLH.txt',action='read') !NOT good: should be writen in the Input.f90 in the future     
    read(100,*) (BLH(i),i=1,98)
    close(100)
  endif
 ! IF (CASE == '20130501') THEN  ! Will be improved in future

    start_time_par=30 !time is in hour !20130522 or 20130615

 ! ENDIF
  !............SET the concentrations above the boudary layer...........
!20130501
!  vapor_upBL=0.8*vapor;
!  n_conc_upBL=0.6*n_conc;
!  vol_conc_upBL=0.6*vol_conc;
!20130522
  svoc_upBL=0.0*vapor
  elvoc_upBL=0.0*vapor
  n_conc_upBL=0.95*n_conc
  vol_conc_upBL=0.95*vol_conc
  !...................................................................
  time_hour=int(timew/3600.)+1
  time_hour2= int((timew+time_step)/3600.)+1 !time in hour in next step
  BLH_current=BLH(time_hour)+(timew-(time_hour-1)*3600)*(BLH(time_hour+1)-BLH(time_hour))/3600
  BLH_later=BLH(time_hour2)+((timew+time_step)-(time_hour2-1)*3600)*(BLH(time_hour2+1)-BLH(time_hour2))/3600

  IF((BLH_later-BLH_current)>0D0) THEN
      if (timew/3600.>start_time_par) then
          do i=1,sections
              !    if (radius(i)<15.e-9) then !cut-off diameter for special dmps input
              dnumber(i)=dnumber(i)+((BLH_current*n_conc(i)+(BLH_later-BLH_current)*n_conc_upBL(i))/BLH_later-n_conc(i))
              dvolume(i,:)=dvolume(i,:)+((BLH_current*vol_conc(i,:)+(BLH_later-BLH_current)*vol_conc_upBL(i,:))/BLH_later-vol_conc(i,:))
          !    endif
          enddo
      endif

      dvapor=dvapor+((BLH_current*vapor+(BLH_later-BLH_current)*svoc_upBL)/BLH_later-vapor)
      dvapor=dvapor+((BLH_current*vapor+(BLH_later-BLH_current)*elvoc_upBL)/BLH_later-vapor)
  ENDIF


end subroutine


!------------------------------------------------------------------------
! Snow scavenging parametrization by Tiia Gronholm
! See e.g. Kyrö et al., Bor. Env. Res., 14, 2009
!------------------------------------------------------------------------

subroutine snow_scavenge(particles,compo,n_snowscav,vol_snowscav)

implicit none
type(uhma_particles),intent(in) :: particles
integer,intent(in) :: compo
real(real_x), dimension(sections), intent(out) :: n_snowscav
real(real_x),dimension(sections,compo), intent(out) :: vol_snowscav

real(real_x), dimension(sections) :: log_dp, scav_coeff,n_conc,radius
integer :: ii
real(real_x),dimension(sections,compo) :: vol_conc
real(real_x) :: n_limit

n_conc=particles%n_conc
n_limit=particles%n_limit
radius=particles%radius
vol_conc=particles%vol_conc

log_dp=log10(2.*radius(:))
scav_coeff(:)=22.662756+381.18017/log_dp(:)+1321.1698/log_dp(:)**2.
scav_coeff=10**scav_coeff

do ii=1,sections
    if (n_conc(ii)>n_limit) then
        n_snowscav(ii)=-n_conc(ii)*scav_coeff(ii)
        vol_snowscav(ii,:)=-vol_conc(ii,:)*scav_coeff(ii)
    else
        n_snowscav(ii)=0.
        vol_snowscav(ii,:)=0.
    endif
enddo

end subroutine snow_scavenge

end module deposit
