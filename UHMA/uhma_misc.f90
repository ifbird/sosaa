!ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
!
! Contains auxiliary subroutines and functions
!
!ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

module uhma_misc

use uhma_datatypes
use arbitrary_input
!use sorting

implicit none

private
integer, parameter :: sections=uhma_sections,&
                      cond=uhma_cond,&
                      compo=uhma_compo,&
                      real_x=uhma_real_x


real(real_x), parameter :: &
boltzmann = 1.3807d-23, &         ! Boltzmann constant (J/K)
pi = 3.1415927

real(real_x), parameter :: &
h2so4sine_max = 5.0d6*1.0d6, &
h2so4sine_lenght = 2*24*60*60


public :: sa_prodrate, calc_oh_conc, cos_zenith, dilute_conc,vbs_oxid,bl_height, &
 changevapors, get_deltavapor

contains

!ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
! Subroutine COS_ZENITH calculates cosine of the solar zenith angle
! in a given place (latitude, longitude) and at a given time.
! Finlayson&Pitts s. 58
!
! Input:	time	= local time (Finland, winter time) in seconds
!		day	= day of year (Jan 1 = 1, Dec 31 = 365)
!		latit	= latitude in degrees
!		longit	= longitude in degrees
!
!ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

REAL(real_x) FUNCTION cos_zenith(time, day, latit, longit)

IMPLICIT NONE

REAL(real_x), INTENT(IN) :: time, day, latit, longit
REAL(real_x) :: GMT, N, EQT, th, delta, daylight_saving

GMT = time/3600.0 - 2.0      ! Greenwich mean time

daylight_saving=0.

N = 2.0 * pi * (day-1)/365.0
EQT = 7.5e-5 + 1.868e-3*cos(N) - 3.2077e-2*sin(N) - 1.4615e-2*cos(2*N) - 4.0849e-2*sin(2*N)
th = (GMT/12.0 - 1 + longit/180.0)*pi + EQT

delta = 6.918e-3 - 0.399912*cos(N) + 0.070257*sin(N) - 6.758e-3*cos(2*N) + 9.07e-4*sin(2*N) &
- 2.697e-3*cos(3*N) + 1.480e-3*sin(3*N)

cos_zenith = sin(delta)*sin(latit*pi/180) + cos(delta)*cos(latit*pi/180)*cos(th)

END FUNCTION cos_zenith


!---------------------------------------------------------------------------
! Subroutine OH_CONC calculates OH concentration in 1/(cm3).
! Calculated directly proportional to cosine of zenith angle,
! scalig factor 6.0e6 is just a "good guess". (1.3e6 according to Boy)
!---------------------------------------------------------------------------

SUBROUTINE calc_OH_conc(time,day,latit,longit,OH_conc)

REAL(real_x), INTENT(IN) :: time, day, latit, longit
real(real_x),intent(out) :: OH_conc

! OH-concentration in 1/cm3 using zenith angle
OH_conc = 1.3e6*cos_zenith(time,day,latit,longit)
IF (OH_conc < 0) OH_conc = 1.0

END SUBROUTINE calc_OH_conc


!---------------------------------------------------------------------------
! Function SA_PRODRATE calculates sulphuric acid production rate (1/(m3 s))
! in reaction SO2 + OH --> HOSO2 --> --> H2SO4.
! Production rate is determined by the rate of the first reaction
! in the reaction chain.
!
! Input:	SO2_mix = mixing ratio of SO2 in ppt
!		others as in cos_zenith
!---------------------------------------------------------------------------

REAL(real_x) FUNCTION sa_prodrate(SO2_mix, time, day, latit, longit,temp,pres,OH_conc)

IMPLICIT NONE

REAL(real_x), INTENT(IN) :: SO2_mix, time, day, latit, longit,temp,pres,OH_conc
REAL(real_x) :: k300, kinf, Fc, k0O2, k0N2, kO2, kN2, c_O2, c_N2, cos_zen

! Chemical reaction rate coefficients (from L. Pirjola's PhD thesis, paper I)
! same values for O2 and N2
k300 = 5.0e-31        ! cm6/(molecules2 s)
kinf = 2.0e-12        ! cm3/(molecules s)

! O2 and N2 concentrations (1/cm3) as a function of temperature at pressure of 1 atm.
c_O2 = 0.209460*2.46e19*298.0/temp
c_N2 = 0.780840*2.46e19*298.0/temp

! Low pressure rate constants in cm3/(molecules2 s) = cm3/s
k0O2 = k300*(temp/300.0)**(-3.3)*c_O2
k0N2 = k300*(temp/300.0)**(-3.3)*c_N2

Fc = exp(-temp/380.0)

! Rate constants in cm3/s
kO2 = k0O2/(1.0 + k0O2/kinf) * Fc**(1.0/(1+(log10(k0O2/kinf))**2))

kN2 = k0N2/(1.0 + k0N2/kinf) * Fc**(1.0/(1+(log10(k0N2/kinf))**2))

! Total SO2 production rate in 1/(m3 s) (SO2_mix in ppt)
sa_prodrate = OH_conc*(kO2 + kN2) *SO2_mix*1.0e-12*pres/(boltzmann*temp)

END FUNCTION sa_prodrate


!-----------------------------------------------------------------------------
! Subroutine DILUTE_CONC dilutes particle and gas concentrations
! according to boundary layer height increase
!-----------------------------------------------------------------------------
SUBROUTINE dilute_conc(so2_mix, bl_old, boundaryheight,n_conc,vol_conc,vap_conc)

IMPLICIT NONE

REAL(real_x), INTENT(IN) :: bl_old, boundaryheight
real(real_x), intent(inout) :: so2_mix
real(real_x),dimension(sections),intent(inout) :: n_conc
real(real_x),dimension(sections,compo),intent(inout) :: vol_conc
real(real_x),dimension(cond),intent(inout) :: vap_conc

integer :: ii

n_conc = n_conc*bl_old/boundaryheight
vol_conc = vol_conc*bl_old/boundaryheight
so2_mix = so2_mix*bl_old/boundaryheight
do ii=2,cond
    vap_conc(ii) = vap_conc(ii)*bl_old/boundaryheight
end do


END SUBROUTINE dilute_conc

! -------------------------------------------------------------------------
! Change the boundary layer height according to the zenith angle of the sun
! -------------------------------------------------------------------------
subroutine bl_height(time,day,latit,longit,boundaryheight)
implicit none
real(real_x),intent(in) :: time,day,latit,longit
real(real_x),intent(inout) :: boundaryheight
IF (time<=12.*3600.0 .and. cos_zenith(time,day,latit,longit)>0.) THEN
    boundaryheight = 300.0 + 500.0*cos_zenith(time,day,latit,longit)
!      boundaryheight = 200.0 + 225.0*(sin(pi/5.0*time/3600.0 - 21.0*pi/10.0) + 1)
!      boundaryheight = 200.0 + 500.0*oh/1.32e12
END IF
end subroutine bl_height


!----------------------------------------------------------------
! Calculate the evolution of organic vapors within the Volatility
! Basis Set (VBS) due to oxidation (rate)
!----------------------------------------------------------------
function vbs_oxid(vap_conc,OH_conc)
implicit none
real(real_x), dimension(cond) :: vbs_oxid ! #/m^3/s
real(real_x),dimension(cond),intent(in) :: vap_conc
real(real_x),intent(in) :: OH_conc
integer :: ii
real(real_x) :: k_oh=5.d-13        ! reaction rate with OH (cm^3/s)

vbs_oxid=0.
do ii=2,cond-1
    vbs_oxid(ii)=vap_conc(ii)*k_oh*OH_conc
enddo

end function vbs_oxid

function changevapors(time,ambient) result(vapors)

real(uhma_real_x), intent(in) :: time
type(uhma_ambient), intent(in) :: ambient
real(uhma_real_x), dimension(cond) :: vapors

vapors = ambient%vap_conc
vapors(2) = 0.0d0
vapors(1) = cut_sinusoidal_profile(time, 2*18*60*60d0, 0.0d0, 5.0d6*1.0d6, 0.d0)!5.0d7*1.0d6

!ambient%vap_conc = vapors

end function changevapors

function get_deltavapor(time) result(delta_vapor)

real(uhma_real_x), intent(in) :: time
real(uhma_real_x), dimension(cond) :: delta_vapor

!rate of change is ill defined in the endpoint
delta_vapor = 0
delta_vapor(1) = cut_sinusoidal_derivate(time, h2so4sine_lenght, 0.0d0, h2so4sine_max)

end function get_deltavapor


!!$subroutine update_gde_variable_order(variables)
!!$
!!$  type(gde_variables), intent(inout) :: variables
!!$  integer, dimension(uhma_sections) :: swaps
!!$
!!$  call give_order(variables%rdry, swaps)
!!$
!!$  variables%order = swaps
!!$
!!$end subroutine update_gde_variable_order

end module uhma_misc
