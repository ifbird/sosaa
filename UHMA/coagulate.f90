!--------------------------------------------------------
! Contains routines and functions related to coagulation
!-------------------------------------------------------- 

module coagulate

use uhma_datatypes
implicit none

private
integer, parameter :: sections=uhma_sections,&
                      cond=uhma_cond,&
                      compo=uhma_compo,&
                      real_x=uhma_real_x

real(real_x), parameter :: pi = 3.1415927, &
pstand = 1.01325e5,     &          ! standard pressure (Pa)
boltzmann = 1.3807d-23             ! Boltzmann constant (J/K)

public :: coagulation

contains

!oooooooooooooooooooooooooooooo  COAGULATION  oooooooooooooooooooooooooooooooooooooooooooooooo
!
! Subroutine COAGULATION calculates the effect of
!   coagulation on particle population
!
!   n_coag          =   particle concentration change due to coagulation in each section (#/m^3 s)
!   vol_coag        =   vol conc. change of each compound  - " - (um^3/(m^3 s))
!
!ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

subroutine coagulation(n_conc, vol_conc, core, rdry, radius, mass, particles, ambient, options, order, dnumber, dvolume)


    real(uhma_real_x), dimension(sections), intent(in) :: n_conc, core, rdry, radius, mass ! current values
    real(uhma_real_x), dimension(sections,compo), intent(in) :: vol_conc

    integer, dimension(:), intent(in) :: order

    !Values assumed to be constant in the short term (may change outside the solver)
    type(uhma_particles),intent(in) :: particles
    type(uhma_ambient),intent(in) :: ambient
    type(uhma_options),intent(in) :: options

    integer, dimension(size(order)) :: bin_index

    real(uhma_real_x) , dimension(size(order)) :: ordered_cores 

    real(uhma_real_x), dimension(sections), intent(inout) :: dnumber !change rates
    real(uhma_real_x), dimension(sections,compo), intent(inout) :: dvolume

    real(uhma_real_x),dimension(sections,sections) :: coag_coeff

    real(uhma_real_x) :: delta, coagul, core_new 
    real(uhma_real_x), dimension(compo) :: vol_new 

    integer :: i, j, ii, jj, kk, selected_bin

    !Removed chk_coag (it can use incorrect assumptions due to floating point arithmetic)
    !(think: very big particles + very small particles is more or less just very big particles)
    !It also doesn't account for a changing environment
    
    !Calculating bin_index, which is basically the reverse operation of using order to index (for place lookup)
    forall(i=1:size(bin_index))
       bin_index(i) = i
    end forall
    bin_index(order) = bin_index
    
    ordered_cores = core(order)

    coag_coeff = 0
    
    do i=1,sections
        do j=i,sections
            if(n_conc(i) > particles%n_limit .and. n_conc(j) > particles%n_limit) then
               if(mass(i) <= 0 .or. mass(j) <=0) then
                  write (*,*) i,j, n_conc(i), n_conc(j)
                  write (*,*) vol_conc(i,:)
                  write (*,*) vol_conc(j,:)
                  stop 'One bin is empty but has number concentration'
               end if
               coag_coeff(i,j)=calc_coag_coeff(i,j,radius,mass,ambient%temp,ambient%pres)
            end if
        end do
        do j=1,i
            coag_coeff(i,j)=coag_coeff(j,i)
        end do
    end do
    !Coagulate
    do jj=1,sections
       if(n_conc(jj) < particles%n_limit) cycle
       
       do ii = 1,jj
          if(n_conc(ii) < particles%n_limit) cycle

          if(jj == ii) then
             delta = 0.5 !self-coagulation (indistinguishability)
          else 
             delta = 1
          end if

          ! volume of the core (m^3) and constituent volumes (um^3) of one
          ! particle formed in collision
          core_new = core(ii) + core(jj)
          vol_new = vol_conc(ii,:)/n_conc(ii) + vol_conc(jj,:)/n_conc(jj)

          coagul = delta*coag_coeff(ii,jj)*n_conc(ii)*n_conc(jj)      ! collision frequency (m^-3 s^-1)

          !search for the biggest bin smaller than the new core
          !by definition such a bin must exist (it may be one of the coagulating bins)
          do kk=max(bin_index(ii), bin_index(jj)), sections
             if(kk==sections) then !Fortran logical operations do not necessarily short circuit
                exit
             else if(ordered_cores(kk) <= core_new .and. ordered_cores(kk+1) > core_new) then             
                exit
             end if
          end do

          selected_bin = order(kk)

          !placement of of coagulated particles
          !NOTE: THIS WILL GROW THE BIN IF core_new /= core(selected_bin) !!THIS IS ALMOST ALWAYS THE CASE AND IT IS INTENTIONAL. !!
          dnumber(selected_bin) = dnumber(selected_bin) + coagul ! (m^-3 s^-1)
          dvolume(selected_bin,:) = dvolume(selected_bin,:) + coagul*vol_new        ! (um^3/(m^3 s))
          
          !and removal from original bins
          dnumber(ii) = dnumber(ii) - coagul ! (m^-3 s^-1)
          dnumber(jj) = dnumber(jj) - coagul ! (m^-3 s^-1)
          dvolume(ii,:) = dvolume(ii,:) - coagul*vol_conc(ii,:)/n_conc(ii)        ! (um^3/(m^3 s))
          dvolume(jj,:) = dvolume(jj,:) - coagul*vol_conc(jj,:)/n_conc(jj)         ! (um^3/(m^3 s))
          

       end do
    end do
    

end subroutine coagulation
!!$subroutine coagulation(n_coag, vol_coag, volgr_coag,particles,ambient,dist_approach)
!!$
!!$implicit none
!!$
!!$type(uhma_particles), intent(in) :: particles
!!$type(uhma_ambient),intent(in) :: ambient
!!$real(real_x), dimension(sections), intent(out) :: n_coag,volgr_coag
!!$real(real_x), dimension(sections, compo), intent(out) :: vol_coag
!!$
!!$character (len=2) :: dist_approach
!!$real(real_x),dimension(sections,sections) :: coag_coeff
!!$
!!$integer :: ii, jj, kk, kk_actual
!!$real(real_x), dimension(compo) :: vol_new
!!$real(real_x) :: core_new, aa, bb, coagul, delta, n_limit, temp,pres
!!$
!!$real(real_x),dimension(sections) :: n_conc,radius,core,mass
!!$real(real_x),dimension(sections,compo) :: vol_conc
!!$n_conc=particles%n_conc
!!$n_limit=particles%n_limit
!!$core=particles%core
!!$mass=particles%mass
!!$radius=particles%radius
!!$vol_conc=particles%vol_conc
!!$
!!$temp=ambient%temp
!!$pres=ambient%pres
!!$
!!$call chk_coag(radius,mass,temp,pres,coag_coeff)             ! calculate coag. coeffs.
!!$
!!$n_coag = 0.                 ! number concentration net change rate (#/m^3 s)
!!$vol_coag = 0.               ! volume concentration net change rate (um^3/(m^3 s))
!!$volgr_coag = 0.             ! volume growth rate due to coagulation
!!$
!!$! particles and volume added to each section by coagulation
!!$
!!$do jj = 1,sections
!!$    if (n_conc(jj) < n_limit) goto 10
!!$
!!$    do ii = 1,jj
!!$        if (n_conc(ii) < n_limit) goto 20
!!$
!!$        if (ii.eq.jj) then
!!$            delta = 0.5                !self-coagulation (indistinguishability)
!!$        else
!!$            delta = 1.
!!$        end if
!!$
!!$        ! volume of the core (m^3) and constituent volumes (um^3) of one
!!$        ! particle formed in collision
!!$        core_new = core(ii) + core(jj)
!!$        vol_new = vol_conc(ii,:)/n_conc(ii) + vol_conc(jj,:)/n_conc(jj)
!!$
!!$
!!$            do kk = jj, sections-1
!!$
!!$                ! find the sections between which the new particle falls
!!$                if (core_new >= core(particles%order(kk)) .and. core_new < core(particles%order(kk+1)) .and. core(particles%order(kk)) /= core(particles%order(kk+1))) then
!!$                    coagul = delta*coag_coeff(ii,jj)*n_conc(ii)*n_conc(jj)      ! collision frequency (m^-3 s^-1)
!!$
!!$                    kk_actual = particles%order(kk)
!!$                    
!!$                    if (dist_approach=='ms') then !estimate particle growth *** EXPERIMENTAL ***
!!$                        n_coag(kk_actual) = n_coag(kk_actual) + coagul                        ! (m^-3 s^-1)
!!$                        vol_coag(kk_actual,:) = vol_coag(kk_actual,:) + core(kk_actual)/core_new*coagul*vol_new        ! (um^3/(m^3 s)) ? What is this doing ?
!!$                        ! core(kk_actual)/core_new > 1, vol_new is the volume of the particle created in coagulation ...
!!$                        ! Maybe remove core(kk_actual)/core_new
!!$
!!$                        !This growth should not happen (core volume is detemined by volume, and we solve volume) WE DO NOT CALCULATE THE CHANGE RATE FOR CORE!!!
!!$                        if(n_conc(kk_actual) /= 0) then
!!$                           volgr_coag(kk_actual) = n_coag(kk_actual)/n_conc(kk_actual)*(core_new-core(kk_actual))                ! (m^3 s^-1)
!!$                        else
!!$                           volgr_coag(kk_actual) = 0
!!$                        end if
!!$
!!$
!!$                    else    !default: no particle growth due to coagulation
!!$                        aa = (core(kk+1)-core_new)/(core(kk+1)-core(kk))        ! number fraction moved to lower section
!!$                        bb = (core_new-core(kk))/(core(kk+1)-core(kk))          !            -"-     higher section
!!$
!!$                        n_coag(kk) = n_coag(kk) + aa*coagul                     ! (m^-3 s^-1)
!!$                        n_coag(kk+1) = n_coag(kk+1) + bb*coagul
!!$                        vol_coag(kk,:) = vol_coag(kk,:) + aa*core(kk)/core_new*coagul*vol_new        ! (um^3/(m^3 s))
!!$                        vol_coag(kk+1,:) = vol_coag(kk+1,:) + bb*core(kk+1)/core_new*coagul*vol_new
!!$                    endif
!!$
!!$                    goto 20
!!$
!!$                end if
!!$            end do
!!$
!!$
!!$        ! if resulting particle larger than the biggest size section, volume conserved
!!$        !  and number adjusted - choose the largest bin large enough!
!!$        if (core_new >= core(sections)) then
!!$
!!$            n_coag(sections) = n_coag(sections) + delta*coag_coeff(ii,jj)*n_conc(ii)*n_conc(jj)* &
!!$            core_new/core(sections)
!!$            vol_coag(sections,:) = vol_coag(sections,:) + delta*coag_coeff(ii,jj)*n_conc(ii)*n_conc(jj) &
!!$            *vol_new
!!$        end if
!!$
!!$        20                        continue
!!$    end do
!!$    10                continue
!!$end do
!!$
!!$! number and volume removed from each section by coagulation
!!$do ii = 1, sections
!!$    if (n_conc(ii) < n_limit) goto 30
!!$
!!$    do jj = 1, sections
!!$        if (n_conc(jj) < n_limit) goto 40
!!$
!!$        coagul = coag_coeff(ii,jj)*n_conc(ii)*n_conc(jj)                ! collision frequency (m^-3 s^-1)
!!$
!!$        n_coag(ii) = n_coag(ii) - coagul
!!$        vol_coag(ii,:) = vol_coag(ii,:) - coagul*vol_conc(ii,:)/n_conc(ii)
!!$
!!$        40                        continue
!!$    end do
!!$    30                continue
!!$end do
!!$end subroutine coagulation


! ------------------------------------------------------------------------------------------
! Checks if coagulation coefficients have to be calculated again,
! and does so if necessary.
! ------------------------------------------------------------------------------------------
subroutine chk_coag(radius,mass,temp,pres,coag_coeff)
implicit none
real(real_x),dimension(:),intent(in) :: radius,mass
real(real_x),intent(in) :: temp,pres
real(real_x),dimension(sections,sections),intent(out) :: coag_coeff
integer :: ii,jj
real(real_x) :: difference, rad_sum
real(real_x),save :: rad_sum_coag=1.d-9
real(real_x),dimension(sections,sections),save :: coag_coeffs

difference=1.   ! allowed difference in the sum of normalized radii in %
rad_sum=sum(radius)                ! checksum of normalized radii

! Re-calculate the coag. coefficients if the "normalized sum" of particle radii
! differs by percentage defined by the variable "difference", and also in the
! beginning of simulation.
if (rad_sum/rad_sum_coag > (1.+difference/100.) .or. &
        rad_sum/rad_sum_coag < (1.-difference/100.)) then
    do ii=1,sections
        do jj=ii,sections
            coag_coeff(ii,jj)=calc_coag_coeff(ii,jj,radius,mass,temp,pres)
        end do
        do jj=1,ii
            coag_coeff(ii,jj)=coag_coeff(jj,ii)
        end do
    end do
    rad_sum_coag=rad_sum
    !write(*,*) 'Calculating coagulation coefficients...'
    coag_coeffs=coag_coeff
end if

coag_coeff=coag_coeffs

end subroutine chk_coag

!---------------------------------------------------------------------------
!
!  Calculates coagulation coefficients for Brownian coagulation (m^3/s)
!
!  Following XXX
!
!---------------------------------------------------------------------------

function calc_coag_coeff(ii,jj,radius,mass,temp,pres)

implicit none

integer, intent(in) :: ii, jj
real(real_x),dimension(:),intent(in) :: radius,mass
real(real_x),intent(in) :: temp,pres

real(real_x) :: calc_coag_coeff
real(real_x) :: free_path, viscosity, vel_12, rad_12, dif_12
real(real_x) :: prov1, prov2, dist, continuum, free_molec
real(real_x), dimension(2) :: radii, m_part, knudsen, corr, diff, veloc, omega
real(real_x), parameter :: stick_prob=1. !sticking probability

! air mean free path (m) and viscosity (kg/ms) (where from?)
free_path = (6.73d-8*temp*(1.+110.4/temp))/(296.*pres/pstand*1.373)
viscosity = (1.832d-5*406.4*temp**1.5)/(5093*(temp+110.4))

! for both sections
radii(1) = radius(ii)        ! radii of colliding particles (m)
radii(2) = radius(jj)
m_part(1) = mass(ii)        ! masses of colliding particles (kg)
m_part(2) = mass(jj)

knudsen = free_path/radii                                                                ! particle Knudsen number
corr = 1. + knudsen*(1.142+0.558*exp(-0.999/knudsen))        ! Cunninghan correction factor (Allen and Raabe, Aerosol Sci. Tech. 4, 269)
diff = boltzmann*temp*corr/(6*pi*viscosity*radii)                ! particle diffusion coefficient (m^2/s)
veloc = sqrt((8.*boltzmann*temp)/(pi*m_part))                        ! mean thermal velocity of a particle (m/s)
omega = 8.*diff/(pi*veloc)             ! mean free path (m)

vel_12 = sqrt(veloc(1)**2 + veloc(2)**2)        ! mean relative thermal velocity
rad_12 = sum(radii)
dif_12 = diff(1)+diff(2)               ! relative diffusion coefficient
continuum = 4.*pi*rad_12*dif_12        ! flux in continuum regime
free_molec = pi*vel_12*rad_12**2       ! flux in free molecular regime

! flux matching according to Fuchs (1964) (e.g. Seinfeld & Pandis p. 661)
prov1 = (rad_12+omega(1))**3 - (rad_12**2 + omega(1)**2)**1.5
prov1 = prov1/(3.*rad_12*omega(1)) - rad_12
prov2 = (rad_12+omega(2))**3 - (rad_12**2 + omega(2)**2)**1.5
prov2 = prov2/(3.*rad_12*omega(2)) - rad_12
dist = sqrt(prov1**2 + prov2**2)        ! distance at which fluxes are matched

! coagulation coefficient between particles [m^3/s]
calc_coag_coeff = stick_prob*continuum / (rad_12/(rad_12+dist) + continuum/free_molec)
end function calc_coag_coeff

end module coagulate
