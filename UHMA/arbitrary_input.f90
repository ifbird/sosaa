module arbitrary_input
    use uhma_datatypes
    implicit none
    private

    real(uhma_real_x), parameter :: pi = 4*atan(1.0_uhma_real_x)

    public :: sinusoidal_profile, cut_sinusoidal_profile, cut_sinusoidal_derivate

contains

    pure function sinusoidal_profile(time, length, offset, maxium, minium)  &
    result(value)

    real(uhma_real_x), intent(in) :: time, length, offset, maxium, minium
    real(uhma_real_x) :: value


    value = maxium*sin(2*pi*(1.0*time/length) + offset)

    if(value < minium) then
    value = minium
    end if

    return

    end function sinusoidal_profile!

    pure function cut_sinusoidal_profile(time, length, offset, maxium, minium)  &
    result(value)

    real(uhma_real_x), intent(in) :: time, length, offset, maxium, minium
    real(uhma_real_x) :: value

    value = 0

    if(time <= length) then
        value = maxium*sin(2*pi*(1.0*time/length) + offset)
    end if

    if(value < minium) then
    value = minium
    end if

    return

    end function cut_sinusoidal_profile!

    pure function cut_sinusoidal_derivate(time, length, offset, maxium) &
    result(value)

    real(uhma_real_x), intent(in) :: time, length, offset, maxium
    real(uhma_real_x) :: value

    value = 0
    if(time <= length) then
        value = maxium*(2*pi/length)*cos(2*pi*(1.0*time/length) + offset)
    end if

    end function cut_sinusoidal_derivate

end module arbitrary_input
