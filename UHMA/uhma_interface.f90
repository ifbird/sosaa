!**********************************************************************************************************************************************
!
! This module should make communication between UHMA and the model UHMA is implemeted e.g. MALTE-BOX
!
!**********************************************************************************************************************************************

module uhma_interface


  !Note: MALTE uses cm^-3 UHMA uses m^-3

  use uhma_datatypes
  use distribute
  use gde_solver
  use sorting

  implicit none

  integer, parameter :: rk_x= selected_real_kind(10, 40)
  ! initial_vapor_number, this value is used to set the initial mfraction. Now =655 (original 2)means the initial particle is INMAT

  ! Set mfraction, need change when changing the vapor_properties file - remember it is the amout of condesable vapours in the file
  ! plus one which is sulfuric acid in the particle phase
  integer, parameter :: initial_vapor_number = uhma_cond - 1  !1175!372 !486 !692 !614 !597 !784 !801 !784 !1290
  !.......................................NOTICE....................................................
  real(rk_x), parameter :: conversion_cm3_to_m3 = 1E6, um3_to_m3 = (1D-6)**3.0

  real(uhma_real_x), private, parameter :: pi = 4.0_rk_x*atan(1.0_rk_x)

  real(uhma_real_x), parameter :: boltzmann = 1.3806488E-23 !J/K
  real(uhma_real_x), parameter :: avogadro = 6.02214129E23 !mol^-1
  real(uhma_real_x), parameter :: r_g = 8.314                             ! molar gas constant (J/(mol K))
  real(uhma_real_x), parameter :: from_g_per_cm3_to_kg_per_m3 = 1E3 !1E-3/1E-6
  real(uhma_real_x), parameter :: volume_epsilon = 1E-6
  real(uhma_real_x), parameter :: particle_eps = 1

  real(uhma_real_x), dimension(uhma_sections) :: fixed_radii, fixed_volumes


  integer, parameter :: maxbuf = 1300

  !Things to keep track of the situation in moving sections
  !integer, dimension(uhma_sections) :: order

contains



  !**********************************************************************************************************************************************
  !
  ! Interface to run UHMA for a specified time
  !
  !**********************************************************************************************************************************************

  subroutine run_aerosol(particle_properties, chemical_concentrations, ambient_properties, options, time, duration, timestep)

    !Use this interface when everything is stored in main program
    !Properly initialize particle_properties, ambient_properties and options
    !prior to calling the subroutine
    type(uhma_particles), intent(inout) :: particle_properties
    real(uhma_real_x), dimension(:) :: chemical_concentrations
    type(uhma_ambient), intent(inout) :: ambient_properties
    type(uhma_options), intent(in) :: options
    real(uhma_real_x):: nuc_rate, Nuc_by_charge(3),ipr
    real(uhma_real_x), intent(inout) :: time
    real(uhma_real_x), intent(in) :: duration,timestep

    real(uhma_real_x) :: endtime,timestep_gde,time0
    integer :: i, loops

    !DEBUG
    logical :: test

    !END DEBUG

    if(options%quasistationary) then
      !Assign in case the volumes are changed (i.e read in by dmps)
      fixed_radii = particle_properties%rdry
      fixed_volumes = (4.0/3.0)*pi*(particle_properties%rdry)**3
    end if

    !Assigning values from chemistry concentrations to ambient vapor concentrations
    !indexing + unit conversion
    call pick_concentrations(chemical_concentrations, ambient_properties)

    endtime = time+duration           ! simulation end time (s)
    loops = int(duration/timestep)    ! number of simulation loops
    do i=1,loops
      timestep_gde=timestep
      time0=time

      !run uhma for specified time
      do while (time<time0+timestep)
        ipr=3.0d0
        ! [Debug Putian], calculate how much it needs for solve_gde, need to be generalized in future
        call solve_gde(time,timestep_gde,particle_properties,ambient_properties,options,particle_properties%nuc_rate,ipr*1D6,Nuc_by_charge)    ! Solves the Grand Dynamic Equation
        time=time+timestep_gde

        if(options%dist_approach == 'ms') then
          call give_order(particle_properties%rdry, particle_properties%order)
        end if
      end do
    end do
    if(options%vapor_loss) then
      !Assigning values from ambient vapor concentrations to chemistry concentrations
      !indexing + special considerations + unit conversion
      call modify_chemical_concentrations(ambient_properties, chemical_concentrations)
    end if
    if(options%quasistationary) then
      particle_properties = fit_back_to_fixed(particle_properties, fixed_radii, fixed_volumes)
      call equilibrium_size(particle_properties,ambient_properties,options)
    end if

    ! [Putian Debug]
    ! write(*,*) 'particle rdry: ', sum(particle_properties%rdry), maxval(particle_properties%rdry), minval(particle_properties%rdry)
    ! write(*,*) 'particle mass: ', sum(particle_properties%mass), maxval(particle_properties%mass), minval(particle_properties%mass)

  end subroutine run_aerosol
  subroutine Assign_environmental_data(ambient_properties,temp, rh, pressure,nh3)

    type(uhma_ambient), intent(inout) :: ambient_properties
    real(uhma_real_x), intent(in) :: temp, rh, pressure,nh3

    ambient_properties%temp = temp
    ambient_properties%rh = rh
    ambient_properties%pres = pressure
    ambient_properties%nh3_cm3=nh3
    !Calculate saturation vapor concentrations based on environmental properties
    !IMPORTANT: if there is a '/' sign in the txt-file the model will not read this line - it will stop and use the values from the line before!
    ambient_properties%c_sat = calculate_saturation(ambient_properties%parameter_A, ambient_properties%parameter_B, ambient_properties%temp)
    ambient_properties%c_sat(ambient_properties%sulfuric_acid_index) = 1

    !for sensitive test -Qi
    !  ambient_properties%c_sat(616:622) = 0




    !write(*,*) '211: ', 10**(ambient_properties%parameter_A(211) - ambient_properties%parameter_B(211)/temp) *101325. / boltzmann / temp, ambient_properties%parameter_A(211), ambient_properties%parameter_B(211)
    !write(*,*) '212: ', 10**(ambient_properties%parameter_A(212) - ambient_properties%parameter_B(212)/temp) *101325. / boltzmann / temp, ambient_properties%parameter_A(212), ambient_properties%parameter_B(212)
    !write(*,*) '213: ', 10**(ambient_properties%parameter_A(213) - ambient_properties%parameter_B(213)/temp) *101325. / boltzmann / temp, ambient_properties%parameter_A(213), ambient_properties%parameter_B(213)
    !write(*,*) '214: ', 10**(ambient_properties%parameter_A(214) - ambient_properties%parameter_B(214)/temp) *101325. / boltzmann / temp, ambient_properties%parameter_A(214), ambient_properties%parameter_B(214)
    !write(*,*) '215: ', 10**(ambient_properties%parameter_A(215) - ambient_properties%parameter_B(215)/temp) *101325. / boltzmann / temp, ambient_properties%parameter_A(215), ambient_properties%parameter_B(215)

    !write(*,*) '213: ', 10**(ambient_properties%parameter_A(213) - ambient_properties%parameter_B(213)/temp) *101325. / boltzmann / temp, ambient_properties%parameter_A(213), ambient_properties%parameter_B(213)
    !write(*,*) '214: ', 10**(ambient_properties%parameter_A(214) - ambient_properties%parameter_B(214)/temp) *101325. / boltzmann / temp, ambient_properties%parameter_A(214), ambient_properties%parameter_B(214)
    !write(*,*) '215: ', 10**(ambient_properties%parameter_A(215) - ambient_properties%parameter_B(215)/temp) *101325. / boltzmann / temp, ambient_properties%parameter_A(215), ambient_properties%parameter_B(215)
    !write(*,*) '216: ', 10**(ambient_properties%parameter_A(216) - ambient_properties%parameter_B(216)/temp) *101325. / boltzmann / temp, ambient_properties%parameter_A(216), ambient_properties%parameter_B(216)
    !write(*,*) '217: ', 10**(ambient_properties%parameter_A(217) - ambient_properties%parameter_B(217)/temp) *101325. / boltzmann / temp, ambient_properties%parameter_A(217), ambient_properties%parameter_B(217)

  end subroutine Assign_environmental_data

  !**********************************************************************************************************************************************
  !
  ! Assigning values from chemistry concentrations to ambient vapor concentrations
  !
  !**********************************************************************************************************************************************

  subroutine pick_concentrations(concs,ambient_properties)
    real(uhma_real_x), dimension(:), intent(in) :: concs
    type(uhma_ambient), intent(inout) :: ambient_properties
    where(ambient_properties%index_in_chemistry /= chemical_not_found)
      ambient_properties%vap_conc = concs(ambient_properties%index_in_chemistry)*conversion_cm3_to_m3
    elsewhere
      ambient_properties%vap_conc = 0
    end where
    ambient_properties%nh3 = ambient_properties%nh3_cm3*conversion_cm3_to_m3
    !ambient_properties%nh3 = nh3*conversion_cm3_to_m3
    ! write(*,*) '[Putian Debug]: rk_x', rk_x
    !write(*,*) '[Putian Debug]: concs', sum(concs)
    !write(*,*) '[Putian Debug]: vap_conc', sum(ambient_properties%vap_conc)

  end subroutine pick_concentrations


  !**********************************************************************************************************************************************
  !
  ! Assigning values from ambient vapor concentrations to chemistry concentrations
  !
  !**********************************************************************************************************************************************

  subroutine modify_chemical_concentrations(ambient_properties, concs)

    type(uhma_ambient), intent(in) :: ambient_properties
    real(uhma_real_x), dimension(:), intent(inout) :: concs

    integer :: i

    forall(i=1:size(ambient_properties%vap_conc,1), ambient_properties%index_in_chemistry(i) /= chemical_not_found .and. i /= ambient_properties%sulfuric_acid_index)
      concs(ambient_properties%index_in_chemistry(i)) = ambient_properties%vap_conc(i)/conversion_cm3_to_m3
    end forall

    !We should leave some vapors alone (which ones?)
    !TODO leave sulfuric acid also nitric acid alone

  end subroutine modify_chemical_concentrations


  !**********************************************************************************************************************************************
  !
  ! Subprograms for initializing/manipulating uhma_ambient structures
  !
  !**********************************************************************************************************************************************

  subroutine map_names(ambient_properties, chemistry_names)

    type(uhma_ambient),intent(inout) :: ambient_properties
    character(len=*), dimension(:), intent(in) :: chemistry_names

    ambient_properties%index_in_chemistry = lookup_species(ambient_properties%vapor_names, chemistry_names)

    where(ambient_properties%index_in_chemistry == chemical_not_found) ambient_properties%condensing_type = not_available
      !system%concentrations = get_available(ambient_properties%index_in_chemistry, gas_concentrations)
      write (*,*), 'Chemicals match:', count(ambient_properties%index_in_chemistry /= chemical_not_found)

      !What do we do when identities do not exist in chemistry?
      !Currently we set to zero and set as not_available in condensation

    end subroutine map_names


    !**********************************************************************************************************************************************
    !
    ! This function is linked with the subroutine above
    !
    !**********************************************************************************************************************************************

    function lookup_species(molnames, chem_molnames)

      character(len=*), dimension(:), intent(in) :: molnames, chem_molnames
      integer, dimension(size(molnames,1)) :: lookup_species

      integer :: i, j

      lookup_species = chemical_not_found

      do i=1,size(molnames,1)
        do j=1,size(chem_molnames, 1)
          if(trim(molnames(i)) == trim(chem_molnames(j))) then
            lookup_species(i) = j
            exit
          end if
        end do
      end do

    end function lookup_species


    !**********************************************************************************************************************************************
    !
    ! ???
    !
    !**********************************************************************************************************************************************

    subroutine read_in_vapor_properties(filename, ambient_properties, ionumber)

      character(len=*), intent(in) :: filename  ! for input_flag == 0, it is the folder of condensable_vapors
      type(uhma_ambient), intent(inout) :: ambient_properties
      integer, optional :: ionumber

      character(len=MAXBUF) :: chem_name, smiles
      real(uhma_real_x) :: molecular_mass, density, diffusion_volume, surface_tension, &
        mass_accomodation, parameter_A, parameter_B

      integer :: vap_type, cond_type

      !List of all chemical properties is available at start, thus we can
      !allocate everything based on the file (and we don't need to change the
      ! array size afterwards).

      integer :: i, io, io2, j, n
      character(len=80) :: line
      if(input_flag .EQ. 0) then
        ! Qi:input_flag = 0: use Pontus Model's method and file
        ! 1: use old version UHMA's file. change it in uhma_datatypes.f90


        open(100, file=trim(adjustl(filename)) // '/Vapour_properties.dat', action='read')
        open(101, file=trim(adjustl(filename)) // '/Vapour_names.dat', action='read')
        i=0
        do
          read(100,*, iostat=io) line
          if(io < 0) then
            exit
          end if
          i=i+1
        end do

        if(io < 0) then
          rewind(100)
        else
          write (*,*), io
          stop 'File read error'
        end if
        write(*,*) i, (size(ambient_properties%c_sat,1)-1)
        !................read all the organics................
        if(i /= (size(ambient_properties%c_sat,1)-1)) then
          write(*,*) 'Vapors available:',i
          write(*,*) 'UHMA ambient structure is setup for:',size(ambient_properties%c_sat,1)
          write(*,*) 'In order to proceed, change UHMA setup and recompile.'
          stop
        end if
        do j=1,i

          read(100,*,iostat=io)  molecular_mass, parameter_A, parameter_B
          read(101,*,iostat=io2) chem_name
          ambient_properties%vapor_names(j) = TRIM(chem_name)
          ! ambient_properties%diff_vol(j) = diffusion_volume !don't use it in this method
          ambient_properties%density(j) = 1400.        ! constant value for all organic
          ambient_properties%surf_ten(j) = 0.05       !constant value
          ambient_properties%alpha(j) = 1.         !constant value (same with another method) - mass accomodation coefficient
          ambient_properties%molarmass(j) = molecular_mass
          ambient_properties%molecmass(j) = calculate_molecule_mass(molecular_mass)
          ambient_properties%molecvol(j) = calculate_molecule_volume(ambient_properties%density(j), ambient_properties%molecmass(j))
          ambient_properties%parameter_A(j) = parameter_A
          ambient_properties%parameter_B(j) = parameter_B
          ambient_properties%vapor_type(j) = 2        !assume all the compounds are the soluble organic
          ambient_properties%condensing_type(j) = 3  ! all the compounds are treated as normal condensation
          if ((j.EQ.(i-2)).OR.(j.EQ.(i-1))) then
            ambient_properties%vapor_type(j) = 0  !un-known type
            ambient_properties%condensing_type(j) = not_available !new vapor_property file, the last two row don't condense
          endif
          if (j.EQ.i) then
            ambient_properties%vapor_type(j) = 4  !un-known type
            ambient_properties%condensing_type(j) = not_available !new vapor_property file, the last two row don't condense
          endif

        enddo
        !....................................................
        !.............read the acid....(H2SO4, HCl, HNO3)....
        !..................H2SO4.............................
        ambient_properties%vapor_names(i+1) = 'H2SO4'
        ambient_properties%condensing_type(i+1) = 1   !acid
        ! ambient_properties%diff_vol(i+1) = diffusion_volume !don't use it in this method
        ambient_properties%density(i+1) = 1819.3946        ! kg/m3
        ambient_properties%surf_ten(i+1) = 0.07       !surf_tens(1:8)=(76.1-0.155*(T-273.15))*1D-3 from Pontus
        ambient_properties%alpha(i+1) = 1.         !constant value (same with another method)
        ambient_properties%molarmass(i+1) = 98.0785 !molarmass for H2SO4
        ambient_properties%molecmass(i+1) = calculate_molecule_mass(ambient_properties%molarmass(i+1))
        ambient_properties%molecvol(i+1) = calculate_molecule_volume(ambient_properties%density(i+1), ambient_properties%molecmass(i+1))
        ambient_properties%parameter_A(i+1) = 3.96971803774  !for H2SO4
        ambient_properties%parameter_B(i+1) = 313.607405085  !for H2SO4
        ambient_properties%vapor_type(i+1) = 1        !acid
        ambient_properties%condensing_type(i+1) = 1   !acid
        ambient_properties%sulfuric_acid_index = i+1  !index for sulfuric acid

        !..........................................................

        close(100)
        close(101)

      endif
      !...........................................................................................
      !..................  different input file. 0:Pontus 1: old UHMA.............................
      if (input_flag .EQ. 1) then

        if(.not.present(ionumber)) then
          n = 100
        else
          n= ionumber
        end if

        open(n, file=filename, action='read')
        write(*,*) filename
        i=0
        do
          read(n,*, iostat=io) line
          if(io < 0) then
            exit
          end if
          i=i+1
        end do

        if(io < 0) then
          rewind(n)
        else
          write (*,*), io
          stop 'File read error'
        end if


        if(i /= size(ambient_properties%c_sat,1)) then
          write(*,*) 'Vapors available:',i
          write(*,*) 'UHMA ambient structure is setup for:',size(ambient_properties%c_sat,1)
          write(*,*) 'In order to proceed, change UHMA setup and recompile.'
          stop
        end if

        do j=1,i

          read(n,*,iostat=io) chem_name, smiles, molecular_mass, density, &
            diffusion_volume, surface_tension, mass_accomodation, parameter_A, parameter_B, vap_type, cond_type
          !****************'
          !mass_accomodation is the mass accommodation coefficient in condensation(chance to stick [0-1])
          !Parameters A and B are the saturation vapour pressure parametrisation
          !vap_type refer to the vapour types in uhma_datatypes.f90 1:sulfuric acid (and nitric acid?), 2:soluble_organic, 3:generic_organic, 4:generic
          !cond_type refers to how it is treated in condensation. 1:acid,2:kohler,3:normal0:not_available
          !*********************************
          ambient_properties%vapor_names(j) = TRIM(chem_name)

          ambient_properties%diff_vol(j) = diffusion_volume
          ambient_properties%density(j) = density*from_g_per_cm3_to_kg_per_m3
          ambient_properties%surf_ten(j) = surface_tension
          ambient_properties%alpha(j) = mass_accomodation
          ambient_properties%molarmass(j) = molecular_mass
          ambient_properties%molecmass(j) = calculate_molecule_mass(molecular_mass)
          ambient_properties%molecvol(j) = calculate_molecule_volume(ambient_properties%density(j), ambient_properties%molecmass(j))
          ambient_properties%parameter_A(j) = parameter_A
          ambient_properties%parameter_B(j) = parameter_B
          ambient_properties%vapor_type(j) = vap_type
          ambient_properties%condensing_type(j) = cond_type
          if(vap_type == sulfuric_acid) then
            ambient_properties%sulfuric_acid_index = j
          end if

        end do
        !stop

        close(n)
      endif
    end subroutine read_in_vapor_properties


    !**********************************************************************************************************************************************
    !
    ! ???
    !
    !**********************************************************************************************************************************************

    pure elemental function calculate_molecule_mass(molar_mass) result(mass)

      real(uhma_real_x), intent(in) :: molar_mass  ! [g mol-1]
      real(uhma_real_x) :: mass

      mass = (molar_mass*1d-3)/(avogadro)

    end function calculate_molecule_mass

    pure elemental function calculate_molecule_volume(molecule_density, molecule_mass) result(volume)

      real(uhma_real_x), intent(in) :: molecule_density, molecule_mass
      real(uhma_real_x) :: volume

      volume = molecule_mass/molecule_density

    end function calculate_molecule_volume


    !**********************************************************************************************************************************************
    !
    ! ???
    !
    !**********************************************************************************************************************************************

    ! Not in use Michael 05/2015

    function locate_sulfuric_acid(ambient_properties) result(H2SO4_ind)

      type(uhma_ambient) :: ambient_properties

      integer :: H2SO4_ind
      integer :: i

      H2SO4_ind = -1

      do i=1,size(ambient_properties%vapor_names,1)
        if(TRIM(ambient_properties%vapor_names(i)) == 'H2SO4') then
          H2SO4_ind = i
          exit
        end if
      end do

      if(H2SO4_ind <= 0) then
        stop 'Cannot find sulfuric acid in chosen compounds'
      end if

    end function locate_sulfuric_acid


    !**********************************************************************************************************************************************
    !
    ! ???
    !
    !**********************************************************************************************************************************************

    subroutine fix_nitric_acid_saturation(ambient_properties, HNO3_saturation)

      type(uhma_ambient), intent(inout) :: ambient_properties
      real(uhma_real_x), intent(in) :: HNO3_saturation

      !   ambient_properties%c_sat(3) = HNO3_saturation !QI:currently we didn't include HNO3

    end subroutine fix_nitric_acid_saturation




    !**********************************************************************************************************************************************
    !
    ! ???
    !
    !**********************************************************************************************************************************************

    pure elemental function calculate_saturation(A,B,temperature) result(vapor_concentration)

      real(uhma_real_x), intent(in) :: A,B,temperature

      real(uhma_real_x) :: vapor_concentration, vapor_pressure


      !From antoine equation log_10(p) = A-B/T
      vapor_pressure =  10**(A - (B/temperature)) !in atm

      !N/V = c = P/kT
      !Note: using ideal gas law
      !Saturation vapor pressure is the partial pressure that the gas has to reach to condense

      !vapor_concentration = vapor_pressure
      vapor_concentration = (vapor_pressure*101325)/(boltzmann*temperature) !#/m^3 !QI: here unit is #/m3, so using boltzmann

    end function calculate_saturation


    !**********************************************************************************************************************************************
    !
    ! ???
    !
    !**********************************************************************************************************************************************

    subroutine initialize_ambient(ambient_properties)

      !Sets misc. properties of the vapors
      !If your simulations require some other values for these, consider a different initialization.
      type(uhma_ambient), intent(inout) :: ambient_properties

      ambient_properties%sink=0.
      ambient_properties%n_crit=0. !Number of molecules in critical cluster

      ambient_properties%vap_conc_min= 0 !Allowing vapors to reach zero
      ambient_properties%gf_org = 1 !hygroscopic growth factor(s) (R_wet/R_dry) of nano-Kohler and water soluble organic compound(s)
      ambient_properties%vap_limit = 1 !Below which vapor is not considered to condensate.

      ambient_properties%nuc_coeff=1.e-14 !<-A7!0.05e-15   !<-A2,A6 !A1:0.1e-15         ! nucleation coefficient (SI units) !2013.05.01
      !       ambient_properties%nuc_coeff=0.05e-15 !<-A2 !A1:0.02e-15         ! nucleation coefficient (SI units)  !2013.05.22
      !       ambient_properties%nuc_coeff=0.05e-15 !<-A6!0.08e-15 !<-A5         !0.12e-15!<-A4! 0.04e-15<-A2   !A1: 0.02e-15         ! nucleation coefficient (SI units)  !2013.06.15
      ! QI: for freedom formation, the unit of nuc_coeff is m3/#, becasue in subroutine pick_concentrations() we change the unit from cm-3 to m-3

      !Old variables (are initialized here, but will probably be removed at some version)
      !Should do nothing. If they do something, please inform me.
      ambient_properties%boundaryheight = 1000.  ! boundary layer height [m]

    end subroutine initialize_ambient


    !**********************************************************************************************************************************************
    !
    ! Subprograms for initializing/manipulating uhma_particles, uhma_options and initial_dist structures
    !
    !**********************************************************************************************************************************************

    subroutine define_initial_distribution(initial_distribution, ambient_properties)

      type(uhma_initial_dist), intent(inout) :: initial_distribution
      type(uhma_ambient), optional, intent(in) :: ambient_properties

      integer :: i

      ! initial_distribution%r_min=1.5d-9!(1.5d-9)/2.          ! (initial) dry radius of smallest section
      initial_distribution%r_min=0.5d-9!(1.5d-9)/2.          ! Change R_min to be smaller by QI,But: not work,Maybe it causes too much particles?  need to be researched ....................Qi
      initial_distribution%r_max=(1.0d-5)!/2.          ! (initial) dry radius of largest section
      initial_distribution%r_max=(1.0d-5)!/2.          ! (initial) dry radius of largest section

      ! initial modes
      initial_distribution%r_mode=(/2.0d-8,1.0d-7/)/2.0  ! mode radius
      initial_distribution%n_mode= (/1.0d7, 1.0d7/)!(/3.5d9,6.4d8/)      ! mode conc.
      initial_distribution%sigma_mode= (/1.5, 1.3/)!(/1.6,1.3/)      ! mode deviation

      initial_distribution%mfractions= 0
      if(present(ambient_properties)) then
        !~40% organics divided equally
        !rest inorganics
        !inorganics treated just like sulfuric acid (i.e. are included in sulfuric acid)
        !i = count((ambient_properties%vapor_type == 2) .or. (ambient_properties%vapor_type == 3))

        !where((ambient_properties%vapor_type == 2) .or. (ambient_properties%vapor_type == 3))
        !  initial_distribution%mfractions = 0
        !end where

        !Let's just do pure sulfuric acid
        !initial_distribution%mfractions(ambient_properties%sulfuric_acid_index) = 1!0.6
        !Fourth vapour is involatile in these runs
        initial_distribution%mfractions(initial_vapor_number)= 1

      else
        !Dumping everything to first vapor, since we don't have any real choices
        initial_distribution%mfractions(initial_vapor_number)= 1
      end if


    end subroutine define_initial_distribution


    !**********************************************************************************************************************************************
    !
    ! ???
    !
    !**********************************************************************************************************************************************

    subroutine initialize_particle_distribution(particle_properties, initial_distribution, ambient_properties, options)

      type(uhma_particles), intent(inout) :: particle_properties
      type(uhma_initial_dist), intent(in) :: initial_distribution
      type(uhma_ambient), intent(inout) :: ambient_properties
      type(uhma_options), intent(in) :: options

      particle_properties%n_conc=0.
      particle_properties%vol_conc=0.
      particle_properties%radius=0.
      particle_properties%gr=0.
      particle_properties%nuc_rate=0.

      particle_properties%n_limit = 1.d-3              ! threshold number concentration (#/m^3)
      particle_properties%cluster_vol=4.*pi/3.*(initial_distribution%r_min)**3.    ! volume of nucleated clusters

      !use distributing function to fill bins
      call init_dist(initial_distribution,particle_properties,ambient_properties%density)

      !and equilibriate with water
      call equilibrium_size(particle_properties,ambient_properties,options)

    end subroutine initialize_particle_distribution


    !**********************************************************************************************************************************************
    !
    ! ???
    !
    !**********************************************************************************************************************************************

    subroutine define_options(options)

      type(uhma_options), intent(inout) :: options

      options%solver= 'rk4'          ! solver for GDE
      options%dist_approach='ms'     ! distribution representation (fx/ms)

      !Fit results back to original fixed sections
      options%quasistationary = .true.

      options%raoult = .true.

      ! Set nuc_approach='ffn' for 'FreeForm' nucleation
      ! Usage: set the variable options%nuc_number to give the nucleating compounds
      ! Examples: nuc_number=1 : sulphuric acid induced activation
      !           nuc_number=13: kinetic nucleation of compounds #1 and #3
      ! Options can be passed with the decimals:
      !           0.5: add vapors, e.g. 13.5: A*(vap(1)+vap(3)) and 0.5 for both in n_crit
      ! options%nuc_approach= 'def'      ! nucleation mechanism

      options%nuc_approach= 'acd'      ! nucleation mechanism

      !NOTE NUC_NUMBER IS NOT IN USE IN CURRENT UHMA VERSIONS, USE NUC_ARRAY
      !format is (compound 1, compound2, special case), where zero means no
      !so [1,0,0] is activation of 1, [1,2,0] is kinetic between 1 and 2,
      ! [1,2,1] is special case between 1 and 2.
      options%nuc_array = (/0,0,0/)!(/1,0,0/)  !QI: This row is useless cause the nuc_array is defined later again.

      ! Flags for vapor dynamics
      options%vapor_loss=.true.       ! loss due to nucleation and condensation

      ! Flags for different processes
      options%nucleation=.true.
      options%condensation=.true.
      options%coagulation=.true.
      options%dry_deposition=.false.!.true.
      options%BLH_dilution=.false.
      options%snow_scavenge=.false.

      ! Flags for other model options
      options%equilibrate=.false.     ! calculate water and ammonia content (buggy), or set wet and dry sizes equal
      options%cluster_divide=.false.  ! divide nucleated clusters to surrounding bins if size inequal

      !The following might not (work/be available) in future versions
      !and do nothing now.
      !----------------------------------------------------------------
      options%vapor_chemistry=.false. ! production and other chemical reactions (inside aerosol module)

      options%meas_interval=300     ! output interval
      options%nuc_number=11            ! options for freeform nucleation ('ffn'), see below

      !Do nothing in the current state (were used for mini chemistry).
      options%latitude=61.51          ! latitude in degrees
      options%longitude=24.17         ! longitude in degrees
      options%day_of_year=87.         ! day of year
      !----------------------------------------------------------------

    end subroutine define_options


    !**********************************************************************************************************************************************
    !
    ! ???
    !
    !**********************************************************************************************************************************************

    function fit_back_to_fixed(current, fixed_sizes, fixed_volumes) result(fixed)

      type(uhma_particles), intent(in) :: current
      real(uhma_real_x), dimension(uhma_sections), intent(in) :: fixed_sizes, fixed_volumes

      real(uhma_real_x), dimension(uhma_sections) :: current_volumes

      type(uhma_particles) :: fixed

      real(uhma_real_x) :: n_j, n_k

      integer :: i,j,k

      !fixed_volumes = (3/4.0)*pi*(fixed_sizes)**3.0 !single particle volume #/m^3
      fixed = current
      fixed%n_conc = 0
      fixed%vol_conc = 0
      fixed%mass = 0
      fixed%radius = fixed_sizes
      fixed%rdry = fixed_sizes
      fixed%core = fixed_volumes
      current_volumes = -1

      do i=1,uhma_sections
        if(current%n_conc(i) /= 0.) then
          current_volumes(i) = (4/3.0)*pi*(current%rdry(i))**3
          !current_volumes(i) = sum(current%vol_conc(i,:))/current%n_conc(i) * um3_to_m3
        end if
      end do

      do i=1,uhma_sections
        if(current_volumes(i)==-1.) cycle
        call get_index(current_volumes(i), fixed_volumes, j)

        k=j+1

        !WARNING:
        !CONSERVES VOLUME AT ALL COSTS. IF THE EDGE CASES HAPPEN, CONSIDER A LARGER DOMAIN. OTHERWISE YOU SACRIFICE NUMBER CONCENTRATION.
        select case(j)
        !Dump the volume to edge bin, adjust number to match
      case(0)
        ! write(*,*) 'Number10'
        fixed%vol_conc(k,:) = fixed%vol_conc(k,:) + current%vol_conc(i,:)
        fixed%n_conc(k) = fixed%n_conc(k) + (sum(current%vol_conc(i,:))*um3_to_m3)/fixed_volumes(k)

      case(uhma_sections)
        !write(*,*) 'Number11'
        fixed%vol_conc(j,:) = fixed%vol_conc(j,:) + current%vol_conc(i,:)
        fixed%n_conc(uhma_sections) = fixed%n_conc(uhma_sections) + (sum(current%vol_conc(i,:))*um3_to_m3)/fixed_volumes(uhma_sections)
        !   stop 'VERY FAST GROWTH'
        !split between adjanced bins
      case(1:(uhma_sections-1))
        !write(*,*) 'Number12'
        n_j = current%n_conc(i) * ((fixed_volumes(k)-current_volumes(i))/(fixed_volumes(k)-fixed_volumes(j)))
        n_k = current%n_conc(i) * ((current_volumes(i)-fixed_volumes(j))/(fixed_volumes(k)-fixed_volumes(j)))
        fixed%n_conc(j) = fixed%n_conc(j) + n_j
        fixed%n_conc(k) = fixed%n_conc(k) + n_k


        fixed%vol_conc(j,:) = fixed%vol_conc(j,:) + ((n_j*fixed_volumes(j))/(n_j*fixed_volumes(j)+n_k*fixed_volumes(k))) * &
          current%vol_conc(i,:)
        fixed%vol_conc(k,:) = fixed%vol_conc(k,:) + ((n_k*fixed_volumes(k))/(n_j*fixed_volumes(j)+n_k*fixed_volumes(k))) * &
          current%vol_conc(i,:)

        !         current%vol_conc(i,:) + ((n_k*fixed_volumes(k))/(n_j*fixed_volumes(j)+n_k*fixed_volumes(k))) * &
        !         current%vol_conc(i,:))

      case default
        write (*,*), 'UNDEFINED BEHAVIOUR in subroutine fit_back_to_fixed.', i,j,k
        stop
      end select
    end do
    !DEBUG
    !stop

  end function fit_back_to_fixed


  !**********************************************************************************************************************************************
  !
  ! ???
  !
  !**********************************************************************************************************************************************

  subroutine get_index(current_volume, volumes, index)

    real(uhma_real_x), intent(in) :: current_volume
    real(uhma_real_x), dimension(uhma_sections), intent(in) :: volumes
    integer, intent(out) :: index

    index = 0

    if(current_volume <= volumes(1)) then
      index = 0
      return
    end if

    do index=1,uhma_sections-1
      if((current_volume > volumes(index)) .and. (current_volume <= volumes(index+1))) return
    end do

    index = uhma_sections

  end subroutine get_index


  !**********************************************************************************************************************************************
  !
  ! Combining these subprograms so one need only to call one initialization.
  ! This sets the values to defaults defined in the initialization functions.
  ! Modify the values or make your own initialization functions if you need to change things.
  !
  !**********************************************************************************************************************************************

  subroutine set_aerosol_structures(particle_properties, ambient_properties, initial_distribution, options, chemistry_names, filename)

    type(uhma_particles), intent(inout) :: particle_properties
    type(uhma_initial_dist), intent(inout) :: initial_distribution
    type(uhma_ambient), intent(inout) :: ambient_properties
    type(uhma_options), intent(inout) :: options

    character(len=*), dimension(:), intent(in) :: chemistry_names
    character(len=*), intent(in) :: filename

    call define_options(options)
    call read_in_vapor_properties(filename, ambient_properties, 777)
    call initialize_ambient(ambient_properties)
    call map_names(ambient_properties, chemistry_names)
    options%nuc_array = (/ambient_properties%sulfuric_acid_index, ambient_properties%sulfuric_acid_index, 0/) !DEFINE the NUC ARRAY!!

    call define_initial_distribution(initial_distribution, ambient_properties)
    call initialize_particle_distribution(particle_properties, initial_distribution, ambient_properties, options)
    particle_properties%original_radiis = particle_properties%rdry
    call initial_order(particle_properties%order)

    if(options%quasistationary) then  !result back to fix structure
      fixed_radii = particle_properties%rdry
      fixed_volumes = (3/4.0)*pi*(particle_properties%rdry)**3
    end if

  end subroutine set_aerosol_structures


  !**********************************************************************************************************************************************
  !
  ! Subprograms to keep all bins in ascending size order in the moving sections case
  !
  !**********************************************************************************************************************************************

  subroutine initial_order(order)

    integer, dimension(:), intent(inout) :: order

    integer :: i

    forall(i=1:size(order,1))
      order(i) = i
    end forall

  end subroutine initial_order


  ! Not in use Michael 05/2015

  subroutine sort_by_radius(particles)
    type(uhma_particles), intent(inout) :: particles
    !type(uhma_ambient), intent(inout) :: ambient
    !integer, dimension(uhma_sections), intent(inout) :: order

    integer, dimension(uhma_sections) :: swaps

    !The reordering is slow, so check first and then sort if necessary
    !In most cases order is preserved

    if(check_order(particles%rdry)) then
      return
    end if


    call insertion_sort(particles%rdry, swaps)

    call reorder(swaps,particles%n_conc)

    call reorder(swaps,particles%radius)

    call reorder(swaps,particles%rdry_orig)

    call reorder(swaps,particles%core)

    call reorder(swaps, particles%mass)

    call reorder(swaps, particles%vol_conc)

    call reorder(swaps, particles%gr)

    particles%order = particles%order(swaps)

    return
  end subroutine sort_by_radius


  !**********************************************************************************************************************************************
  !
  ! Subprogram to zero the initial distribution
  !
  !**********************************************************************************************************************************************

  subroutine zero_distribution(particle_properties)

    type(uhma_particles), intent(inout) :: particle_properties

    !set volume and number concentration to zero
    particle_properties%vol_conc = 0
    particle_properties%n_conc = 0
    particle_properties%mass = 0

    return

  end subroutine zero_distribution


  !**********************************************************************************************************************************************
  !
  ! Subprogram to replace initial distribution one read from a file
  !
  !**********************************************************************************************************************************************

  ! Not in use Michael 05/2015

  subroutine set_distribution(ambient_properties, particle_properties, radiis, numbers, volumes, chemical_number, starting_index, ending_index)

    type(uhma_ambient), intent(in) :: ambient_properties
    type(uhma_particles), intent(inout) :: particle_properties
    real(uhma_real_x), dimension(:), intent(inout) :: radiis, numbers, volumes
    integer, intent(in) :: starting_index, ending_index, chemical_number

    logical :: matches
    real(uhma_real_x) :: accuracy

    accuracy = epsilon(1.0D0)

    !check that volumes match, within certain accuracy
    matches = ALL(abs(volumes/(4.0/3*pi*radiis**3) - numbers) > accuracy)
    if(.NOT. matches) then
      write (*,*), 'Input distribution is not self-consistent'
      write (*,*), 'Modifying it to be so'
    end if
    !where(abs(volumes/((4.0/3)*pi*(radiis**3.0)) - numbers) > accuracy)
    !   volumes = numbers*(4.0/3)*pi*(radiis**3.0)
    !end where

    volumes = (4.0/3)*pi*(radiis**3.0)*numbers

    volumes = volumes *(1E6**3.0) !m^3/m^3 -> um^3/m^3


    !Set the starting_index:ending_index to the given distribution
    particle_properties%vol_conc(starting_index:ending_index, chemical_number) = volumes
    particle_properties%n_conc(starting_index:ending_index) = numbers
    particle_properties%rdry(starting_index:ending_index) = radiis
    particle_properties%core(starting_index:ending_index) = (4.0/3)*pi*(radiis**3)
    particle_properties%mass(starting_index:ending_index) =  4.*pi/3.*particle_properties%rdry(starting_index:ending_index)**(3.)*1400 ! (kg)
    particle_properties%radius = particle_properties%rdry

    !DEBUG
    IF(.NOT.(check_total_volume(particle_properties) .AND. check_core_volume(particle_properties))) THEN
      write (*,*), 'VOLUME STILL NOT CONSISTENT'
      write (*,*), (4.0/3)*pi*(radiis**3.0)*numbers - volumes
    END IF
    !END DEBUG

  end subroutine set_distribution


  !**********************************************************************************************************************************************
  !
  ! consistency checks
  !
  !**********************************************************************************************************************************************

  ! Not in use Michael 05/2015 - called in subroutine set_distribution

  pure function check_total_volume(particle_properties) result(match)

    logical :: match
    !type(uhma_ambient), intent(in) :: ambient_properties
    type(uhma_particles), intent(in) :: particle_properties

    match = ALL(abs(((4/3.0)*pi*((particle_properties%rdry)**3.0)*particle_properties%n_conc*(1E6**3.0))-sum(particle_properties%vol_conc,2)) < volume_epsilon)

  end function check_total_volume

  pure function check_core_volume(particle_properties) result(match)

    logical :: match
    type(uhma_particles), intent(in) :: particle_properties

    match = ALL(abs(particle_properties%core*particle_properties%n_conc*(1E6**3)-sum(particle_properties%vol_conc,2)) < volume_epsilon)

  end function check_core_volume

  pure function check_volume_in_particles(particle_properties) result(match)

    logical :: match
    type(uhma_particles), intent(in) :: particle_properties

    match = ALL(((abs(particle_properties%core*particle_properties%n_conc*(1E6**3)-sum(particle_properties%vol_conc,2)))/(particle_properties%core*(1E6**3))) < particle_eps)

  end function check_volume_in_particles



  !**********************************************************************************************************************************************

end module uhma_interface
