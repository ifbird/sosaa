!Contains the nucleation routines
module nucleate

  use uhma_datatypes
  use acdc_nh3
  implicit none

  private
  integer, parameter :: sections=uhma_sections,&
    cond=uhma_cond,&
    compo=uhma_compo,&
    real_x=uhma_real_x

  public :: nucleation
contains

  !--------------------------------------------------------------------------------
  !
  !  Evaluates the nucleation rate, and the size and composition of nucleated
  ! particles
  !  Note: the nucleated particles are allocated in the smallest bin
  !
  !  Only the sulphuric acid portion of the newly formed particle is put into
  ! the core volumes; water and ammonia equilibrated in equilibrium_size()
  !
  ! nuc_coeff   = nucleation coefficient (used in activation/kinetic nucleation)
  ! n_nuc   = particle concentration change due to condensation
  !         in each section (#/m3 s)
  ! vol_nuc   = volume concentration change for each component
  !         in each section (um3/m3 s)
  ! nuc_rate  = nucleation rate (#/m3 s)
  ! n_sa    = number of sulphuric acid molecules in a critical cluster
  !
  !--------------------------------------------------------------------------------

  subroutine nucleation(vap_conc, n_conc, vol_conc, core, rdry, radius, order, particles, ambient, options, dvapor, dnumber, dvolume,nuc_rate,IPR,Nuc_by_charge)
    real(uhma_real_x), intent(in)  :: IPR
    real(uhma_real_x), intent(out) :: Nuc_by_charge(3)
    real(uhma_real_x), dimension(sections), intent(in) ::  n_conc, core, rdry, radius ! current values
    real(uhma_real_x), dimension(sections,compo), intent(in) :: vol_conc
    real(uhma_real_x), dimension(compo),intent(in) :: vap_conc
    integer, dimension(:), intent(in) :: order

    !Values assumed to be constant in the short term (may change outside the solver)
    type(uhma_particles),intent(in) :: particles
    type(uhma_ambient),intent(in) :: ambient
    type(uhma_options),intent(in) :: options

    real(uhma_real_x), dimension(compo), intent(inout) :: dvapor
    real(uhma_real_x), dimension(sections), intent(inout) :: dnumber !change rates
    real(uhma_real_x), dimension(sections,compo), intent(inout) :: dvolume

    real(uhma_real_x) :: nuc_rate, r_crit, n_h2o, n_nh3, cluster_vol
    real(uhma_real_x) :: diameter_acdc
    real(real_x),dimension(cond) :: n_crit

    n_crit = 0
    nuc_rate = 0
    select case(options%nuc_approach)

    case('def')  ! predefined (loaded in) formation rate
      nuc_rate=particles%nuc_rate
      n_crit(ambient%sulfuric_acid_index)=1. !1 sulphuric acid molecule

    case('ffn')  ! use a 'freeform' nucleation scheme
      call freeform_nucleation(nuc_rate,n_crit,ambient%nuc_coeff,vap_conc,options%nuc_array)

    case('bar')  ! barrierless kinetic nucleation
      if (vap_conc(ambient%sulfuric_acid_index) > ambient%nh3_mix*2.4d13) &
        stop ' [NH3] < [H2SO4]: kinetic nucleation assumptions not valid'
      call barless_nucleation(vap_conc,n_crit,ambient%temp,nuc_rate)

    case('acd')   !ACDC nucleation

      call acdc_nucleation(nuc_rate,n_crit,vap_conc,ambient,diameter_acdc,IPR,Nuc_by_charge)
    case default  ! homogeneous nucleation
      if (vap_conc(ambient%sulfuric_acid_index) < 1.d10) then        ! validity of parameterizations
        nuc_rate = 0.
        n_crit = 0.
        return
      elseif (ambient%nh3_mix < 0.1) then        ! if no ammonia present: binary scheme
        call bin_nucleation(vap_conc(ambient%sulfuric_acid_index),n_h2o,r_crit,ambient%temp,ambient%rh,n_crit(ambient%sulfuric_acid_index),nuc_rate)
        n_nh3 = 0.
      else                                 ! if ammonia present: ternary scheme
        call ter_nucleation(vap_conc(ambient%sulfuric_acid_index),ambient%nh3_mix,n_h2o,n_nh3,r_crit,ambient%temp,ambient%rh,n_crit(ambient%sulfuric_acid_index),nuc_rate)
      endif
      cluster_vol=sum(n_crit*ambient%molecvol)     ! if no other constituents

    end select
    if(options%nuc_approach=='acd') then
      cluster_vol=diameter_acdc*diameter_acdc*diameter_acdc/6.0*3.1416
    else
      cluster_vol=sum(n_crit*ambient%molecvol)
    end if
    if(.not. options%cluster_divide) then
      if(options%nuc_approach == 'acd') then
        dvapor = dvapor  -n_crit*nuc_rate
        dnumber(order(2))= dnumber(order(2)) + nuc_rate
        dvolume(order(2),:)= dvolume(order(2),:) + nuc_rate*cluster_vol*n_crit/sum(n_crit)
      else
        dvapor = dvapor  -n_crit*nuc_rate
        dnumber(order(1))= dnumber(order(1)) + nuc_rate
        dvolume(order(1),:)= dvolume(order(1),:) + nuc_rate*cluster_vol*n_crit/sum(n_crit)
      end if
    else
      stop 'Cluster division is not currently implemented'
      !Take the implementation below and fix the variable names.
    end if

    dvolume = dvolume*1.d6**3        ! (um3/m3)
    ! write(*,*) 'Debug1=',options%nuc_approach
    ! write(*,*) 'Debug2=',options%nuc_array
    ! write(*,*) 'Debug3=',dnumber
    ! stop
  end subroutine nucleation


  subroutine acdc_nucleation(nuc_rate,n_crit,vap_conc,ambient,diameter_acdc, IPR, Nuc_by_charge)
    !=======================================================================================
    ! this subroutine is to call the get_acdc_J in ACDC, any question with ACDC, please contact
    ! Tinja.Olenius@aces.su.se and michael.boy@helsinki.fi.
    !=======================================================================================
    real(real_x) :: c_acid,c_base,c_org     ! vapor concentrations (1/m^3)
    REAL(REAL_X), intent(in) :: IPR
    REAL(REAL_X), intent(out) :: Nuc_by_charge(3)
    real(real_x),dimension(:),intent(in) :: vap_conc           ! vapor concentrations (1/m^3)
    type(uhma_ambient),intent(in) :: ambient
    logical  :: solve_ss            ! solve the steady state or run only for the given time
    real(real_x) :: CS_H2SO4        ! reference coagulation sink (1/s)
    real(real_x)  :: temp       ! temperature (K)
    ! real(real_x)  :: q_ion        ! ion source rate (1/s/m^3)
    real(real_x), parameter  :: dt = 10.0 ! simulation time (s)
    ! Output: formed particles/time and their acid content
    real(real_x),intent(out) :: nuc_rate        ! simulated formation rate (1/s/m^3)
    real(real_x),intent(out) :: diameter_acdc   ! mass diameter of the formed particles (m)
    real(real_x),dimension(cond),intent(out) :: n_crit

    ! assign the data from ambient to acdc
    temp = ambient%temp
    c_acid = vap_conc(ambient%sulfuric_acid_index)
    c_base = ambient%nh3
    !        c_org = ambient%ELVOC_Nucl
    solve_ss = .true.   !  .true.  if steady state is used
    CS_H2SO4 = ambient%sink(ambient%sulfuric_acid_index)
    ! interface of ACDC

    if (c_acid>1E9)then
      CALL get_acdc_J(c_acid,c_base,c_org,CS_H2SO4,temp,IPR,dt,solve_ss,nuc_rate,diameter_acdc, Nuc_by_charge) ! IPR is needed if ionization is considered (P.C.)
    else
      nuc_rate=0.
    end if

    !print*,'1',c_acid,'2',c_base,'3',c_org,'4',CS_H2SO4,'5',temp,'6',IPR,'dt','7',solve_ss,'8',nuc_rate,'9',diameter_acdc,'10',Nuc_by_charge
    ! now we consider no nh3 in particles
    n_crit(ambient%sulfuric_acid_index) = 6


    ! TO DO HOMs in small clusters


  end subroutine acdc_nucleation

  !subroutine nucleation(n_nuc,vol_nuc,nuc_rate,n_crit,particles,ambient,options)
  !!$
  !!$implicit none
  !!$type(uhma_particles),intent(in) :: particles
  !!$type(uhma_ambient),intent(in) :: ambient
  !!$type(uhma_options),intent(in) :: options
  !!$real(real_x),dimension(sections),intent(inout) :: n_nuc   ! number concentration net change rate (#/m3 s)
  !!$real(real_x),dimension(sections,compo),intent(inout) :: vol_nuc    ! volume concentration net change rate (um3/m3 s)
  !!$real(real_x),intent(inout) :: nuc_rate
  !!$real(real_x),dimension(cond),intent(out) :: n_crit
  !!$
  !!$integer :: ii
  !!$real(real_x) :: r_crit, n_h2o, n_nh3, core_nuc, aa,nuc_coeff,nh3_mix,temp,rh,nuc_number,cluster_vol
  !!$real(real_x),dimension(cond) :: vap_conc,molecvol
  !!$real(real_x),dimension(sections) :: core
  !!$logical :: cluster_divide
  !!$
  !!$nuc_coeff=ambient%nuc_coeff
  !!$vap_conc=ambient%vap_conc
  !!$molecvol=ambient%molecvol
  !!$nh3_mix=ambient%nh3_mix
  !!$temp=ambient%temp
  !!$rh=ambient%rh
  !!$nuc_number=options%nuc_number
  !!$core=particles%core
  !!$
  !!$cluster_vol=particles%cluster_vol
  !!$cluster_divide=options%cluster_divide
  !!$
  !!$n_crit=0.
  !!$!nuc_rate=0.
  !!$
  !!$
  !!$select case(options%nuc_approach)
  !!$
  !!$    case('def')  ! predefined (loaded in) formation rate
  !!$    nuc_rate=particles%nuc_rate
  !!$    n_crit(ambient%sulfuric_acid_index)=1. !1 sulphuric acid molecule
  !!$
  !!$    case('ffn')  ! use a 'freeform' nucleation scheme
  !!$!    call freeform_nucleation(nuc_rate,n_crit,nuc_coeff,vap_conc,nuc_number)
  !!$     call freeform_nucleation(nuc_rate,n_crit,nuc_coeff,vap_conc,options%nuc_array)
  !!$
  !!$    case('bar')  ! barrierless kinetic nucleation
  !!$    if (vap_conc(ambient%sulfuric_acid_index) > nh3_mix*2.4d13) &
  !!$    stop ' [NH3] < [H2SO4]: kinetic nucleation assumptions not valid'
  !!$    call barless_nucleation(vap_conc,n_crit,temp,nuc_rate)
  !!$
  !!$    case default  ! homogeneous nucleation
  !!$    if (vap_conc(ambient%sulfuric_acid_index) < 1.d10) then        ! validity of parameterizations
  !!$        nuc_rate = 0.
  !!$        n_crit = 0.
  !!$        return
  !!$    elseif (nh3_mix < 0.1) then        ! if no ammonia present: binary scheme
  !!$        call bin_nucleation(vap_conc(ambient%sulfuric_acid_index),n_h2o,r_crit,temp,rh,n_crit(ambient%sulfuric_acid_index),nuc_rate)
  !!$        n_nh3 = 0.
  !!$    else                                 ! if ammonia present: ternary scheme
  !!$        call ter_nucleation(vap_conc(ambient%sulfuric_acid_index),nh3_mix,n_h2o,n_nh3,r_crit,temp,rh,n_crit(ambient%sulfuric_acid_index),nuc_rate)
  !!$    endif
  !!$    cluster_vol=sum(n_crit*molecvol)     ! if no other constituents
  !!$
  !!$end select
  !!$
  !!$if (nuc_rate>0.) then ! Calculate the rate of effect on particle distribution
  !!$    if (cluster_divide) then !
  !!$        ! find the bins to which the new particle is divided
  !!$        if (cluster_vol <= core(particles%order(1))) then
  !!$            n_nuc(particles%order(1)) = nuc_rate*cluster_vol/core(particles%order(1))     ! here we conserve number, volume overestimated
  !!$            vol_nuc(particles%order(1),:) = cluster_vol*nuc_rate*n_crit/sum(n_crit)
  !!$        elseif (cluster_vol >= core(particles%order(sections))) then
  !!$            n_nuc(particles%order(sections)) = nuc_rate*cluster_vol/core(particles%order(sections))   ! here we conserve number, volume underestimated
  !!$            vol_nuc(particles%order(sections),:) = cluster_vol*nuc_rate*n_crit/sum(n_crit)
  !!$        else
  !!$            ii = 2
  !!$            do while (cluster_vol > core(particles%order(ii)))
  !!$                ii=ii+1
  !!$            enddo
  !!$            aa = (core(particles%order(ii))-cluster_vol)/(core(particles%order(ii))-core(particles%order(ii-1)))
  !!$            n_nuc(particles%order(ii-1)) = aa*nuc_rate
  !!$            n_nuc(particles%order(ii)) = (1.-aa)*nuc_rate
  !!$            vol_nuc(particles%order(ii-1),:) = aa*core(particles%order(ii-1))*nuc_rate*n_crit/sum(n_crit)
  !!$            vol_nuc(particles%order(ii),:) = (1.-aa)*core(particles%order(ii))*nuc_rate*n_crit/sum(n_crit)
  !!$        endif
  !!$
  !!$    else  ! New particle formation in the lowest bin only
  !!$        n_nuc(particles%order(1))=nuc_rate
  !!$        vol_nuc(particles%order(1),:)=nuc_rate*core(particles%order(1))*n_crit/sum(n_crit)
  !!$    endif
  !!$    vol_nuc = vol_nuc*1.d6**3        ! (um3/m3)
  !!$endif
  !!$
  !!$end subroutine nucleation

  !oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
  ! 'Freeform' nucleation (1st or 2nd order)
  ! Usage: set the variable options%nuc_number to give the nucleating compounds
  ! Examples: nuc_number=1 : sulphuric acid induced activation
  !           nuc_number=13: kinetic nucleation of sulphuric acid and compound #3
  ! Options can be passed with the decimals:
  !           0.5: add vapors, e.g. 13.5: A*(vap(1)+vap(3)) and 0.5 for both in n_crit
  !
  !Note this sytem has been replaced with another one,
  !nuc_array has three components , index of first compound, index of second compound(0 for none),
  ! and special case 1 for on and 0 for off.
  !For example [1,2,0] means kinetic between compounds 1 and 2. [1,0,0] activation of compound 1. etc..
  !oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
  !subroutine freeform_nucleation(nuc_rate,n_crit,nuc_coeff,vap_conc,nuc_number)
  !    real(real_x),intent(out) :: nuc_rate
  !    real(real_x),intent(in) :: nuc_coeff,nuc_number
  !    real(real_x),dimension(:),intent(in) :: vap_conc
  !    real(real_x),dimension(cond),intent(out) :: n_crit
  !    integer :: ones,tens,tenths
  !    real :: num2
  !
  !    !What happens here?
  !
  !    num2=nuc_number
  !    tenths=mod(num2,1.) ! options
  !    num2=num2-tenths
  !    ones=mod(num2,10.)  ! index of 1st nucleating compound
  !    num2=num2-ones
  !    tens=int(num2/10.) ! index of 2nd nucleating compound
  !
  !    if (tens==0) then !1st order nucleation (activation)
  !        nuc_rate=nuc_coeff*vap_conc(ones)
  !        n_crit(ones)=1
  !    elseif (tenths==0) then !2nd order nucleation (kinetic)
  !        nuc_rate=nuc_coeff*vap_conc(ones)*vap_conc(tens)
  !        n_crit(ones)=1
  !        n_crit(tens)=n_crit(tens)+1 !add in case ones=tens
  !    elseif (tenths==0.5) then !special case
  !        nuc_rate=nuc_coeff*(vap_conc(ones)+vap_conc(tens))
  !        n_crit(ones)=0.5
  !        n_crit(tens)=0.5
  !    endif
  !
  !end subroutine freeform_nucleation

  subroutine freeform_nucleation(nuc_rate,n_crit,nuc_coeff,vap_conc,nuc_array)
    real(real_x),intent(out) :: nuc_rate
    real(real_x),intent(in) :: nuc_coeff
    integer, dimension(3), intent(in) :: nuc_array
    real(real_x),dimension(:),intent(in) :: vap_conc
    real(real_x),dimension(cond),intent(out) :: n_crit


    if (nuc_array(2) == 0) then !1st order nucleation (activation)
      nuc_rate=nuc_coeff*vap_conc(nuc_array(1))
      n_crit(nuc_array(1))=1
    elseif (nuc_Array(3) == 0) then !2nd order nucleation (kinetic)
      nuc_rate=nuc_coeff*vap_conc(nuc_array(1))*vap_conc(nuc_array(2))
      n_crit(nuc_array(1))=1
      n_crit(nuc_array(2))=n_crit(nuc_array(2))+1 !add in case ones=tens
    elseif (nuc_array(3) == 1 .and. nuc_array(2) /= 0) then !special case
      nuc_rate=nuc_coeff*(vap_conc(nuc_array(1))+vap_conc(nuc_array(2)))
      n_crit(nuc_array(1))=0.5
      n_crit(nuc_array(2))=0.5
    else
      Write(*,*) 'Unindentified nucleation freeform option: ', nuc_array
      stop 'cannot nucleate'
    endif

  end subroutine freeform_nucleation



  !oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
  ! Subroutine BARLESS_NUCLEATION evaluates the barrierless kinetic nucleation rate
  ! for ammonium bisulphate molecules (NH4HSO4 + NH4HSO4 -> 2 NH4HSO4).
  !
  !        nuc_rate   =   nucleation rate (#/m3 s)
  !        n_sa       =   number of sulphuric acid molecules in a critical cluster
  !
  !oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

  subroutine barless_nucleation(vap_conc,n_crit,temp,nuc_rate)

    implicit none
    real(real_x),dimension(:),intent(in) :: vap_conc
    real(real_x),dimension(cond),intent(out) :: n_crit
    real(real_x),intent(in) :: temp
    real(real_x),intent(out) :: nuc_rate
    real(real_x) :: r_init, dens_init, coll_coeff,boltzmann

    boltzmann = 1.3807d-23      ! Boltzmann constant (J/K)
    r_init = 0.315d-9           ! radius (m) of the initial NH4HSO4 molecule
    dens_init = 1465.           ! density (kg/m3) of the initial NH4HSO4 molecule

    ! collision frequency from kinetic theory
    coll_coeff = 8.*sqrt(3.*boltzmann*temp*r_init/dens_init)

    nuc_rate = 0.5*coll_coeff*(vap_conc(1))**2        ! [#/m3 s]
    n_crit(1) = 2.0

  end subroutine barless_nucleation


  !--------------------------------------------------------------------------------
  !
  !  Calculates ternary nucleation rate and properties of critical clusters
  ! using revised theory, Noppel hydrate correction
  !
  ! (see Napari et al., An improved model for ternary nucleation of
  !     sulfuric acid - ammoni- water, J. Chem. Phys., 116, 4221-4227 (2002);
  !      Napari et al., Parameterization of ternary nucleation rates for
  !     H2SO4 - NH3 - H2O vapors, J. Geophys. Res., 107(D19), AAC 6-1 (2002))
  ! sa_conc   = sulphuric acid concentration  (m^-3) (10^10-10^15 m^-3)
  ! c_nh3   = ammonia mixing ratio (ppt)  (0.1-100 ppt)
  ! nuc_rate  = nucleation rate (m^-3 s^-1)
  ! n_h2o   = number of water molecules in a critical cluster
  ! n_sa    =   - " - sulphuric acid    - " -
  ! n_nh3   =   - " - ammonia       - " -
  ! r_crit    = radius of the critical cluster (m)
  !
  ! (c) Ismo Napari, 2002
  !
  !   Division of Atmospheric Sciences
  !   Department of Physical Sciences
  !   P.O. Box 64
  !   FIN-00014 University of Helsinki
  !   Finland
  !
  !   (ismo.napari@helsinki.fi)
  !
  !--------------------------------------------------------------------------------

  subroutine ter_nucleation(sa_conc,c_nh3,n_h2o,n_nh3,r_crit,temp,rh,n_sa,nuc_rate)

    implicit none

    real(real_x), intent(in) :: sa_conc, c_nh3,temp,rh
    real(real_x), intent(out) :: n_h2o, n_nh3, n_sa, r_crit,nuc_rate

    real(real_x) :: c_sa, lnj, ntot

    c_sa = sa_conc*1.d-6        ! (cm^-3)

    ! validity of parameterization : DO NOT REMOVE!
    if(temp < 240.) stop '  INVALID INPUT VALUE (ter. nucleation): temperature < 240 K'
    if(temp > 300.) stop '  INVALID INPUT VALUE (ter. nucleation) temperature > 300 K'
    if(rh < 0.05) stop '  INVALID INPUT VALUE (ter. nucleation) relative humidity < 5 %'
    if(rh > 0.95) stop '  INVALID INPUT VALUE (ter. nucleation) relative humidity > 95 %'
    if(c_sa < 1.e4) stop '  INVALID INPUT VALUE (ter. nucleation) H2SO4 concentration < 10^4 1/cm3'
    if(c_sa > 1.e9) stop '  INVALID INPUT VALUE (ter. nucleation) H2SO4 concentration > 10^9 1/cm3'
    if(c_nh3 < 0.1) stop '  INVALID INPUT VALUE (ter. nucleation) ammonia mixing ratio < 0.1 ppt'
    if(c_nh3 > 100.) stop '  INVALID INPUT VALUE (ter. nucleation) ammonia mixing ratio > 100 ppt'

    lnj = -84.7551114741543 + 0.3117595133628944*rh + 1.640089605712946*rh*temp - &
      0.003438516933381083*rh*temp**2 - 0.00001097530402419113*rh*temp**3 - &
      0.3552967070274677/Log(c_sa) - (0.06651397829765026*rh)/Log(c_sa) - &
      (33.84493989762471*temp)/Log(c_sa) - (7.823815852128623*rh*temp)/Log(c_sa) + &
      (0.3453602302090915*temp**2)/Log(c_sa) + &
      (0.01229375748100015*rh*temp**2)/Log(c_sa) - &
      (0.000824007160514956*temp**3)/Log(c_sa) + &
      (0.00006185539100670249*rh*temp**3)/Log(c_sa) +&
      3.137345238574998*Log(c_sa) + 3.680240980277051*rh*Log(c_sa) - &
      0.7728606202085936*temp*Log(c_sa) - 0.204098217156962*rh*temp*Log(c_sa) + &
      0.005612037586790018*temp**2*Log(c_sa) + &
      0.001062588391907444*rh*temp**2*Log(c_sa) - &
      9.74575691760229e-6*temp**3*Log(c_sa) - &
      1.265595265137352e-6*rh*temp**3*Log(c_sa) + 19.03593713032114*Log(c_sa)**2 - &
      0.1709570721236754*temp*Log(c_sa)**2 + &
      0.000479808018162089*temp**2*Log(c_sa)**2 - &
      4.146989369117246e-7*temp**3*Log(c_sa)**2 + 1.076046750412183*Log(c_nh3) + &
      0.6587399318567337*rh*Log(c_nh3) + 1.48932164750748*temp*Log(c_nh3) +&
      0.1905424394695381*rh*temp*Log(c_nh3) - 0.007960522921316015*temp**2*Log(c_nh3) - &
      0.001657184248661241*rh*temp**2*Log(c_nh3)
    lnj=lnj+7.612287245047392e-6*temp**3*Log(c_nh3) + &
      3.417436525881869e-6*rh*temp**3*Log(c_nh3) + &
      (0.1655358260404061*Log(c_nh3))/Log(c_sa) + &
      (0.05301667612522116*rh*Log(c_nh3))/Log(c_sa) + &
      (3.26622914116752*temp*Log(c_nh3))/Log(c_sa) - &
      (1.988145079742164*rh*temp*Log(c_nh3))/Log(c_sa) - &
      (0.04897027401984064*temp**2*Log(c_nh3))/Log(c_sa) + &
      (0.01578269253599732*rh*temp**2*Log(c_nh3))/Log(c_sa) + &
      (0.0001469672236351303*temp**3*Log(c_nh3))/Log(c_sa) - &
      (0.00002935642836387197*rh*temp**3*Log(c_nh3))/Log(c_sa) + &
      6.526451177887659*Log(c_sa)*Log(c_nh3) - &
      0.2580021816722099*temp*Log(c_sa)*Log(c_nh3) + &
      0.001434563104474292*temp**2*Log(c_sa)*Log(c_nh3) - &
      2.020361939304473e-6*temp**3*Log(c_sa)*Log(c_nh3) - &
      0.160335824596627*Log(c_sa)**2*Log(c_nh3) + &
      0.00889880721460806*temp*Log(c_sa)**2*Log(c_nh3) - &
      0.00005395139051155007*temp**2*Log(c_sa)**2*Log(c_nh3) + &
      8.39521718689596e-8*temp**3*Log(c_sa)**2*Log(c_nh3) + &
      6.091597586754857*Log(c_nh3)**2 + 8.5786763679309*rh*Log(c_nh3)**2 - &
      1.253783854872055*temp*Log(c_nh3)**2 - 0.1123577232346848*rh*temp*Log(c_nh3)**2 + &
      0.00939835595219825*temp**2*Log(c_nh3)**2
    lnj=lnj+0.0004726256283031513*rh*temp**2*Log(c_nh3)**2 - &
      0.00001749269360523252*temp**3*Log(c_nh3)**2 - &
      6.483647863710339e-7*rh*temp**3*Log(c_nh3)**2 + &
      (0.7284285726576598*Log(c_nh3)**2)/Log(c_sa) + &
      (3.647355600846383*temp*Log(c_nh3)**2)/Log(c_sa) - &
      (0.02742195276078021*temp**2*Log(c_nh3)**2)/Log(c_sa) + &
      (0.00004934777934047135*temp**3*Log(c_nh3)**2)/Log(c_sa) + &
      41.30162491567873*Log(c_sa)*Log(c_nh3)**2 - &
      0.357520416800604*temp*Log(c_sa)*Log(c_nh3)**2 + &
      0.000904383005178356*temp**2*Log(c_sa)*Log(c_nh3)**2 - &
      5.737876676408978e-7*temp**3*Log(c_sa)*Log(c_nh3)**2 - &
      2.327363918851818*Log(c_sa)**2*Log(c_nh3)**2 + &
      0.02346464261919324*temp*Log(c_sa)**2*Log(c_nh3)**2 - &
      0.000076518969516405*temp**2*Log(c_sa)**2*Log(c_nh3)**2 + &
      8.04589834836395e-8*temp**3*Log(c_sa)**2*Log(c_nh3)**2 - &
      0.02007379204248076*Log(rh) - 0.7521152446208771*temp*Log(rh) + &
      0.005258130151226247*temp**2*Log(rh) - &
      8.98037634284419e-6*temp**3*Log(rh) + &
      (0.05993213079516759*Log(rh))/Log(c_sa) + &
      (5.964746463184173*temp*Log(rh))/Log(c_sa) - &
      (0.03624322255690942*temp**2*Log(rh))/Log(c_sa) + &
      (0.00004933369382462509*temp**3*Log(rh))/Log(c_sa) - &
      0.7327310805365114*Log(c_nh3)*Log(rh) - &
      0.01841792282958795*temp*Log(c_nh3)*Log(rh) + &
      0.0001471855981005184*temp**2*Log(c_nh3)*Log(rh) - &
      2.377113195631848e-7*temp**3*Log(c_nh3)*Log(rh)

    nuc_rate = exp(lnj)        ! (cm^-3 s^-1)

    if(nuc_rate < 1.d-5) then        ! validity of parametrization
      nuc_rate = 0.
      n_h2o = 0.
      n_sa = 0.
      n_nh3 = 0.
      r_crit = 0.5d-9
      goto 100
    end if

    ! validity of parametrization
    if(nuc_rate > 1.e6) stop '  INVALID OUTPUT VALUE for ternary nucleation: nucleation rate > 10^6 1/cm3s'

    ntot = 79.34842673023872 + 1.738399264246183*lnj + &
      0.007114029786143758*lnj**2 - 0.7449934047779641*temp - &
      0.00820608264651173*lnj*temp + 0.001785497681201935*temp**2

    n_sa = 38.16448247950508 + 0.7741058259731187*lnj + &
      0.002988789927230632*lnj**2 - 0.3576046920535017*temp - &
      0.003663583011953248*lnj*temp + 0.000855300153372776*temp**2

    n_nh3 = 26.89821829677973 + 0.6829050502793024*lnj + &
      0.003575211441884629*lnj**2 - 0.2657482507363107*temp - &
      0.003418950836780133*lnj*temp + 0.000673454337882302*temp**2

    n_h2o = ntot-n_sa-n_nh3

    r_crit = 0.1410271086638381 - 0.001226253898894878*lnj - &
      7.822111731550752e-6*lnj**2 - 0.001567273351921166*temp - &
      0.00003075996088273962*lnj*temp + 0.00001083754117202233*temp**2

    nuc_rate = nuc_rate*1.d6        ! (m^-3 s^-1)
    r_crit = r_crit*1.d-9                ! (m)

    100        continue

  end subroutine ter_nucleation


  !-------------------------------------------------------------------------------
  !
  !  Calculates binary nucleation rate and properties of critical cluster
  ! using revised theory, Stauffer + Binder & Stauffer kinetics and
  ! Noppel hydrate correction
  !
  ! sa_conc   = sulphuric acid concentration (m^-3)
  ! nuc_rate  = nucleation rate (m^-3 s^-1)
  ! n_h2o   = number of water molecules in a critical cluster
  ! n_sa    = - " -  sulphuric acid   - " -
  ! r_crit    = radius of the critical cluster core (m)
  !
  ! (c) Hanna Vehkam�ki, 2002
  !
  !   Division of Atmospheric Sciences
  !   Department of Physical Sciences
  !   P.O. Box 64
  !   FIN-00014 University of Helsinki
  !   Finland
  !
  !   (hanna.vehkamaki@helsinki.fi)
  !
  !-------------------------------------------------------------------------------

  subroutine bin_nucleation(sa_conc,n_h2o,r_crit,temp,rh,n_sa,nuc_rate)

    implicit none

    real(real_x), intent(in) :: sa_conc,temp,rh
    real(real_x), intent(out) :: n_h2o, r_crit,nuc_rate,n_sa

    real(real_x) :: c_sa, x, ntot


    c_sa = sa_conc*1.d-6 !(cm^-3)

    ! validity of parametrization : DO NOT REMOVE!
    if(temp < 190.15) stop '  INVALID INPUT VALUE (bin. nucleation): temperature < 190.15 K'
    if(temp > 300.15) stop '  INVALID INPUT VALUE (bin. nucleation): temperature > 300.15 K'
    if(rh < 0.0001) stop '  INVALID INPUT VALUE (bin. nucleation): relative humidity < 0.01 %'
    if(rh > 1.) stop '  INVALID INPUT VALUE (bin. nucleation): relative humidity > 100 %'
    if(c_sa < 1.e4) stop '  INVALID INPUT VALUE (bin. nucleation): H2SO4 concentration < 10^4 1/cm3'
    if(c_sa > 1.e11) stop '  INVALID INPUT VALUE (bin. nucleation): H2SO4 concentration > 10^11 1/cm3'

    x = 0.7409967177282139 - 0.002663785665140117*temp + 0.002010478847383187*Log(rh) - &
      0.0001832894131464668*temp*Log(rh) + 0.001574072538464286*Log(rh)**2 -    &
      0.00001790589121766952*temp*Log(rh)**2 + 0.0001844027436573778*Log(rh)**3 - &
      1.503452308794887e-6*temp*Log(rh)**3 - 0.003499978417957668*Log(c_sa) + &
      0.0000504021689382576*temp*Log(c_sa)

    nuc_rate = 0.1430901615568665 + 2.219563673425199*temp - 0.02739106114964264*temp**2 +  &
      0.00007228107239317088*temp**3 + 5.91822263375044/x +  &
      0.1174886643003278*Log(rh) + 0.4625315047693772*temp*Log(rh) -  &
      0.01180591129059253*temp**2*Log(rh) +  &
      0.0000404196487152575*temp**3*Log(rh) + (15.79628615047088*Log(rh))/x -  &
      0.215553951893509*Log(rh)**2 - 0.0810269192332194*temp*Log(rh)**2 +  &
      0.001435808434184642*temp**2*Log(rh)**2 -4.775796947178588e-6*temp**3*Log(rh)**2 -  &
      (2.912974063702185*Log(rh)**2)/x - 3.588557942822751*Log(rh)**3 +  &
      0.04950795302831703*temp*Log(rh)**3 - 0.0002138195118737068*temp**2*Log(rh)**3 +  &
      3.108005107949533e-7*temp**3*Log(rh)**3 -  &
      (0.02933332747098296*Log(rh)**3)/x +  &
      1.145983818561277*Log(c_sa) - 0.6007956227856778*temp*Log(c_sa) +  &
      0.00864244733283759*temp**2*Log(c_sa) -  &
      0.00002289467254710888*temp**3*Log(c_sa) -  &
      (8.44984513869014*Log(c_sa))/x + 2.158548369286559*Log(rh)*Log(c_sa) +  &
      0.0808121412840917*temp*Log(rh)*Log(c_sa) -  &
      0.0004073815255395214*temp**2*Log(rh)*Log(c_sa) -  &
      4.019572560156515e-7*temp**3*Log(rh)*Log(c_sa) +  &
      (0.7213255852557236*Log(rh)*Log(c_sa))/x +  &
      1.62409850488771*Log(rh)**2*Log(c_sa) -  &
      0.01601062035325362*temp*Log(rh)**2*Log(c_sa) +  &
      0.00003771238979714162*temp**2*Log(rh)**2*Log(c_sa) +  &
      3.217942606371182e-8*temp**3*Log(rh)**2*Log(c_sa) -  &
      (0.01132550810022116*Log(rh)**2*Log(c_sa))/x +  &
      9.71681713056504*Log(c_sa)**2 - 0.1150478558347306*temp*Log(c_sa)**2 +  &
      0.0001570982486038294*temp**2*Log(c_sa)**2 +  &
      4.009144680125015e-7*temp**3*Log(c_sa)**2 +  &
      (0.7118597859976135*Log(c_sa)**2)/x - 1.056105824379897*Log(rh)*Log(c_sa)**2 +  &
      0.00903377584628419*temp*Log(rh)*Log(c_sa)**2 -  &
      0.00001984167387090606*temp**2*Log(rh)*Log(c_sa)**2 +  &
      2.460478196482179e-8*temp**3*Log(rh)*Log(c_sa)**2 -  &
      (0.05790872906645181*Log(rh)*Log(c_sa)**2)/x -  &
      0.1487119673397459*Log(c_sa)**3 + 0.002835082097822667*temp*Log(c_sa)**3 -  &
      9.24618825471694e-6*temp**2*Log(c_sa)**3 +  &
      5.004267665960894e-9*temp**3*Log(c_sa)**3 -  &
      (0.01270805101481648*Log(c_sa)**3)/x

    nuc_rate = exp(nuc_rate)        ! (cm^-3 s^-1)

    if(nuc_rate < 1.d-7) then        ! validity of parameterization
      nuc_rate = 0.
      n_h2o = 0.
      n_sa = 0.
      r_crit = 0.5d-9
      goto 100
    end if

    if (nuc_rate > 1.d10) stop '  INVALID OUTPUT VALUE (bin. nucleation): nucl. rate > 10^16 1/m^3s'

    ntot = -0.002954125078716302 - 0.0976834264241286*temp + &
      0.001024847927067835*temp**2 - 2.186459697726116e-6*temp**3 -  &
      0.1017165718716887/x - 0.002050640345231486*Log(rh) - &
      0.007585041382707174*temp*Log(rh) +  &
      0.0001926539658089536*temp**2*Log(rh) - 6.70429719683894e-7*temp**3*Log(rh) -  &
      (0.2557744774673163*Log(rh))/x + 0.003223076552477191*Log(rh)**2 + &
      0.000852636632240633*temp*Log(rh)**2 -  &
      0.00001547571354871789*temp**2*Log(rh)**2 + &
      5.666608424980593e-8*temp**3*Log(rh)**2 +  &
      (0.03384437400744206*Log(rh)**2)/x + 0.04743226764572505*Log(rh)**3 -  &
      0.0006251042204583412*temp*Log(rh)**3 + 2.650663328519478e-6*temp**2*Log(rh)**3 -  &
      3.674710848763778e-9*temp**3*Log(rh)**3 - (0.0002672510825259393*Log(rh)**3)/x -  &
      0.01252108546759328*Log(c_sa) + 0.005806550506277202*temp*Log(c_sa) -  &
      0.0001016735312443444*temp**2*Log(c_sa) + 2.881946187214505e-7*temp**3*Log(c_sa) + &
      (0.0942243379396279*Log(c_sa))/x - 0.0385459592773097*Log(rh)*Log(c_sa) -  &
      0.0006723156277391984*temp*Log(rh)*Log(c_sa) + &
      2.602884877659698e-6*temp**2*Log(rh)*Log(c_sa) +  &
      1.194163699688297e-8*temp**3*Log(rh)*Log(c_sa) - &
      (0.00851515345806281*Log(rh)*Log(c_sa))/x -  &
      0.01837488495738111*Log(rh)**2*Log(c_sa) + &
      0.0001720723574407498*temp*Log(rh)**2*Log(c_sa) -  &
      3.717657974086814e-7*temp**2*Log(rh)**2*Log(c_sa) -  &
      5.148746022615196e-10*temp**3*Log(rh)**2*Log(c_sa) +  &
      (0.0002686602132926594*Log(rh)**2*Log(c_sa))/x - 0.06199739728812199*Log(c_sa)**2 +  &
      0.000906958053583576*temp*Log(c_sa)**2 - 9.11727926129757e-7*temp**2*Log(c_sa)**2 -  &
      5.367963396508457e-9*temp**3*Log(c_sa)**2 - (0.007742343393937707*Log(c_sa)**2)/x +  &
      0.0121827103101659*Log(rh)*Log(c_sa)**2 - &
      0.0001066499571188091*temp*Log(rh)*Log(c_sa)**2 +  &
      2.534598655067518e-7*temp**2*Log(rh)*Log(c_sa)**2 -  &
      3.635186504599571e-10*temp**3*Log(rh)*Log(c_sa)**2 +  &
      (0.0006100650851863252*Log(rh)*Log(c_sa)**2)/x + &
      0.0003201836700403512*Log(c_sa)**3 -  &
      0.0000174761713262546*temp*Log(c_sa)**3 + &
      6.065037668052182e-8*temp**2*Log(c_sa)**3 -  &
      1.421771723004557e-11*temp**3*Log(c_sa)**3 + &
      (0.0001357509859501723*Log(c_sa)**3)/x

    ntot=exp(ntot)
    if (ntot < 4.) stop '  INVALID OUTPUT VALUE (bin. nucleation): ntot < 4'

    n_sa = x*ntot
    n_h2o = (1.-x)*ntot

    r_crit = exp(-1.6524245+0.42316402*x+0.33466487*log(ntot)) ! (nm)
    r_crit = r_crit*1.e-9                ! (m)

    nuc_rate = nuc_rate*1.d6        ! (m^-3 s^-1)

    return

    100 continue

  end subroutine bin_nucleation

end module nucleate
