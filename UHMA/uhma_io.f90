!**********************************************************************************************************************************************
!
! This module contains routines for handling input and output
!
!**********************************************************************************************************************************************
module uhma_io

use uhma_datatypes
implicit none

integer, parameter,private :: sections=uhma_sections,&
                              cond=uhma_cond,&
                              compo=uhma_compo,&
                              modes=uhma_init_modes,&
                              real_x=uhma_real_x

contains

!**********************************************************************************************************************************************
!
! Initialize variables
!
!**********************************************************************************************************************************************

! Not in use Michael 05/2015

subroutine uhma_init_variables(particles,ambient)
implicit none
type(uhma_particles),intent(out) :: particles
type(uhma_ambient),intent(out) :: ambient
!type(uhma_options),intent(out) :: options

particles%n_conc=0. 
particles%vol_conc=0.
particles%radius=0.
particles%gr=0.
particles%nuc_rate=0.
ambient%sink=0.
ambient%n_crit=0.

end subroutine uhma_init_variables


!**********************************************************************************************************************************************
!
! Open files needed for output, files saved to specified directory.
!
!**********************************************************************************************************************************************

! Not in use Michael 05/2015

subroutine uhma_open_output_files(run,dir)
implicit none
integer,intent(in) :: run
character (len=32) :: dir
character (len=4) :: runst

write(runst,'(I4)') run ! int -> char

!call system('mkdir '//trim(adjustl(dir))//' 2>/dev/null') ! create the directory (UNIX)
open(21,file=trim(adjustl(dir))//'/gases'//trim(adjustl(runst))//'.res',status='unknown')
open(22,file=trim(adjustl(dir))//'/volume'//trim(adjustl(runst))//'.res',status='unknown')
open(23,file=trim(adjustl(dir))//'/radii'//trim(adjustl(runst))//'.res',status='unknown')
open(24,file=trim(adjustl(dir))//'/number'//trim(adjustl(runst))//'.res',status='unknown')
open(25,file=trim(adjustl(dir))//'/gr'//trim(adjustl(runst))//'.res',status='unknown')
open(26,file=trim(adjustl(dir))//'/dry_radii'//trim(adjustl(runst))//'.res',status='unknown')
open(unit =888,file=trim(adjustl(dir))//'/mass'//trim(adjustl(runst))//'.res',status='unknown')

end subroutine uhma_open_output_files


!**********************************************************************************************************************************************
!
! Write data into output files
!
!**********************************************************************************************************************************************

! Not in use Michael 05/2015

subroutine uhma_write_output(time,particles,ambient)
implicit none
integer :: jj
real(real_x),intent(in) :: time
type(uhma_particles),intent(in) :: particles
type(uhma_ambient),intent(in) :: ambient

write(21,'(300e14.6)') time, ambient%vap_conc, ambient%sink, particles%nuc_rate
do jj = 1,compo
    write(22,'(300e14.6)') particles%vol_conc(:,jj)
end do
write(23,'(300e14.6)') particles%radius
write(24,'(300e14.6)') particles%n_conc
write(25,'(300e14.6)') particles%gr
write(26,'(300e14.6)') particles%rdry
write(888, *) particles%mass

end subroutine uhma_write_output


!**********************************************************************************************************************************************
!
! Close output files
!
!**********************************************************************************************************************************************

! Not in use Michael 05/2015

subroutine uhma_close_output_files()
implicit none
close(21)!, status='save')
close(22)!, status='save')
close(23)!, status='save')
close(24)!, status='save')
close(25)!, status='save')
close(26)!, status='save')
close(27)!, status='save')
close(28)!, status='save')
close(29)!, status='save')

close(888)

end subroutine uhma_close_output_files


!oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
!
!	Read parameters needed to initialize the particle size distribution and others
!
!**********************************************************************************************************************************************

! Not in use Michael 05/2015

subroutine uhma_read_input_files(r_min,r_max,n_mode,sigma_mode,r_mode,mfractions,particles,ambient,options)
implicit none
real(real_x),intent(out) :: r_min, r_max
real(real_x), dimension(:),intent(out) :: n_mode, sigma_mode, r_mode
real(real_x),dimension(:),intent(out) :: mfractions
type(uhma_particles),intent(inout) :: particles
type(uhma_ambient),intent(inout) :: ambient
type(uhma_options),intent(inout) :: options

integer :: ii,jj

!---------------------------------------------------------------------------------------
! The file uhma_distr.dat contains parameters to initialize particle size distribution

open(unit=10,file="uhma_distr.dat",status="old")
read(10,*) r_min, r_max
do ii = 1, modes
    read(10,*) r_mode(ii), n_mode(ii), sigma_mode(ii),(mfractions(jj),jj=1,compo)
end do
close(10)

!---------------------------------------------------------------------------------------
! The file uhma_ambient.dat contains ambient parameters

open(unit=11,file="uhma_ambient.dat",status="old")
read(11,*) ambient%temp, ambient%rh, ambient%pres
read(11,*) ambient%vap_conc
read(11,*) ambient%nh3_mix
read(11,*) options%latitude, options%longitude, options%day_of_year
close(11)
!---------------------------------------------------------------------------------------
! The file uhma_options.dat contains miscellanious model options

open(unit=12,file="uhma_options.dat",status="old")
read(12,*) options%latitude, options%longitude, options%day_of_year
close(12)

end subroutine uhma_read_input_files


!**********************************************************************************************************************************************
!
! Read in measurement data
! File format: [time of day, data]
!
!**********************************************************************************************************************************************

! Not in use Michael 05/2015

subroutine read_meas_data(filename,meass,nmeas,meas_data)
implicit none
character (len=*),intent(in) :: filename
integer,intent(in) :: meass,nmeas !size of measurement data matrix
real(real_x),dimension(meass,nmeas),intent(out) :: meas_data
integer :: jj

open(41,file=filename,status='old')
do jj=1,meass
    read(41,*) meas_data(jj,:)
end do
close(41)
end subroutine read_meas_data


!**********************************************************************************************************************************************
!
! Use measurement data
! Time of day in seconds
!
!**********************************************************************************************************************************************

! Not in use Michael 05/2015

subroutine use_meas_data(meass,time_in,time_meas,data_in,data_out)
implicit none
integer,intent(in) :: meass
real(real_x),dimension(meass),intent(in) :: time_meas, data_in
real(real_x),intent(in) :: time_in
real(real_x),intent(out) :: data_out
integer :: jj
jj=1

if (time_in<time_meas(1)) then
    data_out=data_in(1)
elseif (time_in<time_meas(meass)) then
    do while (jj<meass)
        if (time_in < time_meas(jj+1) .and. time_in >= time_meas(jj)) then
            data_out=data_in(jj)+(data_in(jj+1)-data_in(jj)) &
            /(time_meas(jj+1)-time_meas(jj)) *(time_in-time_meas(jj))
            jj=meass+1
        end if

        jj=jj+1
    end do
else
    data_out=data_in(meass)
endif

end subroutine use_meas_data


!**********************************************************************************************************************************************
!
! Use measurement data (no interpolation)
! Time of day in seconds
!
!**********************************************************************************************************************************************

! Not in use Michael 05/2015

subroutine use_meas_data_noip(meass,time_in,time_meas,data_in,data_out)
implicit none
integer,intent(in) :: meass
real(real_x),dimension(meass),intent(in) :: time_meas, data_in
real(real_x),intent(in) :: time_in
real(real_x),intent(out) :: data_out
integer :: jj
jj=1

if (time_in<time_meas(1)) then
    data_out=data_in(1)
elseif (time_in<time_meas(meass)) then
    do while (jj<meass)
        if (time_in < time_meas(jj+1) .and. time_in >= time_meas(jj)) then
            data_out=data_in(jj)
            jj=meass+1
        end if

        jj=jj+1
    end do
else
    data_out=data_in(meass)
endif

end subroutine use_meas_data_noip


!**********************************************************************************************************************************************
!
! Update run log. Modify according to your needs.
!
!**********************************************************************************************************************************************

! Not in use Michael 05/2015

subroutine update_log(run,dir)
implicit none
integer, intent(in) :: run
character (len=32) :: dir
open(90,file=trim(adjustl(dir))//'/run.log',status='unknown',position='append')
write(90,*) run
close(90)

end subroutine update_log

end module uhma_io




!oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
!
!   Set various model parameter
!   Some parameters have been set GLOBAL in uhma_datatypes.f90 
! --------------------------------------------------------------
! subroutine uhma_set_input(initial_dist,particles,ambient,options)
! implicit none
! type(uhma_initial_dist),intent(out) :: initial_dist
! type(uhma_particles),intent(inout) :: particles
! type(uhma_ambient),intent(inout) :: ambient
! type(uhma_options),intent(inout) :: options
! 
! real(real_x),parameter :: avogadro = 6.0221d23 ! Avogadro number (#/mol)
! 
! initial_dist%r_min=(1.5d-9)/2.          ! (initial) dry radius of smallest section
! initial_dist%r_max=(5.0d-7)/2!(1.0d-6)/2.          ! (initial) dry radius of largest section
! 
! ! initial modes
! initial_dist%r_mode=(/3.0d-8,8.0d-9/)/2.0 !(/2.d-8,1.d-7/)/2.  ! mode radius
! initial_dist%n_mode= (/0.0d9, 0.0d0/)!(/3.5d9,6.4d8/)      ! mode conc.
! initial_dist%sigma_mode= (1.5, 1.3)!(/1.6,1.3/)      ! mode deviation
! initial_dist%mfractions= (/1.0,0.0,0.0,0.0/)*1.0!(/1,0,0,0,0,0,0,0,0,0,0,0 /)/1.0 !(/0.62,0.38,0.0,0./)  !initial mass fractions of each compound
! 
! 
! particles%n_limit = 1.d-3              ! threshold number concentration (#/m^3)
! particles%cluster_vol=4.*3.141/3.*(initial_dist%r_min)**3.    ! volume of nucleated clusters
! 
! !test
! particles%nuc_rate = 1.0d4
! 
! !Set parameters for the vapor species
! ambient%density = (/1830.,1107.,1107.,1107./)!(/1830.,1107.,1107.,1107.,1107.,1107.,1107.,1107.,1107.,1107.,1107.,1107./)!(/ 1830., 1107., 1107., 1200./) !vapor densities (kg/m^3)
! ambient%molarmass = (/ 98.08, 150., 150., 150./)!(/ 98.08, 150., 150., 150.,150.,150.,150.,150.,150.,150.,150.,150. /) !(/ 98.08, 150., 150., 168./)  !vapor molar mass (g/mol)
! ambient%c_sat = (/0.d0, 1.d12, 1.d8, 1.d6/)*1.0d6!(/0.d0, 1.d6, 1.d1, 1.d2, 1.d3, 1.d4, 1.d5, 1.d6, 1.d7, 1.d8, 1.d9, 1.d10/) !(/ 0.d0, 1.d6, 1.d99, 1.d99/)!(/ 0.d0, 0.d6, 1.d12, 1.d10/)     !vapor saturation conc. (1/m^3)
! ambient%surf_ten = (/61.d-3, 0.02d0 , 50.d-3, 50.d-3/)!(/61.d-3, 0.02d0 , 50.d-3, 50.d-3, 50.d-3, 50.d-3 , 50.d-3, 50.d-3, 50.d-3, 50.d-3, 50.d-3 , 50.d-3 /) !(/61.d-3, 0.05d0 , 50.d-3, 50.d-3/) !(/61.d-3, 50.d-3, 50.d-3, 50.d-3/) !vapor surface tension (N/m)
! ambient%vap_conc= (/2.0d7,0.0d0,2.000d7,2.000d7/)*1.0d6!1*(/0.0d0,0.0d0,1.0d8,1.0d8,1.0d8,1.0d8,1.0d8,1.0d8,1.0d8,1.0d8,1.0d8,1.0d8 /)*1.0d4 !(/1.0d10,5.0d15,0.0d1,0.0d1/)!(/1.0d10,0.0d1,0.0d1,0.0d1/) !(initial) vapor conc. (1/m^3)
! ambient%vap_conc_min= 0 !0.0d0  ! minimum vapor conc (for each species)
! ambient%diff_vol = 51.96 !(/ 51.96, 51.96, 51.96/) ! diffusion volume (???? anton: Perhaps from Fuller E.N., K. Ensley and J. C. Giddings: J. Phys. Chem., 73: 3679 (1969) and their original article from 1966, found from Polin, Prausnitz and O'Connell, the Properties of Gases and Liquids, 5th edition 2000, McGraw-Hill)
! ambient%alpha = (/1., 0., 1.,1./) !(/ 1., 1., 1./)  ! mass accomodation coefficient
! 
! ambient%molecmass = ambient%molarmass(1:cond)*1.d-3/avogadro !mass of 1 molecule
! ambient%molecvol=ambient%molecmass/ambient%density(1:cond)   !vol of 1 molecule
! 
! ambient%gf_org = 1!1.1  ! hygroscopic growth factor (R_wet/R_dry) of nano-Kohler compound
! ambient%vap_limit = 1.               ! threshold vapour concentration (#/m^3)
! 
! ambient%temp=293.15             ! temperature in Kelvin
! ambient%rh=0.10                 ! relative humidity x100%
! ambient%pres= 1.01325d5          ! pressure in Pa
! ambient%nh3_mix=1!5.              ! NH3 in ppt
! ambient%so2_mix=0!410.            ! SO2 in ppt
! ambient%OH_conc=0!1.d6            ! OH conc [cm^-3]
! ambient%boundaryheight = 1000.  ! boundary layer height [m]
! ambient%nuc_coeff=5.0e-7!5e-9!5.e-7         ! nucleation coefficient (SI units)
! 
! options%meas_interval=300!300!600      ! output interval
! options%solver= 'rk4'!'eul'            ! solver for GDE
! options%dist_approach='fx'      ! distribution representation
! 
! ! Set nuc_approach='ffn' for 'FreeForm' nucleation
! ! Usage: set the variable options%nuc_number to give the nucleating compounds
! ! Examples: nuc_number=1 : sulphuric acid induced activation
! !           nuc_number=13: kinetic nucleation of compounds #1 and #3
! ! Options can be passed with the decimals:
! !           0.5: add vapors, e.g. 13.5: A*(vap(1)+vap(3)) and 0.5 for both in n_crit
! options%nuc_approach= 'def'!'ffn'      ! nucleation mechanism
! options%nuc_number=1            ! options for freeform nucleation ('ffn'), see below
! 
! !NOTE NUC_NUMBER IS NOT IN USE IN CURRENT UHMA VERSIONS, USE NUC_ARRAY
! !format is (compound 1, compound2, special case), where zero means no
! !so [1,0,0] is activation of 1, [1,2,0] is kinetic between 1 and 2,
! ! [1,2,1] is special case between 1 and 2.
! options%nuc_array = (/1,0,0/) ![] is fortran 2003, use (//) in fortran 95
! 
! options%latitude=61.51          ! latitude in degrees
! options%longitude=24.17         ! longitude in degrees
! options%day_of_year=87.         ! day of year
! 
! ! Flags for vapor dynamics
! options%vapor_chemistry=.false.!.true. ! production and other chemical reactions
! options%vapor_loss=.false.!.true.       ! loss due to nucleation and condensation
! 
! ! Flags for different processes
! options%nucleation=.true.
! options%condensation=.true.
! options%coagulation=.true.
! options%dry_deposition=.false.!.true.
! options%snow_scavenge=.false.
! 
! ! Flags for other model options
! options%equilibrate=.false.!.true.!.false.!.true.!.false.     ! calculate water and ammonia content (buggy), or set wet and dry sizes equal
! options%cluster_divide=.false.  ! divide nucleated clusters to surrounding bins if size inequal
! 
! end subroutine uhma_set_input
