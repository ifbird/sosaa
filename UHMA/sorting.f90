module sorting

! Anton Rusanen 2013

implicit none

!This could be replaced by a use statement for specific programs
integer, parameter :: rk = selected_real_kind(14, 30)

interface reorder
   module procedure sort1d_by_swaps, sort2d_by_swaps, sort3d_by_swaps
end interface reorder

interface check_order
   module procedure check_order_double, check_order_int
end interface check_order

public :: give_order,insertion_sort, reorder, rk, check_order

contains

  subroutine give_order(array, swaps)
    real(rk), dimension(:), intent(in) :: array
    integer, dimension(size(array,1)), intent(out) :: swaps

    real(rk), dimension(size(array,1)) :: temparray

    temparray = array

    call insertion_sort(temparray, swaps)

    return
    
  end subroutine give_order

  subroutine insertion_sort(array, swaps)

    !Sorts the array given in ascending order (smallest -> biggest)

    real(rk), dimension(:), intent(inout) :: array
    integer, dimension(size(array,1)), intent(out) :: swaps
    real(rk) :: stored

    integer :: storedswap

    integer :: i,j,length

    length = size(array,1)

    forall(i=1:length)
       swaps(i) = i
    end forall

    do i=2,length
       stored = array(i)
       storedswap = swaps(i)
       j=i
       do while(j > 1)
          if(stored < array(j-1)) then
             array(j) = array(j-1)
             swaps(j) = swaps(j-1)
             j = j-1
          else
             exit
          end if
       end do
       array(j) = stored
       swaps(j) = storedswap
    end do

  end subroutine insertion_sort

  function check_order_double(array) result(check_order)

    real(rk), dimension(:), intent(in) :: array
    logical :: check_order

    integer :: i

    check_order = .true.

    do i=2,size(array)
       if(array(i-1) > array(i)) then
          check_order = .false.
          return
       end if
    end do

  end function check_order_double

  function check_order_int(array) result(check_order)

    integer, dimension(:), intent(in) :: array
    logical :: check_order

    integer :: i

    check_order = .true.

    do i=2,size(array)
       if(array(i-1) > array(i)) then
          check_order = .false.
          return
       end if
    end do

  end function check_order_int


  subroutine sort1d_by_swaps(swaps, array)
    integer, dimension(:), intent(in) :: swaps
    real(rk), dimension(:), intent(inout) :: array
    array = array(swaps)
    return
  end subroutine sort1d_by_swaps

  subroutine sort2d_by_swaps(swaps, array)
    integer, dimension(:), intent(in) :: swaps
    real(rk), dimension(:,:), intent(inout) :: array
    array = array(swaps,:)
    return
  end subroutine sort2d_by_swaps
  
  subroutine sort3d_by_swaps(swaps, array)
    integer, dimension(:), intent(in) :: swaps
    real(rk), dimension(:,:,:), intent(inout) :: array
    array = array(swaps,:,:)
    return
  end subroutine sort3d_by_swaps

end module sorting 
