module condense

!**********************************************************************************************************************************************
!
! Calculates the effect of condensation on particle population
!
!**********************************************************************************************************************************************

use uhma_datatypes
implicit none
private
integer, parameter :: hydmax = 5,&
                      sections=uhma_sections,&
                      cond=uhma_cond,&
                      compo=uhma_compo,&
                      real_x=uhma_real_x

real(real_x), parameter :: &
     pi =  4.0_real_x*atan(1.0_real_x),    & ! pi = 3.1415927
     dens_as = 1780.,                      & ! density of ammonium bisulphate (kg/m^3)
     mass_as = 115.11d0,                   & ! molar mass of ammonium bisulphate (g/mol)
     dens_h2o = 1000.0d0,                  & ! -"- of water (kg/m^3)
     surften_org = 0.02d0,                 &
     avogadro = 6.0221d23,                 & ! Avogadro number (#/mol)
     molar_h2o = 18.016d0,                 & ! molar mass of water (g/mol)
     molec_h2o = molar_h2o*1.d-3/avogadro, &
     molar_air = 28.965,                   & !        - " -        air
     pstand = 1.01325d5,                   & ! standard pressure (Pa)
     boltzmann = 1.3807d-23,               & ! Boltzmann constant (J/K)
     r_g = 8.314d0                           ! molar gas constant (J/(mol K))


! fitted thermodynamic parameters for nano-Kohler
real(real_x), parameter :: coef_a = -190, coef_c = 0.3, coef_d = 0.8

real(real_x), parameter :: to_volume_conversion = 1E6**3.0, m3_to_um3 = (1D6)**3, um3_to_m3 = (1D-6)**3,g_to_kg=1d-3

integer, parameter :: vapor_start = 1, vapor_end=compo, number_start = compo+1, number_end = compo+sections, volume_start = number_end+1

! hydrate stuff
real(real_x) :: hydpw
real(real_x), dimension(hydmax) :: rhoh, kprod

type(uhma_ambient) ::  condensation_ambient

public :: apc_scheme, condensation, kelvin, Kelvin_Taylor,collision_rate, calc_vol_change


contains


!**********************************************************************************************************************************************
!
! This subroutine solved the condensation and replaces the old condensation subroutine
!
!**********************************************************************************************************************************************

subroutine apc_scheme(values, core, rdry, radius, mass, timestep, end_time, particles, ambient,CS_H2SO4,options)

  REAL(uhma_real_x), INTENT(in), target  :: values(compo+sections+sections*compo)
  real(uhma_real_x), dimension(sections), intent(in) ::  core, rdry, radius, mass
  
  !Values assumed to be constant in the short term (may change outside the solver)
  type(uhma_particles),intent(in) :: particles
  type(uhma_ambient),intent(in) :: ambient
  type(uhma_options),intent(in) :: options
  
  real(uhma_real_x), intent(in) :: timestep, end_time
  
  !!$    (vap_conc, n_conc, vol_conc, core, rdry, radius, mass, particles, ambient, options, timestep, end_time)
  !!$
  !!$    real(uhma_real_x), dimension(compo), intent(inout) :: vap_conc
  !!$    real(uhma_real_x), dimension(sections), intent(in) ::  n_conc, core, rdry, radius, mass ! current values
  !!$    real(uhma_real_x), dimension(sections,compo), intent(inout) :: vol_conc
  real(uhma_real_x), dimension(compo) :: C_q_guess, C_tot, vapor_volume
  real(uhma_real_x), dimension(sections) :: k, S, bin_volume
  
  real(uhma_real_x), pointer, dimension(:) :: n_conc, vap_conc
  real(uhma_real_x), pointer, dimension(:,:) :: vol_conc
  
  
  real(uhma_real_x), dimension(sections, compo) :: mole_fractions
  
  real(uhma_real_x) :: organics, time
  real(uhma_real_x),intent(out) :: CS_H2SO4
  integer :: i, ii


  time = 0

  !Associate array segments with the correct variables
  vap_conc(1:compo) => values(vapor_start:vapor_end)
  n_conc(1:sections) => values(number_start:number_end)
  vol_conc(1:sections, 1:compo) => values(volume_start:)

  do while (time < end_time)
    ! Total number concentration of each condensable vapor (gas phase and particle phase)
    ! unit: [molec m-3]
    C_tot = vap_conc + sum(vol_conc,1)*um3_to_m3/ambient%molecvol

    ! Mole fractions of each organic compounds in particle phase in each section
    ! Used for calculating Raoult's effect
    mole_fractions = 0

    ! Total volume of all the vapors in the particle phase in each section
    bin_volume = sum(vol_conc, 2)

    ! Total volume of each vapor from all the sections
    vapor_volume = sum(vol_conc, 1)

    ! Explain by QI: the inorganic components are also calculated in this lump.      i
    ! For the initial, we assume the composition is H2SO4. This is setted in uhma_interface.f90,initial_vapor_number
    
    if(options%raoult) then
      do ii=1,sections
        if(bin_volume(ii) > 0) then
          mole_fractions(ii,:) = (vol_conc(ii,:)*(1D-6**3))/ambient%molecvol/Avogadro ! um^3/m^3 -> m^3/m^3 -> molecules/m^3 -> mole/m^3
          organics = sum(mole_fractions(ii,:), MASK=(ambient%vapor_type == 2 .or. ambient%vapor_type == 3)) ! 2:soluble_organic, 3:generic_organic
          if(organics > 0) then
            mole_fractions(ii,:) = mole_fractions(ii,:)/organics !QI: inorganic fractions are calculated here in wrong way but they will not be used later.
          else
            mole_fractions(ii,:) = 1
          end if
        else
          !bin is empty, Raoult's law not relevant
          mole_fractions(ii,:) = 1
        end if
        ! only for test
        !mole_fractions(ii,:) = 1
      end do
    end if

    !Add by QI: close Raoult law for small particles
     ! do ii=1,sections
       ! if(radius(ii)<=1.d-9)  then  !can set the cut off size here!!!
         ! mole_fractions(ii,:)= 1
       ! endif
    ! enddo
    
    
    !....................................

    do i=1,compo

      ! Cycle if not a condensable type
      if(ambient%condensing_type(i) == not_available) cycle

      ! Only consider the organics with low c_sat and high vapor concentration
      ! if(.NOT.((ambient%c_sat(i))<3.0d18 .AND. vap_conc(i)>1.0d10)) cycle
      ! c_sat: saturation vapor concentration, [molec m-3]
      if(.NOT.((ambient%c_sat(i))<1.0d18 .AND. vap_conc(i)>1.0d10)) cycle

      ! Qi: new version:different way to calculate mass transfer rate(collision rate)
      ! unit: m3 s-1
      k = collision_rate(i,radius,mass,ambient)
      ! k=2*k  !QI:only for sensitive test!!!!!!!!!!!!!!!!!
      k= n_conc*k  ! m3 s-1 --> # s-1

      if (ambient%condensing_type(i) == normal) then
        ! call kelvin(i,S,ambient%surf_ten,ambient%molecvol,ambient%temp,radius)
        call kelvin_Taylor(i,S,ambient%surf_ten,ambient%molecvol,ambient%temp,radius)
        S = S*mole_fractions(:,i)
        ! S = 1  !for testing Qi
      else
        S = 1  !QI:here is for inorganic component.
      end if
    
      ! C_q,t substitution
      if(sum(k) /= 0) then
        C_q_guess(i) = vap_conc(i) + timestep*sum(k*S*ambient%c_sat(i))
        
        !write(*,*) 'C_q_guess: ', i, sum(mole_fractions(:,i)), vap_conc(i), timestep*sum(k*S*ambient%c_sat(i)), sum(k), sum(S), ambient%c_sat(i)
        !write(*,*) 'C_q_guess: ', i, C_q_guess(i), mole_fractions(3,i), vap_conc(i), timestep*k(3)*S(3)*ambient%c_sat(i), k(3), S(3), ambient%c_sat(i)
        !write(*,*) 'C_q_guess: ', i, C_q_guess(i), mole_fractions(10,i), vap_conc(i), timestep*k(10)*S(10)*ambient%c_sat(i), k(10), S(10), ambient%c_sat(i)
        
        C_q_guess(i) = C_q_guess(i)/(1+timestep*sum(k))  ! QI:C_q_guess is the gas phase vapor concentration. Page 544 in Funds of Atmos. model
      else
        C_q_guess(i) = 0
      end if
      
      !C_q_guess = min(C_tot, C_q_guess)
      IF (C_q_guess(i) > C_tot(i)) THEN
        C_q_guess(i) = C_tot(i)
      ENDIF
      !c_q,i,t
      do ii=1,sections
        vol_conc(ii,i) = vol_conc(ii,i) + timestep*k(ii)*(C_q_guess(i) - S(ii)*ambient%c_sat(i))*ambient%molecvol(i)/um3_to_m3
      
        if(vol_conc(ii,i) < 0) then
          vol_conc(ii,i) = 0
        end if
      end do
    
      ! Luxi included this because of problem when implementing UHMA in SOSAA but it also appears when running MALTE-box with certain vapour-properties files
      if (sum(vol_conc(:,i))*um3_to_m3/ambient%molecvol(i) .gt. C_tot(i)) then
          write(*,*) 'ratio ', C_tot(i) / (sum(vol_conc(:,i))*um3_to_m3/ambient%molecvol(i))
        !  write(*,*) 'i=',i
        !  write(*,*) 'vap',C_tot(i)
        !  write(*,*) 'mfraction',mole_fractions(:,i)
        !  stop
        ! vol_conc(:,i) = vol_conc(:,i) * C_tot(i) / (sum(vol_conc(:,i))*um3_to_m3/ambient%molecvol(i))
      endif !  # m^-3

      ! C_q,t
      if (i/=ambient%sulfuric_acid_index)then
        vap_conc(i) = C_tot(i) - sum(vol_conc(:,i))*um3_to_m3/ambient%molecvol(i) !  # m^-3
      endif


      ! Added by Michael to make sure gas-phase organic concentration are not going negative
      if (vap_conc(i) < 0.) then
          vap_conc(i) = 0.
      end if

      !write(*,*) 'vap: ', i, vap_conc(i), C_tot(i), sum(vol_conc(:,i))*um3_to_m3/ambient%molecvol(i)

      ! if(vap_conc(i) < 0) then
      !   open(734562,file='debug_vap.txt')
      !   write(734562,*) 'section',ii,'compo',i,'timestep',timestep,'unitcon',um3_to_m3,'c_sat',ambient%c_sat(i), &
            ! 'molevol',ambient%molecvol(i),'vol_conc',vol_conc(ii,i),'c_q_guess',c_q_guess(i), 'c_tot',c_tot(i),'s',s(ii),'k',k(ii)
      
      !   write(734562,*) 'vol_conc', vol_conc(:,i)
      !   write(734562,*) 'c_q_guess', c_q_guess
      !   write(734562,*) 'c-tot', C_tot
      !   write(734562,*) 'k',k
      !   write(734562,*) 'S',S
      
      !   print *, vap_conc(i), C_tot(i),sum(vol_conc(:,i))*um3_to_m3/ambient%molecvol(i)
      
      
      !   stop 'check debug_vap.txt'
      
      ! end if
    end do  ! i=1,compo

    if (any(values <0)) then
      do i=1,compo
        if(vap_conc(i) < 0) then
          print *, i, vap_conc(i), sum(vol_conc(:,i))*um3_to_m3/ambient%molecvol(i)
        end if
      end do
    end if
    
    time = time + timestep

  end do  ! do while (time < end_time)

  !...........add by Qi, for testing...........output k,S, c_sat....

  ! Added by Dean
  CS_H2SO4 = sum(n_conc*collision_rate(ambient%sulfuric_acid_index,radius,mass,ambient))
end subroutine apc_scheme




!**********************************************************************************************************************************************
!
! Calculates the Kelvin effect for other than nano-Köhler compounds
!
!**********************************************************************************************************************************************

subroutine kelvin(ii, kelvinEffect,surf_ten,molecvol,temp,radius)

    implicit none

    integer, intent(in) :: ii
    real(real_x),dimension(:),intent(in) :: surf_ten,molecvol,radius
    real(real_x),intent(in) :: temp
    real(real_x), dimension(sections), intent(out) :: kelvinEffect
    real(real_x), dimension(sections) :: exponent

    kelvinEffect = 0

    where(radius > 0)
        exponent=2.*surf_ten(ii)*molecvol(ii)
        exponent=exponent/(boltzmann*temp*radius)
        kelvinEffect=dexp(exponent)
    end where

end subroutine kelvin


!*****************************************************************************
! Kelvin_Taylor is use the Taylor formula to estimate the Kelvin effect equation.
! It's becasuse for 1-3nm particles the Kelvin effect is too high if we use the real Kelvin effect equation.
! ----writed by Qi
!*****************************************************************************
subroutine kelvin_Taylor(ii, kelvinEffect,surf_ten,molecvol,temp,radius)

  implicit none
  
  integer, intent(in) :: ii
  real(real_x),dimension(:),intent(in) :: surf_ten,molecvol,radius
  real(real_x),intent(in) :: temp
  real(real_x), dimension(sections), intent(out) :: kelvinEffect
  real(real_x), dimension(sections) :: exponent
  
  kelvinEffect = 0
  
  where(radius > 0)
    exponent=2.*surf_ten(ii)*molecvol(ii)
    exponent=exponent/(boltzmann*temp*radius)
    kelvinEffect=1+exponent ! Here we use the Taylor formula to estimate the Kelvin equation(i.e. exp(x)=1+x)
  end where

end subroutine kelvin_Taylor


subroutine kelvin_adchem(ii, kelvinEffect,molecvol,temp,radius)

    implicit none

    integer, intent(in) :: ii
    real(real_x),dimension(:),intent(in) :: molecvol,radius
    real(real_x),intent(in) :: temp
    real(real_x), dimension(sections), intent(out) :: kelvinEffect
    real(real_x), dimension(sections) :: exponent

    exponent=2.*surface_tension_adchem(temp)*molecvol(ii)
    exponent=exponent/(boltzmann*temp*radius)
    kelvinEffect=dexp(exponent)

end subroutine kelvin_adchem


!**********************************************************************************************************************************************
!
!  Calculates the collision rate (m^3/s) of condensing vapour molecules with particles in each size section
!
!   (for the 'unconventional' way to describe free-molecular condensation see Lehtinen & Kulmala:
!    A model for particle formation and growth in the atmosphere with molecular resolution in size;
!    Atmos. Chem. Phys. Discuss., 2, 1-17, 2002)
!
!**********************************************************************************************************************************************

function collision_rate(jj,radius,mass,ambient)
  implicit none
  
  type(uhma_ambient),intent(in) :: ambient
  integer, intent(in) :: jj                                                        ! the 'number' of condensing vapour
  real(real_x),dimension(:),intent(in) :: radius,mass
  real(real_x), dimension(sections) :: collision_rate
  
  integer :: ii
  real(real_x) :: air_free_path, dif_vap, r_vap, n1, viscosity, r_hyd, r_h2o,temp,rh,pres,zero
  real(real_x), dimension(sections) :: knudsen, corr, dif_part, mean_path, rate, apu, corr2
  real(real_x),dimension(compo) :: molecmass,molecvol,molarmass,molarmass1,diff_vol,alpha,dens
  !variables for new method(when input_flag=0)
  real(real_x) :: DH2SO40,DH2SO4,Keq1,Keq2,d_H2SO4,mH2SO4,speedH2SO4,Dorg,dX,dens_air,speedorg
  real(real_x),dimension(sections) :: gasmeanfpH2SO4,speed_p,KnH2SO4,f_corH2SO4,DH2SO4eff,gasmeanfporg,&
                                      Knorg,f_cororg,Dorgeff
  temp=ambient%temp
  rh=ambient%rh ! RH unit %,values from 1-100
  pres=ambient%pres
  molecvol=ambient%molecvol !m3
  molarmass=ambient%molarmass !g/mol
  molarmass1=molarmass*g_to_kg
  molecmass=ambient%molecmass !kg
  diff_vol=ambient%diff_vol
  alpha=ambient%alpha
  dens=ambient%density
  ! air mean free path (m) and viscosity (kg s^-1 m^-1) (where from?)
  air_free_path = (6.73d-8*temp*(1.0d0+110.4d0/temp))/(296.0d0*pres/pstand*1.373d0)
  viscosity = (1.832d-5*406.4d0*temp**1.5d0)/(5093*(temp+110.4d0))
  
  r_h2o = (3.0d0*molec_h2o/dens_h2o/(4.0d0*pi))**(1.0d0/3.0d0)  ! radius of water molecule
  r_vap = (molecvol(jj)*3.0d0/(4.0d0*pi))**(1.0d0/3.0d0)        ! radius of condensable molecules (m)
  
  ! for particles in each size section
  knudsen = air_free_path/radius                                  ! particle Knudsen number
  corr = 1.0d0 + knudsen*(1.142d0+0.558d0*exp(-0.999d0/knudsen))  ! Cunninghan correction factor (Allen and Raabe, Aerosol Sci. Tech. 4, 269)
  dif_part = boltzmann*temp*corr/(6*pi*viscosity*radius)          ! particle diffusion coefficient (m^2/s)
  
  IF (input_flag .EQ. 0) THEN
    ! H2SO4 mass transfer rate (m3/s) ,RH dependent Diffusion, diameter and mass
  
    IF ( jj .EQ. ambient%sulfuric_acid_index )  THEN
  
      DH2SO40 = 0.09D-4 ! gas diffusivity H2SO4 m2/s RH=0%
      Keq1 = 0.13D0
      Keq2 = 0.016D0
      DH2SO4 = (DH2SO40+0.85D0*DH2SO40*Keq1*rh+0.76D0*DH2SO40*Keq1*Keq2*rh**2D0) &
        / (1D0+Keq1*rh+Keq1*Keq2*rh**2D0) ! Diffusivity H2SO4 at ambient RH
  
      d_H2SO4 = (((molarmass1(jj)/dens(jj)/avogadro)*6D0/pi)**(1D0/3D0)+&
        Keq1*rh*(((molarmass1(jj)+18D-3)/dens(jj)/avogadro)*6D0/pi)**(1D0/3D0)+&
        Keq1*Keq2*rh**2D0*(((molarmass1(jj)+36D-3)/dens(jj)/avogadro)*6D0/pi)**(1D0/3D0))/&
        (1D0+Keq1*rh+Keq1*Keq2*rh**2D0) ! RH dependent H2SO4 diameter
      
      mH2SO4=(molarmass1(jj)/avogadro+Keq1*rh*(molarmass1(jj)+18D-3)/avogadro+Keq1*Keq2*rh**2D0*(molarmass1(jj)+36D-3)/avogadro)/&
        (1D0+Keq1*rh+Keq1*Keq2*rh**2D0) ! RH H2SO4 molecular mass
      
      speedH2SO4=SQRT(8D0*boltzmann*temp/(pi*mH2SO4)) ! speed of H2SO4 molecule
      gasmeanfpH2SO4=0
      where (mass>0)
       speed_p=SQRT(8D0*boltzmann*temp/(pi*mass)) ! speed of particle unit(mass)=kg
       gasmeanfpH2SO4=3D0*(DH2SO4+dif_part)/SQRT(speedH2SO4**2D0+speed_p**2D0)
      endwhere
  
      KnH2SO4=2D0*gasmeanfpH2SO4/(2*radius+d_H2SO4) ! Knudsen number H2SO4
  
      f_corH2SO4=(0.75*alpha(jj)*(1D0+KnH2SO4))/(KnH2SO4**2D0+KnH2SO4+0.283*KnH2SO4*alpha(jj)+0.75*alpha(jj)) ! Fuchs-Sutugin correction factor for transit
  
      DH2SO4eff=(DH2SO4+dif_part)*f_corH2SO4    !m2/s
  
      ! where (mass>0)
      collision_rate=2D0*pi*(2.*radius+d_H2SO4)*DH2SO4eff  !mass transfer rate m3/s
      ! elsewhere
      !   collision_rate=0
      ! endwhere
  
    ELSE
      !............................................................................
      !Organics mass transfer rate (m3/s)
  
      dX= (6.*molecvol(jj)/pi)**(1./3.)    !estimated diameter ( m)
      dens_air=molar_air*g_to_kg* pres/(r_g*temp)     ! density of air  kg/m3
      Dorg=5./(16.*avogadro*dX**2.*dens_air)*&
        sqrt(r_g*temp*molar_air*g_to_kg/(2.*pi)*((molarmass1(jj)+molar_air*g_to_kg)/molarmass1(jj))) ! diffusivity organic compound
      
      speedorg=SQRT(8D0*boltzmann*temp/(pi*molecmass(jj))) !spped of organic molecules
      gasmeanfporg=0
      where (mass>0)
       speed_p=SQRT(8D0*boltzmann*temp/(pi*mass)) ! speed of particle unit(mass)=kg
       gasmeanfporg=3D0*(Dorg+dif_part)/SQRT(speedorg**2D0+speed_p**2D0)
      endwhere
      
      Knorg=2D0*gasmeanfporg/(2*radius+dX)       ! Knudsen number organic comp
      f_cororg=(0.75*alpha(jj)*(1D0+Knorg))/&
        (Knorg**2D0+Knorg+0.283*Knorg*alpha(jj)+0.75*alpha(jj))     ! Fuchs-Sutugin correction factor for transit
      Dorgeff=(Dorg+dif_part)*f_cororg                    ! m^2/s
      ! where (mass>0)
      collision_rate=2D0*pi*(2.*radius+dX)*Dorgeff        ! mass transfer coefficient s^-1
      ! elsewhere
      !   collision_rate=0
      ! endwhere
      ! write(*,*) 'haha',collision_rate(1),collision_rate(30)
      ! stop
    ENDIF
  ENDIF  ! input_flag == 0
  
  IF (input_flag .EQ. 1) THEN   ! The input method is old version UHMA method. detail see uhma_datatype.f90 and uhma_interface.f90
    zero=0.0 !needed for argument
    ! molecular diffusion coefficient (m^2/s) for condensing vapour and
    ! mean free path of condensation process (m)
    dif_vap = diffusion(molarmass(jj), diff_vol(jj), zero,pres,temp)
    
    mean_path = 0
    where(mass > 0)
        mean_path = 3*(dif_vap + dif_part)/sqrt(8*boltzmann*temp/pi*(1./molecmass(jj) + 1./mass))
    end where
    
    knudsen = mean_path/(radius + r_vap)
    corr = knudsen + 1.                        ! transitional correction factor (Fuchs and Sutugin, 1971)
    corr = corr/(0.377*knudsen + 1 + 4./(3.*alpha(jj))*(knudsen+knudsen**2))
    
    ! no hydrates
    if (jj .ne. ambient%sulfuric_acid_index) then
      where(mass>0)        !corrected by Qi. only when mass>0,collision_rate is positive
        collision_rate = 4*pi*(radius + r_vap)*(dif_vap + dif_part)*corr                ! (m^3/s)
      elsewhere
        collision_rate = 0
      end where
      goto 100
    endif
    
    ! only for h2so4 (if hydrates taken into account)
    call hydrates(temp,rh)
    
    collision_rate = (radius+r_vap)*(dif_vap/hydpw + dif_part)*corr        ! (m^3/s)
    
    ! hydrate correction
    do ii = 1, hydmax
      n1 = real(ii)
      dif_vap = diffusion(molarmass(jj), diff_vol(jj), n1,pres,temp)
      mean_path = 3.*(dif_vap+dif_part)
      
      where(mass > 0)
          mean_path = mean_path/sqrt((8*boltzmann*temp)/pi*(1./(molecmass(jj)+n1*molec_h2o) + 1./mass))
      end where
      
      r_hyd = (r_vap**3+n1*r_h2o**3)**(1./3.)
      knudsen = mean_path/(radius + r_hyd)
      corr = knudsen + 1.
      corr = corr/(0.377*knudsen + 1 + 4./(3.*alpha(jj))*(knudsen+knudsen**2))
      collision_rate = collision_rate + corr*(dif_vap*rhoh(ii) + dif_part)*(radius+r_hyd)
    enddo
    
    where(mass >0)
      collision_rate = 4.*pi*collision_rate !(m^3/s)
    elsewhere
      collision_rate = 0
    end where
    ! write(*,*) 'haha',collision_rate(1),collision_rate(30)
    ! stop
    100 continue
  endif  ! input_flag == 1
end function collision_rate



!**********************************************************************************************************************************************
!
!  Evaluates molecular diffusion coefficient (m^2/s) for condensing vapour
!  (see Reid et al. Properties of Gases and Liquids, 1987)
!
!**********************************************************************************************************************************************

! In use Michael 04/2015
function diffusion(molar_gas, d_gas, hydrates,pres,temp)

implicit none

real(real_x), intent(in) :: molar_gas, d_gas, hydrates,pres,temp
real(real_x) :: d_air, d_h2o, diffusion

d_air = 19.70        ! diffusion volume of air (???)
d_h2o = 13.10        !                - " -                water

diffusion = 1.d-7*temp**1.75*sqrt(1./molar_air + 1./(molar_gas+hydrates*molar_h2o))
diffusion = diffusion/( (pres/pstand)*(d_air**(1./3.)+(d_gas+hydrates*d_h2o)**(1./3.))**2 )

end function diffusion



!**********************************************************************************************************************************************
!
! Calculate water and sulphuric acid saturation vapour pressures in ambient temperature (Pa)
!
!**********************************************************************************************************************************************

! In use Michael 04/2015
function waterps(temp)

implicit none
real(real_x),intent(in) :: temp
real(real_x) :: waterps

waterps=exp(77.344913 - 7235.4247/temp - 8.2*log(temp) + 0.0057113*temp)

end function waterps

! In use Michael 04/2015
function acidps(temp)

implicit none
real(real_x),intent(in) :: temp
real(real_x) :: lpar, acidps

lpar= -11.695+log(pstand) ! Zeleznik
acidps = 1/360.15-1.0/temp + 0.38/545.*(1.0+log(360.15/temp)-360.15/temp )
acidps = 10156.0*acidps +lpar
acidps = exp(acidps)

end function acidps



!**********************************************************************************************************************************************
!
!  Evaluates hydrate equilibrium coefficients and hydrate fractions in gas phase for sulphuric acid
!  by Hanna Vehkam�ki 2002
!!
!**********************************************************************************************************************************************

! In use Michael 04/2015
subroutine hydrates(temp,rh)

implicit none

real(real_x),intent(in) :: temp,rh
integer :: ii, jj
real(real_x), dimension(hydmax) :: kh
real(real_x) :: pres_h2o

pres_h2o = rh*waterps(temp)

! Zeleznik equilibrium constant for reactions
kh(1)= exp(6159.1796/temp-14.3343)        ! water+h2so4 =1-hydrate
kh(2)= exp(5813.8776/temp-15.5140)        ! i-hydrate+water= i+1-hydrate
kh(3)= exp(5246.0/temp-14.90)                ! i-hydrate+water= i+1-hydrate
kh(4)= exp(4934.0/temp-14.47)                ! i-hydrate+water= i+1-hydrate
kh(5)= exp(4763.0/temp-14.21)                ! i-hydrate+water= i+1-hydrate

do ii=1,hydmax
    kprod(ii)=1. !product of the equilibrium constants k1*k2*k3*..*ki1
    do jj = 1,ii
        kprod(ii)= kh(jj)*kprod(ii)
    enddo
enddo

! Jaecker-Voirol et al., J. Chem. Phys., vol. 87, 4849-4852 (1987):
! hydpw = pa(total)/pa(free) = 1+k1(pw/p0)+..+k1*k2*..*ki1(pw/p0)^ii+..+
hydpw = 1.0
do ii = 1,hydmax
    hydpw = hydpw + kprod(ii)*(pres_h2o/pstand)**ii
enddo

!concentration of the hydrates (divided by total concentration)
!rho(ii)/rho(tot)= k1*k2*..*ki1(pw/p0)^ii/hydpw
do ii = 1,hydmax
    rhoh(ii) = kprod(ii)*(pres_h2o/pstand)**ii/hydpw
enddo

end subroutine hydrates



!**********************************************************************************************************************************************

! In use Michael 04/2015
pure function surface_tension_adchem(T) result(surface_tension)

  !Subroutine for surface tension like it is in ADCHEM
!!$scaling_organics=1/3; % organic compounds surface tension is approximately 1/3 of water (Seinfeld and Pandis, 2006).
!!$surf_tens=scaling_organics*(76.1-0.155*(T-273.15))*10^-3; % (kg s^-2)

  real(uhma_real_x), intent(in) :: T

  real(uhma_real_x) :: surface_tension

  surface_tension = ((1.0/3.0)*((76.1-0.155*(T-273.15))*(10**(-3))))

end function surface_tension_adchem



!**********************************************************************************************************************************************
!
!  Calculates the effect of condensation of different vapours on particle population
!  Calculates also the condensation sink and growth rate.
!
!**********************************************************************************************************************************************

! Not in use Michael 04/2015
subroutine condensation(vap_conc, n_conc, vol_conc, core, rdry, radius, mass, particles, ambient, options, dvapor, dvolume)

    real(uhma_real_x), dimension(compo), intent(in) :: vap_conc
    real(uhma_real_x), dimension(sections), intent(in) ::  n_conc, core, rdry, radius, mass ! current values
    real(uhma_real_x), dimension(sections,compo), intent(in) :: vol_conc

    !Values assumed to be constant in the short term (may change outside the solver)
    type(uhma_particles),intent(in) :: particles
    type(uhma_ambient),intent(in) :: ambient
    type(uhma_options),intent(in) :: options

    real(uhma_real_x), dimension(compo), intent(inout) :: dvapor !change rates
    real(uhma_real_x), dimension(sections,compo), intent(inout) :: dvolume


    ! local variables

    real(uhma_real_x), dimension(sections,compo) :: vol_change

    integer :: ii

    stop 'Condensation routine is not in use.'

!!$    call calc_vol_change(vap_conc, n_conc, vol_conc, core, rdry, radius, mass, ambient, options, vol_change)
!!$
!!$
!!$    !This is for moving section
!!$    do ii=1,sections
!!$        dvolume(ii,:)=dvolume(ii,:) + vol_change(ii,:)*n_conc(ii)*m3_to_um3
!!$    enddo
!!$

!!$
!!$    !Calculate the condensation sink
!!$    do ii=1,compo
!!$        dvapor(ii) = dvapor(ii) - sum(n_conc*vol_change(:,ii))/ambient%molecvol(ii)        ! (#/ m^3 s)
!!$     enddo

end subroutine condensation



!**********************************************************************************************************************************************
!
! ?????????????
!
!**********************************************************************************************************************************************

! Not in use Michael 04/2015
subroutine calc_vol_change(vap_conc, n_conc, vol_conc, core, rdry, radius, mass, ambient, options, vol_change)

  real(uhma_real_x), dimension(compo), intent(in) :: vap_conc
  real(uhma_real_x), dimension(sections), intent(in) :: n_conc, core, rdry, radius, mass ! current values
  real(uhma_real_x), dimension(sections,compo), intent(in) :: vol_conc

  type(uhma_ambient),intent(in) :: ambient
  type(uhma_options),intent(in) :: options

  real(uhma_real_x), dimension(sections,compo), intent(out) :: vol_change

  real(uhma_real_x), dimension(sections) :: bin_volume, sat_conc, kelvin_eff
  real(uhma_real_x), dimension(compo) :: vapor_volume

  real(uhma_real_x), dimension(sections,compo) :: mole_fractions
  real(uhma_real_x), dimension(sections) :: collision

  real(uhma_real_x) :: organics

  integer :: ii, jj

  !test linearization
  real(uhma_real_x), parameter :: limit = 0.005

  vol_change = 0
  collision = 0

  mole_fractions = 0

  bin_volume = sum(vol_conc,2)
  vapor_volume = sum(vol_conc, 1)

  if(options%raoult) then
     do ii=1,sections
        if(bin_volume(ii) > 0) then
           mole_fractions(ii,:) = (vol_conc(ii,:)*(1D-6**3))/ambient%molecvol/Avogadro ! um^3/m^3 -> m^3/m^3 -> molecules/m^3 -> mole/m^3
           organics = sum(mole_fractions(ii,:), MASK=(ambient%vapor_type == 2 .or. ambient%vapor_type == 3))
           if(organics > 0) then
              mole_fractions(ii,:) = mole_fractions(ii,:)/organics!sum(mole_fractions(ii,:), MASK=(ambient%vapor_type == 2 .or. ambient%vapor_type == 3))
           else
              mole_fractions(ii,:) = 1
           end if
        else
           !bin is empty, Raoult's law not relevant
           mole_fractions(ii,:) = 1
        end if
     end do
  end if

  !TEST
!!$  where(mole_fractions < 1E-24)
!!$     mole_fractions = 1E-24
!!$  end where

do ii=1,cond
   if(((vap_conc(ii) < ambient%vap_limit) .and. (vapor_volume(ii) == 0)) .or. (ambient%condensing_type(ii) == not_available)) cycle

   collision = collision_rate(ii,radius,mass,ambient)        ! [m^3/s]

   sat_conc = 0

   select case(ambient%condensing_type(ii))
      case(normal)
          call kelvin(ii, kelvin_eff,ambient%surf_ten,ambient%molecvol,ambient%temp,radius)
          sat_conc = ambient%c_sat(ii)*kelvin_eff
          if(options%raoult) then
             sat_conc = sat_conc*mole_fractions(:,ii)
          end if

      case(acid) ! NB sulfuric
         continue

      case default
         write(*,*) 'Condensation scheme not implemented:', ambient%condensing_type(ii)
         stop

   end select


   vol_change(:,ii) = collision*ambient%molecvol(ii)*(vap_conc(ii) - sat_conc)

   !DEBUG TEST
   !Linearisation
!!$   if(ambient%condensing_type(ii) == normal) then
!!$   do jj=1,sections
!!$      if(mole_fractions(jj,ii) == 0) then
!!$         vol_change(jj,ii) = 0
!!$         cycle
!!$      end if
!!$      if(mole_fractions(jj,ii) < 0.5) then
!!$         sat_conc = limit * sat_conc/mole_fractions(jj,ii)
!!$         vol_change(jj,ii) =  collision(jj)*ambient%molecvol(ii)*(vap_conc(ii) - sat_conc(jj))
!!$         vol_change(jj,ii) =  mole_fractions(jj,ii)*(vol_change(jj,ii)/limit)
!!$      end if
!!$   end do
!!$   end if


   !Evaporation cannot happen if there is nothing
   do jj=1,sections
      if(vol_conc(jj,ii) == 0 .and. vol_change(jj,ii) < 0) then
         vol_change(jj,ii) = 0
      end if
   end do

end do


end subroutine calc_vol_change



!**********************************************************************************************************************************************
!
!   Routines related to nano-K�hler theory. For details, see
!       Kulmala et al., 'Organic aerosol formation via sulphate cluster activation'
!       J. Geophys. Res., 109 (D4), 4205, 10.1029/2003JD003961, 2004
!
!**********************************************************************************************************************************************

! Not in use Michael 04/2015
subroutine nanokohler(ii,water_cont, sat_org,ambient,n_conc,vol_conc)

implicit none

type(uhma_ambient),intent(in) :: ambient
integer, intent(in) :: ii
real(real_x), intent(out) :: water_cont, sat_org
real(real_x),dimension(:),intent(in) :: n_conc
real(real_x),dimension(:,:),intent(in) :: vol_conc

integer :: jj
real(real_x), parameter :: nu_org = 1.,        &
nu_as = 2.
real(real_x) :: vol_as, vol_h2o, n_org, n_as, n_h2o, temp, rh
real(real_x) :: nwmin, nwmax, nwold, d_rad, rad, x2, xo, surt, pjw
real(real_x) :: gamw, gamo, gapb, gambi, xbw, f, xbo, rw, v_as, v_org, v_others
real(real_x),dimension(cond) :: molecvol

temp=ambient%temp
rh=ambient%rh
molecvol=ambient%molecvol

call check_molecvol(molecvol)

! sulphate part of the cluster
v_as = vol_conc(ii,ambient%sulfuric_acid_index)/(n_conc(ii)*1.d6**3)        ! volume of sulphate content in one particle (m^3)
n_as = v_as/molecvol(ambient%sulfuric_acid_index)                                                ! number of sulphate molecules in particle

vol_as = mass_as*1.d-3/(avogadro*dens_as)                ! volume of ammoniumbisulphate molecule
vol_h2o = molar_h2o*1.d-3/(avogadro*dens_h2o)        ! volume of water molecule

if (n_as < 0.) then
    write(*,*) 'number of sulphate molecules is smaller than zero'
    write(*,*) ii, vol_conc(ii,1), n_conc(ii)
    stop
endif

! organic part of the cluster
v_org = vol_conc(ii,2)/(n_conc(ii)*1.d6**3)        ! volume of water soluble organic content
n_org = v_org/molecvol(2)                                                ! number of organic molecules in particle
if (n_org <= 0.) n_org = 0.05*n_as

! volume of the other components in a particle
if (compo > 2) then
    v_others = sum(vol_conc(ii,3:compo))/(n_conc(ii)*1.d6**3)
else
    v_others = 0.
endif

! based on the number of ammonium bisulphate and nano-K�hler
! organic compound, calculate the minimum and maximum number
! of water molecules
d_rad =  (3./pi/4.*(n_as*vol_as + n_org*molecvol(2)))**(1./3.)
nwmin = 0.
nwmax= (4.*(pi/3.)*((2.5*d_rad)**3.))/vol_h2o

n_h2o = 0.
do jj=1,20

    nwold= n_h2o
    n_h2o = (nwmin+nwmax)/2.

    ! here we assume that the particle is formed so that the
    ! 'nano-K�hler' solution is surrounding a core formed
    ! by the other compounds (thus the vapours interact only
    ! with 'nano-K�hler' solution)
    rad = (d_rad**3 + 3./(4.*pi)*(n_h2o*vol_h2o + v_others))**(1./3.)

    call xbulk(n_h2o,n_org,n_as,rad,x2,temp)

    xo = x2
    if (xo .eq. 0. ) then
    xo= (1.d-10)*n_as/(n_as+n_h2o)
    endif

    call pseust(xo,n_h2o,n_as,surt,pjw,temp)
    call pseubin(xo,n_h2o,n_as,gamw,gamo,gapb,gambi,n_org,temp)
    xbw = 1./(1./(1.-xo) +2.*n_as/n_h2o)

    f = rh/(dexp(2.*surt*vol_h2o/boltzmann/temp/rad)*xbw*gamw)-1.

    if(f.lt.0.) then
    nwmax = n_h2o
    else
    nwmin = n_h2o
    endif
enddo
xbo = xo*xbw/(1.-xo)

sat_org = gamo*xbo*dexp(2.*surt*molecvol(2)/(boltzmann*temp*rad))

rw = rad
water_cont= (n_h2o/avogadro)*molar_h2o

end subroutine nanokohler



!**********************************************************************************************************************************************
!
! Subroutine to check that the molecular volume of the organic compound following nano-K�hler is correct
!
!**********************************************************************************************************************************************

! Not in use Michael 04/2015
subroutine check_molecvol(molecvol)
implicit none
real(real_x),dimension(:),intent(in) :: molecvol


if ((molecvol(2) < 2.24d-28).or.(molecvol(2) > 2.26d-28)) then
    write(*,*) ' '
    write(*,*) 'NOTICE!'
    write(*,*) 'In order to use nano-Kohler, the molecular volume'
    write(*,*) 'of the organic compound needs to be 2.25d-8 m3.'
    write(*,*) 'Instead, it is ', molecvol(2), ' m3.'
    write(*,*) ' '
    stop
endif

end subroutine check_molecvol



!**********************************************************************************************************************************************
!
! Subroutine to calculate the molar fraction of the organic compound in the solution in Nano-K�hler theory
!
!**********************************************************************************************************************************************

! Not in use Michael 04/2015
subroutine xbulk(n1,n2,n3,rad,x2,temp)

implicit none

real(real_x), intent(in) :: n1, n2, n3, rad,temp
real(real_x), intent(out) :: x2

integer :: i
real(real_x) :: xl, xu, x, xy, xa
real(real_x) :: g1y, g2y, gpby, gabi, g1a, g2a, gpba
real(real_x) :: surty, surta, pj, dsurt, dmu1, dmu2, fb


xl = 0.
xu = 1.
if (n2 .eq. 0.) then
    x= 0.
    goto 11
endif
do i=1,40
    x = (xl+xu)/2.
    xy = x+.00001
    xa = x-.00001
    if (xa .lt. 0.) xa= 1.d-9
    if (xy .gt. 1.) xy= 1.
    call pseubin(xy,n1,n3,g1y,g2y,gpby,gabi,n2,temp)
    call pseubin(xa,n1,n3,g1a,g2a,gpba,gabi,n2,temp)
    call pseust(xy,n1,n3,surty,pj,temp)
    call pseust(xa,n1,n3,surta,pj,temp)
    dsurt = (surty-surta)/(xy-xa)
    dmu1 = boltzmann*temp*(dlog((1.-xy)*gpby)-dlog((1.-xa)*gpba)) &
    /(xy-xa)
    dmu2 = boltzmann*temp*(dlog(xy*g2y)-dlog(xa*g2a))/(xy-xa)
    fb = n1*dmu1 + n2*dmu2 + 4.*pi*rad**2*dsurt
    if(fb.gt.0.) then
    xl = x
    else
    xu = x
    endif
enddo
11        x2 = x

end subroutine xbulk


!**********************************************************************************************************************************************
!
! Pseudobinary surface tension for a system with n1 water, n2 organic and n3 ammonium sulfate molecules in Nano-K�hler theory
!
!**********************************************************************************************************************************************

! Not in use Michael 04/2015
subroutine pseust(xbu,n1,n3,stb,stwa,temp)

implicit none

real(real_x), intent(in) :: xbu, n1, n3,temp
real(real_x), intent(out) :: stb, stwa

real(real_x) :: st1, xb1, w

! water surface tension
st1 = (93.6635+.009133*temp-.000275*temp**2)/dens_h2o
stwa = st1

! mole fraction of water-ammonium bisulphate
xb1 = 1.-xbu
w = n3*mass_as/(n1*molar_h2o+n3*mass_as)

! water-ammonium bisulfate surface tension
st1 = st1 + 0.01467*w

! solution (i.e. water-bisulfate-organic) surface tension
stb = st1-(st1-surften_org)*(1.+coef_c*xb1/(1.-coef_d*xb1))*xbu
!stb = st1-(st1-condensation_ambient%surf_ten(2))*(1.+coef_c*xb1/(1.-coef_d*xb1))*xbu

end subroutine pseust


!**********************************************************************************************************************************************
!
! Pseudobinary activity coefficients for a system with n1 water, n2 organic and n3 ammonium sulfate molecules in Nano-K�hler theory
!
!**********************************************************************************************************************************************

! Not in use Michael 04/2015
subroutine pseubin(xb2,n1,n3,gam1,gam2,vl1,acs,n2,temp)

implicit none

real(real_x), intent(in) :: xb2, n1, n3, n2,temp
real(real_x), intent(out) :: gam1, gam2, vl1, acs

real(real_x) :: v11, v3, v1, bvl
real(real_x) :: xb1, xs1, w, vl2, c1, c2, c3, wf, act

v11 = molar_h2o*1.d-3/(avogadro*dens_h2o)
v3 = mass_as*1.d-3/(avogadro*dens_as)
v1 = v11+n3/n1*v3
bvl= 100.


xb1=1.-xb2

xs1 = n1/(n1+2.*n3)
w = n3*mass_as/(n1*molar_h2o+n3*mass_as)

! Van Laar coefficients

vl1 = coef_a/(1.+xb1/(xb2*bvl))**2/r_g/temp
vl1 = dexp(vl1)
vl2 = coef_a*bvl/(1.+bvl*xb2/xb1)**2/r_g/temp
vl2 = dexp(vl2)

! water activity coefficient in amm. bisulf. solution

c1 = -3.05d-3
c2 = -2.94d-5
c3 = -4.43d-7
wf=w*100.
act = 1+c1*wf+c2*wf**2+c3*wf**3
acs = act/xs1

gam1 = acs*vl1
gam2 = vl2

end subroutine pseubin



!**********************************************************************************************************************************************
!
! ???
!
!**********************************************************************************************************************************************

!!$subroutine condensation(sink,n_cond,vol_cond,volgr,particles,ambient,dist_approach,timestep)
!!$implicit none
!!$type(uhma_particles),intent(in) :: particles
!!$type(uhma_ambient),intent(in) :: ambient
!!$real(real_x), dimension(cond),intent(out) :: sink
!!$real(real_x), dimension(sections), intent(out) :: n_cond,volgr
!!$real(real_x), dimension(sections,compo),intent(out) :: vol_cond
!!$character (len=2),intent(in) :: dist_approach
!!$real(real_x), intent(inout) :: timestep
!!$
!!$real(real_x), dimension(sections) :: rateup, ratedown,n_conc,core,radius,n_temp, total_volume, particle_volume
!!$real(real_x), dimension(sections,compo) :: compo_ratio,vol_change,vol_conc_new,vol_conc, fractions, total_vol_change
!!$real(real_x), dimension(compo) :: density,molecvol, total_volume_transfered
!!$integer :: ii, k
!!$real(real_x) :: n_limit, a, b
!!$
!!$n_conc=particles%n_conc
!!$radius=particles%radius
!!$core=particles%core
!!$vol_conc=particles%vol_conc
!!$n_limit=particles%n_limit
!!$
!!$density=ambient%density
!!$molecvol=ambient%molecvol
!!$
!!$condensation_ambient = ambient
!!$
!!$vol_change=0.               !volume change rate of one particle
!!$n_cond=0.                   !change rate of particles in each bin
!!$vol_cond=0.                 !change rate of volume in each bin
!!$rateup=0.                   !rate of condensation growth to larger bin
!!$ratedown=0.                 !--"-- to smaller bin
!!$sink=0.                     !condensation sink
!!$
!!$!Anton
!!$volgr = 0
!!$!Anton
!!$
!!$!First calculate the growth of individual particles
!!$call calc_vol_change(vol_change,particles,ambient)
!!$
!!$
!!$!limit outgoing to available in particle phase
!!$do ii=1,sections
!!$   do k=1,cond
!!$      if(vol_conc(ii,k) < -vol_change(ii,k)*((1.0D6)**3.0)*n_conc(ii)*timestep ) then ! [um^3/m^3] < [(m^3/s) * (um^3/m^3)*(1/m^3)*s] = um^3/m^3
!!$         if(n_conc(ii) <= 0) then ! Too bad
!!$            print *, 'Volume concentration is not consistent'
!!$            stop 'Number concentration is zero or below, but stuff wants out'
!!$         end if
!!$         vol_change(ii,k) = -((vol_conc(ii,k)*((1D-6)**3.0))/n_conc(ii))/timestep ! [um^3/m^3]*[m^3/um^3]*[m^3/1]*[1/s] = [m^3/s]
!!$      end if
!!$   end do
!!$end do
!!$
!!$total_vol_change = -1
!!$do ii=1,sections
!!$   total_vol_change(ii,:) = vol_change(ii,:) * n_conc(ii)
!!$end do
!!$
!!$total_volume_transfered = sum(total_vol_change,1)
!!$
!!$!limit flux to available in gas phase
!!$!units checked
!!$
!!$! THIS HAS ISSUES WITH FIXED SECTION TIMESTEP MANIPULATION !!!!!!!!
!!$! AND SOMETHING ELSE
!!$
!!$do ii=1,cond
!!$   if((total_volume_transfered(ii)/ambient%molecvol(ii))*timestep >= ambient%vap_conc(ii)) then
!!$      if(ambient%vap_conc(ii) == 0) then
!!$         vol_change(:,ii) = 0
!!$      elseif(ambient%vap_conc(ii) < 0 .and. ambient%condensing_type(ii) /= not_available ) then
!!$         print *, 'Vapor concentration in the gas phase is below zero, vapor:', ii
!!$         !print *, total_vol_change(ii,:)
!!$         print *, total_volume_transfered(ii), ambient%vap_conc(ii)
!!$         stop
!!$      else
!!$         vol_change(:,ii) = vol_change(:,ii) * (ambient%vap_conc(ii)/(timestep*(total_volume_transfered(ii)/ambient%molecvol(ii)))) ! m^3/s * (1/m^3)/(s*(m^3/[m^3 s])/(m^3)) == m^3/s
!!$      end if
!!$   end if
!!$end do
!!$
!!$!Check that we don't evaporate more than exists in the particles
!!$!Note: possible change in timestep not accounted for (see below)
!#do ii=1,compo
!#    n_temp=n_conc
!#    where (n_temp<n_limit) n_temp=n_limit
!#    where (vol_conc(:,ii)/n_temp + vol_change(:,ii)*timestep*1.d6**3. < 0.) &
!#        vol_change(:,ii)=-1.*vol_conc(:,ii)/(n_temp*timestep*1.d6**3.)
!#    where(vol_conc(:,ii) <= 0 .and. vol_change(:,ii) < 0) vol_change(:,ii) = 0
!#enddo
!!$
!!$volgr = sum(vol_change,2)   !volume growth rate (m^3/s)
!!$
!!$
!!$! The result depends on whether bins are allowed to grow or not
!!$select case (dist_approach)
!!$
!!$!---- Fixed sectional approach ----
!!$case('fx')
!!$
!!$   forall(ii=1:sections)
!!$      !Total volume of particles if allowed to grow freely
!!$      vol_conc_new(ii,:) = vol_conc(ii,:)+n_conc(ii)*vol_change(ii,:)*timestep*to_volume_conversion !(um^3/m^3)
!!$   end forall
!!$
!!$   total_volume = sum(vol_conc_new,2) !(um^3/m^3)
!!$   particle_volume = 0
!!$   where(n_conc > 0)
!!$      particle_volume = total_volume/n_conc/to_volume_conversion !(m^3)
!!$   end where
!!$
!!$   do ii=1,sections
!!$      !Converting to fraction
!!$      !(if total volume is zero also all fractions are automatically zero for bin)
!!$      if(total_volume(ii) /= 0) then
!!$         fractions(ii,:) = vol_conc_new(ii,:)/total_volume(ii) !(unitless)
!!$      end if
!!$   end do
!!$
!!$    !find growth/shrink rates (only to the next/previous bin !!)
!!$    do ii=1,sections
!!$        if (volgr(ii)>0 .and. ii<sections) then
!!$            rateup(ii)=volgr(ii)/(core(ii+1)-core(ii))
!!$        elseif (volgr(ii)<0. .and. ii>1) then
!!$            ratedown(ii)=volgr(ii)/(core(ii-1)-core(ii))
!!$        endif
!!$    enddo
!!$
!!$    !check for too high growth rate and adjust the timestep accordingly
!!$    do ii=1,sections
!!$        do while (rateup(ii)*timestep>1. .or. ratedown(ii)*timestep>1.)
!!$            timestep=timestep/2.
!!$            write(*,*) 'Growth rate too high! Adjusting timestep to ', timestep,'s.'
!!$        enddo
!!$    enddo
!!$
!!$    !calculate change rates for bins (cf. GDE)
!    n_cond(1)=ratedown(2)*n_conc(2)-rateup(1)*n_conc(1)
!    do ii=2,sections-1
!        n_cond(ii)=ratedown(ii+1)*n_conc(ii+1)+rateup(ii-1)*n_conc(ii-1)-(rateup(ii)+ratedown(ii))*n_conc(ii)
!    enddo
!    n_cond(sections)=rateup(sections-1)*n_conc(sections-1)-ratedown(sections)*n_conc(sections)
!!$
!!$
!    do ii=1,sections
!        vol_cond(ii,:)=n_cond(ii)*core(ii)*compo_ratio(ii,:)/density/(sum(compo_ratio(ii,:)/density))*1.d6**3.
!        where(vol_cond(ii,:)<0.) vol_cond(ii,:)=0.
!    enddo
!!$
!!$new_vol:do ii=1,sections
!!$
!!$        !vol_cond(ii,:)=n_cond(ii)*core(ii)*compo_ratio(ii,:)/density/(sum(compo_ratio(ii,:)/density))*1.d6**3.
!!$
!!$        if(n_conc(ii) > n_limit) then
!!$
!!$            select case(ii)
!!$
!!$            case(2:sections-1)
!!$
!!$               if(volgr(ii) >= 0) then
!!$                  !Net condensation or no change
!!$                  a = (core(ii+1) - particle_volume(ii))/(core(ii+1) - core(ii))
!!$                  b = 1-a
!!$                  n_cond(ii) = n_cond(ii) + a*n_conc(ii)
!!$                  n_cond(ii+1) = n_cond(ii+1) + b*n_conc(ii)
!!$                  vol_cond(ii,:) = vol_cond(ii,:) + a*n_conc(ii)*core(ii)*fractions(ii,:)*to_volume_conversion
!!$                  vol_cond(ii+1,:) = vol_cond(ii+1,:) + b*n_conc(ii)*core(ii+1)*fractions(ii,:)*to_volume_conversion
!!$               else
!!$                  !Net evaporation
!!$                  a = (particle_volume(ii) - core(ii-1))/(core(ii) - core(ii-1))
!!$                  b = 1-a
!!$                  n_cond(ii) = n_cond(ii) + a*n_conc(ii)
!!$                  n_cond(ii-1) = n_cond(ii-1) + b*n_conc(ii)
!!$                  vol_cond(ii,:) = vol_cond(ii,:) + a*n_conc(ii)*core(ii)*fractions(ii,:)*to_volume_conversion
!!$                  vol_cond(ii-1,:) = vol_cond(ii-1,:) + b*n_conc(ii)*core(ii-1)*fractions(ii,:)*to_volume_conversion
!!$               end if
!!$
!!$            !Border cases ...Improve/Change if necessary
!!$            !lower bound is closed
!!$            !Upper bound is closed
!!$            !This will result in errors so #choose a domain large enough#!
!!$
!!$            case(1)
!!$
!!$               if(volgr(ii) >= 0) then
!!$                  !Net condensation or no change
!!$                  a = (core(ii+1) - particle_volume(ii))/(core(ii+1) - core(ii))
!!$                  b = 1-a
!!$                  n_cond(ii) = n_cond(ii) + a*n_conc(ii)
!!$                  n_cond(ii+1) = n_cond(ii+1) + b*n_conc(ii)
!!$                  vol_cond(ii,:) = vol_cond(ii,:) + a*n_conc(ii)*core(ii)*fractions(ii,:)*to_volume_conversion
!!$                  vol_cond(ii+1,:) = vol_cond(ii+1,:) + b*n_conc(ii)*core(ii+1)*fractions(ii,:)*to_volume_conversion
!!$               else
!!$                  !Boundary closed nothing happens on evaporation,
!!$                  !previously:
!!$                  !Net evaporation (out of domain) assuming that particles are lost into a lower bin
!!$                  !first try, IMPROVE if necessary
!!$                  !may lead to involatile gasses being released
!!$                  !a =  (particle_volume(ii) - particle_volume(ii)*0.5)/(core(ii) - particle_volume(ii)*0.5)
!!$                  !n_cond(ii) = n_cond(ii) + a*n_conc(ii)
!!$                  !vol_cond(ii,:) = vol_cond(ii,:) + a*n_conc(ii)*core(ii)*fractions(ii,:)*to_volume_conversion
!!$               end if
!!$
!!$
!!$            case(sections)
!!$
!!$               if(volgr(ii) >= 0) then
!!$                  !Cannot grow further
!!$                  !Preserving particle number, volume is not correct.
!!$                  !these particles will also not accumulate more volume from the gas phase
!!$                  !Choose a large enough domain to avoid this
!!$               else
!!$                  !Net evaporation
!!$                  a = (particle_volume(ii) - core(ii-1))/(core(ii) - core(ii-1))
!!$                  b = 1-a
!!$                  n_cond(ii) = n_cond(ii) + a*n_conc(ii)
!!$                  n_cond(ii-1) = n_cond(ii-1) + b*n_conc(ii)
!!$                  vol_cond(ii,:) = vol_cond(ii,:) + a*n_conc(ii)*core(ii)*fractions(ii,:)*to_volume_conversion
!!$                  vol_cond(ii-1,:) = vol_cond(ii-1,:) + b*n_conc(ii)*core(ii-1)*fractions(ii,:)*to_volume_conversion
!!$               end if
!!$
!!$
!!$            case default
!!$                write(*,*) 'Attempt to calculate bin: ', ii
!!$                stop 'Bin does not exist'
!!$
!!$            end select
!!$
!!$        end if
!!$
!!$    end do new_vol
!!$
!!$    !Converting to rate
!!$    forall(ii=1:uhma_sections)
!!$       vol_cond(ii,:) = (vol_cond(ii,:)-vol_conc(ii,:))/timestep !(um^3/m^3 s)
!!$    end forall
!!$    n_cond = (n_cond-n_conc)/timestep !(#/m^3 s)
!!$
!!$    !Calculate the condensation sink
!!$    do ii=1,cond
!!$       sink(ii) = (sum(vol_cond(:,ii))/to_volume_conversion)/molecvol(ii)        ! (#/ m^3 s)
!!$    enddo
!!$
!!$    !DEBUG stop negative concentrations, there might be small excess in the sum
!!$    where(ambient%vap_conc < sink*timestep)
!!$       sink = ambient%vap_conc*timestep
!!$    end where
!!$
!!$    volgr = 0
!!$
!!$
!!$!END new volume
!!$
!!$    !commented Anton
!!$!    volgr=0
!!$
!!$!---- Moving sectional approach ----
!!$case('ms')
!!$    n_cond=0.
!!$    do ii=1,sections
!!$        vol_cond(ii,:)=vol_change(ii,:)*n_conc(ii)*1.d6**3.
!!$    enddo
!!$    !This is for moving section
!!$
!!$    !Calculate the condensation sink
!!$    do ii=1,cond
!!$        sink(ii) = sum(n_conc*vol_change(:,ii))/molecvol(ii)        ! (#/ m^3 s)
!!$    enddo
!!$    !debug
!!$    if(any(sink*timestep > ambient%vap_conc*(1+1E-7))) then
!!$       !There might be small excesses due to numerical accuracy (but now it's accurate to 1E-5 %)
!!$       stop 'sink significantly exceeds ambient concentration'
!!$    end if
!!$
!!$!    print *, 'debug finished condensation'
!!$
!!$
!!$case default
!!$    stop 'The new condensation routine unimplemented for this dist. approach so far.'
!!$
!!$end select
!!$
!!$
!!$!Calculate growth rate (due to condensation only)
!!$!gr = (8.* radius**3. + 6./pi*sum(vol_change,2))**(1./3.) - 2.* radius
!!$
!!$end subroutine condensation



!**********************************************************************************************************************************************
!
! Calculates the change in volume due to condensation of different vapor species.
!
!**********************************************************************************************************************************************

!!$subroutine calc_vol_change(vol_change,particles,ambient)
!!$
!!$implicit none
!!$type(uhma_particles),intent(in) :: particles
!!$type(uhma_ambient),intent(in) :: ambient
!!$real(real_x), dimension(sections,compo), intent(out) :: vol_change
!!$
!!$integer :: ii, jj, kk, debug
!!$
!!$real(real_x) :: water_cont,vap_limit,temp,rh,n_limit
!!$real(real_x), dimension(sections) :: collision, kelvin_eff, sat_conc, sat_org,&
!!$            s_org,n_conc,radius,mass
!!$real(real_x),dimension(sections,compo) :: vol_conc, mole_fractions
!!$real(real_x),dimension(cond) :: c_sat,molecvol,surf_ten,vap_conc
!!$
!!$n_conc=particles%n_conc
!!$vol_conc=particles%vol_conc
!!$radius=particles%radius
!!$n_limit=particles%n_limit
!!$mass=particles%mass
!!$
!!$vap_limit=ambient%vap_limit
!!$c_sat=ambient%c_sat
!!$molecvol=ambient%molecvol
!!$temp=ambient%temp
!!$rh=ambient%rh
!!$surf_ten=ambient%surf_ten
!!$vap_conc=ambient%vap_conc
!!$
!!$collision = 0.        ! collision rate of molecules (m^3/s)
!!$s_org=0.
!!$vol_change = 0.
!!$
!!$where(n_conc < n_limit)
!!$   mass =  1!epsilon(n_limit) (these bins are not considered for condensation anyways)
!!$end where
!!$
!!$mole_fractions = 1
!!$
!!$#ifdef RAOULT
!!$
!!$do ii=1,sections
!!$   if(sum(vol_conc(ii,:)) > 0) then
!!$      mole_fractions(ii,:) = (vol_conc(ii,:)*(1D-6**3))/molecvol/Avogadro ! um^3/m^3 -> m^3/m^3 -> molecules/m^3 -> mole/m^3
!!$      mole_fractions(ii,:) = mole_fractions(ii,:)/sum(mole_fractions(ii,:))!sum(mole_fractions(ii,:), MASK=(ambient%vapor_type == 2 .or. ambient%vapor_type == 3))
!!$   else
!!$   !bin was empty, Raoult's law not relevant
!!$      mole_fractions(ii,:) = 1
!!$   end if
!!$end do
!!$
!!$#endif
!!$
!!$
!!$do ii = 1,cond                ! for each condensable vapour whose concentration is high enough
!!$    !if(vap_conc(ii) > vap_limit) then
!!$    if(vap_conc(ii) > vap_limit .or. sum(vol_conc(:,ii)) > 0) then
!!$
!!$        collision = collision_rate(ii,radius,mass,ambient)        ! [m^3/s]
!!$
!!$where(n_conc < n_limit)
!!$   collision = 0
!!$end where
!!$
!!$
!!$        sat_conc = 0.
!!$        ! saturation vapour pressure above the particle surface:
!!$        !
!!$        ! sulphuric acid (compound 1) is set non-volatile -> doesn't need to be treated
!!$
!!$        ! compound 2 follows nano-Köhler for small particles;
!!$        !        condenses with maximum flux for particles larger than 10 nm in diameter
!!$        !        (sat_conc set to zero above)
!!$        if (ambient%condensing_type(ii)==kohler) then
!!$            if (c_sat(ii)>0.) then
!!$                print *, 'Nano-kolhler calculation for compound :', ambient%vapor_names(ii)
!!$                stop 'Nano-kohler has not been updated'
!!$                !!Note Anton , stopped nano-kohler
!!$                cycle
!!$                !!end note
!!$                do jj = 1,sections
!!$                    if (n_conc(jj) > n_limit) then
!!$                        call nanokohler(jj,water_cont,s_org(jj),ambient,n_conc,vol_conc)
!!$                        sat_conc(jj) = c_sat(ii)*s_org(jj)
!!$                    endif
!!$                enddo
!!$            endif
!!$
!!$        ! for the rest, we take into account the Kelvin effect
!!$        else if (ambient%condensing_type(ii) == normal) then!.and. c_sat(ii)>0.) then
!!$            if(c_sat(ii)< 0) stop 'WHAT?'
!!$            call kelvin(ii, kelvin_eff,surf_ten,molecvol,temp,radius)
!!$            !call kelvin_adchem(ii, kelvin_eff,molecvol,temp,radius)
!!$            sat_conc = c_sat(ii)*kelvin_eff
!!$#ifdef RAOULT
!!$
!!$            ! P = P_o * X_i
!!$            sat_conc = sat_conc*mole_fractions(:,ii)
!!$            IF(ANY(sat_conc < 0)) then !again too bad
!!$               stop 'Negative saturation concentration'
!!$            end if
!!$
!!$#endif
!!$
!!$
!!$        endif
!!$
!!$        vol_change(:,ii) = collision*molecvol(ii)*(vap_conc(ii) - sat_conc)        ! volume change rate of a particle (m^3/s)
!!$
!!$        ! 2) we do not let nano-kohler organic evaporate at all
!!$        !                (assumed that it condenses irreversibly)
!!$        if (ambient%condensing_type(ii)== kohler) then
!!$            where (vol_change(:,ii) < 0.)
!!$               vol_change(:,ii) = 0.
!!$            end where
!!$        endif
!!$    endif
!!$enddo
!!$
!!$where(n_conc < n_limit)
!!$   mass = 0
!!$end where
!!$
!!$end subroutine calc_vol_change


!**********************************************************************************************************************************************

end module condense

