module input_reader

  use uhma_datatypes
  use input

  !Reads the input for cases with Lund people, also stores them.

  !maximum filename size
  integer, parameter :: maxbuf = 200

  real(uhma_real_x), dimension(:,:), allocatable :: initial_aerosols, environmental_data

  !initial_size, initial_number, initial_volume

  ! character(len=maxbuf), dimension(:), allocatable :: filenames

  character(len=*), parameter :: input_folder = 'input_lund'

contains

  ! subroutine get_filenames()

  ! end subroutine get_filenames

  subroutine get_data(filename)

    call read_data(initial_aerosols, input_folder//"/"//filename)
    call read_data(environmental_data, input_folder//"/"//filename)
    
  end subroutine get_data

end module input_reader
