module output_netcdf

!without NETCDF-libraries this is just an empty shell.
!Not very elegant, but keeps the makefile simple.
#ifdef NETOUT
    use uhma_datatypes
    use netcdf !Linking this can be difficult

    implicit none
    private

    !Id's for everything, just setting them all to zero to begin with , search for the names for further elaboration
    integer :: ncfile_id=0, &
      variable_id=0, &
      time_id=0, &
      section_id=0, &
      composition_id=0, &
      nconc_id=0, &
      radius_id = 0, &
      mass_id = 0, &
      Vol_conc_id= 0, &
      timevar_id=0, &
      dry_radius_id = 0, &
      orig_dry_radius_id = 0, &
      growth_rate_id = 0, &
      condensable_id= 0, &
      vap_conc_id= 0, &
      constant_id =0, &
      temperature_id= 0, &
      pressure_id= 0 , &
      saturation_ratio_id=0, &
      nucl_coef_id=0, &
      boundaryheight_id=0, &
      vapordensity_id=0, &
      molarmass_id=0, &
      saturation_id=0, &
      surfacetension_id=0, &
      diffusionvolume_id=0, &
      massaccomondation_id=0, &
      core_volume_id = 0, &
      order_id = 0

    integer, dimension(2) :: particles_nconc_dimension_ids, particles_radius_dimension_ids, &
    particles_mass_dimension_ids, ambient_vap_conc_ids
    integer, dimension(3) ::  part_volconc_dim_id

    character(len= *), parameter :: units = 'units'

    integer :: timecounter = 1

    public :: output_particles,output_ambient, initialize_output,finalize_output, &
     output_constants, output_time

contains

subroutine output_constants(ambient)

    type(uhma_ambient), intent(in) :: ambient

!    call handler( nf90_put_var(ncfile_id, temperature_id, ambient%temp, (/1/)))
!    call handler( nf90_put_var(ncfile_id, pressure_id, ambient%pres, (/1/)))
!    call handler( nf90_put_var(ncfile_id, saturation_ratio_id, ambient%rh, (/1/)))
    call handler( nf90_put_var(ncfile_id, nucl_coef_id, ambient%nuc_coeff, (/1/)))
    call handler( nf90_put_var(ncfile_id, boundaryheight_id, ambient%boundaryheight, (/1/)))

    call handler( nf90_put_var(ncfile_id, vapordensity_id, ambient%density))
    call handler( nf90_put_var(ncfile_id, molarmass_id, ambient%molarmass))
!    call handler( nf90_put_var(ncfile_id, saturation_id, ambient%c_sat))
    call handler( nf90_put_var(ncfile_id, surfacetension_id, ambient%surf_ten))
    call handler( nf90_put_var(ncfile_id, diffusionvolume_id, ambient%diff_vol))
    call handler( nf90_put_var(ncfile_id, massaccomondation_id, ambient%alpha))

end subroutine output_constants

subroutine output_options(options)

    type(uhma_options), intent(in) :: options

    call handler(nf90_put_att(ncfile_id, NF90_GLOBAL, 'solver', &
        options%solver))

    call handler(nf90_put_att(ncfile_id, NF90_GLOBAL, 'approach', &
        options%dist_approach))

    call handler(nf90_put_att(ncfile_id, NF90_GLOBAL, 'nucleation', &
        options%nuc_approach))

end subroutine output_options

subroutine output_time(time, timestep)

    integer, intent(in) :: timestep
    real(uhma_real_x), intent(in) :: time

    call handler(nf90_put_var(ncfile_id, timevar_id, time, (/timestep/) ))

end subroutine output_time

subroutine output_particles(particles, timestep)

    type(uhma_particles), intent(in) :: particles
    integer, intent(in) :: timestep

    integer :: bin, compound
    bin=0
    compound=0

!Slow, basic. I guess I did not want to think about maps and count. Should do that in the future, okay for test.

    !do bin = 1, size(particles%n_conc)
    !write(*,*) ncfile_id, nconc_id, particles%n_conc, start=(/1, timestep/), count=(/uhma_sections
    !write(*,*) ncfile_id, nconc_id, particles%n_conc, timestep, uhma_sections
    call handler( nf90_put_var(ncfile_id, nconc_id, particles%n_conc, start=(/1, timestep/), count=(/uhma_sections, 1/)))
    call handler( nf90_put_var(ncfile_id, radius_id, particles%radius, start=(/1, timestep/), count=(/uhma_sections, 1/)))
    call handler( nf90_put_var(ncfile_id, dry_radius_id, particles%rdry, start=(/1, timestep/),count=(/uhma_sections, 1/) ))
    call handler( nf90_put_var(ncfile_id, orig_dry_radius_id, particles%rdry_orig, start=(/1, timestep/), count=(/uhma_sections, 1/)))
    call handler( nf90_put_var(ncfile_id, growth_rate_id, particles%gr, start=(/1, timestep/), count=(/uhma_sections,1/)))
    call handler( nf90_put_var(ncfile_id, mass_id, particles%mass, start=(/1, timestep/), count=(/uhma_sections,1/) ))
    call handler( nf90_put_var(ncfile_id, core_volume_id, particles%core, start=(/1, timestep/), count=(/uhma_sections,1/)))
    
    call handler( nf90_put_var(ncfile_id, order_id, particles%order, start=(/1, timestep/), count=(/uhma_sections, 1/)))    

        !do compound = 1, size(particles%vol_conc, 2)

            call handler(nf90_put_var(ncfile_id, vol_conc_id, particles%vol_conc, (/1, 1, timestep/), (/uhma_sections, uhma_compo, 1/) ))

        !end do

    !end do


    return

end subroutine output_particles

subroutine output_ambient(ambient, timestep)

    type(uhma_ambient), intent(in) :: ambient
    integer, intent(in) :: timestep
    !real(uhma_real_x) :: time

    integer :: compound, condensable
    compound=0
    condensable= 0

    call handler( nf90_put_var(ncfile_id, temperature_id, ambient%temp, (/timestep/)))
    call handler( nf90_put_var(ncfile_id, pressure_id, ambient%pres, (/timestep/)))

    !do condensable=1, size(ambient%c_sat)

        call handler( nf90_put_var(ncfile_id, saturation_id,ambient%c_sat, (/1, timestep/), (/uhma_cond, 1/)))
        call handler( nf90_put_var(ncfile_id, vap_conc_id,ambient%vap_conc, (/1, timestep/), (/uhma_cond,1/)))

    !end do


end subroutine output_ambient

subroutine initialize_output(filename, explanation, experiment_set, options)

    !Extend as needed. Define stuff here.

    character(len=*), intent(in) :: filename, explanation, experiment_set
    type(uhma_options) :: options

    integer :: shuffle=0, compress=1, compression_level=9 !new line
    integer :: number_of_steps

    !Clearing file

    open(999, FILE=filename, ERR = 100)
    close(999)

    !Opening file. Overwrites

   ! call handler( nf90_create(trim(filename), NF90_CLOBBER, ncfile_id) )
    call handler( nf90_create(trim(filename), IOR(NF90_NETCDF4,NF90_CLASSIC_MODEL), ncfile_id) ) !  NEW LINE

    ! Defining dimensions: time(unlimited), size sections, vapor_species

    call handler(nf90_def_dim(ncfile_id, "time",NF90_UNLIMITED, time_id) )

    call handler(nf90_def_dim(ncfile_id, "Sections",uhma_sections, section_id) )

    call handler(nf90_def_dim(ncfile_id, "Composition",uhma_compo, composition_id) )

    call handler(nf90_def_dim(ncfile_id, "Condensable",uhma_cond, condensable_id) )

    call handler(nf90_def_dim(ncfile_id, "Constant", 1, constant_id))

!Identifying different shapes for arrays

!Particles

     particles_nconc_dimension_ids = (/section_id, time_id/)

     particles_radius_dimension_ids = (/section_id, time_id/)

     particles_mass_dimension_ids = (/section_id, time_id/)

     part_volconc_dim_id = (/section_id, composition_id, time_id/)

     ambient_vap_conc_ids = (/condensable_id, time_id/)

!Ambient:



    ! Defining variables to write

    !Particles:

    call handler( nf90_def_var(ncfile_id, "number_concentration", NF90_DOUBLE, particles_nconc_dimension_ids, nconc_id) )

    call handler( nf90_def_var(ncfile_id, "radius", NF90_DOUBLE, particles_radius_dimension_ids, radius_id) )

    call handler( nf90_def_var(ncfile_id, "dry_radius", NF90_DOUBLE, particles_radius_dimension_ids, dry_radius_id) )

    call handler( nf90_def_var(ncfile_id, "original_dry_radius", NF90_DOUBLE, particles_radius_dimension_ids, orig_dry_radius_id) )

    call handler( nf90_def_var(ncfile_id, "growth_rate", NF90_DOUBLE, particles_radius_dimension_ids, growth_rate_id) )

    call handler( nf90_def_var(ncfile_id, "core_volume", &
    NF90_DOUBLE, particles_radius_dimension_ids, core_volume_id) )

    call handler( nf90_def_var(ncfile_id, "mass", NF90_DOUBLE, particles_mass_dimension_ids, mass_id) )

    call handler( nf90_def_var(ncfile_id, "time_in_units", NF90_DOUBLE, time_id, timevar_id) )

    call handler( nf90_def_var(ncfile_id, "volume_concentration", NF90_DOUBLE, part_volconc_dim_id, vol_conc_id) )

    call handler(nf90_def_var(ncfile_id, "Bin order", NF90_INT, particles_radius_dimension_ids, order_id))

    !Ambient:

    call handler( nf90_def_var(ncfile_id, "vapor_concentration", NF90_DOUBLE, ambient_vap_conc_ids, vap_conc_id) )

    call handler(nf90_def_var(ncfile_id, "temperature", NF90_DOUBLE, time_id, temperature_id))

    call handler(nf90_def_var(ncfile_id, "pressure", NF90_DOUBLE, time_id, pressure_id))

    

    !Constants:

!    call handler(nf90_def_var(ncfile_id, "temperature", NF90_DOUBLE, constant_id, temperature_id))

!    call handler(nf90_def_var(ncfile_id, "pressure", NF90_DOUBLE, constant_id, pressure_id))

!    call handler(nf90_def_var(ncfile_id, "saturation_ratio", NF90_DOUBLE, constant_id, saturation_ratio_id))

    call handler(nf90_def_var(ncfile_id, "nucleation_coefficent", NF90_DOUBLE, constant_id, nucl_coef_id))

    call handler(nf90_def_var(ncfile_id, "boundary_layer_height", NF90_DOUBLE, constant_id, boundaryheight_id))

    !vapor parameters

    call handler(nf90_def_var(ncfile_id, "vapor_density", NF90_DOUBLE, condensable_id, vapordensity_id))
    call handler(nf90_def_var(ncfile_id, "molarmass", NF90_DOUBLE, condensable_id, molarmass_id))
    
    call handler(nf90_def_var(ncfile_id, "saturation_concentration", NF90_DOUBLE, ambient_vap_conc_ids, saturation_id))
!    call handler(nf90_def_var(ncfile_id, "saturation_concentration", NF90_DOUBLE, condensable_id, saturation_id))
   
    call handler(nf90_def_var(ncfile_id, "surface_tension", NF90_DOUBLE, condensable_id, surfacetension_id))
    call handler(nf90_def_var(ncfile_id, "diffusion_volume", NF90_DOUBLE, condensable_id, diffusionvolume_id))
    call handler(nf90_def_var(ncfile_id, "mass_accomondation_coefficient", NF90_DOUBLE, condensable_id, massaccomondation_id))

    !Making info variable(well attribute) for general stuff

    call handler(nf90_put_att(ncfile_id, NF90_GLOBAL, 'info', explanation))
    call handler(nf90_put_att(ncfile_id, NF90_GLOBAL, 'experiment', experiment_set))

    call output_options(options)

    !defining units as attributes.

    !Particles

    call handler(nf90_put_att(ncfile_id, nconc_id, units , '1/m^3'))

    call handler(nf90_put_att(ncfile_id, radius_id, units, 'm'))

    call handler(nf90_put_att(ncfile_id, dry_radius_id, units, 'm'))

    call handler(nf90_put_att(ncfile_id, orig_dry_radius_id, units, 'm'))

    call handler(nf90_put_att(ncfile_id, growth_rate_id, units, 'm/s'))

    call handler(nf90_put_att(ncfile_id, core_volume_id, units, 'm^3'))

    call handler(nf90_put_att(ncfile_id, mass_id, units, 'kg'))

    call handler(nf90_put_att(ncfile_id, timevar_id, units, 's'))

    call handler(nf90_put_att(ncfile_id, vol_conc_id, units, 'um^3/m^3'))

    call handler(nf90_put_att(ncfile_id, order_id, units, ''))

    !Ambient

    call handler(nf90_put_att(ncfile_id, vap_conc_id, units, '1/m^3'))
    
    call handler(nf90_put_att(ncfile_id, temperature_id, units, 'K'))

    call handler(nf90_put_att(ncfile_id, pressure_id, units, 'Pa'))
    
    !constants:

!   call handler(nf90_put_att(ncfile_id, saturation_ratio_id, units, '1/1'))

    call handler(nf90_put_att(ncfile_id, nucl_coef_id, units, 'Si units'))

    call handler(nf90_put_att(ncfile_id, boundaryheight_id, units, 'm?'))

    !gas parameters

    call handler(nf90_put_att(ncfile_id, vapordensity_id, units, 'kg/m^3'))

    call handler(nf90_put_att(ncfile_id, molarmass_id, units, 'g/mol'))

    call handler(nf90_put_att(ncfile_id, saturation_id, units, '1/m^3'))

    call handler(nf90_put_att(ncfile_id, surfacetension_id, units, 'N/m'))

    call handler(nf90_put_att(ncfile_id, diffusionvolume_id, units, '????'))

    call handler(nf90_put_att(ncfile_id, massaccomondation_id, units, '1/1'))

    !Ending definition

! COMPRESSION (update according to your variables)
    call handler(nf90_def_var_deflate(ncfile_id, nconc_id, shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, radius_id, shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, dry_radius_id, shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, orig_dry_radius_id, shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, growth_rate_id, shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, core_volume_id, shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, mass_id, shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, timevar_id,shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, vol_conc_id, shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, order_id, shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, vap_conc_id , shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, temperature_id , shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, pressure_id , shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, nucl_coef_id , shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, boundaryheight_id , shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, vapordensity_id , shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, molarmass_id , shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, saturation_id, shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, surfacetension_id,shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, diffusionvolume_id,shuffle,compress, compression_level) )
    call handler(nf90_def_var_deflate(ncfile_id, massaccomondation_id,shuffle,compress, compression_level) )
! END COMPRESSION


    call handler( nf90_enddef(ncfile_id))

    return

    100 continue
    write (*,*) "Error in output, in opening: "//filename//""
    stop

end subroutine initialize_output
!

    subroutine finalize_output()

        call handler( nf90_close(ncfile_id))
        Write(*,*) 'Outputfile closed.'

    end subroutine


subroutine handler(status)

!This function outputs the possible error message, or does nothing if all is well.

integer, intent(in) :: status

if (status /= nf90_noerr) then
write(*,*) 'Error writing netcdf_output, Errorcode: ', NF90_STRERROR(status)
stop
end if


end subroutine handler

#endif

end module output_netcdf
