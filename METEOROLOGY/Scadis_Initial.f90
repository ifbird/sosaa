! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !
! Module for initializing meteorology in sosa.                                                  !
! Code for main loop in MT_MainMet                                                              !
! Commented and restructured by Rosa Gierens, University of Helsinki                            !
!                                                                                               !
!                                                                                               !
! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !

MODULE Scadis_Initial

  ! variables from other modules
  USE second_Precision, ONLY : dp    ! For simplicity, the precision of real numbers is applied to the whole program. L.
  USE SOSA_DATA !, ONLY : variablename
  
  ! subroutines and variables part of the meteorology scheme
  ! USE Scadis_Parameters
  ! USE Scadis_Radiation
  
  IMPLICIT NONE

  ! will make this privata later!
  !PRIVATE

  !Public subroutines:
  !PUBLIC :: 

  !Public variables:
  !PUBLIC ::  


CONTAINS


  SUBROUTINE MT_InitialScadis
    integer :: count_nonnan

    ! Roughness at the surface z0 (0.0001-1.0 m)
    z0 = 0.1

    ! Cover fraction of low, middle, and high clouds (0 - 10).
    mclob=0.
    mclom=0.
    mclot=0.

    ! Initial data for cloudiness  -> move to Scadis_Parameters
    clob=0.1*mclob
    clom=0.1*mclom
    clot=0.1*mclot

    ! s(k) - vertical leaf area density profile

    dtree=0.
    dtree2=0.
    do k=2,kz
      if(z(k) <= hc) then 
        dtree=dtree+0.5*((z(k-1)/hc)**(pa-1)*(1.-z(k-1)/hc)**(3-1)+(z(k)/hc)**(pa-1)*(1.-z(k)/hc)**(3-1))*(z(k)-z(k-1))
        dtree2=dtree2+0.5*((z(k-1)/hc)**(pa2-1)*(1.-z(k-1)/hc)**(3-1)+(z(k)/hc)**(pa2-1)*(1.-z(k)/hc)**(3-1))*(z(k)-z(k-1))
      endif
    enddo

    do k=1,kz
       s1(k)=0.0d0
       s2(k)=0.0d0
       if(z(k) <= hc) s1(k)=LAI_proj*(z(k)/hc)**(pa-1)*(1.-z(k)/hc)**(3-1)/dtree
       if(s1(k) < 0.005d0) s1(k)=0.

       if(z(k) <= hc) s2(k)=hc*(z(k)/hc)**(pa2-1)*(1.-z(k)/hc)**(3-1)/dtree2

       if(s2(k) < 0.005d0) s2(k)=0.

       if(hc == 0.0d0) then
          if(z(k) <= 2) kk=k
          nz=min(kk,kz)+1      !  level for downward radiation flux
       else
          if(z(k) <= (hc+2.)) kk=k
          nz=min(kk,kz)+1      !  level for downward radiation flux
       endif

       if(z(k) <= (33.6)) kkk=k
       nsmear=min(kkk,kz)+1 !  the closest level to 33.6 m

       if(z(k) <= (23.3)) kkk5=k
       n233=min(kkk,kz)+1 !  the closest level to Hyytiäla level

    enddo

    s1(1)=0.  ! [m2 m-3], LAD for Meteorology
    s1(2) = 1.32d0  ! [m2 m-3], LAD for understory layer (shallow dwarf shrub layer, h=0.2-0.3 m, projected LAI~0.25 m2 m-2)
                    ! 1.32 = 0.25/((0.38-0.0)/2). So all the understory layer is included in the layer 2.
    s2(1)=0.

    !======================= Set s1 as ASAM values ===========================!
    ! s1(2:5) = (/0.077, 0.150, 0.222, 0.150/)

    do k=2,kz
       dz(k)=z(k)-z(k-1)
    enddo

    ! Thus we get s1 and s2 that is now beta-function - 
    ! to calculate the integral we should do next to get LAD in % for megan per layer 

    do k=2,kz
       if(z(k) <= hc) then 
          LAD_P(k) = 0.5 * (s2(k)+s2(k-1)) * (z(k)-z(k-1)) / hc
       endif

    enddo

    ! Now dintegr is close to 1 all time depending on resolution of the model
    ! To calculate real value of s1 and LAD we need to do the next 

    do k=1,kz
       s2(k)=s2(k) * LAI_proj / hc   ! now s2 is the real LAD for MEGAN
    enddo


    df(1)=0.0d0

    do k=2,kz-1
       da(k)=dz(k+1)*(dz(k)+dz(k+1))
       db(k)=dz(k)*dz(k+1)
       dc(k)=dz(k)*(dz(k)+dz(k+1))
       df(k)=(dz(k+1)+dz(k))/dz(k+1)/4.
       df(k)=0.5
    enddo
    df(1)=df(2)

    alai=0.
    do k=2,kz
       alai=alai+(s1(k-1)+s1(k))*dz(k)/2.
    enddo

    ! Some parameters (?????)
    ztop=z(kz)
    zdown=0.
    tau3=300.
    wind=10.
    nua=0.0000141
    kw=1
    f1=0.00012
    eps=0.001_dp
    profile=9999.
    kva=0.0000224
    kta=0.0000214

    cc2=0.0436      
    c833=0.833
    c52=0.52
    alf=sqrt(cc2)*(c833-c52)/0.1872

    face= 2.7_dp
    alph=0.80_dp
    alni=0.80_dp
    roa=1.205
    rh2o=1000.0_dp
    cpa=1009.0_dp
    ch2o=4190.0_dp
    hleaf=0.003
    lv=2256000.0_dp
    lv=2501400.0_dp
    ! qv0=0.00485
    qv0=1.3318375_dp
    dels=0.99
    delf=0.95
    asl=1.5
    cd=0.2
    gamma=0.0098_dp


    ! calculation of arrival of total radiation ***********************************
    proatm=0.747   !proatm - average transparency of atmosphere
    aplace=61.5  ! data for Hyytiala
    place=aplace*pi/180.
    f1=7.29*0.00001*2.*sin(place)
    wsun=0.4145*sin(PIx2*(day-79.8)/365.24)    !  tirgnstrom

    zeit=pi*(daytime/c432-1.)
    zenit=sin(place)*sin(wsun)+cos(place)*cos(wsun)*cos(zeit)
    optmas=796.*(sqrt(zenit**2+0.0025)-zenit)
    rads=solar*zenit
    tata=tan(place)*tan(wsun)
    upsn=c432/pi*atan(sqrt(1.-tata**2)/tata)
    downsn=c864*(1.-1./PIx2*atan(sqrt(1.-tata**2)/tata))
    ct=0.4
    if(zenit >= 0.342) ct=0.2
    rads=rads*(1.-0.8*clob-0.5*clom-ct*clot)
    teil=28.5*(asin(zenit))*proatm**4
    rsnt=1.0*rads*teil/(1.+teil)
    rskt=1.0*rads/(1.+teil)
    if(daytime <= upsn) rsnt=0.
    if(daytime <= upsn) rskt=0.
    if(daytime >= downsn) rsnt=0.
    if(daytime >= downsn) rsnt=0.
    if(daytime <= upsn) zenit=1.
    if(daytime <= upsn) zenit=1.
    if(daytime >= downsn) zenit=1.
    if(daytime >= downsn) zenit=1.


    !  *****************************************************
    !  boundary conditions and input parameters
    !  *****************************************************

    if(abl == 1) then
       tah=border(1,1)
       qah=border(4,1)*roa ! [kg kg-1] --> [kg m-3]
    else
       tah=border_abl(1,1)
       qah=border_abl(4,1)*roa

    endif

    fsoil1=49.
    pp=0.
    temtran=0.000001*0.5
    temdif=0.5

    if(abl == 1) then

       windx=border(2,1)
       windy=border(3,1)

    else
       windx=border_abl(2,1)
       windy=border_abl(3,1)

    endif


    windx=sqrt(windx**2+windy**2)

    windy=0.

    udin=abs(wind)*0.40/log((hh+z0)/z0)

    sk00=0.002
    sks00=0.002

    al1=25.

    wg(1)=0.2
    wg1i=0.2
    wg(2)=0.250
    wg2i=0.250

    if (abl == 1) then
      ta(1)=tah-0.*border(5,1)*z(kz)
    else
      ! ta(1)=tah-border_abl(5,1)*z(kz)
      if (.not. isnan(local_temp(1,1))) then
        ta(1) = local_temp(1,1)
      else
        count_nonnan = count(.not. isnan(local_temp(1, :)))
        write(*,*) '[putian debug] count_nonnan', count_nonnan
        ta(1) = sum( local_temp(1, :), mask=.not. isnan(local_temp(1, :)) ) / count_nonnan
      end if

    endif

    qa(1)=qv0*exp(maga*(ta(1)-T00)/(magb+ta(1)-T00))/ta(1)
    qa(1)=qa(1)*0.7
    bt(1)=udin**2/0.09**0.5

    do k=1,kz
       !
       ts0=ta(1)-10.
       tsoil(k)=ts0

       if(abl == 1) then
          u(k)=windx 
          v(k)=windy
       else 
          u(k)=windx 
          if(z(k) < 300.) u(k)=windx*(z(k)/300.)**0.5
          v(k)=windy
          if(z(k) < 300.) v(k)=windy*(z(k)/300.)

       endif

       w(k)=0.

       qa(k)=qah
       ta(k)=ta(1)+z(k)*(tah-ta(1))/hh
       bt(k)=0.00001
       dbt(k)=0.001/bt(k)

       tsn(k)=ta(k)
       tsd(k)=ta(k)
       l(k)=0.40*(z(k)+z0)/(1+0.40*z(k)/al1)

       dhsn(k)=0.00
       dhsd(k)=0.00
       dvsn(k)=0.00
       dvsd(k)=0.00
       psn(k)=1.
       psk(k)=1.
       firu(k)=0.
       fird(k)=0.
       fniu(k)=0.
       fnid(k)=0.
       fphu(k)=0.
       fphd(k)=0.
       fluxle(k)=0.
       fluxh(k)=0.
       rih(k)=0.
    enddo

    DO k=1,11
       kt(k)=0.40*udin*(z(k)+z0)
    ENDDO
    DO k=12,kz
       kt(k)=kt(11)
    ENDDO

    DO k=1,kz
       ur(k)=0.1
    ENDDO


    nturb=1
    smol=1

    wg1(1)=wg(1)
    wg1(2)=wg(2)

    do k=1,kz
       l1(k)=l(k)
       tsoil1(k)=tsoil(k)
       alt(k)=1.35
       alt1(k)=1.35
       bt1(k)=bt(k)
       dbt1(k)=dbt(k)
       u1(k)=u(k)
       kt1(k)=kt(k)
       ta1(k)=ta(k)
       tsn1(k)=tsn(k)
       tsd1(k)=tsd(k)
       qa1(k)=qa(k)
       w1(k)=w(k)
       v1(k)=v(k)
    enddo
    nxod=0
    nxodrad=0


    if(abl == 1) then
       do j=1,1488
          border(4,j)= 0.9*(qv0*exp(maga*(border(1,j)-T00)/(magb+border(1,j)-T00))  /border(1,j)) 
       enddo

    else

       do j=1,121
          border_abl(4,j)=border_abl(4,j)*roa
       enddo

    endif

  END SUBROUTINE MT_InitialScadis


  !************************************************************************************'!
  ! Beta probability density function for LAD (Eq. 1, Markkanen et al., 2003, BLM)
  !************************************************************************************'!
  FUNCTION Beta_PDF(x, a, b)
    REAL(dp), INTENT(IN) :: x(:)
    INTEGER, INTENT(IN) :: a, b

    REAL(dp) :: Beta_PDF(SIZE(x))

    INTEGER :: k, nx
    REAL(dp) :: below

    nx = SIZE(x)

    below = 0.0d0
    DO k=2, nx
      below = below + 0.5d0*( x(k-1)**(a-1)*(1.0d0-x(k-1))**(b-1) + x(k)**(a-1)*(1.0d0-x(k))**(b-1) )*(x(k)-x(k-1))
    ENDDO

    Beta_PDF = x**(a-1)*(1.0d0-x)**(b-1) / below
  END FUNCTION Beta_PDF

END MODULE Scadis_Initial
