Scadis Reference
================

ORIGINAL CODE
which means the code as it was in Sosa 23.8.2014

Line numbers added for referencing




     do k=1,kz                                                                                                                      !1
        s(k)=0.                                                                                                                     !2
     enddo                                                                                                                          !3
    
     do k=1,kz                                                                                                                      !4
        if(rou.le.z(k)) go to 98801  ! rou = 0 snow cover excluded for now                                                          !5
        krelf=k-1                                                                                                                   !6
        s(k)=profile                                                                                                                !7
    98801   continue                                                                                                                !8
        s(k)=s1(k)*cd+s(k)                                                                                                          !9
     enddo                                                                                                                          !10

     wsun=0.409*sin(pi2*(day-79.8)/365.24)  ! Henderson-Sellers                                                                     !11
     zeit=pi*(time/c432-1.)                                                                                                         !12
     zenit=sin(place)*sin(wsun)+cos(place)*cos(wsun)*cos(zeit)                                                                      !13

     optmas=1.*796.*(sqrt(zenit**2+0.0025)-zenit)                                                                                   !14
     optmas=min(6.d0,optmas)                                                                                                        !15
     zenit=max(1.d-2,zenit)                                                                                                         !16



     rads=solar*zenit     !max possible radiation                                                                                   !17
     ! rads - downward directed shortwave radiative flux without cloudness                                                          !18
     tata=tan(place)*tan(wsun)                                                                                                      !19
     upsn=c432/pi*atan(sqrt(1.-tata**2)/tata)                                                                                       !20
!21       downsn=c864*(1.-1./pi2*atan(sqrt(1.-tata**2)/tata))
!22       ct=0.4
!23       transp=0.8
!24       sumrad=solar*zenit
!25       clo=min(1.d0,(clob+clom+clot))
!26       if(zenit.ge.0.342) ct=0.2
!27       tran=(0.6+0.2*zenit)*(1.-0.4*clob)*(1-0.7*clom)*(1.-0.4*clot)
!28       trancl=max(2.d-1,(1.-0.8*clob-0.5*clom-ct*clot))
!29       transp=0.8
!30       rads2=rads*trancl  *(0.3+0.7*transp**optmas) !real radiation 
!31       clo=0.
!32       emm2=(1.-clo)*0.0000092*(ta1(nz )**2) +clo
!33       emm2=max(7.2d-1,emm2)
!
!34       if(abl.eq.1.) then
!
!35          dtmet=1800.
!36          nxodmet=int(tmcontr/dtmet)+1
!37          shift=(tmcontr-(nxodmet-1)*dtmet)/dtmet
!
!38          tax=border(1,nxodmet) +shift*(border(1,nxodmet+1)-border(1,nxodmet))
!
!
!39          temgrad=border(5,nxodmet) +shift*(border(5,nxodmet+1)-border(5,nxodmet))
!
!40          temgrad=max(temgrad,1.d-2)
!
!41          windx=border(2,nxodmet) +shift*(border(2,nxodmet+1)-border(2,nxodmet))
!
!42          windy=border(3,nxodmet) +shift *(border(3,nxodmet+1)-border(3,nxodmet))
!
!43          qax=qah
!44          qax=border(4,nxodmet) +shift *(border(4,nxodmet+1)-border(4,nxodmet))
!
!45          windy=0.
!
!46       else
!
!47          ! linearly interpolating for the current time step
!48          dtmet=21600.
!49          nxodmet=int(tmcontr/dtmet)+1
!50          shift=(tmcontr-(nxodmet-1)*dtmet)/dtmet
!
!
!51          tax=border_abl(1,nxodmet)+shift*(border_abl(1,nxodmet+1)-border_abl(1,nxodmet))
!
!52          temgrad=-1.*(border_abl(5,nxodmet)+shift*(border_abl(5,nxodmet+1)-border_abl(5,nxodmet)))
!
!53          windx=border_abl(2,nxodmet)+shift*(border_abl(2,nxodmet+1)-border_abl(2,nxodmet))
!
!54          windy=border_abl(3,nxodmet)+shift*(border_abl(3,nxodmet+1)-border_abl(3,nxodmet))
!
!55          qax=border_abl(4,nxodmet)+shift*(border_abl(4,nxodmet+1)-border_abl(4,nxodmet))
!
!56          windy=0.
!
!57     endif
!
!58       ! LWR data from reanalysis: linearly interpolating for time of the current time step
!59       dtmet=10800.  ! time difference btw data points 3h = 10800s
!     
!60       ! ECMWF data in UTC time, so
!61       ! time = 0s      -> LWR_in(1)
!62       ! time = 3600s   -> LWR_in(2)
!63       ! time = 4*3600s -> LWR_in(3)
!64       ! etc...
!65       nxodmet= floor((tmcontr - 1*3600)/dtmet) + 2 ! "row to read" (index) from the input data
!66       shift=(tmcontr +2*3600 -(nxodmet-1)*dtmet )/dtmet
!67       LWRdown = LWR_in(nxodmet)+shift*(LWR_in(nxodmet+1)-LWR_in(nxodmet))
!
!68       temgrad = min(temgrad,1.d-1)
!     
!69       emm=emm2
!70       rhelp=0.847-1.61*zenit+0.9*zenit**2-0.05*zenit**3 ! help function
!71       radk=(1.47-rhelp)/1.66               !  same
!72       if(rads.gt.0.) then
!73          relrad=rads2/rads
!74          apar=(0.22/relrad)**0.37
!75       endif
!76       if(relrad.lt.0.22) then
!77          rskt=rads2
!78          rsnt=0.
!79       end if
!80       if(relrad>=0.22.or.relrad<0.35) then
!81          rskt=min(rads2,rads2*(1.-6.4*(0.22*relrad)**2)*apar)
!82          rsnt=rads2-rskt
!83       end if
!84       if(relrad>=0.35.or.relrad<radk) then
!85          rskt=min(rads2,rads2*(1.47-1.66*relrad)*apar)
!86          rsnt=rads2-rskt
!87       end if
!88       if(relrad>=radk) then
!89          rskt=min(rads2,rads2*rhelp*apar)
!90          rsnt=rads2-rskt
!91       end if
!
!92       rsktm=rskt
!93       rsntm=rsnt
!94       rsnt=dirtop1(nxodrad) +(tmcontr-(nxodrad-1)*1800.) *(dirtop1(nxodrad+1)-dirtop1(nxodrad))/1800.
!95       rskt=difftop1(nxodrad) +(tmcontr-(nxodrad-1)*1800.) *(difftop1(nxodrad+1)-difftop1(nxodrad))/1800.

!     
!96       if(abl.ne.1.) then
!
!97          !  INFORMATON FOR NUDGING FROM FILE "SPEED" WITH DIMENSION(5,1488)
!98          !   u74,u33,u16,u8 - wind speed measured at 74, 33.6, 16.8 and 8.4 m
!99          !   b23 - TKE recalculated from U* measured at 23.3 m 
!100         u74=speed(1,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(speed(1,nxodrad+1)-speed(1,nxodrad))/1800.
!101         u33=speed(2,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(speed(2,nxodrad+1)-speed(2,nxodrad))/1800.
!102         u16=speed(3,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(speed(3,nxodrad+1)-speed(3,nxodrad))/1800.
!103         u8=speed(4,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(speed(4,nxodrad+1)-speed(4,nxodrad))/1800.
!104         !bt23=( speed(5,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(speed(5,nxodrad+1)-speed(5,nxodrad))/1800.)**2/(cc2**0.5)   
!
!105         !  INFORMATON FOR NUDGING FROM FILE "TEM" WITH DIMENSION(6,1488)
!106         !   ta67,ta50,ta33,ta16,ta8 and ta4 - temperature measured at 67.2, 50.4, 33.6, 16.8, 8.4 and 4.2 m
!
!107         ta67=tem(1,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(tem(1,nxodrad+1)-tem(1,nxodrad))/1800.  !+1.8 !year 2050
!108         ta50=tem(2,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(tem(2,nxodrad+1)-tem(2,nxodrad))/1800.  !+1.8 !year 2050 
!109         ta33=tem(3,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(tem(3,nxodrad+1)-tem(3,nxodrad))/1800.  !+1.8 !year 2050
!110         ta16=tem(4,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(tem(4,nxodrad+1)-tem(4,nxodrad))/1800.  !+1.8 !year 2050
!111         ta8=tem(5,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(tem(5,nxodrad+1)-tem(5,nxodrad))/1800.   !+1.8 !year 2050
!112         ta4=tem(6,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(tem(6,nxodrad+1)-tem(6,nxodrad))/1800.   !+1.8 !year 2050
!
!113         !  INFORMATON FOR NUDGING FROM FILE "HUM" WITH DIMENSION(6,1488)
!114         !   qa67,qa50,qa33,qa16,qa8 and qa4 - moisture measured at 67.2, 50.4, 33.6, 16.8, 8.4 and 4.2 m
!
!115         qa67=hum(1,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(hum(1,nxodrad+1)-hum(1,nxodrad))/1800.
!116         qa50=hum(2,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(hum(2,nxodrad+1)-hum(2,nxodrad))/1800.
!117         qa33=hum(3,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(hum(3,nxodrad+1)-hum(3,nxodrad))/1800.
!118         qa16=hum(4,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(hum(4,nxodrad+1)-hum(4,nxodrad))/1800.
!119         qa8=hum(5,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(hum(5,nxodrad+1)-hum(5,nxodrad))/1800.
!120         qa4=hum(6,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(hum(6,nxodrad+1)-hum(6,nxodrad))/1800.
!
!121      endif
!
!122      !r soil data: linearly interpolating to current time step
!123      !r hyy_soil(2,:)  Volumetric water content (m3/m3) in organic layer in -5-0 cm
!124      surf_soil_hum = hyy_soil(2,nxodrad)+(tmcontr-(nxodrad-1)*1800.)*(hyy_soil(2,nxodrad+1)-hyy_soil(2,nxodrad))/1800.
!     
!     
!125      rads2=rskt+rsnt
!
!126      if(time.le.upsn) rsnt=0.
!127      if(time.le.upsn) rskt=0.
!128      if(time.ge.downsn) rsnt=0.
!129      if(time.ge.downsn) rskt=0.
!130      if(time.le.upsn) zenit=1.
!131      if(time.le.upsn) zenit=1.
!132      if(time.ge.downsn) zenit=1.
!133      if(time.ge.downsn) zenit=1.
!134      if(rads2.lt.10.) zenit = 1.
!
!135      ttime=ttime+1
!
!136      do k=1,kz    ! loop 223
!        !
!137         if(nsytki.gt.2) go to 23909
!
!138         bt(k)=(bt(k)+bt1(k))/2.
!139         wg(1)=(wg(1)+wg(1))/2.
!140         wg(2)=(wg(2)+wg(2))/2.
!141         tsoil(k)=(tsoil(k)+tsoil1(k))/2.
!        !     
!142         u(k)=(u(k)+u1(k))/2.
!143         kt(k)=(kt(k)+kt1(k))/2.
!144         w(k)=(w(k)+w1(k))/2.
!145         v(k)=(v(k)+v1(k))/2.
!146         ta(k)=(ta(k)+ta1(k))/2.
!147         qa(k)=(qa(k)+qa1(k))/2.
!148         tsn(k)=(tsn(k)+tsn1(k))/2.
!149         tsd(k)=(tsd(k)+tsd1(k))/2.
!150         alt(k)=(alt(k)+alt1(k))/2.
!        !
!151         !      go to 2399 
!152 23909   continue      
!153         if(nsytki.lt.2) go to 2399
!154         wg(1)=wg1(1)
!155         wg(2)=wg1(2)
!156         tsoil(k)=tsoil1(k)
!        !
!157         l(k)=l1(k)
!158         bt(k)=bt1(k)
!159         dbt(k)=dbt1(k)
!160         u(k)=u1(k)
!161         kt(k)=kt1(k)
!162         ta(k)=ta1(k)
!163         w(k)=w1(k)
!164         v(k)=v1(k)
!165         alt(k)=alt1(k)
!166         tsn(k)=tsn1(k)
!167         tsd(k)=tsd1(k)
!168         qa(k)=qa1(k)
!169         qa(k)=min(qa(k), (qv0*exp(maga*(ta1(k)-t273)/(magb+ta1(k)-t273)) /ta1(k)) )
!
!
!170 2399    continue
!171         qsn(k)=max(qa(k), qv0*exp(maga*(tsn(k)-t273)/(magb+tsn(k)-t273)) /tsn(k))
!172         qsd(k)=max(qa(k), qv0*exp(maga*(tsd(k)-t273)/(magb+tsd(k)-t273)) /tsd(k) )
!173         if(qa(k).ge.qsn(k)) qa(k)=qsn(k)
!
!174      enddo    ! end od loop 223
!
!
!
!175      cor1=0.
!176      c_nud=0.
!
!177      if(abl.ne.1.) then
!
!178         cor1=1.
!179         c_nud=4.   !1.
!
!180         c_nud1= 0.01  ! 0.1    !CLEARCUT - IF WE DO NOT NUDGE, THEN THIS SHOULD BE ZERO - else nudging factor is 0.01
!
!
!181         do k=1,kz
!
!182            !    NO ANY NUDGING YET
!183            tnud(k)=ta(k)
!184            qnud(k)=qa(k)
!185            unud(k)=u(k)
!186            vnud(k)=v(k)
!187            bnud(k)=bt(k)
!
!188            !    DEFINE NUDGING VARIABLES BY SIMPLE LINEAR APPROXIMATION
!
!189            !   TEMPERATURE and MOISTURE
!190            if(z(k).gt.50.4.and.z(k).le.67.2) then
!191               tnud(k)=ta50+(ta67-ta50)*(z(k)-50.4)/(67.2-50.4)
!192               qnud(k)=qa50+(qa67-qa50)*(z(k)-50.4)/(67.2-50.4)
!193            endif
!
!194            if(z(k).gt.33.6.and.z(k).le.50.4) then
!195               tnud(k)=ta33+(ta50-ta33)*(z(k)-33.6)/(50.4-33.6)
!196               qnud(k)=qa33+(qa50-qa33)*(z(k)-33.6)/(50.4-33.6)
!197            endif
!
!198            if(z(k).gt.16.8.and.z(k).le.33.6) then
!199               tnud(k)=ta16+(ta33-ta16)*(z(k)-16.8)/(33.6-16.8)
!200               qnud(k)=qa16+(qa33-qa16)*(z(k)-16.8)/(33.6-16.8)
!201            endif
!
!202            if(z(k).gt.8.4.and.z(k).le.16.8) then
!203               tnud(k)=ta8+(ta16-ta8)*(z(k)-8.4)/(16.8-8.4)
!204               qnud(k)=qa8+(qa16-qa8)*(z(k)-8.4)/(16.8-8.4)
!205            endif
!
!206            if(z(k).gt.4.2.and.z(k).le.8.4) then
!207               tnud(k)=ta4+(ta8-ta4)*(z(k)-4.2)/(8.4-4.2)
!208               qnud(k)=qa4+(qa8-qa4)*(z(k)-4.2)/(8.4-4.2)
!209            endif
!
!           !   WIND SPEED
!
!210            goto 600 
!211            if(z(k).gt.33.6.and.z(k).le.74.) then
!212               unud(k)=u33+(u74-u33)*(z(k)-33.6)/(74.-33.6)
!213               vnud(k)=0.
!214            endif
!
!215            if(z(k).gt.16.8.and.z(k).le.33.6) then 
!216               unud(k)=u16+(u33-u16)*(z(k)-16.8)/(33.6-16.8)
!217               vnud(k)=0.
!218            endif
!
!219            if(z(k).gt.8.4.and.z(k).le.16.8) then
!220               unud(k)=u8+(u16-u8)*(z(k)-8.4)/(16.8-8.4)
!221               vnud(k)=0.
!222            endif
!223 600        continue
!
!224         enddo
!
!225         zini(1)=0.
!226         zini(2)=8.4
!227         zini(3)=16.8
!228         zini(4)=36.6
!229         zini(5)=74.
!230         zini(6)=3000.
!
!231         fini(1)=0.0
!232         fini(2)=u8
!233         fini(3)=u16
!234         fini(4)=u33
!235         fini(5)=u74
!236         fini(6)=windx
!
!237         CALL inter3(zini,fini,6,z,kz,ffn)
!238         do k=1,kz
!
!239            if(z(k).gt.8.4.and.z(k).le.75.) then
!240               unud(k)=ffn(k)
!241               vnud(k)=0.
!242            endif
!
!243         enddo
!
!244      endif
!
!
!
!245      !--------------------------------------------------------------
!246      !   solution of U and V - velocity component equations
!247      !---------------------------------------------------------------
!

     do k=2,kz-1   ! loop 93                                                                                                        !248


        fktt=2.*(df(k)*kt(k+1)+(1.-df(k))*kt(k))                                                                                    !249
        fktd=2.*((1.-df(k-1))*kt(k-1)+df(k-1)*kt(k))                                                                                !250

        a(k)= dt*(fktt-w(k)*dz(k))/da(k)                                                                                            !251

        b1(k)= dt*((dz(k)*fktt+ dz(k+1)*fktd) /(dz(k+1)+dz(k)) +w(k)*(dz(k+1)-dz(k)))/db(k)+ &                                      !252
             dt*(s(k)*sqrt(u(k)**2 +v(k)**2+w(k)**2)) +ft                                                                           !253

        c(k)= dt*(fktd+w(k)*dz(k+1))/dc(k)                                                                                          !254

        !d1(k)= u(k) + dt*(cor1*f1*(v(k)-windy)) -24.*dt*(u(k)-windx)/c864       &                                                  !255
        !     *c_nud*(0.5+0.5*sin(0.5*pi*(2.*z(k)/z(kz)-1.)))&                                                                      !256
        !     -(u(k)-unud(k))*c_nud1   ! nudging                                                                                    !257

        d1(k)= u(k) + dt*(cor1*f1*(v(k)-windy)) - dt*(u(k)-windx)/c864       &                                                      !258
             *c_nud   &                                                                                                             !259
        ! *(0.5+0.5*sin(0.5*pi*(2.*z(k)/z(kz)-1.)))&                                                                                !260
        -(u(k)-unud(k))*c_nud1     ! nudging                                                                                        !261


        !d2(k)= v(k)+ dt*(-cor1*f1*(u(k)-u(kz)))-24.*dt*(v(k)-0.)/c864   &                                                          !262
        !     *c_nud*(0.5+0.5*sin(0.5*pi*(2.*z(k)/z(kz)-1.)))&                                                                      !263
        !     -(v(k)-vnud(k))*c_nud1       ! nudging                                                                                !264


        d2(k)= v(k)+ dt*(-cor1*f1*(u(k)-windx)) - dt*(v(k)-0.)/c864   &                                                             !265
             *c_nud                                                                                                                 !266
        !  *(0.5+0.5*sin(0.5*pi*(2.*z(k)/z(kz)-1.))) &                                                                              !267
        !         -(v(k)-vnud(k))*c_nud1     ! nudging                                                                              !268
     enddo !    end of loop 93                                                                                                      !269

    !old      call gtri(a,c,b1,d1,u1,1,0.d0,3.d0,2,0.*windx,3.d0,kz,2)                                                              !270
     call gtri(a,c,b1,d1,u1,1,0.d0,3.d0,1,1.*windx,3.d0,kz,2)  !new (not much difference in the results)                            !271
     call gtri(a,c,b1,d2,v1,1,0.d0,3.d0,1,windy,3.d0,kz,2)                                                                          !272

     !----------------------------------------------------------                                                                    !273
     !  solution of the heat and moisture equations                                                                                 !274
     !----------------------------------------------------------                                                                    !275

     do k=2,kz-1   ! loop 950                                                                                                       !276

        !old  dkz1=diff(kt,kz,dz,k)                                                                                                 !277
        !old daz=diff(alt,kz,dz,k)                                                                                                  !278

        !old  dkz=(daz*kt(k)+dkz1*alt(k))                                                                                           !279

        fktt=2.*(df(k)*alt(k+1)*kt(k+1) +(1.-df(k))*alt(k)*kt(k))                                                                   !280
        fktd=2.*((1.-df(k-1))*alt(k-1)*kt(k-1) +df(k-1)*alt(k)*kt(k))                                                               !281

        dkz=(fktt-fktd)/(dz(k)+dz(k+1))     !new                                                                                    !282

        a(k)= dt*(fktt-w(k)*dz(k))/da(k)                                                                                            !283
        c(k)= dt*(fktd+w(k)*dz(k+1))/dc(k)                                                                                          !284

        b1(k)= dt*((dz(k)*fktt+ dz(k+1)*fktd) /(dz(k+1)+dz(k)) +w(k)*(dz(k+1)-dz(k)))/db(k)+ &                                      !285
               face*dt*s1(k)*(psn(k)*dhsn(k)+(1.-psn(k))*dhsd(k))+ ft                                                               !286


    !        d1(k)= ta(k) + dt*(face*s1(k)*(psn(k)*dhsn(k)*tsn(k)+ (1.-psn(k))*dhsd(k)*tsd(k)) +gamma*dkz-w(k) *gamma )    &        !287
    !             +24*dt*(tax+temgrad*(z(kz)-z(k))-ta(k))/(c864)       &                                                            !288
    !             *c_nud*(0.5+0.5*sin(0.5*pi*(2.*z(k)/z(kz)-1.)))        &                                                          !289
    !             -(ta(k)-tnud(k))*c_nud1 ! nudging                                                                                 !290
        d1(k)= ta(k) + dt*(face*s1(k)*(psn(k)*dhsn(k)*tsn(k)+ (1.-psn(k))*dhsd(k)*tsd(k)) +gamma*dkz-w(k) *gamma )    &
             + c_nud*dt*(tax+temgrad*(z(kz)-z(k))-ta(k))/(c864)       &
                                !       *(0.5+0.5*sin(0.5*pi*(2.*z(k)/z(kz)-1.)))        &
             -(ta(k)-tnud(k))*c_nud1 ! nudging , for temperature

        !  2 - for moisture      
        b2(k)= dt*((dz(k)*fktt+ dz(k+1)*fktd) /(dz(k+1)+dz(k)) +w(k)*(dz(k+1)-dz(k)))/db(k)+  &
               face*dt*s1(k)*(psn(k)*dvsn(k)+(1.-psn(k))*dvsd(k))+ ft
        !
        !  dvsn - dhsn; dvsd - dhsd; qsn - tsn; qsd - qsn
        !
!        d2(k)= qa(k) + dt*(face*s1(k)*(psn(k)*dvsn(k)*qsn(k)+ (1.-psn(k))*dvsd(k)*qsd(k))) &
!             -24.*dt*(qa(k)-qax)/(c864)*c_nud*(0.5+0.5*sin(0.5*pi*(2.*z(k)/z(kz)-1.))) &
!             -(qa(k)-qnud(k))*c_nud1                        ! nudging 
        d2(k)= qa(k) + dt*(face*s1(k)*(psn(k)*dvsn(k)*qsn(k)+ (1.-psn(k))*dvsd(k)*qsd(k))) &
             - dt*(qa(k)-qax)/(c864)*c_nud*(0.5+0.5*sin(0.5*pi*(2.*z(k)/z(kz)-1.))) &
             -(qa(k)-qnud(k))*c_nud1                        ! nudging 

     enddo    ! end of loop 950


     call gtri(a,c,b1,d1,ta1,1,ta(1),3.d0,1,tax,3.d0,kz,2)

     call gtri(a,c,b2,d2,qa1,1,qa(1),3.d0,1,qax,3.d0,kz,2)


     !--------------------------------------------------------------------
     !c      
     !c      Estimation of Ri number                     
     !c      
     !c--------------------------------------------------------------------

!****new part starts ****

     !do k=1,kz
     !   f2(k) = kt(k) * prod1(k)+vtran(k)
     !enddo


     !dtree = 0.
     !do k = 2, kz
     !   if (z(k).le. z(nz)) then
     !    
     !      dtree = dtree + 0.5*(z(k) -z(k-1)) *(f2(k-1) +f2(k)) / f2(nz) / dh
     !   endif
     !enddo

     !ddd1 = max(0., (1-dtree)*dh)

!**** new part ends ****

    do k=2,kz-1    ! loop 9033
        
        dtz1=diff(ta1,kz,dz,k)+gamma
        dqz1=diff(qa1,kz,dz,k)
        duz1=diff(u1,kz,dz,k)
        dvz1=diff(v1,kz,dz,k)
        dwz1=diff(w1,kz,dz,k)


!        if(k .le. 2) then   ! old
!           duz1=0.4*u1(2)/log(z(2)/z0)/l(k)
!           dvz1=0.4*v1(2)/log(z(2)/z0)/l(k)
!           dtz1=0.4*(ta1(2)-ta1(1)+gamma*dz(2))/log(z(2)/z0)/l(k)
!           dqz1=0.4*(qa1(2)-qa1(1))/log(z(2)/z0)/l(k)
!        endif


        if(k .le. 2) then  ! new
           duz1=ur(2)/l(2)
           dvz1=0. 
           dtz1=(((ta1(3)-ta1(1))*dz(2)**2   &
                +(ta1(1)-ta1(2))*(dz(2)+dz(3))**2))/   &
                ( (dz(2)+dz(3))*dz(2)**2 -              &
                dz(2)*(dz(2)+dz(3))**2  )+gamma
           dqz1=(((qa1(3)-qa1(1))*dz(2)**2  &
                +(qa1(1)-qa1(2))*(dz(2)+dz(3))**2))/  &
                ( (dz(2)+dz(3))*dz(2)**2 -   &
                dz(2)*(dz(2)+dz(3))**2  )      
        endif

        !**** old ****
        !utt=(duz1**2+dvz1**2)
        !if (utt .eq. 0) utt = 1E-15
        !if(utt.ne.0.) rih(k)=(g1*((dtz1  )/ (ta1(k)+gamma*z(k)) + 1.*0.608*dqz1/roa ) ) / utt
        !**** old ****

!****new part starts ****

        !abb=min(1.1,l(k)/al1)
        abb=abs(alt(k)*rih(k))/(1.+abs(alt(k)*rih(k)))    ! (neutral case =0) <abb < 1, parameter related to convectivity, related to canopy (bounce or                                                          !  mechanic production)
           
        !abb1=(max(0.,1.-(kt(k)*prod1(k)+1.*vtran(k)) &
        !     /(kt(k)*prod2(k)+kt(k)*prod1(k)+1.*vtran(k)) )   )
        

    !if(z(k).lt.ddd1) then
    !       abb1=(max(0.,1.-(kt(k)*prod1(k)+0.*vtran(k)) &
    !            /(kt(k)*prod2(k)+kt(k)*prod1(k)+0.*vtran(k)) )   )
        !endif

        a005=2.     ! const value for neutral case
        a0005=0.

        abb3=abb
        rih1(k)=(a0005-(a0005+1./(1.-c52/c833))*abb3)
        !if(z(k).le.ddd1) then 
        !   abb3=abb1
        !   a0005=2.
        !   rih1(k)=(a0005-(a0005+1./(1.-c52/c833))*abb3)
        !endif
    if(dtz1.ge.0.) then

           abb3=a005*alt(k)*rih(k)
           rih1(k)=a005

    endif

        utt=max((duz1**2+dvz1**2)  &
             !+bt2(k)/kt(k)  &
             !+2.*det2(k)/kt(k)   &
             +abs(1.*rih1(k)*( alt(k)*g1*((dtz1  )/   &
             (ta1(k)+gamma*z(k))   &
             + 0.608*dqz1/roa)))  &
             ,0.0000001)

!    if(utt.gt.0.) then 
           rih(k)=(   g1*((dtz1  )/(ta1(k)+gamma*z(k) )  + 0.608*dqz1/roa ) ) / utt
!    else
!           rih(k)=0.
!    endif
        

!**** new part ends ****
 
        rih(1)=0.
        rih(kz)=0.

!        if(rih(k).gt.0.) then
!           alt1(k)=1.35/(1.+1.35*rih(k))

        if(rih(k).ge.0.) then !new gt -> ge
           alt1(k)=1.35/(1.+a005*1.35*rih(k))        !new 1.35 -> a005
        else
           alt1(k)=1.35*(1.- 15.*rih(k))**0.25
        endif

    enddo ! 9033

     !--------------------------------------------------------------------
     !      solution of the turbulent kinetic energy & dissipation rate equation
     !--------------------------------------------------------------------


     do k=2,kz-1    ! loop 933
        !
        dtz1=diff(ta1,kz,dz,k)+gamma
        dqz1=diff(qa1,kz,dz,k)
        duz1=diff(u1,kz,dz,k)
        dvz1=diff(v1,kz,dz,k)
        dwz1=diff(w1,kz,dz,k)


!        if(k.le.2) then  !old
!           duz1=0.4*u1(2)/log(z(2)/z0)/l(k)
!           dvz1=0.4*v1(2)/log(z(2)/z0)/l(k)
!           dtz1=0.4*(ta1(2)-ta1(1)+gamma*dz(2))/log(z(2)/z0)/l(k)
!           dqz1=0.4*(qa1(2)-qa1(1))/log(z(2)/z0)/l(k)
!        endif

        if(k.le.2) then   !new
           duz1=ur(2)/l(2) ! new
           dvz1=0.        ! new

           dtz1=(((ta1(3)-ta1(1))*dz(2)**2   &
                +(ta1(1)-ta1(2))*(dz(2)+dz(3))**2))/   &
                ( (dz(2)+dz(3))*dz(2)**2 -              &
                dz(2)*(dz(2)+dz(3))**2  )+gamma

           dqz1=(((qa1(3)-qa1(1))*dz(2)**2  &
                +(qa1(1)-qa1(2))*(dz(2)+dz(3))**2))/  &
                ( (dz(2)+dz(3))*dz(2)**2 -   &
                dz(2)*(dz(2)+dz(3))**2  )      
        endif

        !abb=min(1.1,l(k)/al1)
        prod1(k) = duz1**2. + dvz1**2.        
        prod2(k) = -1.*(alt(k)*g1*(dtz1/(ta1(k) +gamma*z(k)) + 0.608*dqz1/roa))
        
        abb=abs(alt1(k)*rih(k))/(1.+abs(alt1(k)*rih(k)))    ! (neutral case =0) <abb < 1, parameter related to convectivity, related to canopy (bounce or                                                          !  mechanic production)
        

        !abb1=(max(0.,1.-(kt(k)*prod1(k)+1.*vtran(k)) &
        !     /(kt(k)*prod2(k)+kt(k)*prod1(k)+1.*vtran(k)) )   )

    !if(z(k).lt.ddd1) then
    !       abb1=(max(0.,1.-(kt(k)*prod1(k)+0.*vtran(k)) &
    !            /(kt(k)*prod2(k)+kt(k)*prod1(k)+0.*vtran(k)) )   )
!
 !       endif
        abb3 = abb
        cc22=c52+(c833-c52)*abb3
        cc33=c833
        !a0005 = 0.
        rih1(k)=(a0005-(a0005+1./(1.-c52/c833))*abb3)


        !if(z(k) .le. ddd1) then
        !   abb3 = abb1
        !   a0005 = 2.
        !   rih1(k)=(a0005-(a0005+1./(1.-c52/c833))*abb3)
        !   cc22=c52+(c833-c52)*abb3
        !endif
        beta=(c52-c833)*rih1(k)


        if(rih(k).ge.0.) then
           abb3 = a005*alt(k)*rih(k)
           rih1(k) = a005
           cc22=c52+(c833-c52)*abb3
           cc33 = c833
           beta=(c52-c833)*rih1(k)
        endif
        
        c15=(12.*cc2**0.5)*s(k)*( sqrt(u1(k)**2+v1(k)**2)) ! if c16 in use, 10*cc2 is used for c15, if c16 not in use, then 12*c22 is used.  
        
        bt2(k) = (s(k) * (sqrt(u1(k)**2 + v1(k)**2))**3)*(0. + abb3**0.5)            

!**** new part ends ****


        fktt=2.*alf*(df(k)*kt(k+1)+(1.-df(k))*kt(k))
        fktd=2.*alf*((1.-df(k-1))*kt(k-1)+df(k-1)*kt(k))

        if(k.le.2) then    ! new
           fktt=2.*alf*sqrt(kt(k+1)*kt(k)) !new   ! geometrical mean provides the smallest weight possible, according to Andrey most stable
           fktd=2.*alf*sqrt(kt(k-1)*kt(k))  ! new
           
        endif    ! new


        !cc22=(c52+(c833-c52)*l(k)/al1)

        !c15=(12.*cc2**0.5)*s(k)*( sqrt(u1(k)**2+v1(k)**2)+sqrt(u1(k)**2+v1(k)**2))/2.   
        !cc33=(c833-(c833-c52)*(c15)/dbt(k))

        !beta0=(cc22-c833)


        ! Andrey new addings on 2013.09.17

        vtran(k) = bt(k+1) * fktt/da(k)    - bt(k)*(dz(k)*fktt + dz(k+1)*fktd)/(dz(k+1) + dz(k)) /db(k) + bt(k-1)*fktd/dc(k)


        det2(k) = c15 * bt(k)



        a(k)= dt*(fktt -w(k)*dz(k))/da(k)
        b(k)= dt*((dz(k)*fktt+ dz(k+1)*fktd) /(dz(k+1)+dz(k)) +w(k)*(dz(k+1)-dz(k)))/db(k)+ ft+dt*dbt(k)
        c(k)= dt*(fktd+ w(k)*dz(k+1))/dc(k)
        !old: d(k)= bt(k) + dt*kt(k) *(duz1**2+dvz1**2+dwz1**2-alt1(k)*g1*(dtz1/(ta1(k)+gamma*z(k)) + 1.*0.608*dqz1/roa))-0.*(bt(k)-bnud(k))*c_nud !nudging!
        d(k)= bt(k) + dt*kt(k)*(prod1(k)+prod2(k)) 

        !old: bg(k)= dt*((dz(k)*fktt+ dz(k+1)*fktd) /(dz(k+1)+dz(k)) +w(k)*(dz(k+1)-dz(k)))/db(k)+ 2.*dt*cc33*dbt(k)+ft 
        bg(k)= dt*((dz(k)*fktt+ dz(k+1)*fktd) /(dz(k+1)+dz(k)) +w(k)*(dz(k+1)-dz(k)))/db(k)+ 2.*dt*cc33*dbt(k)+ft &
             -1.*dt*(c52-cc33)*bt2(k)/bt(k) + 1.*dt*(c52-cc33)*c15

        !old : dg(k)= dbt(k) +dt*cc33*dbt(k)**2 +dt*cc22*cc2 *(duz1**2+dvz1**2+dwz1**2) &
        !     -beta0*dt*cc2*(1.*alt1(k)*g1*(dtz1/(ta1(k)+gamma*z(k))+ 1.*0.608*dqz1/roa ) ) 
       dg(k)= dbt(k) + dt*cc33*dbt(k)**2 + dt*cc2*(cc22*prod1(k) +beta*prod2(k))

    enddo  ! end of loop 933

     if(abl.eq.1.) then
        bttop=temgrad**2/(cc2**0.5)
        !old CALL gtri(a,c,b,d,bt1,2,0.d0,3.d0,1,bttop,3.d0,kz,2)
        CALL gtri(a,c,b,d,bt1,2,0.d0,3.d0,2,0.d0,3.d0,kz,2)
    else

        btbot=ur(1)**2/(cc2**0.5) ! new
        !write(*,*) 'before bt1(kz) =', bt1(kz)
        !old: CALL gtri(a,c,b,d,bt1,2,0.d0,3.d0,2,0.d0,3.d0,kz,2)
        CALL gtri(a,c,b,d,bt1,1,btbot,3.d0,2,0.d0,3.d0,kz,2)  ! new

     endif

!tke_limit = 1.d-5  ! 1.d-2 in manitou
     do k=1,kz !-1
        !old bt1(k)=max(bt1(k),1.d-5)
        bt1(k)=max(bt1(k),1.d-2) !new   
     enddo


     !old CALL gtri(a, c, bg, dg, dbt1, 1, (cc2**(0.75)*bt1(2)**0.5/l(1)), 3.d0, 1, 1.*(cc2**(0.75)*bt1(kz)**0.5/l(kz)), 3.d0, kz, 2)
     CALL gtri(a, c, bg, dg, dbt1, 1, (cc2**(0.75)*btbot**0.5/l(1)), 3.d0, 2, 0.d0, 3.d0, kz, 2)  !new

     ! limiting disipational rate related to eddy    
    do k=1,kz
        dbt1(k) = max(dbt1(k), cc2**0.75*(bt1(k))**0.5/(al1)) 
     enddo

     !-------------------------------------------------------------
     !    solution for ABL height
     !-------------------------------------------------------------
     !write(*,*) 'marker 10'

     do k=2,kz   !  loop 1320


        if(k.gt.nz.and.rih(k).ge.0.25)  hlang=z(k)

        if(k.gt.nz.and.rih(k).ge.0.25) then 
           go to 13201
        endif

     enddo ! end of loop 1320

13201 continue

     hlayer=max(0.d0,hlang)

 !    if(abl.ne.1.) al1=max(1.*0.00027*abs(windx)/abs(f1),5.d0)
     !----------------------------------------------------------
     !  relation for the eddy exchange coefficient of momentum
     !----------------------------------------------------------
     
!old     kt1(1)=max(cc2*bt1(1)/dbt1(1),1.d-3)
     kt1(1)=max(cc2*bt1(1)/dbt1(1),ur(1)*l(1)) !new  
     kt1(kz)=max(cc2*bt1(kz)/dbt1(kz),kt1(1))   
 !dublicate    l1(1)=l(1)  !new
     l1(kz)=max(sqrt(bt1(kz))*cc2**0.75/dbt1(kz),l1(1))

     do k=2,kz-1   !  loop 291
        gf=abs(bt1(k))
        gf2=dbt1(k)
        kt1(k)=max(cc2*gf/gf2,kt1(1))
        l1(k)=max(sqrt(gf)*cc2**0.75/gf2,l1(1))
     enddo  ! end of loop 291

     if(abl.eq.1.) then  
        do k=2,kz
           if(z(k).gt.dh) l1(k)=l1(k-1)+0.43*dz(k)
        enddo
     endif

     do  k=2,kz-1
        !old   fktt=((df(k)*kt1(k+1) +(1.-df(k))*kt1(k)))
        !old   fktd=(((1.-df(k-1))*kt1(k-1) +df(k-1)*kt1(k)))
        !old   ur(k)=0.5*(sqrt(fktt*abs(sqrt(u1(k+1)**2+v1(k+1)**2) - sqrt(u1(k)**2+v1(k)**2))/dz(k+1))+  &
        !old        sqrt(fktd*abs(sqrt(u1(k)**2+v1(k)**2) - sqrt(u1(k-1)**2+v1(k-1)**2))/dz(k)))
        
        
        
        pa1=(kt1(k+1)+kt1(k))*dz(k)/da(k)/2.  !new
        pa0=(kt1(k)+kt1(k-1))*dz(k+1)/dc(k)/2. !new
        
        akt_duz=(pa1*(u1(k+1)-u1(k))+pa0*(u1(k)-u1(k-1)))  !new
        
        akt_dvz=(pa1*(v1(k+1)-v1(k))+pa0*(v1(k)-v1(k-1)))  !new 
        
        ur(k)=(akt_duz**2+akt_dvz**2)**0.25   !new
        ! same thing, but speeds up calculations
     enddo

     !old ur(1)=ur(2)
     ur(1)=(ur(2)*(dz(2)+dz(3))**2-ur(3)*dz(2)**2)/( (dz(2)+dz(3))**2-dz(2)**2) !new cubic parameterization for first level (so that there is no gradient)
     ur(kz)=ur(kz-1)

!**** new part starts ****

     sb=0.
     szb=0.
     
     
     do k=2,kz
        szb=(sqrt(abs(bt1(k-1)-0.01))*z(k-1)+ &  ! 0.01 is the tke limit
        sqrt(abs(bt1(k)-0.01))*z(k))*   & ! 0.01 is the tke limit
        dz(k)/2.+szb  
         
        sb=(sqrt(abs(bt1(k-1)-0.01))+sqrt(abs(bt1(k)-0.01)))*  & ! 0.01 is the tke_limit
        dz(k)/2.+sb
     enddo
     !      al0=al1
     if (sb .eq. 0) then
        write(*,*) 'error: divided by zero. sb =', sb
     endif
          al100=0.00027*abs(windx)/abs(f1)  ! blackadar value for netural case
     al1=max(al100,0.075*(szb/sb) )  ! convective case

! szb/sb the relation of the integrals above 

!
!**** new part ends ****

     nturb=nturb+1

     !--------------------------------------------------------------
     !   solution of the soil transfer
     !---------------------------------------------------------------

     sand=0.000001*0.50
     sand=temtran  ! above defined temtran=0.000001*0.5, so in this case sand = temtran   ! thermal diffusivity 
     if(rou.le.0.0) sand=temtran   ! rou = 0 snow cover excluded for now
     snow=0.000001*0.27
     do  k=1,kz-2
        if(rou.ge.z(k)) kmix=max(1,k+1)  ! rou = 0 snow cover excluded for now, -> kmix = 1
     enddo
     nmix=int(z(kmix-1)/0.05)
     do k=2,kz-1
        ksnow(k)=sand                     ! thermal diffusivity of soil
        if(k.gt.10) ksnow(k)=snow         ! thermal diffusivity 
        if(k.gt.(10+nmix-1)) ksnow(k)=0.  ! thermal diffusivity 
     enddo
     DO k=2,10-1+nmix
        dksz=0.05*ksnow(k+1)/0.005- 0.05*ksnow(k-1)/0.005  ! depth of each layer is 0.05
        a(k)= dt*(2.*ksnow(k) +0.05*dksz)/0.005
        b(k)= dt*2.*ksnow(k) /0.0025+1.
        c(k)= dt*(2.*ksnow(k) -0.05*dksz)/0.005
        d(k)=tsoil(k)
     ENDDO
     e(1)=0.   ! constant temperature
     
     ! taking deep soil temperature from measurements (9  Temperature (K) in 23-60 cm)
     ts0 = hyy_soil(9,nxodrad)+(tmcontr-(nxodrad-1)*dt_obs)*(hyy_soil(9,nxodrad+1)-hyy_soil(9,nxodrad))/dt_obs
     ts0 = ts0 + 273.15  ! [C] -> [K]  
     
     f(1)=ts0  ! condition
     !f(1)=t273 ! condition

     m2=10-1+nmix

     DO m=2,m2
        den=b(m)-c(m)*e(m-1)
        f(m)=(d(m)+c(m)*f(m-1))/den  ! [K]
        e(m)=a(m)/den                ! [1]
     ENDDO



     tsoil1(10+nmix)=ta1(kmix-1)
     DO n=1,m2
        m=10-n+nmix
        tsoil1(m)=e(m)*tsoil1(m+1)+f(m)
     ENDDO

     do k=1,kz
        if(k.gt.10+nmix-1) tsoil1(k)=ta1(kmix-1)
     enddo

     !----------------------------------------------------------
     ! radiation blok
     !----------------------------------------------------------

     do k=1,kz-2
        if(rou.ge.z(k)) kmix=max(1,k+1)  ! dublicate, already done above   ! rou = 0 snow cover excluded for now, -> kmix = 1
     enddo


     DO k=1,kz
        sl(k)=s1(k)
     ENDDO

     DO k=2,kz
        gl(k)=1.
        gd(k)=1.
     ENDDO

     DO k=2,kz
        if((sl(k)-sl(k-1)).gt.0.) kf=k
        go to 4015
     ENDDO
4015 ks=max(kf,1)
     lai(1)=0.
     DO k=nz-1,1,-1
        jk=nz-k+1
        lai(jk)=lai(jk-1)+(sl(jk-1)+sl(jk))*dz(jk)/2.
     ENDDO

     lai0=lai(nz)
     do k=nz,kz
        lai(k)=lai0
     enddo
     asl=1.5
     DO k=nz,1,-1
        psn(k)=exp(0.5*(lai(k)-lai0)/zenit)
        gl(k)=0.5
        if(rads2.le.10.) gl(k)=zenit
        if(rads2.le.5.) gl(k)=0.
        if(rads2.le.10.) psn(k)=exp((lai(k)-lai0))
        if(su.eq.2) then
           psn(k)=exp((lai(k)-lai0))
           gl(k)=zenit
           if(rads2.le.5.) gl(k)=0.
        endif
     ENDDO

     dtet=pi/2./ntet
     teto=dtet/2.
     psk(nz)=1.


     DO ja=1,ntet
        tet(ja)=teto+dtet*(ja-1)
     ENDDO
     DO k =nz-1,1,-1
        DO ja=1,ntet
           cote=cos(tet(ja))
           site=sin(tet(ja))
           fl(ja)=2.*exp(gl(k)*(lai(k)-lai0)/cote)*cote*site
        ENDDO
        psk(k)=(fl(1)+fl(ntet))*dtet/2.
        DO ja=2,ntet
           psk(k)=psk(k )+(fl(ja-1)+fl(ja))*dtet/2.
           if(su.eq.2) then
              psk(k)=psn(k)
           endif
        ENDDO
     ENDDO

     DO k=nz,1,-1
        if((lai(k)-lai0).ne.0.and.rads2.gt.5.) gd(k)=zenit*log(psk(k))/ (lai(k)-lai0)
        if(su.eq.2) gd(k)=zenit
        if(rads2.le.5.) gd(k)=0.
     ENDDO
     DO k=2,kz-1
        asun(k)=dz(k)*psn(k+1)/da(k)+ (dz(k+1)-dz(k))*psn(k)/db(k)- dz(k+1)*psn(k-1)/dc(k)
        asky(k)=dz(k)*psk(k+1)/da(k)+ (dz(k+1)-dz(k))*psk(k)/db(k)- dz(k+1)*psk(k-1)/dc(k)
     ENDDO


     asun(1)=(psn(2)-psn(1))/dz(2)
     asky(1)=(psk(2)-psk(1))/dz(2)

     alph=0.80
     alni=0.80
     if(tau.gt.0.) then
        alph=alph-0.03*(dt/c864)
        alni=alni-0.03*(dt/c864)
     endif
     if(rou.gt.0.) then
        alph=max(1.d-1,alph)
        alni=max(1.5d-1,alni)
     endif
     if(rou.le.0.) then
        alph=0.10
        alni=0.15
     endif

     if(ta1(1).ge.t273) then
        alph=0.10
        alni=0.15
     else
        alph=0.50
        alni=0.50
     endif

     !alph = 0.125 !CLEARCUT: include this
     !alni = 0.225 !CLEARCUT: include this

     wee=1.
     tphu=0.06*(1.-sqrt( (1.-0.06)**2-0.09**2))/0.15
     tphd=0.06*(1.-sqrt( (1.-0.06)**2-0.09**2))/0.15
     rphu=0.09*(1.-sqrt( (1.-0.06)**2-0.09**2))/0.15
     rphd=0.09*(1.-sqrt( (1.-0.06)**2-0.09**2))/0.15

     tniu=0.40*(1.-sqrt( (1.-0.40)**2-0.35**2))/0.75
     tnid=0.40*(1.-sqrt( (1.-0.40)**2-0.35**2))/0.75
     rniu=0.35*(1.-sqrt( (1.-0.40)**2-0.35**2))/0.75
     rnid=0.35*(1.-sqrt( (1.-0.40)**2-0.35**2))/0.75

     if(su.eq.2) then
        tniu=0.50*(1.-sqrt( (1.-0.50)**2-0.35**2))/0.85 ! ross
        tnid=0.50*(1.-sqrt( (1.-0.50)**2-0.35**2))/0.85 ! ross
        rniu=0.35*(1.-sqrt( (1.-0.50)**2-0.35**2))/0.85 ! ross 
        rnid=0.35*(1.-sqrt( (1.-0.50)**2-0.35**2))/0.85 ! ross 
     endif
     phsn=0.4200
     phsk=0.60
     two=1.5
     tu=tphu
     td=tphd
     ru=rphu
     rd=rphd
     al=alph
     pasn=phsn
     pask=phsk
4026 continue
     DO k=1,nz
        fu0(k)=0.001
        fd0(k)=0.001
        fu1(k)=0.001
        fd1(k)=0.001
     ENDDO
4028 continue

     !   ------------------------------------------ haotic --------------

     zenit1=zenit
     fu1(1)=al*(fd0(1)+psn(1)*rsnt*pasn +psk(1)*rskt*pask)


     DO k=1,nz-1

        trem1=dz(k+1)*((1.*sl(k+1)+1.*sl(k))/2.)* (fd0(k+1)*ru-(wee-td)*fu1(k)+  &
             ru*(((psn(k)+psn(k+1))/2.)*rsnt*pasn*gl(k)/zenit +((psk(k)+psk(k+1))/2.)*rskt*pask*gd(k)/zenit))

        trem2=dz(k+1)*((1.*sl(k+1)+1.*sl(k))/2.)* (0.5*(fd0(k+1)+fd0(k))*ru-(wee-td)*(fu1(k)+trem1/2.)+  &
             ru*(((psn(k)+psn(k+1))/2.)*rsnt*pasn*gl(k)/zenit +((psk(k)+psk(k+1))/2.)*rskt*pask*gd(k)/zenit))

        trem3=dz(k+1)*((1.*sl(k+1)+1.*sl(k))/2.)* (0.5*(fd0(k+1)+fd0(k))*ru-(wee-td)*(fu1(k)+trem2/2.)+  &
             ru*(((psn(k)+psn(k+1))/2.)*rsnt*pasn*gl(k)/zenit +((psk(k)+psk(k+1))/2.)*rskt*pask*gd(k)/zenit))

        trem4=dz(k+1)*((1.*sl(k+1)+1.*sl(k))/2.)* (fd0(k+1)*ru-(wee-td)*(fu1(k)+trem3)+  &
             ru*(((psn(k)+psn(k+1))/2.)*rsnt*pasn*gl(k)/zenit +((psk(k)+psk(k+1))/2.)*rskt*pask*gd(k)/zenit))

        fu1(k+1)=fu1(k)+(trem1+2.*trem2+2.*trem3+trem4)/6.
     ENDDO

     DO k=nz,2,-1
        trek1=dz(k)*((1.*sl(k)+1.*sl(k-1))/2.)* (fu0(k-1)*rd-(wee-tu)*fd1(k)+  &
             td*(((psn(k-1)+psn(k))/2.)*rsnt*pasn *gl(k)/zenit +((psk(k-1)+psk(k))/2.)*rskt*pask*gd(k)/zenit))

        trek2=dz(k)*((1.*sl(k)+1.*sl(k-1))/2.)* (0.5*(fu0(k-1)+fu0(k))*rd-(wee-tu)*(fd1(k)+trek1/2.)+ &
             td*(((psn(k-1)+psn(k))/2.)*rsnt*pasn*gl(k)/zenit +((psk(k-1)+psk(k))/2.)*rskt*pask*gd(k)/zenit))

        trek3=dz(k)*((1.*sl(k)+1.*sl(k-1))/2.)* (0.5*(fu0(k-1)+fu0(k))*rd-(wee-tu)*(fd1(k)+trek2/2.)+  &
             td*(((psn(k-1)+psn(k))/2.)*rsnt*pasn*gl(k)/zenit +((psk(k-1)+psk(k))/2.)*rskt*pask*gd(k)/zenit))

        trek4=dz(k)*((1.*sl(k)+1.*sl(k-1))/2.)* (fu0(k-1)*rd-(wee-tu)*(fd1(k)+trek3)+  &
             td*(((psn(k-1)+psn(k))/2.)*rsnt*pasn*gl(k)/zenit +((psk(k-1)+psk(k))/2.)*rskt*pask*gd(k)/zenit))

        fd1(k-1)=fd1(k)+(trek1+2.*trek2+2.*trek3+trek4)/6.

     ENDDO

     DO k=1,nz
        IF(ABS(fu0(k)/fu1(k)-1.).GT.eps) go to 4033
     ENDDO
     DO k=1,nz-1
        IF(ABS(fd0(k)/fd1(k)-1.).GT.eps) go to 4033
     ENDDO
     !-----------------------------------------haotic------------------------
     go to 4035
4033 continue
     DO k=1,nz
        fu0(k)=fu1(k)
        fd0(k)=fd1(k)
     ENDDO
     go to 4028
4035 if(two.lt.2.) go to 4036
     go to 4038
4036 continue
     DO k=1,nz
        fphu(k)=fu1(k)
        fphd(k)=fd1(k)
     ENDDO
     tu=tniu
     td=tnid
     ru=rniu
     rd=rnid
     al=alni
     pasn=1.-phsn
     pask=1.-phsk
     two=two+1.
     go to 4026

4038 continue
     DO k=1,nz
        fniu(k)=fu1(k)
        fnid(k)=fd1(k)
     ENDDO

     iru0=(dels*sigm*ta1(kmix-1)**4+1.*(1.-dels)*fird(1))/pi ! net LWR at surface
     iruh=sigm*(ta1(nz  )**4)*emm/pi
     
     !r using reanalysis value instead
     iruh = LWRdown/pi   !CLEARCUT - comment this part out

     firu(1)=iru0*pi
     fird(nz)=iruh*pi
     DO k=1,nz
        fu0(k)=0.001
        fd0(k)=0.001
        fu1(k)=0.001
        fd1(k)=0.001
     ENDDO
40278 continue

     fu1(1)=iru0*pi
     fd1(nz)=iruh*pi

     DO k=1,nz-1

        trom1=dz(k+1)*((1.*sl(k)+1.*sl(k+1))/2.)*delf*(sigm*(psn(k)*(tsn(k)**4)+ (1.-psn(k))*(tsd(k)**4))-fu1(k))      

        trom2=dz(k+1)*((1.*sl(k)+1.*sl(k+1))/2.)*delf*(sigm*(0.5*(psn(k+1)*(tsn(k+1)**4) +psn(k)*(tsn(k)**4))+  &
             0.5*((1.-psn(k+1))*(tsd(k+1)**4) +(1.-psn(k))*(tsd(k)**4)))-(fu1(k)+trom1/2.))      

        trom3=dz(k+1)*((1.*sl(k)+1.*sl(k+1))/2.)*delf*(sigm*(0.5*(psn(k+1)*(tsn(k+1)**4) +psn(k)*(tsn(k)**4))+  &
             0.5*((1.-psn(k+1))*(tsd(k+1)**4) +(1.-psn(k))*(tsd(k)**4)))-(fu1(k)+trom2/2.)  )      

        trom4=dz(k+1)*((1.*sl(k)+1.*sl(k+1))/2.)*delf*(sigm*(psn(k+1)*(tsn(k+1)**4)+ (1.-psn(k+1))*(tsd(k+1)**4))-(fu1(k)+trom3) )      
        !
        fu1(k+1)=fu1(k)+(trom1+2.*trom2+2.*trom3+trom4)/6.

     ENDDO
     !    
     DO k=nz,2,-1

        tram1=dz(k)*((1.*sl(k)+1.*sl(k-1))/2.)*delf*(sigm*(psn(k)*(tsn(k)**4)+(1.-psn(k))*(tsd(k)**4))-fd1(k))      

        tram2=dz(k)*((1.*sl(k)+1.*sl(k-1))/2.)*delf*(sigm*(0.5*(psn(k-1)*(tsn(k-1)**4) +psn(k)*(tsn(k)**4))+  &
             0.5*((1.-psn(k-1))*(tsd(k-1)**4) +(1.-psn(k))*(tsd(k)**4)))-(fd1(k)+tram1/2.)  )      

        tram3=dz(k)*((1.*sl(k)+1.*sl(k-1))/2.)*delf*(sigm*(0.5*(psn(k-1)*(tsn(k-1)**4) +psn(k)*(tsn(k)**4))+  &
             0.5*((1.-psn(k-1))*(tsd(k-1)**4) +(1.-psn(k))*(tsd(k)**4)))-(fd1(k)+tram2/2.)  )      

        tram4=dz(k)*((1.*sl(k)+1.*sl(k-1))/2.)*delf*(sigm*(psn(k-1)*(tsn(k-1)**4)+ (1.-psn(k-1))*(tsd(k-1)**4))-(fd1(k)+tram3) )      

        fd1(k-1)=fd1(k)+(tram1+2.*tram2+2.*tram3+tram4)/6.

     ENDDO

     DO k=1,nz
        IF(ABS(fu0(k)/fu1(k)-1.).GT.eps) go to 40373

     ENDDO
     DO k=1,nz-1
        IF(ABS(fd0(k)/fd1(k)-1.).GT.eps) go to 40373
     ENDDO
     !-----------------------------------------haotic------------------------
     go to 40376

40373 continue

     DO k=1,nz
        fu0(k)=fu1(k)
        fd0(k)=fd1(k)
     ENDDO
     go to 40278
40376 continue
     DO k=1,nz
        firu(k)=fu1(k)
        fird(k)=fd1(k)
     ENDDO

     fird(nz)=iruh*pi
     firu(1)=iru0*pi


     nnna=1

     nnna=nnna+1
     pgr=0.6305
     pre=0.6056

     nua=0.0000141
     DO k=1,nz
        rlf=0.03
        if(su.eq.2) then
           rlf=0.05
        endif
        grsn(k)=g1*(pgr*rlf)**3*abs(tsn(k)-ta1(k))/(t273*nua**2)
        grsd(k)=g1*(pgr*rlf)**3*abs(tsd(k)-ta1(k))/(t273*nua**2)
        re(k)=sqrt(u1(k)**2+v1(k)**2)*pre*rlf/nua
     ENDDO

     DO k=1,nz
        if(re(k).ge.0..and.re(k).le.h(1)) nure(k)=h(2)*re(k)**h(3)
        if(re(k).gt.h(1).and.re(k).le.h(4)) nure(k)=h(5)*re(k)**h(6)
        if(re(k).gt.h(4)) nure(k)=h(7)*re(k)**h(8)
     ENDDO
     DO k=1,nz
        if(grsn(k).ge.0..and.grsn(k).le.h(10)) nusn(k)=h(3)
        if(grsn(k).gt.h(10).and.grsn(k).le.h(11)) nusn(k)=h(12)*grsn(k)**h(13)
        if(grsn(k).gt.h(11).and.grsn(k).le.h(14)) nusn(k)=h(15)*grsn(k)**h(16)
        if(grsn(k).gt.h(14)) nusn(k)=h(17)*grsn(k)**h(18)
     ENDDO
     DO k=1,nz
        if(grsd(k).ge.0..and.grsd(k).le.h(10)) nusd(k)=h(3)
        if(grsd(k).gt.h(10).and.grsd(k).le.h(11)) nusd(k)=h(12)*grsd(k)**h(13)
        if(grsd(k).gt.h(11).and.grsd(k).le.h(14)) nusd(k)=h(15)*grsd(k)**h(16)
        if(grsd(k).gt.h(14)) nusd(k)=h(17)*grsd(k)**h(18)
     ENDDO

     DO k=1,nz
        dhsn(k)=1.*kta*nusn(k)/(pgr*rlf)

        if((re(k)*re(k)).ge.grsn(k)) dhsn(k)=1.*kta*nure(k)/(pre*rlf)
     ENDDO

     DO k=1,nz
        dhsd(k)=1.*kta*nusd(k)/(pgr*rlf)
        if((re(k)*re(k)).ge.grsd(k)) dhsd(k)=1.*kta*nure(k)/(pre*rlf)
     ENDDO

     DO k=1,nz
        !
        if(re(k).ge.0..and.re(k).le.h(1)) shre(k)=h(19)*re(k)**h(3)
        if(re(k).gt.h(1).and.re(k).le.h(4)) shre(k)=h(20)*re(k)**h(6)
        if(re(k).gt.h(4)) shre(k)=h(21)*re(k)**h(8)
     ENDDO

     DO k=1,nz
        if(grsn(k).ge.0..and.grsn(k).le.h(23)) shsn(k)=h(3)
        if(grsn(k).gt.h(23).and.grsn(k).le.h(24)) shsn(k)=h(25)*grsn(k)**h(13)
        if(grsn(k).gt.h(24).and.grsn(k).le.h(26)) shsn(k)=h(27)*grsn(k)**h(16)
        if(grsn(k).gt.h(26)) shsn(k)=h(28)*grsn(k)**h(18)
     ENDDO

     DO k=1,nz
        if(grsd(k).ge.0..and.grsd(k).le.h(23)) shsd(k)=h(3)
        if(grsd(k).gt.h(23).and.grsd(k).le.h(24)) shsd(k)=h(25)*grsd(k)**h(13)
        if(grsd(k).gt.h(24).and.grsd(k).le.h(26)) shsd(k)=h(27)*grsd(k)**h(16)
        if(grsd(k).gt.h(26)) shsd(k)=h(28)*grsd(k)**h(18)
     ENDDO


     DO k=1,nz
        shsn(k)=kva*shsn(k)/(pgr*rlf)
        if((re(k)*re(k)).ge.grsn(k)) shsn(k)=kva*shre(k)/(pre*rlf)
     ENDDO


     DO k=1,nz
        shsd(k)=kva*shsd(k)/(pgr*rlf)
        if((re(k)*re(k)).ge.grsd(k)) shsd(k)=kva*shre(k)/(pre*rlf)
     ENDDO

     fniu(nz)=fniu(nz-1)
     fphu(nz)=fphu(nz-1)

     DO k=2,nz-1
        asun(k)=dz(k)*psn(k+1)/da(k)+ (dz(k+1)-dz(k))*psn(k)/db(k)- dz(k+1)*psn(k-1)/dc(k)
        asky(k)=dz(k)*psk(k+1)/da(k)+ (dz(k+1)-dz(k))*psk(k)/db(k)- dz(k+1)*psk(k-1)/dc(k)
        aphu(k)=dz(k)*fphu(k+1)/da(k)+ (dz(k+1)-dz(k))*fphu(k)/db(k)- dz(k+1)*fphu(k-1)/dc(k)
        aphd(k)=dz(k)*fphd(k+1)/da(k)+ (dz(k+1)-dz(k))*fphd(k)/db(k)- dz(k+1)*fphd(k-1)/dc(k)
        aniu(k)=dz(k)*fniu(k+1)/da(k)+ (dz(k+1)-dz(k))*fniu(k)/db(k)- dz(k+1)*fniu(k-1)/dc(k)
        anid(k)=dz(k)*fnid(k+1)/da(k)+ (dz(k+1)-dz(k))*fnid(k)/db(k)- dz(k+1)*fnid(k-1)/dc(k)
        airu(k)=dz(k)*firu(k+1)/da(k)+ (dz(k+1)-dz(k))*firu(k)/db(k)- dz(k+1)*firu(k-1)/dc(k)
        aird(k)=dz(k)*fird(k+1)/da(k)+ (dz(k+1)-dz(k))*fird(k)/db(k)- dz(k+1)*fird(k-1)/dc(k)
     ENDDO

     asun(1)=(psn(2)-psn(1))/dz(2)
     asky(1)=(psk(2)-psk(1))/dz(2)
     aphu(1)=(fphu(2)-fphu(1))/dz(2)
     aphd(1)=(fphd(2)-fphd(1))/dz(2)
     aniu(1)=(fniu(2)-fniu(1))/dz(2)
     anid(1)=(fnid(2)-fnid(1))/dz(2)
     airu(1)=(firu(2)-firu(1))/dz(2)
     aird(1)=(fird(2)-fird(1))/dz(2)

     face=2.7
     if(su.eq.1) then
        dmax=0.0026
        dmin=0.0000723
        topt=17.
        tmin=0.
        tmax=50.
        par50=20.
        humg=0.05
     endif

     if(su.eq.2) then
        dmax=0.0056
        dmin=0.0000723
        topt=18.
        tmin=0.
        tmax=50.
        par50=20.
        humg=0.05
        face=2.
     endif


     if(dh.eq.0.) then
        dmax=0.0056
        dmin=0.0000723
        topt=18.
        tmin=0.
        tmax=70.
        par50=50.
        humg=0.05      
     endif


     DO k=1,nz-1  ! loop 4063
        if(sl(k).eq.0.0) go to 4072
        rosn(k)=(wee-tphu-rphu)*( (phsn*rsnt*gl(k)/zenit) +(phsk*rskt*psk(k)*gd(k)/zenit+fphu(k)+fphd(k)))/face
        go to 4073
4072    rosn(k)=0.
4073    if(sl(k).eq.0.) go to 4074

        rosd(k)=rosn(k)

        rosd(k)=(wee-tphu-rphu)* (phsk*rskt*psk(k)*gd(k)/zenit+fphu(k)+fphd(k))/face

        go to 4075
4074    rosd(k)=0.
4075    continue
        if(rosn(k).le.0.) rosn(k)=0.
        if(rosd(k).le.0.) rosd(k)=0.
        tc=(tmax-topt)/(topt-tmin)

        rhg= qa1(1) /(qv0*exp(maga*(ta1(1)-t273)/(magb+ta1(1)-t273)) /ta1(1))

 ! by Sampo, to avoid division by zero
        if( (wgf-wgwilt) == 0 ) then
           defsoil = 1
        else
           defsoil=min(1.d0, ((1.0*wg(2)+0.*wg(1))-wgwilt)/(wgf-wgwilt))
        end if

        if(wg(2).le.wgwilt) defsoil=0. 

        if(rou.le.0.) goto 99

        if(ta1(k).lt.t273.or.tsd(k).le.t273) then
           defsoil=0.
        else
           defsoil = min(4.d-1,(ta1(k)-t273)**2.5/t273)

        endif
99      continue     
        temlim=max(0.d0,(1.-0.0016*(t273+18.-ta(k))**2))
        dvsn(k)= dmin+  &
             dmax*(1.-exp(-0.01*rosn(k)))  &
             * temlim 
        dvsd(k)= dmin +  &
             dmax*(1.-exp(-0.03*rosd(k)))  &
             * temlim
     ENDDO  ! end of loop 4063

     do k=2,nz-1
        dvsn(k)=(dvsn(k)*shsn(k)/(dvsn(k)+shsn(k)))
        dvsd(k)=(dvsd(k)*shsd(k)/(dvsd(k)+shsd(k)))
     enddo
     ! **************************************************
     DO k=1,nz  ! loop 4076
        rasn(k)=0.
        rasd(k)=0.
        goto 5252
        psn(k)=max(psn(k),1.d-2)
        !     
        !   ************************+++  method 2 +++*********************

    
    if(sl(k).ne.0.0)   &
             rasn(k)=        &
             (rsnt*asun(k)   &
             
             /psn(k)         &
             
             + (rskt*asky(k)-1.*aphu(k)+1.*aphd(k)-   &
             1.*aniu(k)+1.*anid(k)-1.*airu(k)+1.*aird(k)))  &
             /(sl(k))
        if(sl(k).ne.0.0)   &                                              
             rasd(k)=        &
             (rsnt*asun(k)*0. &
             +(rskt*asky(k)-1.*aphu(k)+1.*aphd(k)-    &        
             1.*aniu(k)+1.*anid(k)-1.*airu(k)+1.*aird(k)))   &
             /sl(k)
        !   ************************+++  method 2 +++*********************

        !  *************************+++  method 1  +++++++*************************
5252    continue   
        if(sl(k).ne.0.)  &
             rasn(k)=  & 
             ( ((wee-tniu-rniu)*(1.-phsn)+(wee-tphu-rphu)*phsn)  &
             
             *rsnt*gl(k)/zenit   &
             
             +( ((wee-tniu-rniu)*rskt*(1.-phsk)+  &
             (wee-tphu-rphu)*rskt*phsk)*psk(k)*gd(k)/zenit  &
             +(wee-tniu-rniu)*(fnid(k)+ fniu(k))  &
             +(wee-tphu-rphu)*(fphd(k)+fphu(k))  &
             +delf*(fird(k)+firu(k))             ))   
        if(sl(k).ne.0.)  &
             
             rasd(k)=  &
             ((((wee-tniu-rniu)*rskt*(1.-phsk)  &
             +(wee-tphu-rphu)*rskt*phsk)*psk(k)*gd(k)/zenit  &
             
             + (wee-tniu-rniu)*(fnid(k)+fniu(k))  &
             +(wee-tphu-rphu)*(fphd(k)+fphu(k))  &
             +delf*(fird(k)+firu(k))             ))

     ENDDO  ! end of loop 4076

     DO k=1,kz
        if(sl(k).eq.0.) tsn1(k )=ta1(k )
        if(sl(k).eq.0.) tsd1(k )=ta1(k )
     ENDDO

     DO k=1,nz-1
        fluxle(k)=fluxle(1)
        fluxh(k)=fluxh(1)

        if(sl(k).ne.0.)  &
             
             tsn1(k)=  &        !  with inertia in leaves i
             (rasn(k) +face*roa*cpa*dhsn(k)*ta1(k)  &
             + 3.*delf*2.*sigm*tsn(k)**4  &
             -face*lv*dvsn(k)*(qsn(k)-qa1(k))  &
             +ch2o*rh2o*hleaf*tsn(k)/dt )/  &
             (face*roa*cpa*dhsn(k)+4.*delf*2.*sigm*tsn(k)**3  &
             +ch2o*rh2o*hleaf/dt   )
        !
        if(sl(k).ne.0.)  &
             
             tsd1(k)=   &       !  with inertia in leaves i
             (rasd(k) +face*roa*cpa*dhsd(k)*ta1(k)  &
             + 3.*delf*2.*sigm*tsd(k)**4  &
             -face*lv*dvsd(k)*(qsd(k)-qa1(k))  &
             +ch2o*rh2o*hleaf*tsd(k)/dt )/  &
             ( face*roa*cpa*dhsd(k)+4.*delf*2.*sigm*tsd(k)**3  &
             +ch2o*rh2o*hleaf/dt   )  ! same as for surface temperature
        !
     ENDDO

     DO k=2,kz-1

        !old fktt=0.5*(df(k)*alt(k+1)*kt(k+1) +(1.-df(k))*alt(k)*kt(k))
        !old fktd=0.5*((1.-df(k-1))*alt(k-1)*kt(k-1) +df(k-1)*alt(k)*kt(k))

        
        fktt=(df(k)*alt1(k+1)*kt(k+1)+(1.-df(k))*alt1(k)*kt(k))*dz(k)  !new
        fktd=((1.-df(k-1))*alt1(k-1)*kt(k-1)+df(k-1)*alt1(k)*kt(k))*dz(k+1)  !new
        
        
        !   qmg=diff(qa1,kz,dz,k)  !don't needed
        !   tmg=diff(ta1,kz,dz,k)  !don't needed
        
        !   fluxle3(k)=-alt1(k)*lv*kt1(k)*qmg   ! don't needed
        !   fluxh3(k)=-alt1(k)*cpa*roa*kt1(k)*(tmg+gamma)  ! don't needed


        !old fluxh3(k)=-cpa*roa*(((ta1(k)-ta1(k-1)) /dz(k)+gamma)*fktd+ ((ta1(k+1)-ta1(k))/dz(k+1)+gamma)*fktt)
        !old fluxle3(k)=-lv*(((qa1(k)-qa1(k-1)) /dz(k))*fktd+ ((qa1(k+1)-qa1(k))/dz(k+1))*fktt)

        fluxh3(k)=-cpa*roa*(((ta1(k)-ta1(k-1))/dz(k)+gamma)*fktd+((ta1(k+1)-ta1(k))/dz(k+1)+gamma)*fktt)/(dz(k)+dz(k+1)) !new
        
        fluxle3(k)=-lv*(((qa1(k)-qa1(k-1))/dz(k))*fktd+((qa1(k+1)-qa1(k))/dz(k+1))*fktt)/(dz(k)+dz(k+1)) !new
        
        
     ENDDO
     
     !old fluxle3(1)=fluxle3(2)+ dz(2)*(fluxle3(2)-fluxle3(3))/dz(3)
     !old fluxh3(1)=fluxh3(2)+ dz(2)*(fluxh3(2)-fluxh3(3))/dz(3)

     !old fluxle3(1)=-lv* (alt1(2)*kt1(2)+alt1(1)*kt1(1))/2.* (qa1(2)-qa1(1))/dz(2)
     !old fluxh3(1)=-cpa*roa* (alt1(2)*kt1(2)+alt1(1)*kt1(1))/2.* ((ta1(2)-ta1(1))/dz(2)+gamma)

     fluxle3(1)=(fluxle3(2)*(dz(2)+dz(3))**2-fluxle3(3)*dz(2)**2)/( (dz(2)+dz(3))**2-dz(2)**2) ! new
     fluxh3(1)=(fluxh3(2)*(dz(2)+dz(3))**2-fluxh3(3)*dz(2)**2)/( (dz(2)+dz(3))**2-dz(2)**2)  !new
     
     fluxle3(kz)=fluxle3(kz-1)
     fluxh3(kz)=fluxh3(kz-1)

     gw=0.2
     g0=0.32
     tasoil=ta1(1)
     qasoil=qa1(1)

     !****old****
     !  dk00=dz(2)*alt1(3)*kt1(2+1)/da(2)+ (dz(2+1)-dz(2))*alt1(2)*kt1(2)/db(2)- dz(2+1)*alt1(1)*kt1(2-1)/dc(2)
     !      dkq=dz(2)*qa1(3)/da(2)+ (dz(2+1)-dz(2))*qa1(2)/db(2)- dz(2+1)*qa1(1)/dc(2)
     !  dkt=dz(2)*ta1(3)/da(2)+ (dz(2+1)-dz(2))*ta1(2)/db(2)- dz(2+1)*ta1(1)/dc(2)
     !  sk00= ((alt1(2)*kt1(2)+alt1(2)*kt1(2))/2. +alt1(1)*kt1(1))/2.
     !  sks00= ((alt1(kmix)*kt1(kmix) +alt1(kmix)*kt1(kmix))/2. +alt1(kmix-1)*kt1(kmix-1))/2.
     !  sks00=alt1(2)*dz(2)*0.1872*sqrt(u1(2)**2+v1(2)**2)/(log( z(2)/z0 ))**2
     !  qmg =((qa1(2)+qa1(2))/2.-qa1(1))/(dz(2))
     !  tmg =((ta1(2)+ta1(2))/2.-ta1(1))/(dz(2))+gamma
     !  qmgs =((qa1(kmix)+qa1(kmix))/2.-qa1(kmix-1)) /(dz(kmix))
     !  tmgs =((ta1(kmix)+ta1(kmix))/2. -ta1(kmix-1))/(dz(kmix))+gamma
     !  fluxle(1) = -lv*sk00*qmg
     !  if(rou.gt.0.) then
     !   fluxles = -1.13*lv*sks00*qmgs
     !   if(tau.gt.0.) fluxles = -lv*sks00*qmgs
     !  else
     !   fluxles = -lv*sks00*qmgs
     !   endif

     !   fluxh(1) = -roa*cpa*sk00*tmg
     !   fluxhs = -roa*cpa*sks00*tmgs

     !   haag= cpa*roa*sk00/dz(2) 
     !   haags= cpa*roa*sks00/dz(kmix) 
     !****old****

     
     fluxles =fluxle3(1)  !new
     fluxle(1) =fluxle3(1)  !new
     fluxhs =fluxh3(1)     !new
     fluxh(1) =fluxh3(1)   !new
     
     sks00= (alt1(1)*kt1(1)+alt1(2)*kt1(2))/2.  !new
     
     haags=cpa*roa*sks00/dz(2)     !new
     
     
     
     ff1=fnid(1)+fphd(1)+rsnt*psn(1)+ rskt*psk(1)+fird(1) -fniu(1)-fphu(1)
     !tar=ta1(kmix-1)  ! not neeed for anything


     if(rou.gt.0.05) then 
        g0=0.45
        if((ff1-firu(1)).lt.0.) g0=0.1

     else
        g0=0.32
        if((ff1-firu(1)).lt.0.) g0=0.19
     endif
     ! old  pp=g0*(ff1-firu(1))
     
     !pp=0.5*(3.*tsoil1(10+nmix)-4.*tsoil1(10+nmix-1)+tsoil1(10+nmix-2))/0.10 ! new
     
     !r soil heat flux
     
     if (   (int(soil_flux(nxodrad)) == -999) .or. (int(soil_flux(nxodrad+1)) == -999)   ) then  ! measurement values missing - CLEARCUT COMMENT OUT
     
         ! based on modelled soil temperatures
         pp=0.5*(3.*tsoil1(10+nmix)-4.*tsoil1(10+nmix-1)+tsoil1(10+nmix-2))/0.10 ! new
         
     else !- CLEARCUT COMMENT OUT

     
         ! based on the measurements
         pp=soil_flux(nxodrad)+(tmcontr-(nxodrad-1)*dt_obs)*(soil_flux(nxodrad+1)-soil_flux(nxodrad))/dt_obs   ! ground heat flux [W/m2] !- CLEARCUT COMMENT OUT

         
     endif !- CLEARCUT COMMENT OUT
        

     !fluxc=(ff1-firu(1))-pp-fluxle(1)  ! not needed for anything

     !old ta1(kmix-1)=(ff1 -pp-fluxles-(1.-dels)*fird(1)+ haags*((ta1(kmix)+ta1(kmix))/2.+gamma*(dz(kmix)))  &
     !old     +3.*sigm*dels*(ta(kmix-1)**4))/ (4.*sigm*dels*(ta(kmix-1)**3))/ (1.+haags/(4.*sigm*dels*(ta(kmix-1)**3)))

     ta1(kmix-1)=(ff1 -pp-fluxles-0.*(1.-dels)*fird(1)+ haags*(ta1(kmix)+gamma*(dz(kmix))) &  !new
          +3.*sigm*dels*(ta(kmix-1)**4))/ (4.*sigm*dels*(ta(kmix-1)**3))/(1.+haags/(4.*sigm*dels*(ta(kmix-1)**3))) !new

     ! fluxhs = -roa*cpa*sks00*( ((ta1(kmix)+ta1(kmix))/2. -ta1(kmix-1))/(dz(kmix))+gamma) !don't needed
     firu(1)=dels*sigm*ta1(kmix-1)**4+(1.-dels)*fird(1)  
     tau=0.

     ! guess: snow melting?
     if(ta1(kmix-1).ge.t273.and.rou.gt.0.) then  ! if air temperature > 0C and snow cover present
        ta1(kmix-1)=t273
        fluxhs = -roa*cpa*sks00* ( ((ta1(kmix)+ta1(kmix))/2. -ta1(kmix-1))/(dz(kmix))+gamma )
        firu(1)=dels*sigm*t273**4+(1.-dels)*fird(1)
        tau=max(0.d0,(((ff1-firu(1))-pp-fluxles-fluxhs)+0.*tau ))  ! net radiation on surface - soil heat flux - turbulent heat fluxes  ! this is the offset from balance
        tau2=1.*dt*(sks00*qmgs-tau*rh2o/333500./psnow)/rh2o       ! rh2o = 1000  ! psnow = 300
        rou=rou+tau2
        rou=max(rou,0.d0)
     endif
     tau4=0.

     !     soil parameters for loamy sand
     wgmax=0.410
     wgf=0.150  !  the field capacity of the soil which corresponds to an hydraulic conductivity of 0.1 mm d−1
     wgwilt=0.075
     bsoil=4.38
     cgsat=3.057*0.000001
     psoil=4.
     asoil=0.404
     cw2ref=3.7
     cw1sat=0.098
     wps=-9.  ! moisture potential when the soil is saturated
     tau3=tau/333500.   ! 333500 could be sulamislämpö, and I'm guessing that tau is the energy used for melting snow 
                        ! -> water from tau3 melting snow
     wg1(2)=wg(2)+0.*dt*(tau3 -fluxle(nz)/lv)/(rh2o*1.)+0.*dt*(wg2i-wg(2))/c864  ! wg1 volumetric water content of the soil  ! eq. (9) in Sog. 2002
     !wg1(2) is constant!
     ! volumetric water content of the soil, time step
     ! d(wg)/dt = f_m -> wg1 = wg + f_hum * dt
          
     wg1(2)=min(wg1(2),wgmax)
     wgeq=wg1(2)-wgmax*asoil*(wg1(2)/wgmax)**psoil* (1.-wg1(2)/wgmax)**(8.*psoil)
     if (wg(1) /= 0) then
        cw1=cw1sat*(wgmax/wg(1))**(0.5*bsoil+1.)
        cw1=min(cw1,4.d0)
     else
        cw1=4
     end if
     if(fluxles.le.0.) cw1=0.*cw1sat
     cw2=cw2ref*wg1(2)/(wgmax+wgf-wg1(2))
     if(rou.gt.0.0) cw1=0.098
     if(rou.gt.0.0) cw2=0.
     wg1(1)=wg(1)+1.*dt*tau3/(rh2o*0.010) -dt* cw1*fluxles/lv/(rh2o*0.010) -dt*cw2*(wg(1)-wgeq)/c864+0.*dt*(wg1i-wg(1))/c864
     ! volumetric water content of the soil, time step
     ! d(wg)/dt = f_m -> wg1 = wg + f_hum * dt
     ! equation (8) in Sog. 2002
     !   term 1: tau3/(rh2o*0.010)     ! rh2o = density of liquid water  ! 0.010 thickness of the soil layer  
                                       ! tau3 could be water from melting snow
     !   term 2: - cw1*fluxles/lv/(rh2o*0.010) ! fluxles evaporation  ! lv = 2501400 ! cw1 hydraulic coefficient C_1
     !   term 3: - cw2*(wg(1)-wgeq)/c864   ! wgeq = the equilibrium value  ! cw2 hydraulic coefficient C_2

     !   term 4: 0.*(wg1i-wg(1))/c864
     !           wg1i = 0.2
     wg1(1)=max(wg1(1),0.d0)
     if(rou.gt.1.d-2) wg1(1)=min(wg1(1),3.d-2)

     !r using measured value instead
     wg1(1) = surf_soil_hum
     
     cgw=cgsat*exp(0.5*bsoil*log(wgmax/wg1(2))/log(10.))
     temdif= max(418.*exp(-log(abs(wps*(wgmax/wg1(2))**bsoil))-2.7),1.72d-1)     ! thermal conductivity of soil, eq. below C5 in Sog. 2002
     ! wps*(wgmax/wg1(2))**bsoil) = moisture potential of soil tension
     ! wps =  moisture potential when the soil is saturated
     ! wgmax = maximum volumetric water content that a given soil type can hold (saturation water con)
     ! wg1(2) = volumetric water content of the soil, layer 1 is 0.01 m thick and layer 2 is 1 m thick
     ! bsoil = the slope of the retention curve  (?)
     ! see Appendix C in Sog. 2002
     temtran=temdif/((1.-wg1(2))*1270000.+wg1(2)*4180000.)     ! thermal diffusivity 
     ! 1270000 = density of the solid soil * specific heat of the solid soil

     if(rou.gt.0.0) then

        if(wg1(1).le.0.03) then
           vla=1. 
        else
           vla=1.
        endif

     else

     
        ! equation below C5 in Sog. 2002 
        if(wg1(1).le.wgf) then     
           vla=(0.5*(1.-cos(pi*wg1(1)/wgf))+0.*vla)/1.  ! relative humidity
        else
           vla=1.   ! fraction of humidity
        endif

     endif


 !     vla=0.2  !r works better this way
 
     if(ta1(kmix-1).lt.t273) vla=0.


     
     ! broder condition for humidity
     qa1(kmix-1)=vla* (qv0*exp(maga*(ta1(kmix-1)-t273)/ (magb+ta1(kmix-1)-t273)) /ta1(kmix-1))+1.*(1.-vla)*qa1(kmix)

     qa1(kmix-1)=min(qa1(kmix-1), (qv0*exp(maga*(ta1(kmix-1)-t273) /(magb+ta1(kmix-1)-t273)) /ta1(kmix-1)))


     do k=1,kz-1   ! loop 5647

        fluxle(k+1)=fluxle(k)+face*0.5*dz(k+1)* (sl(k+1)*(psn(k+1)*lv*dvsn(k+1)*(qsn(k+1) -qa1(k+1))+  &
             (1.-psn(k+1))*lv*dvsd(k+1)*(qsd(k+1)-qa1(k+1))) +sl(k)*(psn(k)*lv*dvsn(k)*(qsn(k)-qa1(k))+  &
             (1.-psn(k))*lv*dvsd(k)*   (qsd(k)-qa1(k))))   ! latent heat flux

        fluxh(k+1)=fluxh(k)+face*0.5*dz(k+1)* (sl(k+1)*(psn(k+1)*cpa*roa*dhsn(k+1) *(tsn1(k+1)-ta1(k+1))+  &
             (1.-psn(k+1))*cpa*roa*dhsd(k+1) * (tsd1(k+1)-ta1(k+1))) +sl(k)*(psn(k)*cpa*roa*dhsn(k)*(tsn1(k)-ta1(k))+  &
             (1.-psn(k))*cpa*roa*dhsd(k)* (tsd1(k)-ta1(k))))  ! sensible heat flux

     enddo   ! end of 5647

     
     ! energy balance at the top of the canopy 
     balans=fird(nz)+rsnt+rskt -1.*fniu(nz) -firu(nz)-1.*fphu(nz)-pp- 1.*fluxle3(nz)-1.*fluxh3(nz)-0.*tau-0.*tau4   ! C4 & C1 Sog. 2002
     ! energy balance at the surface
     balans1=fnid(1)+fphd(1)+rsnt*psn(1)+ rskt*psk(1)+fird(1) -1.*fniu(1) -firu(1)-1.*fphu(1)-pp- fluxles-fluxhs-1.*tau-tau4   ! C4 & C1 Sog. 2002
     ! tau = 0., tau4 = 0., tau might be energy used for melting snow
    



     ! *** AIR molecules ********************************************************************************************


   pres(1) = CH_gas_hyy(11,nxodrad) + (tmcontr-(nxodrad-1)*dt_obs) * (CH_gas_hyy(11,nxodrad+1)-CH_gas_hyy(11,nxodrad))/dt_obs

     do k = 2,kz
        pres(k)=pres(k-1) * exp(-9.81 * (z(k)-z(k-1))/R_d/(0.5*(ta1(k)+ta1(k-1))))
     enddo

     do k = 1,kz
        air(k) = pres(k) / ta1(k) / RG * Avog / 1E4 
     enddo

     ! Solar zenith angle
     Beta = acos(zenit)*180/pi



     ! Calculation of RH and water vapour pressure used in emission modules
     DO k = 1,kz
        ! Temperature in Kelvin
        ta1_C(k) = ta1(k) - 273.15

        ! Calculate saturation vapor pressure of water
        ! based on Seifield & Pandis, p765, unit in Pascal!!!!!!!!!!!!
        EM_ES(k)    = (a0 + a1 * ta1_C(k)**1 + a2 * ta1_C(k)**2 + a3 * ta1_C(k)**3 + a4 * ta1_C(k)**4 + a5 * ta1_C(k)**5  & 
                       + a6 * ta1_C(k)**6)* 100 
        
        ! Water vapour pressure in Pa - different options
        EM_EW(k) = (qa1(k)/(18.0153/1000.)) * 8.314 * ta1(k)

        ! Water vapour mixing ratio (g/kg) (based on Megan conversion function)
        EM_WVM(k) = EM_EW(k)/100. * 18.0153/ Pres(k) / 28.96

        RH(k) = 100.* qa1(k)/ (qv0*exp(maga*(ta1(k)-t273)/(magb+ta1(k)-t273))/ta1(k))
        RH(k) = 100. * EM_EW(k) / EM_ES(k)

        ! Water vapour pressure in Pa - different options
        !EM_EW(k)  = RH(k) * EM_ES(k) / 100  ! based on RH

        ! Limit EM_EW to maximum value of ES
        !IF (EM_EW(k) .GT. EM_ES(k)) THEN
        !   EM_EW(k) = EM_ES(k)
        !ENDIF
     ENDDO
  
   END SUBROUTINE MT_MainScadis


  FUNCTION diff(p,kz,dz,k)
    ! some kind of difference derivative(?), uses only indices k-1, k and k+1
    INTEGER :: kz,k
    REAL(kind=dp) :: diff,p(kz),dz(kz),da1,db1,dc1
    da1=dz(k+1)*(dz(k)+dz(k+1))
    db1=dz(k)*dz(k+1)
    dc1=dz(k)*(dz(k)+dz(k+1))
    diff=dz(k)*p(k+1)/da1+(dz(k+1)-dz(k))*p(k)/db1-dz(k+1)*p(k-1)/dc1
    RETURN
  END FUNCTION diff


  SUBROUTINE gtri(a, c, b, d, p, l1, a1, q1, lm, am, qm, kz, l2)

    IMPLICIT NONE
    INTEGER, PARAMETER :: dp = SELECTED_REAL_KIND(14,300)
    ! interface:
    INTEGER, INTENT(in) :: l1,l2,lm,kz
    REAL(kind=dp), INTENT(in) :: a1,q1,am, qm
    REAL(kind=dp), INTENT(in) :: a(kz),b(kz),c(kz),d(kz)
    REAL(kind=dp), INTENT(inout) :: p(kz)

    ! local:
    INTEGER :: m2,m,n
    REAL(kind=dp) :: den
    REAL(kind=dp) :: e(kz),f(kz)
    !     double precision a,b,c,d,e,f

    IF(l1.EQ.1) e(l2-1)=0.
    IF(l1.EQ.1) f(l2-1)=a1
    IF(l1.EQ.2) e(l2-1)=1.
    IF(l1.EQ.2) f(l2-1)=-a1


    IF(l1.EQ.3) e(l2-1)=a1/(a1-1.)

    IF(l1.EQ.3) f(l2-1)=q1/(1.-a1)

    m2=kz-1
    DO m=l2,m2
       den=b(m)-c(m)*e(m-1)
       f(m)=(d(m)+c(m)*f(m-1))/den

       e(m)=a(m)/den

    ENDDO



    IF(lm.EQ.1) p(kz)=am
    IF(lm.EQ.2) p(kz)=(f(m2)+am)/(1.-e(m2))

    IF(lm.EQ.3) p(kz)=(f(m2)+qm/am)/((1.+am)/am-e(m2))

    DO n=1,m2-l2+2
       m=kz-n
       p(m)=e(m)*p(m+1)+f(m)
    ENDDO

  END SUBROUTINE gtri


 SUBROUTINE inter3(z,f,kz,z1,kz1,f2)
    !c     Subroutine inter3 takes in a 3d-array of data (f), and a 2d-array
    !c     describing the height of terrain (rou). Then it interpolates
    !c     the data in z-dimension and produces a new dataset (f2).
    !c     The new, interpolated, dataset is in absolute coordinate system.
    !c     Cubic spline (natural) interpolation is used.
    !c
    !c     z(1:kz)
    !c       z-coordinates of gridpoints in terrain-following system
    !c     f(1:kx,1:ky,1:kz)
    !c       values of some function in terrain-following system
    !c     rou(1:kx,1:ky)
    !c       ground height in the grid nodes
    !c     kx,ky
    !c       x- and y-dimensions of the 3d-arrays
    !c     kz
    !c       z-dimension of the terrain-following data
    !c     kz2
    !c       z-dimension of the new data that we shall produce by interpolation
    !c     z2(1:kz2)
    !c       z-coordinates of the new gridpoints
    !c     f2(1:kx,1:ky,1:kz2)
    !c       here we will put the new, interpolated, values of the function        
    IMPLICIT NONE
    INTEGER :: kz,kz1
    REAL(kind=dp) :: z(kz),f(kz),z1(kz1),f2(kz1)
    INTEGER :: k
    REAL(kind=dp) :: fvals(kz),fvals2(kz1),h

    DO k=1,kz
       fvals(k) = f(k)
    ENDDO
    h = 0.
    CALL inter1(z,fvals,kz,h,z1,kz1,fvals2)
    DO k=1,kz1
       f2(k) = fvals2(k)
    ENDDO
    RETURN
  END SUBROUTINE inter3

  SUBROUTINE inter1(za,fa,n,z0,z2a,n2,f2a)
    IMPLICIT NONE
    INTEGER :: n, n2
    REAL(kind=dp) :: za(n),fa(n),z0,z2a(n2),f2a(n2)
    INTEGER :: i
    REAL(kind=dp) :: derivs(n), hh, resul
    CALL splinh(za,fa,n,derivs)
    DO i=1,n2
       IF (z2a(i) .LT. z0) THEN
          f2a(i) = 0.0
       ELSE
          !            IF (z2a(i) .gt. za(n)) PAUSE 'inter1 does not extrapolate'
          hh = z2a(i)-z0
          CALL splint(za,fa,derivs,n,hh,resul)
          f2a(i) = resul
       ENDIF
    ENDDO
    RETURN
  END SUBROUTINE inter1

  SUBROUTINE splint(xa,ya,y2a,n,x,y)
    IMPLICIT NONE
    INTEGER :: n
    REAL(kind=dp) :: xa(n),ya(n),y2a(n),x,y
    INTEGER :: k, klo, khi
    REAL(kind=dp) :: a,b,hs
    klo = 1
    khi = n
1005 IF (khi-klo .GT. 1) THEN
       k = (khi+klo)/2
       IF (xa(k) .GT. x) THEN
          khi = k
       ELSE
          klo = k
       ENDIF
       GOTO 1005
    ENDIF
    hs = xa(khi)-xa(klo)
    if (hs .eq. 0.0) STOP 'bad xa input in splint'
    a = (xa(khi)-x)/hs
    b = (x-xa(klo))/hs
    y = a*ya(klo) + b*ya(khi) +&
         ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi)) * (hs**2)/6.
    RETURN
  END SUBROUTINE splint

  SUBROUTINE splinh(x,y,n,y2)
    IMPLICIT NONE
    INTEGER :: n
    REAL(kind=dp) :: x(n),y(n),y2(n)
    INTEGER, PARAMETER :: NMAX=1000
    INTEGER :: i,k
    REAL(kind=dp) :: p,sig,uu(NMAX)
    y2(1) = 0.
    uu(1) = 0.
    DO i=2,n-1
       sig = (x(i)-x(i-1))/(x(i+1)-x(i-1))
       p = sig*y2(i-1)+2.
       y2(i) = (sig-1.)/p
       uu(i) = (6.*((y(i+1)-y(i))/(x(i+1)-x(i)) - (y(i)-y(i-1))&
            /(x(i)-x(i-1)))/(x(i+1)-x(i-1)) - sig*uu(i-1))/p
    ENDDO
    y2(n) = 0.
    DO k=n-1,1,-1
       y2(k)=y2(k)*y2(k+1)+uu(k)
    ENDDO
    RETURN
  END SUBROUTINE splinh

