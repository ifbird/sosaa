! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !
! All parts of SCADIS related to soil processes                                                 !
! For more information see MT_MainMet.f90                                                       !
!                                                                                               !
! Commented and restructured by Rosa Gierens, University of Helsinki                            !
!                                                                                               !
! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !


MODULE Scadis_Soil

  ! variables from other modules
  USE second_Precision, ONLY : dp    ! For simplicity, the precision of real numbers is applied to the whole program. L.
 ! USE SOSA_DATA !, ONLY : variablename
  
  ! subroutines and variables part of the meteorology scheme
  ! USE Scadis_Parameters
  ! USE Scadis_Tools

  IMPLICIT NONE

  ! will make this private later
  !PRIVATE

  !Public subroutines:
  !PUBLIC :: 

  !Public variables:
  !PUBLIC ::   
  
  
CONTAINS


! executable code here
    
END MODULE Scadis_Soil
