clear all
close all

% Change the temperature if you have selected a different range in the
% vapour pressure calculation and provid the number of the temperature 
% for the test calculations: T(XX) with XX = 6 is T(6)
T=[263:313]; %Petri and German and Simon and Ray
%T=[253:303]; %Carlton
%T=[270:320]; %Melpitz
XX = 51; 

% In the beginning you have to check the exel-file produced by Nannoolal, Evaporation 
% or other method and check the end point and include it in the read statement below
%[aa,bb,cb]=xlsread('/Users/boy/Documents/Michael/Model/Vapour_prop/Petri/umansysprop.xlsx','B5:BRW55'); 
%[aa,bb,cb]=xlsread('/Users/boy/Documents/Michael/Model/Vapour_prop/Carlton/umansysprop.xlsx','B5:BUE55'); 
%[aa,bb,cb]=xlsread('/Users/boy/Documents/Michael/Model/Vapour_prop/Melpitz/umansysprop.xlsx','B5:FMK55'); 
%[aa,bb,cb]=xlsread('/Users/boy/Documents/Michael/Model/Vapour_prop/Simon/umansysprop.xlsx','B5:FLK55'); 
[aa,bb,cb]=xlsread('/Users/boy/Documents/Michael/Model/Vapour_prop/Ray/umansysprop.xlsx','B5:FPB55'); 


ELVOC_p=aa'; % Vapour pressures at log10 values in atmosphere

N_of_Comp = length(aa);
N_of_Temp = length(T);

% Only to calculate the O:C ratio of the compounds read in the smiles
fileID = fopen('/Users/boy/Documents/Michael/Model/Vapour_prop/Ray/aaa.txt');
C1 = textscan(fileID, '%s',N_of_Comp);

clear O_count C_count N_count c_count CT_count
for j = 1:N_of_Comp
    C_count(j)=0.; c_count(j)=0.; O_count(j)=0.; N_count(j)=0.; CT_count(j)=0.;
    [x y] = size(C1{1}{j});
    for i = 1:y
        if C1{1}{j}(i) == 'C'
           C_count(j) = C_count(j) + 1;
        end
        if C1{1}{j}(i) == 'c'
           c_count(j) = c_count(j) + 1;
        end
        if C1{1}{j}(i) == 'O'
           O_count(j) = O_count(j) + 1;
        end
        if C1{1}{j}(i) == 'N'
           N_count(j) = N_count(j) + 1;
        end
    end
    CT_count(j) = C_count(j) + c_count(j);
    
    O_C_ratio(j) = O_count(j) / CT_count(j);    
end



% Calculate the A and B coefficients for the vapour pressure
for j=1:length(ELVOC_p(:,1))
    j
    log10p =ELVOC_p(j,:);
    
    % Fit Antoine equation
    fun = @(p) sum((log10p - p(1)+p(2)./T).^2);
 
    % Starting guess
    pguess = [10,6000];

    % Optimise
    [p,fminres] = fminsearch(fun,pguess);
    log10p_fit=p(1)-p(2)./T;
    A(j)=p(1);
    B(j)=p(2);
end


% If you want to include ELVOC or HOM you have to provide the following files
HOM = load(['/Users/boy/Documents/Michael/Model/Vapour_prop/Pontus/HOM_v21_prop.dat']);
fileID = fopen('/Users/boy/Documents/Michael/Model/Vapour_prop/Pontus/HOM_v21_names.dat');
C3 = textscan(fileID, '%s',194);
fclose(fileID)

% Open the mass file for all organic compounds from MCM
Mass = load(['/Users/boy/Documents/Michael/Model/Vapour_prop/Ray/Mass.txt']);

% Open the name file for all organic compounds from MCM
fileID = fopen('/Users/boy/Documents/Michael/Model/Vapour_prop/Ray/Names.txt');
C2 = textscan(fileID, '%s',N_of_Comp);


% Create the output files and selcet only the vapour pressure below an upper limit - e.g. 1E-6 
fid1 = fopen('/Users/boy/Documents/Michael/Model/Vapour_prop/Ray/Vapour_properties.dat','w');
fid2 = fopen('/Users/boy/Documents/Michael/Model/Vapour_prop/Ray/Vapour_names.dat','w');
fid3 = fopen('/Users/boy/Documents/Michael/Model/Vapour_prop/Ray/Vapour_O_C_ratio.dat','w');
fid4 = fopen('/Users/boy/Documents/Michael/Model/Vapour_prop/Ray/Vapour_smiles.dat','w');
fid5 = fopen('/Users/boy/Documents/Michael/Model/Vapour_prop/Ray/Vapour_O_C_ratio_Carlton.dat','w');

boltzmann = 1.3806488E-23; % (J/K)
i = 0.;
for j = 1:length(ELVOC_p(:,1))
    vap_pressure(j) = 10^(A(j)-(B(j)/T(XX)));
    vapor_concentration(j) = (vap_pressure(j)*101325)/(boltzmann*T(XX)*1.E6);
   if vap_pressure(j) < 1.E-7
   %if vap_pressure(j) < 1.E-9
       
       fprintf (fid1, '%22.3f',  Mass(j));
       fprintf (fid1, '%32.3f',  A(j));
       fprintf (fid1, '%32.3f',  B(j));
       fprintf (fid1, '\n');  
    
       fprintf (fid2, '%s',      C2{1}{j});
       fprintf (fid2, '\n');

       fprintf (fid3, '%15s',    C2{1}{j});
       fprintf (fid3, '%10.2f',  O_C_ratio(j));
       fprintf (fid3, '%10.2f',  CT_count(j));
       fprintf (fid3, '%10.2f',  O_count(j));
       fprintf (fid3, '%10.2f',  N_count(j));
       fprintf (fid3, '%15.3f',  Mass(j));
       fprintf (fid3, '%15.2e',  vap_pressure(j));
       fprintf (fid3, '%15.2e',  vapor_concentration(j));
       fprintf (fid3, '\n');

       fprintf (fid4, '%s',      C1{1}{j});
       fprintf (fid4, '\n');

       fprintf (fid5, '%10.2f',  O_C_ratio(j));
       fprintf (fid5, '%10.2f',  CT_count(j));
       fprintf (fid5, '%10.2f',  O_count(j));
       fprintf (fid5, '%10.2f',  N_count(j));
       fprintf (fid5, '%15.3f',  Mass(j));
       fprintf (fid5, '%15.2e',  vap_pressure(j));
       fprintf (fid5, '%15.2e',  vapor_concentration(j));
       fprintf (fid5, '\n');

       i = i+1;
       
       % For plotting only the selected compounds
       Selected_vp(i) = vap_pressure(j);
       Selected_vc(i) = vapor_concentration(j);
       Selected_ma(i) = Mass(j);
       Selected_OC(i) = O_C_ratio(j);
   end
end
for j = 1:length(HOM)
       fprintf (fid1, '%22.3f',  HOM(j,1));
       fprintf (fid1, '%32.3f',  HOM(j,2));
       fprintf (fid1, '%32.3f',  HOM(j,3));
       fprintf (fid1, '\n'); 
   
       fprintf (fid2, '%s',      C3{1}{j});
       fprintf (fid2, '\n');
       
       fprintf (fid3, '%15s',    C3{1}{j});
       fprintf (fid3, '%10.2f',  HOM(j,7)/HOM(j,4));
       fprintf (fid3, '%10.2f',  HOM(j,4));
       fprintf (fid3, '%10.2f',  HOM(j,7));
       fprintf (fid3, '%10.2f',  HOM(j,6));
       fprintf (fid3, '%15.3f',  HOM(j,1));
       fprintf (fid3, '%15.2e',  0.);
       fprintf (fid3, '%15.2e',  0.);
       fprintf (fid3, '\n');

       fprintf (fid5, '%10.2f',  HOM(j,7)/HOM(j,4));
       fprintf (fid5, '%10.2f',  HOM(j,4));
       fprintf (fid5, '%10.2f',  HOM(j,7));
       fprintf (fid5, '%10.2f',  HOM(j,6));
       fprintf (fid5, '%15.3f',  HOM(j,1));
       fprintf (fid5, '%15.2e',  0.);
       fprintf (fid5, '%15.2e',  0.);
       fprintf (fid5, '\n');
end

% Plot of all compounds 
figure(1)
clf
semilogy(vap_pressure, 'ro')
hold on
semilogy(vapor_concentration, 'bo')
semilogy(10.^(ELVOC_p(:,25)), 'kx')

% Plot of the selected compounds
figure(2)
clf
semilogy(Selected_OC, Selected_vp, 'ko')

for j = 1:71
    vap_MCM(j) = 10^(A(576)-(B(576)/(252+j)));
    vap_HOM(j) = 10^(HOM(68,2)-(HOM(68,3)/(252+j)));
end

figure(4)
clf
plot(vap_MCM,'g')
hold on
plot(vap_HOM, 'r')
