#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : Petri CLusius
# Created Date: 18/12/2019
# =============================================================================

try:
    import numpy as np
    import sys
    import json
    import requests
    import pickle
    from scipy import optimize
except:
    print('To run this script, you need NumPy, SciPy, requests, pickle')

def fitSimple(T,A,B):
    return A - B/(T)

def fitGood(T,A,B,C):
    return A - B/(T-C)

if len(sys.argv) > 1:
    root = sys.argv[1]
    v_press_limit = float(sys.argv[2])
else:
    root = 'mcm_subset_mass.txt'
    v_press_limit = 1e-9

pram = False
for n in sys.argv:
    if n == '--pram':
        pram = True

print('Using ',root)
print('Picking compounds with vapour pressure lower than ',v_press_limit)

for n in sys.argv:
    if n == '--pram':
        pram = True
        print('Using PRAM')

f = open(root)
i = 0
for line in f:
    if "Molecular weights for species present in the subset" in line:
        break
    i = i+1
f.close()

aaa, smiles, mass = np.genfromtxt(root, usecols=(0,1,3),unpack=True, skip_header=i+3, dtype=str)
N = len(aaa)

n_temp = 11
temp = np.linspace(263.15,313.15,n_temp)

get_stuff = False

try:
    alldata = pickle.load(open("umansysprop.pickle", "rb"))
except:
    get_stuff = True

if get_stuff:
    url = 'http://umansysprop.seaes.manchester.ac.uk/api/vapour_pressure'
    print('Contacting ', url)
    print('Have patience, this might take a while')

    header = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Accept-Encoding': 'gzip, deflate',
            'User-Agent': 'python-requests/2.4.1 CPython/2.7.6 Linux/3.13.0-48-generic',
            'keep-alive': 'timeout=600, max=100',
            'Connection': 'keep-alive'
            }

    get_vapours = {
                "vp_method": "nannoolal",
                "bp_method": "nannoolal",
                "temperatures": list(temp),
                "compounds": list(smiles)
                }

    x = requests.post(url, data = json.dumps(get_vapours), headers=header)
    data = x.json()[0]
    alldata = data['data']
    pickle.dump( alldata, open( "umansysprop.pickle", "wb" ) )

# for d in alldata:
#     print('at ', d['key'][0], d['key'][1], d['value'])
n_homs = 0
if pram:
    homs = np.genfromtxt('HOM_v21_names.dat', dtype=str)
    hom_mass, HomA,HomB = np.genfromtxt('HOM_v21_prop.dat',usecols=(0,1,2), unpack=True)
    n_homs = len(homs)
    aaa = np.append(aaa,homs)
    mass = np.append(mass,hom_mass)

prop_matrix = np.zeros((N+n_homs, n_temp))

if pram:
    for i,v in enumerate(homs):
        prop_matrix[i+N,:] = fitSimple(temp,HomA[i], HomB[i] )

for i in range(len(alldata)):
    prop_matrix[i%N,i//N] = float(alldata[i]['value'])



selected_vapours = prop_matrix[10**(prop_matrix[:,n_temp//2]) < v_press_limit]
selected_vapournames = aaa[10**(prop_matrix[:,n_temp//2]) < v_press_limit]
selected_vapourmass = mass[10**(prop_matrix[:,n_temp//2]) < v_press_limit]

N = np.size(selected_vapours,0)

print(N, ' compounds selected')
np.savetxt('Vapour_names.dat', selected_vapournames, fmt='%s')

para_matrixSimple = np.zeros((N, 3))
para_matrixGood = np.zeros((N, 4))
para_matrixBest = np.zeros((N, 7))

for i in range(N):
    para_matrixSimple[i,0] = selected_vapourmass[i]
    para_matrixGood[i,0] = selected_vapourmass[i]
    para_matrixSimple[i,1:], pcovS = optimize.curve_fit(fitSimple, temp, selected_vapours[i,:], maxfev = 100000)
    para_matrixGood[i,1:], pcovG = optimize.curve_fit(fitGood, temp, selected_vapours[i,:],p0=(0,0,0), maxfev = 100000)


print('Saving files')
np.savetxt('Vapour_properties3.dat', para_matrixGood, fmt='%24.12f')
np.savetxt('Vapour_properties.dat', para_matrixSimple, fmt='%24.12f')



#
