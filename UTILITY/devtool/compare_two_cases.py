import sys

import numpy as np
import xarray as xr

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import datapack as dp


# ===== Helper functions ===== #
def rmse(x, y):
  return np.sqrt( np.mean((x - y)**2) )


def compare_contours(vname, time, lev):
  x1 = case1.xrds[vname].transpose().values
  x2 = case1.xrds[vname].transpose().values
  dx = x2 - x1
  
  # Calculate the RMSE
  print('The RMSE between {0}2 and {0}1 is: {1}'.format(vname, rmse(x1, x2)))
  
  # Plot
  fg, ax = plt.subplots(3, 1, figsize=(16, 12), dpi=150)
  
  # case1
  ax[0].contourf(time, lev, x1)
  
  # case2
  ax[1].contourf(time, lev, x2)
  
  # case2 - case1
  ax[2].contourf(time, lev, dx)
  
  fg.savefig('{0}_contour.png'.format(vname))


# ===== Read data ===== #
case1 = dp.SosaaDat('/scratch/project_2001025/sosaa/cases/base_run_sample/output-201604')
case1.get_dimensions()
case1.get_variables_from_file('ua', ('time', 'lev'), 'UUU.dat')
case1.get_variables_from_file('va', ('time', 'lev'), 'VVV.dat')
case1.get_variables_from_file('nconc_OH', ('time', 'lev'), 'Gas_OH.dat')
case1.get_variables_from_file('nconc_APINENE', ('time', 'lev'), 'Gas_APINENE.dat')
case1.get_variables_from_file('nconc_BPINENE', ('time', 'lev'), 'Gas_BPINENE.dat')

case2 = dp.SosaaNc('/scratch/project_2001025/sosaa/cases/netcdf_sample/output-201604')
case2.xrds.coords['time'] = case1.xrds['time']

print(case1.xrds)

print()
print('=========================')
print()

print(case2.xrds)


# ===== Compare time - height contours =====#

# Get the dimensions
time = case1.xrds.coords['time'].values
lev  = case1.xrds.coords['lev'].values

# ua
compare_contours('ua', time, lev)

# nconc_OH
compare_contours('nconc_OH', time, lev)

# nconc_APINENE
compare_contours('nconc_APINENE', time, lev)
