"""
Dowload ECMWF data:
1. Register an account in ECMWF
2. Login at https://apps.ecmwf.int/auth/login/
3. Retrieve you key at https://api.ecmwf.int/v1/key/
4. Copy the information in this page and paste it in the file $HOME/.ecmwfapirc
5. (1) Install ecmwfapi python library:
   sudo pip install https://software.ecmwf.int/wiki/download/attachments/47287906/ecmwf-api-client-python.tgz
   (2) Or download ecmwf-api-client-python.tgz from:
   https://software.ecmwf.int/wiki/download/attachments/56664858/ecmwf-api-client-python.tgz
   Then extract it and install the package.
   (3) Or download ecmwf-api-client-python.tgz then extract it to current folder.
6. See the example of downloading ERA-Interim data at:
   https://software.ecmwf.int/wiki/display/WEBAPI/Python+ERA-interim+examples
7. Download the data, notice that the data starts from 03:00 to 00:00, so we need to download the previous day in last
   year. Then the dataset will includes the time 00:00. Here we also need to consider the time zone, ECMWF is UTC+00,
   Hyytiala is UTC+02. So we need 21:00 and 24:00 from previous day.
Process data:
1. ??? Interpolate the ECMWF data to the Hyytiala point, also interpolate in time dimension to meet the model demands.
   So finally the time points in the file are consistent with the model.
2. Write the data to file.

The code is tested by python 3.4.5.
"""


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
# Header
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
import os
import sys
import errno
import shutil

import re

import urllib
import requests
import json

import datetime
import calendar

import numpy as np
import netCDF4

from ecmwfapi import ECMWFDataServer

import matplotlib as mpl
import matplotlib.pyplot as plt


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
# Set parameters
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
# year = 2010
# fname = 'ERA_Interim_Daily-LWR-{0:4d}1231_{1:4d}1231.nc'.format(year-1, year)
# lat_h = 61.8500
# lon_h = 24.2833


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
# Functions
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#---------------------------------------------------------#$
# Create dir if not exist$
#---------------------------------------------------------#$
def mkdir_p(path):
  try:
    os.makedirs(path)
  except OSError as exc:  # Python >2.5
    if exc.errno == errno.EEXIST and os.path.isdir(path):
      pass
    else:
      raise


def read_SMEAR_data(fname):
  # Read original file
  raw_data = np.genfromtxt(fname, skip_header=1, dtype=float, delimiter=',')

  # Count number of rows
  nrow, ncol = raw_data.shape

  # Initialize data and time
  data = np.array([None]*nrow)
  time = np.array([None]*nrow)

  # Read data and time
  data = raw_data[:, 6]
  for i in range(nrow):
    time[i] = datetime.datetime(*raw_data[i, 0:6].astype(int))

  return data, time


def expand_SMEAR_data(time, data, time_new):
  # Dimensions of time and time_new
  nt = time.size
  nt_new = time_new.size

  # Time step for new time series
  dt = (time_new[1] - time_new[0]).total_seconds()

  # Find the indices of data in the data_new
  ind = [int(round((time[i]-time[0]).total_seconds()/dt)) for i in range(nt)]

  # Initiate data_new with nan, then set values according to data
  data_new = np.ones((nt_new,))*np.nan
  data_new[ind] = data

  return data_new


def download_SMEAR_data(params, force_download=False, folder='.'):
  #
  # Set parameters
  #

  # Basic url for downloading data
  # url_data = 'http://avaa.tdata.fi/palvelut/smeardata.jsp'  # old url
  url_data = 'http://avaa.tdata.fi/smear-services/smeardata.jsp'

  # Field names
  params_keys = ['variables', 'table', 'from', 'to', 'quality', 'averaging', 'type']

  #
  # Download or not
  #

  # If params is empty, then return error
  if not params:
    print("The argument 'params' is empty.")
    return None

  # Get the file name
  prefix = folder + '/' + params[2][0:10].replace('-', '') + '_' + params[3][0:10].replace('-', '') + '-'
  fname = prefix + params[0] + '.txt'

  # Check if download the file
  is_download = True
  try:
    if os.stat(fname).st_size > 0:  # file exists and is not empty
      is_download = False
    else:  # file empty
      is_download = True
  except OSError:  # file does not exist
    is_download = True

  if force_download or is_download:
    print('Downloading ', fname)

    # Get requests object from url
    resp = requests.get( url=url_data, params=dict(zip(params_keys, params)) )  # requests seem to have a SSL error
    # full_url = url_data + '?' + urllib.parse.urlencode(dict(zip(params_keys, params)))
    # resp = urllib.request.urlopen(full_url)

    # Write resp directly to the file
    with open(fname, 'w') as f:
      f.write(resp.text)
      # f.write(resp.read())

  return fname

  """
  # Get time and data arrays
  time = []
  data = []
  for i, line in enumerate(resp.iter_lines()):
    # Skip the header
    if i == 0:
      continue
    lstr = line.decode('utf-8')  # bytes --> string
    mobj = re.split(',', lstr)  # year, month, day, hour, minute, second, data
    if mobj:
      # Get time
      time.append( datetime.datetime( int(mobj[0]), int(mobj[1]), int(mobj[2]), int(mobj[3]), int(mobj[4]), int(mobj[5]) ) )
      # Get data
      data.append( float(mobj[6]) )

  # Convert time and data to numpy arrays
  time = np.array(time)
  data = np.array(data)

  # Write time and data into output file, if you do not need the output file, then set download to False.
  fname = prefix + params[0] + '.txt'

  is_download = True
  try:
    if os.stat(fname).st_size > 0:  # file exists and is not empty
      is_download = False
    else:  # file empty
      is_download = True
  except OSError:  # file does not exist
    is_download = True

  if force_download or is_download:
    with open(fname, 'w') as f:
      ## Write time and data
      for i, (t, d) in enumerate(zip(time, data)):
        f.write('{0}  {1:25.16e}\n'.format(t.strftime('%Y  %m  %d  %H  %M  %S'), d))

  return data, time
  """


def download_ECMWF_data(dataset_in='interim', class_in='ei', type_in='fc', stream_in='oper', \
	date_in='20131201/to/20131230', time_in='00/12', step_in='3/6/9/12', \
	grid_in='0.125/0.125', area_in='61.875/24.250/61.750/24.375', levtype_in='sfc', levelist_in='1000/975/950', \
	param_in='175.128', \
	format_in='netcdf', target_in='ECMWF_dataset.nc', \
	force_download=False):
  """
  Download the data from ECMWF data server.
  param_in: Name string for ECMWF variables.
    '175.128': surface thermal radiation downwards
		'129.128': geopotential
		'130.128': temperature
		'131.128': u component of wind
		'132.128': v component of wind
		'133.128': specific humidity
  date_in: Date of datasets.
    'yyyy-mm-dd/to/yyyy-mm-dd/': from one date to another
    'yyyymmdd/yyyymmdd/.../yyyymmdd': selected dates
  area_in: Area range.
		(N, W, S, E), NS: -90 to 90, WE: -180 to 180.
		For Hyytiala, '61.875/24.25/61.75/24.375', the point is 61d51mN, 24d17mE -> 61.85N, 24.2833E.
  levtype_in: Level type.
		'sfc':surface
		'pl': pressure levels.
	levelist_in: Level list. If levtype_in is 'sfc', this does not take effect. If levtype is 'pl', then levellist should
		be sepcified.
		'200/225/250/300/350/400/450/500/550/600/650/700/750/775/800/825/850/875/900/925/950/975/1000'
  """
  # Check the status of the target
  is_download = True
  try:
    if os.stat(target_in).st_size > 0:  # file exists and is not empty
      is_download = False
    else:  # file empty
      is_download = True
  except OSError:  # file does not exist
    is_download = True

  # Download data with ECMWF webapi
  if force_download or is_download:
    server = ECMWFDataServer()
    
    server.retrieve({
      'dataset'   : dataset_in,
      'class'     : class_in,
      'type'      : type_in,
      'stream'    : stream_in,
      'date'      : date_in,
      'time'      : time_in,
      'step'      : step_in,
      'grid'      : grid_in,
      'area'      : area_in,
      'levtype'   : levtype_in,
      'levelist'  : levelist_in,
      'param'     : param_in,
      'format'    : format_in,
      'target'    : target_in,
      })


def combine_SMEAR_data(time, levels, param_list, folder='.'):
  nt = time.size  # time dimension

  # Number of levels
  if np.isscalar(levels):
    nl = 1
  else:
    nl = levels.size

  data = np.ones( (nt, nl) ) * np.nan  # combined data array

  for i, pl in enumerate(param_list):
    prefix = folder + '/' + pl[2][0:10].replace('-', '') + '_' + pl[3][0:10].replace('-', '') + '-'
    fname = prefix + pl[0] + '.txt'

    data[:, i], _ = read_SMEAR_data(fname)

  return np.squeeze(data)


def write_combined_SMEAR_data(data, time, levels, fname):
  """
  data(nt, nl)
  """
  # Make sure input variables are all arrays
  if np.isscalar(levels):
    nl = 1
    levels_new = np.array([levels,])
    data_new = data[:, np.newaxis]
  else:
    if levels.size == 1:
      nl = 1
      levels_new = np.array([levels,])
      data_new = data[:, np.newaxis]
    else:
      nl = levels.size
      levels_new = levels
      data_new = data

  with open(fname, 'w') as f:
    # Header
    f.write( '{0:4d}{1:4d}{2:4d}{3:4d}{4:4d}{5:4d}'.format(0, 0, 0, 0, 0, 0) + \
      ''.join(['{:25.3f}']*nl).format(*levels_new) + '\n' )
    # year, month, day, hour, minute, second, data
    for it, t in enumerate(time):
      f.write('{0:4d}{1:4d}{2:4d}{3:4d}{4:4d}{5:4d}'.format(t.year, t.month, t.day, t.hour, t.minute, t.second) + \
        ''.join(['{:25.16e}']*nl).format(*data_new[it, :]) + '\n')


def gap_filling_Gierens2014(data, time):
  """
  Gap filling for NAN values with Gierens2014 method (Gierens, 2014, SAJS), here use forward gradients
  nt: number of time points
  data: (nt,), data of time series, should be at least longer than 2 days
  time: (nt,), time
  """
  
  # Setting parameters
  nt = time.size  # number of time points
  time0 = time[0]  # start time
  time1 = time[-1]  # end time

  # Save the original data
  data_new = np.copy(data)

  for i in range(nt):
    if np.isnan(data[i]):
      # If the first value is NAN, then skip it
      if i == 0:
        continue

      # Dates of previous and following days
      day1 = time[i] - datetime.timedelta(days=1)
      day2 = time[i] + datetime.timedelta(days=1)
      
      # Both values should be from the same day within the time range if one day is out of range
      if day1 < time0:
        day1 = day2
      if day2 > time1:
        day2 = day1

      # Inidces of previous and following days
      i1 = np.where(time == day1)[0][0]
      i2 = np.where(time == day2)[0][0]

      # Time delta in [s]
      dt1 = (time[i1] - time[i1-1]).total_seconds()  # [s], time delta
      dt2 = (time[i2] - time[i2-1]).total_seconds()  # [s], time delta
      dt = (time[i] - time[i-1]).total_seconds()  # [s]

      # Gradients at the same time on previous and following days
      grad1 = (data[i1] - data[i1-1]) / dt1
      grad2 = (data[i2] - data[i2-1]) / dt2
      grad = np.nanmean([grad1, grad2])

      # Fill current NAN value
      data_new[i] = data_new[i-1] + grad*dt

  return data_new


def gap_filling(data, time, method):
  """
  Gap filling for NAN values.
  nt: number of time points
  data: (nt,), data of time series, should be at least longer than 2 days
  time: (nt,), time, should have same time intervals and starts at 00:00
  """
  # Setting parameters
  nt = time.size  # number of time points
  time0 = time[0]  # start time
  time1 = time[-1]  # end time

  # Also return a numpy array containing information of NAN points indices
  # ind_nan = np.where(np.isnan(data))[0]
  ind_nan = np.isnan(data)

  #
  # Refer to Gierens2014, SAJS
  #
  if method == 'Gierens2014':
    data_new = gap_filling_Gierens2014(data, time)
  # 
  # Hybrid method proposed by Putian for current use.
  # 1. data --> data1: Use Gierens method for data to get data1.
  # 2. data1 --> data2: Calculate monthly diurnal pattern from data to fill other NAN values.
  #
  elif method == 'Hybrid_Zhou':
    # First Gierens2014 method
    data_new = gap_filling_Gierens2014(data, time)

    # Calculate monthly diurnal pattern from original data
    dt = (time[1] - time[0]).total_seconds()
    ndt = np.rint(86400.0/dt).astype(int)
    dtime = np.array([i for i in np.arange(0, 86400, dt)])  # 0, dt, 2*dt, 3*dt, ..., 86400-dt
    davg = np.ones(ndt)
    for i in range(ndt):
      davg[i] = np.nanmean(data[i::ndt])

    for i in range(nt):
      if np.isnan(data_new[i]):  # if the value is still NAN
        # Corresponding daily ind
        dind = np.rint( (time[i].hour * 3600.0 + time[i].minute * 60.0 + time[i].second) / dt ).astype(int)

        data_new[i] = davg[dind]

  return data_new, ind_nan


def download_data(date, grid, area, folder='.'):
  # Create folder if not existed
  mkdir_p(folder)

  #--------------------------------------------------------------------------------------------------------------------#
  # ECMWF
  #--------------------------------------------------------------------------------------------------------------------#
  print('+++++ Downloading ECMWF data ...')

  #
  # Set parameters
  #
  # Extended one day to both ends of the download dates
  one_day = datetime.timedelta(days=1)
  date_dl =  np.array([date[0]-one_day, date[1]+one_day])  # download two more days for interpolation
  date_dlstr = get_date_string(date_dl)
  date_in = '{0:8s}/to/{1:8s}'.format(*date_dlstr)

  grid_in = '{0:5.3f}/{1:5.3f}'.format(*grid)
  area_in = '{0:8.3f}/{1:8.3f}/{2:8.3f}/{3:8.3f}'.format(*area)

  ECMWF_prefix = folder + '/{0:8s}_{1:8s}-'.format(*date_dlstr)
  
  #
  # Surface
  #   175.128: surface thermal radiation downwards
  #
  type_in = 'fc'
  time_in = '00/12'
  step_in = '3/6/9/12'
  levtype_in = 'sfc'  # levelist uses default value

  param_list = ['175.128',]
  target_list = [ ECMWF_prefix + i for i in ['ECMWF_strd.nc',] ]
  force_download_list = [False,]

  # Download all the variables
  for param_in, target_in, force_download in zip(param_list, target_list, force_download_list):
    print('----', param_in, target_in)
    download_ECMWF_data(type_in=type_in, \
      date_in=date_in, time_in=time_in, step_in=step_in, \
      grid_in=grid_in, area_in=area_in, levtype_in=levtype_in, \
      param_in=param_in, \
      target_in=target_in, \
      force_download=force_download)
	
  #
  # Pressure levels
  #   129.128: geopotential
  #   130.128: temperature
  #   131.128: u component of wind
  #   132.128: v component of wind
  #   133.128: specific humidity
  #
  type_in = 'an'
  time_in = '00/06/12/18'
  step_in = '0'
  levtype_in = 'pl'
  levelist = [1, 2, 3, 5, 7, \
    10, 20, 30, 50, 70, \
    100, 125, 150, 175, 200, \
    225, 250, 300, 350, 400, \
    450, 500, 550, 600, 650, \
    700, 750, 775, 800, 825, \
    850, 875, 900, 925, 950, \
    975, 1000]
  levelist_in = '/'.join([str(l) for l in levelist])

  param_list = ['129.128', '130.128', '131.128', '132.128', '133.128']
  target_list = [ ECMWF_prefix + i \
    for i in ['ECMWF_geop.nc', 'ECMWF_temp.nc', 'ECMWF_uwind.nc', 'ECMWF_vwind.nc', 'ECMWF_qv.nc'] ]
  force_download_list = [False, False, False, False, False]

	# Download all the variables
  for param_in, target_in, force_download in \
    zip(param_list, target_list, force_download_list):
    print('----', param_in, target_in)
    download_ECMWF_data(type_in=type_in, \
      date_in=date_in, time_in=time_in, step_in=step_in, \
      grid_in=grid_in, area_in=area_in, levtype_in=levtype_in, levelist_in=levelist_in, \
      param_in=param_in, \
      target_in=target_in, \
      force_download=force_download)

  #--------------------------------------------------------------------------------------------------------------------#
  # SMEAR Data
  #--------------------------------------------------------------------------------------------------------------------#
  print('+++++ Downloading SMEAR data ...')

  # Start time and end time, the end time includes 24:00 of the last day
  one_day = datetime.timedelta(days=1)
  half_hour = datetime.timedelta(minutes=30)
  one_second = datetime.timedelta(seconds=1)

  start_date = date[0]
  end_date = date[1] + one_day

  # Some variable need more time to do the average, so 29 minutes are added in date1
  date0 = '{0:04d}-{1:02d}-{2:02d} {3:02d}:{4:02d}:{5:02d}'.format(start_date.year, start_date.month, start_date.day,  0,  0,  0)
  date1 = '{0:04d}-{1:02d}-{2:02d} {3:02d}:{4:02d}:{5:02d}'.format(end_date.year, end_date.month, end_date.day, 0, 29, 0)

  t_inv = 1800.0  # [s]
  time_30min = np.array([ date[0]+datetime.timedelta(seconds=i) \
    for i in range( 0, int(round((end_date-start_date+one_second).total_seconds())), int(round(t_inv)) ) ])
  nt_30min = time_30min.size

  t_inv = 60.0  # [s]
  time_1min = np.array([ date[0]+datetime.timedelta(seconds=i) \
    for i in range( 0, int(round((end_date-start_date+one_second).total_seconds())), int(round(t_inv)) ) ])
  nt_1min = time_1min.size

  #
  # Air temperature: [degC]
  #
  print('---- Downloading SMEAR temperature ...')

  param_list_temp = [\
    ('T42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('T84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('T168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('T336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('T504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('T672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('T1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ) ]

  # Download original data files
  for pl in param_list_temp:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_temp = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 125.0])

  # Combine data
  SMEAR_temp = combine_SMEAR_data(time_30min, levels_temp, param_list_temp, folder)

  # Correction and gap filling
  # Now in SOSAA the observed temperature data are not used for nudging if they are NAN, so there is no need to do
  # correction and gap filling.

  # Write combined data
  fname = folder + '/SMEAR_temp.txt'
  write_combined_SMEAR_data(SMEAR_temp, time_30min, levels_temp, fname)

  #
  # Wind speed: [m s-1]
  #
  print('---- Downloading SMEAR wind speed ...')

  param_list_wind = [\
    ('WSU84'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('WSU168' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('WSU336' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('WSU672' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('WSU740' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ) ]

  # Download original data files
  for pl in param_list_wind:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_wind = np.array([8.4, 16.8, 33.6, 67.2, 74.0])

  # Combine data
  SMEAR_wind = combine_SMEAR_data(time_30min, levels_wind, param_list_wind, folder)

  # Correction and gap filling
  # Now in SOSAA the observed wind data are used to calculate uwind and vwind, but not used directly.

  # Write combined data
  fname = folder + '/SMEAR_wind.txt'
  write_combined_SMEAR_data(SMEAR_wind, time_30min, levels_wind, fname)

  #
  # Wind direction: [deg]
  # Refer to: http://climate.umn.edu/snow_fence/components/winddirectionanddegreeswithouttable3.htm
  #
  print('---- Downloading SMEAR wind direction ...')

  param_list_wdir = [\
    ('WDU84'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('WDU168' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('WDU336' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('WDU672' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('WDU740' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ) ]

  # Download original data files
  for pl in param_list_wdir:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_wdir = np.array([8.4, 16.8, 33.6, 67.2, 74.0])

  # Combine them
  SMEAR_wdir = combine_SMEAR_data(time_30min, levels_wdir, param_list_wdir, folder)

  # Correction and gap filling
  # Now in SOSAA the observed wind direction data are used to calculate uwind and vwind, but not used directly.

  # Write combined data
  fname = folder + '/SMEAR_wdir.txt'
  write_combined_SMEAR_data(SMEAR_wdir, time_30min, levels_wdir, fname)

  #
  # Water vapor mixing ratio: [ppth]
  #
  print('---- Downloading SMEAR mixing ratio ...')

  param_list_mrvH2O = [\
    ('H2O42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('H2O84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('H2O168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('H2O336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('H2O504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('H2O672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('H2O1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('H2O1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ) ]

  # Download original data files
  for pl in param_list_mrvH2O:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_mrvH2O = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_mrvH2O = combine_SMEAR_data(time_30min, levels_mrvH2O, param_list_mrvH2O, folder)

  # Correction and gap filling
  # Now in SOSAA the observed wv mixing ratio data are used to calculate rhov, but not used directly.

  # Write combined data
  fname = folder + '/SMEAR_mrvH2O.txt'
  write_combined_SMEAR_data(SMEAR_mrvH2O, time_30min, levels_mrvH2O, fname)

  #
  # Global radiation (0.3 - 4.8 um) at 18 m: [W m-2]
  #
  print('---- Downloading SMEAR global radiation at 18 m ...')

  param_list_glob = [\
    ('Glob' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ) ]

  # Download original data files
  for pl in param_list_glob:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_glob = 18.0

  # Combine data
  SMEAR_glob = combine_SMEAR_data(time_30min, levels_glob, param_list_glob, folder)

  # Correction and gap filling
  # SMEAR_glob, SMEAR_glob_indnan = gap_filling(SMEAR_glob, time_30min, 'Gierens2014')
  SMEAR_glob, SMEAR_glob_indnan = gap_filling(SMEAR_glob, time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_glob.txt'
  write_combined_SMEAR_data(SMEAR_glob, time_30min, levels_glob, fname)

  #
  # PAR (400 - 700 nm): [umol m-2 s-1]
  #
  print('---- Downloading SMEAR PAR ...')

  param_list_PAR= [\
    ('PAR' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ) ]

  # Download original data files
  for pl in param_list_PAR:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_PAR = 18.0

  # Combine data
  SMEAR_PAR = combine_SMEAR_data(time_30min, levels_PAR, param_list_PAR, folder)

  # Correction and gap filling
  # SMEAR_PAR, SMEAR_PAR_indnan = gap_filling(SMEAR_PAR, time_30min, 'Gierens2014')
  SMEAR_PAR, SMEAR_PAR_indnan = gap_filling(SMEAR_PAR, time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_PAR.txt'
  write_combined_SMEAR_data(SMEAR_PAR, time_30min, levels_PAR, fname)

  #
  # Global radiation at mast at 125 m (0.3 - 4.8 um): [W m-2]
  #
  print('---- Downloading SMEAR global radiation at 125 m ...')

  param_list_glob125 = [\
    ('Globmast' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ) ]

  # Download original data files
  for pl in param_list_glob125:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_glob125 = 125.0

  # Combine data
  SMEAR_glob125 = combine_SMEAR_data(time_30min, levels_glob125, param_list_glob125, folder)

  # Correction and gap filling
  # SMEAR_glob125, SMEAR_glob125_indnan = gap_filling(SMEAR_glob125, time_30min, 'Gierens2014')
  SMEAR_glob125, SMEAR_glob125_indnan = gap_filling(SMEAR_glob125, time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_glob125.txt'
  write_combined_SMEAR_data(SMEAR_glob125, time_30min, levels_glob125, fname)

  #
  # Reflected global radiation at mast at 125 m (0.3 - 4.8 um): [W m-2]
  #
  print('---- Downloading SMEAR reflected global radiation at 125 m ...')

  param_list_rglob125= [\
    ('RGlob125' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ) ]

  # Download original data files
  for pl in param_list_rglob125:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_rglob125 = 125.0

  # Combine data
  SMEAR_rglob125 = combine_SMEAR_data(time_30min, levels_rglob125, param_list_rglob125, folder)

  # Correction and gap filling
  # SMEAR_rglob125, SMEAR_rglob125_indnan = gap_filling(SMEAR_rglob125, time_30min, 'Gierens2014')
  SMEAR_rglob125, SMEAR_rglob125_indnan = gap_filling(SMEAR_rglob125, time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_rglob125.txt'
  write_combined_SMEAR_data(SMEAR_rglob125, time_30min, levels_rglob125, fname)

  #
  # Pressure (at 180 m a.s.l., locally at ground level): [hPa]
  #
  print('---- Downloading SMEAR ground level air pressure ...')

  param_list_pres= [\
    ('Pamb0' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ) ]

  # Download original data files
  for pl in param_list_pres:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_pres = 0.0

  # Combine data
  SMEAR_pres = combine_SMEAR_data(time_30min, levels_pres, param_list_pres, folder)

  # Correction and gap filling
  # SMEAR_pres, SMEAR_pres_indnan = gap_filling(SMEAR_pres, time_30min, 'Gierens2014')
  SMEAR_pres, SMEAR_pres_indnan = gap_filling(SMEAR_pres, time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_pres.txt'
  write_combined_SMEAR_data(SMEAR_pres, time_30min, levels_pres, fname)

  #
  # Soil volumetric water content: [m3 m-3]
  #   -5 - 0 cm for organic layer: wsoil_humus
  #   2-6 cm in mineral soil: wsoil_A
  #   14-25 cm in mineral soil: wsoil_B1
  #   26-36 cm in mineral soil: wsoil_B2
  #   38-61 cm in mineral soil: wsoil_C1
  #
  print('---- Downloading SMEAR soil volumetric water content ...')

  param_list_soilmoist = [\
    ('wsoil_humus' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('wsoil_A'     , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('wsoil_B1'    , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('wsoil_B2'    , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('wsoil_C1'    , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_soilmoist:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_soilmoist = np.array([-2.5, 4.0, 19.5, 31.0, 49.5])

  # Combine data
  SMEAR_soilmoist = combine_SMEAR_data(time_30min, levels_soilmoist, param_list_soilmoist, folder)

  # Correction and gap filling
  SMEAR_soilmoist_indnan = np.zeros( (nt_30min, levels_soilmoist.size) )
  for il, lev in enumerate(levels_soilmoist):
    SMEAR_soilmoist[:, il], SMEAR_soilmoist_indnan[:, il] = gap_filling(SMEAR_soilmoist[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_soilmoist.txt'
  write_combined_SMEAR_data(SMEAR_soilmoist, time_30min, levels_soilmoist, fname)

  #
  # Soil temperature: [degC]
  #   -5 - 0 cm for organic layer: tsoil_humus
  #   2-5 cm in mineral soil: tsoil_A
  #   9-14 cm in mineral soil: tsoil_B1
  #   22-29 cm in mineral soil: tsoil_B2
  #   42-58 cm in mineral soil: tsoil_C1
  #
  print('---- Downloading SMEAR soil temperature ...')

  param_list_soiltemp = [\
    ('tsoil_humus' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('tsoil_A'     , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('tsoil_B1'    , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('tsoil_B2'    , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('tsoil_C1'    , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_soiltemp:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_soiltemp = np.array([-2.5, 3.5, 11.5, 25.5, 50.0])

  # Combine data
  SMEAR_soiltemp = combine_SMEAR_data(time_30min, levels_soiltemp, param_list_soiltemp, folder)

  # Correction and gap filling
  SMEAR_soiltemp_indnan = np.zeros( (nt_30min, levels_soiltemp.size) )
  for il, lev in enumerate(levels_soiltemp):
    SMEAR_soiltemp[:, il], SMEAR_soiltemp_indnan[:, il] = gap_filling(SMEAR_soiltemp[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_soiltemp.txt'
  write_combined_SMEAR_data(SMEAR_soiltemp, time_30min, levels_soiltemp, fname)

  #
  # Soil heat flux: [W m-2]
  #   soil heat flux + heat storage change in the topsoil above the heat flux plates (mean of 5 locations)
  #
  print('---- Downloading SMEAR soil heat flux ...')

  param_list_gsoil = [\
    ('G_sc' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_gsoil:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_gsoil = 0.0

  # Combine data
  SMEAR_gsoil = combine_SMEAR_data(time_30min, levels_gsoil, param_list_gsoil, folder)

  # Correction and gap filling
  SMEAR_gsoil, SMEAR_gsoil_indnan = gap_filling(SMEAR_gsoil, time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_gsoil.txt'
  write_combined_SMEAR_data(SMEAR_gsoil, time_30min, levels_gsoil, fname)

  print('---- Downloading SMEAR flux data ...')

  #
  # Gas concentration: [ppb]
  #   O3, SO2, NO, NOx, CO
  #   Use the average concentration from all the height levels.
  #   The output should be in the order finally: O3, SO2, NO, NO2, CO
  #
  print('---- Downloading SMEAR gas concentrations ...')
  
  #
  # O3
  # Jarvi2009:
  #   instrument: TEI 49
  #   detection limit: 0.5 ppb
  #
  print('O3')

  detection_limit_O3 = 0.5  # [ppb]

  param_list_O3 = [\
    ('O342'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('O384'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('O3168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('O3336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('O3504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('O3672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('O31010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('O31250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_O3:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_O3 = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_O3 = combine_SMEAR_data(time_30min, levels_O3, param_list_O3, folder)

  # Correction and gap filling
  SMEAR_O3_indnan = np.zeros( (nt_30min, levels_O3.size) )
  for il, lev in enumerate(levels_O3):
    SMEAR_O3[:, il], SMEAR_O3_indnan[:, il] = gap_filling(SMEAR_O3[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_O3.txt'
  write_combined_SMEAR_data(SMEAR_O3, time_30min, levels_O3, fname)

  #
  # SO2
  # Kampf2012, ACP:
  #   instrument: TEI 43 CTL
  #   detection limit: 0.1 ppb
  # Jarvi2009, BER:
  #   instrument: Horiba APSA 360
  #   detection limit: 0.2 ppb
  #
  print('SO2')

  detection_limit_SO2 = 0.005  # [ppb]

  param_list_SO2 = [\
    ('SO242'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('SO284'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('SO2168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('SO2336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('SO2504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('SO2672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('SO21010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('SO21250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_SO2:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_SO2 = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_SO2 = combine_SMEAR_data(time_30min, levels_SO2, param_list_SO2, folder)

  # Correction and gap filling
  SMEAR_SO2_indnan = np.zeros( (nt_30min, levels_SO2.size) )
  for il, lev in enumerate(levels_SO2):
    SMEAR_SO2[:, il], SMEAR_SO2_indnan[:, il] = gap_filling(SMEAR_SO2[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_SO2.txt'
  write_combined_SMEAR_data(SMEAR_SO2, time_30min, levels_SO2, fname)

  #
  # NO
  # No reference for its detection limit, so set to 0.0
  #
  print('NO')

  detection_limit_NO = 0.005  # [ppb]

  param_list_NO = [\
    ('NO42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('NO84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('NO168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('NO336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('NO504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('NO672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('NO1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('NO1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_NO:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_NO = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_NO = combine_SMEAR_data(time_30min, levels_NO, param_list_NO, folder)

  # Correction and gap filling
  SMEAR_NO_indnan = np.zeros( (nt_30min, levels_NO.size) )
  for il, lev in enumerate(levels_NO):
    SMEAR_NO[:, il], SMEAR_NO_indnan[:, il] = gap_filling(SMEAR_NO[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_NO.txt'
  write_combined_SMEAR_data(SMEAR_NO, time_30min, levels_NO, fname)

  #
  # NOx
  # Jarvi2009, BER:
  #   instrument: TEI 42S
  #   detection limit: 0.2 ppb
  #
  print('NOx')

  detection_limit_NOx = 0.05  # [ppb]

  param_list_NOx = [\
    ('NOx42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('NOx84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('NOx168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('NOx336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('NOx504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('NOx672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('NOx1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('NOx1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_NOx:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_NOx = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_NOx = combine_SMEAR_data(time_30min, levels_NOx, param_list_NOx, folder)

  # Correction and gap filling
  SMEAR_NOx_indnan = np.zeros( (nt_30min, levels_NOx.size) )
  for il, lev in enumerate(levels_NOx):
    SMEAR_NOx[:, il], SMEAR_NOx_indnan[:, il] = gap_filling(SMEAR_NOx[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_NOx.txt'
  write_combined_SMEAR_data(SMEAR_NOx, time_30min, levels_NOx, fname)

  #
  # CO
  # Jarvi2009, BER:
  #   instrument: Horiba APMA 370
  #   detection limit: 20 ppb
  #
  print('CO')

  detection_limit_CO = 20  # [ppb]

  param_list_CO = [\
    ('CO42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('CO84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('CO168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('CO336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('CO504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('CO672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('CO1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('CO1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_CO:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_CO = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_CO = combine_SMEAR_data(time_30min, levels_CO, param_list_CO, folder)

  # Correction and gap filling
  SMEAR_CO_indnan = np.zeros( (nt_30min, levels_CO.size) )
  for il, lev in enumerate(levels_CO):
    SMEAR_CO[:, il], SMEAR_CO_indnan[:, il] = gap_filling(SMEAR_CO[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_CO.txt'
  write_combined_SMEAR_data(SMEAR_CO, time_30min, levels_CO, fname)


# monoterpenes
  print('monoterpenes')

  param_list_mono = [\
    ('VOC_M137_42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M137_84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M137_168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M137_336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M137_504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M137_672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M137_1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M137_1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_mono:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_mono = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_mono = combine_SMEAR_data(time_30min, levels_mono, param_list_mono, folder)

  # Correction and gap filling
  SMEAR_mono_indnan = np.zeros( (nt_30min, levels_mono.size) )
  for il, lev in enumerate(levels_mono):
    SMEAR_mono[:, il], SMEAR_mono_indnan[:, il] = gap_filling(SMEAR_mono[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_mono.txt'
  write_combined_SMEAR_data(SMEAR_mono, time_30min, levels_mono, fname)

# methanol concentration
  print('CH3OH')

  param_list_CH3OH = [\
    ('VOC_M33_42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M33_84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M33_168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M33_336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M33_504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M33_672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M33_1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M33_1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_CH3OH:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_CH3OH = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_CH3OH = combine_SMEAR_data(time_30min, levels_CH3OH, param_list_CH3OH, folder)

  # Correction and gap filling
  SMEAR_CH3OH_indnan = np.zeros( (nt_30min, levels_CH3OH.size) )
  for il, lev in enumerate(levels_CH3OH):
    SMEAR_CH3OH[:, il], SMEAR_CH3OH_indnan[:, il] = gap_filling(SMEAR_CH3OH[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_CH3OH.txt'
  write_combined_SMEAR_data(SMEAR_CH3OH, time_30min, levels_CH3OH, fname)


# acetonitrile concentration
  print('acetonitrile')

  param_list_acetonitrile = [\
    ('VOC_M33_42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M33_84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M33_168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M33_336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M33_504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M33_672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M33_1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M33_1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_acetonitrile:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_acetonitrile = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_acetonitrile = combine_SMEAR_data(time_30min, levels_acetonitrile, param_list_acetonitrile, folder)

  # Correction and gap filling
  SMEAR_acetonitrile_indnan = np.zeros( (nt_30min, levels_acetonitrile.size) )
  for il, lev in enumerate(levels_acetonitrile):
    SMEAR_acetonitrile[:, il], SMEAR_acetonitrile_indnan[:, il] = gap_filling(SMEAR_acetonitrile[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_acetonitrile.txt'
  write_combined_SMEAR_data(SMEAR_acetonitrile, time_30min, levels_acetonitrile, fname)

# acetaldehyde concentration 
# UNIT: nmol mol⁻¹
  print('acetaldehyde')

  param_list_CH3CHO = [\
    ('VOC_M45_42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M45_84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M45_168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M45_336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M45_504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M45_672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M45_1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M45_1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_CH3CHO:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_CH3CHO = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_CH3CHO = combine_SMEAR_data(time_30min, levels_CH3CHO, param_list_CH3CHO, folder)

  # Correction and gap filling
  SMEAR_CH3CHO_indnan = np.zeros( (nt_30min, levels_CH3CHO.size) )
  for il, lev in enumerate(levels_CH3CHO):
    SMEAR_CH3CHO[:, il], SMEAR_CH3CHO_indnan[:, il] = gap_filling(SMEAR_CH3CHO[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_CH3CHO.txt'
  write_combined_SMEAR_data(SMEAR_CH3CHO, time_30min, levels_CH3CHO, fname)

# C2H5OH+HCOOH concentration 
# UNIT: nmol mol⁻¹
  print('ethanol+formic acid')

  param_list_C2H5OH_HCOOH = [\
    ('VOC_M47_42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M47_84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M47_168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M47_336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M47_504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M47_672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M47_1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M47_1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_C2H5OH_HCOOH:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_C2H5OH_HCOOH = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_C2H5OH_HCOOH = combine_SMEAR_data(time_30min, levels_C2H5OH_HCOOH, param_list_C2H5OH_HCOOH, folder)

  # Correction and gap filling
  SMEAR_C2H5OH_HCOOH_indnan = np.zeros( (nt_30min, levels_C2H5OH_HCOOH.size) )
  for il, lev in enumerate(levels_C2H5OH_HCOOH):
    SMEAR_C2H5OH_HCOOH[:, il], SMEAR_C2H5OH_HCOOH_indnan[:, il] = gap_filling(SMEAR_C2H5OH_HCOOH[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_C2H5OH_HCOOH.txt'
  write_combined_SMEAR_data(SMEAR_C2H5OH_HCOOH, time_30min, levels_C2H5OH_HCOOH, fname)

# acetone concentration 
# UNIT: nmol mol⁻¹
  print('acetone')

  param_list_acetone = [\
    ('VOC_M59_42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M59_84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M59_168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M59_336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M59_504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M59_672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M59_1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M59_1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_acetone:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_acetone = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_acetone = combine_SMEAR_data(time_30min, levels_acetone, param_list_acetone, folder)

  # Correction and gap filling
  SMEAR_acetone_indnan = np.zeros( (nt_30min, levels_acetone.size) )
  for il, lev in enumerate(levels_acetone):
    SMEAR_acetone[:, il], SMEAR_acetone_indnan[:, il] = gap_filling(SMEAR_acetone[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_acetone.txt'
  write_combined_SMEAR_data(SMEAR_acetone, time_30min, levels_acetonitrile, fname)

# CH3COOH concentration 
# UNIT: nmol mol⁻¹
  print('acetic acid')

  param_list_CH3COOH = [\
    ('VOC_M61_42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M61_84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M61_168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M61_336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M61_504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M61_672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M61_1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M61_1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_CH3COOH:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_CH3COOH = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_CH3COOH = combine_SMEAR_data(time_30min, levels_CH3COOH, param_list_CH3COOH, folder)

  # Correction and gap filling
  SMEAR_CH3COOH_indnan = np.zeros( (nt_30min, levels_CH3COOH.size) )
  for il, lev in enumerate(levels_CH3COOH):
    SMEAR_CH3COOH[:, il], SMEAR_CH3COOH_indnan[:, il] = gap_filling(SMEAR_CH3COOH[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_CH3COOH.txt'
  write_combined_SMEAR_data(SMEAR_CH3COOH, time_30min, levels_CH3COOH, fname)

# C5H8 + MBO concentration 
# UNIT: nmol mol⁻¹
  print('isoprene+MBO')

  param_list_C5H8_MBO = [\
    ('VOC_M69_42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M69_84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M69_168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M69_336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M69_504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M69_672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M69_1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M69_1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_C5H8_MBO:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_C5H8_MBO = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_C5H8_MBO = combine_SMEAR_data(time_30min, levels_C5H8_MBO, param_list_C5H8_MBO, folder)

  # Correction and gap filling
  SMEAR_C5H8_MBO_indnan = np.zeros( (nt_30min, levels_C5H8_MBO.size) )
  for il, lev in enumerate(levels_C5H8_MBO):
    SMEAR_C5H8_MBO[:, il], SMEAR_C5H8_MBO_indnan[:, il] = gap_filling(SMEAR_C5H8_MBO[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_C5H8_MBO.txt'
  write_combined_SMEAR_data(SMEAR_C5H8_MBO, time_30min, levels_C5H8_MBO, fname)

# MACR+MVK concentration 
# UNIT: nmol mol⁻¹
  print('MACR+MVK')

  param_list_MACR_MVK = [\
    ('VOC_M71_42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M71_84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M71_168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M71_336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M71_504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M71_672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M71_1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M71_1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_MACR_MVK:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_MACR_MVK = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_MACR_MVK = combine_SMEAR_data(time_30min, levels_MACR_MVK, param_list_MACR_MVK, folder)

  # Correction and gap filling
  SMEAR_MACR_MVK_indnan = np.zeros( (nt_30min, levels_MACR_MVK.size) )
  for il, lev in enumerate(levels_MACR_MVK):
    SMEAR_MACR_MVK[:, il], SMEAR_MACR_MVK_indnan[:, il] = gap_filling(SMEAR_MACR_MVK[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_MACR_MVK.txt'
  write_combined_SMEAR_data(SMEAR_MACR_MVK, time_30min, levels_MACR_MVK, fname)

# MEK concentration 
# UNIT: nmol mol⁻¹
  print('MEK')

  param_list_MEK = [\
    ('VOC_M73_42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M73_84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M73_168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M73_336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M73_504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M73_672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M73_1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M73_1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_MEK:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_MEK = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_MEK = combine_SMEAR_data(time_30min, levels_MEK, param_list_MEK, folder)

  # Correction and gap filling
  SMEAR_MEK_indnan = np.zeros( (nt_30min, levels_MEK.size) )
  for il, lev in enumerate(levels_MEK):
    SMEAR_MEK[:, il], SMEAR_MEK_indnan[:, il] = gap_filling(SMEAR_MEK[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_MEK.txt'
  write_combined_SMEAR_data(SMEAR_MEK, time_30min, levels_MEK, fname)

# benzene concentration 
# UNIT: nmol mol⁻¹
  print('benzene')

  param_list_benzene = [\
    ('VOC_M79_42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M79_84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M79_168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M79_336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M79_504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M79_672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M79_1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M79_1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_benzene:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_benzene = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_benzene = combine_SMEAR_data(time_30min, levels_benzene, param_list_benzene, folder)

  # Correction and gap filling
  SMEAR_benzene_indnan = np.zeros( (nt_30min, levels_benzene.size) )
  for il, lev in enumerate(levels_benzene):
    SMEAR_benzene[:, il], SMEAR_benzene_indnan[:, il] = gap_filling(SMEAR_benzene[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_benzene.txt'
  write_combined_SMEAR_data(SMEAR_benzene, time_30min, levels_benzene, fname)

# toluene+p-cymene concentration 
# UNIT: nmol mol⁻¹
  print('toluene+p-cymene')

  param_list_toluene_pcymene = [\
    ('VOC_M93_42'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M93_84'   , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M93_168'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M93_336'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M93_504'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M93_672'  , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M93_1010' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), \
    ('VOC_M93_1250' , 'HYY_META' , date0, date1, 'ANY', '30MIN', 'ARITHMETIC', ), ]

  # Download original data files
  for pl in param_list_toluene_pcymene:
    download_SMEAR_data(pl, False, folder)

  # Set height levels
  levels_toluene_pcymene = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 101.0, 125.0])

  # Combine data
  SMEAR_toluene_pcymene = combine_SMEAR_data(time_30min, levels_toluene_pcymene, param_list_toluene_pcymene, folder)

  # Correction and gap filling
  SMEAR_toluene_pcymene_indnan = np.zeros( (nt_30min, levels_toluene_pcymene.size) )
  for il, lev in enumerate(levels_toluene_pcymene):
    SMEAR_toluene_pcymene[:, il], SMEAR_toluene_pcymene_indnan[:, il] = gap_filling(SMEAR_toluene_pcymene[:, il], time_30min, 'Hybrid_Zhou')

  # Write combined data
  fname = folder + '/SMEAR_toluene_pcymene.txt'
  write_combined_SMEAR_data(SMEAR_toluene_pcymene, time_30min, levels_toluene_pcymene, fname)

  #
  # Calculate average gas concentrations
  #
  print('---- Calculating SMEAR average gas concentrations ...')
  
  # O3
  print('O3')
  levels_O3avg = np.nanmean(levels_O3)
  SMEAR_O3avg = np.nanmean(SMEAR_O3, axis=1)
  SMEAR_O3avg[SMEAR_O3avg < detection_limit_O3] = detection_limit_O3
  fname = folder + '/SMEAR_O3avg.txt'
  write_combined_SMEAR_data(SMEAR_O3avg, time_30min, levels_O3avg, fname)

  # SO2
  print('SO2')
  levels_SO2avg = np.nanmean(levels_SO2)
  SMEAR_SO2avg = np.nanmean(SMEAR_SO2, axis=1)
  SMEAR_SO2avg[SMEAR_SO2avg < detection_limit_SO2] = detection_limit_SO2
  fname = folder + '/SMEAR_SO2avg.txt'
  write_combined_SMEAR_data(SMEAR_SO2avg, time_30min, levels_SO2avg, fname)

  # NO
  print('NO')
  levels_NOavg = np.nanmean(levels_NO)
  SMEAR_NOavg = np.nanmean(SMEAR_NO, axis=1)
  SMEAR_NOavg[SMEAR_NOavg < detection_limit_NO] = detection_limit_NO
  fname = folder + '/SMEAR_NOavg.txt'
  write_combined_SMEAR_data(SMEAR_NOavg, time_30min, levels_NOavg, fname)

  # NOx
  print('NOx')
  levels_NOxavg = np.nanmean(levels_NOx)
  SMEAR_NOxavg = np.nanmean(SMEAR_NOx, axis=1)
  SMEAR_NOxavg[SMEAR_NOxavg < detection_limit_NOx] = detection_limit_NOx
  fname = folder + '/SMEAR_NOxavg.txt'
  write_combined_SMEAR_data(SMEAR_NOxavg, time_30min, levels_NOxavg, fname)

  # CO
  print('CO')
  levels_COavg = np.nanmean(levels_CO)
  SMEAR_COavg = np.nanmean(SMEAR_CO, axis=1)
  SMEAR_COavg[SMEAR_COavg < detection_limit_CO] = detection_limit_CO
  fname = folder + '/SMEAR_COavg.txt'
  write_combined_SMEAR_data(SMEAR_COavg, time_30min, levels_COavg, fname)

  # NO2 = NOx - NO
  print('NO2')
  levels_NO2 = levels_NO
  SMEAR_NO2 = SMEAR_NOx - SMEAR_NO
  fname = folder + '/SMEAR_NO2.txt'
  write_combined_SMEAR_data(SMEAR_NO2, time_30min, levels_NO2, fname)

  levels_NO2avg = levels_NOavg
  SMEAR_NO2avg = SMEAR_NOxavg - SMEAR_NOavg
  fname = folder + '/SMEAR_NO2avg.txt'
  write_combined_SMEAR_data(SMEAR_NO2avg, time_30min, levels_NO2avg, fname)

  #
  # Calculate albedo at 125 m
  #
  print('---- Not calculating SMEAR albedo ...')

  # Set height levels
#  levels_albedo = 125.0

  # albedo = rglob125 / glob125
#  SMEAR_albedo = SMEAR_rglob125/SMEAR_glob125
#  SMEAR_albedo = np.minimum( np.maximum( SMEAR_albedo, 0.0 ), 1.0 )  # keep nan values

  # Write data
#  fname = folder + '/SMEAR_albedo.txt'
#  write_combined_SMEAR_data(SMEAR_albedo, time_30min, levels_albedo, fname)

  #
  # Calculate uwind and vwind
  #
  print('---- Calculating SMEAR uwind and vwind ...')

  # Set height levels
  levels_uwind = np.array([8.4, 16.8, 33.6, 67.2, 74.0])
  levels_vwind = np.array([8.4, 16.8, 33.6, 67.2, 74.0])

  # u = wind*cos(270-wdir), v = wind*sin(270-wdir)
  SMEAR_uwind = SMEAR_wind*np.cos( (270.0-SMEAR_wdir) * np.pi/180.0 )
  SMEAR_vwind = SMEAR_wind*np.sin( (270.0-SMEAR_wdir) * np.pi/180.0 )

  # Write data
  fname = folder + '/SMEAR_uwind.txt'
  write_combined_SMEAR_data(SMEAR_uwind, time_30min, levels_uwind, fname)

  fname = folder + '/SMEAR_vwind.txt'
  write_combined_SMEAR_data(SMEAR_vwind, time_30min, levels_vwind, fname)

  #
  # Convert mixing ratio in volume to absolute humidity
  #
  print('---- Calculating SMEAR absolute humidity ...')

  # Set height levels
  levels_rhov = np.array([4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 125.0])  # the same as temperature

  # Since,
  #   rho_v = rho_d*mrv_v = (rho - rho_v)*mrv_v
  # So,
  #   rho_v = rho*mrv_v / (1 + mrv_v) = P*Ma/(RT) * mrv_v/(1+mrv_v)
  Ma = 28.97e-3  # [kg mol-1], molar mass of air
  Rgas = 8.3144598  # [J mol-1 K-1], gas constant
  s_pres = SMEAR_pres * 100.0  # [Pa], pressure
  s_temp = SMEAR_temp[:, :]+273.15  # [K], temperature
  s_mrvH2O = SMEAR_mrvH2O[:, [0, 1, 2, 3, 4, 5, 7]]*1e-3  # [-], mixing ratio of H2O, only use the same height levels as the temperature
  SMEAR_rhov = s_pres[:, np.newaxis]*Ma / (Rgas*s_temp) * s_mrvH2O/(1.0+s_mrvH2O)  # [kg m-3], absolute humidity

  fname = folder + '/SMEAR_rhov.txt'
  write_combined_SMEAR_data(SMEAR_rhov, time_30min, levels_rhov, fname)

  # Test data
  # fg, ax = plt.subplots(1, 1)
  # ax.plot(time_30min, SMEAR_mrvH2O[:, 0]*1e-3, 'r')
  # ax.plot(time_30min, SMEAR_rhov[:, 0], 'b')
  # 
  # plt.show()

  # return some values
  return ECMWF_prefix


def read_netcdf_data(fname, vname):
  fid = netCDF4.Dataset(fname, mode='r')

  # Read variable, note that scale_factor and add_offset are considered already in netCDF4
  var_obj = fid.variables[vname]
  var = np.ma.filled(var_obj[:], fill_value=np.nan)  # masked array to numpy array with setting fill values to np.nan
  
  # Read time
  time_obj = fid.variables['time']
  time = netCDF4.num2date(time_obj[:], units=time_obj.units, calendar=time_obj.calendar)  # convert to datetime type
  
  # Read latitude, longitude and level
  lat = fid.variables['latitude'][::-1]  # latitude is saved from north to south, so here I have reversed it
  lon = fid.variables['longitude'][:]
  if 'level' in fid.variables.keys():  # pressure levels
    lev = fid.variables['level'][:]
  else:  # only surface
    lev = -1
  
  return var, time, lev, lat, lon


def write_netcdf_file_example():
  # Open a new netCDF file for writing.
  fname = 'write_netcdf_example.nc'
  fid = netCDF4.Dataset(fname,'w') 

  # Variables and dimensions
  nlats = 2; nlons = 3 

  # Dimensions: latitudes and longitudes
  lats_out = 0 + 5.0*np.arange(nlats,dtype='float32')
  lons_out = 0 + 5.0*np.arange(nlons,dtype='float32')

  # Variables: pressure and temperature
  press_out = 900. + np.arange(nlats*nlons,dtype='float32')  # 1d array
  press_out[0] = -32767
  press_out.shape = (nlats, nlons)  # reshape to 2d array
  temp_out = 9. + 0.25*np.arange(nlats*nlons,dtype='float32')  # 1d array
  temp_out.shape = (nlats,nlons)  # reshape to 2d array

  #
  # Create the lat and lon dimensions.
  #
  fid.createDimension('latitude', nlats)
  fid.createDimension('longitude', nlons)

  # Define the coordinate variables. They will hold the coordinate information, that is, the latitudes and longitudes.
  lats = fid.createVariable('latitude', np.dtype('float32').char, ('latitude',))
  lons = fid.createVariable('longitude', np.dtype('float32').char, ('longitude',))

  # Assign units attributes to coordinate var data. This attaches a text attribute to each of the coordinate variables,
  # containing the units.
  lats.units = 'degrees_north'
  lons.units = 'degrees_east'

  # Write data to coordinate vars.
  lats[:] = lats_out
  lons[:] = lons_out

  #
  # create the pressure and temperature variables 
  #
  press = ncfile.createVariable('pressure', np.dtype('float32').char, ('latitude','longitude'), fill_value=-32767)
  temp = ncfile.createVariable('temperature', np.dtype('float32').char, ('latitude','longitude'))

  # Set the units attribute.
  press.units =  'hPa'
  temp.units = 'celsius'

  # Write data to variables.
  press[:] = press_out
  temp[:] = temp_out

  # Close the file.
  fid.close()
  
  #
  # Test
  #
  fid_read = netCDF4.Dataset(fname, 'r')

  # Read data
  # Convert masked array --> numpy array with setting fill_value to np.nan
  press_read = np.ma.filled( ncfile_read.variables['pressure'][:], fill_value=np.nan )


def integrated_to_simultaneous(data, cnum, interval):
  """
  The size of data should be multiples of cnum (cycle number).
  """
  nd = data.size
  data_new = np.ones(nd)*np.nan
  for i in range(cnum):
    if i == 0:
      data_new[i::cnum] = data[i::cnum] / interval  # first value of forcast every cnum
    else:
      data_new[i::cnum] = (data[i::cnum] - data[i-1::cnum]) / interval  # current value - previous value every cnum
  
  return data_new


def integrated_to_simultaneous_sfc(data, cnum, interval):
  """
  data: [time, lat, lon]
  """
  n1 = data.shape[1]
  n2 = data.shape[2]
  data_real = np.copy(data)
  for i in range(n1):
    for j in range(n2):
      data_real[:, i, j] = integrated_to_simultaneous(data[:, i, j], cnum, interval)

  return data_real


def interpolate_loc_sfc(lat_s, lon_s, lat, lon, data):
  """
  data: [time, lat, lon], nlat=2, nlon=2
  """
  nt = data.shape[0]
  data_interp = np.ones(nt) * np.nan
  for it in range(nt):
    west = np.interp(lat_s, lat, data[it, :, 0])  # interpolate the west side
    east = np.interp(lat_s, lat, data[it, :, 1])  # interpolate the east side
    data_interp[it] = np.interp(lon_s, lon, np.array([west, east]))  # interpolate to local point

  return data_interp
  

def interpolate_loc_pl(lat_s, lon_s, lat, lon, data):
  """
  data: [time, lev, lat, lon], nlat=2, nlon=2
  """
  nt = data.shape[0]
  nl = data.shape[1]
  data_interp = np.ones((nt, nl)) * np.nan
  for it in range(nt):
    for il in range(nl):
      west = np.interp(lat_s, lat, data[it, il, :, 0])  # interpolate the west side
      east = np.interp(lat_s, lat, data[it, il, :, 1])  # interpolate the east side
      data_interp[it, il] = np.interp(lon_s, lon, np.array([west, east]))  # interpolate to local point

  return data_interp


def interpolate_time_sfc(time_d, data_d, date_i, time_inv):
  # Parameters
  time0 = date_i[0]  # datetime of time 0

  # xd, yd (data_d)
  xd = np.array( [ (i - time0).total_seconds() for i in time_d ] )

  # xi, yi
  one_day = datetime.timedelta(days=1)
  hours = int( round( (date_i[1] - date_i[0] + one_day).total_seconds() / 3600.0 ) )  # how many hours for this month
  date_interp = np.array( [date[0] + datetime.timedelta(hours=i) for i in range(0, hours+1, time_inv)] )
  xi = np.array( [ (i - time0).total_seconds() for i in date_interp ] )
  yi = np.interp(xi, xd, data_d)

  return yi, date_interp


def interpolate_time_pl(time_d, data_d, date_i, time_inv):
  # Parameters
  time0 = date_i[0]  # datetime of time 0
  nl = data_d.shape[1]

  # xd, yd (data_d)
  xd = np.array( [ (i - time0).total_seconds() for i in time_d ] )

  # xi, yi
  one_day = datetime.timedelta(days=1)
  hours = int( round( (date_i[1] - date_i[0] + one_day).total_seconds() / 3600.0 ) )  # how many hours for this month
  date_interp = np.array( [date[0] + datetime.timedelta(hours=i) for i in range(0, hours+1, time_inv)] )
  xi = np.array( [ (i - time0).total_seconds() for i in date_interp ] )
  yi = np.ones( (xi.size, nl) )
  for i in range(nl):
    yi[:, i] = np.interp(xi, xd, data_d[:, i])

  return yi, date_interp
  

def write_input_file_sfc(fname, data, time):
  with open(fname, 'w') as f:
    for it, t in enumerate(time):
      f.write('{0:8d}{1:8d}{2:8d}{3:8d}{4:8d}{5:8d}{6:25.16e}\n'.format(
        t.year, t.month, t.day, t.hour, t.minute, t.second, data[it]))


def write_input_file_pl(fname, data, time):
  nl = data.shape[1]
  with open(fname, 'w') as f:
    for it, t in enumerate(time):
      f.write('{0:8d}{1:8d}{2:8d}{3:8d}{4:8d}{5:8d}'.format(t.year, t.month, t.day, t.hour, t.minute, t.second) + \
        ''.join(['{:25.16e}']*nl).format(*data[it, :]) + '\n')


def get_date_string(date):
  nd = date.size
  date_str = []
  for i, d in enumerate(date):
    date_str.append('{0:4d}{1:02d}{2:02d}'.format(date[i].year, date[i].month, date[i].day))

  return date_str


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
# Main program
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
if __name__ == '__main__':
  #--------------------------------------#
  # Site parameters
  #
  # What you may want to change:
  # lat_hyy: site latitude, change it to your local value
  # lon_hyy: site longitude, change it to your local value
  # year
  # month
  # grid: the resolution you need for ECMWF data
  # area: The ranges of latitude and longitude containing the local site.
  #       Now you can only include 2 points for each direction.
  #--------------------------------------#
  print('========== Set parameters ... ==========')
  lat_hyy = 61.8500
  lon_hyy = 24.2833
  
  year = 2013
  month = 3
  monthrange = calendar.monthrange(year, month)[1]
  
  date = np.array([datetime.datetime(year, month, 1), datetime.datetime(year, month, monthrange)])  # start date and end date
  
  grid = [0.125, 0.125]  # grid resolution in latitude and longitude directions
  area = [61.875, 24.250, 61.750, 24.375]  # [N, W, S, E]
  
  print('lat, lon: ', lat_hyy, lon_hyy)
  print('date: ', date)
  print('grid: ', grid)
  print('area: ', area)
  
  folder = '{0:4d}{1:02d}'.format(year, month)
  
  # sys.exit()
  
  #--------------------------------------#
  # Download data
  #--------------------------------------#
  print('========== Downloading data ... ==========')
  
  ECMWF_prefix = download_data(date, grid, area, folder)
  
  #--------------------------------------#
  # Read data
  # time: the same for every variable
  # lev: -1 for strd, the same for others
  # lat, lon: the same for all
  #--------------------------------------#
  print('========== Reading data ... ==========')
  
  # Read original data
  # prefix = '{0:8s}_{1:8s}-'.format(*date_dlstr)
  strd , time_sfc, lev_sfc , lat, lon = read_netcdf_data(ECMWF_prefix+'ECMWF_strd.nc' , 'strd')
  geop , time_pl , lev_pl  , lat, lon = read_netcdf_data(ECMWF_prefix+'ECMWF_geop.nc' , 'z')
  temp , time_pl , lev_pl  , lat, lon = read_netcdf_data(ECMWF_prefix+'ECMWF_temp.nc' , 't')
  qv   , time_pl , lev_pl  , lat, lon = read_netcdf_data(ECMWF_prefix+'ECMWF_qv.nc'   , 'q')
  uwind, time_pl , lev_pl  , lat, lon = read_netcdf_data(ECMWF_prefix+'ECMWF_uwind.nc', 'u')
  vwind, time_pl , lev_pl  , lat, lon = read_netcdf_data(ECMWF_prefix+'ECMWF_vwind.nc', 'v')
  
  # Dimension sizes
  nt_sfc = time_sfc.size  # number of time points for surface variables
  nt_pl  = time_pl.size  # number of time points for pressure level variables
  nl_sfc = 1  # number of levels, 1 for surface level
  nl_pl  = lev_pl.size  # number of levels
  nlat   = lat.size  # latitude points
  nlon   = lon.size  # longitude points
  
  # Convert integrated data to simultaneous data
  cnum      = 4
  interval  = 3.0*3600.0
  strd_real = integrated_to_simultaneous_sfc(strd, cnum, interval)
  
  #--------------------------------------#
  # Interpolate data
  #--------------------------------------#
  print('========== Interpolating data ... ==========')
  
  # Interpolate to specific location
  strd_interp_loc  = interpolate_loc_sfc(lat_hyy, lon_hyy, lat, lon, strd_real)
  geop_interp_loc  = interpolate_loc_pl(lat_hyy, lon_hyy, lat, lon, geop)
  temp_interp_loc  = interpolate_loc_pl(lat_hyy, lon_hyy, lat, lon, temp)
  qv_interp_loc    = interpolate_loc_pl(lat_hyy, lon_hyy, lat, lon, qv)
  uwind_interp_loc = interpolate_loc_pl(lat_hyy, lon_hyy, lat, lon, uwind)
  vwind_interp_loc = interpolate_loc_pl(lat_hyy, lon_hyy, lat, lon, vwind)
  
  #
  # Interpolate to local time
  # For surface values:  0, 3, 6, 9, 12, 15, 18, 21, 0, ..., 21, 0
  # For pressure levels:  0, 3, 6, 9, 12, 15, 18, 21, 0, ..., 21, 0
  #
  UTC = 2  # local time is UTC+02 for SMEAR II
  time_offset = datetime.timedelta(hours=UTC)
  
  time_interval = 3  # 3 hours between two time points
  strd_interp_time, time_out_sfc = interpolate_time_sfc(time_sfc + time_offset, strd_interp_loc, date, time_interval)
  
  time_interval = 3  # 3 hours between two time points
  geop_interp_time , time_out_pl = interpolate_time_pl(time_pl + time_offset, geop_interp_loc , date, time_interval)
  temp_interp_time , time_out_pl = interpolate_time_pl(time_pl + time_offset, temp_interp_loc , date, time_interval)
  qv_interp_time   , time_out_pl = interpolate_time_pl(time_pl + time_offset, qv_interp_loc   , date, time_interval)
  uwind_interp_time, time_out_pl = interpolate_time_pl(time_pl + time_offset, uwind_interp_loc, date, time_interval)
  vwind_interp_time, time_out_pl = interpolate_time_pl(time_pl + time_offset, vwind_interp_loc, date, time_interval)
  
  #
  # Convert qv (specific humidity) to rhov (absolute humidity)
  #
  pres = 100.0*np.array([ \
    1, 2, 3, 5, 7, \
    10, 20, 30, 50, 70, \
    100, 125, 150, 175, 200, \
    225, 250, 300, 350, 400, \
    450, 500, 550, 600, 650, \
    700, 750, 775, 800, 825, \
    850, 875, 900, 925, 950, \
    975, 1000 ])  # [Pa]
  Mair = 28.97e-3  # [kg mol-1], molar mass of air
  Rgas = 8.3144598  # [J mol-1 K-1], gas constant
  
  # Calculate rho air first 
  nt, nl = temp_interp_time.shape
  rho_interp_time = np.zeros((nt, nl))
  for i in range(nt):
    rho_interp_time[i, :] = pres*Mair/(Rgas*temp_interp_time[i, :])  # rho = PM/(RT)
  
  # Calculate rhov
  rhov_interp_time = qv_interp_time * rho_interp_time  # rhov = qv * rho


  #--------------------------------------#
  # Write data
  #--------------------------------------#
  print('========== Writing data to input files ... ==========')
  
  input_prefix = folder + '/ECMWF_'
  write_input_file_sfc(input_prefix + 'strd.txt' , strd_interp_time , time_out_sfc)
  write_input_file_pl (input_prefix + 'geop.txt' , geop_interp_time , time_out_pl )
  write_input_file_pl (input_prefix + 'temp.txt' , temp_interp_time , time_out_pl )
  write_input_file_pl (input_prefix + 'qv.txt'   , qv_interp_time   , time_out_pl )
  write_input_file_pl (input_prefix + 'rhov.txt' , rhov_interp_time , time_out_pl )
  write_input_file_pl (input_prefix + 'uwind.txt', uwind_interp_time, time_out_pl )
  write_input_file_pl (input_prefix + 'vwind.txt', vwind_interp_time, time_out_pl )
  
  #--------------------------------------#
  # Copy input files to input folder
  #--------------------------------------#
  print('========== Copying files to the input folder ... ==========')
  
  dir_src = folder
  dir_dest = './input/'+str(year)+'/M0'+str(month)
  mkdir_p(dir_dest) 
  fnames = [ \
    'ECMWF_geop.txt' , \
    'ECMWF_uwind.txt', 'ECMWF_vwind.txt', \
    'ECMWF_temp.txt' , 'ECMWF_rhov.txt', \
    'ECMWF_strd.txt' , \
    'SMEAR_uwind.txt', 'SMEAR_vwind.txt', \
    'SMEAR_temp.txt' , 'SMEAR_pres.txt', 'SMEAR_rhov.txt', \
    'SMEAR_soilmoist.txt', 'SMEAR_soiltemp.txt', 'SMEAR_gsoil.txt', \
    'SMEAR_glob.txt', 'SMEAR_PAR.txt', 'SMEAR_albedo.txt', \
    'SMEAR_O3avg.txt', 'SMEAR_SO2avg.txt', \
    'SMEAR_NOavg.txt','SMEAR_NO2avg.txt', 'SMEAR_COavg.txt']
  
  for f in fnames:
    full_src = dir_src + '/' + f
    full_dest = dir_dest
  
    print(full_src, ' --> ', full_dest)
  
    shutil.copy(full_src, full_dest)

  print('========== Copying files to the output folder ... ==========')
  
  dir_src = folder
  dir_dest = './output/'+str(year)+'/M0'+str(month)
  mkdir_p(dir_dest) 
  fnames = [ \
    'ECMWF_geop.txt' , \
    'ECMWF_uwind.txt', 'ECMWF_vwind.txt', \
    'ECMWF_temp.txt' , 'ECMWF_rhov.txt', \
    'ECMWF_strd.txt' , \
    'SMEAR_uwind.txt', 'SMEAR_vwind.txt', \
    'SMEAR_temp.txt' , 'SMEAR_pres.txt', 'SMEAR_rhov.txt', \
    'SMEAR_O3.txt' , 'SMEAR_SO2.txt', \
    'SMEAR_NO.txt','SMEAR_NO2.txt', \
    'SMEAR_CO.txt' ,\
    'SMEAR_mono.txt','SMEAR_C5H8_MBO.txt',\
    'SMEAR_CH3CHO.txt','SMEAR_C2H5OH_HCOOH.txt',\
    'SMEAR_acetone.txt','SMEAR_CH3COOH.txt',\
    'SMEAR_MEK.txt','SMEAR_MACR_MVK.txt']
  
  for f in fnames:
    full_src = dir_src + '/' + f
    full_dest = dir_dest
  
    print(full_src, ' --> ', full_dest)
  
    shutil.copy(full_src, full_dest)
  
  sys.exit()
  
  
  #--------------------------------------#
  # Check data
  #--------------------------------------#
  print('========== Checking data ... ==========')
  cnum = 4
  three_hour = 3.0*3600.0
  strd_out = integrated_to_simultaneous(strd[:,0,0], cnum, three_hour)
  
  fg, ax = plt.subplots(1, 1)
  # ax.plot(time_sfc+time_offset, strd_interp_loc, 'r')
  # ax.plot(time_out_sfc, strd_interp_time, 'bo')
  # ax.plot(time_pl+time_offset, uwind_interp_loc[:, 10], 'r')
  # ax.plot(time_out_pl, uwind_interp_time[:, 10], 'bo')
  # ax.plot(time, temp[:, -5, 1, 1], 'b-o')
  # ax.plot(time, temp_interp[:, -5], 'k-o')
  # ax.plot(SMEAR_albedo)
  
  # plt.show()
