1. Install python-openbabel.
$ sudo apt-get install python-openbabel

2. Install python package 'chemspipy'
$ pip install chemspipy
or with root privilege
$ sudo pip install chemspipy

3. Download EPISuite from:
https://www.epa.gov/tsca-screening-tools/download-epi-suitetm-estimation-program-interface-v411

Install EPISuite with wine if you are using linux system.

4. Prepare your own '*_Parameters.f90' file with KPP and copy it to this folder.
Run the python code to get an input file (e.g., BVOC-episuite.in when prefix is 'BVOC-') for EPISuite.
$ python get_H_f0.py BVOC-

5. Run HENRYNT.exe (in ~/.wine in my laptop) in EPISuite4.1 (with wine in linux system).

Select 'BatchInput' --> 'Batch File Input using SMILES Strings' --> 'StringFormat' --> select 'BVOC-episuite.in'.

Tick the checkboxes for "Group Estimated Henry" and "Experimental Henry".

You will get an output file BATCH01.OUT under EPISuite4.1 installation folder. Copy it to current folder which contains 'get_H_f0.py'.

Rename 'BATCH01.OUT' to 'BVOC-episuite.out'.

6. Run the code again to get the final output file 'BVOC-H_f0_mmass.inp' containing H and f0 values, as well as other information.
$ python get_H_f0.py BVOC-

7. Note: The code has been tested with python 2.7 under Ubuntu 16.04. Python 3.x should also work.

8. Contact putian.zhou@helsinki.fi if you have any problems.
