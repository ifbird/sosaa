import os
import sys
import time
import re
from shutil import copyfile

from urllib.request import urlopen
# import urllib2
import requests

import math
import numpy as np

import openbabel as ob
import pybel
from chemspipy import ChemSpider

from openpyxl import Workbook
from openpyxl import load_workbook

import smiles2group as s2g

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.animation as animation
import matplotlib.path as mpath
import matplotlib.lines as mlines
import matplotlib.patches as mpatches


#---------------------------------------------------------#
# Create dir if not exist
#---------------------------------------------------------#
def mkdir_p(path):
  try:
    os.makedirs(path)
  except OSError as exc:  # Python >2.5
    if exc.errno == errno.EEXIST and os.path.isdir(path):
      pass
    else:
      raise


def read_epi(file_name):
  """
  " Read episuite output
  " input: file name
  " output: Hcp values obtained via bond, group methods and experimental in the unit of [M atm-1]
  """

  Hcp_b = []
  Hcp_g = []
  Hcp_e = []

  with open(file_name, 'r') as f:  # [atm m3 mol-1]
    for line in f:
      if re.search(r'\*\*\*\*', line):
        Hcp_b.append(np.nan)
        Hcp_g.append(np.nan)
        Hcp_e.append(np.nan)
      else:
        groups = re.search(r'(.*)\(bond\)(.*)\(group\)(.*)\(exp\)', line)
        hb = groups.group(1)
        hg = groups.group(2)
        he = groups.group(3)

        if not ( re.search(r'Incomplete', hb) or re.search(r'----', hb) ):
          Hcp_b.append(float(hb))
        else:
          Hcp_b.append(np.nan)

        if not ( re.search(r'Incomplete', hg) or re.search(r'----', hg) ):
          Hcp_g.append(float(hg))
        else:
          Hcp_g.append(np.nan)

        if not ( re.search(r'Incomplete', he) or re.search(r'----', he) ):
          Hcp_e.append(float(he))
        else:
          Hcp_e.append(np.nan)

  h = np.ones((len(Hcp_b), 3))
  h[:,0] = np.array(Hcp_b)
  h[:,1] = np.array(Hcp_g)
  h[:,2] = np.array(Hcp_e)

  h = 1.0/(h*1.0e3)  # [atm m3 mol-1] --> [M atm-1]

  return h


def inchikey_to_Hcp_from_file(inchikey, file_name):
  """
  " Find the experiment data for those inchikey strings from the Hcp.f90
  """
  Hcpe = np.ones((len(inchikey), 2))*np.nan
  for i, ink in enumerate(inchikey):
    print(ink)
    if ink.strip() != '*':  # SMILES has been defined
      #===== Search Henry's law constants =====#
      find = False
      with open(file_name, 'r') as hf:
        while not find:
          line = hf.readline()
          if not line: break
          line = line.strip()
          temp_str = re.search('inchikey: *([A-Za-z-]+)', line)
          if temp_str:  # if 'inchikey' is found
            if temp_str.group(1) == ink.strip():  # we find the compounds in Hcp.f90
              find = True
              line = hf.readline()
              if not line.isspace():  # we can find compounds in Hcp.f90, but some Henry's constants are not defined
                henry_str = re.search(r'= *(.*)!', line).group(1).rstrip()

                ##### Find constant A
                henry_re_A = re.search(r'([0-9\.Ee+-]+)', henry_str)
                Hcpe[i,0] = float(henry_re_A.group(1))

                ##### Find constant B
                henry_re_B = re.search(r'EXP\( *([0-9\.]+).*\(', henry_str)
                if henry_re_B:
                  Hcpe[i,1] = float(henry_re_B.group(1))

  return Hcpe


def inchikey_to_Hcp_from_data(inchikey, Hcp):
  """
  " Search the matched inchikey in Hcp to get the H values
  """
  Hcpe = np.ones( (len(inchikey), 2) )*np.nan
  for i, ink in enumerate(inchikey):
    for j, s in enumerate(Hcp['inchikey']):
      if ink.strip() == s.strip():
        if Hcp['A'][j]:
          Hcpe[i,0] = Hcp['A'][j][0]
          Hcpe[i,1] = Hcp['B'][j][0]
        break
  return Hcpe


def read_Hcp_file(file_name, getSmiles=False):
  """
  " Read all the original data in Hcp.f90 to Hcp
  """
  
  Hcp = {}
  Hcp['species'] = []
  Hcp['formula'] = []
  Hcp['trivial'] = []
  Hcp['casrn'] = []
  Hcp['inchikey'] = []
  Hcp['A'] = []
  Hcp['B'] = []
  Hcp['type'] = []
  Hcp['ref'] = []

  with open(file_name, 'r') as hf:
    line = hf.readline()
    while line:
      temp_str = re.search(r'species: *(.*)', line)
      if temp_str:  # the keyword 'species' is found
        #===== Read species =====#
        Hcp['species'].append(temp_str.group(1))

        #===== Read formula =====#
        line = hf.readline()
        Hcp['formula'].append(re.search(r'formula: *(.*)', line).group(1))

        #===== Read trivial =====#
        line = hf.readline()
        Hcp['trivial'].append(re.search(r'trivial: *(.*)', line).group(1))

        #===== Read casrn =====#
        line = hf.readline()
        Hcp['casrn'].append(re.search(r'casrn: *(.*)', line).group(1))

        #===== Read inchikey =====#
        line = hf.readline()
        Hcp['inchikey'].append(re.search(r'inchikey: *(.*)', line).group(1))

        #===== Read Hcp values, including A, B, type and ref =====#
        # 'A': [[A from ref1], [A from ref2], ...], 'B': [ same with A], 'type': [ same with A ], 'ref': [ similar with A ]
        Hcp['A'].append([])
        Hcp['B'].append([])
        Hcp['type'].append([])
        Hcp['ref'].append([])

        line = hf.readline()
        while (not line.isspace()) and line:  # is not space line and not EOF, Hcp values
          temp_str = re.search(r'Hcp.*= *(.*)!.*type: *(.*), *ref: *(.*)', line)  # henry_str, type_str and ref_str

          ################ Get A and B
          henry_str = temp_str.group(1)

          henry_A = re.search(r'([0-9\.Ee+-]+)', henry_str)
          Hcp['A'][-1].append(float(henry_A.group(1)))

          henry_B = re.search(r'EXP\( *([0-9\.]+).*\(', henry_str)
          if henry_B:
            Hcp['B'][-1].append(float(henry_B.group(1)))
          else:
            Hcp['B'][-1].append(0.0)

          ################ Get data type
          type_str = temp_str.group(2).rstrip()
          Hcp['type'][-1].append(type_str)

          ################ Get data ref
          ref_str = temp_str.group(3).rstrip()
          Hcp['ref'][-1].append(ref_str)

          ################ Next line
          line = hf.readline()
      else:
        line = hf.readline()

  # Get smiles and smilesref with ChemSpider
  if getSmiles:
    Hcp['smiles'], Hcp['smilesref'] = inchikey_to_smiles(Hcp['inchikey'], Hcp['species'])
  else:
    Hcp['smiles'] = ['*']*len(Hcp['inchikey'])
    Hcp['smilesref'] = ['unknown']*len(Hcp['inchikey'])

  return Hcp


def inchikey_to_smiles(inchikey, species=[]):
  """
  " inchikey: a list of inchikey
  """

  cs = ChemSpider('60aeb52d-1ace-45f7-b841-80fac7e635c4')  # use the id in your profile

  smiles = ['*']*len(inchikey)
  smilesref = ['unknown']*len(inchikey)

  for i, ik in enumerate(inchikey):
    result = cs.search(ik)  # search inchikey
    if result.count > 0:
      smiles[i] = result[0].smiles
      smilesref[i] = 'ChemSpider'
    elif len(species) > 0:
      result1 = cs.search(species[i])  # search species names
      smiles[i] = result1[0].smiles  # use the first compound shown up
      smilesref[i] = 'ChemSpider'

  return smiles, smilesref


def save_Hcp_to_files(Hcp, fileBin, fileXlsx):
  # Save to binary file
  with open(fileBin, 'w') as f:
    np.save(f, Hcp)

  # Save to xlsx file
  nc = len(Hcp['species'])

  wb = Workbook()
  ws = wb.active

  # Add title for each column, A0, B0 represent the first values appearing in each compound which are used in future
  ws.append(['index', 'species', 'formula', 'trivial', 'casrn',
    'inchikey', 'smiles', 'smilesref', 'A0', 'B0',
    'type0', 'ref0', 'A', 'B', 'type',
    'ref'
    ])
  for i in range(nc):
    print('  ', i+1, Hcp['species'][i])
    # If A is empty, set A, B, type, ref as default
    if not Hcp['A'][i]:
      Hcp['A'][i] = [np.nan]
      Hcp['B'][i] = [np.nan]
      Hcp['type'][i] = '*'
      Hcp['ref'][i] = '*'
    ## Index starts from 1
    ws.append([i, Hcp['species'][i],
      Hcp['formula'][i],
      Hcp['trivial'][i],
      Hcp['casrn'][i],
      Hcp['inchikey'][i],
      Hcp['smiles'][i],
      Hcp['smilesref'][i],
      '{0:.2e}'.format(Hcp['A'][i][0]),
      '{0:.1f}'.format(Hcp['B'][i][0]),
      Hcp['type'][i][0],
      Hcp['ref'][i][0],
      ', '.join(['{0:.2e}'.format(s) for s in Hcp['A'][i]]),
      ', '.join(['{0:.1f}'.format(s) for s in Hcp['B'][i]]),
      ', '.join(['{0}'.format(s) for s in Hcp['type'][i]]),
      ', '.join(['{0}'.format(s) for s in Hcp['ref'][i]]),
      ])

  wb.save(fileXlsx)


def read_Hcp_from_xlsx(fileXlsx):
  wb = load_workbook(fileXlsx)
  ws = wb['Sheet']

  for i, row in enumerate(ws.rows[1:]):  # First row is the title texts
    # print str(row[12].value).split()
    Hcp['species'][i]   = row[1].value
    Hcp['formula'][i]   = row[2].value
    Hcp['trivial'][i]   = row[3].value
    Hcp['casrn'][i]     = row[4].value
    Hcp['inchikey'][i]  = row[5].value
    Hcp['smiles'][i]    = row[6].value
    Hcp['smilesref'][i] = row[7].value
    Hcp['A'][i] = [float(j.strip()) for j in row[12].value.split(',')]  # row[12] is the string of list of A, split it and then strip the spaces, finally convert it to float
    Hcp['B'][i] = [float(j.strip()) for j in row[13].value.split(',')]
    Hcp['type'][i] = [j.strip() for j in str(row[14].value).split(',')]
    Hcp['ref'][i] = [j.strip() for j in str(row[15].value).split(',')]

  return Hcp


def second_to_ind_mcm(second_file):
  """
  " Read chem index and MCM names from second_file
  " The second file is generated from KPP which includes the indices of all the compounds.
  " Usually the second file name is *_Parameters.f90.
  """
  chem_index = []
  mcm_names = []
  with open(second_file, 'r') as sf:
    for line in sf.readlines():
      temp_match = re.search(r':: *ind_([0-9A-Za-z_]+) *= *([0-9]+)', line)  # search line like 'INTEGER, PARAMETER :: ind_PINAL = 1824'
      if temp_match:
        chem_index.append(temp_match.group(2))
        mcm_names.append(temp_match.group(1))

  return chem_index, mcm_names


def mcm_to_names(mcm, version, output_file):
  """
  " Obtain SMILES strings from MCM website
  " mcm: MCM name
  " version: 'MCM3.2' or 'MCM3.3'
  """
  if version == 'MCM3.2':
    url0 = 'http://mcm.leeds.ac.uk/MCMv3.2/browse.htt?species='
  elif version == 'MCM3.3':
    url0 = 'http://mcm.leeds.ac.uk/MCM/browse.htt?species='
  else:
    print('Current version only supports MCM3.2 and MCM3.3.')
    sys.exit()

  #===== Read the file containing mcm names and smiles =====#
  # ms_dict = {}
  # with open(ms_file, 'r') as f:
  #   for line in f:
  #     if not line.isspace():  # if the line contains not only spaces
  #       line_strip = line.strip()
  #       ms_dict[line_strip.split()[0]] = line_strip.split()[1]

  #===== Search the smiles names from file first then MCM website =====#
  names = ['*']*len(mcm)
  for i, mn in enumerate(mcm):
    url = url0 + mn
    sock = urlopen(url)
    # sock = requests.get(url)
    find = False
    for line_b in sock:
    # for line_b in sock.iter_lines():
      # line = line_b.decode('utf-8')
      if re.search(r'Synonyms', line):  # <th align="left">Synonyms</th>
        find = True
        break

    if find:
      html_line = sock.readline().decode('utf-8')
      html_line = sock.readline().decode('utf-8')
      html_line = sock.readline().decode('utf-8')
      names[i] = html_line.strip()[0:-1]

    print(mn, names[i])

    # with open(output_file, 'w') as f:
    #   f.write('{0:14s}{1:50s}\n'.format(mn, names[i]))

  return names


def mcm_to_smiles(mcm, version, ms_file):
  """
  " Obtain SMILES strings in the order of
  " 1. ms_file
  " 2. MCM website
  "   version: 'MCM3.2' or 'MCM3.3'
  "   Append the new mcm smiles pair to ms_file
  "
  " mcm: list of MCM names
  " smiles: list of SMILES strings
  """
  if version == 'MCM3.2':
    url0 = 'http://mcm.leeds.ac.uk/MCMv3.2/browse.htt?species='
  elif version == 'MCM3.3':
    url0 = 'http://mcm.leeds.ac.uk/MCM/browse.htt?species='
  else:
    print('Current version only supports MCM3.2 and MCM3.3.')
    sys.exit()

  #===== Read the file containing mcm names and smiles =====#
  ms_dict = {}
  with open(ms_file, 'r') as f:
    for line in f:
      if not line.isspace():  # if the line contains not only spaces
        line_strip = line.strip()
        ms_dict[line_strip.split()[0]] = line_strip.split()[1]

  #===== Search the smiles names from file first then MCM website =====#
  smiles = ['*']*len(mcm)  # default SMILES string is '*'
  for i, mn in enumerate(mcm):
    if mn in ms_dict:
      smiles[i] = ms_dict[mn]
    else:
      url = url0 + mn
      sock = urlopen(url)
      html_line = sock.readline().decode('utf-8')  # decode the bytes to string with utf-8 encoding
      while not re.search(r'class="smiles"', html_line):  # <span class="smiles">CC(=C)C=O</span>
        html_line = sock.readline().decode('utf-8')  # decode the bytes to string with utf-8 encoding
      sock.close()

      smiles[i] = re.search(r'"smiles">(.*)</', html_line).group(1)
      if re.search(r'Exception', smiles[i]):  # if there is no related name in MCM website
        smiles[i] = '*'

      with open(ms_file, 'a') as f:
        f.write('{0:14s}{1:50s}\n'.format(mn, smiles[i]))

    print('{0:14s}{1:50s}'.format(mn, smiles[i]))

  return smiles


def smiles_to_inchikey_mmass(smiles, mcm):
  """
  " Use openbabel to convert SMILES to INCHIkey and get their molar mass.
  " Return INCHIkey string and molar mass [g mol-1].
  """

  inchikey = ['*']*len(smiles)
  mmass = np.zeros(len(smiles))
  ##### Convert SMILES to INCHIkey
  conv = ob.OBConversion()
  conv.SetInAndOutFormats('smi', 'inchi')
  conv.SetOptions('K', conv.OUTOPTIONS)
  mol = ob.OBMol()
  for i, sn in enumerate(smiles):
    if sn != '*':
      conv.ReadString(mol, sn)
      temp = conv.WriteString(mol).strip()
      if temp:
        inchikey[i] = temp

      ##### Use openbabel to get molar mass
      mmass[i] = mol.GetMolWt()
    elif mcm[i] == 'OMT':
      mmass[i] = 136.0
    elif mcm[i] == 'OSQ':
      mmass[i] = 204.0
    elif mcm[i] == 'O1D':
      mmass[i] = 16.0
    # elif mcm[i] == 'Farnesene':
    #   mmass[i] = 204.0
    else:
      pass


  return inchikey, mmass


def smiles_to_f0(smiles):
  f0 = np.zeros(len(smiles))  # dry reactivity factor
  f0_flag = np.array(['***']*len(smiles))
  for i, sn in enumerate(smiles):
    ##
    ## SO2, O3, NO2, NO, HNO3, H2O2, NH3, HONO (Wesely, 1989)
    ##
    if any(sn == temp for temp in ['[O-][O+]=O', 'OO',]):  # O3, H2O2
      f0[i] = 1.0
      f0_flag[i] = 'DIW'
    elif any(sn == temp for temp in ['[N](=O)=O', 'N(=O)O']):  # NO2, HONO
      f0[i] = 0.1
      f0_flag[i] = 'DIW'
    elif any(sn == temp for temp in ['O=S=O', '[N]=O', '[N+](=O)(O)[O-]', 'N']):  # SO2, NO, HNO3, NH3
      f0[i] = 0.0
      f0_flag[i] = 'DIW'
    ##
    ## OH, NO3, HO2, O, O1D (no smiles string, so not set here), extrapolated from Wesely (1989)
    ##
    elif any(sn == temp for temp in ['[OH]', '[N+](=O)([O-])[O]', '[O]']):  # OH, NO3, O
      f0[i] = 1.0
      f0_flag[i] = 'EFW'
    elif any(sn == temp for temp in ['O[O-]']):  # HO2
      f0[i] = 0.1
      f0_flag[i] = 'EFW'
    ##
    ## HCHO, CH3CO3H, CH3CHO, GLYOX, MGLYOX,
    ## HOCH2CHO, HC4CHO, CH3OH, C2H5OH, CH3COCH3,
    ## ACETOL, MVK, MACR (Karl et al., 2010, S)
    ##
    elif any(sn == temp for temp in [  \
      'C=O', 'CC(=O)OO', 'CC=O', 'O=CC=O', 'O=CC(=O)C',  \
      'OCC=O', 'CC(=C)C(C=O)O', 'CO', 'CCO', 'CC(=O)C',  \
      'CC(=O)CO', 'CC(=O)C=C', 'CC(=C)C=O',  \
      ]):
      f0[i] = 1.0
      f0_flag[i] = 'DIK'
    ##
    ## PAN = 1 (GEOS-CHEM, http://wiki.seas.harvard.edu/geos-chem/index.php/Caltech_isoprene_scheme#Henry.27s_law_constant)
    ## PAN = 0.1 (GEOS-CHEM, http://wiki.seas.harvard.edu/geos-chem/index.php/Dry_deposition)
    ## PAN = 0.1 (Wesely, 1989)
    ##
    elif any(sn == temp for temp in ['CC(=O)OON(=O)=O']):
      f0[i] = 0.1
      f0_flag[i] = 'DIW'
    ##
    ## organic peroxide radicals, set to 0 since no reference mentioned their f0 values
    ##
    # elif any(re.search(temp, sn) for temp in [r'^\[O\]OC', r'CO\[O\]$', r'\(CO\[O\]\)', r'\(\[O\]OC\)']):
    elif any(re.search(temp, sn) for temp in [r'\[O\]O', r'O\[O\]']):
      f0[i] = 0.0
      f0_flag[i] = 'OPR'
    ##
    ## organic peroxides, f0(-OOH) = 1 (Karl et al., 2010, S)
    ## When OOH is in the beginning, it should be directly connected to C:
    ##   1. ^OOC
    ## When OOH is in the end:
    ##   1. Direct connection: C[1-9]*OO$
    ##   2. With one branch: C[1-9]*([^()]*)OO$
    ##   3. With two branches: C[1-9]*([^()]*)([^()]*)OO$
    ## When OOH is in the middle:
    ##   1. As a branch: C[1-9]*(OO)
    ##   2. As one of two branches: C[1-9]*([^()]*)(OO)
    ##   3. Connected in a branch: C[1-9]*OO), C[1-9]*([^()]*)OO), C[1-9]*([^()]*)([^()]*)OO)
    ##
    # old patterns
    # elif any(re.search(temp, sn) for temp in [r'^OOC', r'COO$', r'C2OOC', r'\(COO\)', r'COOC', r'\(C\)OOC', r'COO\(C\)',
    #                                           r'\(C\)OO1', r'OON\(=O\)=O', r'O=N\(=O\)OO', r'OOC\(=O\)', r'\(=O\)COO']):
    elif any(re.search(temp, sn) for temp in [  \
      r'^OOC',  \
      r'C[1-9]*OO$', r'C[1-9]*\([^\(\)]*\)OO$', r'C[1-9]*\([^\(\)]*\)\([^\(\)]*\)OO$',  \
      r'C[1-9]*\(OO\)', r'C[1-9]*\([^\(\)]*\)\(OO\)',  \
      r'C[1-9]*OO\)', r'C[1-9]*\([^\(\)]*\)OO\)', r'C[1-9]*\([^\(\)]*\)\([^\(\)]*\)OO\)',  \
      ]):
      f0[i] = 1.0
      f0_flag[i] = 'OPE'
    ##
    ## Other compounds
    ##
    else:
      f0_flag[i] = 'OTH'

  return f0, f0_flag


def find_H_final(Hcp_se, Hcp_epi, mcm):
  """
  " Find HA and HB.
  """

  # Number of species
  ns = Hcp_se.shape[0]

  # Initiate values
  H = np.zeros((ns, 2))  # number of species * 2 (HA and HB)
  H[:,1] = 6013.95  # for default, use 50000/8.314=6013.95 K as the parameter for temperature dependence
  H_flag = np.array(['**']*ns)  # np.array will use the longest string element as the data type for the whole array

  # Set H values in the order of SE, EE, EG, EB, MH, NA
  for i in range(ns):
    if not np.isnan(Hcp_se[i,0]):  # Sander's experiment value
      H[i,0] = Hcp_se[i,0]
      H[i,1] = Hcp_se[i,1]
      H_flag[i] = 'SE'
    elif not np.isnan(Hcp_epi[i,2]):  # experiment value from EPI
      H[i,0] = Hcp_epi[i,2]
      H_flag[i] = 'EE'
    elif not np.isnan(Hcp_epi[i,1]):  # calculated value from EPI group method
      H[i,0] = Hcp_epi[i,1]
      H_flag[i] = 'EG'
    elif not np.isnan(Hcp_epi[i,0]):  # calculated value from EPI bond method
      H[i,0] = Hcp_epi[i,0]
      H_flag[i] = 'EB'
    elif mcm[i] == 'OMT':  # manually set
      H[i,0] = 0.023  # average of APINENE (0.03, 1800, SE) and BPINENE (0.016, 0, SE)
      H_flag[i] = 'MH'
    elif mcm[i] == 'OSQ':  # manually set
      H[i,0] = 1.45e-3  # equals to BCARY (1.45e-3, EB)
      H_flag[i] = 'MH'
    else:  # H not available
      H[i,0] = 0.0
      H_flag[i] = 'NA'

  return H, H_flag

def smiles_to_EPISuite_input(smiles, fileEpi):
  with open(fileEpi, 'w') as f:
    for i, ss in enumerate(smiles):
      f.write(ss+'\n')
    # f.write('\n')


def smiles_to_formula(smiles, mcm):
  forms = ['*']*len(smiles)
  ##### Convert SMILES to INCHIkey
  conv = ob.OBConversion()
  conv.SetInAndOutFormats('smi', 'inchi')
  mol = ob.OBMol()
  for i, sn in enumerate(smiles):
    if sn != '*':
      conv.ReadString(mol, sn)
      forms[i] = mol.GetFormula()

  return forms


def smiles_to_name_formula(smiles, mcm):
  output_file = 'mcm_smiles_name_formula.txt'
  cs = ChemSpider('60aeb52d-1ace-45f7-b841-80fac7e635c4')
  names = ['*']*len(smiles)
  forms = ['*']*len(smiles)

  with open(output_file, 'w') as f:
    for i, sn in enumerate(smiles):
      print('=========================== MCM, SMILES: ', mcm[i], sn, '===============================')
      nc = 0
      if sn != '*':
        # c = cs.get_compound(sn)
        c = cs.search(sn)
        if c:
          names[i] = c[0].common_name
          forms[i] = c[0].molecular_formula
          nc = len(c)
          print(nc, names[i], forms[i])
        # for cc in c:
        #   print cc.common_name
    # forms = ['*']*len(smiles)
    # ##### Convert SMILES to INCHIkey
    # conv = ob.OBConversion()
    # conv.SetInAndOutFormats('smi', 'inchi')
    # mol = ob.OBMol()
    # for i, sn in enumerate(smiles):
    #   if sn != '*':
    #     conv.ReadString(mol, sn)
    #     forms[i] = mol.GetFormula()
        f.write('{0:14s}{1:50s}{2:5d}{3:50s}{4:30s}\n'.format(mcm[i], sn, nc, names[i], forms[i]))

  return


def write_SOSAA_input(index, mcm, smiles, Hcp_se, Hcp_epi, Hcp_flag, Hcp_final, f0_flag, f0, forms, mmass, fileOutput):
  # Obtain the length of longest smiles, mcm and formula strings (developed in future)
  pass

  with open(fileOutput, 'w') as f:
    f.write('{index:>5s}  {mcm:<15s}{smiles:<70s}{H_sea:>10s}{H_ee:>10s}{H_eg:>10s}{H_eb:>10s}{H_flag:>4s}{H_A:>10s}{H_B:>10s}{f0_flag:>5s}{f0:>5s}  {forms:<20s}{mmass:>11s}\n'.format(
      index='ID', mcm='MCM', smiles='SMILES',
      H_sea='H_SE_A',
      H_ee='H_EE', H_eg='H_EG', H_eb='H_EB',
      H_flag='H_F', H_A='H_A', H_B='H_B',
      f0_flag='f0_F', f0='f0',
      forms='formula', mmass='mmass'))

    for i in range(Hcp_final.shape[0]):
      f.write('{index:5d}  {mcm:<15s}{smiles:<70s}{H_sea:10.2e}{H_ee:10.2e}{H_eg:10.2e}{H_eb:10.2e}{H_flag:>4s}{H_A:10.2e}{H_B:10.2e}{f0_flag:>5s}{f0:5.1f}  {forms:<20s}{mmass:11.5f}\n'.format(
        index=i+1, mcm=mcm[i], smiles=smiles[i],
        H_sea=Hcp_se[i,0], H_seb=Hcp_se[i,1],
        H_ee=Hcp_epi[i,2], H_eg=Hcp_epi[i,1], H_eb=Hcp_epi[i,0],
        H_flag=Hcp_flag[i], H_A=Hcp_final[i,0], H_B=Hcp_final[i,1],
        f0_flag=f0_flag[i], f0=f0[i],
        forms=forms[i], mmass=mmass[i]))

##### Set parameters
if len(sys.argv) == 1:
  prefix = ''
elif len(sys.argv) == 2:
  prefix = sys.argv[1].strip()
else:
  print('[Warning] Too many arguments, only the first one is used.')

dir_this            = os.path.dirname(os.path.abspath(__file__))
file_mcm_smiles_lib = dir_this + '/mcm_smiles.txt'
file_Sander_Hcp     = dir_this + '/Hcp.f90'

file_parameter = prefix + 'second_Parameters.f90'
file_epi_in    = prefix + 'episuite.in'
file_epi_out   = prefix + 'episuite.out'
file_final     = prefix + 'H_f0_mmass.inp'


##### Get basic information: index, mcm, smiles, inchikey, mmass, formula

# Get indices and MCM names from second_Parameters.f90
print('Getting indices and MCM names from second_Parameters.f90 file ...')
index, mcm = second_to_ind_mcm(file_parameter)

# Get SMILES strings from MCM names with the help of the dictionary file mcm_smiles.txt
print('Getting SMILES strings from MCM names ...')
smiles = mcm_to_smiles(mcm, 'MCM3.3', file_mcm_smiles_lib)

# Get InChIKey and molar mass from SMILES and MCM names
print('Getting InChIKey and molar mass from SMILES and MCM names ...')
inchikey, mmass = smiles_to_inchikey_mmass(smiles, mcm)

# Get formulas from SMILES and MCM names
print('Getting formulas from SMILES and MCM names ...')
formula = smiles_to_formula(smiles, mcm)

if not os.path.isfile(file_epi_out):
  print('Generating input file for EPISuite ...')
  # Generate the input file for EPISuite, then use EPI to calculate H values, generating an output file
  smiles_to_EPISuite_input(smiles, file_epi_in)

else:
  print('Generating final H and f0 files ...')
  # Read H from EPISuite: bond, group, exp at 298.15 K
  Hcp_epi = read_epi(file_epi_out)

  # Save all the information into Hcp from Sander's data (Hcp.f90)
  Hcp = read_Hcp_file(file_Sander_Hcp)

  # Read Sander's H values
  Hcp_se = inchikey_to_Hcp_from_data(inchikey, Hcp)

  # Get final H values and flags showing the source
  Hcp_final, Hcp_flag = find_H_final(Hcp_se, Hcp_epi, mcm)

  # Get f0 and f0_flag
  f0, f0_flag = smiles_to_f0(smiles)

  # Output H and f0 values in one file
  write_SOSAA_input(index, mcm, smiles, Hcp_se, Hcp_epi, Hcp_flag, Hcp_final, f0_flag, f0, formula, mmass, file_final)

  # Write some statistics on the screen 
  for i in ['SE', 'EE', 'EG', 'EB', 'MH', 'NA']:
    print(i, np.count_nonzero(Hcp_flag == i))
  
  for i in ['DIW', 'EFW', 'OPR', 'OPE']:
    print(i, np.count_nonzero(f0_flag == i))
