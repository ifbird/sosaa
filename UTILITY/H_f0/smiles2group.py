import re

def smiles2group(sn):
  """
  " Convert SMILES to different chemical groups
  """

  ##### Ditte's rules.
  ## organic peroxide radicals, based on CH3CO3 (peroxyacetyl radical)
  if   any(re.search(temp, sn) for temp in [r'^\[O\]OC', r'CO\[O\]$', r'\(CO\[O\]\)', r'\(\[O\]OC\)']):
    return 'organic peroxide radicals'
  ## organic peroxides, based on methyl hydroperoxide (CH3OOH) in Sander 2015
  elif any(re.search(temp, sn) for temp in [r'^OOC', r'COO$', r'C2OOC', r'\(COO\)', r'COOC', r'\(C\)OOC', r'COO\(C\)',
                                            r'\(C\)OO1', r'OON\(=O\)=O', r'O=N\(=O\)OO', r'OOC\(=O\)', r'\(=O\)COO']):
    return 'organic peroxides'
  ## organic acids, based on formic acid in Sander 2015
  elif any(re.search(temp, sn) for temp in [r'^O\(O=\)C', r'C\(=O\)O', r'OC\(=O\)']):
    return 'organic acids'
  ## aldehydes, based on acetaldehyde in Sander 2015
  elif any(re.search(temp, sn) for temp in [r'C\(=O\)', r'\(O=\)C']) \
    and (not any(re.search(temp, sn) for temp in [r'O\(O=\)C', r'C\(=O\)O'])):
    return 'aldehydes'
  ## alcohol, based on methanol in Sander 2015
  elif any(re.search(temp, sn) for temp in [r'^OC', r'^\(O\)C', r'CO$', r'C\(O\)$', r'C\(O\)']):
    return 'alcohols'
  ## alkenes, based on alpha-pinene in Sander 2015
  elif any(re.search(temp, sn) for temp in [r'C1=C', r'C=C1', r'C\(=C\)', r'C=C']):
    return 'alkenes'
  ## other
  else:
    return 'other'


def smiles2groups(sn):
  """
  " Convert SMILES to different chemical groups
  """

  groups = []

  ##### Ditte's rules.
  ## organic peroxide radicals, based on CH3CO3 (peroxyacetyl radical)
  if   any(re.search(temp, sn) for temp in [r'^\[O\]OC', r'CO\[O\]$', r'\(CO\[O\]\)', r'\(\[O\]OC\)']):
    groups.append('OrgPerRad')

  ## organic peroxides, based on methyl hydroperoxide (CH3OOH) in Sander 2015
  if any(re.search(temp, sn) for temp in [r'^OOC', r'COO$', r'C2OOC', r'\(COO\)', r'COOC', r'\(C\)OOC', r'COO\(C\)',
                                          r'\(C\)OO1', r'OON\(=O\)=O', r'O=N\(=O\)OO', r'OOC\(=O\)', r'\(=O\)COO']):
    groups.append('OrgPer')

  ## organic acids, based on formic acid in Sander 2015
  if any(re.search(temp, sn) for temp in [r'^O\(O=\)C', r'C\(=O\)O', r'OC\(=O\)']):
    groups.append('OrgAcd')

  ## aldehydes, based on acetaldehyde in Sander 2015
  if any(re.search(temp, sn) for temp in [r'C\(=O\)', r'\(O=\)C']) \
    and (not any(re.search(temp, sn) for temp in [r'O\(O=\)C', r'C\(=O\)O'])):
    groups.append('Ald')

  ## alcohol, based on methanol in Sander 2015
  if any(re.search(temp, sn) for temp in [r'^OC', r'^\(O\)C', r'CO$', r'C\(O\)$', r'C\(O\)']):
    groups.append('Alc')

  ## alkenes, based on alpha-pinene in Sander 2015
  if any(re.search(temp, sn) for temp in [r'C1=C', r'C=C1', r'C\(=C\)', r'C=C']):
    groups.append('Alk')

  ## alkenes, based on alpha-pinene in Sander 2015
  if any(re.search(temp, sn) for temp in [r'[N+](=O)[O-]']):
    groups.append('OrgNtr')

  return groups
