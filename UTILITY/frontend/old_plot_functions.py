import os
import sys

import datetime

import math
import pandas as pd
import numpy as np
import scipy.stats as stats

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.patches as mpatches
# from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange
from matplotlib.ticker import MultipleLocator, FormatStrFormatter, ScalarFormatter
import matplotlib.gridspec as gridspec

# import seaborn as sns
# import seaborn.apionly as sns

import ptpack.parameter as ppp
import ptpack.datapack as dp
import ptpack.functions as ppf

# import datapack as dp
# import functions as ppf

# from header import *



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#
# Help functions
#
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

def choose_color_series(cmap, N):
  xx = np.linspace(0, 1, N)
  cs = [cmap(x) for x in xx]
  return cs


def calculate_relative_contribution(data_list):
  """
  " The elements in data_list are numpy arrays or scalars,
  " they need to be the same size.
  """
  d0 = data_list[0]

  ##### Initiate pos and neg according to scalar or not
  if np.isscalar(d0):
    pos = 0.0
    neg = 0.0
  else:
    pos = np.zeros(d0.shape)
    neg = np.zeros(d0.shape)

  ##### Get sum of positive and negative values
  for d in data_list:
    pos += d.clip(min=0.0)
    neg += d.clip(max=0.0)

  ##### Get maximum absolute value for each data point
  max_val = np.maximum(pos, -neg)

  ##### Get contribution list
  contr_list = []
  for d in data_list:
    contr_list.append(d / max_val)

  return contr_list

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#
# Default values
#
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
# sns.reset_orig()
# plt.style.use('seaborn-talk')
# plt.style.use('seaborn-poster')
# plt.style.use('seaborn-paper')
mpl.rcParams['legend.numpoints'] = 1
mpl.rcParams['legend.scatterpoints'] = 1

# Check current color palette
# cp = sns.color_palette('hls', 6)  # blue, gree, red, purple, yellow, cyan
# cp = sns.color_palette('muted')  # blue, gree, red, purple, yellow, cyan
# print(cp)
# sns.palplot(cp)
# plt.show()

PROP_DEFAULT = {
  'save_dir': '.',
  'dpi': 150,
  'figsize': (12,8),
  'fileName': '',
  }

cmap_seq_warm = plt.get_cmap('YlOrRd')
cmap_seq_cool = plt.get_cmap('YlGnBu')
cmap_div_RdBu = plt.get_cmap('seismic')
cmap_div_BrBG = plt.get_cmap('BrBG')
cmap_qua = plt.get_cmap('Paired')

cmap_qua_colors = choose_color_series(cmap_qua, 5)
# print(cmap_qua_colors)
# sys.exit()

font = {
  'size': 16
  }
mpl.rc('font', **font)


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#
# Plot functions
#
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

#----------------------------------------------------------#
# Basic axes functions
#----------------------------------------------------------#
def axes_2_xy(x1, y1, prop1, x2, y2, prop2):
  """
  " 1: model data
  " 2: observation data
  """
  fg, ax = plt.subplots(1, 1)

  ax.plot(x1, y1, **prop1)
  ax.plot(x2, y2, **prop2)

  return ax


def axes_2_xy_std(ax, x1, y1, std1, prop1, x2, y2, std2, prop2):
  """
  " 1: model data
  " 2: observation data
  " prop1: prop for plot
  " prop2: prop for errorbar, usually contais 'fmt' as 'o'
  """
  if ax is None:
    ax = gca()
  l1, = ax.plot(x1, y1, **prop1)
  y1_up = y1 + std1
  y1_down = y1 - std1
  ax.fill_between(x1, y1_up, y1_down, where=y1_up>=y1_down, color=plt.getp(l1, 'color'), alpha=0.4)

  l2 = ax.errorbar(x2, y2, yerr=std2, **prop2)

  return l1, l2


#----------------------------------------------------------#
# Basic plot function
#----------------------------------------------------------#
def axes_bar_stack(ax, x, y_list, barwidth, color_list, direct=1, alpha=0.25):
  ##### Initiate accumulated positive and negative values
  y0 = y_list[0]
  if np.isscalar(y0):
    y_posacc = 0.0
    y_negacc = 0.0
  else:
    y_posacc = np.zeros(y0.shape)
    y_negacc = np.zeros(y0.shape)

  ##### Get positive ang negative list and their accumulated values list
  pos_list = []
  posacc_list = [y_posacc.copy()]
  neg_list = []
  negacc_list = [y_negacc.copy()]
  for i, y in enumerate(y_list):
    y_pos = y.clip(min=0.0)
    y_posacc += y_pos
    pos_list.append(y_pos.copy())
    posacc_list.append(y_posacc.copy())

    y_neg = y.clip(max=0.0)
    y_negacc += y_neg
    neg_list.append(y_neg.copy())
    negacc_list.append(y_negacc.copy())

  ##### Stack values
  if direct==1:
    for i, (pos, posacc, neg, negacc, c) in enumerate(zip(pos_list, posacc_list[0:-1], neg_list, negacc_list[0:-1], color_list)):
      ax.bar(x, pos, width=barwidth, color=c, edgecolor='k', bottom=posacc, align='center', alpha=alpha)
      ax.bar(x, neg, width=barwidth, color=c, edgecolor='k', bottom=negacc, align='center', alpha=alpha)
  elif direct==2:
    for i, (pos, posacc, neg, negacc, c) in enumerate(zip(pos_list, posacc_list[0:-1], neg_list, negacc_list[0:-1], color_list)):
      ax.barh(x, pos, height=barwidth, color=c, edgecolor='k', left=posacc, align='center', alpha=alpha)
      ax.barh(x, neg, height=barwidth, color=c, edgecolor='k', left=negacc, align='center', alpha=alpha)
    

#----------------------------------------------------------#
# Plot x vs y
#----------------------------------------------------------#
def plot_x_vs_y(x, y, is_diag, is_reg, prop):
  # font = {
  #   'size': 18
  #   }
  # mpl.rc('font', **font)

  ##### Calculate linear regression and correlation coefficient
  ind_nonnan = ~ (np.isnan(x) | np.isnan(y))

  slope, intercept, r_value, p_value, std_err = stats.linregress(x[ind_nonnan], y[ind_nonnan])
  y_fit = x*slope + intercept
  fit_text = r'y={0:.2f}x+{1:.2f}'.format(slope, intercept)

  R = np.corrcoef(x[ind_nonnan], y[ind_nonnan])[0,1]
  R_text = r'R={0:.2f} R$^2$={1:.2f}'.format(R, R*R)

  ##### Plot the figure
  fg, ax = plt.subplots(1, 1, figsize=prop['figsize'], dpi=prop['dpi'])

  ax.plot(x, y, 'o', color=cmap_qua_colors[0])
  ## if plot regression line
  # print(np.count_nonzero(~np.isnan(x)))
  # print(np.count_nonzero(~np.isnan(y_fit)))
  if is_reg:
    ax.plot(x[ind_nonnan], y_fit[ind_nonnan], '--', color=cmap_qua_colors[1])
    ax.text(0.2, 0.8, fit_text, transform=ax.transAxes)
    ax.text(0.2, 0.7, R_text, transform=ax.transAxes)

  ## if the plot is square, then set it to square look and plot the diagonal line from bottom left to upper top
  if is_diag:
    ax.plot([0, 1], [0, 1], transform=ax.transAxes, color='k', linestyle='--')
    ax.set_aspect('equal')

    ## set the same value for xlim and ylim
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    lim = [min(xlim[0], ylim[0]), max(xlim[1], ylim[1])]
    ax.set_xlim(lim)
    ax.set_ylim(lim)

  fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['file_name']), dpi=prop['dpi'])

  return fg, ax


#----------------------------------------------------------#
# Plot 2 time series in one figure
#----------------------------------------------------------#
def plot_2_time_series(time1, data1, prop1, time2, data2, prop2):
  fg, ax = plt.subplots(1, 1, figsize=prop1['figsize'], dpi=prop1['dpi'])

  ax.plot(time1, data1, '-', color=cmap_qua_colors[0])
  ax.plot(time2, data2, 'o', color=cmap_qua_colors[1])

  ax.grid(True)

  fg.savefig('{0}/{1}.png'.format(prop1['save_dir'], prop1['file_name']), dpi=prop1['dpi'])

  plt.clf()

  return fg, ax


#----------------------------------------------------------#
# Plot modelled and measured diurnal cycle of conc and flux
# for every possible VOCs.
#----------------------------------------------------------#
def plot_comp_time_series_obs_mod(time_obs, data_obs, std_obs, time_mod, data_mod, std_mod):
  fg, ax = plt.subplots(1, 1)  # , figsize=prop['figsize'], dpi=prop['dpi'])
  # ax.plot(vd, 'o')
  # ax.plot(conc_2_dc, 'ro')
  # ax.plot(conc_3_dc, 'bo')
  # l1, = ax.plot(time_obs, data_obs, color=cmap_qua_colors[0], ls='--', marker='o')
  # y1 = data_obs - std_obs
  # y2 = data_obs + std_obs
  # ax.fill_between(time_obs, y1, y2, where=y2>=y1, color=plt.getp(l1, 'color'), alpha=0.4)
  ax.errorbar(time_obs, data_obs, yerr=std_obs, fmt='o', color=cmap_qua_colors[0])

  l2, = ax.plot(time_mod, data_mod, color=cmap_qua_colors[1], ls='-')
  y1 = data_mod - std_mod
  y2 = data_mod + std_mod
  ax.fill_between(time_mod, y1, y2, where=y2>=y1, color=plt.getp(l2, 'color'), alpha=0.4)

  return fg, ax


#----------------------------------------------------------#
# Plot time series of a variable at different layers
#----------------------------------------------------------#
def check_v_t_z(var, td, zz):
  # Get dimensions of time and levels
  vshape = var.shape
  vsize = var.size

  ntime = vshape[0]
  nlev = vsize/vshape[0]  # how many levels contained in the variable

  # Check the dimension size of var
  stat = 0
  if nlev != zz.size:
    print('The 1st dimensional size of variable is not equal to time dimension.')
    stat = -1
  elif ntime != td.size:
    print('The 2nd dimensional size of variable is not equal to height dimension.')
    stat = -2

  return (ntime, nlev, stat)


#----------------------------------------------------------#
# Plot time series of a variable at different layers
#----------------------------------------------------------#
def plot_time_series_at_levels(var, td, zz, prop=PROP_DEFAULT):
  """
  " var can be 1d or 2d with time as the first dimension
  """
  # Check dimensions
  ntime, nlev, stat = check_v_t_z(var, td, zz)
  if stat < 0:
    print('Wrong dimensions of var, td and zz.')
    return

  # One plot for each layer
  fg, ax = plt.subplots(nlev, 1, figsize=prop['figsize'], dpi=prop['dpi'])

  # If there is only one level, then we need to add the 2nd dimension to variables
  if nlev == 1:
    zz_temp = (zz, )
    ax_temp = [ax, ]
    var_temp = var[:,np.newaxis]
  else:
    zz_temp = zz
    ax_temp = ax
    var_temp = var

  for iz, z in enumerate(zz_temp):
    # izr = nlev - 1 - iz  # lower level drawn at lower plots
    ax_temp[iz].plot(td, var_temp[:,iz], label='{0:6.1f} m'.format(z))
    ax_temp[iz].set_title(ax_temp[iz].get_label())
    ax_temp[iz].grid(True)

    locator = mdates.AutoDateLocator()
    formatter = mdates.AutoDateFormatter(locator)
    ax_temp[iz].xaxis.set_major_locator(locator)
    ax_temp[iz].xaxis.set_major_formatter(formatter)
    # ax_temp[iz].xaxis.set_major_locator(mdates.DayLocator(range(1,32,2)))
    # ax_temp[iz].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(ax_temp[iz].xaxis.get_majorticklabels(), rotation=30, ha='right', fontsize=10)

    # Only plot the tick labels for the bottom figure
    if iz<nlev-1:
      ax_temp[iz].set_xticklabels([])

  # Save the figure
  fg.savefig('{0}/ts_al.png'.format(prop['save_dir']), dpi=prop['dpi'])


#----------------------------------------------------------#
# Plot time series at different levels for model and
# observation data to compare.
#----------------------------------------------------------#
def plot_time_series_at_levels_2(var1, td1, zz1, prop1, var2, td2, zz2, prop2):
  # Get paramters from prop
  save_dir = prop1['save_dir']
  if not os.path.exists(save_dir):
    os.makedirs(save_dir)
  figsize = prop1['figsize']
  dpi = prop1['dpi']
  fileName = prop1['fileName']
  if not fileName:  # if fileName is not set (empty)
    fileName = 'ts_levs_2'

  # Check dimensions
  ntime1, nlev1, stat1 = check_v_t_z(var1, td1, zz1)
  ntime2, nlev2, stat2 = check_v_t_z(var2, td2, zz2)
  if (stat1 < 0) or (stat2 < 0):
    print('Wrong dimensions of var, td and zz.')
    return
  elif nlev1 != nlev2:
    print('nlev1 is not equal to nlev2.')
    return

  # One plot for each layer
  fg, ax = plt.subplots(nlev1, 1, figsize=figsize, dpi=dpi)

  # If there is only one level, then we need to add the 2nd dimension to variables
  if nlev1 == 1:
    ax_temp = [ax, ]
    zz1_temp = (zz1, )
    zz2_temp = (zz2, )
    var1_temp = var1[:,np.newaxis]
    var2_temp = var2[:,np.newaxis]
  else:
    ax_temp = ax
    zz1_temp = zz1
    zz2_temp = zz2
    var1_temp = var1
    var2_temp = var2

  for iz, z1 in enumerate(zz1_temp):
    z2 = zz2_temp[iz]

    ax_temp[iz].plot(td1, var1_temp[:,iz], label='{0:7.2f} m'.format(z1))
    ax_temp[iz].plot(td2, var2_temp[:,iz], label='{0:7.2f} m'.format(z2))

    ax_temp[iz].grid(True)
    
    # Set date locator and formatter
    locator = mdates.AutoDateLocator()
    formatter = mdates.AutoDateFormatter(locator)
    ax_temp[iz].xaxis.set_major_locator(locator)
    ax_temp[iz].xaxis.set_major_formatter(formatter)
    # ax_temp[iz].xaxis.set_major_locator(mdates.DayLocator(range(1,32,2)))
    # ax_temp[iz].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.setp(ax_temp[iz].xaxis.get_majorticklabels(), rotation=30, ha='right', fontsize=10)

    # Only show the tick labels of the lowest plot
    if iz<nlev1-1:
      ax_temp[iz].set_xticklabels([])

    # Show the legends
    handles, labels = ax_temp[iz].get_legend_handles_labels()
    ax_temp[iz].legend(handles, labels, scatterpoints=1, bbox_to_anchor=(1, 1), loc='upper left', ncol=1)

  fg.savefig('{0}/{1}.png'.format(save_dir, fileName), dpi=dpi, bbox_inches='tight')


#----------------------------------------------------------#
# Plot contour along time series and height levels.
#----------------------------------------------------------#
def plot_contour_time_height(var, td, zz, prop=PROP_DEFAULT):
  # Get paramters from prop
  save_dir = prop['save_dir']
  figsize = prop['figsize']
  dpi = prop['dpi']
  fileName = prop['fileName']
  if not fileName:  # if fileName is not set (empty)
    fileName = 'cont_time_height'

  # Check dimensions
  ntime, nlev, stat = check_v_t_z(var, td, zz)
  if (stat < 0):
    print('Wrong dimensions of var, td and zz.')
    return

  # One plot for the whole time series and height levels
  fg, ax = plt.subplots(1, 1, figsize=figsize, dpi=dpi)

  print('plot')
  CS = ax.contourf(td, zz, var.T, 10, origin='lower')
    
  # Set date locator and formatter
  locator = mdates.AutoDateLocator()
  formatter = mdates.AutoDateFormatter(locator)
  ax.xaxis.set_major_locator(locator)
  ax.xaxis.set_major_formatter(formatter)
  # ax_temp[iz].xaxis.set_major_locator(mdates.DayLocator(range(1,32,2)))
  # ax_temp[iz].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
  plt.setp(ax.xaxis.get_majorticklabels(), rotation=30, ha='right', fontsize=10)

  # Set colorbar
  cbar = plt.colorbar(CS)

  print(save_dir, fileName)
  fg.savefig('{0}/{1}.png'.format(save_dir, fileName), dpi=dpi)


#----------------------------------------------------------#
# Plot modelled and measured VOC fluxes and concentrations
#----------------------------------------------------------#
def plot_comp_conc(sosa, voc, vocName, prop):
  save_dir = prop['save_dir']
  figsize = prop['figsize']
  dpi = prop['dpi']
  fileName = prop['fileName']

  ncz = sosa['grid']['ncz']

  ind1_30min = ppf.get_date_ind(voc['conc']['time']['30min'], sosa['time']['raw'][0])
  ind2_30min = ppf.get_date_ind(voc['conc']['time']['30min'], sosa['time']['raw'][-1])
  vocTime_30min = voc['conc']['time']['30min'][ind1_30min:ind2_30min+1]
  vocConc_30min = voc['conc'][vocName]['30min'][ind1_30min:ind2_30min+1,:] * 1.0e-9 * ppp.Nd * 1.0e-6  # [ppbv] --> [molec cm-3]

  ind1_1hour = ppf.get_date_ind(voc['conc']['time']['1hour'], sosa['time']['raw'][0])
  ind2_1hour = ppf.get_date_ind(voc['conc']['time']['1hour'], sosa['time']['raw'][-1])
  vocTime_1hour = voc['conc']['time']['1hour'][ind1_1hour:ind2_1hour+1]
  vocConc_1hour = voc['conc'][vocName]['1hour'][ind1_1hour:ind2_1hour+1,:] * 1.0e-9 * ppp.Nd * 1.0e-6  # [ppbv] --> [molec cm-3]

  ind1_1day = ppf.get_date_ind(voc['conc']['time']['1day'], sosa['time']['raw'][0])
  ind2_1day = ppf.get_date_ind(voc['conc']['time']['1day'], sosa['time']['raw'][-1])
  vocTime_1day = voc['conc']['time']['1day'][ind1_1day:ind2_1day+1]
  vocConc_1day = voc['conc'][vocName]['1day'][ind1_1day:ind2_1day+1,:] * 1.0e-9 * ppp.Nd * 1.0e-6  # [ppbv] --> [molec cm-3]

  fg, ax = plt.subplots(1, 1, figsize=prop['figsize'], dpi=dpi)

  for iz in [10, 14, 18, 22, 25, 26]:
    ax.plot(sosa['time']['raw'], sosa[vocName]['conc']['raw'][:,iz], label='SOSA {0}'.format(sosa['grid']['zz'][iz]))  # level 19, 16.86 m

  for iz in [0,1,2,3,4,5]:
    ax.plot(vocTime_1day, vocConc_1day[:, iz], 'o', label='Conc {0}'.format(voc['conc']['zz'][iz]))

  # Show the legends
  handles, labels = ax.get_legend_handles_labels()
  ax.legend(handles, labels, scatterpoints=1, bbox_to_anchor=(1, 1), loc='upper right', ncol=1)

  fg.savefig('{0}/{1}.png'.format(save_dir, fileName), dpi=dpi)


# def plot_vd(sosa, voc, vocName, prop):
#   save_dir = prop['save_dir']
#   figsize = prop['figsize']
#   dpi = prop['dpi']
#   fileName = prop['fileName']
#
#   ncz = sosa['grid']['ncz']


def plot_comp_flux(sosa, voc, vocName, prop):
  save_dir = prop['save_dir']
  figsize = prop['figsize']
  dpi = prop['dpi']
  fileName = prop['fileName']

  ncz = sosa['grid']['ncz']

  ind1_raw = ppf.get_date_ind(voc['flux']['time']['raw'], sosa['time']['raw'][0])
  ind2_raw = ppf.get_date_ind(voc['flux']['time']['raw'], sosa['time']['raw'][-1])
  vocTime_raw = voc['flux']['time']['raw'][ind1_raw:ind2_raw+1]
  vocFlux_raw = voc['flux'][vocName]['raw'][ind1_raw:ind2_raw+1]  # * 1.0e-9 * ppp.Nd * 1.0e-6  # [ppbv] --> [molec cm-3]

  ind1_30min = ppf.get_date_ind(voc['flux']['time']['30min'], sosa['time']['raw'][0])
  ind2_30min = ppf.get_date_ind(voc['flux']['time']['30min'], sosa['time']['raw'][-1])
  vocTime_30min = voc['flux']['time']['30min'][ind1_30min:ind2_30min+1]
  vocFlux_30min = voc['flux'][vocName]['30min'][ind1_30min:ind2_30min+1]  # * 1.0e-9 * ppp.Nd * 1.0e-6  # [ppbv] --> [molec cm-3]

  # ind1_1hour = ppf.get_date_ind(voc['flux']['time']['1hour'], sosa['time']['raw'][0])
  # ind2_1hour = ppf.get_date_ind(voc['flux']['time']['1hour'], sosa['time']['raw'][-1])
  # vocTime_1hour = voc['flux']['time']['1hour'][ind1_1hour:ind2_1hour+1]
  # vocFlux_1hour = voc['flux'][vocName]['1hour'][ind1_1hour:ind2_1hour+1]  # * 1.0e-9 * ppp.Nd * 1.0e-6  # [ppbv] --> [molec cm-3]

  ind1_1day = ppf.get_date_ind(voc['flux']['time']['1day'], sosa['time']['raw'][0])
  ind2_1day = ppf.get_date_ind(voc['flux']['time']['1day'], sosa['time']['raw'][-1])
  vocTime_1day = voc['flux']['time']['1day'][ind1_1day:ind2_1day+1]
  vocFlux_1day = voc['flux'][vocName]['1day'][ind1_1day:ind2_1day+1]  # * 1.0e-9 * ppp.Nd * 1.0e-6  # [ppbv] --> [molec cm-3]

  fg, ax = plt.subplots(1, 1, figsize=prop['figsize'], dpi=dpi)

  ax.plot(sosa['time']['raw'], sosa[vocName]['flux']['raw'][:,ncz-3], 'r')  # level 19, 16.86 m
  ax.plot(sosa['time']['raw'], sosa[vocName]['flux']['raw'][:,ncz-2], 'g')  # level 19, 16.86 m
  ax.plot(sosa['time']['raw'], sosa[vocName]['flux']['raw'][:,ncz-1], 'b')  # level 19, 16.86 m
  # ax.plot(sosa['time']['raw'], sosa[vocName]['conc']['raw'][:,ncz])  # level 20, 19.96 m
  # ax.plot(sosa['time']['raw'], sosa[vocName]['conc']['raw'][:,ncz+1])
  # ax.plot(voc['conc']['time']['30min'][ind1:ind2+1], vocConc[:, 0])
  # ax.plot(voc['conc']['time']['30min'][ind1:ind2+1], vocConc[:, 1])
  ax.plot(vocTime_raw, vocFlux_raw, 'o')  # level 2, 16.7 m
  ax.plot(vocTime_30min, vocFlux_30min)  # level 2, 16.7 m
  # ax.plot(vocTime_1hour, vocFlux_1hour[:, 2])  # level 2, 16.7 m
  # ax.plot(vocTime_1day, vocFlux_1day)
  # ax.plot(voc['conc']['time']['30min'][ind1:ind2+1], vocConc[:, 3])

  fg.savefig('{0}/{1}.png'.format(save_dir, fileName), dpi=dpi)


#----------------------------------------------------------#
# Plot modelled and measured diurnal cycle of conc and flux
# for every possible VOCs.
#----------------------------------------------------------#
def plot_comp_diurnal_conc_flux(sosa, voc, prop):
  for ic, c in enumerate(dp.VocDataPack.flux_names):
    if (c in dp.VocDataPack.conc_names) and (c in dp.SosaDataPack.gas_list):
      #***** Concentration *****#
      conc_ind0 = ppf.get_date_ind(voc['conc']['time']['raw'], sosa['time']['raw'][0])
      conc_ind1 = ppf.get_date_ind(voc['conc']['time']['raw'], sosa['time']['raw'][-1])
      conc_23 = (voc['conc'][c]['raw'][:,2] + voc['conc'][c]['raw'][:,3]) * 0.5 * 1.0e-9 * ppp.Nd * 1.0e-6  # [ppbv] --> [molec cm-3]
      conc_2 = voc['conc'][c]['raw'][:,2] * 1.0e-9 * ppp.Nd * 1.0e-6  # [ppbv] --> [molec cm-3]
      conc_3 = voc['conc'][c]['raw'][:,3] * 1.0e-9 * ppp.Nd * 1.0e-6  # [ppbv] --> [molec cm-3]
      # print np.count_nonzero(~np.isnan(conc_23))
      # print np.count_nonzero(~np.isnan(voc['conc'][c]['30min'][:,2]))
      # conc, conc_std = ppf.diurnal_hourly_average(voc['conc']['time']['raw'][conc_ind0:conc_ind1], conc_23[conc_ind0:conc_ind1])
      _dtime, conc_2_dc, conc_2_std = ppf.diurnal_average(voc['conc']['time']['raw'][conc_ind0:conc_ind1], conc_2[conc_ind0:conc_ind1])
      conc_dtime, conc_3_dc, conc_3_std = ppf.diurnal_average(voc['conc']['time']['raw'][conc_ind0:conc_ind1], conc_3[conc_ind0:conc_ind1])

      conc_23_dc = 0.5 * (conc_2_dc + conc_3_dc)
      conc_23_std = 0.5 * (conc_2_std + conc_3_std)

      #***** Flux *****#
      flux_ind0 = ppf.get_date_ind(voc['flux']['time']['raw'], sosa['time']['raw'][0])
      flux_ind1 = ppf.get_date_ind(voc['flux']['time']['raw'], sosa['time']['raw'][-1])
      flux_dtime, flux, flux_std = ppf.diurnal_average(voc['flux']['time']['raw'][flux_ind0:flux_ind1], voc['flux'][c]['raw'][flux_ind0:flux_ind1])

      #***** SOSA Conc and Flux at 23 m *****#
      sosa_iz = 20
      _dtime, sosa_conc, sosa_conc_std = ppf.diurnal_average(sosa['time']['raw'], sosa[c]['conc']['raw'][:, sosa_iz])
      sosa_dtime, sosa_flux, sosa_flux_std = ppf.diurnal_average(sosa['time']['raw'], sosa[c]['flux']['raw'][:, sosa_iz])

      vd = flux / conc_23_dc

      prop['fileName'] = 'conc_flux-'+c

      #********** Plot concentration **********#
      # fg, ax = plot_comp_time_series_obs_mod(, data_obs, std_obs, time_mod, data_mod, std_mod):

      ## Create figure and axes
      # fg, ax = plt.subplots(1, 1, figsize=prop['figsize'], dpi=prop['dpi'])

      ## Plot
      fg, ax = plot_comp_time_series_obs_mod(conc_dtime, conc_23_dc, conc_23_std, sosa_dtime, sosa_conc, sosa_conc_std)

      fg.savefig('{0}/{1}-comp_conc-{2}.png'.format(prop['save_dir'], prop['case'], c), dpi=prop['dpi'])

      fg, ax = plot_comp_time_series_obs_mod(flux_dtime, flux, flux_std, sosa_dtime, sosa_flux, sosa_flux_std)

      fg.savefig('{0}/{1}-comp_flux-{2}.png'.format(prop['save_dir'], prop['case'], c), dpi=prop['dpi'])

      # ax.plot(conc_23_dc, 'ro', label='SMR2 conc')
      # ax.plot(flux, 'bo', label='SMR2 flux')
      # ax.plot(sosa_conc, 'r--', label='SOSA conc')
      # ax.plot(sosa_flux, 'b--', label='SOSA flux')

      ## Legend
      # handles, labels = ax.get_legend_handles_labels()
      # ax.legend(handles, labels, scatterpoints=1, bbox_to_anchor=(1, 1), loc='best', ncol=1)

      ## Save the figure
      # fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['fileName']), dpi=prop['dpi'])


#----------------------------------------------------------#
# Plot comparison between different H values from different
# methods.
#----------------------------------------------------------#
def plot_comp_H(fileName):
  DPI = 100
  figName = 'comp_H'
  with open(fileName, 'r') as f:
    header = f.readline().split()
  # print header

  temp = np.genfromtxt(fileName, skip_header=1, delimiter=[5, 15, 50, 10, 10, 10, 10, 4, 10, 10, 5, 5, 20, 11],
    dtype=['i8', 'S15', 'S50', 'f8', 'f8', 'f8', 'f8', 'S4', 'f8', 'f8', 'S5', 'f8', 'S20', 'f8'])
  # {index:5d}{mcm:>15s}{smiles:>50s}{H_sea:10.2e}{H_ee:10.2e}{H_eg:10.2e}{H_eb:10.2e}{H_flag:>4s}{H_A:10.2e}{H_B:10.2e}{f0_flag:>5s}{f0:5.1f}{forms:>20s}{mmass:11.5f}
  # print temp.shape
  # print temp[0]
  H = np.array([ [i[3], i[4], i[5], i[6], i[8], i[9]] for i in temp ])
  index = np.array([ i[0] for i in temp ])
  mcm = np.array([ i[1] for i in temp ])
  smiles = np.array([ i[2] for i in temp ])

  logH = np.log10(H)
  dH = [1, 2]
  nH = len(dH)
  outInd = {}
  for h in dH:
    for k, v in {'ee':1, 'eg':2, 'eb':3}.iteritems():
      outInd[(k, h, v)] = index[np.abs(logH[:,0]-logH[:,v]) >= h]  # k: ee, eg, eb, h: dH, v: index of each methods
  df = pd.DataFrame(data=logH)
  a = df.corr().as_matrix()
  # print df
  # print df.values
  # print df.corr()
  # print ee_out
  # print eg_out
  # print eb_out
  # print outInd

  # # One plot for the whole time series and height levels
  nAxRow = 1
  nAxCol = 3
  nAx = nAxRow * nAxCol

  fg, ax = plt.subplots(nAxRow, nAxCol, figsize=(12,4), dpi=DPI)

  for i, g, gInd in zip(range(3), ['ee', 'eg', 'eb'], [1,2,3]):
    ax[i].loglog(H[:,0], H[:,gInd], 'o', label=g)
    limMin = min(ax[i].get_xlim()[0], ax[i].get_ylim()[0])
    limMax = max(ax[i].get_xlim()[1], ax[i].get_ylim()[1])

    for h, c in zip(dH, ['r', 'g']):
      outInd = index[np.abs(logH[:,0]-logH[:,gInd]) >= h]
      ax[i].loglog(H[outInd-1,0], H[outInd-1,gInd], 'o', color='w', label=str(h))
      ax[i].loglog(H[outInd-1,0], H[outInd-1,gInd], 'o', mec=c, mfc='None', label=str(h))
      limMin = min(limMin, ax[i].get_xlim()[0], ax[i].get_ylim()[0])
      limMax = max(limMax, ax[i].get_xlim()[1], ax[i].get_ylim()[1])

      # print g, h
      # print mcm[outInd-1]
    print(a[0,gInd])
    # print pd.corrcoef(logH[:,0], logH[:,gInd])

    ax[i].set_xlim(limMin, limMax)
    ax[i].set_ylim(limMin, limMax)
    logLim = [math.floor(np.log10(limMin)), math.ceil(np.log10(limMax))]
    ax[i].set_xticks([10**j for j in np.arange(logLim[0], logLim[1], 2)])
    ax[i].set_yticks([10**j for j in np.arange(logLim[0], logLim[1], 2)])
    ax[i].set_aspect('equal')


  # ax[0,1].loglog(H[:,0], H[:,1], 'o', label='EE')
  # ax[0,1].loglog(H[ee_out1-1,0], H[ee_out1-1,1], 'ro', mec='r', mfc='None')  # , ms=5, mfc='None', label='EE outer')
  # ax[0,1].loglog(H[ee_out2-1,0], H[ee_out2-1,1], 'go', mec='g', mfc='None')  # , ms=5, mfc='None', label='EE outer')

  # ax[1,0].loglog(H[:,0], H[:,2], 'o', label='EG')
  # ax[1,0].loglog(H[eg_out1-1,0], H[eg_out1-1,2], 'ro', mec='r', mfc='None')
  # ax[1,0].loglog(H[eg_out2-1,0], H[eg_out2-1,2], 'go')

  # ax[1,1].loglog(H[:,0], H[:,3], 'o', label='EB')
  # ax[1,1].loglog(H[eb_out1-1,0], H[eb_out1-1,3], 'ro')
  # ax[1,1].loglog(H[eb_out2-1,0], H[eb_out2-1,3], 'go')

  # ax[1,1].set_xlim(1e-3, 1e11)
  # ax[1,1].set_ylim(1e-3, 1e11)
  # print 'plot'
  # CS = ax.contourf(td, zz, var.T, 10, origin='lower')
  #   
  # # Set colorbar
  # cbar = plt.colorbar(CS)
  # Show the legends

  # for i in ax:
  # handles, labels = ax_temp[iz].get_legend_handles_labels()
  # ax_temp[iz].legend(handles, labels, scatterpoints=1, bbox_to_anchor=(1, 1), loc='upper left', ncol=1)

  # print save_dir, fileName
  saveDir = '.'
  fg.savefig('{0}/{1}.png'.format(saveDir, figName), dpi=DPI)


def plot_test_single_VOC(sosa, voc, vocName, prop):
  #***** Concentration *****#
  conc_ind0 = ppf.get_date_ind(voc['conc']['time']['raw'], sosa['time']['raw'][0])
  conc_ind1 = ppf.get_date_ind(voc['conc']['time']['raw'], sosa['time']['raw'][-1])
  # conc_23 = (voc['conc'][c]['raw'][:,2] + voc['conc'][c]['raw'][:,3]) * 0.5 * 1.0e-9 * ppp.Nd * 1.0e-6  # [ppbv] --> [molec cm-3]
  conc_2 = voc['conc'][c]['raw'][:,2] * 1.0e-9 * ppp.Nd * 1.0e-6  # [ppbv] --> [molec cm-3]
  # conc_3 = voc['conc'][c]['raw'][:,3] * 0.5 * 1.0e-9 * ppp.Nd * 1.0e-6  # [ppbv] --> [molec cm-3]
  # print np.count_nonzero(~np.isnan(conc_23))
  # print np.count_nonzero(~np.isnan(voc['conc'][c]['30min'][:,2]))
  # conc, conc_std = ppf.diurnal_hourly_average(voc['conc']['time']['raw'][conc_ind0:conc_ind1], conc_23[conc_ind0:conc_ind1])
  conc_2_dc, conc_2_std, _dtime = ppf.diurnal_hourly_average(voc['conc']['time']['raw'][conc_ind0:conc_ind1], conc_2[conc_ind0:conc_ind1])
  conc_3_dc, conc_3_std, _dtime = ppf.diurnal_hourly_average(voc['conc']['time']['raw'][conc_ind0:conc_ind1], conc_3[conc_ind0:conc_ind1])

  conc_23_dc = 0.5 * (conc_2_dc + conc_3_dc)
  conc_23_std = 0.5 * (conc_2_std + conc_3_std)

  #***** Flux *****#
  flux_ind0 = ppf.get_date_ind(voc['flux']['time']['raw'], sosa['time']['raw'][0])
  flux_ind1 = ppf.get_date_ind(voc['flux']['time']['raw'], sosa['time']['raw'][-1])
  flux, flux_std, _dtime = ppf.diurnal_hourly_average(voc['flux']['time']['raw'][flux_ind0:flux_ind1], voc['flux'][c]['raw'][flux_ind0:flux_ind1])

  #***** SOSA Conc and Flux at 23 m *****#
  sosa_iz = 20
  sosa_conc, sosa_conc_std, _dtime = ppf.diurnal_hourly_average(sosa['time']['raw'], sosa[c]['conc']['raw'][:, sosa_iz])
  sosa_flux, sosa_flux_std, _dtime = ppf.diurnal_hourly_average(sosa['time']['raw'], sosa[c]['flux']['raw'][:, sosa_iz])

  vd = flux / conc_23_dc
  # print 'flux', flux
  # print 'conc', conc

  prop['fileName'] = 'conc_flux-'+c
  fg, ax = plt.subplots(1, 1, figsize=prop['figsize'], dpi=prop['dpi'])
  # ax.plot(vd, 'o')
  # ax.plot(conc_2_dc, 'ro')
  # ax.plot(conc_3_dc, 'bo')
  ax.plot(conc_23_dc, 'ro')
  ax.plot(flux, 'bo')
  ax.plot(sosa_conc, 'r--')
  ax.plot(sosa_flux, 'b--')
  fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['fileName']), dpi=prop['dpi'])


#============================================================#
# Plot contributions of sources and sinks of gases in SOSAA
#============================================================#
def plot_sosa_Q_contr(sosa, prop):
  save_dir = prop['save_dir']
  file_name = prop['file_name']
  figsize = prop['figsize']
  dpi = prop['dpi']

  periods = ['all', 'day', 'nit']
  st_str = ['(a) all', '(b) day', '(c) night']

  # Gas list
  gas_list = [ \
    'MT', 'C5H8+MBO', 'SQT', 'CH3CHO', \
    'CH3OH', 'CH3COCH3', 'HCHO', 'ACETOL', 'PINIC', \
    'BCSOZOH', 'ISOP34OOH', 'ISOP34NO3', \
    ]
  name_list = [ \
    'monoterpenes', 'isoprene+MBO', 'sesquiterpenes', 'acetaldehyde', \
    'methanol', 'acetone', 'formaldehyde', 'acetol', 'pinic acid', \
    'BCSOZOH', 'ISOP34OOH', 'ISOP34NO3', \
    ]
  ng = len(gas_list)

  bw = 1
  # ct = {'emis': '#4daf4a', 'chem': '#e41a1c', 'turb': '#377eb8', 'depo': '#984ea3'}  # dark
  ct = {'emis': '#a6d854', 'chem': '#fb8072', 'turb': '#80b1d3', 'depo': '#bebada'}  # light
  # ct = {'emis': '#a6d854', 'chem': '#fc8d62', 'turb': '#8da0cb', 'depo': '#e78ac3'}  # soft

  # Get Q values
  Q, nc, mc, ind = get_Q_values(sosa)

  # Initialize contributions for all gases
  contr_emis = {}
  contr_chem = {}
  contr_turb = {}
  contr_depo = {}

  # Calculate contributions for every period and all gases
  for p in periods:
    contr_emis[p] = np.zeros((ng,))
    contr_chem[p] = np.zeros((ng,))
    contr_turb[p] = np.zeros((ng,))
    contr_depo[p] = np.zeros((ng,))

    # Calculate contributions for every gases
    for ig, g in enumerate(gas_list):
      contr_emis[p][ig], contr_chem[p][ig], contr_turb[p][ig], contr_depo[p][ig] = \
        calculate_relative_contribution( \
        [Q['emis']['dct'][p][g], Q['chem']['dct'][p][g], Q['turb']['dct'][p][g], Q['depo']['dct'][p][g]] )
      print('{0:4s} {1:15s} {2:6.3f} {3:6.3f} {4:6.3f} {5:6.3f}'.format( \
        p, g, contr_emis[p][ig], contr_chem[p][ig], contr_turb[p][ig], contr_depo[p][ig]))

  # Plot Q contribution
  fg, ax = plt.subplots(3, 1, figsize=figsize, dpi=dpi)
  for ip, p in enumerate(periods):
    axes_bar_stack(ax[ip], np.arange(ng)+0.5, \
      [contr_emis[p], contr_chem[p], contr_turb[p], contr_depo[p]], \
      1, [ct['emis'], ct['chem'], ct['turb'], ct['depo']], alpha=1)

    ax[ip].set_xlim(0, ng)
    ax[ip].set_ylim(-1, 1)

    ax[ip].set_xticks(np.arange(ng)+bw/2.0)
    ax[ip].set_xticklabels(name_list)
    ax[ip].set_yticks(np.arange(-1, 1.1, 0.2))
    ax[ip].set_ylabel(r'$\overline{Q}^{\Delta,h_c}_{rel,n}$')

    plt.setp(ax[ip].xaxis.get_majorticklabels(), rotation=30, ha='right', fontsize=18)

    # Put the legend box outside
    if ip==0:
      alpha = 1.0
      p_emis = mpatches.Patch(color=ct['emis'], linewidth=0, alpha=alpha)
      p_chem = mpatches.Patch(color=ct['chem'], linewidth=0, alpha=alpha)
      p_turb = mpatches.Patch(color=ct['turb'], linewidth=0, alpha=alpha)
      p_depo = mpatches.Patch(color=ct['depo'], linewidth=0, alpha=alpha)
      lgd = ax[ip].legend([p_emis, p_chem, p_turb, p_depo], ['emis', 'chem', 'turb', 'depo'],
        bbox_to_anchor=(0.3, 1.0, 0.7, 0.2), loc=3, ncol=4, mode='expand', prop={'size': 16})
      # handles,labels = ax.get_legend_handles_labels()
      # ax.legend(handles, labels, loc='best', scatterpoints=1)

    # Subtitles
    ax[ip].set_title(st_str[ip], fontsize=18, x=0.06, y=1.05)

  fg.tight_layout()
  fg.savefig('{0}/{1}_combined.png'.format(save_dir, file_name), dpi=dpi, bbox_inches='tight')

  return (gas_list, contr_emis, contr_chem, contr_turb, contr_depo)

  #===== Calculate the contribution fraction =====#
  ncz = sosa['grid']['ncz']
  ddz = sosa['grid']['ddz']

  sosaq = {}
  for ig, g in enumerate(gas_list):
    sosaq[g] = {}
    sosaq[g]['conc_all'] = stats.nanmean( np.sum( sosa[g]['conc']['raw'][:, 1:ncz+1]*ddz[np.newaxis, 1:ncz+1] ) / np.sum(ddz[1:ncz+1]) )
    sosaq[g]['Qemis_all'] = stats.nanmean(sosa[g]['Qemis_int']['raw'])  # monthly average
    sosaq[g]['Qchem_all'] = stats.nanmean(sosa[g]['Qchem_int']['raw'])
    sosaq[g]['Qdepo_all'] = stats.nanmean(sosa[g]['Qdepo_int']['raw'])
    sosaq[g]['Qturb_all'] = stats.nanmean(sosa[g]['Qturb_int']['raw'])
    pos = 0.0
    neg = 0.0
    pos = sosaq[g]['Qemis_all']
    neg = sosaq[g]['Qdepo_all']
    if sosaq[g]['Qchem_all'] >= 0:
      pos += sosaq[g]['Qchem_all']
    else:
      neg += sosaq[g]['Qchem_all']
    if sosaq[g]['Qturb_all'] >= 0:
      pos += sosaq[g]['Qturb_all']
    else:
      neg += sosaq[g]['Qturb_all']
    sosaq[g]['Qmax_all'] = np.amax( [pos, -neg] )
    # sosaq[g]['Qmax_all'] = np.amax([abs(sosaq[g]['Qemis_all']), abs(sosaq[g]['Qchem_all']),
    #   abs(sosaq[g]['Qdepo_all']), abs(sosaq[g]['Qturb_all'])])

    nit_ind = np.squeeze( sosa['n_ind']['raw'] )
    sosaq[g]['conc_nit'] = stats.nanmean( np.sum( sosa[g]['conc']['raw'][nit_ind, 1:ncz+1]*ddz[np.newaxis, 1:ncz+1] ) / np.sum(ddz[1:ncz+1]) )
    sosaq[g]['Qemis_nit'] = stats.nanmean(sosa[g]['Qemis_int']['raw'][nit_ind])
    sosaq[g]['Qchem_nit'] = stats.nanmean(sosa[g]['Qchem_int']['raw'][nit_ind])
    sosaq[g]['Qdepo_nit'] = stats.nanmean(sosa[g]['Qdepo_int']['raw'][nit_ind])
    sosaq[g]['Qturb_nit'] = stats.nanmean(sosa[g]['Qturb_int']['raw'][nit_ind])
    pos = 0.0
    neg = 0.0
    pos = sosaq[g]['Qemis_nit']
    neg = sosaq[g]['Qdepo_nit']
    if sosaq[g]['Qchem_nit'] >= 0:
      pos += sosaq[g]['Qchem_nit']
    else:
      neg += sosaq[g]['Qchem_nit']
    if sosaq[g]['Qturb_nit'] >= 0:
      pos += sosaq[g]['Qturb_nit']
    else:
      neg += sosaq[g]['Qturb_nit']
    sosaq[g]['Qmax_nit'] = np.amax( [pos, -neg] )
    # sosaq[g]['Qmax_nit'] = np.amax([abs(sosaq[g]['Qemis_nit']), abs(sosaq[g]['Qchem_nit']),
    #   abs(sosaq[g]['Qdepo_nit']), abs(sosaq[g]['Qturb_nit'])])

    day_ind = np.squeeze( sosa['d_ind']['raw'] )
    sosaq[g]['conc_day'] = stats.nanmean( np.sum( sosa[g]['conc']['raw'][day_ind, 1:ncz+1]*ddz[np.newaxis, 1:ncz+1] ) / np.sum(ddz[1:ncz+1]) )
    sosaq[g]['Qemis_day'] = stats.nanmean(sosa[g]['Qemis_int']['raw'][day_ind])
    sosaq[g]['Qchem_day'] = stats.nanmean(sosa[g]['Qchem_int']['raw'][day_ind])
    sosaq[g]['Qdepo_day'] = stats.nanmean(sosa[g]['Qdepo_int']['raw'][day_ind])
    sosaq[g]['Qturb_day'] = stats.nanmean(sosa[g]['Qturb_int']['raw'][day_ind])
    pos = 0.0
    neg = 0.0
    pos = sosaq[g]['Qemis_day']
    neg = sosaq[g]['Qdepo_day']
    if sosaq[g]['Qchem_day'] >= 0:
      pos += sosaq[g]['Qchem_day']
    else:
      neg += sosaq[g]['Qchem_day']
    if sosaq[g]['Qturb_day'] >= 0:
      pos += sosaq[g]['Qturb_day']
    else:
      neg += sosaq[g]['Qturb_day']
    sosaq[g]['Qmax_day'] = np.amax( [pos, -neg] )
    # sosaq[g]['Qmax_day'] = np.amax([abs(sosaq[g]['Qemis_day']), abs(sosaq[g]['Qchem_day']),
    #   abs(sosaq[g]['Qdepo_day']), abs(sosaq[g]['Qturb_day'])])

  for tw in ['all', 'day', 'nit']:
    print('Ploting {0} ...'.format(tw))
    fg, ax = plt.subplots(1, 1, figsize=figsize, dpi=dpi)
    # for ig, g in enumerate(sosa['gas_list']):
    for ig, g in enumerate(gas_list):
      print('Ploting contributions of {0} ...'.format(g))

      ##### Plot the ratio between dC and C
      # ax.plot(ig+0.5, sosaq[g]['Qmax_'+tw]/1.0e6/np.sum(ddz[1:ncz+1])*1800.0 / sosaq[g]['conc_'+tw], 'ko')

      # balance_int = sosa[g]['Qconc_int'] - (sosa[g]['Qturb_int']+sosa[g]['Qemis_int']+sosa[g]['Qchem_int']+sosa[g]['Qdepo_int'])

      Qemis_frac = sosaq[g]['Qemis_'+tw] / sosaq[g]['Qmax_'+tw]
      Qchem_frac = sosaq[g]['Qchem_'+tw] / sosaq[g]['Qmax_'+tw]
      Qturb_frac = sosaq[g]['Qturb_'+tw] / sosaq[g]['Qmax_'+tw]
      Qdepo_frac = sosaq[g]['Qdepo_'+tw] / sosaq[g]['Qmax_'+tw]

      # Output accurate values for Q
      print(tw, ig, g, Qemis_frac, Qchem_frac, Qturb_frac, Qdepo_frac)

      # ax.bar(ig, Qemis_frac, bw, color=ct['emis'], label='Qemis')
      # ax.bar(ig+bw, Qchem_frac, bw, color=ct['chem'], label='Qchem')
      # ax.bar(ig+2*bw, Qturb_frac, bw, color=ct['turb'], label='Qturb')
      # ax.bar(ig+3*bw, Qdepo_frac, bw, color=ct['depo'], label='Qdepo')

      ax.bar(ig, Qemis_frac, bw, color=ct['emis'], label='Qemis')
      ax.bar(ig, Qdepo_frac, bw, color=ct['depo'], label='Qdepo')
      print(tw, g, Qchem_frac, Qturb_frac, Qemis_frac, Qdepo_frac)
      if Qchem_frac >= 0:
        ax.bar(ig, Qchem_frac, bw, color=ct['chem'], bottom=Qemis_frac, label='Qchem')
      else:
        ax.bar(ig, Qchem_frac, bw, color=ct['chem'], bottom=Qdepo_frac, label='Qchem')

      if Qturb_frac >= 0:
        if Qchem_frac >= 0:
          ax.bar(ig, Qturb_frac, bw, color=ct['turb'], bottom=Qemis_frac+Qchem_frac, label='Qturb')
        else:
          ax.bar(ig, Qturb_frac, bw, color=ct['turb'], bottom=Qemis_frac, label='Qturb')
      else:
        if Qchem_frac >= 0:
          ax.bar(ig, Qturb_frac, bw, color=ct['turb'], bottom=Qdepo_frac, label='Qturb')
        else:
          ax.bar(ig, Qturb_frac, bw, color=ct['turb'], bottom=Qdepo_frac+Qchem_frac, label='Qturb')

      # ax.set_xlim(0,28)
      # ax.set_ylim(-1,1)
      # ax.set_xticks(ind+width)
      ax.set_xticks(np.arange(ng)+bw/2.0)
      ax.set_xticklabels(gas_list)
      plt.setp(ax.xaxis.get_majorticklabels(), rotation=30, ha='right', fontsize=18)

      # ax.grid(True)

      ##### Put the legend box outside
      if ig==0:
        alpha = 1.0
        p_emis = mpatches.Patch(color=ct['emis'], linewidth=0, alpha=alpha)
        p_chem = mpatches.Patch(color=ct['chem'], linewidth=0, alpha=alpha)
        p_turb = mpatches.Patch(color=ct['turb'], linewidth=0, alpha=alpha)
        p_depo = mpatches.Patch(color=ct['depo'], linewidth=0, alpha=alpha)
        lgd = fg.legend([p_emis, p_chem, p_turb, p_depo], ['Qemis', 'Qchem', 'Qturb', 'Qdepo'],
          bbox_to_anchor=(0.09, 0.95, 0.5, 0.2), loc=3, ncol=4, mode='expand', prop={'size': 16})
        # handles,labels = ax.get_legend_handles_labels()
        # ax.legend(handles, labels, loc='best', scatterpoints=1)

    print(prop['file_name'], tw)
    fg.savefig('{0}/{1}_{2}.png'.format(save_dir, prop['file_name'], tw), dpi=dpi, bbox_extra_artists=(lgd,), bbox_inches='tight')


    ##### Plot time series of Q #####

def plot_prof_average(zz1, c1, prop1, zz2, c2, prop2):
  print(c1.shape, c2.shape)

  save_dir = prop1['save_dir']
  fileName = prop1['fileName']
  figsize = prop1['figsize']
  dpi = prop1['dpi']

  c1_prof_avg = stats.nanmean(c1, axis=0)
  c1_prof_std = stats.nanstd(c1, axis=0)
  c2_prof_avg = stats.nanmean(c2, axis=0)
  c2_prof_std = stats.nanstd(c2, axis=0)

  fg, ax = plt.subplots(1, 1, figsize=figsize, dpi=dpi)

  ax.plot(c1_prof_avg, zz1, label=prop1['label'])
  ax.plot(c2_prof_avg, zz2, 'o', label=prop2['label'])

  ax.set_ylim(0, 100)

  fg.savefig('{0}/{1}.png'.format(save_dir, fileName), dpi=dpi)


#============================================================#
# Plot energy fluxes
#============================================================#


#============================================================#
# Plot standard emission potentials
#============================================================#
def plot_sep(name, sosa_sep, paper_sep, used_sep):
  """
  " name: name of the chemical compound
  " sosa_sep: 1D array or a scalar
  " paper_sep: dictionary, {ref name: (mean, std), ...}
  " used_sep: 1D array or a scalar, usually the same type and format as the sosa_dep
  """

  sosa_sep_mean = np.nanmean(sosa_sep)
  sosa_sep_min = np.amin(sosa_sep)
  sosa_sep_max = np.amax(sosa_sep)

  ##### Usually paper_sep is a list of several values in papers
  # paper_sep_mean = np.nanmean(paper_sep)
  # paper_sep_min = np.nanmean(paper_sep)
  # paper_sep_max = np.nanmean(paper_sep)

  ##### Currently used value
  used_sep_mean = np.nanmean(used_sep)
  used_sep_min = np.amin(used_sep)
  used_sep_max = np.amax(used_sep)

  #*************************************#
  # Plot figure
  #*************************************#
  save_dir = '../../Discussion/paper_v1_20160513/Figures_2'
  figsize = (12, 8)
  dpi = 150

  fg, ax = plt.subplots(1, 1, figsize=figsize, dpi=dpi)

  ax.plot(sosa_sep, color='r', linestyle='-', label='sosa')
  ax.axhline(sosa_sep_mean, color='r', linestyle='--', label='sosa mean')

  ls = ['-', '--', '-.']
  for i, (k, v) in enumerate(paper_sep.items()):
    ax.axhline(v[0], color='b', linestyle=ls[i], label='{0}'.format(k))

  ax.plot(used_sep, color='k', linestyle='-', label='used')
  ax.axhline(used_sep_mean, color='k', linestyle='--', label='used mean')

  ax.set_xlabel('Time')
  ax.set_ylabel(r'SEP [ng m$^{-2}$ s$^{-1}$')

  ax.set_title('{0}'.format(name))

  ##### legends
  handles, labels = ax.get_legend_handles_labels()
  ax.legend(handles, labels, scatterpoints=1, bbox_to_anchor=(1, 1), loc='best', ncol=1)

  fg.savefig('{0}/sep_{1}.png'.format(save_dir, name), dpi=dpi)


def show_sep(MT_sep_raw, other_sep_raw):
  ## SEP variables for MT
  MT_names = ['alpha-pinene', 'delta3-carene', 'beta-pinene', 'limonene', 'other MT', \
    'myrcene', 'sabinene', 'ocimene', '1,8-cineole', 'linalool']

  MT_sep = np.zeros( (10, 4) )

  MT_sep_mean = np.nanmean(MT_sep_raw)
  MT_sep_std = np.nanstd(MT_sep_raw)
  MT_sep_min = np.amin(MT_sep_raw)
  MT_sep_max = np.amax(MT_sep_raw)

  MT_sep[0, :] = 0.437 * np.array([MT_sep_mean, MT_sep_std, MT_sep_min, MT_sep_max])  # alpha-pinene
  MT_sep[1, :] = 0.396 * np.array([MT_sep_mean, MT_sep_std, MT_sep_min, MT_sep_max])  # delta3-carene
  MT_sep[2, :] = 0.090 * np.array([MT_sep_mean, MT_sep_std, MT_sep_min, MT_sep_max])  # beta-pinene
  MT_sep[3, :] = 0.023 * np.array([MT_sep_mean, MT_sep_std, MT_sep_min, MT_sep_max])  # limonene
  MT_sep[4, :] = 0.053 * np.array([MT_sep_mean, MT_sep_std, MT_sep_min, MT_sep_max])  # other MT
  MT_sep[5, :] = 0.000 * np.array([MT_sep_mean, MT_sep_std, MT_sep_min, MT_sep_max])  # myrcene
  MT_sep[6, :] = 0.000 * np.array([MT_sep_mean, MT_sep_std, MT_sep_min, MT_sep_max])  # sabinene
  MT_sep[7, :] = 0.000 * np.array([MT_sep_mean, MT_sep_std, MT_sep_min, MT_sep_max])  # ocimene
  MT_sep[8, :] = 0.001 * np.array([MT_sep_mean, MT_sep_std, MT_sep_min, MT_sep_max])  # 1,8-cineole
  MT_sep[9, :] = 0.000 * np.array([MT_sep_mean, MT_sep_std, MT_sep_min, MT_sep_max])  # linalool

  ## Other compounds
  other_names = ['isoprene', 'myrcene', 'sabinene', 'limonene', 'delta3-carene', \
    'ocimene', 'beta-pinene', 'alpha-pinene', 'other MT', 'farnesene', \
    'beta-caryophellene', 'other SQT', 'MBO', 'methanol', 'acetone', \
    'CH4', 'NO', 'acetaldehyde', 'formaldehyde', 'CO', \
    '1,8-cineole', 'linalool']

  ## July: day 183 to 214, the data is for 366 days
  other_sep_mean = np.nanmean(other_sep_raw[183:214, :], axis=0)
  other_sep_std = np.nanstd(other_sep_raw[183:214, :], axis=0)
  other_sep_min = np.amin(other_sep_raw[183:214, :], axis=0)
  other_sep_max = np.amax(other_sep_raw[183:214, :], axis=0)

  ##### Print SEP for each compound
  print('{0:23s}: {1:>10s} {2:>10s} {3:>10s} {4:>10s}'.format('name', 'mean', 'std', 'min', 'max'))
  print('EF_day_1.txt')
  for i, on in enumerate(other_names):
    print('{0:02d} {1:20s}: {2:10.2f} {3:10.2f} {4:10.2f} {5:10.2f}'.format( \
      i+1, on, other_sep_mean[i], other_sep_std[i], other_sep_min[i], other_sep_max[i]))

  print('\nEFJ_mono.txt')
  for i, mn in enumerate(MT_names):
    print('{0:02d} {1:20s}: {2:10.2f} {3:10.2f} {4:10.2f} {5:10.2f}'.format( \
      i+1, mn, *MT_sep[i]))


#----------------------------------------------------------#
#
# Plots for paper
#
#----------------------------------------------------------#

def plot_flux_diurnal_pattern_all_in_one(sosa_data, voc_data, ind_nan, prop):
  print('Plotting diurnal pattern ...')
  ##### Set parameters
  ncz = sosa_data['grid']['ncz']
  zz = sosa_data['grid']['zz']

  k_list = ['MT', 'C5H8+MBO', 'CH3OH', 'CH3CHO', 'CH3COCH3', 'HCHO']
  c_list = ['monoterpenes', 'isoprene+MBO', 'methanol', 'acetaldehyde', 'acetone', 'formaldehyde']
  c1_list = ['monoterpenes', 'isoprene', 'MBO', 'methanol', 'acetaldehyde', 'acetone', 'formaldehyde']
  st_list = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)']

  ##### Plot
  nrow = 2
  ncol = 3
  fg, ax = plt.subplots(nrow, ncol, figsize=prop['figsize'], dpi=prop['dpi'])

  for ic, (k, c) in enumerate(zip(k_list, c_list)):
    print(k, c)

    ## subplot position
    row = ic // ncol
    col = ic % ncol

    ## sosa diurnal flux at 23 m (ncz + 1)
    sosa_time = sosa_data['time']['raw']
    if k == 'C5H8+MBO':
      sosa_flux = sosa_data['C5H8']['flux']['raw'][:, ncz+1] * ppf.moleccm2s1_to_ugm2s1(ppp.molar_mass['C5H8']*1.0e-3) + \
        sosa_data['MBO']['flux']['raw'][:, ncz+1] * ppf.moleccm2s1_to_ugm2s1(ppp.molar_mass['MBO']*1.0e-3)
    else:
      sosa_flux = sosa_data[k]['flux']['raw'][:, ncz+1] * ppf.moleccm2s1_to_ugm2s1(ppp.molar_mass[k]*1.0e-3)
    sosa_dtime, sosa_flux_davg, sosa_flux_dstd = ppf.diurnal_average(sosa_time, sosa_flux)

    ## measured voc diurnal flux at 23 m
    ind1 = ppf.get_date_ind(voc_data['flux']['time']['raw'], sosa_time[0])  # ind1: 2010, 6, 30, 23, 22, 30, ind1+1: 2010, 7, 1, 2, 22, 30
    ind2 = ppf.get_date_ind(voc_data['flux']['time']['raw'], sosa_time[-1])  # ind2: 2010, 7, 31, 23, 22, 30, ind2+1: 2010, 8, 1, 2, 22, 30
    voc_time = voc_data['flux']['time']['raw'][ind1+1:ind2+1]
    if k == 'C5H8+MBO':
      voc1 = np.copy( voc_data['flux']['C5H8']['raw'][ind1+1:ind2+1] )
      voc1[ind_nan] = np.nan
      voc2 = np.copy( voc_data['flux']['MBO']['raw'][ind1+1:ind2+1] )
      voc2[ind_nan] = np.nan
      voc_flux = voc1 * ppf.moleccm2s1_to_ugm2s1(ppp.molar_mass['C5H8']*1.0e-3) + \
        voc2 * ppf.moleccm2s1_to_ugm2s1(ppp.molar_mass['MBO']*1.0e-3)
    else:
      voc1 = np.copy( voc_data['flux'][k]['raw'][ind1+1:ind2+1] )
      voc1[ind_nan] = np.nan
      voc_flux = voc1 * ppf.moleccm2s1_to_ugm2s1(ppp.molar_mass[k]*1.0e-3)
    voc_dtime, voc_flux_davg, voc_flux_dstd = ppf.diurnal_average(voc_time, voc_flux)

    # Print some key values
    print('VOC fluxes mod - obs: ', k, sosa_flux_davg - voc_flux_davg)
    print('VOC fluxes (mod - obs)/obs: ', k, (sosa_flux_davg - voc_flux_davg)/voc_flux_davg)

    prop1 = {'color': cmap_qua_colors[0], 'linewidth': 2}
    prop2 = {'color': cmap_qua_colors[1], 'fmt': 'o'}
    l1, l2 = axes_2_xy_std(ax[row, col], sosa_dtime, sosa_flux_davg, sosa_flux_dstd, prop1, \
      voc_dtime, voc_flux_davg, voc_flux_dstd, prop2)

  ## Set limits and ticks

  for i in range(nrow):
    for j in range(ncol):
      print(i, j)
      iax = i*ncol+j
      ax[i, j].set_xlim(0, 24)
      ax[i, j].set_xticks(range(0, 25, 4))
      ax[i, j].text(0.05, 0.9, st_list[iax] + ' ' + c_list[iax], transform=ax[i, j].transAxes, fontsize=14)
  ax[1, 0].set_xlabel('Time [h]')
  ax[1, 0].set_ylabel(r'Flux [$\mu$g m$^{-2}$ s$^{-1}$]')

  ## Adjust space between subplots
  fg.tight_layout()

  ## Save the figure
  fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['file_name']), dpi=prop['dpi'])


def get_Q_values(sosa):
  #********************************************#
  # Initialize
  #********************************************#
  ##### Gas list
  gases = [ \
    'MT', 'C5H8+MBO', 'CH3CHO', 'SQT', 'CH3OH', \
    'CH3COCH3', 'HCHO', 'ACETOL', 'PINIC', 'BCSOZOH', \
    'ISOP34NO3', 'ISOP34OOH', 'C5H8', 'MBO', \
    ]

  ##### Term list
  terms = ['emis', 'turb', 'depo', 'chem']

  ##### Period list
  periods = ['all', 'day', 'nit']

  ##### Initiate Q
  Q = {}
  for t in terms:
    Q[t] = {}
    Q[t]['raw'] = {}  # raw data of Q, Q = dC over 30 min
    Q[t]['prof'] = {}  # Q vertical profile
    Q[t]['dct'] = {}  # dc/dt
    for p in periods:
      Q[t]['prof'][p] = {}
      Q[t]['dct'][p] = {}

  ##### Initiate mass concentration and number concentration
  nc = {}
  nc['raw'] = {}
  nc['mid'] = {}
  nc['cnp'] = {}
  nc['cnp']['raw'] = {}
  nc['prof'] = {}

  mc = {}
  mc['raw'] = {}
  mc['mid'] = {}
  mc['cnp'] = {}
  mc['cnp']['raw'] = {}
  mc['prof'] = {}
  for p in periods:
    nc['cnp'][p] = {}
    nc['prof'][p] = {}
    mc['cnp'][p] = {}
    mc['prof'][p] = {}


  ##### Set other variables
  ncz = sosa['grid']['ncz']
  ddz = sosa['grid']['ddz']  # [m]
  hc = np.sum(ddz[1:ncz+1])
  dt = sosa['time']['dt_output']  # [s], 1800

  ind = {}
  ind['d_ind'] = sosa['d_ind']['raw']
  ind['n_ind'] = sosa['n_ind']['raw']
  ind['a_ind'] = np.copy(ind['d_ind'])
  ind['a_ind'][:] = True

  #********************************************#
  # Calculate values
  #********************************************#
  for ig, g in enumerate(gases):
    ##### Some direct values from sosa datapack
    if g == 'C5H8+MBO':
      unit_conv_C5H8 = ppf.moleccm3_to_ugm3(ppp.molar_mass['C5H8']*1.0e-3)
      unit_conv_MBO = ppf.moleccm3_to_ugm3(ppp.molar_mass['MBO']*1.0e-3)
      for t in terms:
        Q[t]['raw'][g] = sosa['C5H8']['Q'+t]['raw'] * unit_conv_C5H8 + sosa['MBO']['Q'+t]['raw'] * unit_conv_MBO  # [ug m-3]
        Q[t]['dct'][g] = sosa['C5H8']['Q'+t+'_dct']['raw'] * unit_conv_C5H8 + \
          sosa['MBO']['Q'+t+'_dct']['raw'] * unit_conv_MBO  # [ug m-3 s-1], concentration inside the canopy change

      nc['raw'][g] = sosa['C5H8']['conc']['raw'] + sosa['MBO']['conc']['raw']  # [molec cm-3]
      mc['raw'][g] = sosa['C5H8']['conc']['raw'] * unit_conv_C5H8 + sosa['MBO']['conc']['raw'] * unit_conv_MBO  # [ug m-3]

    else:
      unit_conv = ppf.moleccm3_to_ugm3(ppp.molar_mass[g]*1.0e-3)
      for t in terms:
        Q[t]['raw'][g] = sosa[g]['Q'+t]['raw'] * unit_conv  # [ug m-3]
        Q[t]['dct'][g] = sosa[g]['Q'+t+'_dct']['raw'] * unit_conv  # [ug m-3 s-1]

      nc['raw'][g] = sosa[g]['conc']['raw']  # [molec cm-3]
      mc['raw'][g] = sosa[g]['conc']['raw'] * unit_conv  # [ug m-3]

    nc['mid'][g] = np.copy(nc['raw'][g])  # (nt, nz), mid and raw are at the same time points
    nc['mid'][g][1:, :] = 0.5 * ( nc['raw'][g][0:-1, :] + nc['raw'][g][1:, :] )  # average value during previous 30 mins
    nc['cnp']['raw'][g] = np.nanmean(nc['mid'][g][:, 1:ncz+1]*ddz[1:ncz+1]/hc, axis=1)  # (nt,), average concentration within the canopy during previous 30 mins

    mc['mid'][g] = np.copy(mc['raw'][g])  # (nt, nz)
    mc['mid'][g][1:, :] = 0.5 * ( mc['raw'][g][0:-1, :] + mc['raw'][g][1:, :] )  # average value during previous 30 mins
    mc['cnp']['raw'][g] = np.nanmean(mc['mid'][g][:, 1:ncz+1]*ddz[1:ncz+1]/hc, axis=1)  # (nt,), average concentration within the canopy during previous 30 mins

    ##### Q prof and Q dct at different periods
    for t in terms:
      for p, ki in zip(periods, ['a_ind', 'd_ind', 'n_ind']):
        ii = ind[ki]
        Q[t]['prof'][p][g] = np.nanmean(Q[t]['raw'][g][ii, :], axis=0)  # [ug m-3], (nz,)
        Q[t]['dct'][p][g] = np.nanmean(Q[t]['dct'][g][ii], axis=0)  # [ug m-3 s-1], scalar

        nc['prof'][p][g] = np.nanmean(nc['mid'][g][ii, :], axis=0)  # [molec cm-3], (nz,)
        mc['prof'][p][g] = np.nanmean(mc['mid'][g][ii, :], axis=0)  # [ug m-3], (nz,)
        # nc['prof'][p][g] = np.nanmedian(nc['mid'][g][ii, :], axis=0)  # [molec cm-3], use median instead of mean
        # mc['prof'][p][g] = np.nanmedian(mc['mid'][g][ii, :], axis=0)  # [ug m-3], use median instead of mean

        nc['cnp'][p][g] = np.nanmean(nc['cnp']['raw'][g][ii])  # [molec cm-3], scalar
        mc['cnp'][p][g] = np.nanmean(mc['cnp']['raw'][g][ii])  # [ug m-3], scalar

  return Q, nc, mc, ind


def plot_Q_profile_all_in_one(sosa, prop):
  print('Plotting profiles of Q in one figure ...')

  Q, nc, mc, ind = get_Q_values(sosa)

  #********************************************#
  # Set parameters
  #********************************************#
  ##### gas list
  emis_gas_list   = [ 'MT', 'C5H8+MBO', 'SQT', 'CH3CHO', 'CH3OH', 'CH3COCH3' ]
  emis_name_list   = [ 'monoterpenes', 'isoprene+MBO', 'sesquiterpenes', 'acetaldehyde', 'methanol', 'acetone' ]
  noemis_gas_list = [ 'HCHO', 'ACETOL', 'PINIC', 'BCSOZOH', 'ISOP34NO3', 'ISOP34OOH' ]
  noemis_name_list = [ 'formaldehyde', 'acetol', 'pinic acid', 'BCSOZOH', 'ISOP34NO3', 'ISOP34OOH' ]

  ##### Other variables
  dt  = 1800.0  # [s], output time interval
  ddz = sosa['grid']['ddz']
  hc = sosa['grid']['hc']
  ncz = sosa['grid']['ncz']
  zz  = sosa['grid']['zz']
  ddz_new = np.copy(ddz)
  ddz_new[1] -= 0.5*zz[1]
  time = sosa['time']['raw']
  # put time point 15 minutes ahead to represent the concentration change in the middle time interval
  mtime = time - datetime.timedelta(minutes=15)
  # print('hc, ddz', hc, ddz)

  # Print some key values
  factor = 3.6e3  # 1 hour
  for i, g in enumerate(emis_gas_list + noemis_gas_list):
    Qdepo_all = Q['depo']['prof']['all'][g]/dt * ddz/hc * factor
    Qdepo_day = Q['depo']['prof']['day'][g]/dt * ddz/hc * factor
    Qdepo_nit = Q['depo']['prof']['nit'][g]/dt * ddz/hc * factor

    print(g)
    print('Depo all soil/integrated:', Qdepo_all[1]/np.nansum(Qdepo_all))
    print('Depo day soil/integrated:', Qdepo_day[1]/np.nansum(Qdepo_day))
    print('Depo nit soil/integrated:', Qdepo_nit[1]/np.nansum(Qdepo_nit))
    print('Qnemis', Q['emis']['prof']['all'][g]/dt * ddz/hc * factor)
    print('Qnchem', Q['chem']['prof']['all'][g]/dt * ddz/hc * factor)
    print('Qnturb', Q['turb']['prof']['all'][g]/dt * ddz/hc * factor)
    print('Qndepo', Q['depo']['prof']['all'][g]/dt * ddz/hc * factor)

  ##### Q related values
  terms = ['emis', 'turb', 'depo', 'chem']
  periods = ['all', 'day', 'nit']

  ##### Test for debug
  # dtime, Qdepo_davg, Qdepo_dstd = ppf.diurnal_average(mtime, Q['depo']['dct']['PINIC'])
  # dtime, mc_davg, mc_dstd = ppf.diurnal_average(mtime, mc['cnp']['raw']['PINIC'])
  # print(Qdepo_davg/mc_davg)
  # print(np.nanmean(Q['depo']['dct']['PINIC']) / mc['cnp']['all']['PINIC'])
  # print(np.sum(Q['depo']['prof']['all']['PINIC']/dt*ddz/(mc['cnp']['all']['PINIC']*hc)))

  #********************************************#
  # Plot
  #********************************************#
  # ct = {'emis': '#a6d854', 'chem': '#fb8072', 'turb': '#80b1d3', 'depo': '#bebada', 'conc': '#1d1d1d'}  # light
  ct = {'emis': '#4daf4a', 'chem': '#e41a1c', 'turb': '#377eb8', 'depo': '#984ea3', 'conc': '#1d1d1d'}  # light
  st_list = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', \
    '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', \
    '(m)', '(n)', '(o)', '(p)', '(q)', '(r)' ]
  suf = ['-emis', '-noemis']

  #***** Profiles of Q/C for whole day, daytime and nighttime *****#
  nrow = 3
  ncol = 6

  for il, gn in enumerate([(emis_gas_list, emis_name_list), (noemis_gas_list, noemis_name_list)]):
    fg, ax = plt.subplots(3, 6, figsize=(16, 24), dpi=prop['dpi'])
    gl, nl = gn
    xlim1 = np.zeros( (2, len(periods), len(gl)) )
    xlim1[0, 0, :] = [8.0, 1.0, 1.0, 1.0, 2.0, 4.0]
    xlim1[0, 1, :] = [8.0, 1.0, 1.0, 1.0, 2.0, 4.0]
    xlim1[0, 2, :] = [4.0, 5.0e-3, 0.5, 0.5, 1.0, 4.0]
    xlim1[1, 0, :] = [0.4, 0.01, 0.02, 5.0e-5, 2.0e-5, 1.0e-4]
    xlim1[1, 1, :] = [0.4, 0.01, 0.02, 5.0e-5, 2.0e-5, 1.0e-4]
    xlim1[1, 2, :] = [0.2, 5.0e-3, 0.02, 5.0e-5, 1.0e-5, 1.0e-4]
    for ip, p in enumerate(periods):
      for ig, g in enumerate(gl):
        print(p, g)

        contr_emis, contr_chem, contr_turb, contr_depo = \
          calculate_relative_contribution( \
          [Q['emis']['prof'][p][g], Q['chem']['prof'][p][g], Q['turb']['prof'][p][g], Q['depo']['prof'][p][g]] )

        ##### Q/C
        # ax[col].plot(QCemis_prof['all'][g]*1.0e3, zz, color=ct['emis'], lw=2, label='Qemis')
        # ax[col].plot(QCchem_prof['all'][g]*1.0e3, zz, color=ct['chem'], lw=2, label='Qchem')
        # ax[col].plot(QCturb_prof['all'][g]*1.0e3, zz, color=ct['turb'], lw=2, label='Qturb')
        # ax[col].plot(QCdepo_prof['all'][g]*1.0e3, zz, color=ct['depo'], lw=2, label='Qdepo')
        # ax[col].plot(Q['emis']['prof']['all'][g]*ddz/18.0, zz, color=ct['emis'], lw=2, label='Qemis')
        # ax[col].plot(Q['chem']['prof']['all'][g]*ddz/18.0, zz, color=ct['chem'], lw=2, label='Qchem')
        # ax[col].plot(Q['turb']['prof']['all'][g]*ddz/18.0, zz, color=ct['turb'], lw=2, label='Qturb')
        # ax[col].plot(Q['depo']['prof']['all'][g]*ddz/18.0, zz, color=ct['depo'], lw=2, label='Qdepo')
        l = 20
        for t in terms:
          # Use h-1 for the Q/C unit
          factor = 3.6e3  # 1 hour

          # Set a Qplot to save the Q values for plotting
          # Qplot = Q[t]['prof'][p][g]/dt*ddz/(mc['cnp'][p][g]*hc)
          # Qplot = Q[t]['prof'][p][g]/dt/mc['prof'][p][g]
          Qplot = Q[t]['prof'][p][g]/dt * ddz/hc * factor

          # Divide bottom level by 10
          # if g in ['PINIC', 'BCSOZOH', 'ISOP34NO3', 'ISOP34OOH']:
          #   Qplot[1] = Qplot[1] / 10.0
          Qplot[1] = Qplot[1] / 10.0

          # Plot from level 1 instead of level 0 which is at the surface
          ax[ip, ig].plot(Qplot[1:l], zz[1:l]/hc, color=ct[t], lw=2, label='Q'+t)

        # ylim = ax[row, col].get_ylim()
        # ylim_max = max(abs(ylim[0]), ylim[1])
        # ax[row, col].set_ylim(-ylim_max, ylim_max)

        # Set xlim and ylim
        # xlim1 = ax[col].get_xlim()
        # Use the values in xticks automatically
        # xlim1 = ax[ip, ig].get_xticks()[[0,-1]]
        # xlim1_max = max(abs(xlim1[0]), xlim1[1])
        # ax[ip, ig].set_xlim(-xlim1_max, xlim1_max)
        # Use manually-set values
        xlim1_max = xlim1[il, ip, ig]
        ax[ip, ig].set_ylim(0, 1.2)  # 

        # Set xticks and yticks
        ax[ip, ig].set_xticks([-xlim1_max, 0, xlim1_max])
        ax[ip, ig].set_yticks(np.arange(0, 25/hc, 0.1))

        # Rotate the xticklabels
        plt.setp(ax[ip, ig].get_xticklabels(), rotation=90, ha='right')

        # Use scientific notation for xticklabels when xlim1_max is very small
        if xlim1_max < 0.01:
          sf = ScalarFormatter(useOffset=False, useMathText=True)
          # sf.set_orderOfMagnitude(-3)
          ax[ip, ig].xaxis.set_major_formatter(sf)
          # print(g, sf.get_useOffset(), sf.orderOfMagnitude)
          # print(g, ax[col].get_xticklabels())
          # sf = ax[col].xaxis.get_major_formatter()
          # print(g, sf, sf.offset, sf.orderOfMagnitude)
          ax[ip, ig].ticklabel_format(axis='x', style='sci', scilimits=(-2, 1))

        # Wrtie Qplot[1] value on the plot
        if g == 'BCSOZOH' or g == 'ISOP34NO3':
          Qtext_depo = Q['depo']['prof'][p][g][1]/dt*ddz[1]/hc*factor * 1.0e5
          Qtext_turb = Q['turb']['prof'][p][g][1]/dt*ddz[1]/hc*factor * 1.0e5
        elif g == 'ISOP34OOH':
          Qtext_depo = Q['depo']['prof'][p][g][1]/dt*ddz[1]/hc*factor * 1.0e4
          Qtext_turb = Q['turb']['prof'][p][g][1]/dt*ddz[1]/hc*factor * 1.0e4
        else:
          Qtext_depo = Q['depo']['prof'][p][g][1]/dt*ddz[1]/hc*factor
          Qtext_turb = Q['turb']['prof'][p][g][1]/dt*ddz[1]/hc*factor
        ax[ip, ig].text(0.05, 0.05, '{0:5.2f}'.format(Qtext_depo), transform=ax[ip, ig].transAxes, fontsize=12)
        ax[ip, ig].text(0.60, 0.05, '{0:5.2f}'.format(Qtext_turb), transform=ax[ip, ig].transAxes, fontsize=12)

        ##### Q contribution
        ax1 = ax[ip, ig].twiny()
        axes_bar_stack(ax1, zz[1:]/hc, \
          [contr_emis[1:], contr_chem[1:], contr_turb[1:], contr_depo[1:]], \
          ddz_new[1:]/hc, [ct['emis'], ct['chem'], ct['turb'], ct['depo']], 2)
        ax1.set_xlim(-1, 1)
        ax1.set_ylim(0, 1.2)  # 0 --> 1.2*hc
        ax1.set_xticks(np.arange(-1, 1.1, 0.5))
        plt.setp(ax1.get_xticklabels(), rotation=90)
        # ax1.set_yticks(np.arange(-1, 1.1, 0.2))

        # Set xlabel for the first subplot
        if ip == nrow-1 and ig == 0:
          ax1.set_xlabel(r'$\overline{Q}^{\Delta}_{rel,n}$')

        ## Set subtitles
        ax[ip, ig].set_title(st_list[ip*6+ig] + ' ' + nl[ig], fontsize=14, y=1.20)
        ## Show the legends
        # handles, labels = ax[row][col].get_legend_handles_labels()
        # lgd = ax[row][col].legend(handles, labels, scatterpoints=1, bbox_to_anchor=(1, 1), loc='upper left', ncol=1)

      # Remove yticklabels for subplots except the leftmost one
      for i in range(ncol):
        if i != 0:
          labels = [item.get_text() for item in ax[ip, ig].get_yticklabels()]

          empty_string_labels = ['']*len(labels)
          ax[ip, i].set_yticklabels(empty_string_labels)

    # Set xlabel and ylabel for the leftmost subplot
    # ax[0].set_xlabel(r'Q/C [10$^{-3}$ s$^{-1}$]')
    ax[nrow-1, 0].set_xlabel(r'$\overline{Q}^{\Delta}_n$ [$\mu$g m$^{-3}$ h$^{-1}$]')
    ax[nrow-1, 0].set_ylabel(r'Height/$h_c$')
    # ax[0].xaxis.set_major_formatter(FormatStrFormatter('%5.0f'))

    # Use tight layout
    # fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['file_name'] + '-emis'), dpi=prop['dpi'], \
    #   bbox_extra_artists=(lgd,), bbox_inches='tight')
    fg.tight_layout()
    fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['file_name']+suf[il]), dpi=prop['dpi'], bbox_inches='tight')


def plot_QC_profile_all_in_one(sosa, prop):
  print('Plotting profiles of Q/C in one figure ...')

  Q, nc, mc, ind = get_Q_values(sosa)

  #********************************************#
  # Set parameters
  #********************************************#
  ##### gas list
  emis_gas_list   = [ 'MT', 'C5H8+MBO', 'SQT', 'CH3CHO', 'CH3OH', 'CH3COCH3' ]
  noemis_gas_list = [ 'HCHO', 'ACETOL', 'PINIC', 'BCSOZOH', 'ISOP34NO3', 'ISOP34OOH' ]

  ##### Other variables
  dt  = 1800.0  # [s], output time interval
  ddz = sosa['grid']['ddz']
  hc = np.sum(ddz)
  ncz = sosa['grid']['ncz']
  zz  = sosa['grid']['zz']
  ddz_new = np.copy(ddz)
  ddz_new[1] -= 0.5*zz[1]
  time = sosa['time']['raw']
  mtime = time - datetime.timedelta(minutes=15)  # put time point 15 minutes ahead to represent the concentration change in the middle time interval

  ##### Q related values
  terms = ['emis', 'turb', 'depo', 'chem']
  periods = ['all', 'day', 'nit']

  ##### Test for debug
  dtime, Qdepo_davg, Qdepo_dstd = ppf.diurnal_average(mtime, Q['depo']['dct']['PINIC'])
  dtime, mc_davg, mc_dstd = ppf.diurnal_average(mtime, mc['cnp']['raw']['PINIC'])
  print(Qdepo_davg/mc_davg)
  print(np.nanmean(Q['depo']['dct']['PINIC']) / mc['cnp']['all']['PINIC'])
  print(np.sum(Q['depo']['prof']['all']['PINIC']/dt*ddz/(mc['cnp']['all']['PINIC']*hc)))

  # sys.exit()

  #********************************************#
  # Plot
  #********************************************#
  # ct = {'emis': '#a6d854', 'chem': '#fb8072', 'turb': '#80b1d3', 'depo': '#bebada', 'conc': '#1d1d1d'}  # light
  ct = {'emis': '#4daf4a', 'chem': '#e41a1c', 'turb': '#377eb8', 'depo': '#984ea3', 'conc': '#1d1d1d'}  # light
  st_list = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)']
  suf = ['-emis', '-noemis']

  #***** Profiles of Q/C for whole day, daytime and nighttime *****#
  nrow = 1
  ncol = 6

  for p in periods:
    for il, gl in enumerate([emis_gas_list, noemis_gas_list]):
      fg, ax = plt.subplots(nrow, ncol, figsize=(14, 8), dpi=prop['dpi'])

      for ig, g in enumerate(gl):
        print(p, g)

        row = ig // ncol
        col = ig % ncol

        contr_emis, contr_chem, contr_turb, contr_depo = \
          calculate_relative_contribution( \
          [Q['emis']['prof'][p][g], Q['chem']['prof'][p][g], Q['turb']['prof'][p][g], Q['depo']['prof'][p][g]] )

        ##### Q/C
        # ax[col].plot(QCemis_prof['all'][g]*1.0e3, zz, color=ct['emis'], lw=2, label='Qemis')
        # ax[col].plot(QCchem_prof['all'][g]*1.0e3, zz, color=ct['chem'], lw=2, label='Qchem')
        # ax[col].plot(QCturb_prof['all'][g]*1.0e3, zz, color=ct['turb'], lw=2, label='Qturb')
        # ax[col].plot(QCdepo_prof['all'][g]*1.0e3, zz, color=ct['depo'], lw=2, label='Qdepo')
        # ax[col].plot(Q['emis']['prof']['all'][g]*ddz/18.0, zz, color=ct['emis'], lw=2, label='Qemis')
        # ax[col].plot(Q['chem']['prof']['all'][g]*ddz/18.0, zz, color=ct['chem'], lw=2, label='Qchem')
        # ax[col].plot(Q['turb']['prof']['all'][g]*ddz/18.0, zz, color=ct['turb'], lw=2, label='Qturb')
        # ax[col].plot(Q['depo']['prof']['all'][g]*ddz/18.0, zz, color=ct['depo'], lw=2, label='Qdepo')
        l = 20
        for t in terms:
          # Set a Qplot to save the Q values for plotting
          # Qplot = Q[t]['prof'][p][g]/dt*ddz/(mc['cnp'][p][g]*hc)
          Qplot = Q[t]['prof'][p][g]/dt/mc['prof'][p][g]

          # Divide bottom level by 10
          # if g in ['PINIC', 'BCSOZOH', 'ISOP34NO3', 'ISOP34OOH']:
          #   Qplot[1] = Qplot[1] / 10.0
          Qplot[1] = Qplot[1] / 10.0

          # Use h-1 for the Q/C unit
          factor = 3.6e3  # 1 hour

          # Plot from level 1 instead of level 0 which is at the surface
          ax[col].plot(Qplot[1:l]*factor, zz[1:l], color=ct[t], lw=2, label='Q'+t)

        # ylim = ax[row, col].get_ylim()
        # ylim_max = max(abs(ylim[0]), ylim[1])
        # ax[row, col].set_ylim(-ylim_max, ylim_max)

        ax[col].set_ylim(0, 25)
        xlim1 = ax[col].get_xlim()
        xlim1_max = max(abs(xlim1[0]), xlim1[1])
        ax[col].set_xlim(-xlim1_max, xlim1_max)
        plt.setp(ax[col].get_xticklabels(), rotation=90)

        # Wrtie Qplot[1] value on the plot
        ax[col].text(0.05, 0.05, '{0:5.2f}'.format(Q['depo']['prof'][p][g][1]/dt/mc['prof'][p][g][1]*factor), transform=ax[col].transAxes, fontsize=12)
        ax[col].text(0.60, 0.05, '{0:5.2f}'.format(Q['turb']['prof'][p][g][1]/dt/mc['prof'][p][g][1]*factor), transform=ax[col].transAxes, fontsize=12)

        ##### Q contribution
        ax1 = ax[col].twiny()
        axes_bar_stack(ax1, zz[1:], \
          [contr_emis[1:], contr_chem[1:], contr_turb[1:], contr_depo[1:]], \
          ddz_new[1:], [ct['emis'], ct['chem'], ct['turb'], ct['depo']], 2)
        ax1.set_xlim(-1, 1)
        ax1.set_ylim(0, 21)
        ax1.set_xticks(np.arange(-1, 1.1, 0.5))
        plt.setp(ax1.get_xticklabels(), rotation=90)
        # ax1.set_yticks(np.arange(-1, 1.1, 0.2))

        # Set xlabel for the first subplot
        if row == 0 and col == 0:
          ax1.set_xlabel('Q contribution')

        ## Set subtitles
        ax[col].set_title(st_list[ig] + ' ' + g, fontsize=14, y=1.20)
        ## Show the legends
        # handles, labels = ax[row][col].get_legend_handles_labels()
        # lgd = ax[row][col].legend(handles, labels, scatterpoints=1, bbox_to_anchor=(1, 1), loc='upper left', ncol=1)

      # Remove yticklabels for subplots except the leftmost one
      for i in range(ncol):
        if i != 0:
          labels = [item.get_text() for item in ax[i].get_yticklabels()]

          empty_string_labels = ['']*len(labels)
          ax[i].set_yticklabels(empty_string_labels)

      # Set xlabel and ylabel for the leftmost subplot
      # ax[0].set_xlabel(r'Q/C [10$^{-3}$ s$^{-1}$]')
      ax[0].set_xlabel(r'Q/C [h$^{-1}$]')
      ax[0].set_ylabel('Height [m]')
      ax[0].xaxis.set_major_formatter(FormatStrFormatter('%5.0f'))

      # Use tight layout
      # fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['file_name'] + '-emis'), dpi=prop['dpi'], \
      #   bbox_extra_artists=(lgd,), bbox_inches='tight')
      fg.tight_layout()
      fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['file_name']+suf[il]+'-'+p), dpi=prop['dpi'], bbox_inches='tight')


def plot_Q_diurnal_pattern_all_in_one(sosa, prop):
  print('Plotting diurnal patterns of Q in one figure ...')

  #********************************************#
  # Set parameters
  #********************************************#
  # gas list
  emis_gas_list   = [ 'MT', 'C5H8+MBO', 'SQT', 'CH3CHO', 'CH3OH', 'CH3COCH3' ]
  emis_name_list   = [ 'monoterpenes', 'isoprene+MBO', 'sesquiterpenes', 'acetaldehyde', 'methanol', 'acetone' ]
  noemis_gas_list = [ 'HCHO', 'ACETOL', 'PINIC', 'BCSOZOH', 'ISOP34NO3', 'ISOP34OOH' ]
  noemis_name_list = [ 'formaldehyde', 'acetol', 'pinic acid', 'BCSOZOH', 'ISOP34NO3', 'ISOP34OOH' ]

  # Get Q values
  Q, nc, mc, ind = get_Q_values(sosa)

  # Initialize davg and dstd Q values
  Qemis_davg = {}
  Qchem_davg = {}
  Qturb_davg = {}
  Qdepo_davg = {}
  Qemis_dstd = {}
  Qchem_dstd = {}
  Qturb_dstd = {}
  Qdepo_dstd = {}

  # Print some key values
  ind_day = np.squeeze( sosa['d_ind']['raw'] )
  ind_nit = np.squeeze( sosa['n_ind']['raw'] )
  ind_day[0] = False
  ind_nit[0] = False

  # Get diurnal average
  ncz = sosa['grid']['ncz']
  time = sosa['time']['raw']
  mtime = time - datetime.timedelta(minutes=15)  # put time point 15 minutes ahead to represent the concentration change in the middle time interval
  factor = 3600.0  # s --> h
  for ig, g in enumerate(emis_gas_list + noemis_gas_list):
    dtime, Qemis_davg[g], Qemis_dstd[g] = ppf.diurnal_average(mtime[1:], Q['emis']['dct'][g][1:]*factor)
    dtime, Qchem_davg[g], Qchem_dstd[g] = ppf.diurnal_average(mtime[1:], Q['chem']['dct'][g][1:]*factor)
    dtime, Qturb_davg[g], Qturb_dstd[g] = ppf.diurnal_average(mtime[1:], Q['turb']['dct'][g][1:]*factor)
    dtime, Qdepo_davg[g], Qdepo_dstd[g] = ppf.diurnal_average(mtime[1:], Q['depo']['dct'][g][1:]*factor)

    print(g)
    for t in ['emis', 'chem', 'turb', 'depo']:
      nm = np.nanmean(Q[t]['dct'][g][ind_nit]*factor)
      dm = np.nanmean(Q[t]['dct'][g][ind_day]*factor)
      am = np.nanmean(Q[t]['dct'][g][1:]*factor)
      nm_dm = nm/dm
      print('Qdct, ', t, ', nit, day, all, nit/day : ', nm, dm, am, nm_dm)

  #********************************************#
  # Plot
  #********************************************#
  # Set parameters
  ct = {'emis': '#a6d854', 'chem': '#fb8072', 'turb': '#80b1d3', 'depo': '#bebada', 'conc': '#1d1d1d'}  # light
  st_list = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)']
  suf = ['-emis', '-noemis']
  # suf = ['-emis_QC', '-noemis_QC']

  #***** Diurnal pattern of Q/C for emission and non-emission compounds *****#
  nrow = 2
  ncol = 3

  for il, gn in enumerate([(emis_gas_list, emis_name_list), (noemis_gas_list, noemis_name_list)]):
    fg, ax = plt.subplots(nrow, ncol, figsize=(14, 8), dpi=prop['dpi'])
    gl, nl = gn

    for ig, g in enumerate(gl):
      row = ig // ncol
      col = ig % ncol

      contr_emis, contr_chem, contr_turb, contr_depo = calculate_relative_contribution([Qemis_davg[g], Qchem_davg[g], Qturb_davg[g], Qdepo_davg[g]])

      # Q
      ax[row, col].plot(dtime, Qemis_davg[g], color=ct['emis'], lw=2, label='Qemis')
      ax[row, col].plot(dtime, Qchem_davg[g], color=ct['chem'], lw=2, label='Qchem')
      ax[row, col].plot(dtime, Qturb_davg[g], color=ct['turb'], lw=2, label='Qturb')
      ax[row, col].plot(dtime, Qdepo_davg[g], color=ct['depo'], lw=2, label='Qdepo')

      # ax[row, col].plot(dtime, QCemis_davg[g]*1.0e3, color=ct['emis'], lw=2, label='Qemis')
      # ax[row, col].plot(dtime, QCchem_davg[g]*1.0e3, color=ct['chem'], lw=2, label='Qchem')
      # ax[row, col].plot(dtime, QCturb_davg[g]*1.0e3, color=ct['turb'], lw=2, label='Qturb')
      # ax[row, col].plot(dtime, QCdepo_davg[g]*1.0e3, color=ct['depo'], lw=2, label='Qdepo')

      ylim = ax[row, col].get_ylim()
      ylim_max = max(abs(ylim[0]), ylim[1])
      ax[row, col].set_ylim(-ylim_max, ylim_max)

      ax[row, col].set_title(st_list[ig] + ' ' + nl[ig], fontsize=14, x=0.6)

      if ylim_max < 0.1:
        # ax[row, col].yaxis.set_major_formatter(FormatStrFormatter('%.2e'))
        ax[row, col].yaxis.set_major_formatter(ScalarFormatter(useOffset=True, useMathText=True))
        # ax[row, col].set_title(st_list[ig] + ' ' + g, fontsize=14, y=1.2)
        ax[row, col].ticklabel_format(axis='y', style='sci', scilimits=(-1, 1))

      # Q contribution
      ax1 = ax[row, col].twinx()
      axes_bar_stack(ax1, dtime, [contr_emis, contr_chem, contr_turb, contr_depo], 1.0, [ct['emis'], ct['chem'], ct['turb'], ct['depo']])
      ax1.set_xlim(0, 24)
      ax1.set_ylim(-1, 1)
      ax1.set_xticks(range(0, 25, 4))
      ax1.set_yticks(np.arange(-1, 1.1, 0.2))
      if row == 1 and col == 0:
        ax1.set_ylabel(r'$\overline{Q}^{\Delta,h_c}_{rel,n}$')

      ## Show the legends
      # handles, labels = ax[row][col].get_legend_handles_labels()
      # lgd = ax[row][col].legend(handles, labels, scatterpoints=1, bbox_to_anchor=(1, 1), loc='upper left', ncol=1)

    ax[1, 0].set_xlabel('Time [h]')
    ax[1, 0].set_ylabel(r'$\overline{Q}^{\Delta,h_c}_n$ [$\mu$g m$^{-3}$ h$^{-1}$]')

    fg.tight_layout()
    fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['file_name'] + suf[il]), dpi=prop['dpi'], bbox_inches='tight')

  return


def plot_QC_diurnal_pattern_all_in_one(sosa, prop):
  print('Plotting diurnal patterns of Q/C in one figure ...')

  #********************************************#
  # Set parameters
  #********************************************#
  ##### gas list
  emis_gas_list   = [ 'MT', 'C5H8+MBO', 'SQT', 'CH3CHO', 'CH3OH', 'CH3COCH3' ]
  noemis_gas_list = [ 'HCHO', 'ACETOL', 'PINIC', 'BCSOZOH', 'ISOP34NO3', 'ISOP34OOH' ]

  ##### integrated Q within in the canopy [ug m-3 s-1]
  ##### average concentration withing the canopy [ug m-3] 
  Qemis = {}
  Qturb = {}
  Qdepo = {}
  Qchem = {}

  Qemis_davg = {}
  Qturb_davg = {}
  Qdepo_davg = {}
  Qchem_davg = {}

  Qemis_dstd = {}
  Qturb_dstd = {}
  Qdepo_dstd = {}
  Qchem_dstd = {}

  conc = {}
  conc_mid = {}
  conc_cnp = {}
  conc_davg = {}
  conc_dstd = {}

  QCemis = {}
  QCturb = {}
  QCdepo = {}
  QCchem = {}

  QCemis_davg = {}
  QCturb_davg = {}
  QCdepo_davg = {}
  QCchem_davg = {}

  QCemis_dstd = {}
  QCturb_dstd = {}
  QCdepo_dstd = {}
  QCchem_dstd = {}

  ncz = sosa['grid']['ncz']
  time = sosa['time']['raw']
  mtime = time - datetime.timedelta(minutes=15)  # put time point 15 minutes ahead to represent the concentration change in the middle time interval
  for ig, g in enumerate(emis_gas_list + noemis_gas_list):
    if g == 'C5H8+MBO':
      unit_conv_C5H8 = ppf.moleccm3_to_ugm3(ppp.molar_mass['C5H8']*1.0e-3)
      unit_conv_MBO = ppf.moleccm3_to_ugm3(ppp.molar_mass['MBO']*1.0e-3)
      Qemis[g] = sosa['C5H8']['Qemis_dct']['raw'] * unit_conv_C5H8 + sosa['MBO']['Qemis_dct']['raw'] * unit_conv_MBO
      Qturb[g] = sosa['C5H8']['Qturb_dct']['raw'] * unit_conv_C5H8 + sosa['MBO']['Qturb_dct']['raw'] * unit_conv_MBO
      Qdepo[g] = sosa['C5H8']['Qdepo_dct']['raw'] * unit_conv_C5H8 + sosa['MBO']['Qdepo_dct']['raw'] * unit_conv_MBO
      Qchem[g] = sosa['C5H8']['Qchem_dct']['raw'] * unit_conv_C5H8 + sosa['MBO']['Qchem_dct']['raw'] * unit_conv_MBO

      conc[g]  = sosa['C5H8']['conc']['raw']      * unit_conv_C5H8 + sosa['MBO']['conc']['raw']      * unit_conv_MBO
      conc_mid[g] = np.copy(conc[g])
      conc_mid[g][1:-1, :] = 0.5 * ( conc[g][0:-2, :] + conc[g][2:, :] )  # average value during previous 30 mins
      conc_cnp[g] = np.nanmean(conc_mid[g][:, 1:ncz+1], axis=1)  # average concentration within the canopy

    else:
      unit_conv = ppf.moleccm3_to_ugm3(ppp.molar_mass[g]*1.0e-3)
      Qemis[g] = sosa[g]['Qemis_dct']['raw'] * unit_conv  # [molec cm-3 s-1] --> [ug m-3 s-1], the average concentration change within the canopy during previous 30 mins.
      Qturb[g] = sosa[g]['Qturb_dct']['raw'] * unit_conv
      Qdepo[g] = sosa[g]['Qdepo_dct']['raw'] * unit_conv
      Qchem[g] = sosa[g]['Qchem_dct']['raw'] * unit_conv

      conc[g]  = sosa[g]['conc']['raw']      * unit_conv
      conc_mid[g] = np.copy(conc[g])
      conc_mid[g][1:-1, :] = 0.5 * ( conc[g][0:-2, :] + conc[g][2:, :] )  # average value during previous 30 mins
      conc_cnp[g] = np.nanmean(conc_mid[g][:, 1:ncz+1], axis=1)  # average concentration within the canopy

    ##### The first value for Q does not have meaning
    Qemis[g][0] = np.nan
    Qturb[g][0] = np.nan
    Qdepo[g][0] = np.nan
    Qchem[g][0] = np.nan
    conc_cnp[g][0] = np.nan

    QCemis[g] = Qemis[g] / conc_cnp[g]
    QCturb[g] = Qturb[g] / conc_cnp[g]
    QCdepo[g] = Qdepo[g] / conc_cnp[g]
    QCchem[g] = Qchem[g] / conc_cnp[g]
    # QCemis[g][0] = np.nan
    # QCturb[g][0] = np.nan
    # QCdepo[g][0] = np.nan
    # QCchem[g][0] = np.nan

    dtime, Qemis_davg[g], Qemis_dstd[g] = ppf.diurnal_average(mtime, Qemis[g])
    dtime, Qchem_davg[g], Qchem_dstd[g] = ppf.diurnal_average(mtime, Qchem[g])
    dtime, Qturb_davg[g], Qturb_dstd[g] = ppf.diurnal_average(mtime, Qturb[g])
    dtime, Qdepo_davg[g], Qdepo_dstd[g] = ppf.diurnal_average(mtime, Qdepo[g])

    dtime, conc_davg[g] , conc_dstd[g]  = ppf.diurnal_average(mtime, conc_cnp[g])

    dtime, QCemis_davg[g], QCemis_dstd[g] = ppf.diurnal_average(mtime, QCemis[g])
    dtime, QCchem_davg[g], QCchem_dstd[g] = ppf.diurnal_average(mtime, QCchem[g])
    dtime, QCturb_davg[g], QCturb_dstd[g] = ppf.diurnal_average(mtime, QCturb[g])
    dtime, QCdepo_davg[g], QCdepo_dstd[g] = ppf.diurnal_average(mtime, QCdepo[g])

  #********************************************#
  # Plot
  #********************************************#
  ct = {'emis': '#a6d854', 'chem': '#fb8072', 'turb': '#80b1d3', 'depo': '#bebada', 'conc': '#1d1d1d'}  # light
  st_list = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)']
  suf = ['-emis', '-noemis']
  # suf = ['-emis_QC', '-noemis_QC']

  #***** Compare QC and Q/C for test
  fg, ax = plt.subplots(3, 1, figsize=(14, 8), dpi=prop['dpi'])

  ax[0].plot(mtime, QCemis['CH3COCH3'], 'r')
  ax[1].plot(mtime, Qemis['CH3COCH3'], 'r')
  ax[2].plot(mtime, conc_cnp['CH3COCH3'], 'r')

  fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['file_name'] + '-QC_CH3COCH3'), dpi=prop['dpi'], bbox_inches='tight')

  #***** Diurnal pattern of Q/C for emission and non-emission compounds *****#

  nrow = 2
  ncol = 3

  for il, gl in enumerate([emis_gas_list, noemis_gas_list]):
    fg, ax = plt.subplots(nrow, ncol, figsize=(14, 8), dpi=prop['dpi'])

    for ig, g in enumerate(gl):
      row = ig // ncol
      col = ig % ncol

      contr_emis, contr_chem, contr_turb, contr_depo = calculate_relative_contribution([Qemis_davg[g], Qchem_davg[g], Qturb_davg[g], Qdepo_davg[g]])

      ##### Q/C
      factor = 3.6e3  # 1 hour
      ax[row, col].plot(dtime, Qemis_davg[g]/conc_davg[g]*factor, color=ct['emis'], lw=2, label='Qemis')
      ax[row, col].plot(dtime, Qchem_davg[g]/conc_davg[g]*factor, color=ct['chem'], lw=2, label='Qchem')
      ax[row, col].plot(dtime, Qturb_davg[g]/conc_davg[g]*factor, color=ct['turb'], lw=2, label='Qturb')
      ax[row, col].plot(dtime, Qdepo_davg[g]/conc_davg[g]*factor, color=ct['depo'], lw=2, label='Qdepo')

      # ax[row, col].plot(dtime, QCemis_davg[g]*1.0e3, color=ct['emis'], lw=2, label='Qemis')
      # ax[row, col].plot(dtime, QCchem_davg[g]*1.0e3, color=ct['chem'], lw=2, label='Qchem')
      # ax[row, col].plot(dtime, QCturb_davg[g]*1.0e3, color=ct['turb'], lw=2, label='Qturb')
      # ax[row, col].plot(dtime, QCdepo_davg[g]*1.0e3, color=ct['depo'], lw=2, label='Qdepo')

      ylim = ax[row, col].get_ylim()
      ylim_max = max(abs(ylim[0]), ylim[1])
      ax[row, col].set_ylim(-ylim_max, ylim_max)

      ax[row, col].set_title(st_list[ig] + ' ' + g, fontsize=14)

      ##### Q contribution
      ax1 = ax[row, col].twinx()
      axes_bar_stack(ax1, dtime, [contr_emis, contr_chem, contr_turb, contr_depo], 1.0, [ct['emis'], ct['chem'], ct['turb'], ct['depo']])
      ax1.set_xlim(0, 24)
      ax1.set_ylim(-1, 1)
      ax1.set_xticks(range(0, 25, 4))
      ax1.set_yticks(np.arange(-1, 1.1, 0.2))
      if row == 1 and col == 0:
        ax1.set_ylabel('Q contribution')

      ## Show the legends
      # handles, labels = ax[row][col].get_legend_handles_labels()
      # lgd = ax[row][col].legend(handles, labels, scatterpoints=1, bbox_to_anchor=(1, 1), loc='upper left', ncol=1)

    ax[1, 0].set_xlabel('Time [h]')
    # ax[1, 0].set_ylabel(r'Q/C [10$^{-3}$ s$^{-1}$]')
    ax[1, 0].set_ylabel(r'Q/C [h$^{-1}$]')

    # fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['file_name'] + '-emis'), dpi=prop['dpi'], \
    #   bbox_extra_artists=(lgd,), bbox_inches='tight')
    fg.tight_layout()
    fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['file_name'] + suf[il]), dpi=prop['dpi'], bbox_inches='tight')



def plot_sosa_Q_monthly_and_diurnal(sosa, prop):
  print('Plotting Q of monthly time series and diurnal patterns ...')

  #********************************************#
  # Set parameters
  #********************************************#
  ## gas list
  gas_list_reorder = [ \
    'ACETOL', 'CH3CHO', 'CH3CO2H', 'CH3CO3H', 'CH3COCH3', \
    'CH3OH', 'HCHO', 'HCOOH', 'GLYOX', \
    'C2H4', 'PINONIC', 'PINIC', 'BCSOZOH', 'C151OH', \
    'H2O2', 'HNO3', \
    'APINENE', 'BPINENE', 'C5H8', 'MBO', 'OMT', \
    'LIMONENE', 'Carene', \
    'BCARY', 'OSQ', \
    'ISOP34NO3', 'ISOP34OOH', 'O3', 'OH', \
    ]

  # gas_list_reorder = [ \
  #   'ACETOL', 'CH3CHO', 'CH3CO2H', 'CH3CO3H', 'CH3COCH3']

  dt  = 1800.0  # [s], output time interval
  ddz = sosa['grid']['ddz']
  ncz = sosa['grid']['ncz']
  zz  = sosa['grid']['zz']

  ##### time
  time = sosa['time']['raw']

  ## color set
  ct = {'emis': '#a6d854', 'chem': '#fb8072', 'turb': '#80b1d3', 'depo': '#bebada', 'conc': '#1d1d1d'}  # light

  #********************************************#
  # Start looping in different gases
  #********************************************#
  for ig, g in enumerate(gas_list_reorder):
    print(g)

    #***** Read variables from datapack and calculate necessary variables *****#
    ##### Concentration
    unit_conv = ppf.moleccm3_to_ugm3(ppp.molar_mass[g]*1.0e-3)
    conc  = sosa[g]['conc']['raw'] * unit_conv  # [ug m-3]
    conc_mid = np.copy(conc)
    conc_mid[1:-1, :] = 0.5 * ( conc[0:-2, :] + conc[2:, :] )  # average value during previous 30 mins
    conc_cnp = np.nanmean(conc_mid[:, 1:ncz+1], axis=1)

    ##### Q
    unit_conv = ppf.moleccm3_to_ugm3(ppp.molar_mass[g]*1.0e-3) / 1800.0
    Qemis = sosa[g]['Qemis']['raw'] * unit_conv  # [molec cm-3] --> [ug m-3 s-1], the everage concentration change during previous 30 mins.
    Qchem = sosa[g]['Qchem']['raw'] * unit_conv
    Qturb = sosa[g]['Qturb']['raw'] * unit_conv
    Qdepo = sosa[g]['Qdepo']['raw'] * unit_conv
    Qconc = sosa[g]['Qconc']['raw'] * unit_conv

    ##### Q_dct
    unit_conv = ppf.moleccm3_to_ugm3(ppp.molar_mass[g]*1.0e-3)
    Qemis_dct = sosa[g]['Qemis_dct']['raw'] * unit_conv  # [molec cm-3 s-1] --> [ug m-3 s-1], the everage concentration change within the canopy during previous 30 mins.
    Qchem_dct = sosa[g]['Qchem_dct']['raw'] * unit_conv
    Qturb_dct = sosa[g]['Qturb_dct']['raw'] * unit_conv
    Qdepo_dct = sosa[g]['Qdepo_dct']['raw'] * unit_conv
    Qconc_dct = sosa[g]['Qconc_dct']['raw'] * unit_conv
    Qturbnow_dct = sosa[g]['Qturbnow_dct']['raw'] * unit_conv

    ##### Q_int
    unit_conv = ppf.moleccm2s1_to_ugm2s1(ppp.molar_mass[g]*1.0e-3) * 1.0e-4
    Qemis_int = sosa[g]['Qemis_int']['raw'] * unit_conv  # [molec m-2 s-1] --> [ug m-2 s-1], the everage flux into (pos) or out of (neg) canopy during previous 30 mins.
    Qchem_int = sosa[g]['Qchem_int']['raw'] * unit_conv
    Qturb_int = sosa[g]['Qturb_int']['raw'] * unit_conv
    Qdepo_int = sosa[g]['Qdepo_int']['raw'] * unit_conv
    Qconc_int = sosa[g]['Qconc_int']['raw'] * unit_conv
    Qturbnow_int = sosa[g]['Qturbnow_int']['raw'] * unit_conv
    # Ccnp  = np.sum(conc[:, 1:ncz+1]*ddz[np.newaxis, 1:ncz+1], axis=1) / np.sum(ddz[1:ncz+1])
    # Fgas = - 0.5 * ( sosa[g]['flux']['raw'][:, ncz] + sosa[g]['flux']['raw'][:, ncz+1] ) * unit_conv * 1.0e4

    ##### Flux
    unit_conv = ppf.moleccm2s1_to_ugm2s1(ppp.molar_mass[g]*1.0e-3)
    Fgas = sosa[g]['flux']['raw'][:, ncz] * unit_conv  # [ug m-2 s-1], starting from 0: ncz(19): 19.96 m, 20: 23.60

    ##### Q prof
    ## The whole day
    Qemis_dct_prof_all = np.nanmean(Qemis, axis=0)
    Qchem_dct_prof_all = np.nanmean(Qchem, axis=0)
    Qturb_dct_prof_all = np.nanmean(Qturb, axis=0)
    Qdepo_dct_prof_all = np.nanmean(Qdepo, axis=0)
    Qconc_dct_prof_all = np.nanmean(Qconc, axis=0)
    conc_prof_all = np.nanmean(conc_mid, axis=0)

    ## Daytime
    d_ind = sosa['d_ind']['raw']
    Qemis_dct_prof_day = np.nanmean(Qemis[d_ind, :], axis=0)
    Qchem_dct_prof_day = np.nanmean(Qchem[d_ind, :], axis=0)
    Qturb_dct_prof_day = np.nanmean(Qturb[d_ind, :], axis=0)
    Qdepo_dct_prof_day = np.nanmean(Qdepo[d_ind, :], axis=0)
    conc_prof_day = np.nanmean(conc_mid[d_ind, :], axis=0)

    ## Nighttime
    n_ind = sosa['n_ind']['raw']
    Qemis_dct_prof_nit = np.nanmean(Qemis[n_ind, :], axis=0)
    Qchem_dct_prof_nit = np.nanmean(Qchem[n_ind, :], axis=0)
    Qturb_dct_prof_nit = np.nanmean(Qturb[n_ind, :], axis=0)
    Qdepo_dct_prof_nit = np.nanmean(Qdepo[n_ind, :], axis=0)
    conc_prof_nit = np.nanmean(conc_mid[n_ind, :], axis=0)

    ##### Diurnal average of integrated Q and flux
    mtime = time - datetime.timedelta(minutes=15)  # put time point 15 minutes ahead to represent the concentration change in the middle time interval

    dtime, Qemis_dct_davg, Qemis_dstd = ppf.diurnal_average(mtime, Qemis_dct)
    dtime, Qchem_dct_davg, Qchem_dstd = ppf.diurnal_average(mtime, Qchem_dct)
    dtime, Qturb_dct_davg, Qturb_dstd = ppf.diurnal_average(mtime, Qturb_dct)
    dtime, Qdepo_dct_davg, Qdepo_dstd = ppf.diurnal_average(mtime, Qdepo_dct)
    dtime, Qconc_dct_davg, Qconc_dstd = ppf.diurnal_average(mtime, Qconc_dct)
    dtime, Qturbnow_dct_davg , Qturbnow_dct_dstd  = ppf.diurnal_average(mtime, Qturbnow_dct)
    dtime, Fgas_davg , Fgas_dstd  = ppf.diurnal_average(mtime, Fgas)
    dtime, conc_cnp_davg, conc_cnp_dstd = ppf.diurnal_average(mtime, conc_cnp)

    #***** Plot figures *****#
    ##### Plot monthly
    fg, ax = plt.subplots(1, 1, figsize=prop['figsize'], dpi=prop['dpi'])

    ax.plot(time, Qemis, color=ct['emis'], label='Qemis')
    ax.plot(time, Qchem, color=ct['chem'], label='Qchem')
    ax.plot(time, Qturb, color=ct['turb'], label='Qturb')
    ax.plot(time, Qdepo, color=ct['depo'], label='Qdepo')
    ax.plot(time, Qconc, color=ct['conc'], label='Qconc')
    # ax.plot(time, Ccnp , color='k'       , label='Ccnp' )
    # ax.plot(time, Fgas , color='b'       , label='Fgas' )
    # ax.plot(time, Qturbnow, color='r', label='Qturbnow')

    ## Show the legends
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels, scatterpoints=1, loc='upper right', ncol=1)

    fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['file_name'] + 'monthly_' + g), dpi=prop['dpi'])

    ##### Plot diurnal
    contr_emis, contr_chem, contr_turb, contr_depo = calculate_relative_contribution([Qemis_dct_davg, Qchem_dct_davg, Qturb_dct_davg, Qdepo_dct_davg])

    fg, ax1 = plt.subplots(1, 1, figsize=prop['figsize'], dpi=prop['dpi'])

    ax1.plot(dtime, Qemis_dct_davg/conc_cnp_davg, color=ct['emis'], lw=2, label='Qemis')
    ax1.plot(dtime, Qchem_dct_davg/conc_cnp_davg, color=ct['chem'], lw=2, label='Qchem')
    ax1.plot(dtime, Qturb_dct_davg/conc_cnp_davg, color=ct['turb'], lw=2, label='Qturb')
    ax1.plot(dtime, Qdepo_dct_davg/conc_cnp_davg, color=ct['depo'], lw=2, label='Qdepo')
    # ax1.plot(dtime, Qconc_dct_davg, color=ct['conc'], lw=2, label='Qconc')
    # ax1.plot(dtime, Fgas_davg , color='b'       , lw=2, label='Fgas')
    # ax1.plot(dtime, Qturbnow_davg, color='r', lw=2, label='Qturbnow')

    ylim1 = ax1.get_ylim()
    ylim1_max = max(abs(ylim1[0]), ylim1[1])
    ax1.set_ylim(-ylim1_max, ylim1_max)

    ax2 = ax1.twinx()
    axes_bar_stack(ax2, dtime, [contr_emis, contr_chem, contr_turb, contr_depo], 1.0, [ct['emis'], ct['chem'], ct['turb'], ct['depo']])
    ax2.set_xlim(0, 24)
    ax2.set_ylim(-1, 1)

    # ax.plot(dtime, contr_emis, color=ct['emis'], lw=2, label='Qemis')
    # ax.plot(dtime, contr_chem, color=ct['chem'], lw=2, label='Qchem')
    # ax.plot(dtime, contr_turb, color=ct['turb'], lw=2, label='Qturb')
    # ax.plot(dtime, contr_depo, color=ct['depo'], lw=2, label='Qdepo')

    ## Show the legends
    handles, labels = ax1.get_legend_handles_labels()
    lgd = ax1.legend(handles, labels, scatterpoints=1, bbox_to_anchor=(1, 1), loc='upper left', ncol=1)

    fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['file_name'] + 'diurnal_' + g), dpi=prop['dpi'], \
      bbox_extra_artists=(lgd,), bbox_inches='tight')

    ##### Plot profiles of Q for the whole day
    contr_emis_prof_all, contr_chem_prof_all, contr_turb_prof_all, contr_depo_prof_all = \
      calculate_relative_contribution([Qemis_dct_prof_all, Qchem_dct_prof_all, Qturb_dct_prof_all, Qdepo_dct_prof_all])

    fg, ax1 = plt.subplots(1, 1, figsize=(6, 12), dpi=prop['dpi'])

    ax1.plot(Qemis_dct_prof_all/conc_prof_all, zz, color=ct['emis'], lw=2.5, label='Qemis')
    ax1.plot(Qchem_dct_prof_all/conc_prof_all, zz, color=ct['chem'], lw=2.5, label='Qchem')
    ax1.plot(Qturb_dct_prof_all/conc_prof_all, zz, color=ct['turb'], lw=2.5, label='Qturb')
    ax1.plot(Qdepo_dct_prof_all/conc_prof_all, zz, color=ct['depo'], lw=2.5, label='Qdepo')
    # ax1.plot(Qconc_dct_prof, zz, color=ct['conc'], lw=2, label='Qconc')
    # ax1.plot(Fgas_prof, zz, color='b', lw=2, label='Fgas')

    ax1.set_ylim(0, 25)
    xlim1 = ax1.get_xlim()
    xlim1_max = max(abs(xlim1[0]), xlim1[1])
    ax1.set_xlim(-xlim1_max, xlim1_max)

    ax2 = ax1.twiny()
    ddz_new = np.copy(ddz)
    ddz_new[1] -= 0.5*zz[1]
    axes_bar_stack(ax2, zz[1:], \
      [contr_emis_prof_all[1:], contr_chem_prof_all[1:], contr_turb_prof_all[1:], contr_depo_prof_all[1:]], \
      ddz_new[1:], [ct['emis'], ct['chem'], ct['turb'], ct['depo']], 2)
    ax2.set_xlim(-1, 1)
    ax2.set_ylim(0, 25)

    handles, labels = ax1.get_legend_handles_labels()
    lgd = ax1.legend(handles, labels, scatterpoints=1, bbox_to_anchor=(1, 1), loc='upper left', ncol=1)

    fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['file_name'] + 'prof_all_' + g), dpi=prop['dpi'], \
      bbox_extra_artists=(lgd,), bbox_inches='tight')


def plot_diurnal_energy_balance_2(dp_mod, dp_obs, prop):
  """
  " Plot energy fluxes for energy balance
  " dp_mod: data pack of modelled data, [time, H, LE, G_sc, Rnet]
  " dp_obs: data pack of observed data, [time, H, LE, G_sc, Rnet]
  """
  # Set parameters
  dpi = 150
  save_dir = prop['save_dir']
  name_list = ['H', 'LE', 'Gsoil', 'Rnet']  # corresponding to indices 1, 2, 3, 4
  show_name = [r'$SH$', r'$LE$', r'$G_{\mathrm{soil}}$', r'$R_{\mathrm{net}}$']  # corresponding to indices 1, 2, 3, 4

  # Calculate averaged diurnal pattern for modelled data
  dpd_mod = {}
  dpd_obs = {}
  for i, k in enumerate(name_list):
    # Initialize empty dict for each energy flux
    dpd_mod[k] = {}
    dpd_obs[k] = {}

    # Get daily avg and std
    dtime_mod, dpd_mod[k]['davg'], dpd_mod[k]['dstd'] = ppf.diurnal_average(dp_mod[0], dp_mod[i+1])
    dtime_obs, dpd_obs[k]['davg'], dpd_obs[k]['dstd'] = ppf.diurnal_average(dp_obs[0], dp_obs[i+1])

  # Print key values
  # print(dtime_mod)  # 0.5, 1.5, ..., 23.5
  i = 12
  print('Modelled time, H, LE, Gsoil, Rnet at 12:30:', \
    dtime_mod[i], dpd_mod['H']['davg'][i], dpd_mod['LE']['davg'][i], dpd_mod['Gsoil']['davg'][i], dpd_mod['Rnet']['davg'][i])
  print('Measured time, H, LE, Gsoil, Rnet at 12:30:', \
    dtime_obs[i], dpd_obs['H']['davg'][i], dpd_obs['LE']['davg'][i], dpd_obs['Gsoil']['davg'][i], dpd_obs['Rnet']['davg'][i])
  i = 1
  print('Modelled time, H, LE, Gsoil, Rnet at 01:30:', \
    dtime_mod[i], dpd_mod['H']['davg'][i], dpd_mod['LE']['davg'][i], dpd_mod['Gsoil']['davg'][i], dpd_mod['Rnet']['davg'][i])
  print('Measured time, H, LE, Gsoil, Rnet at 01:30:', \
    dtime_obs[i], dpd_obs['H']['davg'][i], dpd_obs['LE']['davg'][i], dpd_obs['Gsoil']['davg'][i], dpd_obs['Rnet']['davg'][i])
  print('Difference between mod and obs H: ', dpd_mod['H']['davg'] - dpd_obs['H']['davg'])
  print('Difference between mod and obs LE: ', dpd_mod['LE']['davg'] - dpd_obs['LE']['davg'])
  print('Difference between mod and obs Gsoil: ', dpd_mod['Gsoil']['davg'] - dpd_obs['Gsoil']['davg'])
  print('Difference between mod and obs Rnet: ', dpd_mod['Rnet']['davg'] - dpd_obs['Rnet']['davg'])

  # Initialize fg and ax
  fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=dpi)

  # Modelled data with shaded area as 1 std
  # print('cmap_qua:', cmap_qua_colors)
  cm_list = list(cmap_qua_colors)
  cm_list[0] = (65.0/255.0, 105.0/255.0, 225.0/255.0, 0.7)
  # cm_list[0] = (0.0/255.0, 0.0/255.0, 139.0/255.0, 0.7)
  # cm_list[-2] = cm_list[-1]
  for i, k in enumerate(name_list):
    # Set errorbar to be transparent
    # print(cmap_qua_colors[i])
    # cm = [*cmap_qua_colors[i]]
    cm = list(cm_list[i])
    cm[-1] = 1.0  # r, g, b, alpha

    p, = ax.plot(dtime_mod, dpd_mod[k]['davg'], color=cm, linestyle='-', linewidth=2, label=r'{0}'.format(show_name[i]))
    y1 = dpd_mod[k]['davg'] + dpd_mod[k]['dstd']
    y2 = dpd_mod[k]['davg'] - dpd_mod[k]['dstd']
    ax.fill_between(dtime_mod, y1, y2, where=y1>=y2, facecolor=plt.getp(p, 'color'), alpha=0.2)

    # ax.errorbar(dtime_obs, dpd_obs[k]['davg'], yerr=dpd_obs[k]['dstd'], fmt='o', color=cmap_qua_colors[i])
    cm[-1] = 0.8  # r, g, b, alpha
    ax.errorbar(dtime_obs, dpd_obs[k]['davg'], yerr=dpd_obs[k]['dstd'], fmt='o', color=cm)

  # Empty lines for showing labels
  ax.plot(dtime_mod, np.empty(dtime_mod.size)*np.nan, 'k-', label='model')
  ax.plot(dtime_obs, np.empty(dtime_obs.size)*np.nan, 'ko', label='measurement')

  # Set ticks
  ax.set_xticks(np.arange(0, 25, 4))

  # Set axis limit
  ax.set_xlim(0, 24)
  ax.set_ylim(-600, 300)

  # Set labels
  ax.set_xlabel('Time [hour]')
  ax.set_ylabel(r'Energy fluxes [W m$^{-2}$]')

  # Set legend
  handles,labels = ax.get_legend_handles_labels()
  ax.legend(handles, labels, loc=3)

  # Plot gird lines
  ax.grid()

  # Save figure
  fg.savefig('{0}/{1}.png'.format(save_dir,'paper-energy_fluxes'), dpi=dpi)


def plot_monthly_mod_obs(mod_time, mod_data, obs_time, obs_data, prop):
  # Set parameters
  dpi = 150
  save_dir = prop['save_dir']
  show_name = prop['show_name']
  file_name = prop['file_name']

  # Initialize fg and ax
  fg, ax = plt.subplots(1, 1, figsize=(12, 8), dpi=dpi)

  p_mod, = ax.plot(mod_time, mod_data, color=cmap_qua_colors[0], linestyle='-', linewidth=2, label='model')
  p_obs, = ax.plot(obs_time, obs_data, 'o', color=cmap_qua_colors[0], label='measurement')

  # Set limits
  # ax.set_ylim(0, 100)

  # Set xticklabels
  locator = mdates.AutoDateLocator()
  formatter = mdates.AutoDateFormatter(locator)
  ax.xaxis.set_major_locator(mdates.DayLocator(range(1,32,2)))
  ax.xaxis.set_major_formatter(mdates.DateFormatter('%d'))

  # Plot gird lines
  ax.grid()

  # Set labels
  ax.set_xlabel('July')
  ax.set_ylabel(r'{0}'.format(show_name))

  # Set legend
  handles,labels = ax.get_legend_handles_labels()
  ax.legend(handles, labels, loc='best')

  # Tight layout
  fg.tight_layout()

  # Save figure
  fg.savefig('{0}/{1}'.format(save_dir, file_name), bbox_inches='tight', dpi=dpi)


def plot_diurnal_mod_obs(mod_time, mod_data, obs_time, obs_data, prop):
  # Set parameters
  dpi = 150
  save_dir = prop['save_dir']
  show_name = prop['show_name']
  file_name = prop['file_name']
  ylabel = prop['ylabel']

  #### Calculate averaged diurnal pattern for modelled data
  # Get daily avg and std
  mod_dtime, mod_davg, mod_dstd = ppf.diurnal_average(mod_time, mod_data)
  obs_dtime, obs_davg, obs_dstd = ppf.diurnal_average(obs_time, obs_data)

  #### Plot
  # Initialize fg and ax
  fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=dpi)

  # Left yaxis for T: modelled data with shaded area as 1 std, observed data with errorbar
  p, = ax.plot(mod_dtime, mod_davg, color=cmap_qua_colors[0], linestyle='-', linewidth=2, label='model')
  y1 = mod_davg + mod_dstd
  y2 = mod_davg - mod_dstd
  ax.fill_between(mod_dtime, y1, y2, where=y1>=y2, facecolor=plt.getp(p, 'color'), alpha=0.2)

  # Set errorbar to be transparent
  cm = list(cmap_qua_colors[0])
  cm[-1] = 0.7  # r, g, b, alpha
  
  # Plot obs data with errorbar
  ax.errorbar(obs_dtime, obs_davg, yerr=obs_dstd, fmt='o', color=cm, label='measurement')

  # Set ticks
  ax.set_xticks(np.arange(0, 25, 4))

  # Set axis limit
  ax.set_xlim(0, 24)

  # Set labels
  ax.set_xlabel('Time [hour]')
  ax.set_ylabel(r'{0}'.format(ylabel))

  # Set legend
  handles,labels = ax.get_legend_handles_labels()
  ax.legend(handles, labels, loc=3)

  # Plot gird lines
  ax.grid()

  # Save figure
  fg.savefig('{0}/{1}'.format(save_dir, file_name), dpi=dpi)


def plot_diurnal_T_RH(dp_mod, dp_obs, prop):
  # Set parameters
  dpi = 150
  save_dir = prop['save_dir']
  name_list = ['T', 'RH']
  show_name = ['T', 'RH']

  #### Calculate averaged diurnal pattern for modelled data
  dpd_mod = {}
  dpd_obs = {}
  for i, k in enumerate(name_list):
    # Initialize empty dict
    dpd_mod[k] = {}
    dpd_obs[k] = {}

    # Get daily avg and std
    print(k)
    dtime_mod, dpd_mod[k]['davg'], dpd_mod[k]['dstd'] = ppf.diurnal_average(dp_mod[0], dp_mod[i+1])
    dtime_obs, dpd_obs[k]['davg'], dpd_obs[k]['dstd'] = ppf.diurnal_average(dp_obs[0], dp_obs[i+1])

  #### Plot
  # Initialize fg and ax
  fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=dpi)

  # Left yaxis for T: modelled data with shaded area as 1 std, observed data with errorbar
  p, = ax.plot(dtime_mod, dpd_mod['T']['davg'], color=cmap_qua_colors[0], linestyle='-', linewidth=2, label='{0}'.format(show_name[0]))
  y1 = dpd_mod['T']['davg'] + dpd_mod['T']['dstd']
  y2 = dpd_mod['T']['davg'] - dpd_mod['T']['dstd']
  ax.fill_between(dtime_mod, y1, y2, where=y1>=y2, facecolor=plt.getp(p, 'color'), alpha=0.2)

  # Set errorbar to be transparent
  cm = list(cmap_qua_colors[0])
  cm[-1] = 0.7  # r, g, b, alpha
  
  # Plot obs data with errorbar
  ax.errorbar(dtime_obs, dpd_obs['T']['davg'], yerr=dpd_obs['T']['dstd'], fmt='o', color=cm)

  # Right yaixs for RH
  ax1 = ax.twinx()

  p, = ax1.plot(dtime_mod, dpd_mod['RH']['davg'], color=cmap_qua_colors[1], linestyle='-', linewidth=2, label='{0}'.format(show_name[1]))
  y1 = dpd_mod['RH']['davg'] + dpd_mod['RH']['dstd']
  y2 = dpd_mod['RH']['davg'] - dpd_mod['RH']['dstd']
  ax1.fill_between(dtime_mod, y1, y2, where=y1>=y2, facecolor=plt.getp(p, 'color'), alpha=0.2)

  # Set errorbar to be transparent
  cm = list(cmap_qua_colors[1])
  cm[-1] = 0.7  # r, g, b, alpha
  
  # Plot obs data with errorbar
  ax1.errorbar(dtime_obs, dpd_obs['RH']['davg'], yerr=dpd_obs['RH']['dstd'], fmt='o', color=cm)

  # Empty lines for showing labels
  ax.plot(dtime_mod, np.empty(dtime_mod.size)*np.nan, 'k-', label='model')
  ax.plot(dtime_obs, np.empty(dtime_obs.size)*np.nan, 'ko', label='measurement')

  # Set ticks
  ax.set_xticks(np.arange(0, 25, 4))

  # Set axis limit
  ax.set_xlim(0, 24)
  # ax.set_ylim(-600, 300)

  # Set labels
  ax.set_xlabel('Time [hour]')
  ax.set_ylabel(r'T [K]')
  ax1.set_ylabel(r'RH')

  # Set legend
  handles,labels = ax.get_legend_handles_labels()
  ax.legend(handles, labels, loc=3)

  # Plot gird lines
  ax.grid()

  # Save figure
  fg.savefig('{0}/{1}.png'.format(save_dir,'paper-T_RH'), dpi=dpi)


def plot_T_RH_PAR(time, T, RH, PAR, maaPAR, prop):
  # Set parameters
  dpi = 150
  save_dir = prop['save_dir']
  cp = sns.color_palette('hls', 8)  # blue, gree, red, purple, yellow, cyan

  # Initialize fg and ax
  fg, ax = plt.subplots(2, 1, figsize=(12, 8), dpi=dpi)

  # Subplot 1
  l_T, = ax[0].plot(time, T, color=cp[0], label='T')
  ax0r = ax[0].twinx()
  l_RH, = ax0r.plot(time, RH, color=cp[5], label='RH')

  # Set limits
  ax0r.set_ylim(0, 100)

  # Set ylabel
  ax[0].set_ylabel(r'T ($^\circ$C)')
  ax0r.set_ylabel(r'RH (%)')

  # Subplot 2
  ax[1].plot(time, PAR, color=cp[6], label='canopy top')
  ax[1].plot(time, maaPAR, color=cp[2], label='subcanopy')

  # Set xlabel and ylabel
  ax[1].set_xlabel('July')
  ax[1].set_ylabel(r'PAR ($\mu$mol m$^{-2}$ s$^{-1}$)')

  # Set limits
  ax[1].set_ylim(0, 2000)

  for i in range(2):
    # Set limits
    ax[i].set_xlim(time[0], time[-1])
    # Set xticklabels
    locator = mdates.AutoDateLocator()
    formatter = mdates.AutoDateFormatter(locator)
    # ax[i].xaxis.set_major_locator(locator)
    ax[i].xaxis.set_major_locator(mdates.DayLocator(range(1,32,2)))
    # ax[i].xaxis.set_major_formatter(formatter)
    ax[i].xaxis.set_major_formatter(mdates.DateFormatter('%d'))
    # plt.setp(ax[i].xaxis.get_majorticklabels(), rotation=30, ha='right')

    # Plot gird lines
    ax[i].grid()

  # Set legend
  # ax[0].legend([l_T, l_RH], ['T', 'RH'], bbox_to_anchor=(0.1, 0.9), loc='lower left', fancybox=True)
  ax[0].legend([l_T, l_RH], ['T', 'RH'], bbox_to_anchor=(0.05, 0.73), loc='lower left', fontsize=14)

  handles,labels = ax[1].get_legend_handles_labels()
  ax[1].legend(handles, labels, loc='best')

  # Set subplot indices
  ax[0].text(0.01, 0.90, '(a)', transform=ax[0].transAxes)
  ax[1].text(0.01, 0.90, '(b)', transform=ax[1].transAxes)

  # Tight layout
  fg.tight_layout()

  # Save figure
  fg.savefig('{0}/{1}.png'.format(save_dir,'paper-T_RH_PAR_maaPAR'), bbox_inches='tight', dpi=dpi)


def plot_paper_mod_obs_windh_T_ustar_RH(data_mod, time_mod, height_mod, data_obs, time_obs, height_obs, d_ind, n_ind, prop):
  """
  windh: windh[nt, nz], [m s-1], horizontal wind speed
  Tair: Tair[nt, nz], [degC], air temperature
  ustar: ustar[nt], [m s-1], ustar at 23 m
  RHinc: RHinc[nt], [%], RH averaged inside the canopy
  data_mod: [windh_mod, Tair_mod, ustar_mod, RHinc_mod]
  time_mod: sosa time array
  height_mod: sosa height levels
  data_obs: [windh_obs, Tair_obs, ustar_obs, RHinc_obs]
  time_obs: avaa time array
  height_obs: [windh_zz, Tair_zz], avaa height levels, usually different for different variables
  d_ind: daytime indices
  n_ind: nighttime indices
  """

  # Set parameters
  c1 = cmap_qua_colors[1]  # sosa color
  c2 = cmap_qua_colors[2]  # avaa color
  windh_offset = 3.0
  Tair_offset = 0.0
  dpi = prop['dpi']
  save_dir = prop['save_dir']
  file_name = prop['file_name']

  #
  # Calculate needed variables
  #

  # Model
  windh_mod = data_mod[0]
  Tair_mod  = data_mod[1]
  ustar_mod = data_mod[2]
  RHinc_mod = data_mod[3]

  windh_prof_davg_mod = np.nanmean(windh_mod[d_ind, :], axis=0)
  windh_prof_dstd_mod = np.nanstd(windh_mod[d_ind, :], axis=0)
  windh_prof_navg_mod = np.nanmean(windh_mod[n_ind, :], axis=0)
  windh_prof_nstd_mod = np.nanstd(windh_mod[n_ind, :], axis=0)
  Tair_prof_davg_mod = np.nanmean(Tair_mod[d_ind, :], axis=0)
  Tair_prof_dstd_mod = np.nanstd(Tair_mod[d_ind, :], axis=0)
  Tair_prof_navg_mod = np.nanmean(Tair_mod[n_ind, :], axis=0)
  Tair_prof_nstd_mod = np.nanstd(Tair_mod[d_ind, :], axis=0)

  ustar_dtime_mod, ustar_davg_mod, ustar_dstd_mod = ppf.diurnal_average(time_mod, ustar_mod)
  RHinc_dtime_mod, RHinc_davg_mod, RHinc_dstd_mod = ppf.diurnal_average(time_mod, RHinc_mod)

  # Observation
  windh_obs = data_obs[0]
  Tair_obs  = data_obs[1]
  ustar_obs = data_obs[2]
  RHinc_obs = data_obs[3]
  height_windh_obs = height_obs[0]
  height_Tair_obs  = height_obs[1]

  windh_prof_davg_obs = np.nanmean(windh_obs[d_ind, :], axis=0)
  windh_prof_dstd_obs = np.nanstd(windh_obs[d_ind, :], axis=0)
  windh_prof_navg_obs = np.nanmean(windh_obs[n_ind, :], axis=0)
  windh_prof_nstd_obs = np.nanstd(windh_obs[n_ind, :], axis=0)
  Tair_prof_davg_obs = np.nanmean(Tair_obs[d_ind, :], axis=0)
  Tair_prof_dstd_obs = np.nanstd(Tair_obs[d_ind, :], axis=0)
  Tair_prof_navg_obs = np.nanmean(Tair_obs[n_ind, :], axis=0)
  Tair_prof_nstd_obs = np.nanstd(Tair_obs[d_ind, :], axis=0)

  ustar_dtime_obs, ustar_davg_obs, ustar_dstd_obs = ppf.diurnal_average(time_obs, ustar_obs)
  RHinc_dtime_obs, RHinc_davg_obs, RHinc_dstd_obs = ppf.diurnal_average(time_obs, RHinc_obs)

  #
  # Display data information
  #
  print('----- windh profile obs -----')
  print('Height: ', height_windh_obs)
  print('Day avg: ', windh_prof_davg_obs)
  print('Day std: ', windh_prof_dstd_obs)
  print('Night avg: ', windh_prof_navg_obs)
  print('Night std: ', windh_prof_nstd_obs)

  print('----- Tair profile obs -----')
  print('Height: ', height_Tair_obs)
  print('Day avg: ', Tair_prof_davg_obs)
  print('Day std: ', Tair_prof_dstd_obs)
  print('Night avg: ', Tair_prof_navg_obs)
  print('Night std: ', Tair_prof_nstd_obs)

  print('----- RHinc obs -----')
  print('Diurnal times: ', RHinc_dtime_obs)
  print('Diurnal average: ', RHinc_davg_obs)
  print('Diurnal std: ', RHinc_dstd_obs)

  print('----- u* obs -----')
  print('Diurnal times: ', ustar_dtime_obs)
  print('Diurnal average: ', ustar_davg_obs)
  print('Diurnal std: ', ustar_dstd_obs)

  #
  # Plot figures
  #

  # Figure properties
  # font = {'size': 12}
  # mpl.rc('font', **font)
  tfs = 12

  #
  # Create figure handle and subplots
  # ax1: profiles of wind speed during daytime and nighttime
  # ax2: profiles of air temperature during daytime and nighttime
  # ax3: diurnal pattern of u* at 23 m
  # ax4: diurnal pattern of RH inside the canopy
  #
  fg = plt.figure(figsize=(9, 12), dpi=dpi)
  grid_mat = (4, 3)

  ax1 = plt.subplot2grid(grid_mat, (0, 0), rowspan=2, colspan=1)
  ax1n = ax1.twiny()
  ax2 = plt.subplot2grid(grid_mat, (2, 0), rowspan=2, colspan=3)
  ax3 = plt.subplot2grid(grid_mat, (0, 1), colspan=2)
  ax4 = plt.subplot2grid(grid_mat, (1, 1), colspan=2)
  
  fg.subplots_adjust(hspace=0.5, wspace=0.4)

  #
  # ax1: windh profiles
  # 

  # Model daytime
  p1, = ax1.plot(windh_prof_davg_mod, height_mod, '-', color=c1, label='mod day')
  x1 = windh_prof_davg_mod + windh_prof_dstd_mod
  x2 = windh_prof_davg_mod - windh_prof_dstd_mod
  ax1.fill_betweenx(height_mod, x1, x2, where=x1>=x2, facecolor=plt.getp(p1, 'color'), alpha=0.4)

  # Model nightitime
  p1, = ax1n.plot(windh_prof_navg_mod, height_mod, '--', color=c1, label='mod night')
  x1 = windh_prof_navg_mod + windh_prof_nstd_mod
  x2 = windh_prof_navg_mod - windh_prof_nstd_mod
  ax1n.fill_betweenx(height_mod, x1, x2, where=x1>=x2, facecolor=plt.getp(p1, 'color'), alpha=0.4)

  # Observation daytime
  (_, caps, _) = ax1.errorbar(windh_prof_davg_obs, height_windh_obs, xerr=windh_prof_dstd_obs, \
                             fmt='o', ms=7, mfc=c2, mec=c2, mew=2, ecolor=c2, capsize=10, elinewidth=2,
                             label='obs day')

  # Observation nighttime
  (_, caps, _) = ax1n.errorbar(windh_prof_navg_obs, height_windh_obs, xerr=windh_prof_nstd_obs,
                           fmt='o', ms=7, mfc='none', mec=c2, mew=2, ecolor=c2, capsize=10, elinewidth=2,
                           label='obs night')

  # ax1 set up
  ax1.set_xlim((0, 8))
  ax1.set_ylim((0, 2.0))
  ax1.set_xticks(range(0,9,2))
  ax1.set_xlabel(r'day windh [m s$^{-1}$]')
  ax1.set_ylabel('Height/$h_c$')
  # ax1.text(0.06, 0.85, 'daytime', transform=ax1.transAxes, size=tfs)
  # ax1.text(0.50, 0.10, r'nighttime'+'\n'+'+ {0:1.0f} m s$^{{-1}}$'.format(windh_offset), transform=ax1.transAxes, size=tfs)
  ax1.text(0.05, 0.95, '(a)', transform=ax1.transAxes, size=tfs)

  # ax1n set up
  ax1n.set_xlim((-3, 5))
  ax1n.set_xticks(range(-3,6,2))
  ax1n.set_xlabel(r'night windh [m s$^{-1}$]')

  handles,labels = ax1.get_legend_handles_labels()
  handlesn,labelsn = ax1n.get_legend_handles_labels()
  lgd1 = ax1n.legend(handles+handlesn, labels+labelsn, loc='center right', \
    bbox_to_anchor=(1.4, 0.2), scatterpoints=1, numpoints=1, prop={'size':12})
  # lgd1.get_frame().set_facecolor('#FFFFFF')
  # ax1.legend = None
  # ax1n.add_artist(lgd1)

  #
  # ax2: Tair profiles
  # 

  # Model
  p1, = ax2.plot(Tair_prof_davg_mod, height_mod, '-', color=c1, label='mod day')
  x1 = Tair_prof_davg_mod + Tair_prof_dstd_mod
  x2 = Tair_prof_davg_mod - Tair_prof_dstd_mod
  ax2.fill_betweenx(height_mod, x1, x2, where=x1>=x2, facecolor=plt.getp(p1, 'color'), alpha=0.4)

  p1, = ax2.plot(Tair_prof_navg_mod+Tair_offset, height_mod, '--', color=c1, label='mod night')
  x1 = Tair_prof_navg_mod + Tair_prof_nstd_mod + Tair_offset
  x2 = Tair_prof_navg_mod - Tair_prof_nstd_mod + Tair_offset
  ax2.fill_betweenx(height_mod, x1, x2, where=x1>=x2, facecolor=plt.getp(p1, 'color'), alpha=0.4)
  
  # Observation
  (_, caps, _) = ax2.errorbar(Tair_prof_davg_obs, height_Tair_obs, xerr=Tair_prof_dstd_obs,
                             fmt='o', ms=7, mfc=c2, mec=c2, mew=2, ecolor=c2, capsize=10, elinewidth=2,
                             label='obs day')
  
  (_, caps, _) = ax2.errorbar(Tair_prof_navg_obs+Tair_offset, height_Tair_obs, xerr=Tair_prof_nstd_obs,
                             fmt='o', ms=7, mfc='none', mec=c2, mew=2, ecolor=c2, capsize=10, elinewidth=2,
                             label='obs night')
  
  # ax2 set up
  ax2.set_xlim((13, 28))
  ax2.set_ylim((0, 2.0))
  ax2.set_xticks(range(13,29,1))
  ax2.set_xlabel(r'T [$^\circ$C]')
  ax2.set_ylabel(r'Height/$h_c$')
  # ax2.set_yticklabels([])
  ax2.text(0.65, 0.85, 'daytime', transform=ax2.transAxes, size=tfs)
  # ax2.text(0.65, 0.10, 'nighttime\n {0:1.0f} $^\circ$C'.format(Tair_offset), transform=ax2.transAxes, size=tfs)
  ax2.text(0.22, 0.85, 'nighttime', transform=ax2.transAxes, size=tfs)
  ax2.text(0.02, 0.95, '(b)', transform=ax2.transAxes, size=tfs)

  #
  # ax3: diurnal pattern of ustar at 23 m
  #
  
  # Model
  p1, = ax3.plot(ustar_dtime_mod, ustar_davg_mod, color=c1, label='model')
  y1 = ustar_davg_mod + ustar_dstd_mod
  y2 = ustar_davg_mod - ustar_dstd_mod
  ax3.fill_between(ustar_dtime_mod, y1, y2, where=y1>=y2, facecolor=plt.getp(p1, 'color'), alpha=0.2)

  # Observation
  ax3.errorbar(ustar_dtime_obs, ustar_davg_obs, yerr=ustar_dstd_obs, fmt='o', ms=7, color=c2)
  # (_, caps, _) = ax2.errorbar(Tair_prof_navg_obs+Tair_offset, height_Tair_obs, xerr=Tair_prof_nstd_obs,
  #                            fmt='o', ms=8, mfc='none', mec=c2, mew=2, ecolor=c2, capsize=10, elinewidth=2,
  #                            label='obs night')

  # ax3 set up
  ax3.set_xlim((0,24))
  ax3.set_ylim((0, 1))
  ax3.set_xticklabels([])
  ax3.yaxis.tick_right()
  ax3.set_ylabel(r'$u_*$ at 23 m [m s$^{-1}$]')
  ax3.yaxis.set_label_position('right')
  ax3.text(0.05, 0.90, '(c)', transform=ax3.transAxes, size=tfs)

  # handles,labels = ax3.get_legend_handles_labels()
  # ax3.legend(handles, labels, loc='lower center', scatterpoints=1, prop={'size':10})

  #
  # ax4: diurnal pattern of RHinc
  #

  # Model
  p1, = ax4.plot(RHinc_dtime_mod, RHinc_davg_mod, color=c1, label='mod')
  y1 = RHinc_davg_mod + RHinc_dstd_mod
  y2 = RHinc_davg_mod - RHinc_dstd_mod
  ax4.fill_between(RHinc_dtime_mod, y1, y2, where=y1>=y2, facecolor=plt.getp(p1, 'color'), alpha=0.2)

  # Observation
  ax4.errorbar(RHinc_dtime_obs, RHinc_davg_obs, yerr=RHinc_dstd_obs, fmt='o', ms=7, color=c2, label='obs')

  # ax4 set up
  ax4.set_xlim((0, 24))
  ax4.set_ylim((30, 90))
  ax4.yaxis.tick_right()
  ax4.set_xlabel('Time [h]')
  ax4.set_ylabel(r'RH [%]')
  ax4.yaxis.set_label_position('right')
  ax4.text(0.05, 0.90, '(d)', transform=ax4.transAxes, size=tfs)

  handles,labels = ax4.get_legend_handles_labels()
  lgd4 = ax4.legend(handles, labels, loc='upper center', bbox_to_anchor=(0.5, 1.03), scatterpoints=1, numpoints=1, prop={'size':12})

  # Turn on the grids for all the axes
  for a in [ax1, ax2, ax3, ax4]:
    a.grid()

  # Save the figure
  # fg.savefig('{0}/{1}'.format(save_dir, file_name), dpi=dpi, bbox_extra_artists=(lgd1,), bbox_inches='tight')
  fg.savefig('{0}/{1}'.format(save_dir, file_name), dpi=dpi, bbox_inches='tight')


def plot_Qconc():
  pass


#----------------------------------------------------------#
#
# Plots for thesis
#
#----------------------------------------------------------#
def plot_flux_diurnal_pattern_all_in_one_thesis(sosa_data, voc_data, ind_nan, prop):
  print('Plotting diurnal pattern ...')
  ##### Set parameters
  ncz = sosa_data['grid']['ncz']
  zz = sosa_data['grid']['zz']

  k_list = ['MT', 'C5H8+MBO', 'CH3OH', 'CH3CHO', 'CH3COCH3', 'HCHO']
  c_list = ['monoterpenes', 'isoprene+MBO', 'methanol', 'acetaldehyde', 'acetone', 'formaldehyde']
  c1_list = ['monoterpenes', 'isoprene', 'MBO', 'methanol', 'acetaldehyde', 'acetone', 'formaldehyde']
  st_list = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)']

  ##### Plot
  nrow = 2
  ncol = 3
  fg, ax = plt.subplots(nrow, ncol, figsize=prop['figsize'], dpi=prop['dpi'])
  # gs = gridspec.GridSpec(2, 3)
  # gs.update(hspace=0.1)
  # ax = np.empty((1, 

  for ic, (k, c) in enumerate(zip(k_list, c_list)):
    print(k, c)

    ## subplot position
    # ax = plt.subplot(gs[ic])
    row = ic // ncol
    col = ic % ncol

    ## sosa diurnal flux at 23 m (ncz + 1)
    sosa_time = sosa_data['time']['raw']
    if k == 'C5H8+MBO':
      sosa_flux = sosa_data['C5H8']['flux']['raw'][:, ncz+1] * ppf.moleccm2s1_to_ugm2s1(ppp.molar_mass['C5H8']*1.0e-3) + \
        sosa_data['MBO']['flux']['raw'][:, ncz+1] * ppf.moleccm2s1_to_ugm2s1(ppp.molar_mass['MBO']*1.0e-3)
    else:
      sosa_flux = sosa_data[k]['flux']['raw'][:, ncz+1] * ppf.moleccm2s1_to_ugm2s1(ppp.molar_mass[k]*1.0e-3)
    sosa_dtime, sosa_flux_davg, sosa_flux_dstd = ppf.diurnal_average(sosa_time, sosa_flux)

    ## measured voc diurnal flux at 23 m
    ind1 = ppf.get_date_ind(voc_data['flux']['time']['raw'], sosa_time[0])  # ind1: 2010, 6, 30, 23, 22, 30, ind1+1: 2010, 7, 1, 2, 22, 30
    ind2 = ppf.get_date_ind(voc_data['flux']['time']['raw'], sosa_time[-1])  # ind2: 2010, 7, 31, 23, 22, 30, ind2+1: 2010, 8, 1, 2, 22, 30
    voc_time = voc_data['flux']['time']['raw'][ind1+1:ind2+1]
    if k == 'C5H8+MBO':
      voc1 = np.copy( voc_data['flux']['C5H8']['raw'][ind1+1:ind2+1] )
      voc1[ind_nan] = np.nan
      voc2 = np.copy( voc_data['flux']['MBO']['raw'][ind1+1:ind2+1] )
      voc2[ind_nan] = np.nan
      voc_flux = voc1 * ppf.moleccm2s1_to_ugm2s1(ppp.molar_mass['C5H8']*1.0e-3) + \
        voc2 * ppf.moleccm2s1_to_ugm2s1(ppp.molar_mass['MBO']*1.0e-3)
    else:
      voc1 = np.copy( voc_data['flux'][k]['raw'][ind1+1:ind2+1] )
      voc1[ind_nan] = np.nan
      voc_flux = voc1 * ppf.moleccm2s1_to_ugm2s1(ppp.molar_mass[k]*1.0e-3)
    voc_dtime, voc_flux_davg, voc_flux_dstd = ppf.diurnal_average(voc_time, voc_flux)

    # Print some key values
    print('VOC fluxes mod - obs: ', k, sosa_flux_davg - voc_flux_davg)
    print('VOC fluxes (mod - obs)/obs: ', k, (sosa_flux_davg - voc_flux_davg)/voc_flux_davg)

    prop1 = {'color': cmap_qua_colors[0], 'linewidth': 2}
    prop2 = {'color': cmap_qua_colors[1], 'fmt': 'o'}
    l1, l2 = axes_2_xy_std(ax[row, col], sosa_dtime, sosa_flux_davg, sosa_flux_dstd, prop1, \
      voc_dtime, voc_flux_davg, voc_flux_dstd, prop2)

  ## Set limits and ticks

  for i in range(nrow):
    for j in range(ncol):
      print(i, j)
      iax = i*ncol+j
      ax[i, j].set_xlim(0, 24)
      ax[i, j].set_xticks(range(0, 25, 4))
      ax[i, j].text(0.05, 0.9, st_list[iax] + ' ' + c_list[iax], transform=ax[i, j].transAxes, fontsize=14)
  ax[0, 0].set_xticklabels([])
  ax[0, 1].set_xticklabels([])
  ax[0, 2].set_xticklabels([])
  ax[1, 0].set_xlabel('Time [h]')
  ax[1, 0].set_ylabel(r'Flux [$\mu$g m$^{-2}$ s$^{-1}$]')

  ## Adjust space between subplots
  fg.subplots_adjust(wspace=0.4, hspace=0.1)
  # fg.tight_layout()

  ## Save the figure
  fg.savefig('{0}/{1}.png'.format(prop['save_dir'], prop['file_name']), dpi=prop['dpi'])


def plot_sosa_Q_contr_thesis(sosa, prop):
  save_dir = prop['save_dir']
  file_name = prop['file_name']
  figsize = prop['figsize']
  dpi = prop['dpi']

  periods = ['all', 'day', 'nit']
  st_str = ['(a) all', '(b) day', '(c) night']

  # Gas list
  gas_list = [ \
    'MT', 'C5H8+MBO', 'SQT', 'CH3CHO', \
    'CH3OH', 'CH3COCH3', 'HCHO', 'ACETOL', 'PINIC', \
    'BCSOZOH', 'ISOP34OOH', 'ISOP34NO3', \
    ]
  name_list = [ \
    'monoterpenes', 'isoprene+MBO', 'sesquiterpenes', 'acetaldehyde', \
    'methanol', 'acetone', 'formaldehyde', 'acetol', 'pinic acid', \
    'BCSOZOH', 'ISOP34OOH', 'ISOP34NO3', \
    ]
  ng = len(gas_list)

  bw = 1
  # ct = {'emis': '#4daf4a', 'chem': '#e41a1c', 'turb': '#377eb8', 'depo': '#984ea3'}  # dark
  ct = {'emis': '#a6d854', 'chem': '#fb8072', 'turb': '#80b1d3', 'depo': '#bebada'}  # light
  # ct = {'emis': '#a6d854', 'chem': '#fc8d62', 'turb': '#8da0cb', 'depo': '#e78ac3'}  # soft

  # Get Q values
  Q, nc, mc, ind = get_Q_values(sosa)

  # Initialize contributions for all gases
  contr_emis = {}
  contr_chem = {}
  contr_turb = {}
  contr_depo = {}

  # Calculate contributions for every period and all gases
  for p in periods:
    contr_emis[p] = np.zeros((ng,))
    contr_chem[p] = np.zeros((ng,))
    contr_turb[p] = np.zeros((ng,))
    contr_depo[p] = np.zeros((ng,))

    # Calculate contributions for every gases
    for ig, g in enumerate(gas_list):
      contr_emis[p][ig], contr_chem[p][ig], contr_turb[p][ig], contr_depo[p][ig] = \
        calculate_relative_contribution( \
        [Q['emis']['dct'][p][g], Q['chem']['dct'][p][g], Q['turb']['dct'][p][g], Q['depo']['dct'][p][g]] )
      print('{0:4s} {1:15s} {2:6.3f} {3:6.3f} {4:6.3f} {5:6.3f}'.format( \
        p, g, contr_emis[p][ig], contr_chem[p][ig], contr_turb[p][ig], contr_depo[p][ig]))

  # Plot Q contribution
  # fg, ax = plt.subplots(3, 1, figsize=figsize, dpi=dpi)
  fg = plt.figure(figsize=figsize, dpi=dpi)
  gs = gridspec.GridSpec(2, 4)
  ax = [None]*3
  ax[0] = plt.subplot(gs[0, 1:3])
  ax[1] = plt.subplot(gs[1, 0:2])
  ax[2] = plt.subplot(gs[1, 2:])
  for ip, p in enumerate(periods):
    axes_bar_stack(ax[ip], np.arange(ng)+0.5, \
      [contr_emis[p], contr_chem[p], contr_turb[p], contr_depo[p]], \
      1, [ct['emis'], ct['chem'], ct['turb'], ct['depo']], alpha=1)

    ax[ip].set_xlim(0, ng)
    ax[ip].set_ylim(-1, 1)

    ax[ip].set_xticks(np.arange(ng)+bw/2.0)
    ax[ip].set_xticklabels(name_list)
    ax[ip].set_yticks(np.arange(-1, 1.1, 0.2))
    # ax[ip].set_ylabel(r'$\overline{Q}^{\Delta,h_c}_{rel,n}$')
    ax[ip].set_ylabel('Relative contribution', fontsize=18)

    plt.setp(ax[ip].xaxis.get_majorticklabels(), rotation=30, ha='right', fontsize=18)
    plt.setp(ax[ip].yaxis.get_majorticklabels(), fontsize=18)

    # Put the legend box outside
    if ip==0:
      alpha = 1.0
      p_emis = mpatches.Patch(color=ct['emis'], linewidth=0, alpha=alpha)
      p_chem = mpatches.Patch(color=ct['chem'], linewidth=0, alpha=alpha)
      p_turb = mpatches.Patch(color=ct['turb'], linewidth=0, alpha=alpha)
      p_depo = mpatches.Patch(color=ct['depo'], linewidth=0, alpha=alpha)
      lgd = ax[ip].legend([p_emis, p_chem, p_turb, p_depo], ['emis', 'chem', 'turb', 'depo'],
        bbox_to_anchor=(0.3, 1.0, 0.7, 0.2), loc=3, ncol=4, mode='expand', prop={'size': 16})
      # handles,labels = ax.get_legend_handles_labels()
      # ax.legend(handles, labels, loc='best', scatterpoints=1)

    # Subtitles
    ax[ip].set_title(st_str[ip], fontsize=18, x=0.06, y=1.05)

  fg.tight_layout()
  fg.savefig('{0}/{1}_combined.png'.format(save_dir, file_name), dpi=dpi, bbox_inches='tight')

  return (gas_list, contr_emis, contr_chem, contr_turb, contr_depo)

  #===== Calculate the contribution fraction =====#
  ncz = sosa['grid']['ncz']
  ddz = sosa['grid']['ddz']

  sosaq = {}
  for ig, g in enumerate(gas_list):
    sosaq[g] = {}
    sosaq[g]['conc_all'] = stats.nanmean( np.sum( sosa[g]['conc']['raw'][:, 1:ncz+1]*ddz[np.newaxis, 1:ncz+1] ) / np.sum(ddz[1:ncz+1]) )
    sosaq[g]['Qemis_all'] = stats.nanmean(sosa[g]['Qemis_int']['raw'])  # monthly average
    sosaq[g]['Qchem_all'] = stats.nanmean(sosa[g]['Qchem_int']['raw'])
    sosaq[g]['Qdepo_all'] = stats.nanmean(sosa[g]['Qdepo_int']['raw'])
    sosaq[g]['Qturb_all'] = stats.nanmean(sosa[g]['Qturb_int']['raw'])
    pos = 0.0
    neg = 0.0
    pos = sosaq[g]['Qemis_all']
    neg = sosaq[g]['Qdepo_all']
    if sosaq[g]['Qchem_all'] >= 0:
      pos += sosaq[g]['Qchem_all']
    else:
      neg += sosaq[g]['Qchem_all']
    if sosaq[g]['Qturb_all'] >= 0:
      pos += sosaq[g]['Qturb_all']
    else:
      neg += sosaq[g]['Qturb_all']
    sosaq[g]['Qmax_all'] = np.amax( [pos, -neg] )
    # sosaq[g]['Qmax_all'] = np.amax([abs(sosaq[g]['Qemis_all']), abs(sosaq[g]['Qchem_all']),
    #   abs(sosaq[g]['Qdepo_all']), abs(sosaq[g]['Qturb_all'])])

    nit_ind = np.squeeze( sosa['n_ind']['raw'] )
    sosaq[g]['conc_nit'] = stats.nanmean( np.sum( sosa[g]['conc']['raw'][nit_ind, 1:ncz+1]*ddz[np.newaxis, 1:ncz+1] ) / np.sum(ddz[1:ncz+1]) )
    sosaq[g]['Qemis_nit'] = stats.nanmean(sosa[g]['Qemis_int']['raw'][nit_ind])
    sosaq[g]['Qchem_nit'] = stats.nanmean(sosa[g]['Qchem_int']['raw'][nit_ind])
    sosaq[g]['Qdepo_nit'] = stats.nanmean(sosa[g]['Qdepo_int']['raw'][nit_ind])
    sosaq[g]['Qturb_nit'] = stats.nanmean(sosa[g]['Qturb_int']['raw'][nit_ind])
    pos = 0.0
    neg = 0.0
    pos = sosaq[g]['Qemis_nit']
    neg = sosaq[g]['Qdepo_nit']
    if sosaq[g]['Qchem_nit'] >= 0:
      pos += sosaq[g]['Qchem_nit']
    else:
      neg += sosaq[g]['Qchem_nit']
    if sosaq[g]['Qturb_nit'] >= 0:
      pos += sosaq[g]['Qturb_nit']
    else:
      neg += sosaq[g]['Qturb_nit']
    sosaq[g]['Qmax_nit'] = np.amax( [pos, -neg] )
    # sosaq[g]['Qmax_nit'] = np.amax([abs(sosaq[g]['Qemis_nit']), abs(sosaq[g]['Qchem_nit']),
    #   abs(sosaq[g]['Qdepo_nit']), abs(sosaq[g]['Qturb_nit'])])

    day_ind = np.squeeze( sosa['d_ind']['raw'] )
    sosaq[g]['conc_day'] = stats.nanmean( np.sum( sosa[g]['conc']['raw'][day_ind, 1:ncz+1]*ddz[np.newaxis, 1:ncz+1] ) / np.sum(ddz[1:ncz+1]) )
    sosaq[g]['Qemis_day'] = stats.nanmean(sosa[g]['Qemis_int']['raw'][day_ind])
    sosaq[g]['Qchem_day'] = stats.nanmean(sosa[g]['Qchem_int']['raw'][day_ind])
    sosaq[g]['Qdepo_day'] = stats.nanmean(sosa[g]['Qdepo_int']['raw'][day_ind])
    sosaq[g]['Qturb_day'] = stats.nanmean(sosa[g]['Qturb_int']['raw'][day_ind])
    pos = 0.0
    neg = 0.0
    pos = sosaq[g]['Qemis_day']
    neg = sosaq[g]['Qdepo_day']
    if sosaq[g]['Qchem_day'] >= 0:
      pos += sosaq[g]['Qchem_day']
    else:
      neg += sosaq[g]['Qchem_day']
    if sosaq[g]['Qturb_day'] >= 0:
      pos += sosaq[g]['Qturb_day']
    else:
      neg += sosaq[g]['Qturb_day']
    sosaq[g]['Qmax_day'] = np.amax( [pos, -neg] )
    # sosaq[g]['Qmax_day'] = np.amax([abs(sosaq[g]['Qemis_day']), abs(sosaq[g]['Qchem_day']),
    #   abs(sosaq[g]['Qdepo_day']), abs(sosaq[g]['Qturb_day'])])

  for tw in ['all', 'day', 'nit']:
    print('Ploting {0} ...'.format(tw))
    fg, ax = plt.subplots(1, 1, figsize=figsize, dpi=dpi)
    # for ig, g in enumerate(sosa['gas_list']):
    for ig, g in enumerate(gas_list):
      print('Ploting contributions of {0} ...'.format(g))

      ##### Plot the ratio between dC and C
      # ax.plot(ig+0.5, sosaq[g]['Qmax_'+tw]/1.0e6/np.sum(ddz[1:ncz+1])*1800.0 / sosaq[g]['conc_'+tw], 'ko')

      # balance_int = sosa[g]['Qconc_int'] - (sosa[g]['Qturb_int']+sosa[g]['Qemis_int']+sosa[g]['Qchem_int']+sosa[g]['Qdepo_int'])

      Qemis_frac = sosaq[g]['Qemis_'+tw] / sosaq[g]['Qmax_'+tw]
      Qchem_frac = sosaq[g]['Qchem_'+tw] / sosaq[g]['Qmax_'+tw]
      Qturb_frac = sosaq[g]['Qturb_'+tw] / sosaq[g]['Qmax_'+tw]
      Qdepo_frac = sosaq[g]['Qdepo_'+tw] / sosaq[g]['Qmax_'+tw]

      # Output accurate values for Q
      print(tw, ig, g, Qemis_frac, Qchem_frac, Qturb_frac, Qdepo_frac)

      # ax.bar(ig, Qemis_frac, bw, color=ct['emis'], label='Qemis')
      # ax.bar(ig+bw, Qchem_frac, bw, color=ct['chem'], label='Qchem')
      # ax.bar(ig+2*bw, Qturb_frac, bw, color=ct['turb'], label='Qturb')
      # ax.bar(ig+3*bw, Qdepo_frac, bw, color=ct['depo'], label='Qdepo')

      ax.bar(ig, Qemis_frac, bw, color=ct['emis'], label='Qemis')
      ax.bar(ig, Qdepo_frac, bw, color=ct['depo'], label='Qdepo')
      print(tw, g, Qchem_frac, Qturb_frac, Qemis_frac, Qdepo_frac)
      if Qchem_frac >= 0:
        ax.bar(ig, Qchem_frac, bw, color=ct['chem'], bottom=Qemis_frac, label='Qchem')
      else:
        ax.bar(ig, Qchem_frac, bw, color=ct['chem'], bottom=Qdepo_frac, label='Qchem')

      if Qturb_frac >= 0:
        if Qchem_frac >= 0:
          ax.bar(ig, Qturb_frac, bw, color=ct['turb'], bottom=Qemis_frac+Qchem_frac, label='Qturb')
        else:
          ax.bar(ig, Qturb_frac, bw, color=ct['turb'], bottom=Qemis_frac, label='Qturb')
      else:
        if Qchem_frac >= 0:
          ax.bar(ig, Qturb_frac, bw, color=ct['turb'], bottom=Qdepo_frac, label='Qturb')
        else:
          ax.bar(ig, Qturb_frac, bw, color=ct['turb'], bottom=Qdepo_frac+Qchem_frac, label='Qturb')

      # ax.set_xlim(0,28)
      # ax.set_ylim(-1,1)
      # ax.set_xticks(ind+width)
      ax.set_xticks(np.arange(ng)+bw/2.0)
      ax.set_xticklabels(gas_list)
      plt.setp(ax.xaxis.get_majorticklabels(), rotation=30, ha='right', fontsize=18)

      # ax.grid(True)

      ##### Put the legend box outside
      if ig==0:
        alpha = 1.0
        p_emis = mpatches.Patch(color=ct['emis'], linewidth=0, alpha=alpha)
        p_chem = mpatches.Patch(color=ct['chem'], linewidth=0, alpha=alpha)
        p_turb = mpatches.Patch(color=ct['turb'], linewidth=0, alpha=alpha)
        p_depo = mpatches.Patch(color=ct['depo'], linewidth=0, alpha=alpha)
        lgd = fg.legend([p_emis, p_chem, p_turb, p_depo], ['Qemis', 'Qchem', 'Qturb', 'Qdepo'],
          bbox_to_anchor=(0.09, 0.95, 0.5, 0.2), loc=3, ncol=4, mode='expand', prop={'size': 16})
        # handles,labels = ax.get_legend_handles_labels()
        # ax.legend(handles, labels, loc='best', scatterpoints=1)

    print(prop['file_name'], tw)
    # fg.subplots_adjust(hspace=0.1)
    fg.savefig('{0}/{1}_{2}.png'.format(save_dir, prop['file_name'], tw), dpi=dpi, bbox_extra_artists=(lgd,), bbox_inches='tight')
