import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import matplotlib.font_manager as mfm
import matplotlib.cm as cm


DPI = 150

plt.style.use('seaborn-talk')

#
# Set the font properties (can use more variables for more fonts)
#

# General font
# font_path = 'C:\Windows\Fonts\AGaramondPro-Regular.otf'
all_font = mfm.FontProperties(size=16)

# Tick labels
ticklb_font = mfm.FontProperties(size=16)

# Axis labels
axislb_font = mfm.FontProperties(size=18)

# Subtitle
subtitle_font = mfm.FontProperties(size=18)

# Title
title_font = mfm.FontProperties(size=20)

# Set default values
mpl.rcParams['axes.titlesize'  ] = 24
mpl.rcParams['axes.labelsize'  ] = 22
mpl.rcParams['lines.linewidth' ] =  2
mpl.rcParams['lines.markersize'] = 10
mpl.rcParams['xtick.labelsize' ] = 20
mpl.rcParams['ytick.labelsize' ] = 20
mpl.rcParams['legend.fontsize' ] = 20

# axis settings
axis_label = { \
  'h': r'$h$ (m)', \
  'time': r'time', \
  'u': r'$u$ (m s$^{-1}$)', \
  'v': r'$v$ (m s$^{-1}$)', \
  'theta': r'$\theta$ ($^{\circ}$C)', \
  'wtheta': r"$\overline{w'\theta'}$ (K m s$^{-1}$)", \
  'uw': r"$\overline{u'w'}$ (m$^2$ s$^{-2}$)", \
  }

axis_lim = { \
  'h': (0, 3000), \
  'u': (0, 20), \
  'v': (-5, 8), \
  }

axis_ticks = { \
  'h': np.arange(0, 3001, 500), \
  }

DPI = 150

# Helper functions
def find_nearest_value(array, value):
  idx = (np.abs(array-value)).argmin()
  return idx


def find_nearest_values(array, values):
  idxs = []
  for v in values:
    idxs.append(find_nearest_value(array, v))

  return idxs


# Plot functions
def plot_nxy(time, data, color, label=None, \
  xlim=None, xlabel=None, \
  ylim=None, ylabel=None, \
  title=None, \
  save_dir='.', fname='temp', ax=None):
  # Create fg and ax
  save_fig = False
  if ax is None:
    fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=DPI)
    save_fig = True

  # Plot line
  n = len(time)
  for i in range(n):
    if label is None:
      ax.plot(time[i], data[i], linewidth=2, color=color[i])
    else:
      ax.plot(time[i], data[i], linewidth=2, color=color[i], label=label[i])

  # Set limits
  if xlim is not None:
    ax.set_xlim(xlim)
  if ylim is not None:
    ax.set_ylim(ylim)

  # Set formatter of ticks
  formatter = tck.ScalarFormatter()
  formatter.set_powerlimits((-3,4))
  ax.yaxis.set_major_formatter(formatter)

  # Set labels
  if xlabel is not None:
    ax.set_xlabel(xlabel)
  if ylabel is not None:
    ax.set_ylabel(ylabel)

  # Set title
  if title is not None:
    ax.set_title(title)

  # Show legends
  if label is not None:
    handles, labels = ax.get_legend_handles_labels()
    # lgd = ax.legend(handles, labels, scatterpoints=1, bbox_to_anchor=(1, 1), loc='upper left', ncol=1)
    lgd = ax.legend(handles, labels, scatterpoints=1, loc='best', ncol=1)
  else:
    lgd = None

  # Show grids for major and minor ticks
  ax.grid(axis='both', which='major', linewidth=1)
  ax.grid(axis='both', which='minor', linewidth=1, alpha=0.4)

  # Save the figure
  if save_fig:
    # Tight layout
    fg.tight_layout()

    if lgd is not None:
      fg.savefig('{0}/{1}'.format(save_dir, fname), dpi=DPI, bbox_extra_artists=(lgd,), bbox_inches='tight')
    else:
      fg.savefig('{0}/{1}'.format(save_dir, fname), dpi=DPI, bbox_inches='tight')


def plot_prf_in_one(var, tt, zz, pt, \
  xlim=None, xticks=None, xlabel='x', \
  ylim=None, yticks=None, ylabel='y', \
  title='Day', save_dir='.', fname='plot_prf_in_one.png'):

  """
  " Plot profiles at different time points in one figure.
  " var: (nt, nz)
  " hh: (nz,), [m]
  " tt: (nt,), [day]
  " pt: picked time points, (np,), [s]
  """

  # Get dimensions
  nt, nz = var.shape
  npt = pt.size
  
  # Get corresponding indices from time array, only the first occurence is returned for each value
  pind = find_nearest_values(tt, pt)

  #
  # Plot
  #
  fg, ax = plt.subplots(1, 1, figsize=(12, 12), dpi=DPI)

  # Plot profiles one by one
  ls = ['-', '--', '-.', ':', 'o', '+']
  ms = ['o', 'v', '^', 's', 'X', '*']
  # cs = ['#d73027', '#fc8d59', '#fee090', '#e0f3f8', '#91bfdb', '#4575b4']
  # cs = ['#a50026', '#d73027', '#f46d43', '#74add1', '#4575b4', '#313695']
  # cs = ['#d53e4f', '#fc8d59', '#fee08b', '#e6f598', '#99d594', '#3288bd']
  # cmap_now = plt.cm.get_cmap('gist_ncar', 6)
  # cmap_array = cmap_now(np.arange(6))
  # plt.rc( 'axes', prop_cycle=( cycler('color', cmap_now(np.arange(6))) ) )
  # with plt.style.context(('seaborn-poster')):
  for i, t in enumerate(pt):
    ind = pind[i]
    # ax.plot(var[ind, :], zz, marker=ms[i], color=cs[i], label='{0:3.0f} h'.format(t*24 % 24))
    ax.plot(var[ind, :], zz, marker=ms[i], label='{0:3.0f} h'.format(t*24 % 24))
    # ax.plot(var[ind, :], zz, label='{0:3.0f} h'.format(t*24 % 24))

  # Set major ticks for x and y
  if xticks is not None:
    ax.set_xticks(xticks) 
  if yticks is not None:
    ax.set_yticks(yticks) 

  # Show minor ticks for x and y
  xminor_locator = tck.AutoMinorLocator(2)
  ax.xaxis.set_minor_locator(xminor_locator)
  yminor_locator = tck.AutoMinorLocator(2)
  ax.yaxis.set_minor_locator(yminor_locator)

  # Set font properties for tick labels
  # for label in (ax[i].get_xticklabels() + ax[i].get_yticklabels()):
  #   label.set_fontproperties(ticklb_font)
  #   label.set_fontsize(14) # Size here overrides font_prop

  # Set xlim and ylim
  if xlim is not None:
    ax.set_xlim(xlim)
  if ylim is not None:
    ax.set_ylim(ylim)

  # Show grids for major and minor ticks
  ax.grid(axis='both', which='major', linewidth=1)
  ax.grid(axis='both', which='minor', linewidth=1, alpha=0.4)

  # Set xlabel and ylabel
  ax.set_xlabel(xlabel)
  ax.set_ylabel(ylabel)
  # ax[i].set_ylabel(ylabel, rotation=0, va='bottom', fontproperties=axislb_font)

  # Show legends
  handles, labels = ax.get_legend_handles_labels()
  lgd = ax.legend(handles, labels, scatterpoints=1, loc='best', ncol=1)

  # Set the title
  ax.set_title( title )

  fg.savefig('{0}/{1}'.format(save_dir, fname), dpi=DPI)


def plot_contourf(time, h, var, \
  save_dir='.', fname='temp', \
  title='Z', xlim=None, ylim=None, zlim=None, levels=None):
  """
  " var: (h, time)
  """

  # Get 2D arrays X and Y
  X, Y = np.meshgrid(time, h)
  Z = var

  # Set vmin and vmax
  zmin = np.nanmin(Z)
  zmax = np.nanmax(Z)

  if zlim is None:
    vmin = zmin
    vmax = zmax
  else:
    vmin = zlim[0]
    vmax = zlim[1]

  # Plot
  fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=DPI)

  # h_ims = ax.imshow(Z, interpolation='none', extent=[1, 6, 0, 3000])

  # Set formatter of colorbar ticklabels
  formatter = tck.ScalarFormatter()
  formatter.set_powerlimits((-3,4))
  # cbar.ax.yaxis.set_major_formatter(formatter)

  # cs = plt.contourf(X, Y, Z, interpolation='none')
  # cmap = mpl.colors.ListedColormap(['#d53e4f', '#fee08b', '#99d594', '#3288bd'])
  # bounds = [-999, 0, 0.25, 1, 1e8]
  # norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
  # cs = ax.pcolor(X, Y, Z, vmin=vmin, vmax=vmax, cmap=cmap, norm=norm)
  # cs = ax.pcolor(X, Y, Z, vmin=vmin, vmax=vmax, cmap=plt.get_cmap('seismic'))
  # cs = ax.pcolor(X, Y, Z, vmin=vmin, vmax=vmax, cmap=plt.get_cmap('viridis', 10))
  if levels is None:
    cs = ax.contourf(X, Y, Z, vmin=vmin, vmax=vmax, cmap=plt.get_cmap('viridis'))
    # cs = ax.contourf(X, Y, Z, vmin=vmin, vmax=vmax, cmap=plt.cm.viridis)
    cbar = plt.colorbar(cs, format=formatter)
  else:
    nlevels = len(levels)
    colors = plt.get_cmap('viridis', nlevels-1)(np.arange(nlevels-1))
    # cs = ax.contourf(X, Y, Z, levels=levels, interpolation='none', colors=colors, cmap=plt.get_cmap('viridis'))
    cs = ax.contourf(X, Y, Z, levels=levels, colors=colors)
    cbar = plt.colorbar(cs, ticks=levels, format=formatter)
    cbar.ax.set_yticklabels(['{:.2f}'.format(i) for i in levels])

  # ax.set_aspect('equal')

  # Set major ticks for x and y
  xticks = np.arange( time[0], time[-1]+0.1, 0.5 )
  ax.set_xticks(xticks)
  yticks = np.arange( 0, 3001, 500 )
  ax.set_yticks(yticks)

  # Set font properties for tick labels
  for label in (ax.get_xticklabels() + ax.get_yticklabels()):
    label.set_fontproperties(ticklb_font)
    # label.set_fontsize(14) # Size here overrides font_prop

  # Set xlim and ylim
  if xlim is None:
    ax.set_xlim(0, 5)
  else:
    ax.set_xlim(xlim)
  if ylim is None:
    ax.set_ylim(0, 3000)
  else:
    ax.set_ylim(ylim)

  # Set xlabel and ylabel
  ax.set_xlabel('Day (d)', fontproperties=axislb_font)
  ax.set_ylabel(r'$h$ (m)', fontproperties=axislb_font)

  # Set title
  ax.set_title('{0} ({1:.2f}, {2:.2f})'.format(title, zmin, zmax), fontproperties=subtitle_font)

  # Set colorbar font size
  cbar.ax.tick_params(labelsize=16)

  # Save figure
  fg.savefig('{0}/{1}.png'.format(save_dir, fname), dpi=DPI)


def plot_meteorology(data, save_dir, prefix):
  # u profile
  plot_prf_in_one( data['u'], data['time'], data['h'], np.arange(0.0, 1.0, 1.0/6.0), \
    xlim=(0, 16), xticks=np.arange(0, 17, 2), xlabel=axis_label['u'], \
    ylim=axis_lim['h'], yticks=axis_ticks['h'], ylabel=axis_label['h'], \
    title=prefix + ': Day 1', save_dir=save_dir, fname=prefix+'_u_profile_day1.png')
  
  plot_prf_in_one( data['u'], data['time'], data['h'], np.arange(4.0, 5.0, 1.0/6.0), \
    xlim=(0, 14), xticks=np.arange(0, 15, 2), xlabel=axis_label['u'], \
    ylim=axis_lim['h'], yticks=axis_ticks['h'], ylabel=axis_label['h'], \
    title=prefix + ': Day 5', save_dir=save_dir, fname=prefix+'_u_profile_day5.png')
  
  # v profile
  plot_prf_in_one( data['v'], data['time'], data['h'], np.arange(0.0, 1.0, 1.0/6.0), \
    xlim=(-4, 8), xticks=np.arange(-4, 9, 2), xlabel=axis_label['v'], \
    ylim=axis_lim['h'], yticks=axis_ticks['h'], ylabel=axis_label['h'], \
    title=prefix + ': Day 1', save_dir=save_dir, fname=prefix+'_v_profile_day1.png')
  
  plot_prf_in_one( data['v'], data['time'], data['h'], np.arange(4.0, 5.0, 1.0/6.0), \
    xlim=(-2, 5), xticks=np.arange(-1, 4, 1), xlabel=axis_label['v'], \
    ylim=axis_lim['h'], yticks=axis_ticks['h'], ylabel=axis_label['h'], \
    title=prefix + ': Day 5', save_dir=save_dir, fname=prefix+'_v_profile_day5.png')
  
  # theta profile
  thetaC = data['theta'] - 273.15

  xlim = [15, 30]
  
  plot_prf_in_one( thetaC, data['time'], data['h'], np.arange(0.0, 1.0, 1.0/6.0), \
    xlim=xlim, xticks=np.arange(xlim[0], xlim[1]+1, 2), xlabel=axis_label['theta'], \
    ylim=axis_lim['h'], yticks=axis_ticks['h'], ylabel=axis_label['h'], \
    title=prefix + ': Day 1', save_dir=save_dir, fname=prefix+'_theta_profile_day1.png')
  
  plot_prf_in_one( thetaC, data['time'], data['h'], np.arange(4.0, 5.0, 1.0/6.0), \
    xlim=xlim, xticks=np.arange(xlim[0], xlim[1]+1, 2), xlabel=axis_label['theta'], \
    ylim=axis_lim['h'], yticks=axis_ticks['h'], ylabel=axis_label['h'], \
    title=prefix + ': Day 5', save_dir=save_dir, fname=prefix+'_theta_profile_day5.png')

  #
  # Contours of Km, Kh, Ri
  #
  plot_contourf(data['time'], data['hm'], data['Km'].transpose(), \
    save_dir=save_dir, fname=prefix+'_Km_ct', title=prefix + ': Km', zlim=None)
  
  plot_contourf(data['time'], data['hm'], data['Kh'].transpose(), \
    save_dir=save_dir, fname=prefix+'_Kh_ct', title=prefix + ': Kh', zlim=None)
  
  plot_contourf(data['time'], data['hm'], data['Ri'].transpose(), \
    save_dir=save_dir, fname=prefix+'_Ri_ct', title=prefix + ': Ri', levels=[-1, 0, 0.25, 1, 100, 200, 300])

  #
  # Fluxes
  #
  
  # Heat flux: w_theta = -K dtheta/dz
  xlim = [-0.1, 0.05]

  plot_prf_in_one( data['wtheta'], data['time'], data['hm'], np.arange(0.0, 1.0, 1.0/6.0), \
    xlim=xlim, xticks=np.arange(xlim[0], xlim[1]+0.05, 0.05), xlabel=axis_label['wtheta'], \
    ylim=axis_lim['h'], yticks=axis_ticks['h'], ylabel=axis_label['h'], \
    title=prefix + ': Day 1', save_dir=save_dir, fname=prefix+'_wtheta_profile_day1.png')
  
  plot_prf_in_one( data['wtheta'], data['time'], data['hm'], np.arange(4.0, 5.0, 1.0/6.0), \
    xlim=xlim, xticks=np.arange(xlim[0], xlim[1]+0.05, 0.05), xlabel=axis_label['wtheta'], \
    ylim=axis_lim['h'], yticks=axis_ticks['h'], ylabel=axis_label['h'], \
    title=prefix + ': Day 5', save_dir=save_dir, fname=prefix+'_wtheta_profile_day5.png')

  # Momentum flux: u_w = -K du/dz
  xlim = [-0.4, 0.1]
  dx = 0.1
  dxmin = dx*0.5

  plot_prf_in_one( data['uw'], data['time'], data['hm'], np.arange(0.0, 1.0, 1.0/6.0), \
    xlim=xlim, xticks=np.arange(xlim[0], xlim[1]+dxmin, dx), xlabel=axis_label['uw'], \
    ylim=axis_lim['h'], yticks=axis_ticks['h'], ylabel=axis_label['h'], \
    title=prefix + ': Day 1', save_dir=save_dir, fname=prefix+'_uw_profile_day1.png')
  
  plot_prf_in_one( data['uw'], data['time'], data['hm'], np.arange(4.0, 5.0, 1.0/6.0), \
    xlim=xlim, xticks=np.arange(xlim[0], xlim[1]+dxmin, dx), xlabel=axis_label['uw'], \
    ylim=axis_lim['h'], yticks=axis_ticks['h'], ylabel=axis_label['h'], \
    title=prefix + ': Day 5', save_dir=save_dir, fname=prefix+'_uw_profile_day5.png')


def plot_chemistry(data, save_dir, prefix):
  # Contours of number concentrations of iso, alp, ELVOC, OH, HO2, H2SO4
  plot_contourf(data['time'], data['h'], data['iso'].transpose(), \
    save_dir=save_dir, fname=prefix + '_iso_ct', title='isoprene', xlim=(3, 5), ylim=(0, 1000))
  
  plot_contourf(data['time'], data['h'], data['alp'].transpose(), \
    save_dir=save_dir, fname=prefix + '_alp_ct', title=r'$\alpha$-pinene', xlim=(3, 5), ylim=(0, 1000))

  plot_contourf(data['time'], data['h'], data['ELVOC'].transpose(), \
    save_dir=save_dir, fname=prefix + '_ELVOC_ct', title='ELVOC', xlim=(3, 5), ylim=(0, 1000))

  plot_contourf(data['time'], data['h'], data['OH'].transpose(), \
    save_dir=save_dir, fname=prefix + '_OH_ct', title='OH', xlim=(3, 5))

  plot_contourf(data['time'], data['h'], data['HO2'].transpose(), \
    save_dir=save_dir, fname=prefix + '_HO2_ct', title='HO2', xlim=(3, 5))

  plot_contourf(data['time'], data['h'], data['H2SO4'].transpose(), \
    save_dir=save_dir, fname=prefix + '_H2SO4_ct', title='H2SO4', xlim=(3, 5))

  #===== Time series =====#
  time = data['time']
  xlim = [3, 5]  # only plot day 3 to 5
  xlabel = 'Day'

  # OH, HO2, H2SO4, ELVOC at level 2 in one plot
  fg, ax = plt.subplots(2, 2, figsize=(12, 12), dpi=DPI)

  gas = 'OH'
  plot_nxy([time, time], [data[gas][:, 1], data[gas][:, 5]], ['r', 'b'], ['10 m', '50 m'], \
    xlim=xlim, xlabel=xlabel, \
    ylabel=r'concentration (molec cm$^{-3}$)', \
    title=gas, save_dir=save_dir, fname=prefix + '_' + gas + '_ts.png', ax=ax[0,0])

  gas = 'HO2'
  plot_nxy([time, time], [data[gas][:, 1], data[gas][:, 5]], ['r', 'b'], label=None, \
    xlim=xlim, xlabel=xlabel, \
    ylabel=r'concentration (molec cm$^{-3}$)', \
    title=gas, save_dir=save_dir, fname=prefix + '_' + gas + '_ts.png', ax=ax[0,1])

  gas = 'H2SO4'
  plot_nxy([time, time], [data[gas][:, 1], data[gas][:, 5]], ['r', 'b'], label=None, \
    xlim=xlim, xlabel=xlabel, \
    ylabel=r'concentration (molec cm$^{-3}$)', \
    title=gas, save_dir=save_dir, fname=prefix + '_' + gas + '_ts.png', ax=ax[1,0])

  gas = 'ELVOC'
  plot_nxy([time, time], [data[gas][:, 1], data[gas][:, 5]], ['r', 'b'], label=None, \
    xlim=xlim, xlabel=xlabel, \
    ylabel=r'concentration (molec cm$^{-3}$)', \
    title=gas, save_dir=save_dir, fname=prefix + '_' + gas + '_ts.png', ax=ax[1,1])

  fg.tight_layout()
  fg.savefig('{0}/{1}.png'.format(save_dir, prefix + '_OH_HO2_H2SO4_ELVOC'), dpi=DPI, bbox_inches='tight')

  # alpha-pinene, isoprene at level 2 (10 m) and level 6 (50 m)
  fg, ax = plt.subplots(1, 2, figsize=(12, 8), dpi=DPI)

  gas = 'alp'
  plot_nxy([time, time], [data[gas][:, 1], data[gas][:, 5]], ['r', 'b'], [r'10 m', r'50 m'], \
    xlim=xlim, xlabel=xlabel, \
    ylabel=r'concentration (molec cm$^{-3}$)', \
    title=r'$\alpha$-pinene', save_dir=save_dir, fname=prefix + '_' + gas + '_ts.png', ax=ax[0])

  gas = 'iso'
  plot_nxy([time, time], [data[gas][:, 1], data[gas][:, 5]], ['r', 'b'], label=None, \
    xlim=xlim, xlabel=xlabel, \
    ylabel=r'concentration (molec cm$^{-3}$)', \
    title='isoprene', save_dir=save_dir, fname=prefix + '_' + gas + '_ts.png', ax=ax[1])

  fg.tight_layout()
  fg.savefig('{0}/{1}.png'.format(save_dir, prefix + '_alp_iso'), dpi=DPI, bbox_inches='tight')

  # OH, HO2, H2SO4, ELVOC at level
  # for gas, ig in zip(['OH', 'HO2', 'H2SO4', 'ELVOC'], [2, 7, 20, 24]):
  #   fg, ax = plt.subplots(1, 1, figsize=(12, 8), dpi=DPI)
  #   plot_nxy([time,], [data[gas][:, ig],], ['r',], ['10 m',], \
  #     xlim=xlim, xlabel=xlabel, \
  #     ylabel=r'concentration (molec cm$^{-3}$)', \
  #     title=gas, save_dir=save_dir, fname=gas + '_ts.png', ax=ax)

  #   fg.tight_layout()
  #   fg.savefig('{0}/{1}.png'.format(save_dir, gas), dpi=DPI, bbox_inches='tight')

  # surface temperature, exp_coszen
  fg, ax = plt.subplots(2, 1, figsize=(12, 12), dpi=DPI)

  plot_nxy([time, ], [data['theta'][:, 0], ], ['r', ], \
    xlim=(0, 5), xlabel=xlabel, \
    ylabel='surface temperature (K)', \
    save_dir=save_dir, fname='surf_temp.png', ax=ax[0])

  # plot_nxy([time, ], [data['exp_coszen'], ], ['r', ], \
  #   xlim=(0, 5), xlabel=xlabel, \
  #   ylabel='exp_coszen', \
  #   save_dir=save_dir, fname='exp_coszen.png', ax=ax[1])

  fg.tight_layout()
  fg.savefig('{0}/surf_temp_exp_coszen.png'.format(save_dir), dpi=DPI, bbox_inches='tight')

  # Emission rates of isoprene and alpha-pinene
  plot_nxy([time, time], [data['emi_iso'], data['emi_alp']], \
    ['r', 'b'], ['isoprene', r'$\alpha$-pinene'], \
    xlim=xlim, \
    xlabel=xlabel, ylabel=r'Emission rate (molec cm$^{-3}$ s$^{-1}$)', \
    save_dir=save_dir, fname='emi.png')


  # Nair, O3 at level 2 in one plot
  # xlim = [0, 5]

  # fg, ax = plt.subplots(1, 2, figsize=(12, 12), dpi=DPI)

  # gas = 'Nair'
  # plot_nxy([time, time], [data[gas][:, 1], data[gas][:, 5]], ['r', 'b'], ['10 m', '50 m'], \
  #   xlim=xlim, xlabel=xlabel, \
  #   ylabel=r'concentration (molec cm$^{-3}$)', \
  #   title=gas, save_dir=save_dir, fname=prefix + '_' + gas + '_ts.png', ax=ax[0])

  # gas = 'O3'
  # plot_nxy([time, time], [data[gas][:, 1], data[gas][:, 5]], ['r', 'b'], label=None, \
  #   xlim=xlim, xlabel=xlabel, \
  #   ylabel=r'concentration (molec cm$^{-3}$)', \
  #   title=gas, save_dir=save_dir, fname=prefix + '_' + gas + '_ts.png', ax=ax[1])

  # fg.tight_layout()
  # fg.savefig('{0}/{1}.png'.format(save_dir, prefix + '_Nair_O3'), dpi=DPI, bbox_inches='tight')


def plot_aerosol(data, save_dir, prefix):
  #====================================#
  # Plot total particle number concentration, total particle mass and volume concentration
  #====================================#
  time = data['time']
  xlim = [3, 5]  # only plot day 3 to 5
  xlabel = 'Day'

  fg, ax = plt.subplots(3, 1, figsize=(12, 12), dpi=DPI)

  plot_nxy([time, time], [data['tpn'][:, 1], data['tpn'][:, 5]], ['r', 'b'], ['10 m', '50 m'], \
    xlim=xlim, xlabel=xlabel, \
    ylabel=r'Total PN [molec cm$^{-3}$]', \
    title='tpn', save_dir=save_dir, fname='tpn.png', ax=ax[0])

  plot_nxy([time, time], [data['tpm'][:, 1], data['tpm'][:, 5]], ['r', 'b'], ['10 m', '50 m'], \
    xlim=xlim, xlabel=xlabel, \
    ylabel=r'Total PM [$\mu$g cm$^{-3}$]', \
    title='tpm', save_dir=save_dir, fname='tpm.png', ax=ax[1])

  plot_nxy([time, time], [data['tpv'][:, 1], data['tpv'][:, 5]], ['r', 'b'], ['10 m', '50 m'], \
    xlim=xlim, xlabel=xlabel, \
    ylabel=r'Total PV [$\mu$g m$^{-3}$]', \
    title='tpv', save_dir=save_dir, fname='tpv.png', ax=ax[2])

  fg.tight_layout()
  fg.savefig('{0}/{1}.png'.format(save_dir, prefix + '_tpn_tpm_tpv'), dpi=DPI, bbox_inches='tight')

  #====================================#
  # N ~ diameter
  #====================================#
  # # Create fg and ax
  fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=DPI)

  x = data['d_par']*1.0e9
  y = data['nconc_par'][-1, 1, :]*1.0e-6  # [molec cm-3]
  print(x)
  print(y)

  ax.plot(x, y)

  ax.set_xscale('log')
  ax.set_yscale('log')

  ax.set_yticks([1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5])

  ax.set_xlim([1.0e0, 1.0e3])
  ax.set_ylim([1.0, 1.0e6])

  # Show grids for major and minor ticks
  ax.grid(axis='both', which='major', linewidth=1)
  ax.grid(axis='both', which='minor', linewidth=1, alpha=0.4)

  # Save the figure
  fname = 'N_diameter.png'
  fg.tight_layout()
  fg.savefig('{0}/{1}'.format(save_dir, fname), dpi=DPI, bbox_inches='tight')

  #====================================#
  # PM ~ time
  #====================================#
  fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=DPI)

  x = data['time']
  y = data['tpm']

  ax.plot(x[73:], y[73:, 1])

  # Show grids for major and minor ticks
  ax.grid(axis='both', which='major', linewidth=1)
  ax.grid(axis='both', which='minor', linewidth=1, alpha=0.4)

  # Save the figure
  fname = 'PM_time.png'
  fg.tight_layout()
  fg.savefig('{0}/{1}'.format(save_dir, fname), dpi=DPI, bbox_inches='tight')

  #====================================#
  # PN ~ time
  #====================================#
  fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=DPI)

  x = data['time']
  y = data['tpn']*1.0e-6  # [molec cm-3]

  ax.plot(x[73:], y[73:, 1])

  # Show grids for major and minor ticks
  ax.grid(axis='both', which='major', linewidth=1)
  ax.grid(axis='both', which='minor', linewidth=1, alpha=0.4)

  # Save the figure
  fname = 'PN_time.png'
  fg.tight_layout()
  fg.savefig('{0}/{1}'.format(save_dir, fname), dpi=DPI, bbox_inches='tight')

  #====================================#
  # CS_H2SO4 ~ time
  #====================================#
  # fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=DPI)

  # x = data['time']
  # y = data['cs_H2SO4']  # [s-1]

  # ax.plot(x[73:], y[73:, 1])

  # # Show grids for major and minor ticks
  # ax.grid(axis='both', which='major', linewidth=1)
  # ax.grid(axis='both', which='minor', linewidth=1, alpha=0.4)

  # # Save the figure
  # fname = 'CS_H2SO4_time.png'
  # fg.tight_layout()
  # fg.savefig('{0}/{1}'.format(save_dir, fname), dpi=DPI, bbox_inches='tight')

  #====================================#
  # Diameter ~ time ~ N
  #====================================#
  fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=DPI)

  x = data['time']
  y = data['d_par']
  z = data['nconc_par']*1.0e-6

  cs = ax.pcolor(x, y, np.log10(z[:, 1, :].transpose()), vmin=0.0, cmap=plt.cm.jet)
  cbar = plt.colorbar(cs)

  ax.set_xlim([3, 5])

  ax.set_yscale('log')

  # Save the figure
  fname = 'banana.png'
  fg.tight_layout()
  fg.savefig('{0}/{1}'.format(save_dir, fname), dpi=DPI, bbox_inches='tight')

  #====================================#
  # Contour of TPN
  #====================================#
  fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=DPI)

  x = data['time'][73:]
  y = data['h']
  z = data['tpn'][73:, :]

  cs = ax.pcolor(x, y, z.transpose(), cmap=plt.cm.jet)
  cbar = plt.colorbar(cs)

  # ax.set_xlim([3, 5])
  # ax.set_xscale('log')

  fname = 'tpn_ct.png'
  fg.tight_layout()
  fg.savefig('{0}/{1}'.format(save_dir, fname), dpi=DPI, bbox_inches='tight')

  #====================================#
  # Contour of TPM
  #====================================#
  fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=DPI)

  x = data['time'][73:]
  y = data['h']
  z = data['tpm'][73:, :]

  cs = ax.pcolor(x, y, z.transpose(), vmin=1.2, cmap=plt.cm.jet)
  cbar = plt.colorbar(cs)

  # ax.set_xlim([3, 5])
  # ax.set_xscale('log')

  fname = 'tpm_ct.png'
  fg.tight_layout()
  fg.savefig('{0}/{1}'.format(save_dir, fname), dpi=DPI, bbox_inches='tight')

  #====================================#
  # Profile of TPN and TPM
  #====================================#
  fg, ax = plt.subplots(1, 2, figsize=(12, 9), dpi=DPI)

  x1 = data['tpn'][-1, :]
  y1 = data['h']
  x2 = data['tpm'][-1, :]
  y2 = data['h']

  ax[0].plot(x1, y1)
  ax[1].plot(x2, y2)

  # cs = ax.pcolor(x, y, z.transpose(), vmin=1.2, cmap=plt.cm.jet)
  # cbar = plt.colorbar(cs)

  ax[0].set_xlabel('tpn [# cm-3]')
  ax[1].set_xlabel('tpm [ug m-3]')
  # ax[1].set_xlim([3, 5])
  # ax.set_xscale('log')

  fname = 'tpn_tpm_prf.png'
  fg.tight_layout()
  fg.savefig('{0}/{1}'.format(save_dir, fname), dpi=DPI, bbox_inches='tight')


def plot_prf_hrz(time, h, var, ptime, \
  save_dir='.', fname='temp', \
  xlim=None, xticks=None, ylim=None, yticks=None, \
  xlabel='x', ylabel='y'):
  """
  " Plot profiles with several axes placed horizontally.
  " time: (nt,), [s]
  " h: (nz,), [m]
  " var: (nt, nz)
  " ptime: [s], selected time to plot
  """

  # Get dimensions
  nt, nz = var.shape
  npt = ptime.size

  # Get corresponding indices from time array
  pind = find_nearest_values(time, ptime)

  # Compute automatic xlim as default
  if xlim is None:
    xmin = np.nanmin(var[pind, :])
    xmax = np.nanmax(var[pind, :])
    xrng = xmax - xmin
    fct = 0.1

    # Increase the plot range to include both minimum values and maximum values
    xlim = ( xmin - fct*xrng, xmax + fct*xrng )

  # Compute automatic ylim as default
  if ylim is None:
    ymin = np.nanmin(h)
    ymax = np.nanmax(h)
    yrng = ymax - ymin

    ylim = (ymin, ymax)

  # Plot
  fg, ax = plt.subplots(1, npt, figsize=(16, 8), dpi=DPI)
  for i, t in enumerate(ptime):
    ind = pind[i]

    ax[i].plot(var[ind, :], h, '-o')

    # Set major ticks for x and y
    if xticks is not None:
      ax[i].set_xticks(xticks) 
    if yticks is not None:
      ax[i].set_yticks(yticks) 

    # Show minor ticks for x and y
    xminor_locator = tck.AutoMinorLocator(2)
    ax[i].xaxis.set_minor_locator(xminor_locator)
    yminor_locator = tck.AutoMinorLocator(2)
    ax[i].yaxis.set_minor_locator(yminor_locator)

    # Set font properties for tick labels
    for label in (ax[i].get_xticklabels() + ax[i].get_yticklabels()):
      label.set_fontproperties(ticklb_font)
      # label.set_fontsize(14) # Size here overrides font_prop

    # Set xlim and ylim
    ax[i].set_xlim(xlim)
    ax[i].set_ylim(ylim)

    # Show grids for major and minor ticks
    ax[i].grid(axis='both', which='major', linewidth=1)
    ax[i].grid(axis='both', which='minor', linewidth=1, alpha=0.4)

    # Set xlabel and ylabel
    ax[i].set_xlabel(xlabel, fontproperties=axislb_font)
    # ax[i].set_ylabel(ylabel, rotation=0, va='bottom', fontproperties=axislb_font)
    ax[i].set_ylabel(ylabel, fontproperties=axislb_font)

    # Only show ylabel and yticklabels for the first plot
    if i > 0:
      ax[i].set_ylabel('')
      ax[i].yaxis.set_ticklabels([])

    # Set titles
    ax[i].set_title( 'Day {0:2.0f}, Hour {1:4.1f}'.format(int(t+1), t*24 % 24), fontproperties=subtitle_font )

  fg.savefig('{0}/{1}.png'.format(save_dir, fname), dpi=DPI)


# def plot_ts_vd(time, data, \
#   save_dir='.', fname='temp'):
# 
#   fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=DPI)
# 
#   ax.plot(time, data)
# 
#   fg.savefig('{0}/{1}.png'.format(save_dir, fname), dpi=DPI)


#---------------------------------------
# Real plot functions
#---------------------------------------
def plot_aerosol_box():
  
  dir_aerosol = '../../output/aerosol_box'
  data1 = read_data(dir_aerosol+'/nucl')
  data2 = read_data(dir_aerosol+'/nucl_cond')
  data3 = read_data(dir_aerosol+'/nucl_cond_coag')

  save_dir = dir_aerosol + '/figures'

  #====================================#
  # PN ~ time
  #====================================#
  fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=DPI)

  x = data1['time']
  y = data1['tpn_par']

  ax.plot(x, y)

  ax.set_ylim([0, 5.0e4])

  # Show grids for major and minor ticks
  ax.grid(axis='both', which='major', linewidth=1)
  ax.grid(axis='both', which='minor', linewidth=1, alpha=0.4)

  # Save the figure
  fname = 'PN_time.png'
  fg.tight_layout()
  fg.savefig('{0}/{1}'.format(save_dir, fname), dpi=DPI, bbox_inches='tight')

  #====================================#
  # N ~ diameter
  #====================================#
  # Create fg and ax
  fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=DPI)

  x1 = data1['d_par']*1.0e9
  y1 = data1['nconc_par'][-1, :]*1.0e-6

  x2 = data2['d_par']*1.0e9
  y2 = data2['nconc_par'][-1, :]*1.0e-6

  x3 = data3['d_par']*1.0e9
  y3 = data3['nconc_par'][-1, :]*1.0e-6

  ax.plot(x1, y1)
  ax.plot(x2, y2)
  ax.plot(x3, y3)

  ax.set_xscale('log')
  ax.set_yscale('log')

  ax.set_xlim([1.0e0, 1.0e3])
  ax.set_ylim([1.0e-6, 1.0e4])

  # Show grids for major and minor ticks
  ax.grid(axis='both', which='major', linewidth=1)
  ax.grid(axis='both', which='minor', linewidth=1, alpha=0.4)

  # Save the figure
  fname = 'N_diameter.png'
  fg.tight_layout()
  fg.savefig('{0}/{1}'.format(save_dir, fname), dpi=DPI, bbox_inches='tight')


def plot_extra(data, save_dir, prefix):
  #===== Time series =====#
  time = data['time']
  xlim = [0, 5]  # only plot day 3 to 5
  xlabel = 'Day'

  # C_L
  fg, ax = plt.subplots(1, 1, figsize=(12, 12), dpi=DPI)

  plot_nxy([time,], [data['C_L'], ], ['r', ], ['C_L', ], \
    xlim=xlim, xlabel=xlabel, \
    ylabel=r'C_L', \
    title='C_L', save_dir=save_dir, fname=prefix + '_CL_ts.png', ax=ax)

  fg.tight_layout()
  fg.savefig('{0}/{1}.png'.format(save_dir, prefix + '_CL'), dpi=DPI, bbox_inches='tight')

  # C_T
  fg, ax = plt.subplots(1, 1, figsize=(12, 12), dpi=DPI)

  plot_nxy([time,], [data['C_T'], ], ['r', ], ['C_T', ], \
    xlim=xlim, xlabel=xlabel, \
    ylabel=r'C_T', \
    title='C_T', save_dir=save_dir, fname=prefix + '_CT_ts.png', ax=ax)

  fg.tight_layout()
  fg.savefig('{0}/{1}.png'.format(save_dir, prefix + '_CT'), dpi=DPI, bbox_inches='tight')


def plot_deposition_tasks(data_full, data_full_nodep, data_full_nogasdep, data_full_nopardep, save_dir, prefix):
  time = data_full['time']

  #====================================#
  # PN ~ time
  #====================================#
  fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=DPI)

  # x = time
  # y = data1['tpn_par']*1.0e-6
  ind = 1

  ax.plot(time, data_full['tpn'][:, ind], 'k^', label='full')
  ax.plot(time, data_full_nodep['tpn'][:, ind], 'o', mew=2, mec='k', mfc='None', label='full_nodep')
  ax.plot(time, data_full_nogasdep['tpn'][:, ind], 'b', label='full_nogasdep')
  ax.plot(time, data_full_nopardep['tpn'][:, ind], 'r', label='full_nopardep')

  # ax.set_ylim([0, 5.0e4])

  # Show grids for major and minor ticks
  ax.grid(axis='both', which='major', linewidth=1)
  ax.grid(axis='both', which='minor', linewidth=1, alpha=0.4)

  # Show legends
  handles, labels = ax.get_legend_handles_labels()
  lgd = ax.legend(handles, labels, scatterpoints=1, loc='best', ncol=1)

  # Save the figure
  fname = 'PN_time.png'
  fg.tight_layout()
  fg.savefig('{0}/{1}'.format(save_dir, fname), dpi=DPI, bbox_inches='tight')
