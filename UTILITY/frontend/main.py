# Try to follow the python style guide from google: https://google.github.io/styleguide/pyguide.html

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, qApp
from PyQt5.QtWidgets import QDesktopWidget, QWidget, QGridLayout, QAction, QHBoxLayout, QVBoxLayout, QSplitter
from PyQt5.QtWidgets import QPushButton, QFileDialog, QFileSystemModel, QTreeView, QTabWidget
from PyQt5.QtWidgets import QTableWidget,QTableWidgetItem
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt

import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.cbook

import os
import sys
import warnings

import datetime
import numpy as np

import plot_functions as pf

# Dir dialog


class SosaaData():
  def __init__(self, fname):
    # Load the raw data
    self.raw = np.loadtxt(fname)
    
    # Get the raw data size info 
    self.nrow_raw, self.ncol_raw = self.raw.shape

    # Read data
    try:
      self.is_sosaa_data = True

      # Read data
      self.data = self.raw[:, 6:]
      self.nrow, self.ncol = self.data.shape
      
      # Read time
      self.time = np.array([ datetime.datetime(*(self.raw[i, 0:6].astype(int))) for i in range(self.nrow) ])

      # A column array or a matrix
      self.is1d = (self.ncol_raw == 7)
    except:
      print('Not a standard SOSAA output data, maybe an ECMWF file, or border file.')
      self.is_sosaa_data = False
      self.time = None
      self.data = None
      self.nrow, self.ncol = None, None
      self.is1d = None

      # Write the error info
      e = sys.exc_info()
      print(e)


class SosaaWindow(QMainWindow):
  def __init__(self, parent=None):
    super(SosaaWindow, self).__init__(parent)
    self.initUI()


  def initUI(self):
    #========================================#
    # Set main window
    #========================================#
    self.setGeometry(600, 300, 1000, 600)
    self.setWindowTitle('SOSAA Plotting')
    self.center()

    #========================================#
    # Set menubar
    #========================================#
    # Create menus
    mainMenu = self.menuBar()
    fileMenu = mainMenu.addMenu('File')
    plotMenu = mainMenu.addMenu('Plot')
    helpMenu = mainMenu.addMenu('Help')

    # Add menu actions
    openAction = QAction('Open', self)
    openAction.setShortcut('Ctrl+O')
    openAction.setStatusTip('Open SOSAA output location')
    openAction.triggered.connect(self.openOutputDir)
    fileMenu.addAction(openAction)

    openAction = QAction('Plot', self)
    openAction.setShortcut('Ctrl+P')
    openAction.setStatusTip('Plot')
    openAction.triggered.connect(self.plotOutput)
    plotMenu.addAction(openAction)

    exitAction = QAction('Exit', self)
    exitAction.setShortcut('Ctrl+Q')
    exitAction.setStatusTip('Exit application')
    exitAction.triggered.connect(qApp.quit)
    fileMenu.addAction(exitAction)

    #========================================#
    # Set statusbar
    #========================================#
    # timeout:
    # 0: stay until you change or clear it
    # others, e.g., 5000: 5000 ms == 5 s
    self.statusBar().showMessage('Ready', 0)

    #========================================#
    # Set central widget
    #========================================#
    # Initialize
    self.centralWidget = QWidget()
    self.setCentralWidget(self.centralWidget)

    # Set tree view
    self.setTreeView()
    
    # Set figure view
    self.setFigureView()

    #+++++ Set main view +++++#

    # Splitter layout
    self.setSplitterLayout()

    # Box layout
    # self.setMainBoxLayout()

    # Grid layout
    # self.setGridLayout()  # grid layout

    #+++++++++++++++++++++++++#

    # Show the UI
    self.show()


  def setTreeView(self):
    # SOSAA work dir
    workDir = '/home/local/pzzhou/Scripts/Fortran/SOSAA'


    # Input tree
    self.inputTreeModel = QFileSystemModel()
    rootPath = self.inputTreeModel.setRootPath(workDir)

    self.inputTree = QTreeView()
    self.inputTree.setModel(self.inputTreeModel)
    self.inputTree.setRootIndex(rootPath)
    self.inputTree.setAnimated(False)
    self.inputTree.setIndentation(20)
    self.inputTree.setSortingEnabled(True)
    # self.inputTree.setSelectionMode(2)
    self.inputTree.clicked.connect(self.selectInputFile)

    # Output tree
    self.outputTreeModel = QFileSystemModel()
    rootPath = self.outputTreeModel.setRootPath(workDir)

    self.outputTree = QTreeView()
    self.outputTree.setModel(self.outputTreeModel)
    self.outputTree.setRootIndex(rootPath)
    self.outputTree.setAnimated(False)
    self.outputTree.setIndentation(20)
    self.outputTree.setSortingEnabled(True)
    # self.outputTree.setSelectionMode(2)
    self.outputTree.clicked.connect(self.selectOutputFile)


  def selectInputFile(self, signal):
    self.inputFile = self.inputTreeModel.filePath(signal)


  def selectOutputFile(self, signal):
    self.outputFile = self.outputTreeModel.filePath(signal)

    # Read the data from the output file if it is not a folder
    if os.path.isfile(self.outputFile):
      # Try to load the data if it is regularly written
      try:
        # Load the data
        self.outputData = SosaaData(self.outputFile)

        # Get the raw data size info including the time
        nrow, ncol = self.outputData.nrow_raw, self.outputData.ncol_raw

        # Show data in the dataTable
        self.dataTable.setRowCount(nrow)
        self.dataTable.setColumnCount(ncol)

        for ir in range(nrow):
          for ic in range(ncol):
            self.dataTable.setItem( ir, ic, QTableWidgetItem('{0}'.format(self.outputData.raw[ir, ic])) )

      # Return if it is not a regular data file
      except:
        e = sys.exc_info()
        print(e)

      self.plotOutput()


  def setFigureView(self):
    self.figure = plt.figure(figsize=(15, 5))
    self.canvas = FigureCanvas(self.figure)
    self.toolbar = NavigationToolbar(self.canvas, self)


  def setSplitterLayout(self):
    # Left splitter for tree views
    self.leftSplitter = QSplitter(Qt.Vertical)
    self.leftSplitter.addWidget(self.inputTree)
    self.leftSplitter.addWidget(self.outputTree)

    # Right splitter for figure toolbar and canvas
    self.figureSplitter = QSplitter(Qt.Vertical)
    self.figureSplitter.addWidget(self.toolbar)
    self.figureSplitter.addWidget(self.canvas)
    self.figureSplitter.setStretchFactor(0, 0)
    # self.rightSplitter.setStretchFactor(1, 1)

    # Initialize tab screen
    self.rightTab = QTabWidget()
    # self.tabs.resize(300,200)
    
    # Add tabs
    self.dataTable = QTableWidget()
    self.dataTable.setRowCount(3)
    self.dataTable.setColumnCount(3)
    self.rightTab.addTab(self.dataTable, "Data")

    # self.figureTab = QWidget()
    self.rightTab.addTab(self.figureSplitter, "Figure")
    
    # Create first tab
    # self.tab1.layout = QVBoxLayout(self)
    # self.pushButton1 = QPushButton("PyQt5 button")
    # self.tab1.layout.addWidget(self.pushButton1)
    # self.tab1.setLayout(self.tab1.layout)

    # Main splitter containing left and right splitters
    self.mainSplitter = QSplitter(Qt.Horizontal)
    self.mainSplitter.addWidget(self.leftSplitter)
    self.mainSplitter.addWidget(self.rightTab)

    # Put main splitter to hbox layout
    self.hbox = QHBoxLayout()
    self.hbox.addWidget(self.mainSplitter)

    # 
    self.centralWidget.setLayout(self.hbox)


  def setMainBoxLayout(self):
    self.treeBox = QVBoxLayout()
    # self.treeBox.addStretch(1)
    self.treeBox.addWidget(self.inputTree)
    self.treeBox.addWidget(self.outputTree)

    self.figureBox = QVBoxLayout()
    # self.figureBox.addStretch(1)
    self.figureBox.addWidget(self.toolbar)
    self.figureBox.addWidget(self.canvas)

    self.mainBox = QHBoxLayout()
    # self.mainBox.addStretch(1)
    self.mainBox.addLayout(self.treeBox)
    self.mainBox.addLayout(self.figureBox)

    self.centralWidget.setLayout(self.mainBox)


  def setGridLayout(self):
    # Initiate grid layout
    # grid.addWidget(widget, row, col, rowspan, colspan)
    self.grid = QGridLayout()
    self.grid.addWidget(self.inputTree, 0, 0, 1, 1)
    self.grid.addWidget(self.outputTree, 1, 0, 1, 1)
    self.grid.addWidget(self.canvas, 1, 1, 1, 4)
    self.grid.addWidget(self.toolbar, 0, 1, 1, 4)

    self.centralWidget.setLayout(self.grid)


  def openOutputDir(self):
    self.outputDir = str(QFileDialog.getExistingDirectory(self, "Select Output Directory"))


  def plotOutput(self):
    # Read data
    # raw = np.loadtxt(self.outputFile)

    # Clear axes
    plt.cla()

    # Create figure
    ax = self.figure.add_subplot(1, 1, 1)

    # Try to plot
    try:
      if self.outputData.is1d:  # time series for 1d array
        ax.plot(self.outputData.time, self.outputData.data)
      else:
        ax.pcolor(np.transpose(self.outputData.data))
    except:
      print('The output data file is not suitable for plotting.')

      e = sys.exc_info()
      print(e)

    # Refresh the canvas
    self.canvas.draw()


  def plot1(self):
    plt.cla()
    ax = self.figure.add_subplot(1, 1, 1)
    x = np.linspace(0, 99, 100)
    y = x**2
    ax.plot(x, y, 'b')
    ax.set_title('Quadratic Plot')
    self.canvas.draw()


  def plot2(self):
    plt.cla()
    ax = self.figure.add_subplot(1, 1, 1)
    x = np.linspace(0, 99, 100)
    y = np.sqrt(x)
    ax.plot(x, y, 'r.-')
    ax.set_title('Square Root Plot')
    self.canvas.draw()


  def center(self):
    # Get the geometry of current screen
    screen = QDesktopWidget().screenGeometry()

    # Get the geometry of this window
    form   = self.geometry()
    
    # (x, y): left top point, x: from left to right, y: from top to down
    x_move_step = (screen.width() - form.width()) / 2
    y_move_step = (screen.height() - form.height()) / 2
    self.move(x_move_step, y_move_step)

    # qr = self.frameGeometry()
    # cp = QtWidgets.QDesktopWidget().availableGeometry().center()
    # qr.moveCenter(cp)
    # self.move(qr.topLeft())


def main():
  app = QApplication(sys.argv)

  w = SosaaWindow()
  w.show()

  sys.exit(app.exec_())


if __name__ == '__main__':
  # Suppress the deprecate warnings
  warnings.filterwarnings("ignore",category=matplotlib.cbook.mplDeprecation)

  # Main program
  main()
