import os
import sys

import datetime

import math
import pandas as pd
import numpy as np
import scipy.stats as stats

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.patches as mpatches
# from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange
from matplotlib.ticker import MultipleLocator, FormatStrFormatter, ScalarFormatter
import matplotlib.gridspec as gridspec

# import seaborn as sns
# import seaborn.apionly as sns

import ptpack.parameter as ppp
import ptpack.datapack as dp
import ptpack.functions as ppf

# import datapack as dp
# import functions as ppf

# from header import *



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#
# Help functions
#
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

def choose_color_series(cmap, N):
  xx = np.linspace(0, 1, N)
  cs = [cmap(x) for x in xx]
  return cs


#----------------------------------------------------------#
# Plot contour along time series and height levels.
#----------------------------------------------------------#
def plot_contour_time_height(data, t, h):
  # Check dimensions
  ntime, nlev, stat = check_v_t_z(var, td, zz)
  if (stat < 0):
    print('Wrong dimensions of var, td and zz.')
    return

  # One plot for the whole time series and height levels
  fg, ax = plt.subplots(1, 1, figsize=figsize, dpi=dpi)

  print('plot')
  CS = ax.contourf(td, zz, var.T, 10, origin='lower')
    
  # Set date locator and formatter
  locator = mdates.AutoDateLocator()
  formatter = mdates.AutoDateFormatter(locator)
  ax.xaxis.set_major_locator(locator)
  ax.xaxis.set_major_formatter(formatter)
  # ax_temp[iz].xaxis.set_major_locator(mdates.DayLocator(range(1,32,2)))
  # ax_temp[iz].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
  plt.setp(ax.xaxis.get_majorticklabels(), rotation=30, ha='right', fontsize=10)

  # Set colorbar
  cbar = plt.colorbar(CS)

  print(save_dir, fileName)
  fg.savefig('{0}/{1}.png'.format(save_dir, fileName), dpi=dpi)


def plot_test(ax, data):
  ax.plot(data[:, 7])


def plot_contourf(ax, time, h, var, \
  save_dir='.', fname='temp', \
  title='Z', xlim=None, ylim=None, zlim=None, levels=None):
  """
  " var: (h, time)
  """

  # Get 2D arrays X and Y
  X, Y = np.meshgrid(time, h)
  Z = var

  # Set vmin and vmax
  zmin = np.nanmin(Z)
  zmax = np.nanmax(Z)

  if zlim is None:
    vmin = zmin
    vmax = zmax
  else:
    vmin = zlim[0]
    vmax = zlim[1]

  # Plot
  fg, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=DPI)

  # h_ims = ax.imshow(Z, interpolation='none', extent=[1, 6, 0, 3000])

  # Set formatter of colorbar ticklabels
  formatter = tck.ScalarFormatter()
  formatter.set_powerlimits((-3,4))
  # cbar.ax.yaxis.set_major_formatter(formatter)

  # cs = plt.contourf(X, Y, Z, interpolation='none')
  # cmap = mpl.colors.ListedColormap(['#d53e4f', '#fee08b', '#99d594', '#3288bd'])
  # bounds = [-999, 0, 0.25, 1, 1e8]
  # norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
  # cs = ax.pcolor(X, Y, Z, vmin=vmin, vmax=vmax, cmap=cmap, norm=norm)
  # cs = ax.pcolor(X, Y, Z, vmin=vmin, vmax=vmax, cmap=plt.get_cmap('seismic'))
  # cs = ax.pcolor(X, Y, Z, vmin=vmin, vmax=vmax, cmap=plt.get_cmap('viridis', 10))
  if levels is None:
    cs = ax.contourf(X, Y, Z, vmin=vmin, vmax=vmax, cmap=plt.get_cmap('viridis'))
    # cs = ax.contourf(X, Y, Z, vmin=vmin, vmax=vmax, cmap=plt.cm.viridis)
    cbar = plt.colorbar(cs, format=formatter)
  else:
    nlevels = len(levels)
    colors = plt.get_cmap('viridis', nlevels-1)(np.arange(nlevels-1))
    # cs = ax.contourf(X, Y, Z, levels=levels, interpolation='none', colors=colors, cmap=plt.get_cmap('viridis'))
    cs = ax.contourf(X, Y, Z, levels=levels, colors=colors)
    cbar = plt.colorbar(cs, ticks=levels, format=formatter)
    cbar.ax.set_yticklabels(['{:.2f}'.format(i) for i in levels])

  # ax.set_aspect('equal')

  # Set major ticks for x and y
  xticks = np.arange( time[0], time[-1]+0.1, 0.5 )
  ax.set_xticks(xticks)
  yticks = np.arange( 0, 3001, 500 )
  ax.set_yticks(yticks)

  # Set font properties for tick labels
  for label in (ax.get_xticklabels() + ax.get_yticklabels()):
    label.set_fontproperties(ticklb_font)
    # label.set_fontsize(14) # Size here overrides font_prop

  # Set xlim and ylim
  if xlim is None:
    ax.set_xlim(0, 5)
  else:
    ax.set_xlim(xlim)
  if ylim is None:
    ax.set_ylim(0, 3000)
  else:
    ax.set_ylim(ylim)

  # Set xlabel and ylabel
  ax.set_xlabel('Day (d)', fontproperties=axislb_font)
  ax.set_ylabel(r'$h$ (m)', fontproperties=axislb_font)

  # Set title
  ax.set_title('{0} ({1:.2f}, {2:.2f})'.format(title, zmin, zmax), fontproperties=subtitle_font)

  # Set colorbar font size
  cbar.ax.tick_params(labelsize=16)

  # Save figure
  fg.savefig('{0}/{1}.png'.format(save_dir, fname), dpi=DPI)
