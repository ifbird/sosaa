!
! The input and output module.
! Try to avoid using these predefined file units: 0, 5, 6, 100, 101, 102
!   0: stderr, some compilers may use 100
!   5: stdin, some compilers may use 101
!   6: stoutd, some compilers may use 102
!
MODULE SOSA_IO

  USE SOSA_DATA
  USE second_Monitor, ONLY:SPC_NAMES
  USE second_Parameters
  USE dios_mod

  IMPLICIT NONE
  PRIVATE
  PUBLIC :: READ_INPUTS, CONVERT_ECMWF, open_output_files
  PUBLIC :: output_write
  PUBLIC :: output_done
  PUBLIC :: linear_interp
  PUBLIC :: outspc_count, outspc_names, outspc_cheminds, outemi_count, outemi_names, outemi_cheminds, outemi_meganinds

  INTEGER, PARAMETER :: OUTPUT_NUMBER_MAX = 400
  REAL(kind=dp), DIMENSION(kz, OUTPUT_NUMBER_MAX) :: output_array  ! save output variable values
  CHARACTER(LEN=15), DIMENSION(OUTPUT_NUMBER_MAX) :: output_name   ! save output variable names
  INTEGER :: output_number = 0
  INTEGER, PARAMETER :: UNIT_START = 100
  LOGICAL :: isopen(OUTPUT_NUMBER_MAX)

  CHARACTER(LEN=LINE_WIDTH) :: rawline
  CHARACTER(LEN=SPC_NAME_LEN) :: gname  ! save the temporary gas or species name

  LOGICAL :: exists
  CHARACTER(LEN=100) :: fname_init 

  INTEGER :: ntime_local, ntime_ECMWF  ! number of time points for local data and ECMWF dataset

  INTEGER :: stat  ! record io status

  INTEGER :: itrec  ! current time record
  INTEGER :: varid_lad_meteo, varid_lad_megan
  INTEGER :: varid_temp, varid_rhov, varid_rh, varid_pres, varid_ua, varid_va
  INTEGER :: varid_tke, varid_mixlen, varid_diffm, varid_diffh, varid_ri
  INTEGER :: varid_ustar, varid_shf, varid_lhf
  INTEGER :: varid_nair
  INTEGER :: varid_ceb, varid_gsoil, varid_pblh, varid_albedo, varid_zenith
  INTEGER :: varid_rd_dir, varid_rd_dif, varid_ld, varid_lu, varid_paru, varid_niru
  INTEGER :: varid_nconc_particle, varid_flux_particle, varid_growth_rate
  INTEGER :: varid_nconc_condv_gas, varid_vconc_condv_par

  type netcdf_general
    CHARACTER(LEN=100) :: fname
    INTEGER :: fid
    INTEGER :: dimid_lev
    INTEGER :: varid_lev
  end type netcdf_general

  type netcdf_meteo
    CHARACTER(LEN=100) :: fname
    INTEGER :: fid
    INTEGER :: dimid_lev, dimid_time
    INTEGER :: dimids_2d(2)  ! (/dimid_lev, dimid_time/)
    INTEGER :: varid_lev, varid_time
  end type netcdf_meteo

  type netcdf_chem
    CHARACTER(LEN=100) :: fname
    INTEGER :: fid
    INTEGER :: dimid_lev, dimid_time
    INTEGER :: dimids_2d(2)
    INTEGER :: varid_lev, varid_time
    ! INTEGER, ALLOCATABLE :: varid_nconc(:)
    ! INTEGER, ALLOCATABLE :: varid_flux(:)
    ! INTEGER, ALLOCATABLE :: varid_vd(:)
    ! INTEGER, ALLOCATABLE :: varid_emi(:)
  end type netcdf_chem

  type netcdf_aerosol
    CHARACTER(LEN=100) :: fname
    INTEGER :: fid
    INTEGER :: dimid_icondv, dimid_rdry, dimid_lev, dimid_time
    INTEGER :: dimids_3d_rdry(3), dimids_3d_condv(3), dimids_4d(4)
    INTEGER :: varid_icondv, varid_rdry, varid_lev, varid_time
  end type netcdf_aerosol

  type(netcdf_general) :: nc_general
  type(netcdf_meteo)   :: nc_meteo
  type(netcdf_chem)    :: nc_chem
  type(netcdf_aerosol) :: nc_aerosol

  ! INTEGER, PARAMETER :: MAX_OUTPUT_SPC_COUNT = 100

  ! Output species list
  INTEGER                                  :: outspc_count       ! number of output species
  CHARACTER(LEN=SPC_NAME_LEN), ALLOCATABLE :: outspc_names(:)     ! name list of output species
  INTEGER                    , ALLOCATABLE :: outspc_cheminds(:)  ! indices of output species in the chemistry scheme
  INTEGER, ALLOCATABLE :: varid_outspc_nconc(:), varid_outspc_flux(:), varid_outspc_vd(:)

  ! Emission species list
  INTEGER                                  :: outemi_count        ! number of emitted species
  CHARACTER(LEN=SPC_NAME_LEN), ALLOCATABLE :: outemi_names(:)      ! name list of emitted species
  INTEGER                    , ALLOCATABLE :: outemi_cheminds(:)   ! indices of emitted species in the chemistry scheme
  INTEGER                    , ALLOCATABLE :: outemi_meganinds(:)  ! indices of emitted species in MEGAN
  INTEGER, ALLOCATABLE :: varid_outemi(:)

  ! HOM10 species list
  INTEGER                                  :: outspc_HOM10_count    ! number of output species
  CHARACTER(LEN=SPC_NAME_LEN), ALLOCATABLE :: outspc_HOM10_name(:)  ! name list of output species
  INTEGER                    , ALLOCATABLE :: outspc_HOM10_ind(:)   ! indices of output species in the chemistry scheme

  ! HOM20 species list
  INTEGER                                  :: outspc_HOM20_count    ! number of output species
  CHARACTER(LEN=SPC_NAME_LEN), ALLOCATABLE :: outspc_HOM20_name(:)  ! name list of output species
  INTEGER                    , ALLOCATABLE :: outspc_HOM20_ind(:)   ! indices of output species in the chemistry scheme

  ! Temporary array to save the position of each species in the name list
  INTEGER, ALLOCATABLE :: tmp_pos(:, :)

  ! A string of a name list separated by comma (space is ignored)
  ! Example:
  !   output_list_spc = "OH, O3, SO2"
  !   output_list_emi = "APINENE, BPINENE"
  CHARACTER(LEN=1000) :: output_list_spc  ! number concentration, flux, dry deposition velocity
  CHARACTER(LEN=1000) :: output_list_emi  ! emissions

  NAMELIST /NML_OUTPUT/ output_list_spc, output_list_emi

  CHARACTER(50) :: start_date_string, first_day_of_month_string


CONTAINS


SUBROUTINE READ_INPUTS
  !--------------------------------------------------------------------------------------------------------------------!
  ! Read namelists from initiation file fname_init. Current values will be used if not specified in the namelist.
  !--------------------------------------------------------------------------------------------------------------------!
  CALL GETARG(1, fname_init)  ! Get the first argument of the executable command
  WRITE(*,*) 'Reading namelists from ', TRIM(ADJUSTL(fname_init))
  OPEN(UNIT=99, FILE=TRIM(ADJUSTL(fname_init)), STATUS='OLD')
  READ(UNIT=99, NML=NML_MAIN  , IOSTAT=stat)  ! directories and station
  READ(UNIT=99, NML=NML_FLAG  , IOSTAT=stat)  ! scheme flags
  READ(UNIT=99, NML=NML_GRID  , IOSTAT=stat)  ! e.g., masl
  READ(UNIT=99, NML=NML_TIME  , IOSTAT=stat)  ! start and end time, time steps
  READ(UNIT=99, NML=NML_OUTPUT, IOSTAT=stat)  ! start and end time, time steps
  CLOSE(99)

  !--------------------------------------------------------------------------------------------------------------------!
  ! Get date infomation
  !--------------------------------------------------------------------------------------------------------------------!
  now_date = start_date
  WRITE(*,*) 'Simulation starts on ', start_date
  WRITE(*,*) 'Simulation ends on ', end_date
  mon = now_date(2)  ! set current month number

  ! prepare those time variables

  WRITE(year_str,'(I4)') now_date(1)
  WRITE(year_str2, '(I2.2)') MOD(now_date(1), 100)
  WRITE(month_str_Mxx, '(A1, I2.2)') 'M', now_date(2)  ! 'M01', 'M02', ..., 'M12'
  WRITE(month_str, '(I2.2)') now_date(2)  ! '01', '02', ..., '12'
  WRITE(day_str, '(I2.2)') now_date(3)  ! '01', '02', ..., '31'

  !--------------------------------------------------------------------------------------------------------------------!
  ! Set output and input dirs.
  ! The previous filename1 is substituted by the INPUT_DIR.
  !--------------------------------------------------------------------------------------------------------------------!
  input_dir_general = TRIM(ADJUSTL(INPUT_DIR)) // '/general'
  input_dir_station = TRIM(ADJUSTL(INPUT_DIR)) // '/station' // '/' // STATION
  input_dir_station_info = TRIM(ADJUSTL(input_dir_station)) // '/info'
  input_dir_station_data = TRIM(ADJUSTL(input_dir_station)) //  '/' // year_str // '/' // month_str_Mxx ! data input

  write(*,*) 'WORK_DIR = ', TRIM(WORK_DIR)  ! SOSAA project folder
  write(*,*) 'CODE_DIR = ', TRIM(CODE_DIR)  ! code folder
  write(*,*) 'CASE_DIR = ', TRIM(CASE_DIR)  ! case folder
  write(*,*) 'CHEM_DIR = ', TRIM(CHEM_DIR)  ! chemistry scheme folder
  write(*,*) 'INPUT_DIR = ', TRIM(INPUT_DIR)
  write(*,*) 'input_dir_general = ', TRIM(input_dir_general)
  write(*,*) 'input_dir_station = ', TRIM(input_dir_station)
  write(*,*) 'input_dir_station_info = ', TRIM(input_dir_station_info)
  write(*,*) 'input_dir_station_data = ', TRIM(input_dir_station_data)
  write(*,*) 'output_dir = ', TRIM(output_dir)

  ! Check if the output directory path exists, if not, create folders needed.
  INQUIRE(FILE=TRIM(ADJUSTL(output_dir)) // '/.', EXIST=exists)
  IF (.NOT. exists) THEN
    CALL SYSTEM('mkdir -p ' // TRIM(ADJUSTL(output_dir)))
  END IF

  ! Get outspc_count, outspc_names, outspc_cheminds
  ! and outemi_count, outemi_names, outemi_cheminds, outemi_meganinds
  CALL get_output_spc_emi_from_string()
  
  ! write(*,*) 'There are totally outspc_count, outspc_names, outspc_cheminds
  write(*,*) outemi_count, outemi_names, outemi_cheminds, outemi_meganinds
  
  !----------------------------------------------------------------------------!
  ! Read input of measurement data
  !----------------------------------------------------------------------------!
  ! CALL READ_DATA_OLD()
  CALL READ_DATA_NEW()

CONTAINS

  subroutine get_output_spc_emi_from_string()
    !----------------------------------------------------------------------------!
    ! Get the output species list
    !----------------------------------------------------------------------------!
    ! Allocate temporary position array
    ALLOCATE(tmp_pos(len_trim(adjustl(output_list_spc)), 2))

    ! Get outspc_count
    CALL string_split_trim2(trim(adjustl(output_list_spc)), ',', outspc_count, tmp_pos)

    ! Get outspc_names
    ALLOCATE(outspc_names(outspc_count))
    CALL get_substring_list(trim(adjustl(output_list_spc)), outspc_count, tmp_pos, outspc_names)

    ! Get outspc_cheminds
    ALLOCATE(outspc_cheminds(outspc_count))
    CALL get_inds_outnames_in_rawnames(outspc_names, SPC_NAMES, outspc_cheminds)

    DEALLOCATE(tmp_pos)

    !----------------------------------------------------------------------------!
    ! Get the output emission species list
    !----------------------------------------------------------------------------!
    ! Allocate temporary position array
    ALLOCATE(tmp_pos(len_trim(adjustl(output_list_emi)), 2))

    ! Get outemi_count
    CALL string_split_trim2(trim(adjustl(output_list_emi)), ',', outemi_count, tmp_pos)

    ! Get outemi_names
    ALLOCATE(outemi_names(outemi_count))
    CALL get_substring_list(trim(adjustl(output_list_emi)), outemi_count, tmp_pos, outemi_names)

    ! Get outemi_cheminds, outemi_meganinds
    ALLOCATE(outemi_cheminds(outemi_count))
    ALLOCATE(outemi_meganinds(outemi_count))
    CALL get_inds_outnames_in_rawnames(outemi_names, SPC_NAMES, outemi_cheminds)
    CALL get_inds_outnames_in_rawnames(outemi_names, MEGAN_SPC_NAMES, outemi_meganinds)

    DEALLOCATE(tmp_pos)
  end subroutine get_output_spc_emi_from_string


  subroutine get_output_spc_emi_from_file()
    !--------------------------------------------------------------------------!
    ! Get the information of species for output from outspc.txt
    !--------------------------------------------------------------------------!
    ! Read the line count of outspc.txt
    CALL GET_FILE_LINE_COUNT(TRIM(ADJUSTL(CHEM_DIR)) // '/INPUT/outspc.txt', outspc_count)

    ! Read the file again to get outspc_name and outspc_cheminds.
    ! Here we assume that the names are unique.
    ALLOCATE(outspc_names(outspc_count), outspc_cheminds(outspc_count))
    CALL GET_SPC_LIST_FROM_FILE(TRIM(ADJUSTL(CHEM_DIR)) // '/INPUT/outspc.txt', &
      outspc_names, outspc_cheminds)

    !--------------------------------------------------------------------------!
    ! Get the information of HOM species for output from HOM10.txt and HOM20.txt
    !--------------------------------------------------------------------------!
    IF (AeroFlag == 1) THEN
      ! Read the line count of HOM10.txt
      CALL GET_FILE_LINE_COUNT(TRIM(ADJUSTL(CHEM_DIR)) // '/INPUT/HOM10.txt', outspc_HOM10_count)

      ! Read the file again to get outspc_name and outspc_ind.
      ! Here we assume that the names are unique.
      ALLOCATE(outspc_HOM10_name(outspc_HOM10_count), outspc_HOM10_ind(outspc_HOM10_count))
      CALL GET_SPC_LIST_FROM_FILE(TRIM(ADJUSTL(CHEM_DIR)) // '/INPUT/HOM10.txt', outspc_HOM10_name, outspc_HOM10_ind)

      ! Read the line count of HOM20.txt
      CALL GET_FILE_LINE_COUNT(TRIM(ADJUSTL(CHEM_DIR)) // '/INPUT/HOM20.txt', outspc_HOM20_count)

      ! Read the file again to get outspc_name and outspc_ind.
      ! Here we assume that the names are unique.
      ALLOCATE(outspc_HOM20_name(outspc_HOM20_count), outspc_HOM20_ind(outspc_HOM20_count))
      CALL GET_SPC_LIST_FROM_FILE(TRIM(ADJUSTL(CHEM_DIR)) // '/INPUT/HOM20.txt', outspc_HOM20_name, outspc_HOM20_ind)
      ! write(*,*) 'outspc_count ', outspc_count
      ! write(*,*) 'outspc_name ', outspc_name
      ! write(*,*) 'outspc_ind ', outspc_ind
    END IF

    !--------------------------------------------------------------------------!
    ! Get the information of species for emission output from outemi.txt
    !--------------------------------------------------------------------------!
    ! Read the line count of outemi.txt
    CALL GET_FILE_LINE_COUNT(TRIM(ADJUSTL(CHEM_DIR)) // '/INPUT/outemi.txt', outemi_count)

    ! Read the file again to get emispc_name and emispc_ind.
    ! Here we assume that the names are unique.
    ALLOCATE(outemi_names(outemi_count), outemi_cheminds(outemi_count), outemi_meganinds(outemi_count))
    CALL GET_SPC_LIST_FROM_FILE(TRIM(ADJUSTL(CHEM_DIR)) // '/INPUT/outemi.txt', &
      outemi_names, outemi_cheminds)
    CALL GET_SPC_MEGAN_IND(outemi_names, outemi_meganinds)
  end subroutine get_output_spc_emi_from_file

END SUBROUTINE READ_INPUTS


SUBROUTINE GET_FILE_LINE_COUNT(fname, n)
  ! Count the non-empty lines

  CHARACTER(LEN=*), INTENT(IN) :: fname
  INTEGER, INTENT(OUT) :: n

  ! Initial value
  n = 0

  ! Open the file
  OPEN(UNIT=99, FILE=TRIM(ADJUSTL(fname)))
  REWIND(99)

  ! Count the line number
  DO
    READ(99, '(a)', IOSTAT=stat) rawline  ! read every line including empty lines
    IF (stat /= 0) EXIT                   ! exit if eof
    IF (LEN(TRIM(rawline)) > 0) n=n+1          ! add one line count if the line is not empty
  END DO
  
  ! Close the file
  CLOSE(99)
END SUBROUTINE GET_FILE_LINE_COUNT


SUBROUTINE GET_SPC_LIST_FROM_FILE(fname, spc_name, spc_ind)
  CHARACTER(LEN=*           ), INTENT(IN   ) :: fname
  CHARACTER(LEN=SPC_NAME_LEN), INTENT(  OUT) :: spc_name(:)
  INTEGER                    , INTENT(  OUT) :: spc_ind(:)

  INTEGER :: spc_count, ispc

  ! Number of output species
  spc_count = SIZE(spc_name)
  write(*,*) 'spc_count ', spc_count

  ! Read spc names one by one
  ispc = 0

  ! Open the file and go back the beginning then read the spc names line by line 
  OPEN(UNIT=99, FILE=TRIM(ADJUSTL(fname)))
  REWIND(99)
  DO
    READ(99, '(a)', IOSTAT=stat) rawline  ! read every line including empty lines
    IF (stat /= 0) EXIT                   ! exit if eof
    ! Add one species if the line is not empty
    IF (LEN(TRIM(rawline)) > 0) THEN
      ispc=ispc+1        
      spc_name(ispc) = TRIM(rawline)
    END IF
  END DO
  CLOSE(99)

  ! Obtain indices of output species (case insensative)
  ! NSPEC and SPC_NAMES are from chemistry module
  spc_ind(:) = -1  ! default value
  DO I=1, spc_count
    DO J=1, NSPEC
      IF ( upper( TRIM(ADJUSTL( SPC_NAMES(J) )) ) == upper( TRIM(ADJUSTL(spc_name(I))) ) ) THEN
        spc_ind(I) = J
        EXIT
      END IF
    END DO
  END DO
END SUBROUTINE GET_SPC_LIST_FROM_FILE


SUBROUTINE GET_SPC_MEGAN_IND(spc_name, spc_mind)
  CHARACTER(LEN=SPC_NAME_LEN), INTENT(IN   ) :: spc_name(:)
  INTEGER                    , INTENT(  OUT) :: spc_mind(:)

  INTEGER :: spc_count

  spc_count = SIZE(spc_name)

  spc_mind(:) = -1  ! default value
  DO I=1, spc_count
    DO J=1, MEGAN_NSPEC
      IF ( upper( TRIM(ADJUSTL( MEGAN_SPC_NAMES(J) )) ) == upper( TRIM(ADJUSTL(spc_name(I))) ) ) THEN
        spc_mind(I) = J
        EXIT
      END IF
    END DO
  END DO
END SUBROUTINE GET_SPC_MEGAN_IND


SUBROUTINE READ_DATA_NEW()
  ! Save original data from input files, first 6 columns are date, then for different height levels.
  ! First line is header, first 6 are 0, then height levels.
  ! The maximum time points are 48*31 + 1, then add another 1 for header. So totally 1490.
  ! Now 15 is enough for at most 7 height levels.
  ! For ECMWF data, the maximum time points are 8*31+1 (249).
  REAL(dp) :: raw_data(1490, 15)

  INTEGER :: i, j

  IF (TRIM(ADJUSTL(STATION)) == 'hyytiala') THEN
    ! Number of time points for local dataset.
    ! yyyymm01-00:00:00 to yyyy(mm+1)01-00:00:00, so 1 is added.
    ntime_local = MonthDay(now_date(1), now_date(2))*48 + 1

    !
    ! Input of measured gases O3, SO2, NO, NO2, CO and air (every half-hour data)
    !

    ! O3
    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_O3avg.txt', STATUS='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    CH_gas_hyy(2, 1:ntime_local) = raw_data(2:ntime_local+1, 7)
    CLOSE(UNIT=101)

    ! SO2
    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_SO2avg.txt', STATUS='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    CH_gas_hyy(3, 1:ntime_local) = raw_data(2:ntime_local+1, 7)
    CLOSE(UNIT=101)

    ! NO
    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_NOavg.txt', STATUS='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    CH_gas_hyy(4, 1:ntime_local) = raw_data(2:ntime_local+1, 7)
    CLOSE(UNIT=101)

    ! NO2
    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_NO2avg.txt', STATUS='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    CH_gas_hyy(5, 1:ntime_local) = raw_data(2:ntime_local+1, 7)
    CLOSE(UNIT=101)

    ! CO
    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_COavg.txt', STATUS='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    CH_gas_hyy(6, 1:ntime_local) = raw_data(2:ntime_local+1, 7)
    CLOSE(UNIT=101)

    ! NH3, if ACDC is used
    IF (AeroFlag==1) THEN
      IF (NH3flag==1) THEN
        OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_NH3avg.txt', STATUS='old')
      ELSE IF (NH3flag==2) THEN  !if number concentration is used 
        OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_NH3avg_NC.txt', STATUS='old')
      END IF
      READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
      NH3_hyy(1:ntime_local) = raw_data(2:ntime_local+1, 7)
      CLOSE(UNIT=101)
    END IF 

    if(Emiflag==0)then

    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_mono_4_2.txt', STATUS='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    CH_VOC_hyy(1,1:ntime_local) = raw_data(2:ntime_local+1, 7)
    CLOSE(UNIT=101)

    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_C5H8_MBO_4_2.txt', STATUS='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    CH_VOC_hyy(2,1:ntime_local) = raw_data(2:ntime_local+1, 7)
    CLOSE(UNIT=101)

    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_MVK_4_2.txt', STATUS='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    CH_VOC_hyy(3,1:ntime_local) = raw_data(2:ntime_local+1, 7)
    CLOSE(UNIT=101)

!    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_MEK_4_2.txt', STATUS='old')
!    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
!    CH_VOC_hyy(4,1:ntime_local) = raw_data(2:ntime_local+1, 7)
!    CLOSE(UNIT=101)

    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_CH3OH_4_2.txt', STATUS='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    CH_VOC_hyy(5,1:ntime_local) = raw_data(2:ntime_local+1, 7)
    CLOSE(UNIT=101)

    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_CH3CHO_4_2.txt', STATUS='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    CH_VOC_hyy(6,1:ntime_local) = raw_data(2:ntime_local+1, 7)
    CLOSE(UNIT=101)

!    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_C2H5OH_4_2.txt', STATUS='old')
!    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
!    CH_VOC_hyy(7,1:ntime_local) = raw_data(2:ntime_local+1, 7)
!    CLOSE(UNIT=101)

    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_CH3COCH3_4_2.txt', STATUS='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    CH_VOC_hyy(8,1:ntime_local) = raw_data(2:ntime_local+1, 7)
    CLOSE(UNIT=101)

    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_CH3CHO2H_4_2.txt', STATUS='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    CH_VOC_hyy(9,1:ntime_local) = raw_data(2:ntime_local+1, 7)
    CLOSE(UNIT=101)

    OPEN(UNIT=101, FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_H2SO4.txt', STATUS='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    CH_H2SO4_hyy(1:ntime_local) = raw_data(2:ntime_local+1, 7)
    CLOSE(UNIT=101)    
    endif
    !
    ! Input of measured uwind, vwind, temperature, absolute humidity, pressure
    !

    ! uwind
    ! 8.4, 16.8, 33.6, 67.2, 74.0
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_uwind.txt',status='old')
    READ(101, *) ( (raw_data(i, j), j=1, 11), i=1, ntime_local+1 )
    local_uwind(1:ntime_local, 1:5) = raw_data(2:ntime_local+1, 7:11)  ! data
    loclv_uwind(1:5) = raw_data(1, 7:11)  ! height levels
    CLOSE(UNIT=101)

    ! vwind
    ! 8.4, 16.8, 33.6, 67.2, 74.0
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_vwind.txt',status='old')
    READ(101, *) ( (raw_data(i, j), j=1, 11), i=1, ntime_local+1 )
    local_vwind(1:ntime_local, 1:5) = raw_data(2:ntime_local+1, 7:11)  ! data
    loclv_vwind(1:5) = raw_data(1, 7:11)  ! height levels
    CLOSE(UNIT=101)

    ! air temperature, [degC]
    ! 4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 125.0
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_temp.txt',status='old')
    READ(101, *) ( (raw_data(i, j), j=1, 13), i=1, ntime_local+1 )
    local_temp(1:ntime_local, 1:7) = raw_data(2:ntime_local+1, 7:13) + 273.15_dp  ! data, [degC] --> [K]
    loclv_temp(1:7) = raw_data(1, 7:13)  ! height levels
    CLOSE(UNIT=101)

    ! absolute humidity, [kg m-3]
    ! 4.2, 8.4, 16.8, 33.6, 50.4, 67.2, 125.0
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_rhov.txt',status='old')
    READ(101, *) ( (raw_data(i, j), j=1, 13), i=1, ntime_local+1 )
    local_rhov(1:ntime_local, 1:7) = raw_data(2:ntime_local+1, 7:13)  ! data
    loclv_rhov(1:7) = raw_data(1, 7:13)  ! height levels
    CLOSE(UNIT=101)

    ! air pressure, [hPa]
    ! 0.0
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_pres.txt',status='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    local_pres(1:ntime_local, 1) = raw_data(2:ntime_local+1, 7) * 1e2_dp  ! [hPa] --> [Pa]
    loclv_pres(1) = raw_data(1, 7)  ! height levels
    CLOSE(UNIT=101)

    !
    ! Input of measured soil moist, soil temperature, soil heat flux
    !

    ! soil moist, [m3 m-3] 
    ! -5 - 0 cm for organic layer
    ! 2-6 cm in mineral soil
    ! 14-25 cm in mineral soil
    ! 26-36 cm in mineral soil
    ! 38-61 cm in mineral soil
    ! into the soil: -2.5, 4.0, 19.5, 31.0, 49.5
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_soilmoist.txt',status='old')
    READ(101, *) ( (raw_data(i, j), j=1, 11), i=1, ntime_local+1 )
    local_soilmoist(1:ntime_local, 1:5) = raw_data(2:ntime_local+1, 7:11)
    loclv_soilmoist(1:5) = raw_data(1, 7:11)
    CLOSE(UNIT=101)

    ! soil temperature, [degC]
    ! -5 - 0 cm for organic layer
    ! 2-5 cm in mineral soil
    ! 9-14 cm in mineral soil
    ! 22-29 cm in mineral soil
    ! 42-58 cm in mineral soil
    ! into the soil: -2.5, 3.5, 11.5, 25.5, 50.0
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_soiltemp.txt',status='old')
    READ(101, *) ( (raw_data(i, j), j=1, 11), i=1, ntime_local+1 )
    local_soiltemp(1:ntime_local, 1:5) = raw_data(2:ntime_local+1, 7:11) + 273.15_dp  ! [degC] --> [K]
    loclv_soiltemp(1:5) = raw_data(1, 7:11)
    CLOSE(UNIT=101)
    ! Calculate deep soil temperature from the average of soiltemp at 22-29 cm and 42-58 cm
    local_deep_soiltemp(1:ntime_local) = 0.5_dp * (local_soiltemp(1:ntime_local, 4) + local_soiltemp(1:ntime_local, 5))

    ! soil heat flux, [W m-2]
    ! 0.0
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_gsoil.txt',status='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    local_gsoil(1:ntime_local, 1) = raw_data(2:ntime_local+1, 7)
    loclv_gsoil(1) = raw_data(1, 7)
    CLOSE(UNIT=101)

    !
    ! Input of measured global radiation, PAR, albedo
    !

    ! downward global radiation
    ! 18.0
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_glob.txt',status='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    local_glob(1:ntime_local, 1) = raw_data(2:ntime_local+1, 7)
    loclv_glob(1) = raw_data(1, 7)
    CLOSE(UNIT=101)

    ! downward PAR
    ! 18.0
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_PAR.txt',status='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    local_PAR(1:ntime_local, 1) = raw_data(2:ntime_local+1, 7)
    loclv_PAR(1) = raw_data(1, 7)
    CLOSE(UNIT=101)

    ! albedo
    ! 125.0
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_albedo.txt',status='old')
    READ(101, *) ( (raw_data(i, j), j=1, 7), i=1, ntime_local+1 )
    local_albedo(1:ntime_local, 1) = raw_data(2:ntime_local+1, 7)
    loclv_albedo(1) = raw_data(1, 7)
    CLOSE(UNIT=101)
    !Input of CS and CoagS
    !
    !Condensation Sink: sum of dmps and aps with rh-correction
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_cs.txt',status='old')
    READ(101,*)((raw_data(i,j),j=1,7),i=1,ntime_local+1)
    local_cs(1:ntime_local,1)=raw_data(2:ntime_local+1,7)
    loclv_cs(1)=raw_data(1,7)
    CLOSE(UNIT=101)
    !
    !Condensation Sink: sum of dmps and aps with HNO3-rh-correction
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_data))//'/SMEAR_ch.txt',status='old')
    READ(101,*)((raw_data(i,j),j=1,7),i=1,ntime_local+1)
    local_cs2(1:ntime_local,1)=raw_data(2:ntime_local+1,7)
    loclv_cs2(1)=raw_data(1,7)
    CLOSE(UNIT=101)

    ! Check if there are any NANs in the local datasets
    IF ( ANY( ISNAN(CH_gas_hyy(2:6, 1:ntime_local     )) ) ) WRITE(*,*) 'NANs in CH_gas_hyy.'
    IF ( ANY( ISNAN(local_uwind(1:ntime_local, 1:5    )) ) ) WRITE(*,*) 'NANs in local_uwind.'
    IF ( ANY( ISNAN(local_vwind(1:ntime_local, 1:5    )) ) ) WRITE(*,*) 'NANs in local_vwind.'
    IF ( ANY( ISNAN(local_temp(1:ntime_local, 1:7     )) ) ) WRITE(*,*) 'NANs in local_temp.'
    IF ( ANY( ISNAN(local_rhov(1:ntime_local, 1:7     )) ) ) WRITE(*,*) 'NANs in local_rhov.'
    IF ( ANY( ISNAN(local_pres(1:ntime_local, 1       )) ) ) WRITE(*,*) 'NANs in local_pres.'
    IF ( ANY( ISNAN(local_soilmoist(1:ntime_local, 1:5)) ) ) WRITE(*,*) 'NANs in local_soilmoist.'
    IF ( ANY( ISNAN(local_soiltemp(1:ntime_local, 1:5 )) ) ) WRITE(*,*) 'NANs in local_soiltemp.'
    IF ( ANY( ISNAN(local_gsoil(1:ntime_local, 1      )) ) ) WRITE(*,*) 'NANs in local_gsoil.'
    IF ( ANY( ISNAN(local_glob(1:ntime_local, 1       )) ) ) WRITE(*,*) 'NANs in local_glob.'
    IF ( ANY( ISNAN(local_PAR(1:ntime_local, 1        )) ) ) WRITE(*,*) 'NANs in local_PAR.'
    IF ( ANY( ISNAN(local_albedo(1:ntime_local, 1     )) ) ) WRITE(*,*) 'NANs in local_albedo.'
    IF ( ANY( ISNAN(local_cs(1:ntime_local, 1     )) ) ) WRITE(*,*) 'NANs in local_hyy.'
    IF ( ANY( ISNAN(local_cs2(1:ntime_local, 1     )) ) ) WRITE(*,*) 'NANs in local_hyy2.'
    !
    ! Canopy information
    !
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_info)) // '/canopy.txt',status='old')
    canopyfile = 0.0
    ! There are some texts after 9 columns of number. So a do-loop is needed.
    DO i = 1, 16
      READ(101, *) (canopyfile(i, j), j=1, 9)
    END DO
    CLOSE(UNIT=101)
    ! hc = canopyfile(4,1) !This is the 2012 value 

    ! Canopy height is growing ~18 cm/year in Hyytiala, but I now used the
    ! measured values - check Megan_version2.f90 for more info
    IF (now_date(1) == 2018) hc = 20.04_dp
    IF (now_date(1) == 2017) hc = 19.86_dp    
    IF (now_date(1) == 2016) hc = 19.68_dp  ! hc(2014) + 0.18*2
    IF (now_date(1) == 2015) hc = 19.50_dp  ! hc(2014) + 0.18*1
    IF (now_date(1) == 2014) hc = 19.32_dp
    IF (now_date(1) == 2013) hc = 18.97_dp
    IF (now_date(1) == 2012) hc = 18.63_dp
    IF (now_date(1) == 2011) hc = 18.29_dp
    IF (now_date(1) == 2010) hc = 17.98_dp
    IF (now_date(1) == 2009) hc = 17.58_dp
    IF (now_date(1) == 2008) hc = 17.16_dp
    IF (now_date(1) == 2007) hc = 16.86_dp
    IF (now_date(1) == 2006) hc = 16.57_dp
    IF (now_date(1) == 2005) hc = 16.27_dp
    IF (now_date(1) == 2004) hc = 15.97_dp
    IF (now_date(1) == 2003) hc = 15.68_dp
    IF (Treeflag == 4) THEN
      hc = 4.82
    END IF
    WRITE(*,*) 'The updated canopy height (hc) is ', hc

    !The SEP for all years are based on year 2010 measurements
    !C1: measured chamber temperature (celcius): C2: measured monoterpene emission rate (ng g-1 s-1)
    !'J' refers to the fact that this data comes from Juho
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station_data))//'/EFJ_mono.txt',status='old')
    READ(101, *) ( (EF_meas(i, j),i=1, 2),j=1, 1488 )
    CLOSE(UNIT=101)

    ! The concentration of methane for all years are based on year 2014 meaurements
    OPEN(UNIT=101,FILE=TRIM(ADJUSTL(input_dir_station)) // '/2014/' // month_str_Mxx // '/hyy_conc_CH4.txt', status='old')
    READ(101, *) ( hyy_ch4(j), j=1, ntime_local-1 )  ! last time point is not included
    CLOSE(UNIT=101)

    DO j = 1, ntime_local
      ! Wind: the order is from high level to low level in nudging subroutines
      ! Used height levles: 8.4, 16.8, 33.6, 74.0
      uwind(1:4, j) = local_uwind(j, (/5, 3, 2, 1/))
      vwind(1:4, j) = local_vwind(j, (/5, 3, 2, 1/))

      ! Temperature: the order is from high level to low level in nudging subroutines
      ! Used height levles: 4.2, 8.4, 16.8, 33.6, 50.4, 67.2
      tem(1:6, j) = local_temp(j, 6:1:-1)

      ! Absolute humidity: the order is from high level to low level in nudging subroutines
      ! Used height levles: 4.2, 8.4, 16.8, 33.6, 50.4, 67.2
      hum(1:6, j) = local_rhov(j, 6:1:-1)

      ! Incoming diffuse and direct radiation
      ! Current measurement is not reliable, so they are obtained from global radiation
      difftop1(j) = MAX(0.5_dp*local_glob(j, 1), 0.0_dp)
      dirtop1(j)  = MAX(0.5_dp*local_glob(j, 1), 0.0_dp)

      glo_hyy(j) = MAX(local_glob(j, 1),0.0_dp)
      par_hyy(j) = MAX(local_PAR(j, 1),0.0_dp)
      albedo(j)  = MIN(MAX(local_albedo(j, 1), 0.1_dp), 0.5_dp)

      sm_hyy(j) = 0.5_dp * ( local_soilmoist(j, 4) + local_soilmoist(j, 5) )  ! average of 26-36 cm and 38-61 cm
      sm_b(j)   = 0.5_dp * ( local_soilmoist(j, 2) + local_soilmoist(j, 3) )  ! average of 2-6 cm and 14-25 cm
      sm_h(j)   = local_soilmoist(j, 1)  ! Volumetric water content (m3/m3) in organic layer in -5-0 cm
      soil_flux(j) = local_gsoil(j, 1)

      ! Condensation sinks, commented out because it's being updated below
      cs_hyy(j)     = 0.001_dp  ! hyy_mix(4,j20)
      cs_hyy2(j)    = 0.001_dp  ! hyy_mix(24,j20)
      
      !new Condensation sinks
      cs_hyy(j) = local_cs(j,1)
      cs_hyy2(j)= local_cs2(j,1)
    END DO

    IF (Treeflag == 4) THEN !CLEARCUT
      !This is used for the clear cut and includes a modification of the
      !measured data from Haapanala et al., BGS, 2012.
      !C1: daily monoterpene flux assuming constant flux from day
      !116-177. Unit: ug m^-2 h^-1
      !C2: daily monoterpene flux assuming decreasing flux from day
      !116-177 with a start value of around 7400. Unit: ug m^-2 h^-1
      !C3:  daily monoterpene flux assuming decreasing flux from day
      !116-177 with a start value of around 10000. Unit: ug m^-2 h^-1
      OPEN(2715,FILE=TRIM(ADJUSTL(input_dir_station_info)) // '/SAMI_IN.txt')
      DO J = 1,366
         READ(2715,*) (SAMI(J,I), I=1,3)
      ENDDO
      CLOSE(2715)
    ENDIF

    IF (Treeflag == 3) THEN !BIRCH
        open(2716,file=TRIM(ADJUSTL(input_dir_station_info)) // '/lai_function.txt')
        do J = 1,366
           read(2716,*) LAI_function(J)
        enddo
        close(2716)
    ENDIF
  END IF  ! STATION is 'hyytiala'

  abl = 2

  IF (ECMWFflag2 == 1) THEN
    ! time: 0, 3, 6, 9, 12, 15, 18, 21 every day, with 24:00 in last day
    ! space: 6 date numbers + 37 pressure layers, or 6 date numbers + 1 surface value
    ntime_ECMWF = MonthDay(now_date(1), now_date(2))*8 + 1

    OPEN(unit=102,file = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_uwind.txt',status='old')
    READ(102,*) ( (input_uwind(i, j), j=1, 43), i=1, ntime_ECMWF )
    CLOSE(102)

    OPEN(unit=102,file = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_vwind.txt',status='old')
    READ(102,*) ( (input_vwind(i, j), j=1, 43),i=1, ntime_ECMWF )
    CLOSE(102)

    OPEN(unit=102,file = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_temp.txt',status='old')
    READ(102,*) ( (input_temp(i, j), j=1, 43), i=1, ntime_ECMWF )
    CLOSE(102)

    OPEN(unit=102,file = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_rhov.txt',status='old')
    READ(102,*) ( (input_rhov(i, j), j=1, 43), i=1, ntime_ECMWF )
    CLOSE(102)

    OPEN(UNIT=102,FILE = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_geop.txt',status='old')
    READ(102,*) ( (input_geop(i, j), j=1, 43), i=1, ntime_ECMWF)
    CLOSE(102)
    input_geom = input_geop / grav  ! geopotential height to geometric height

    OPEN(UNIT=102,FILE = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_strd.txt',status='old')
    READ(102,*) ( (input_strd(i, j), j=1, 7), i=1, ntime_ECMWF )
    CLOSE(UNIT=102)

    ! Check if there are any NANs in the input ECMWF datasets.
    IF ( ANY(ISNAN(input_geom )) ) WRITE(*,*) 'NANs in the ECMWF geometric heights.'
    IF ( ANY(ISNAN(input_uwind)) ) WRITE(*,*) 'NANs in the ECMWF u wind.'
    IF ( ANY(ISNAN(input_vwind)) ) WRITE(*,*) 'NANs in the ECMWF v wind.'
    IF ( ANY(ISNAN(input_temp )) ) WRITE(*,*) 'NANs in the ECMWF temperature.'
    IF ( ANY(ISNAN(input_qv   )) ) WRITE(*,*) 'NANs in the ECMWF specific humidity.'
    IF ( ANY(ISNAN(input_rhov )) ) WRITE(*,*) 'NANs in the ECMWF absolute humidity.'

  END IF  ! ECMWFflag2 == 1

END SUBROUTINE READ_DATA_NEW


SUBROUTINE READ_DATA_OLD()
  IF (TRIM(ADJUSTL(STATION)) == 'hyytiala') THEN

    ! Input of mixed data from Hyytiala (half-hour data):
    ! 1  = date and time
    ! 2  = diffuse global radiation [W/m2]
    ! 3  = direct global radiation [W/m2]       !r for Welgegund this the TOTAL global radiation
    ! 4  = cs - sum of dmps and aps with rh-correction
    ! 5  = u* in m/s
    ! 6  = temperature in K at 67.2 m
    ! 7  = temperature in K  at 50.4 m
    ! 8  = temperature in K  at 33.6 m
    ! 9  = temperature in K  at 16.8 m
    ! 10 = temperature in K  at 8.4 m
    ! 11 = temperature in K  at 4.2 m
    ! 12 = absolute humidity in kg/m3 at 67.2 m
    ! 13 = absolute humidity in kg/m3 at 50.4 m
    ! 14 = absolute humidity in kg/m3 at 33.6 m
    ! 15 = absolute humidity in kg/m3 at 16.8 m
    ! 16 = absolute humidity in kg/m3 at 8.4 m
    ! 17 = absolute humidity in kg/m3 at 4.2 m
    ! 18 = ws in m/s at 74 m
    ! 19 = ws in m/s  at 33.6 m
    ! 20 = ws in m/s  at 16.8 m
    ! 21 = ws in m/s  at 8.4 m
    ! 22 = wd at 33.6 m
    ! 23 = wd at 16.8 m
    ! 24 = cs - sum of dmps and aps with rh-correction for HNO3
    ! 25 = global radiation
    ! 26 = solar par radiation
    ! 27 = coa for 1 nm - sum of dmps and aps with rh-correction for HNO3
    ! 28 = albedo
    ! 29-32 wind data from sonic anemometers
    ntime_local = MonthDay(now_date(1), now_date(2))*48 + 1  ! yyyymm01-00:00:00 to yyyy(mm+1)01-00:00:00, so 1 is added

    ! Input of measured spectral radiation data (every half-hour data), not used now due to measurement problems
    ! OPEN(unit=4,file = TRIM(ADJUSTL(input_dir_station_data))//'/hyy_swr.txt',status='old')
    ! READ(4,*)((swr_hyy(i20,j20),i20=1,47),j20=1,ntime_local)
    ! CLOSE(unit=4)

    ! Input of measured gases O3, SO2, NO, NO2, CO and air (every half-hour data)
    OPEN(unit=4,file = TRIM(ADJUSTL(input_dir_station_data))//'/hyy_gas_new.txt',status='old')
    READ(4,*)((CH_gas_hyy(i20,j20),i20=1,12),j20=1,ntime_local)
    CLOSE(unit=4)
    
    if (now_date(1)>=2013) then
       OPEN(unit=2,file = TRIM(ADJUSTL(input_dir_station_data))//'/hyy_mix.txt',status='old')
       READ(2,*)((hyy_mix(i20,j20),i20=1,32),j20=1,1488)
       CLOSE(unit=2)
    else

       OPEN(unit=2,file = TRIM(ADJUSTL(input_dir_station_data))//'/hyy_mix.txt',status='old')
       READ(2,*)((hyy_mix(i20,j20),i20=1,28),j20=1,ntime_local)
       CLOSE(unit=2)

    endif

    !r content of the hyy_soil file:
    !r 1  date and time
    !r 2  Volumetric water content (m3/m3) in organic layer in -5-0 cm
    !r 3  Volumetric water content (m3/m3) in 0-5 cm 
    !r 4  Volumetric water content (m3/m3) in 5-23 cm
    !r 5  Volumetric water content (m3/m3) in 23-60 cm
    !r 6  Temperature (C) in organic layer in -5-0 cm
    !r 7  Temperature (C) in 0-5 cm
    !r 8  Temperature (C) in 5-23 cm
    !r 9  Temperature (C) in 23-60 cm

    OPEN(unit=5,file = TRIM(ADJUSTL(input_dir_station_data))//'/hyy_soil.txt',status='old')
    READ(5,*)((hyy_soil(i20,j20),i20=1,9),j20=1,ntime_local)
    CLOSE(unit=5)

    open(unit=2037,file=TRIM(ADJUSTL(input_dir_station_info)) // '/canopy.txt', status='old')
    canopyfile = 0.0
    DO j20 = 1,16
       READ(2037,*)(canopyfile(j20,i20),i20=1,9)
    ENDDO
    CLOSE(unit=2037)
    !hc = canopyfile(4,1) !This is the 2012 value 

    !Canopy height is growing ~18 cm/year in HyytiÃlÃ, but I now used the
    !measured values - check Megan_version2.f90 for more info
    IF (year_str == '2018') hc = 20.61
    IF (year_str == '2017') hc = 20.36
    IF (year_str == '2016') hc = 20.01
    IF (year_str == '2015') hc = 19.66
    IF (year_str == '2014') hc = 19.32
    IF (year_str == '2013') hc = 18.97
    IF (year_str == '2012') hc = 18.63
    IF (year_str == '2011') hc = 18.29
    IF (year_str == '2010') hc = 17.98
    IF (year_str == '2009') hc = 17.58
    IF (year_str == '2008') hc = 17.16
    IF (year_str == '2007') hc = 16.86
    IF (year_str == '2006') hc = 16.57
    IF (year_str == '2005') hc = 16.27
    IF (year_str == '2004') hc = 15.97
    IF (year_str == '2003') hc = 15.68
    IF (Treeflag == 4) THEN
       hc = 4.82
    ENDIF
    write(*,*) 'this is the updated hc: ', hc

    !The SEP for all years are based on year 2010 measurements
    !C1: measured chamber temperature (celcius): C2: measured monoterpene emission rate (ng g-1 s-1)
    !'J' refers to the fact that this data comes from Juho
    OPEN(unit=8890,file = TRIM(ADJUSTL(input_dir_station_data))//'/EFJ_mono.txt',status='old')
    READ(8890,*)((EF_meas(i20,j20),i20=1,2),j20=1,1488)
    CLOSE(unit=8890)

    ! The concentration of methane for all years are based on year 2014 meaurements
    OPEN(unit=8891,file = TRIM(ADJUSTL(input_dir_station)) // '/2014/' // month_str_Mxx // '/hyy_conc_CH4.txt', status='old')
    READ(8891,*) (hyy_ch4(j20),j20=1,ntime_local)
    CLOSE(unit=8891)

    do j20 = 1,ntime_local
      difftop1(j20) = hyy_mix(2,j20)
      dirtop1(j20)  = hyy_mix(3,j20)           !r for Welgegund this the total global radiation

      do i20 = 1,6
         tem(i20,j20) = hyy_mix(i20+5,j20)
      enddo

      do i20 = 1,6
         hum(i20,j20) = hyy_mix(i20+11,j20)
      enddo
      do i20 = 1,4
         speed(i20,j20) = hyy_mix(i20+17,j20)
      enddo
      cs_hyy(j20)     = hyy_mix(4,j20)
      cs_hyy2(j20)    = hyy_mix(24,j20)
      glo_hyy(j20)    = hyy_mix(25,j20)
      par_hyy(j20)    = hyy_mix(26,j20)
      !speed(5,j20)    = hyy_mix(5,j20)       !r this data not used
      ALBEDO(j20)     = hyy_mix(28,j20)
      sm_hyy(j20)     = hyy_soil(5,j20)       !r 5  Volumetric water content (m3/m3) in 23-60 cm
      sm_b(j20)       = hyy_soil(4,j20)       !r 4  Volumetric water content (m3/m3) in 5-23 cm
      sm_h(j20)       = hyy_soil(2,j20)       !r 2  Volumetric water content (m3/m3) in organic layer in -5-0 cm

    enddo

    do i=1,ntime_local
      dirtop1(i)  = max(0.,dirtop1(i))
      difftop1(i) = max(0.,difftop1(i))
    enddo

    IF (Treeflag == 4) THEN !CLEARCUT
        !This is used for the clear cut and includes a modification of the
        !measured data from Haapanala et al., BGS, 2012.
        !C1: daily monoterpene flux assuming constant flux from day
        !116-177. Unit: ug m^-2 h^-1
        !C2: daily monoterpene flux assuming decreasing flux from day
        !116-177 with a start value of around 7400. Unit: ug m^-2 h^-1
        !C3:  daily monoterpene flux assuming decreasing flux from day
        !116-177 with a start value of around 10000. Unit: ug m^-2 h^-1
        open(2715,file=TRIM(ADJUSTL(input_dir_station_info)) // '/SAMI_IN.txt')
        do J = 1,366
           read(2715,*) (SAMI(J,I), I=1,3)
        enddo
        close(2715)
    ENDIF

      IF (Treeflag == 3) THEN !BIRCH
          open(2716,file=TRIM(ADJUSTL(input_dir_station_info)) // '/lai_function.txt')
          do J = 1,366
             read(2716,*) LAI_function(J)
          enddo
          close(2716)
      ENDIF

  ELSEIF (STATION .EQ. 'MAN') THEN

     OPEN(unit=4,file = TRIM(ADJUSTL(input_dir_station_data))//'/Gas_filled.txt',status='old')
     READ(4,*) ((Gas_in(i20,j20),j20=1,6),i20=1,1488)

     CLOSE(unit=4)

     OPEN(unit=2,file = TRIM(ADJUSTL(input_dir_station_data))//'/Mix_filled.txt',status='old')
     
     READ(2,*) ((Mix_in(i20,j20),j20=1,35),i20=1,1488)
     CLOSE(unit=2)


     OPEN(unit=5,file = TRIM(ADJUSTL(input_dir_station_data))//'/Soil.txt',status='old')
     READ(5,*)((hyy_soil(i20,j20),i20=1,10),j20=1,1488)
     CLOSE(unit=5)
     
     
     OPEN(unit=5,file = TRIM(ADJUSTL(input_dir_station_data))//'/sounding.txt',status='old')
     READ(5,*)((sounding(i20,j20),j20=1,8),i20=1,1809)
     CLOSE(unit=5)
 
! Columns in Mix_filled.txt    
!! 1          % day of year (DOY)
!! 2          % Diffuse solar radiation (W/m2) 
!! 3          % Direct solar radiation (W/m2)
!! 4          % Global radiation/ incoming shortwave radiation (W/m2)
!! 5          % incoming PAR (micromols/m2/s) at 3.7m
!! 6:10       % Temperature (K) at 2 7 16 30 43m
!! 11:15      % Humidity (kg/m3) at 2 7 16 30 43m
!! 16:19      % Pressure (Pa) at 2 7 16 43m
!! 20:24      % Wind speed (m/s) at 2 7 16 30 43m
!! 25         % condensation sink for h2so4
!! 26         % condensation sink for hno3
!! 27:31      % in %.
!! 32:35      % Rlw.in, Rlw.out, Rsw.in, Rsw.out at 22m         


       do j20 = 1,1488

          difftop1(j20) = Mix_in(j20,2)
          dirtop1(j20)  = Mix_in(j20,3)

          do i20 = 1,5
             tem(i20,j20) = Mix_in(j20,i20+5)
          enddo

          do i20 = 1,5
             hum(i20,j20) = Mix_in(j20,i20+10)
          enddo

          do i20 = 1,5
             speed(i20,j20) = Mix_in(j20, i20+19)
          enddo

          cs_hyy(j20)     = Mix_in(j20,25)
          cs_hyy2(j20)    = Mix_in(j20,26)
          glo_hyy(j20)    = Mix_in(j20,4)
          par_hyy(j20)    = Mix_in(j20,5)
          !speed(5,j20)    = hyy_mix(5,j20)
          sm_hyy(j20)     = hyy_soil(2,j20)

       enddo
       
       do i=1,1488
          dirtop1(i)  = max(0.,dirtop1(i))
          difftop1(i) = max(0.,difftop1(i))
       enddo

    ENDIF

!!$
!!$    open(unit=12222,file = TRIM(ADJUSTL(output_dir))//'/QQQ1.dat')        ! air moisture, kg/m3 
!!$    do j20 = 1, 1488
!!$       write(12222,'(5E20.8)') (hum(i20,j20),i20=1,5)
!!$    enddo
!!$    close(unit=12222)

    abl = 2
    if (TRIM(ADJUSTL(STATION)) .eq. 'hyytiala') then
       if (abl .eq. 1) then

          open(unit=3,file = TRIM(ADJUSTL(input_dir_station_data))//'/border.txt',status='old')
          read(3,*)((border(i20,j20),i20=1,5),j20=1,1488)
          close(unit=3)

       else

          !open(unit=3,file = TRIM(ADJUSTL(input_dir_station_data))//'/border_new.txt',status='old')
          !read(3,*)((border_abl(i20,j20),i20=1,6),j20=1,125)
          !close(unit=3)


       endif
    endif

    IF (ECMWFflag2 == 1) THEN
      ntime_ECMWF = 4 * month_length(mon)  ! 0, 6, 12, 18 every day

      ! OPEN(unit=11,file = TRIM(ADJUSTL(input_dir_station_data))//'/O3',status='old')
      ! READ(11,*)((INPUT_O3_mixing_ratio(i20,j20),j20=1,43),i20=1,ntime_ECMWF)

      OPEN(unit=12,file = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_qv.txt',status='old')
      READ(12,*)((input_qv(i20,j20),j20=1,43),i20=1,ntime_ECMWF)

      OPEN(unit=13,file = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_uwind.txt',status='old')
      READ(13,*)((input_uwind(i20,j20),j20=1,43),i20=1,ntime_ECMWF)

      OPEN(unit=14,file = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_vwind.txt',status='old')
      READ(14,*)((input_vwind(i20,j20),j20=1,43),i20=1,ntime_ECMWF)

      OPEN(unit=15,file = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_temp.txt',status='old')
      READ(15,*)((input_temp(i20,j20),j20=1,43),i20=1,ntime_ECMWF)

      OPEN(unit=16,file = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_geop.txt',status='old')
      READ(16,*)((input_geop(i20,j20),j20=1,43),i20=1,ntime_ECMWF)

!      OPEN(unit=17,file = TRIM(ADJUSTL(input_dir_station_data))//'/RH',status='old')
!      READ(17,*)((INPUT_RH(i20,j20),j20=1,43),i20=1,ntime_ECMWF)

      ! READ ONE MORE DATAPOINT IN THE FOWLLING DAY
      ntime_ECMWF = ntime_ECMWF+1
      !READ(11,*,END=8888)(INPUT_O3_mixing_ratio(ntime_ECMWF,j20),j20=1,43)
      !CLOSE(unit=11)

      READ(12,*,END=8888)(input_qv(ntime_ECMWF,j20),j20=1,43)
      CLOSE(unit=12)

      READ(13,*)(input_uwind(ntime_ECMWF,j20),j20=1,43)
      CLOSE(unit=13)

      READ(14,*)(input_vwind(ntime_ECMWF,j20),j20=1,43)
      CLOSE(unit=14)

      READ(15,*)(input_temp(ntime_ECMWF,j20),j20=1,43)
      CLOSE(unit=15)

      READ(16,*)(input_geop(ntime_ECMWF,j20),j20=1,43)
      CLOSE(unit=16)

      !READ(17,*)(INPUT_RH(ntime_ECMWF,j20),j20=1,43)
      !CLOSE(unit=17)

      GOTO 9999

8888   CONTINUE


       ! if (mon .eq. 12) then
       if (.TRUE.) then
          ! CLOSE (unit=11)
          CLOSE (unit=12)
          CLOSE (unit=13)
          CLOSE (unit=14)
          CLOSE (unit=15)
          CLOSE (unit=16)
          ! INPUT_O3_mixing_ratio(ntime_ECMWF,:) = INPUT_O3_mixing_ratio(ntime_ECMWF-1,:)  
          input_qv(ntime_ECMWF,:) = input_qv(ntime_ECMWF-1,:)  
          input_uwind(ntime_ECMWF,:) = input_uwind(ntime_ECMWF-1,:)  
          input_vwind(ntime_ECMWF,:) = input_vwind(ntime_ECMWF-1,:)  
          input_temp(ntime_ECMWF,:) = input_temp(ntime_ECMWF-1,:)  
          input_geop(ntime_ECMWF,:) = input_geop(ntime_ECMWF-1,:)  

       else

          ! CLOSE (unit=11)
          CLOSE (unit=12)
          CLOSE (unit=13)
          CLOSE (unit=14)
          CLOSE (unit=15)
          CLOSE (unit=16)
          !CLOSE (unit=17)
          WRITE(next_month, '(I2.2)') mon+1
          month_str_Mxx = 'M'//next_month

          !! read in the first data point in the following month 
          ! OPEN(unit=11,file = TRIM(ADJUSTL(input_dir_station_data))//'/O3',status='old')
          ! READ(11,*)(INPUT_O3_mixing_ratio(ntime_ECMWF,j20),j20=1,43)
          ! CLOSE(unit=11)

          OPEN(unit=12,file = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_qv.txt',status='old')
          READ(12,*)(input_qv(ntime_ECMWF,j20),j20=1,43)
          CLOSE(unit=12)

          OPEN(unit=13,file = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_uwind.txt',status='old')
          READ(13,*)(input_uwind(ntime_ECMWF,j20),j20=1,43)
          CLOSE(unit=13)

          OPEN(unit=14,file = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_vwind.txt',status='old')
          READ(14,*)(input_vwind(ntime_ECMWF,j20),j20=1,43)
          CLOSE(unit=14)

          OPEN(unit=15,file = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_temp.txt',status='old')
          READ(15,*)(input_temp(ntime_ECMWF,j20),j20=1,43)
          CLOSE(unit=15)

          OPEN(unit=16,file = TRIM(ADJUSTL(input_dir_station_data))//'/ECMWF_geop.txt',status='old')
          READ(16,*)(input_geop(ntime_ECMWF,j20),j20=1,43)
          CLOSE(unit=16)

          !OPEN(unit=17,file = TRIM(ADJUSTL(input_dir_station_data))//'/RH',status='old')
          !READ(17,*)(INPUT_RH(ntime_ECMWF,j20),j20=1,43)
          !CLOSE(unit=17)

          WRITE(next_month, '(I2.2)') mon
          month_str_Mxx = 'M'//next_month

       endif

9999   CONTINUE

       input_wind = input_uwind   ! this done for getting the time stamp (first 6 columns)
       DO i = 7, 43  
          input_wind(:,i) = sqrt(input_uwind(:,i)**2+input_vwind(:,i)**2)
       ENDDO

       !!       ntime_ECMWF = ntime_ECMWF - 1

       input_geom = input_geop / grav  ! geopotential height to geometric height

    ENDIF

   !r added by rosa
   ! long wave radiation (downwelling) from reanalysis
   OPEN(unit=11,file = TRIM(ADJUSTL(input_dir_station_data))//'/LWR.txt',status='old')
   READ(11,*) (LWR_in(j20),j20=1, (ntime_local/6+2))
   CLOSE(unit=11)
   !r 
   soil_flux = -999
   OPEN(unit=11,file = TRIM(ADJUSTL(input_dir_station_data))//'/soil_heat_flux.txt',status='old')
   READ(11,*) (soil_flux(j20),j20=1,ntime_local)  ! heat flux to the soil based on measurements
   CLOSE(unit=11)   
END SUBROUTINE READ_DATA_OLD


SUBROUTINE CONVERT_ECMWF

  ECMWF_uwind = 0.0_dp
  ECMWF_vwind = 0.0_dp
  ECMWF_temp  = 0.0_dp
  ECMWF_qv    = 0.0_dp
  ECMWF_rhov  = 0.0_dp
  ECMWF_strd  = input_strd(:, 7)

  ECMWF_TIME = input_uwind(:, 1:6)


  ! Interpolate input ECMWF data to model levels from ECMWF pressure levels (geometric heights)
  DO j = 1, ntime_ECMWF
    IF (TRIM(ADJUSTL(STATION)) == 'hyytiala') THEN
      CALL interp_1d( 37, input_geom(j, 7:), input_uwind(j, 7:), kz, z+masl, ECMWF_uwind(j, :) )
      CALL interp_1d( 37, input_geom(j, 7:), input_vwind(j, 7:), kz, z+masl, ECMWF_vwind(j, :) )
      CALL interp_1d( 37, input_geom(j, 7:), input_temp (j, 7:), kz, z+masl, ECMWF_temp (j, :) )
      CALL interp_1d( 37, input_geom(j, 7:), input_qv   (j, 7:), kz, z+masl, ECMWF_qv   (j, :) )
      CALL interp_1d( 37, input_geom(j, 7:), input_rhov (j, 7:), kz, z+masl, ECMWF_rhov (j, :) )

      ECMWF_dtemp(j) = (input_temp(j,31)-input_temp(j,33))/(input_geom(j,31)-input_geom(j,33))  ! 31: 850 hPa, 33: 900 hPa

    ELSEIF (TRIM(ADJUSTL(STATION)) == 'Manitou') THEN
      CALL interp_1d( 37, input_geom(j, 7:), input_temp(j, 7:), kz, z+masl, ECMWF_temp(j, :) )
      CALL interp_1d( 37, input_geom(j, 7:), input_uwind(j, 7:), kz, z+masl, ECMWF_uwind(j, :) )
      CALL interp_1d( 37, input_geom(j, 7:), input_vwind(j, 7:), kz, z+masl, ECMWF_vwind(j, :) )
      CALL interp_1d( 37, input_geom(j, 7:), input_qv(j, 7:), kz, z+masl, ECMWF_qv(j, :) )

      ECMWF_dtemp(j) = (input_temp(j,28)-input_temp(j,30))/(input_geom(j,28)-input_geom(j,30)) ! temp gradient 3600 and 2200m above see level
    ENDIF
  ENDDO

  ! Set top boundary layer values to ECMWF data
  border_abl(1, 1:249) = ECMWF_temp (1:249, kz)
  border_abl(2, 1:249) = ECMWF_uwind(1:249, kz)
  border_abl(3, 1:249) = ECMWF_vwind(1:249, kz)
  border_abl(4, 1:249) = ECMWF_rhov (1:249, kz)
  border_abl(5, 1:249) = ECMWF_dtemp(1:249)

END SUBROUTINE CONVERT_ECMWF


SUBROUTINE output_ascii_write()
  CHARACTER(LEN=10) :: str_prefix

  output_number = 0
  !================================================================================================!
  ! Meteorology
  !================================================================================================!
  CALL output_ascii_write_one('TEMP', ta1)
  CALL output_ascii_write_one('QQQ', qa1)        ! [kg m-3], absolute humidity
  CALL output_ascii_write_one('UUU', u1)        ! streamwise velocity, m/s
  CALL output_ascii_write_one('VVV', v1)        ! lateral velocity, m/s
  CALL output_ascii_write_one('WWW', w1)        ! vertical velocity, m/s 
  CALL output_ascii_write_one('TKE', bt1)        ! turbul kinetic energy, m2/s2 
  CALL output_ascii_write_one('DIS', bt1*dbt1)        ! dissipation rate TKE, m2/s3
  CALL output_ascii_write_one('EDD', kt1)        ! eddy diff momentum, m2/s
  CALL output_ascii_write_one('ALTxEDD', alt1*kt1)    ! eddy diff scalar, m2/s
  CALL output_ascii_write_one('LLL', l1)        ! mixing length, m
  CALL output_ascii_write_one('ALT', alt1)        ! converse Schmidt number
  CALL output_ascii_write_one('RIH', rih)        ! Richardson number
  CALL output_ascii_write_one('TSN', tsn)        ! sunlit leaf temperature, K
  CALL output_ascii_write_one('TSD', tsd)        ! shaded leaf temperature, K
  CALL output_ascii_write_one('QSN', qsn)        ! sunlit leaf moisture, kg/m3
  CALL output_ascii_write_one('QSD', qsd)        ! shaded leaf moisture, kg/m3
  CALL output_ascii_write_one('Ustar', ur)      ! local friction velocity, m/s
  CALL output_ascii_write_one('TSOIL', tsoil1)      ! soil temperature, K
  CALL output_ascii_write_one('WIND', sqrt(u1*u1+v1*v1))       ! mean wind, m/s
  CALL output_ascii_write_one('H_TURB', fluxh3)     ! turbulent heat flux Wt/m2  
  CALL output_ascii_write_one('H_INT', fluxh)      ! canopy heat flux, Wt/m2
  CALL output_ascii_write_one('LE_TURB', fluxle3)    ! turb latent flux, Wt/m2
  CALL output_ascii_write_one('LE_INT', fluxle)     ! canopy latent flux, Wt/m2
  CALL output_ascii_write_one('Pres', pres)       ! pressure in Pa
  CALL output_ascii_write_one('Rh', RH)         ! realtive humidity in %
  CALL output_ascii_write_one('H2O', CH_H2O)        ! H2O in molecules / cm3 (text file)
  CALL output_ascii_write_one('TSN_M', tsn_megan)      ! sunlit leaf temperature, K from Megan
  CALL output_ascii_write_one('TSD_M', tsd_megan)      ! shaded leaf temperature, K from Megan
  CALL output_ascii_write_one('VPD_S', EM_VPD_S)      ! VPD for sunny leafs in kPa
  CALL output_ascii_write_one('VPD_C', EM_VPD_C)      ! VPD for shaded leafs in kPa
  CALL output_ascii_write_one('t_nud', tnud)      ! 
  CALL output_ascii_write_one('q_nud', qnud)      ! 
  CALL output_ascii_write_one('u_nud', unud)      ! 
  CALL output_ascii_write_one('BALANCE', (/balans/), '(6I6, E20.8)')    ! radiation closure, Wt/m2 
  CALL output_ascii_write_one('SKY EMIS', (/emm/), '(6I6, E20.8)')   ! sky emissivity, approx 
  CALL output_ascii_write_one('ALBEDO', (/albedo_f/), '(6I6, E20.8)')     ! cover albedo
  CALL output_ascii_write_one('SOIL_FLUX', (/pp/), '(6I6, E20.8)')  ! flux into soil, Wt/m2
  CALL output_ascii_write_one('ABL_HEIGHT', (/hlayer/), '(6I6, E20.8)') ! ABL height, m
  CALL output_ascii_write_one('Zenith', (/BETA/), '(6I6, E20.8)')     ! Solar zenith angle
  CALL output_ascii_write_one('fktt_fktd', (/fktt, fktd/), '(6I6, 2E20.8)')
  CALL output_ascii_write_one('DIR_RAD', (/rsnt/), '(6I6, E20.8)')    ! downward direct solar radiation (W/m2)
  CALL output_ascii_write_one('DIFF_RAD', (/rskt/), '(6I6, E20.8)')   ! diffuse direct solar radiation (W/m2)
  CALL output_ascii_write_one('LWR_DOWN', (/fird(k_canopy)/), '(6I6, E20.8)')   ! LWR down at the top of the canopy (W/m2)
  CALL output_ascii_write_one('LWR_UP', (/firu(k_canopy)/), '(6I6, E20.8)')     ! LWR up at the top of the canopy (W/m2)
  CALL output_ascii_write_one('PAR_UP', (/fphu(k_canopy)/), '(6I6, E20.8)')     ! PAR up at the top of the canopy (W/m2)
  CALL output_ascii_write_one('NIR_UP', (/fniu(k_canopy)/), '(6I6, E20.8)')     ! NIR up at the top of the canopy (W/m2)
  CALL output_ascii_write_one('LH_FLUX', (/fluxle3(k_canopy)/), '(6I6, E20.8)')    ! latent heat flux at the top of the canopy (W/m2)
  CALL output_ascii_write_one('SH_FLUX', (/fluxh3(k_canopy)/), '(6I6, E20.8)')     ! sensible heat flux at the top of the canopy (W/m2)
  CALL output_ascii_write_one('SOIL_Q', (/wg1/), '(6I6, 2E20.8)')     ! soil water content
  CALL output_ascii_write_one('Rd_dir', (/rsnt/), '(6I6, E20.8)')     ! downward direction radiation
  CALL output_ascii_write_one('Rd_dif', (/rskt/), '(6I6, E20.8)')     ! downward diffuse radiation
  CALL output_ascii_write_one('air', air)     ! air number concentration

  !================================================================================================!
  ! Gas and gas flux
  !================================================================================================!
  ! str_prefix = 'Gas_'
  DO I=1,outspc_count
    IF (outspc_cheminds(I) > 0) THEN
      gname = SPC_NAMES(outspc_cheminds(I))
      CALL output_ascii_write_one('Gas_'//TRIM(ADJUSTL(gname)), CH_CONS_ALL(:, outspc_cheminds(I)))
      CALL output_ascii_write_one('Flux_'//TRIM(ADJUSTL(gname)), CH_CONS_FLUX(:, outspc_cheminds(I)))
      ! CALL output_ascii_write_one('Qconc_'//TRIM(ADJUSTL(gname)), Qconc(:, outspc_cheminds(I)))
      ! CALL output_ascii_write_one('Qemis_'//TRIM(ADJUSTL(gname)), Qemis(:, outspc_cheminds(I)))
      ! CALL output_ascii_write_one('Qchem_'//TRIM(ADJUSTL(gname)), Qchem(:, outspc_cheminds(I)) - Qemis(:, outspc_cheminds(I)))
      ! CALL output_ascii_write_one('Qdepo_'//TRIM(ADJUSTL(gname)), Qdepo(:, outspc_cheminds(I)))
      ! CALL output_ascii_write_one('Qturb_'//TRIM(ADJUSTL(gname)), Qturb(:, outspc_cheminds(I)))
      ! CALL output_ascii_write_one('Qturbnow_'//TRIM(ADJUSTL(gname)), Qturb_now(:, outspc_cheminds(I)))
      ! CALL output_ascii_write_one('Qfluxdep_'//TRIM(ADJUSTL(gname)), Qfluxdep(:, outspc_cheminds(I)))
    END IF
  END DO

  !================================================================================================!
  ! Aerosol
  !================================================================================================!
  IF (AeroFlag == 1) THEN
  !================================================================================================!
  ! HOMs
    if ( any(outspc_HOM10_ind > 0) ) then
      CALL output_ascii_write_one('HOM10', sum(CH_CONS_ALL(:, pack(outspc_HOM10_ind, outspc_HOM10_ind>0)),2))
      CALL output_ascii_write_one('HOM20', sum(CH_CONS_ALL(:, pack(outspc_HOM20_ind, outspc_HOM20_ind>0)),2))
    end if
  !================================================================================================!
    DO I=1, uhma_sections
      CALL output_ascii_write_one('nconc_aerosol', n_conc(:, I), isappend_in=.TRUE.)

      ! Do not advance the file id
      output_number = output_number - 1
    END DO
     CALL output_ascii_write_one('CH_CS', (/CH_CS/))
!     CALL output_ascii_write_one('RADIUS', (/RADIUS(13,:)/), AR_OUTPUT_FORMAT)

   CALL output_ascii_write_one('NUC_RATE', (/NUC_RATE(1:kz)/))
   CALL output_ascii_write_one('ION_NUC_RATE', (/ION_NUC_RATE(1:kz)/)) 
   DO k = 1, kz 
     write(layerstamp,'(F6.1)') z(k)
     CALL output_ascii_write_one('N_CONC'//layerstamp, (/N_CONC(k,:)/), AR_OUTPUT_FORMAT)
     CALL output_ascii_write_one('PAR_FLUX'//layerstamp, (/PAR_FLUX(k,:)/), AR_OUTPUT_FORMAT)
     CALL output_ascii_write_one('GR'//layerstamp, (/GR(k,:)/), AR_OUTPUT_FORMAT)
   ENDDO
  END IF

  !================================================================================================!
  ! OH, O3 and NO3 reactivity
  !================================================================================================!
    IF (CH_oh_count > 0) THEN
       DO CH_oh_i = 1, CH_oh_count
       CALL output_ascii_write_one('GAS_'//TRIM(SPC_NAMES(CH_oh_indices(CH_oh_i))), CH_oh_cons3(:,CH_oh_i,3))
       ENDDO
    ENDIF
    IF (CH_o3_count > 0) THEN
       DO CH_o3_i = 1, CH_o3_count
       CALL output_ascii_write_one('GAS_'//TRIM(SPC_NAMES(CH_o3_indices(CH_o3_i))), CH_o3_cons3(:,CH_o3_i,3))
       ENDDO
    ENDIF
    IF (CH_no3_count > 0) THEN
       DO CH_no3_i = 1, CH_no3_count
       CALL output_ascii_write_one('GAS_'//TRIM(SPC_NAMES(CH_no3_indices(CH_no3_i))), CH_no3_cons3(:,CH_no3_i,3))
       ENDDO
    ENDIF

  !
  CALL output_ascii_write_one('NO2_Photolysis',(/CH_J_values_ALL(:,4)/))
  !================================================================================================!
  ! Emission fluxes
  !================================================================================================!
  ! DO I=1,SIZE(emi_ind)
  !   CALL output_ascii_write_one('Emi_'//SPC_NAMES(emi_ind(I)), EM_EMI(:, I))
  ! END DO

  DO I=1, outemi_count
    CALL output_ascii_write_one('Emi_'//SPC_NAMES(outemi_cheminds(I)), EM_EMI(:, outemi_meganinds(I)))
  END DO

  !================================================================================================!
  ! Output by Putian
  !================================================================================================!
  !***** Vd *****!
  DO I=1,outspc_count
    IF (outspc_cheminds(I) > 0) THEN
      CALL output_ascii_write_one('Vd_'//outspc_names(I), vdep(:, outspc_cheminds(I)))
    END IF
  END DO

  CALL output_ascii_write_one('eta', psn)
  CALL output_ascii_write_one('mu', psk)
  CALL output_ascii_write_one('gl', gl)
  CALL output_ascii_write_one('gd', gd)
  CALL output_ascii_write_one('RSu_PAR', fphu)
  CALL output_ascii_write_one('RSd_PAR', fphd)
  CALL output_ascii_write_one('rac', dep_output(:, ind_O3, 1))
  CALL output_ascii_write_one('rstm', dep_output(:, ind_O3, 2))
  CALL output_ascii_write_one('rbveg', dep_output(:, ind_O3, 3))
  CALL output_ascii_write_one('frac_ws', dep_output(:, ind_O3, 4))
  CALL output_ascii_write_one('rleaf', dep_output(:, ind_O3, 5))
  CALL output_ascii_write_one('rleafw', dep_output(:, ind_O3, 6))
  CALL output_ascii_write_one('rsveg', dep_output(:, ind_O3, 7))
  CALL output_ascii_write_one('rswet', dep_output(:, ind_O3, 8))
  CALL output_ascii_write_one('rtot', dep_output(:, ind_O3, 9))
  CALL output_ascii_write_one('fvpd', dep_output(:, ind_O3, 10))
  CALL output_ascii_write_one('rstm_h2o', dep_output(:, ind_O3, 11))
  CALL output_ascii_write_one('gs_h2o_sn', dep_output(:, ind_O3, 12))
  CALL output_ascii_write_one('gs_h2o_sd', dep_output(:, ind_O3, 13))
  CALL output_ascii_write_one('gs_h2o_avg', dep_output(:, ind_O3, 14))
  CALL output_ascii_write_one('gstm_h2o_sn', gstm_h2o_sn)
  CALL output_ascii_write_one('gstm_h2o_sd', gstm_h2o_sd)

END SUBROUTINE output_ascii_write


SUBROUTINE output_ascii_write_one(var_name, var, output_format, isappend_in)
  CHARACTER(LEN=*) :: var_name
  REAL(dp) :: var(:)
  CHARACTER(LEN=*), OPTIONAL :: output_format
  LOGICAL, OPTIONAL :: isappend_in

  CHARACTER(LEN=100) :: current_format
  INTEGER :: file_id
  LOGICAL :: isappend

  IF (PRESENT(output_format)) THEN
    current_format = output_format
  ELSE
    current_format = CH_OUTPUT_FORMAT
  END IF

  IF (PRESENT(isappend_in)) THEN
    isappend = isappend_in
  ELSE
    isappend = .FALSE.
  END IF

  output_number = output_number + 1
  file_id = UNIT_START+output_number
  IF (.not. isopen(output_number)) THEN
    OPEN( UNIT=file_id, FILE=TRIM(ADJUSTL(output_dir))//'/'//TRIM(var_name)//'.dat' )
    IF (isappend) THEN
      WRITE(file_id, *) ''
      CLOSE(file_id)
      OPEN(UNIT=file_id, FILE=TRIM(ADJUSTL(output_dir))//'/'//TRIM(var_name)//'.dat', POSITION='APPEND', ACTION='WRITE', STATUS='OLD')
    END IF
    isopen(output_number) = .TRUE.
  END IF
  WRITE(file_id, TRIM(current_format)) now_date(1:6), var
END SUBROUTINE output_ascii_write_one


SUBROUTINE open_output_files
  IF (kz .EQ. 51) THEN 
    AR_OUTPUT_FORMAT = FORMAT1
    CH_OUTPUT_FORMAT = FORMAT7
  ELSE
    AR_OUTPUT_FORMAT = FORMAT1
    CH_OUTPUT_FORMAT = FORMAT8
  ENDIF

  ! LUXI BE AWARE THAT YOU ARE ALREADY USING UNIT=101 FOR THE GAS OUTPUT!
  open(unit=101,file = TRIM(ADJUSTL(output_dir))//'/border1.dat')
  write(101,'(249E12.3)') border_abl(1,1:249)
  write(101,'(249E12.3)') border_abl(2,1:249)
  write(101,'(249E12.3)') border_abl(3,1:249)
  write(101,'(249E12.3)') border_abl(4,1:249)
  write(101,'(249E12.3)') border_abl(5,1:249)
  close(unit=101)

  open(unit=102,file = TRIM(ADJUSTL(output_dir))//'/border2.dat')
  write(102,'(249E12.3)') border_abl(1,1:249)
  write(102,'(249E12.3)') border_abl(2,1:249)
  write(102,'(249E12.3)') border_abl(3,1:249)
  write(102,'(249E12.3)') border_abl(4,1:249)
  write(102,'(249E12.3)') border_abl(5,1:249) 
  close(unit=102)

  !----------------- open file for aerosols----------------------------------------------------------  
  IF (Aeroflag .EQ. 2) THEN
    open(unit=1001, file = TRIM(ADJUSTL(output_dir))//'/NUC_RATE.dat' )
    write(1001, CH_OUTPUT_FORMAT) 0, 0, 0, 0, 0, 0, z

    open(unit=1002, file = TRIM(ADJUSTL(output_dir))//'/ION_NUC_RATE.dat' )
    write(1002, CH_OUTPUT_FORMAT) 0, 0, 0, 0, 0, 0, z

    open(unit=1003, file = TRIM(ADJUSTL(output_dir))//'/CS_Measured.dat' )
    !write(1003, CH_OUTPUT_FORMAT) 0, 0, 0, 0, 0, 0, z

    open(unit=1011, file = TRIM(ADJUSTL(output_dir))//'/RADIUS.dat' )

    DO k = 1, kz
      write(layerstamp,'(F6.1)') z(k)

      open(unit=1500+k, file=TRIM(ADJUSTL(output_dir))//'/GR'//layerstamp//'.dat')
      open(unit=1600+k, file=TRIM(ADJUSTL(output_dir))//'/PAR_num'//layerstamp//'.dat')
      open(unit=1700+k, file=TRIM(ADJUSTL(output_dir))//'/Sink'//layerstamp//'.dat')
      open(unit=1900+k, file=TRIM(ADJUSTL(output_dir))//'/Vap'//layerstamp//'.dat')

      !          open(unit=2000+k, file=TRIM(ADJUSTL(output_dir))//'/Par_flux'//layerstamp//'.dat') 
    ENDDO
  ENDIF

  IF (ECMWFflag2 == 1) THEN
    ! write converted ecmwf data to files
    OPEN(unit=700,file = TRIM(ADJUSTL(output_dir))//'/ECMWF_temp.dat')        
    write(700,CH_OUTPUT_FORMAT) 0, 0, 0, 0, 0, 0, z

    OPEN(unit=701,file = TRIM(ADJUSTL(output_dir))//'/ECMWF_Specific_Humidity.dat')        
    write(701,CH_OUTPUT_FORMAT) 0, 0, 0, 0, 0, 0, z

    OPEN(unit=702,file = TRIM(ADJUSTL(output_dir))//'/ECMWF_wind.dat')        
    write(702,CH_OUTPUT_FORMAT) 0, 0, 0, 0, 0, 0, z

    OPEN(unit=703,file = TRIM(ADJUSTL(output_dir))//'/ECMWF_RH.dat')        
    write(703,CH_OUTPUT_FORMAT) 0, 0, 0, 0, 0, 0, z

    DO k = 1, ntime_ECMWF
      WRITE (700,FORMAT7) (ECMWF_TIME(k,1:6)), (ECMWF_temp(k,i),i=1,kz)
      WRITE (701,FORMAT7) (ECMWF_TIME(k,1:6)), (ECMWF_qv(k,i),i=1,kz)
      WRITE (702,FORMAT7) (ECMWF_TIME(k,1:6)), (ECMWF_wind(k,i),i=1,kz)
      WRITE (703,FORMAT7) (ECMWF_TIME(k,1:6)), (ECMWF_RH(k,i),i=1,kz)
    ENDDO

    CLOSE(UNIT = 700)
    CLOSE(UNIT = 701)
    CLOSE(UNIT = 702)
    CLOSE(UNIT = 703)
  ENDIF

  !==============================================================================================================!
  ! Initialize stuff for output
  !==============================================================================================================!
  isopen = .FALSE.

END SUBROUTINE open_output_files


!==============================================================================!
! New output write subroutines using netcdf format
!==============================================================================!
SUBROUTINE output_write()
  INTEGER :: I
  INTEGER, PARAMETER :: n_other=35
  LOGICAL, SAVE :: FIRST_TIME=.TRUE.

  hour = INT(daytime/3600.)
  minute = INT((daytime-hour*3600.)/60.)
  second = INT(daytime-hour*3600.-minute*60.)

  nmx=nmetro 
  nmetro=nmetro+1

  ! DATA AT ABOUT SMEAR LEVEL (time, variable)

  albedo_f=max(0.,(fniu(nz)+fphu(nz))/(rads2+1.))

  ! UVA and swr_hyy are used for old version input and output
  ! UVA = 0.
  ! DO J = 9,24
  !    UVA = UVA + swr_hyy(j,nxodrad)
  ! ENDDO

  ! IF (FIRST_TIME) THEN
  !   FIRST_TIME = .FALSE.
  !   !
  !   ! Write some critical information in the model.
  !   !
  !   OPEN(UNIT=10, FILE = TRIM(ADJUSTL(OUTPUT_DIR))//'/Info.dat')

  !   WRITE(10,'(A20, I10)') 'kz', kz
  !   WRITE(10,'(A20, I10)') 'EmiFlag', EmiFlag 
  !   WRITE(10,'(A20, I10)') 'ChemFlag', ChemFlag
  !   WRITE(10,'(A20, I10)') 'GasdrydepFlag', GasdrydepFlag
  !   WRITE(10,'(A20, I10)') 'AeroFlag', AeroFlag
  !   WRITE(10,'(A20, I10)') 'SoilFlag', SoilFlag
  !   WRITE(10,'(A20, I10)') 'ECMWFFlag2', ECMWFFlag2
  !   WRITE(10,'(A20, I10)') 'nclo_end', nclo_end
  !   WRITE(10,'(A20, ES25.14E3)') 'dt_mete', dt_mete
  !   WRITE(10,'(A20, ES25.14E3)') 'dt_chem', dt_chem
  !   WRITE(10,'(A20, ES25.14E3)') 'dt_aero', dt_aero
  !   WRITE(10,'(A20, ES25.14E3)') 'dt_uhma', dt_uhma
  !   WRITE(10,'(A20, ES25.14E3)') 'hc', hc
  !   WRITE(10,'(A20, I10)') 'abl', abl
  !   WRITE(10,'(A20, ES25.14E3)') 'LAI_tot', LAI_tot
  !   WRITE(10,'(A20, ES25.14E3)') 'LAI_curv', LAI_curv
  !   WRITE(10,'(A20, ES25.14E3)') 'LAI_proj', LAI_proj
  !   WRITE(10,'(A20, 51ES25.14E3)') 'z', z    ! height levels
  !   WRITE(10,'(A20, 51ES25.14E3)') 's1', s1    ! LAD for meteorology
  !   WRITE(10,'(A20, 51ES25.14E3)') 's2', s2    ! LAD for MEGAN

  !   IF (AeroFlag == 1) THEN
  !     WRITE(10,*) 'Nucleation scheme:', options%nuc_number
  !     WRITE(10,*) 'Nucleation rate:', ambient%nuc_coeff
  !     WRITE(10,*) 'Concentration of reaction products from NO3 oxidation is first increased by ', &
  !                no3_coe, ' times.'
  !     WRITE(10,*) 'Concentration of reaction products from OH, O3 and NO3 oxidation is then all increased by ', &
  !                vap_coe, ' times.'
  !   ENDIF

  !   CLOSE(10)
  ! END IF

  ! CALL output_ascii_write() 

  IF (FIRST_TIME) THEN
    write(start_date_string, '(I0.4,A1,I0.2,A1,I0.2,A1,I0.2,A1,I0.2,A1,I0.2)') &
      start_date(1), '.', start_date(2), '.', start_date(3), ' ', start_date(4), ':', start_date(5), ':', start_date(6)
    write(first_day_of_month_string, '(I0.4,A1,I0.2,A1,I0.2,A1,I0.2,A1,I0.2,A1,I0.2)') &
      start_date(1), '.', start_date(2), '.', 1, ' ', 0, ':', 0, ':', 0

    ! Initiate netcdf output
    call output_netcdf_init()

    ! Initiate netcdf output and close since it is only written once
    call output_netcdf_general_init()
    call output_netcdf_general_done()

    ! Initiate netcdf for meteorology output
    call output_netcdf_meteo_init()

    ! Initiate netcdf for meteorology output
    call output_netcdf_chem_init()

    ! Initiate netcdf for meteorology output
    call output_netcdf_aerosol_init()

    ! Set current time record to 0 (not written yet)
    itrec = 0

    FIRST_TIME = .FALSE.
  END IF 

  ! Update time record counter
  itrec = itrec + 1

  ! Write meteo data to the netcdf file
  call output_netcdf_meteo_write()

  ! Write chemistry data to the netcdf file
  call output_netcdf_chem_write()

  ! Write aerosol data to the netcdf file
  call output_netcdf_aerosol_write()

END SUBROUTINE output_write


!==============================================================================!
! output netcdf init subroutines
!==============================================================================!

subroutine output_netcdf_init()
  ! Initiate DIOS
  call dios_init(stat)
end subroutine output_netcdf_init


subroutine output_netcdf_general_init()
  !-----------------------------------------------------------!
  ! Init general nc file 
  ! General information is saved
  !-----------------------------------------------------------!

  ! General file name
  nc_general%fname = trim(adjustl(output_dir))//'/general.nc'
  
  ! Create file
  call dios_create(trim(nc_general%fname), DIOS_NETCDF, DIOS_REPLACE, nc_general%fid, stat)
  
  ! Global attributes
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'title', 'SOSAA general file', stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'number_of_height_levels', kz, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'EmiFlag', EmiFlag , stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'ChemFlag', ChemFlag, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'GasdrydepFlag', GasdrydepFlag, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'AeroFlag', AeroFlag, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'SoilFlag', SoilFlag, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'ECMWFFlag2', ECMWFFlag2, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'nclo_end', nclo_end, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'dt_mete', dt_mete, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'dt_chem', dt_chem, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'dt_aero', dt_aero, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'dt_uhma', dt_uhma, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'canopy_height', hc, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'abl', abl, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'LAI_tot', LAI_tot, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'LAI_curv', LAI_curv, stat)
  call dios_put_att(nc_general%fid, DIOS_GLOBAL, 'LAI_proj', LAI_proj, stat)

  ! Define dimensions
  call dios_def_dim(nc_general%fid, 'lev', kz, nc_general%dimid_lev, stat)

  ! Define variables
  call dios_def_var(nc_general%fid, 'lev', DIOS_DOUBLE, (/nc_general%dimid_lev/), &
                    nc_general%varid_lev, stat)
  call dios_put_att(nc_general%fid, nc_general%varid_lev, 'unit', 'm', stat)
  call dios_put_att(nc_general%fid, nc_general%varid_lev, 'long_name', 'height above the ground', stat)

  call dios_def_var(nc_general%fid, 'lad_meteo', DIOS_DOUBLE, (/nc_general%dimid_lev/), &
                    varid_lad_meteo, stat)
  call dios_put_att(nc_general%fid, varid_lad_meteo, 'unit', 'm2 m-3', stat)
  call dios_put_att(nc_general%fid, varid_lad_meteo, &
    'long_name', 'leaf area density for meteorology module', stat)

  call dios_def_var(nc_general%fid, 'lad_megan', DIOS_DOUBLE, (/nc_general%dimid_lev/), &
                    varid_lad_megan, stat)
  call dios_put_att(nc_general%fid, varid_lad_megan, 'unit', 'm2 m-3', stat)
  call dios_put_att(nc_general%fid, varid_lad_megan, &
    'long_name', 'leaf area density for megan module', stat)

  ! End of definition
  call dios_enddef(nc_general%fid, stat)

  ! Put variables
  call dios_put_var(nc_general%fid, nc_general%varid_lev, z, stat, start=(/1/), count=(/kz/))
  call dios_put_var(nc_general%fid, varid_lad_meteo, s1, stat, start=(/1/), count=(/kz/))
  call dios_put_var(nc_general%fid, varid_lad_megan, s2, stat, start=(/1/), count=(/kz/))

end subroutine output_netcdf_general_init


subroutine output_netcdf_meteo_init()

  !----- Init meteo nc file ------------------------------------------------------!

  ! Meteo file name
  nc_meteo%fname = trim(adjustl(output_dir))//'/meteo.nc'
  
  ! Create file
  call dios_create(trim(nc_meteo%fname), DIOS_NETCDF, DIOS_REPLACE, nc_meteo%fid, stat)
  
  ! Global attributes
  call dios_put_att(nc_meteo%fid, DIOS_GLOBAL, 'title', 'SOSAA meteorology file', stat)
  
  ! Definition of dimensions
  call dios_def_dim(nc_meteo%fid, 'time', DIOS_UNLIMITED, nc_meteo%dimid_time, stat)
  call dios_def_dim(nc_meteo%fid, 'lev', kz, nc_meteo%dimid_lev, stat)
  nc_meteo%dimids_2d = (/nc_meteo%dimid_lev, nc_meteo%dimid_time/)
  
  !----- Definition of variables -----!
  ! Unlimited dimension should be defined in the last dimid (for classic NetCDF),
  ! notice that the dims are inversed when using 'ncdump' to check the data.
  
  ! Dimension variables
  call dios_def_var(nc_meteo%fid, 'lev', DIOS_DOUBLE, (/nc_meteo%dimid_lev/), nc_meteo%varid_lev, stat)
  call dios_put_att(nc_meteo%fid, nc_meteo%varid_lev, 'unit', 'm', stat)
  call dios_put_att(nc_meteo%fid, nc_meteo%varid_lev, 'long_name', 'height above the ground', stat)
  
  call dios_def_var(nc_meteo%fid, 'time', DIOS_DOUBLE, (/nc_meteo%dimid_time/), nc_meteo%varid_time, stat)
  call dios_put_att(nc_meteo%fid, nc_meteo%varid_time, 'unit', 's', stat)
  call dios_put_att(nc_meteo%fid, nc_meteo%varid_time, 'long_name', 'time since the beginning of month', stat)
  call dios_put_att(nc_meteo%fid, nc_meteo%varid_time, 'first_day_of_month', trim(first_day_of_month_string), stat)
  call dios_put_att(nc_meteo%fid, nc_meteo%varid_time, 'start_date', trim(start_date_string), stat)
  
  ! 2D variables
  call dios_def_var(nc_meteo%fid, 'temp', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_temp, stat)
  call dios_put_att(nc_meteo%fid, varid_temp, 'unit', 'K', stat)
  call dios_put_att(nc_meteo%fid, varid_temp, 'long_name', 'air temperature', stat)
  
  call dios_def_var(nc_meteo%fid, 'rhov', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_rhov, stat)
  call dios_put_att(nc_meteo%fid, varid_rhov, 'unit', 'kg m-3', stat)
  call dios_put_att(nc_meteo%fid, varid_rhov, 'long_name', 'absolute humidity', stat)
  
  call dios_def_var(nc_meteo%fid, 'rh', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_rh, stat)
  call dios_put_att(nc_meteo%fid, varid_rh, 'unit', 'kg m-3', stat)
  call dios_put_att(nc_meteo%fid, varid_rh, 'long_name', 'relative humidity', stat)
  
  call dios_def_var(nc_meteo%fid, 'pres', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_pres, stat)
  call dios_put_att(nc_meteo%fid, varid_pres, 'unit', 'Pa', stat)
  call dios_put_att(nc_meteo%fid, varid_pres, 'long_name', 'air pressure', stat)
  
  call dios_def_var(nc_meteo%fid, 'ua', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_ua, stat)
  call dios_put_att(nc_meteo%fid, varid_ua, 'unit', 'm s-1', stat)
  call dios_put_att(nc_meteo%fid, varid_ua, 'long_name', 'zonal wind', stat)
  
  call dios_def_var(nc_meteo%fid, 'va', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_va, stat)
  call dios_put_att(nc_meteo%fid, varid_va, 'unit', 'm s-1', stat)
  call dios_put_att(nc_meteo%fid, varid_va, 'long_name', 'meridional wind', stat)
  
  call dios_def_var(nc_meteo%fid, 'tke', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_tke, stat)
  call dios_put_att(nc_meteo%fid, varid_tke, 'unit', 'm2 s-2', stat)
  call dios_put_att(nc_meteo%fid, varid_tke, 'long_name', 'turbulent kinetic energy', stat)
  
  call dios_def_var(nc_meteo%fid, 'diffm', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_diffm, stat)
  call dios_put_att(nc_meteo%fid, varid_diffm, 'unit', 'm2 s-1', stat)
  call dios_put_att(nc_meteo%fid, varid_diffm, 'long_name', 'eddy diffusivity of momentum', stat)
  
  call dios_def_var(nc_meteo%fid, 'diffh', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_diffh, stat)
  call dios_put_att(nc_meteo%fid, varid_diffh, 'unit', 'm2 s-1', stat)
  call dios_put_att(nc_meteo%fid, varid_diffh, 'long_name', 'eddy diffusivity of scalar', stat)
  
  call dios_def_var(nc_meteo%fid, 'mixlen', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_mixlen, stat)
  call dios_put_att(nc_meteo%fid, varid_mixlen, 'unit', 'm', stat)
  call dios_put_att(nc_meteo%fid, varid_mixlen, 'long_name', 'mixing length', stat)
  
  call dios_def_var(nc_meteo%fid, 'ri', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_ri, stat)
  call dios_put_att(nc_meteo%fid, varid_ri, 'unit', '-', stat)
  call dios_put_att(nc_meteo%fid, varid_ri, 'long_name', 'Richardson number', stat)
  
  call dios_def_var(nc_meteo%fid, 'ustar', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_ustar, stat)
  call dios_put_att(nc_meteo%fid, varid_ustar, 'unit', 'm s-1', stat)
  call dios_put_att(nc_meteo%fid, varid_ustar, 'long_name', 'friction velocity', stat)
  
  call dios_def_var(nc_meteo%fid, 'shf', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_shf, stat)
  call dios_put_att(nc_meteo%fid, varid_shf, 'unit', 'W m-2', stat)
  call dios_put_att(nc_meteo%fid, varid_shf, 'long_name', 'sensible heat flux', stat)
  
  call dios_def_var(nc_meteo%fid, 'lhf', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_lhf, stat)
  call dios_put_att(nc_meteo%fid, varid_lhf, 'unit', 'W m-2', stat)
  call dios_put_att(nc_meteo%fid, varid_lhf, 'long_name', 'latent heat flux', stat)
  
  call dios_def_var(nc_meteo%fid, 'nair', DIOS_DOUBLE, nc_meteo%dimids_2d, varid_nair, stat)
  call dios_put_att(nc_meteo%fid, varid_nair, 'unit', 'molec cm-3', stat)
  call dios_put_att(nc_meteo%fid, varid_nair, 'long_name', 'number concentration of air', stat)
  
  ! 1D variables
  call dios_def_var(nc_meteo%fid, 'ceb', DIOS_DOUBLE, (/nc_meteo%dimid_time/), varid_ceb, stat)
  call dios_put_att(nc_meteo%fid, varid_ceb, 'unit', 'W m-2', stat)
  call dios_put_att(nc_meteo%fid, varid_ceb, 'long_name', 'energy balance at canopy top', stat)
  
  call dios_def_var(nc_meteo%fid, 'gsoil', DIOS_DOUBLE, (/nc_meteo%dimid_time/), varid_gsoil, stat)
  call dios_put_att(nc_meteo%fid, varid_gsoil, 'unit', 'W m-2', stat)
  call dios_put_att(nc_meteo%fid, varid_gsoil, 'long_name', 'soil heat flux plus heat storage change in the top soil', stat)
  
  call dios_def_var(nc_meteo%fid, 'pblh', DIOS_DOUBLE, (/nc_meteo%dimid_time/), varid_pblh, stat)
  call dios_put_att(nc_meteo%fid, varid_pblh, 'unit', 'm', stat)
  call dios_put_att(nc_meteo%fid, varid_pblh, 'long_name', 'PBL height', stat)
  
  call dios_def_var(nc_meteo%fid, 'albedo', DIOS_DOUBLE, (/nc_meteo%dimid_time/), varid_albedo, stat)
  call dios_put_att(nc_meteo%fid, varid_albedo, 'unit', '-', stat)
  call dios_put_att(nc_meteo%fid, varid_albedo, 'long_name', 'cover albedo', stat)
  
  call dios_def_var(nc_meteo%fid, 'zenith', DIOS_DOUBLE, (/nc_meteo%dimid_time/), varid_zenith, stat)
  call dios_put_att(nc_meteo%fid, varid_zenith, 'unit', 'deg', stat)
  call dios_put_att(nc_meteo%fid, varid_zenith, 'long_name', 'solar zenith angle', stat)
  
  call dios_def_var(nc_meteo%fid, 'rd_dir', DIOS_DOUBLE, (/nc_meteo%dimid_time/), varid_rd_dir, stat)
  call dios_put_att(nc_meteo%fid, varid_rd_dir, 'unit', 'W m-2', stat)
  call dios_put_att(nc_meteo%fid, varid_rd_dir, 'long_name', 'downward direct solar radiation', stat)
  
  call dios_def_var(nc_meteo%fid, 'rd_dif', DIOS_DOUBLE, (/nc_meteo%dimid_time/), varid_rd_dif, stat)
  call dios_put_att(nc_meteo%fid, varid_rd_dif, 'unit', 'W m-2', stat)
  call dios_put_att(nc_meteo%fid, varid_rd_dif, 'long_name', 'downward diffuse solar radiation', stat)
  
  call dios_def_var(nc_meteo%fid, 'ld', DIOS_DOUBLE, (/nc_meteo%dimid_time/), varid_ld, stat)
  call dios_put_att(nc_meteo%fid, varid_ld, 'unit', 'W m-2', stat)
  call dios_put_att(nc_meteo%fid, varid_ld, 'long_name', 'downward long wave radiation', stat)
  
  call dios_def_var(nc_meteo%fid, 'lu', DIOS_DOUBLE, (/nc_meteo%dimid_time/), varid_lu, stat)
  call dios_put_att(nc_meteo%fid, varid_lu, 'unit', 'W m-2', stat)
  call dios_put_att(nc_meteo%fid, varid_lu, 'long_name', 'upward long wave radiation', stat)
  
  call dios_def_var(nc_meteo%fid, 'paru', DIOS_DOUBLE, (/nc_meteo%dimid_time/), varid_paru, stat)
  call dios_put_att(nc_meteo%fid, varid_paru, 'unit', 'W m-2', stat)
  call dios_put_att(nc_meteo%fid, varid_paru, 'long_name', 'upward PAR radiation', stat)
  
  call dios_def_var(nc_meteo%fid, 'niru', DIOS_DOUBLE, (/nc_meteo%dimid_time/), varid_niru, stat)
  call dios_put_att(nc_meteo%fid, varid_niru, 'unit', 'W m-2', stat)
  call dios_put_att(nc_meteo%fid, varid_niru, 'long_name', 'upward near-infrared radiation', stat)
  
  ! End of definition
  call dios_enddef(nc_meteo%fid, stat)

  ! Write limited dimension variables
  call dios_put_var(nc_meteo%fid, nc_meteo%varid_lev, z, stat, start=(/1/), count=(/kz/))
end subroutine output_netcdf_meteo_init


subroutine output_netcdf_chem_init
  !----- Init meteo nc file ------------------------------------------------------!

  ! Chemistry file name
  nc_chem%fname = trim(adjustl(output_dir))//'/chem.nc'
  
  ! Create file
  call dios_create(trim(nc_chem%fname), DIOS_NETCDF, DIOS_REPLACE, nc_chem%fid, stat)
  
  ! Global attributes
  call dios_put_att(nc_chem%fid, DIOS_GLOBAL, 'title', 'SOSAA chemisty file', stat)
  
  ! Definition of dimensions
  call dios_def_dim(nc_chem%fid, 'time', DIOS_UNLIMITED, nc_chem%dimid_time, stat)
  call dios_def_dim(nc_chem%fid, 'lev', kz, nc_chem%dimid_lev, stat)
  ! call dios_def_dim(nc_chem%fid, 'ispc', outspc_count, nc_chem%dimid_ispc, stat)
  ! call dios_def_dim(nc_chem%fid, 'iemi', outemi_count, nc_chem%dimid_iemi, stat)
  ! nc_chem%dimids_emi2d = (/nc_chem%dimid_iemi, nc_chem%dimid_time/)
  nc_chem%dimids_2d = (/nc_chem%dimid_lev, nc_chem%dimid_time/)
  
  !----- Definition of variables -----!
  ! Unlimited dimension should be defined in the last dimid (for classic NetCDF),
  ! notice that the dims are inversed when using 'ncdump' to check the data.
  
  ! Dimension variables
  call dios_def_var(nc_chem%fid, 'lev', DIOS_DOUBLE, (/nc_chem%dimid_lev/), nc_chem%varid_lev, stat)
  call dios_put_att(nc_chem%fid, nc_chem%varid_lev, 'unit', 'm', stat)
  call dios_put_att(nc_chem%fid, nc_chem%varid_lev, 'long_name', 'height above the ground', stat)
  
  call dios_def_var(nc_chem%fid, 'time', DIOS_DOUBLE, (/nc_chem%dimid_time/), nc_chem%varid_time, stat)
  call dios_put_att(nc_chem%fid, nc_chem%varid_time, 'unit', 's', stat)
  call dios_put_att(nc_chem%fid, nc_chem%varid_time, 'long_name', 'time since the beginning of month', stat)
  call dios_put_att(nc_chem%fid, nc_chem%varid_time, 'first_day_of_month', trim(first_day_of_month_string), stat)
  call dios_put_att(nc_chem%fid, nc_chem%varid_time, 'start_date', trim(start_date_string), stat)

  ! 2D variables

  ! Number concentration
  ALLOCATE(varid_outspc_nconc(outspc_count))
  DO i=1, outspc_count
    if (outspc_cheminds(i) > 0) then  ! only for existed species
      call dios_def_var(nc_chem%fid, 'nconc_'//trim(adjustl(outspc_names(i))), &
        DIOS_DOUBLE, nc_chem%dimids_2d, varid_outspc_nconc(i), stat)
      call dios_put_att(nc_chem%fid, varid_outspc_nconc(i), 'unit', 'molec cm-3', stat)
      call dios_put_att(nc_chem%fid, varid_outspc_nconc(i), 'long_name', 'number concentration', stat)
    end if
  END DO

  ! Vertical flux
  ALLOCATE(varid_outspc_flux(outspc_count))
  DO i=1, outspc_count
    if (outspc_cheminds(i) > 0) then  ! only for existed species
      call dios_def_var(nc_chem%fid, 'flux_'//trim(adjustl(outspc_names(i))), &
        DIOS_DOUBLE, nc_chem%dimids_2d, varid_outspc_flux(i), stat)
      call dios_put_att(nc_chem%fid, varid_outspc_flux(i), 'unit', 'molec cm-2 s-1', stat)
      call dios_put_att(nc_chem%fid, varid_outspc_flux(i), 'long_name', 'vertical flux of molecules', stat)
    end if
  END DO

  ! Gas dry deposition velocity
  ALLOCATE(varid_outspc_vd(outspc_count))
  DO i=1, outspc_count
    if (outspc_cheminds(i) > 0) then  ! only for existed species
      call dios_def_var(nc_chem%fid, 'vd_'//trim(adjustl(outspc_names(i))), &
        DIOS_DOUBLE, nc_chem%dimids_2d, varid_outspc_vd(i), stat)
      call dios_put_att(nc_chem%fid, varid_outspc_vd(i), 'unit', 'm s-1', stat)
      call dios_put_att(nc_chem%fid, varid_outspc_vd(i), 'long_name', 'dry deposition velocity', stat)
    end if
  END DO

  ! Emission rates
  ALLOCATE(varid_outemi(outemi_count))
  DO i=1, outemi_count
    if (outemi_meganinds(i) > 0) then  ! only for existed emission species
      call dios_def_var(nc_chem%fid, 'emirate_'//trim(adjustl(outemi_names(i))), &
        DIOS_DOUBLE, nc_chem%dimids_2d, varid_outemi(i), stat)
      call dios_put_att(nc_chem%fid, varid_outemi(i), 'unit', 'molec cm-3 s-1', stat)
      call dios_put_att(nc_chem%fid, varid_outemi(i), 'long_name', 'emission rate', stat)
    end if
  END DO
  
  ! End of definition
  call dios_enddef(nc_chem%fid, stat)

  ! Write limited dimension variables
  call dios_put_var(nc_chem%fid, nc_chem%varid_lev, z, stat, start=(/1/), count=(/kz/))
end subroutine output_netcdf_chem_init


subroutine output_netcdf_aerosol_init
  !----- Init meteo nc file ------------------------------------------------------!

  ! Chemistry file name
  nc_aerosol%fname = trim(adjustl(output_dir))//'/aerosol.nc'
  
  ! Create file
  call dios_create(trim(nc_aerosol%fname), DIOS_NETCDF, DIOS_REPLACE, nc_aerosol%fid, stat)
  
  ! Global attributes
  call dios_put_att(nc_aerosol%fid, DIOS_GLOBAL, 'title', 'SOSAA aerosol file', stat)
  
  ! Definition of dimensions
  call dios_def_dim(nc_aerosol%fid, 'icondv', uhma_cond     , nc_aerosol%dimid_icondv, stat)
  call dios_def_dim(nc_aerosol%fid, 'rdry'  , uhma_sections , nc_aerosol%dimid_rdry  , stat)
  call dios_def_dim(nc_aerosol%fid, 'lev'   , kz            , nc_aerosol%dimid_lev   , stat)
  call dios_def_dim(nc_aerosol%fid, 'time'  , DIOS_UNLIMITED, nc_aerosol%dimid_time  , stat)
  nc_aerosol%dimids_3d_rdry  = (/nc_aerosol%dimid_lev ,nc_aerosol%dimid_rdry  ,  nc_aerosol%dimid_time/)
  nc_aerosol%dimids_3d_condv = (/nc_aerosol%dimid_lev ,nc_aerosol%dimid_icondv,  nc_aerosol%dimid_time/)
  nc_aerosol%dimids_4d       = (/nc_aerosol%dimid_lev, nc_aerosol%dimid_rdry, nc_aerosol%dimid_icondv, nc_aerosol%dimid_time/)
  
  !----- Definition of variables -----!
  ! Unlimited dimension should be defined in the last dimid (for classic NetCDF),
  ! notice that the dims are inversed when using 'ncdump' to check the data.
  
  ! Dimension variables

  call dios_def_var(nc_aerosol%fid, 'icondv', DIOS_DOUBLE, (/nc_aerosol%dimid_icondv/), nc_aerosol%varid_icondv, stat)
  call dios_put_att(nc_aerosol%fid, nc_aerosol%varid_icondv, 'unit', '-', stat)
  call dios_put_att(nc_aerosol%fid, nc_aerosol%varid_icondv, 'long_name', 'indices of condensable vapors', stat)

  call dios_def_var(nc_aerosol%fid, 'rdry', DIOS_DOUBLE, (/nc_aerosol%dimid_rdry/), nc_aerosol%varid_rdry, stat)
  call dios_put_att(nc_aerosol%fid, nc_aerosol%varid_rdry, 'unit', 'm', stat)
  call dios_put_att(nc_aerosol%fid, nc_aerosol%varid_rdry, 'long_name', 'dry radius of aerosol particles in each size bin', stat)

  call dios_def_var(nc_aerosol%fid, 'lev', DIOS_DOUBLE, (/nc_aerosol%dimid_lev/), nc_aerosol%varid_lev, stat)
  call dios_put_att(nc_aerosol%fid, nc_aerosol%varid_lev, 'unit', 'm', stat)
  call dios_put_att(nc_aerosol%fid, nc_aerosol%varid_lev, 'long_name', 'height above the ground', stat)
  
  call dios_def_var(nc_aerosol%fid, 'time', DIOS_DOUBLE, (/nc_aerosol%dimid_time/), nc_aerosol%varid_time, stat)
  call dios_put_att(nc_aerosol%fid, nc_aerosol%varid_time, 'unit', 's', stat)
  call dios_put_att(nc_aerosol%fid, nc_aerosol%varid_time, 'long_name', 'time since the beginning of month', stat)
  call dios_put_att(nc_aerosol%fid, nc_aerosol%varid_time, 'first_day_of_month', trim(first_day_of_month_string), stat)
  call dios_put_att(nc_aerosol%fid, nc_aerosol%varid_time, 'start_date', trim(start_date_string), stat)

  ! 1D variables

  ! 2D variables

  ! 3D variables

  ! Number concentration of particles
  call dios_def_var(nc_aerosol%fid, 'nconc_particle', DIOS_DOUBLE, nc_aerosol%dimids_3d_rdry, varid_nconc_particle, stat)
  call dios_put_att(nc_aerosol%fid, varid_nconc_particle, 'unit', 'particles m-3', stat)
  call dios_put_att(nc_aerosol%fid, varid_nconc_particle, 'long_name', 'number concentration of particles', stat)

  ! Vertical fluxes of particles
  call dios_def_var(nc_aerosol%fid, 'flux_particle', DIOS_DOUBLE, nc_aerosol%dimids_3d_rdry, varid_flux_particle, stat)
  call dios_put_att(nc_aerosol%fid, varid_flux_particle, 'unit', 'particles m-2 s-1', stat)
  call dios_put_att(nc_aerosol%fid, varid_flux_particle, 'long_name', 'vertical flux of particles', stat)

  ! Growth rate of particles
  call dios_def_var(nc_aerosol%fid, 'growth_rate', DIOS_DOUBLE, nc_aerosol%dimids_3d_rdry, varid_growth_rate, stat)
  call dios_put_att(nc_aerosol%fid, varid_growth_rate, 'unit', 'm s-1', stat)  ! ?? check the unit later
  call dios_put_att(nc_aerosol%fid, varid_growth_rate, 'long_name', 'growth rate of particles', stat)

  ! Number concentrations of condensable vapors in gas phase in all layers
  call dios_def_var(nc_aerosol%fid, 'nconc_condv_gas', DIOS_DOUBLE, nc_aerosol%dimids_3d_condv, varid_nconc_condv_gas, stat)
  call dios_put_att(nc_aerosol%fid, varid_nconc_condv_gas, 'unit', 'molec cm-3', stat)  ! ?? check the unit later
  call dios_put_att(nc_aerosol%fid, varid_nconc_condv_gas, 'long_name', 'number concentrations of condensable vapors in gas phase', stat)

  ! 4D variables

  ! Volume concentrations of condensable vapors in particle phase at each size bin in all layers
  call dios_def_var(nc_aerosol%fid, 'vconc_condv_par', DIOS_DOUBLE, nc_aerosol%dimids_4d, varid_vconc_condv_par, stat)
  call dios_put_att(nc_aerosol%fid, varid_vconc_condv_par, 'unit', 'um3 m-3', stat)  ! ?? check the unit later
  call dios_put_att(nc_aerosol%fid, varid_vconc_condv_par, 'long_name', 'volume concentrations of condensable vapors in particle phase', stat)

  ! End of definition
  call dios_enddef(nc_aerosol%fid, stat)

  ! Write limited dimension variables
  call dios_put_var(nc_aerosol%fid, nc_aerosol%varid_icondv, (/ (I, I = 1, uhma_cond) /), stat, start=(/1/), count=(/uhma_cond/))
  call dios_put_var(nc_aerosol%fid, nc_aerosol%varid_rdry, particles%rdry, stat, start=(/1/), count=(/uhma_sections/))
  call dios_put_var(nc_aerosol%fid, nc_aerosol%varid_lev, z, stat, start=(/1/), count=(/kz/))
end subroutine output_netcdf_aerosol_init


!==============================================================================!
! output netcdf write subroutines
!==============================================================================!

subroutine output_netcdf_meteo_write()
  !----- Write meteo nc file ------------------------------------------------------!

  call dios_put_var(nc_meteo%fid, nc_meteo%varid_time, (/montime/), stat, start=(/itrec/), count=(/1/))

  ! 2D variables
  call dios_put_var(nc_meteo%fid, varid_temp, ta1, stat, start=(/1, itrec/), count=(/kz, 1/))
  call dios_put_var(nc_meteo%fid, varid_rhov, qa1, stat, start=(/1, itrec/), count=(/kz, 1/))
  call dios_put_var(nc_meteo%fid, varid_rh, RH, stat, start=(/1, itrec/), count=(/kz, 1/))
  call dios_put_var(nc_meteo%fid, varid_pres, pres, stat, start=(/1, itrec/), count=(/kz, 1/))
  call dios_put_var(nc_meteo%fid, varid_ua, u1, stat, start=(/1, itrec/), count=(/kz, 1/))
  call dios_put_var(nc_meteo%fid, varid_va, v1, stat, start=(/1, itrec/), count=(/kz, 1/))
  call dios_put_var(nc_meteo%fid, varid_tke, bt1, stat, start=(/1, itrec/), count=(/kz, 1/))
  call dios_put_var(nc_meteo%fid, varid_mixlen, l1, stat, start=(/1, itrec/), count=(/kz, 1/))
  call dios_put_var(nc_meteo%fid, varid_diffm, kt1, stat, start=(/1, itrec/), count=(/kz, 1/))
  call dios_put_var(nc_meteo%fid, varid_diffh, alt1*kt1, stat, start=(/1, itrec/), count=(/kz, 1/))
  call dios_put_var(nc_meteo%fid, varid_ri, rih, stat, start=(/1, itrec/), count=(/kz, 1/))
  call dios_put_var(nc_meteo%fid, varid_ustar, ur, stat, start=(/1, itrec/), count=(/kz, 1/))
  call dios_put_var(nc_meteo%fid, varid_shf, fluxh3, stat, start=(/1, itrec/), count=(/kz, 1/))
  call dios_put_var(nc_meteo%fid, varid_lhf, fluxle3, stat, start=(/1, itrec/), count=(/kz, 1/))
  call dios_put_var(nc_meteo%fid, varid_nair, air, stat, start=(/1, itrec/), count=(/kz, 1/))

  ! 1D variables
  call dios_put_var(nc_meteo%fid, varid_ceb, (/balans/), stat, start=(/itrec/), count=(/1/))
  call dios_put_var(nc_meteo%fid, varid_gsoil, (/pp/), stat, start=(/itrec/), count=(/1/))
  call dios_put_var(nc_meteo%fid, varid_pblh, (/hlayer/), stat, start=(/itrec/), count=(/1/))
  call dios_put_var(nc_meteo%fid, varid_albedo, (/albedo_f/), stat, start=(/itrec/), count=(/1/))
  call dios_put_var(nc_meteo%fid, varid_zenith, (/beta/), stat, start=(/itrec/), count=(/1/))
  call dios_put_var(nc_meteo%fid, varid_rd_dir, (/rsnt/), stat, start=(/itrec/), count=(/1/))
  call dios_put_var(nc_meteo%fid, varid_rd_dif, (/rskt/), stat, start=(/itrec/), count=(/1/))
  call dios_put_var(nc_meteo%fid, varid_ld, (/fird(k_canopy)/), stat, start=(/itrec/), count=(/1/))
  call dios_put_var(nc_meteo%fid, varid_lu, (/firu(k_canopy)/), stat, start=(/itrec/), count=(/1/))
  call dios_put_var(nc_meteo%fid, varid_paru, (/fphu(k_canopy)/), stat, start=(/itrec/), count=(/1/))
  call dios_put_var(nc_meteo%fid, varid_niru, (/fniu(k_canopy)/), stat, start=(/itrec/), count=(/1/))
end subroutine output_netcdf_meteo_write


subroutine output_netcdf_chem_write()
  ! Time
  call dios_put_var(nc_chem%fid, nc_chem%varid_time, (/montime/), stat, start=(/itrec/), count=(/1/))

      ! CALL output_ascii_write_one('Gas_'//TRIM(ADJUSTL(gname)), CH_CONS_ALL(:, outspc_cheminds(I)))
      ! CALL output_ascii_write_one('Flux_'//TRIM(ADJUSTL(gname)), CH_CONS_FLUX(:, outspc_cheminds(I)))
  DO i=1, outspc_count
    if (outspc_cheminds(i) > 0) then  ! only for existed species
      ! Number concentration
      call dios_put_var(nc_chem%fid, varid_outspc_nconc(i), CH_CONS_ALL(:, outspc_cheminds(i)), stat, &
        start=(/1, itrec/), count=(/kz, 1/))

      ! Vertical flux
      call dios_put_var(nc_chem%fid, varid_outspc_flux(i), CH_CONS_FLUX(:, outspc_cheminds(i)), stat, &
        start=(/1, itrec/), count=(/kz, 1/))

      ! Gas dry deposition
      call dios_put_var(nc_chem%fid, varid_outspc_vd(i), vdep(:, outspc_cheminds(i)), stat, &
        start=(/1, itrec/), count=(/kz, 1/))
    end if
  END DO

  ! Emission rate
  DO i=1, outemi_count
    if (outemi_meganinds(i) > 0) then  ! only for existed emission species
      call dios_put_var(nc_chem%fid, varid_outemi(i), EM_EMI(:, outemi_meganinds(i)), stat, &
        start=(/1, itrec/), count=(/kz, 1/))
    end if
  END DO
end subroutine output_netcdf_chem_write


subroutine output_netcdf_aerosol_write()
  ! Time
  call dios_put_var(nc_aerosol%fid, nc_aerosol%varid_time, (/montime/), stat, start=(/itrec/), count=(/1/))

  ! Number concentration
  call dios_put_var(nc_aerosol%fid, varid_nconc_particle, N_CONC(:, :), stat, &
    start=(/1, 1, itrec/), count=(/ kz, uhma_sections, 1/))

  ! Vertical flux
  call dios_put_var(nc_aerosol%fid, varid_flux_particle, PAR_FLUX(:, :), stat, &
    start=(/1, 1, itrec/), count=(/ kz, uhma_sections, 1/))

  ! Growth rate
  call dios_put_var(nc_aerosol%fid, varid_growth_rate, GR(:, :), stat, &
    start=(/1, 1, itrec/), count=(/ kz, uhma_sections, 1/))

  ! Number concentration of condensable vapors in gas phase
  call dios_put_var(nc_aerosol%fid, varid_nconc_condv_gas, VAPOR(:, :), stat, &
    start=(/1, 1, itrec/), count=(/ kz, uhma_cond, 1/))

  ! Volume concentration of condensable vapors in particle phase
  call dios_put_var(nc_aerosol%fid, varid_vconc_condv_par, VOL_CONC(:, :, :), stat, &
    start=(/1, 1, 1, itrec/), count=(/kz, uhma_sections, uhma_cond, 1/))
end subroutine output_netcdf_aerosol_write


!==============================================================================!
! output netcdf done subroutines
!==============================================================================!

subroutine output_netcdf_general_done()
  call dios_close(nc_general%fid, stat)
end subroutine output_netcdf_general_done


subroutine output_netcdf_meteo_done()
  call dios_close(nc_meteo%fid, stat)
end subroutine output_netcdf_meteo_done


subroutine output_netcdf_chem_done()
  call dios_close(nc_chem%fid, stat)
end subroutine output_netcdf_chem_done


subroutine output_netcdf_aerosol_done()
  call dios_close(nc_aerosol%fid, stat)
end subroutine output_netcdf_aerosol_done


subroutine output_netcdf_done()
  call output_netcdf_meteo_done()
  call output_netcdf_chem_done()
  call output_netcdf_aerosol_done()

  call dios_done(stat)
end subroutine output_netcdf_done


subroutine output_done()
  call output_netcdf_done()
end subroutine output_done


!------------------------------------------------------------------------------!
!
! String subprograms
!
!------------------------------------------------------------------------------!


!==============================================================================!
! Convert all the letters to upper case
!==============================================================================!
FUNCTION upper(s1)  RESULT (s2)
  CHARACTER(*)       :: s1
  CHARACTER(LEN(s1)) :: s2
  CHARACTER          :: ch
  INTEGER,PARAMETER  :: DUC = ICHAR('A') - ICHAR('a')
  INTEGER            :: i

  DO i = 1,LEN(s1)
    ch = s1(i:i)
    IF (ch >= 'a'.AND.ch <= 'z') ch = CHAR(ICHAR(ch)+DUC)
    s2(i:i) = ch
  END DO
END FUNCTION upper


!==============================================================================!
! Convert all the letters to lower case
!==============================================================================!
FUNCTION lower(s1)  RESULT (s2)
  CHARACTER(*)       :: s1
  CHARACTER(LEN(s1)) :: s2
  CHARACTER          :: ch
  INTEGER,PARAMETER  :: DUC = ICHAR('A') - ICHAR('a')
  INTEGER            :: i

  DO i = 1,LEN(s1)
    ch = s1(i:i)
    IF (ch >= 'A'.AND.ch <= 'Z') ch = CHAR(ICHAR(ch)-DUC)
    s2(i:i) = ch
  END DO
END FUNCTION Lower


!==============================================================================!
! Split the string to substrings separated by wall.
! It does not include the rooms with only spaces.
!==============================================================================!
subroutine string_split_trim2(str, wall, nrooms, irooms)
  ! The rooms are not trimmed

  character(*), intent(in) :: str
  character   , intent(in) :: wall

  integer, intent(out) :: nrooms
  integer, intent(out) :: irooms(:, :)

  integer :: nwall
  integer :: str_len
  integer, allocatable :: wall_pos(:)

  integer :: i, ir

  str_len = len(str)

  ! maximum rooms separated by walls
  allocate(wall_pos(str_len))
  call count_substring(str, wall, .false., nwall, wall_pos)

  ir = 0

  ! the room before first wall
  if (len_trim( adjustl( str(1:wall_pos(1)-1) ) ) /= 0) then
    ir = ir + 1
    irooms(ir, 1) = 1
    irooms(ir, 2) = wall_pos(1)-1
  end if

  ! rooms between wall 1 and nwall-1
  do i=1, nwall-1
    if (len_trim( adjustl( str(wall_pos(i)+1:wall_pos(i+1)-1) ) ) /= 0) then
      ir = ir + 1
      irooms(ir, 1) = wall_pos(i)+1
      irooms(ir, 2) = wall_pos(i+1)-1
    end if
  end do

  ! the room after nwall
  if (len_trim( adjustl( str(wall_pos(nwall)+1:) ) ) /= 0) then
    ir = ir + 1
    irooms(ir, 1) = wall_pos(nwall)+1
    irooms(ir, 2) = str_len
  end if

  nrooms = ir
end subroutine string_split_trim2


!==============================================================================!
! Split the string to substrings separated by wall.
! It includes the rooms with only spaces.
!==============================================================================!
subroutine string_split(str, wall, nrooms, irooms)
  character(*), intent(in) :: str
  character   , intent(in) :: wall

  integer, intent(out) :: nrooms
  integer, intent(out) :: irooms(:, :)

  integer :: nwall
  integer :: str_len
  integer, allocatable :: wall_pos(:)

  integer :: i, ir

  str_len = len(str)

  ! maximum rooms separated by walls
  allocate(wall_pos(str_len))
  call count_substring(str, wall, .false., nwall, wall_pos)

  ir = 0
  ! the room before first wall
  if (len(str(1:wall_pos(1)-1)) /= 0) then
    ir = ir + 1
    irooms(ir, 1) = 1
    irooms(ir, 2) = wall_pos(1)-1
  end if

  ! rooms between wall 1 and nwall-1
  do i=1, nwall-1
    if (len(str(wall_pos(i)+1:wall_pos(i+1)-1)) /= 0) then
      ir = ir + 1
      irooms(ir, 1) = wall_pos(i)+1
      irooms(ir, 2) = wall_pos(i+1)-1
    end if
  end do

  ! the room after nwall
  if (len(str(wall_pos(nwall)+1:)) /= 0) then
    ir = ir + 1
    irooms(ir, 1) = wall_pos(nwall)+1
    irooms(ir, 2) = str_len
  end if

  nrooms = ir
end subroutine string_split


!==============================================================================!
! Count how many substrings are in the string. 
!==============================================================================!
subroutine count_substring(s0, ss, greedy, c, pa)
  ! input raw string s0 and substring ss
  character(*), intent(in) :: s0, ss

  ! greedy, e.g. 'oo' in 'oooo'
  ! true : c = 3, pa = [1, 2, 3]
  ! false: c = 2, pa = [1, 3]
  logical, intent(in) :: greedy

  ! count of ss in s0
  integer, intent(out) :: c

  ! position array: starting indices of every ss in s0
  ! dimension size should be the same as the length of s0, but can be reduced if necessary.
  ! it depends on the input array
  ! set to zero when the position is not used
  integer, intent(out) :: pa(:)

  ! current position of s0
  integer :: p

  ! the first occurence of substring
  integer :: posn

  ! initiate
  c = 0   ! count is 0
  pa = 0  ! indices of ss are 0 (does not exist)

  ! raw string is empty, return 0
  if (len(s0) == 0) return

  ! current position
  p = 1

  ! search until not found ss in s0's substrings
  do
    ! search the first occurence of ss in the rest of s0
    posn = index(s0(p:), ss)

    ! return current condition if not found any more
    if (posn == 0) return

    ! add one to counter if found
    c = c + 1

    ! save the position
    pa(c) = p + posn - 1

    ! advance 1 if greedy
    if (greedy) then
      p = p + posn - 1 + 1
    ! skip all ss if not greedy
    else
      p = p + posn - 1 + len(ss)
    end if
  end do
end subroutine count_substring


!==============================================================================!
! Get substring from the position array
!==============================================================================!
SUBROUTINE get_substring_list(s0, nrooms, irooms, rooms)
  character(*), intent(in) :: s0
  integer, intent(in) :: nrooms
  integer, intent(in) :: irooms(:, :)

  character(*), intent(out) :: rooms(:)

  integer :: i

  do i = 1, nrooms
    rooms(i) = trim(adjustl( s0(irooms(i, 1):irooms(i, 2)) ))
  end do
END SUBROUTINE get_substring_list


!==============================================================================!
! Get the indices of names in the original name list
!==============================================================================!
subroutine get_inds_outnames_in_rawnames(outnames, rawnames, outinds)
  character(*), intent(in) :: outnames(:)
  character(*), intent(in) :: rawnames(:)

  integer, intent(out)     :: outinds(:)

  integer :: i
  
  ! Obtain indices of output species (case insensative) from raw name list
  outinds = -1  ! default value
  do i = 1, size(outnames)
    do j=1, size(rawnames)
      if ( upper( trim(adjustl( rawnames(j) )) ) == upper( trim(adjustl( outnames(i) )) ) ) then
        outinds(i) = j
        exit
      end if
    end do
  end do
end subroutine get_inds_outnames_in_rawnames


END MODULE SOSA_IO
