PROGRAM Sosa

  USE SOSA_DATA
  USE SOSA_IO
  USE MEGAN_version_1
  USE MEGAN_version_2
  USE SIMBIM
  USE LAI_Hyy_month
  !USE LAI_Hyy_month_old
  !USE LAI_Hyy_month_new
  USE Chemistry_Mod

  USE second_Monitor, ONLY: SPC_NAMES ! a CHARACTER(LEN=12) array of chemical names

  USE DISTRIBUTE
  ! USE UHMA_IO
  USE uhma_datatypes
  USE uhma_interface
  USE GDE_SOLVER

  USE MT_MainMet
  USE Scadis_Initial

  USE gdd_function_mod

  IMPLICIT NONE


  !=======================================================================================================================
  ! Start to parallel
  !=======================================================================================================================

  !#ifdef PARALLEL

  WRITE(*,*) 'Initializing parallel ...'

  ! First set up things for the parallel computation
  CALL MPI_Init(mpi_rc)  ! initialize MPI

  IF (mpi_rc /= MPI_SUCCESS) THEN
    WRITE(*,*) 'MPI initialization failed'
    STOP
  END IF


  CALL MPI_Comm_size(MPI_COMM_WORLD, mpi_ntasks, mpi_rc)  ! find out how many processess are running

  ! after this (or actually already after CALL MPI_Init), there are mpi_ntasks copies of this code running
  mpi_nslaves = mpi_ntasks - 1

  IF (mpi_nslaves > kz) THEN
    WRITE (*,*) 'Error: too many processes, ',mpi_ntasks,', only kz = ',kz,' atmospheric layers'
    GOTO 666
  END IF

  CALL MPI_Comm_rank(MPI_COMM_WORLD, my_id, mpi_rc)  ! find out our id number

  IF (my_id == master_id) THEN
    WRITE(*,*) 'MPI: number of processes: ',mpi_ntasks
  END IF


  !=======================================================================================================================
  ! Read input data for all cores
  !=======================================================================================================================
  WRITE(*,*) 'The core ', my_id, ' is reading input data ...'
  CALL READ_INPUTS


  !=======================================================================================================================
  ! Initialize aerosol for all cores
  !=======================================================================================================================
  IF (Aeroflag == 1) THEN
    WRITE(*,*) 'The core ', my_id, ' is initiating aerosol ...'
    CALL init_aerosol()
  END IF


  !=======================================================================================================================
  ! The slaves go to work
  !=======================================================================================================================
  IF (my_id /= master_id) THEN
    CALL slave_task  ! if we are not the master branch, go to slave mode
    GOTO 666         ! after all slavery, go to end of program
  END IF

  !#endif


  !=======================================================================================================================
  ! Initiate time and grid
  !=======================================================================================================================
  !---------------------------------------
  ! Time
  !---------------------------------------
  first_time_loop = .true.

  nxodrad = 1

  ! Time to start the simulation.
  daytime = 3600.0d0*start_date(4)+60.0d0*start_date(5)  ! [s], current daytime
  montime = daytime + (start_date(3)-1)*SECONDS_IN_ONE_DAY  ! [s], current month time

  ! Check if the start time is a new day and new month
  if ( (start_date(4)==0) .and. (start_date(5)==0) .and. (start_date(6)==0) ) then
    is_newday = .true.
  else
    is_newday = .false.
  end if

  if ( (start_date(3)==1) .and. (start_date(4)==0) &
    .and. (start_date(5)==0) .and. (start_date(6) == 0) ) then
    is_newmonth = .true.
  else
    is_newmonth = .false.
  end if

  ! The clock for the finishing time
  time_end = (end_date(3)-1)*SECONDS_IN_ONE_DAY + SECONDS_IN_ONE_HOUR*end_date(4) + SECONDS_IN_ONE_MINUTE*end_date(5) + end_date(6)
  nstep = 10000000 ! Number of steps. Actually it's needed to define error in calculations. So take 1000000 for instance

  ! Date information
  julian = JulianDay(now_date(1), now_date(2), now_date(3))
  date = now_date(3)
  nclo_end = MonthDay(now_date(1), now_date(2))*48
  ! mon = start_date(2)

  ! Timers
  do k = 1, kz
    call init_timer(root_recv_aero(k))
    call init_timer(root_send_aero(k))
  end do

  !---------------------------------------
  ! Grid
  !---------------------------------------
  CALL generate_grid(kz, 'LOG', hh, z, abl)

  ! SOME VARIABLE PARAMETERS INSIDE THE LAYER BETWEEN GROUND AND DOMAIN TOP
  if (ECMWFflag2 == 1) then
    CALL CONVERT_ECMWF
  endif


  !=======================================================================================================================
  ! Initiate canopy information
  !=======================================================================================================================
  ! 'curved' LAI for Hyytiala pine, spruce and hardwood over 15 years: 1996 - 2010 by Janne Levola (2011 same as 2010)
  ! EM_LAI_year_Hyy = (/2.62, 2.82, 3.00, 3.12, 3.29, 3.42, 3.44, 3.47, 3.47/)
  ! This is used for the emission part.
  ! Normally the total LAI is measured.
  ! The relationships for pine and spruce are:
  ! total LAI * 0.6 = curved LAI     &     total LAI = 2.7 * projected LAI  &  projected LAI = curved LAI / 1.62
  ! The relationships for birch are:
  ! total LAI * 0.5 = EM_LAI      &      total LAI * 0.5 = projected LAI
  ! EM_LAI_year_Hyy for:
  ! spruce 15 years = 4.079
  ! spruce 30 years = 8.0
  ! spruce 50 years = 7.335
  ! pine 15 years = 1.600
  ! pine 20 years = 4.26
  ! pine 50 years = 4.160
  ! birch 10 years = 0.863
  ! birch 20 years = 4.01
  ! birch 50 years = 2.74

  EM_LAI_year_Hyy = (/2.62_dp, 2.82_dp, 3.00_dp, 3.12_dp, 3.29_dp, 3.42_dp, 3.44_dp, 3.47_dp, 3.49_dp, 3.51_dp, 3.52_dp, 3.54_dp/)  ! curved LAI
  IF (now_date(1) == 2003) EM_LAI_year = EM_LAI_year_Hyy(1)
  IF (now_date(1) == 2004) EM_LAI_year = EM_LAI_year_Hyy(2)
  IF (now_date(1) == 2005) EM_LAI_year = EM_LAI_year_Hyy(3)
  IF (now_date(1) == 2006) EM_LAI_year = EM_LAI_year_Hyy(4)
  IF (now_date(1) == 2007) EM_LAI_year = EM_LAI_year_Hyy(5)
  IF (now_date(1) == 2008) EM_LAI_year = EM_LAI_year_Hyy(6)
  IF (now_date(1) == 2009) EM_LAI_year = EM_LAI_year_Hyy(7)
  IF (now_date(1) == 2010) EM_LAI_year = EM_LAI_year_Hyy(8)
  IF (now_date(1) == 2011) EM_LAI_year = EM_LAI_year_Hyy(9)
  IF (now_date(1) == 2012) EM_LAI_year = EM_LAI_year_Hyy(10)
  IF (now_date(1) == 2013) EM_LAI_year = EM_LAI_year_Hyy(11)
  IF (now_date(1) == 2014) EM_LAI_year = EM_LAI_year_Hyy(12)
  IF (now_date(1) == 2015) EM_LAI_year = EM_LAI_year_Hyy(12)
  IF (now_date(1) >= 2016) EM_LAI_year = EM_LAI_year_Hyy(12)

  IF (hc > 0.0d0) THEN  ! if canopy exists
    !LAI_proj is ONLY used for the met part and it is the projected LAI (so the flat part of the needle).
    !So MEGAN do not get this info!
    IF ((Treeflag == 1) .OR. (Treeflag == 2)) THEN  !pine & spruce
      LAI_proj = EM_LAI_year / 1.62
      ! LAI_proj = 3.  ! Try to set projected LAI to 3 [m2 m-2], since in met part it will be divided by 2.7.
      su=1
    ELSEIF (Treeflag == 3) THEN  !birch
      LAI_proj = EM_LAI_year
      su=2
    ELSEIF (Treeflag == 4) THEN  !clearcut
      LAI_proj = 0.1
      su=1
    ENDIF
    pa  = 3 ! For the meteorology
    pa2 = 6 ! only for megan or general emission activity

    ! Find the model level at the top of the canopy (higher than the canopy, not equal to)
    k_canopy = 1
    DO k=1,kz
      if (z(k) <= hc) then
        k_canopy = k + 1
      else
        exit
      end if
    ENDDO
  else  ! no canopy exists
    k_canopy = 0
  ENDIF

  ! Initial values for simbim for sunny (S) and shadow (C) leaves
  DO k = 1,kz
    BIM_IN_S(k,1)  = 30.0   ! start value for stomatal conductance
    BIM_IN_S(k,2)  = 380.0  ! start value for CI (CO2 inside)
    BIM_IN_S(k,3)  = 1.0    ! start value for photosynthesis rate
    BIM_IN_S(k,4)  = 10.0   ! start value PGA (phosphor-glycerate-aldehyde)
    BIM_IN_S(k,5)  = 30.0   ! start value GAP (
    BIM_IN_S(k,6)  = 100.0  ! start value and constant for NADPH ()
    BIM_IN_S(k,7)  = 0.0    ! start value DXP
    BIM_IN_S(k,8)  = 0.0    ! start value MEP
    BIM_IN_S(k,9)  = 0.0    ! start value IDP
    BIM_IN_S(k,10) = 0.0    ! start value DMADP
    BIM_IN_S(k,11) = 0.0    ! start value Isoprene production rate [nmol / m2-leaf area / s]
    BIM_IN_S(k,12) = 0.0    ! start value GDP
    BIM_IN_S(k,13) = 0.0    ! start value Monoterpene production rate [nmol / m2-leaf area / s]

    BIM_IN_C(k,1)  = 30.0   ! start value for stomata conductance
    BIM_IN_C(k,2)  = 380.0  ! start value for CI (CO2 inside)
    BIM_IN_C(k,3)  = 1.0    ! start value for photosynthesis rate
    BIM_IN_C(k,4)  = 10.0   ! start value PGA (phosphor-glycerate-aldehyde)
    BIM_IN_C(k,5)  = 30.0   ! start value GAP (
    BIM_IN_C(k,6)  = 100.0  ! start value and constant for NADPH ()
    BIM_IN_C(k,7)  = 0.0    ! start value DXP
    BIM_IN_C(k,8)  = 0.0    ! start value MEP
    BIM_IN_C(k,9)  = 0.0    ! start value IDP
    BIM_IN_C(k,10) = 0.0    ! start value DMADP
    BIM_IN_C(k,11) = 0.0    ! start value Isoprene production rate [nmol / m2-leaf area / s]
    BIM_IN_C(k,12) = 0.0    ! start value GDP
    BIM_IN_C(k,13) = 0.0    ! start value Monoterpene production rate [nmol / m2-leaf area / s]
  ENDDO


  !=======================================================================================================================
  ! Initiate meteorology
  !=======================================================================================================================
  WRITE(*,*) 'Initiate meteorology ...'
  CALL MT_InitialScadis()

  ! About snow
  rou=0.0_dp    !  snow cover is excluded from consideration now
  psnow=300.0_dp


  !=======================================================================================================================
  ! Initiate chemistry
  !=======================================================================================================================
  WRITE(*,*) 'Initiate chemistry ...'
  ! everything concerning ##!! warning!!
  !#ifndef PARALLEL
  CALL KPP_SetUp()  ! This only called once for KPP in the beginning
  !#endif

  ! Set all gas-concentrations to 0 [molec cm-3] in the beginning
  CH_CONS_ALL(:, :) = 0.0d0

  ! Set initial value for ozone
  ! write(*,*) 'montime, nxodrad, dt_obs', montime, nxodrad, dt_obs
  ! CH_CONS_ALL(:,ind_O3)  = linear_interp(montime, nxodrad, CH_gas_hyy(2,:), dt_obs) * 2.4d19*1.0d-9

  !---------------------------------------
  ! Count how many chemical names begin with 'rOH': CH_oh_count
  ! Set up a logical array, NSPEC long, so that only for rOH-pseudochemicals we have
  ! CH_oh_flag(j) == .TRUE., j being the number of chemical. Do the same for O3 and NO3.
  !---------------------------------------
  CALL init_rOH()
  CALL init_rO3()
  CALL init_rNO3()


  !=======================================================================================================================
  ! Initialize emissions
  !=======================================================================================================================
  WRITE(*,*) 'Initiate emission ...'
  ! Soil emission parameters
  I = 1
  Do WHILE (z(I) < 4.7_dp)
    I = I + 1
  END DO
  temp_level = I


  !=======================================================================================================================
  ! Initialize gas dry deposition
  !=======================================================================================================================
  o3_weight = 0.0d0
  o3_weight(nz) = 1.0d0  ! get from measurement in layer nz

  IF (Gasdrydepflag == 1) THEN
    WRITE(*,*) 'Initiate gas dry deposition ...'

    l_drydep = .True.
    l_vpd = .True.
    l_wetskin = .TRUE.

    ! ALLOCATE(frac_veg(1:nz-2), frac_ws(1:nz-2)
    ! frac_veg(2:nz-1) = 1.0d0
    ! frac_ws(2:nz-1) = 0.0d0

    LAIl = 0.0d0
    DO k = 2, nz-1
      ! LAIl(k) = s1(k)*(dz(k)+dz(k+1))*0.5d0 * 1.62d0  ! change from meteorology LAI to emission LAI, namely projected to curved
      LAIl(k) = s1(k)*(dz(k)+dz(k+1))*0.5d0 * 2.7d0  ! change from meteorology LAI to total LAI, namely projected to total
    END DO
    LAIl(2) = LAIl(2)/2.7d0*2.0d0  ! the lowest layer (understory) is the normal-leaf shrub, not needle leaves, should be 0.5 [m2 m-2]
    DO k = 2, nz-1
      LAIl_c(k) = 0.5d0*LAIl(k) + SUM(LAIl(k+1:nz-1))
    END DO

    ! LAIl_debug = 1.0d0  ! for debug

    PARl = 0.0d0

    u_veg = 0.0d0
    rho_veg = roa
    Tc_veg = 273.15d0
    stomblock = 0.0d0

    vdep = 0.0d0
    gasdep_flux = 0.0d0

    !===== Read molar_mass, Henry's law contants, dry reactivity factors from input file H_f0_mmass.inp =====!
    OPEN(UNIT=99, FILE=TRIM(ADJUSTL(CHEM_DIR))//'/INPUT/H_f0_mmass.inp', STATUS='OLD')
    READ(99, *)
    DO
      READ(99,*,IOSTAT=dummy_io) dummy_index, dummy_MCM, dummy_SMILES, &
        dummy_HenrySE, dummy_HenryEE, dummy_HenryEG, dummy_HenryEB, dummy_HFlag, dummy_HenryA, dummy_HenryB, &
        dummy_f0Flag, dummy_f0, dummy_formula, dummy_molar_mass
      IF (dummy_io > 0) THEN  ! error
        WRITE(*,*) 'Something is wrong with H_f0_mmass.inp.'
        STOP
      ELSE IF (dummy_io < 0) THEN  ! EOF
        EXIT
      ELSE  ! everything is OK
        trname(dummy_index) = dummy_MCM
        molar_mass(dummy_index) = dummy_molar_mass
        HenrySE(dummy_index) = dummy_HenrySE
        HenryEE(dummy_index) = dummy_HenryEE
        HenryEG(dummy_index) = dummy_HenryEG
        HenryEB(dummy_index) = dummy_HenryEB
        HenryA(dummy_index) = dummy_HenryA
        HenryB(dummy_index) = dummy_HenryB
        dryreac(dummy_index) = dummy_f0
      END IF
    END DO
    CLOSE(99)
    Henry = 0.0d0
  END IF


  !=======================================================================================================================
  ! Initiate other variables
  !=======================================================================================================================
  WRITE(*,*) 'Initiate other variables ...'
  ! Initial values for calculating accumulative sources and sinks
  CH_CONS_0(:, :) = 0.0d0
  CH_CONS_1(:, :) = 0.0d0
  Qconc(:,:) = 0.0d0
  Qemis(:,:) = 0.0d0
  Qchem(:,:) = 0.0d0
  Qturb(:,:) = 0.0d0
  Qdepo(:,:) = 0.0d0


  !=======================================================================================================================
  ! Open output files and write the initial values
  !=======================================================================================================================
  WRITE(*,*) 'Open output files and write the initial values ...'
  CALL open_output_files()
  CALL output_write()


  !=======================================================================================================================
  ! Basic cycle
  !=======================================================================================================================
  ! time
  nsytki=1
  nmetro=start_date(4)*2  ! used to calculate nmx which is used in Chemistry

  WRITE(*,*) 'Start main loop within one month ...'
  WRITE(*,*) 'Current time', montime
  WRITE(*,*) 'Ending time', time_end

  DO WHILE (time_end > montime)

    nxodrad = int(montime/dt_obs)+1

    ! Output current time which is being calculated
    IF (MOD( INT(montime), INT(SECONDS_IN_HALF_HOUR) ) == 0) THEN
      WRITE(*,'("=====  julian",I5,"  month",I3,"  date",I3,"  hours",F8.3,"  halfhour",I4,"  hrsinmth =====",I5,F10.1,F10.2)') &
        julian, now_date(2), now_date(3), daytime/3600, nxodrad, nclo_end, montime, montime/dt_obs
    END IF

    !***** Starting gas concentrations at every time step *****!
    CH_CONS_0 = CH_CONS_ALL


    !**********************************************************************************************************************
    !    Meteorology part
    !**********************************************************************************************************************
    CALL debug_message('DEBUGING: calculating meteorology')

    CALL MT_MainScadis(dt_obs)




    !**********************************************************************************************************************
    !    Emission and chemistry part
    !**********************************************************************************************************************

    ! Data transfer from main-program to emission variables - make sure all units are correct
    ! Emissions using MEGAN or SIMBIM
    ! That is the order of emisson factors by MEGAN and SIMBIM in EM_EMI
    ! ISOP                  1
    ! MYRC                  2
    ! SABI                  3
    ! LIMO                  4
    ! 3CAR                  5
    ! OCIM                  6
    ! BPIN                  7
    ! APIN                  8
    ! OMTP                  9
    ! FARN                 10
    ! BCAR                 11
    ! OSQT                 12
    ! MBO                  13
    ! MEOH (methanol)      14
    ! ACTO (acetone)       15
    ! CH4                  16
    ! NO                   17
    ! ACTA (acetaldehyde)  18
    ! FORM (formaldehyde)  19
    ! CO                   20
    ! CIN (cineole, Eucalyptol) 21
    ! LIN (Linalool)            22

    CALL debug_message('DEBUGING: calculating chemistry')

    IF (MOD( INT(daytime), INT(dt_chem) ) == 0) THEN  ! Calculate chemistry every dt_chem
      !***** Save the concentrations before chemistry *****!
      CH_CONS_temp = CH_CONS_ALL

      IF (Treeflag == 1) THEN !pine
        !This is to interpolate the measured monoterpene tree emission rates + chamber temeperature (for SEP in MEGAN)
        IF ((EMIflag == 1) .OR. (EMIflag == 2) .OR. (EMIflag == 3)) THEN
          EMI_MEAS_MONO = EF_meas(1,nxodrad) + (montime-(nxodrad-1)*dt_obs) * (EF_meas(1,nxodrad+1) - EF_meas(1,nxodrad))/dt_obs !measured monoterpene emission rate
          CHAM_temp = EF_meas(2,nxodrad) + (montime-(nxodrad-1)*dt_obs) * (EF_meas(2,nxodrad+1) - EF_meas(2,nxodrad))/dt_obs !chamber temperature ~ leaf temperature
        ENDIF
      ELSE
        EMI_MEAS_MONO = 0.
        CHAM_temp = 0.
      ENDIF  ! Treeflag==1

      IF ((EMIflag == 1) .OR. (EMIflag == 2) .OR. (EMIflag == 3)) THEN
        ! The input data and other parameters are only read in or set at the first
        IF (first_call == 1) THEN ! do only once
          first_call = 0
        ENDIF  ! first_call

        ! Getting values from input data
        GLO = glo_hyy(nxodrad)  + (montime-(nxodrad-1)*dt_obs) * (glo_hyy(nxodrad+1) - glo_hyy(nxodrad))/dt_obs
        PAR = par_hyy(nxodrad)  + (montime-(nxodrad-1)*dt_obs) * (par_hyy(nxodrad+1) - par_hyy(nxodrad))/dt_obs
        SMO = sm_hyy(nxodrad)   + (montime-(nxodrad-1)*dt_obs) * (sm_hyy(nxodrad+1)  - sm_hyy(nxodrad))/dt_obs
        if (isnan(SMO)) SMO = 0.3d0  ! a temporary solution
        SMOB = sm_b(nxodrad)   + (montime-(nxodrad-1)*dt_obs) * (sm_b(nxodrad+1)  - sm_b(nxodrad))/dt_obs
        SMOH = sm_h(nxodrad)   + (montime-(nxodrad-1)*dt_obs) * (sm_h(nxodrad+1)  - sm_h(nxodrad))/dt_obs

        ! First put all emissions to zero at each time step
        EM_EMI(:,1:22) = 0.0d0

        ! Input
        EM_Year         = now_date(1)                    ! Year
        EM_Julian       = julian                         ! Julian day
        EM_Time         = daytime                        ! Time (s)
        EM_DATE         = ((1000*EM_Year)+EM_Julian)     ! in format YYYDDD scalar
        EM_Time_M2_R    = EM_Time                        ! Time (s)
        EM_Time_M2      = Timeformatter(EM_Time_M2_R)    ! Function in megan module: integer output - real input
        EM_kz           = kz                             ! Number of vertical layers (not only canopy)
        EM_Can_Lay      = k_canopy                       ! Number of vertical layers inside the canopy
        EM_Lat          = 61. + 50./60. + 50.685/3600.   ! Latitude for Hyytiala
        EM_Long         = 24. + 17./60. + 41.206/3600.   ! Longitude for Hyytiala

        DO JJ = 1,EM_kz
          EM_z(JJ)        = z(JJ)                        ! Array of height of the layers (m)                ! Array of height of the layers
          EM_TempK(JJ)    = ta1(JJ)  +0.0d0                    ! Temperature in Kelvin
          EM_TempC(JJ)    = ta1_C(JJ)+0.0d0                    ! Temperature in Celsius
          EM_Wind(JJ)     = w_emi(JJ)                    ! Wind speed [m s-1]
          EM_qa1(JJ)      = qa1(JJ)                      ! Absolute humidity [kg m-3]
          EM_Pres(JJ)     = pres(JJ)                     ! Pressure in the canopy (Pa)
          EM_RH(JJ)       = RH(JJ)                       ! Relative humidity in %
        ENDDO

        EM_SRAD         = GLO                            ! Incoming short wave solar radiation in (W/m²)
        EM_PAR          = PAR                            ! Incoming photosynthetic active radiation [umol/m2/s] for MEGAN
        EM_PAR_SB       = PAR                            ! Incoming photosynthetic active radiation [umol/m2/s] for SIMBIM
        EM_SMOIST       = SMO                            ! Soil moisture (%)
        EM_Beta         = Beta                           ! Solar zenith angle
        EM_PAR_day      = 0.                             ! Incoming PAR daily average
        EM_SRAD_day     = 0.                             ! Daily average short wave radiation (W/m2)
        EM_TempK_day    = 0.                             ! Daily average temperature Kelvin

        EM_LAD          = LAD_P                          ! Leaf area density as [0,1] in the canopy layer 1

        EM_Mon_Proc(1)  = 0.010                          !  MYRC - data from Jaana chemotypy paper average values
        EM_Mon_Proc(2)  = 0.010                          !  SABI -                     "
        EM_Mon_Proc(3)  = 0.023                          !  LIMO -                     "
        EM_Mon_Proc(4)  = 0.396                          !  3CAR -                     "
        EM_Mon_Proc(5)  = 0.010                          !  OCIM -                     "
        EM_Mon_Proc(6)  = 0.090                          !  BPIN -                     "
        EM_Mon_Proc(7)  = 0.437                          !  APIN -                     "
        EM_Mon_Proc(8)  = 0.024                          !  OMTP -                     "


        IF (Treeflag == 1) THEN !pine
          !LAI_Megan is for Hyytiala monthly distribution
          EM_LAI = EM_LAI_year * LAI_Megan(Mon)/6.5728     ! One sided LAI (for needle leaf trees it is the projected area of the 3D needle)
          IF (mon /= 1) THEN
            EM_LAI_past = EM_LAI_year * LAI_Megan(Mon-1)/6.5728
          ELSE
            EM_LAI_past = EM_LAI_year * LAI_Megan(12)/6.5728
          END IF
        ELSEIF (Treeflag == 2) THEN !spruce
          EM_LAI = EM_LAI_year
          EM_LAI_past = EM_LAI
        ELSEIF (Treeflag == 3) THEN !birch
          EM_LAI = EM_LAI_year * LAI_function(EM_Julian)
          IF (julian /= 1) THEN
            EM_LAI_past = EM_LAI_year * LAI_function(EM_Julian-1)
          ELSE
            EM_LAI_past = EM_LAI_year * LAI_function(1)
          ENDIF
        ENDIF

        ! Output values set to zero for each run
        IF (Treeflag == 4) THEN
          EM_SUN_PAR      = 1.
        ENDIF

        EM_SUN_PAR      = 0.                             ! Array of sun fraction - 1 above the canopy and decreases inside the canopy
        EM_SunleafTK    = 0.                             ! Array of temparture for sun leaf
        EM_ShadeleafTK  = 0.                             ! Array of temparture for shade leaf
        EM_Sunfrac      = 0.                             ! Array of the fraction of sun leaves. i = 1 is the top canopy layer, 2 is the next layer, etc.
        EM_EMI          = 0.                             ! Emissions in molecules per cm3 per second
        EM_Ea1pL_M1     = 0.                             ! Emission factor light
        EM_Ea1tL_M1     = 0.                             ! Emission factor temperature
        EM_Ea1NL_M1     = 0.                             ! Emission factor compined
        EM_Ea1pL_M2     = 0.                             ! Emission factor light
        EM_Ea1tL_M2     = 0.                             ! Emission factor temperature
        EM_Ea1NL_M2     = 0.                             ! Emission factor compined
        EM_GAM_TMP      = 0.                             ! Gamma factor for temperature, Non isoprene
        EM_GAM_OTHER    = 0.                             ! Gamma factors for other parameters

        !  Layer list NOTE megan used the canopy for added accuracy so "cover" appears in every part of the canopy, cause it just changes a few variables
        ! in the calculations in comparison to the other plant functional types, (g/s)
        EM_ER_HB        = 0.0                            ! emission rate from Herbaceous
        EM_ER_SB        = 0.0                            ! emission rate from Shrubland(s) , see note above
        EM_ER_NT        = 0.0                            ! emission rate from Needle Trees, - || -
        EM_ER_BT        = 0.0                            ! emission rate from Broadleaf Trees, - || -


        CALL debug_message('DEBUGING: calculating emission')
        IF (Emiflag == 1) THEN ! old Megan code
          CALL EMISSION_M1(EM_TempK, EM_PAR, EM_Wind, EM_kz, EM_Julian, EM_EW, EM_LAI, EM_Beta, EM_Time, EM_LAD, EM_Can_Lay,   &
            EM_z, EM_Ea1pL_M1, EM_Ea1tL_M1, EM_Ea1NL_M1, EM_Emi, EM_SUN_PAR, EM_SunleafTK, EM_ShadeleafTK, EM_Sunfrac)
        ELSEIF (Emiflag == 2) THEN ! new Megan code
          Call EMISSION_M2(EM_Julian, EM_Can_Lay, EM_Beta, EM_Lat, EM_Long, EM_Date, EM_Time_M2, EM_PAR, EM_PAR_day,  &
            EM_TempK, EM_TempK_day, EM_Pres(2), EM_WVM, EM_WIND, EM_SMOIST, EM_LAD, EM_LAI, EM_LAI_past, &
            EM_ER, EM_VAR, EM_ER_HB, EM_ER_SB, EM_ER_NT, EM_ER_BT, EM_Ea1pL_M2,EM_Ea1NL_M2, EM_Ea1tL_M2, &
            EM_GAM_TMP, EM_GAM_OTHER, EM_z, EM_kz, EM_EMI, EM_SUN_PAR, EM_SunleafTK, EM_ShadeleafTK, EM_Sunfrac, STATION,   &
            EMI_MEAS_MONO, CHAM_temp, Treeflag, now_date(1),check_rh, tsn1, tsd1, Synflag)

          DO JJ = 1,EM_Can_Lay
            EM_Ea1pL_M1(JJ) = EM_Ea1pL_M2(2,JJ)
            EM_Ea1tL_M1(JJ) = EM_Ea1tL_M2(2,JJ)
            EM_Ea1NL_M1(JJ) = EM_Ea1NL_M2(2,JJ)
          ENDDO
        ELSEIF (Emiflag == 3) THEN ! SIMBIM - only for monoterpenes
          ! If we use SIMBIM we have to run also MEGAN to get the leaf temperatures and the emission rates for non-monoterpenes
          Call EMISSION_M2(EM_Julian, EM_Can_Lay, EM_Beta, EM_Lat, EM_Long, EM_Date, EM_Time_M2, EM_PAR, EM_PAR_day,           &
            EM_TempK, EM_TempK_day, EM_Pres(2), EM_WVM, EM_WIND, EM_SMOIST, EM_LAD, EM_LAI, EM_LAI_past,        &
            EM_ER, EM_VAR, EM_ER_HB, EM_ER_SB, EM_ER_NT, EM_ER_BT, EM_Ea1pL_M2,EM_Ea1NL_M2, EM_Ea1tL_M2,                    &
            EM_GAM_TMP, EM_GAM_OTHER, EM_z, EM_kz, EM_EMI, EM_sun_par, EM_SunleafTK, EM_ShadeleafTK, EM_Sunfrac, STATION,   &
            EMI_MEAS_MONO, CHAM_temp, Treeflag, now_date(1),check_rh, tsn1, tsd1, Synflag)

          DO JJ = 2, EM_Can_Lay
            ! Initial values for simbim for sunny (S) and shadow (C) leaves only at time equals zero
            IF (daytime .EQ. 0.) THEN
              EM_BIM_S(JJ,1)  = 30.0   ! start value for stomatal conductance
              EM_BIM_S(JJ,2)  = 380.0  ! start value for CI (CO2 inside)
              EM_BIM_S(JJ,3)  = 1.0    ! start value for photosynthesis rate
              EM_BIM_S(JJ,4)  = 10.0   ! start value PGA (phosphor-glycerate-aldehyde)
              EM_BIM_S(JJ,5)  = 30.0   ! start value GAP (
              EM_BIM_S(JJ,6)  = 100.0  ! start value and constant for NADPH ()
              EM_BIM_S(JJ,7)  = 0.0    ! start value DXP
              EM_BIM_S(JJ,8)  = 0.0    ! start value MEP
              EM_BIM_S(JJ,9)  = 0.0    ! start value IDP
              EM_BIM_S(JJ,10) = 0.0    ! start value DMADP
              EM_BIM_S(JJ,11) = 0.0    ! start value Isoprene production rate [nmol / m2-leaf area / s]
              EM_BIM_S(JJ,12) = 0.0    ! start value GDP
              EM_BIM_S(JJ,13) = 0.0    ! start value Monoterpene production rate [nmol / m2-leaf area / s]
              EM_BIM_S(JJ,14) = 0.0    ! output [nmol / m2-leaf area / s]

              EM_BIM_C(JJ,1)  = 30.0   ! start value for stomata conductance
              EM_BIM_C(JJ,2)  = 380.0  ! start value for CI (CO2 inside)
              EM_BIM_C(JJ,3)  = 1.0    ! start value for photosynthesis rate
              EM_BIM_C(JJ,4)  = 10.0   ! start value PGA (phosphor-glycerate-aldehyde)
              EM_BIM_C(JJ,5)  = 30.0   ! start value GAP (
              EM_BIM_C(JJ,6)  = 100.0  ! start value and constant for NADPH ()
              EM_BIM_C(JJ,7)  = 0.0    ! start value DXP
              EM_BIM_C(JJ,8)  = 0.0    ! start value MEP
              EM_BIM_C(JJ,9)  = 0.0    ! start value IDP
              EM_BIM_C(JJ,10) = 0.0    ! start value DMADP
              EM_BIM_C(JJ,11) = 0.0    ! start value Isoprene production rate [nmol / m2-leaf area / s]
              EM_BIM_C(JJ,12) = 0.0    ! start value GDP
              EM_BIM_C(JJ,13) = 0.0    ! start value Monoterpene production rate [nmol / m2-leaf area / s]
              EM_BIM_C(JJ,14) = 0.0    ! output [nmol / m2-leaf area / s]
            ENDIF


            ! Calculation of VPD (vapour pressure deficit) in kPA
            EM_SunleafTC(JJ)   = EM_SunleafTK(JJ) - 273.15
            EM_ShadeleafTC(JJ) = EM_ShadeleafTK(JJ) - 273.15

            EM_ES_S(JJ) = (a0 + a1 * EM_SunleafTC(JJ)**1 + a2 * EM_SunleafTC(JJ)**2 + a3 * EM_SunleafTC(JJ)**3                &
              + a4 * EM_SunleafTC(JJ)**4 + a5 * EM_SunleafTC(JJ)**5 + a6 * EM_SunleafTC(JJ)**6) * 100

            EM_ES_C(JJ) = (a0 + a1 * EM_ShadeleafTC(JJ)**1 + a2 * EM_ShadeleafTC(JJ)**2 + a3 * EM_ShadeleafTC(JJ)**3          &
              + a4 * EM_ShadeleafTC(JJ)**4 + a5 * EM_ShadeleafTC(JJ)**5 + a6 * EM_ShadeleafTC(JJ)**6) * 100

            EM_VPD_S(JJ) = (EM_ES_S(JJ) - EM_EW(JJ)) / 1000
            EM_VPD_C(JJ) = (EM_ES_C(JJ) - EM_EW(JJ)) / 1000

            IF (EM_VPD_S(JJ) .GT. 3) THEN
              EM_VPD_S(JJ) = 3
            ENDIF
            IF (EM_VPD_C(JJ) .GT. 3) THEN
              EM_VPD_C(JJ) = 3
            ENDIF

            ! start value for integral of monoterpenes [nmol / m2-leaf area - during the time step] has to be zero before call
            EM_BIM_S(JJ,14) = 0.0
            EM_BIM_C(JJ,14) = 0.0

            ! Call of simbim 2 times for sunny and shadow leave temperature at each height level inside the canopy
            EM_Time_in    = daytime/60.
            EM_Time_out   = EM_Time_in + EM_DT/60.

            CALL EMISSION_SB(EM_BIM_S(JJ,:), EM_Time_in, EM_Time_out, EM_PAR_SB, EM_VPD_S(JJ))

            CALL EMISSION_SB(EM_BIM_C(JJ,:), EM_Time_in, EM_Time_out, EM_PAR_SB, EM_VPD_C(JJ))

            ! Emission rates from SIMBIM for all monoterpenes
            DO II = 2,9
              EM_Emi(JJ,II)  = (EM_BIM_S(JJ,14) * EM_Sunfrac(EM_Can_Lay+1-JJ) + &
                EM_BIM_C(JJ,14) * (1-EM_Sunfrac(EM_Can_Lay+1-JJ)))     &
                * 1d-9 * Avog * 1d-4 * 6.7d0 * EM_Mon_Proc(II-1) / (EM_z(JJ)-EM_z(JJ-1)) / 100 * EM_LAD(JJ) / 1000.
            ENDDO

          ENDDO  ! JJ = 2, EM_Can_Lay

          ! SIMBIM does not calculate emissions for the first layer so we use the second layer twice
          DO II = 2,9
            EM_Emi(1,II)  = EM_Emi(2,II)
          ENDDO

        ENDIF  ! IF Emiflag == 1
      ENDIF  ! IF Emiflag is 1 or 2 or 3

      IF (Treeflag == 4) THEN
        EM_SUN_PAR(1:kz) = 1.0d0
      ENDIF
      !**********************************************************************************************************************
      !
      ! CHEMISTRY solved with KPP
      !
      !**********************************************************************************************************************


      IF (CHEMflag == 1) THEN

        !#ifdef PARALLEL
        mpi_sendcount = 0
        mpi_recvcount = 0
        ! CALL CPU_TIME(wtime1)
        !#endif

        CALL debug_message('DEBUGING: interpolating gas concentrations')
        CH_CONS_ALL(:,ind_O3)  = linear_interp(montime, nxodrad, CH_gas_hyy(2,:), dt_obs) * air(:)*1.0d-9  ! only use observation at level nz

        CH_CONS_ALL(:,ind_SO2) = linear_interp(montime, nxodrad, CH_gas_hyy(3,:), dt_obs) * air(:)*1.0d-9

        CH_CONS_ALL(:,ind_NO) = linear_interp(montime, nxodrad, CH_gas_hyy(4,:), dt_obs) * air(:)*1.0d-9

        CH_CONS_ALL(:,ind_NO2) = linear_interp(montime, nxodrad, CH_gas_hyy(5,:), dt_obs) * air(:)*1.0d-9  ! NO2=NOx-NO, already done in preprocessing
        CH_CONS_ALL(:,ind_CO) = linear_interp(montime, nxodrad, CH_gas_hyy(6,:), dt_obs) * air(:)*1.0d-9
        if (NH3flag==1)then
          NH3_hyy_ALL = linear_interp(montime, nxodrad, NH3_hyy, dt_obs) * air(:)*1.0d-9
        else if (NH3flag==2)then
          !if number concentration is used
          NH3_hyy_ALL = linear_interp(montime, nxodrad, NH3_hyy, dt_obs)
        endif
        if (Emiflag==0)then
          CH_CONS_ALL(2,ind_APINENE) = linear_interp(montime, nxodrad, CH_VOC_hyy(1,:), dt_obs) * air(2)*1.0d-9*0.51
          CH_CONS_ALL(2,ind_BPINENE) = linear_interp(montime, nxodrad, CH_VOC_hyy(1,:), dt_obs) * air(2)*1.0d-9*0.12
          CH_CONS_ALL(2,ind_LIMONENE) = linear_interp(montime, nxodrad, CH_VOC_hyy(1,:), dt_obs) * air(2)*1.0d-9*0.09
          CH_CONS_ALL(2,ind_CARENE) = linear_interp(montime, nxodrad, CH_VOC_hyy(1,:), dt_obs) * air(2)*1.0d-9*0.28
          CH_CONS_ALL(2,ind_C5H8) = linear_interp(montime, nxodrad, CH_VOC_hyy(2,:), dt_obs) * air(2)*1.0d-9
          ! CH_CONS_ALL(2,ind_MVK) = linear_interp(montime, nxodrad, CH_VOC_hyy(3,:), dt_obs) * air(2)*1.0d-9
          !        CH_CONS_ALL(2,ind_MEK) = linear_interp(montime, nxodrad, CH_VOC_hyy(4,:), dt_obs) * air(2)*1.0d-9
          CH_CONS_ALL(2,ind_CH3OH) = linear_interp(montime, nxodrad, CH_VOC_hyy(5,:), dt_obs) * air(2)*1.0d-9
          CH_CONS_ALL(2,ind_CH3CHO) = linear_interp(montime, nxodrad, CH_VOC_hyy(6,:), dt_obs) * air(2)*1.0d-9
          !        CH_CONS_ALL(2,ind_C2H5OH) = linear_interp(montime, nxodrad, CH_VOC_hyy(7,:), dt_obs) * air(2)*1.0d-9
          CH_CONS_ALL(2,ind_CH3COCH3) = linear_interp(montime, nxodrad, CH_VOC_hyy(8,:), dt_obs) * air(2)*1.0d-9
          CH_CONS_ALL(2,ind_CH3CO2H) = linear_interp(montime, nxodrad, CH_VOC_hyy(9,:), dt_obs) * air(2)*1.0d-9
          CH_CONS_ALL(2,ind_H2SO4) = linear_interp(montime, nxodrad, CH_H2SO4_hyy, dt_obs)
        endif
        ! Read CH4 concentration
        IF ( TRIM(ADJUSTL(STATION)) == 'hyytiala' )  THEN
          ! The CH4 concentration was measured with Picarro G1301 (Picarro Inc., USA) and it has been corrected for
          ! dilution and spectroscopic effects by the instrument itself. Data processed by Olli Peltola.
          ! There exists measurements from 16.8, 67.2 and 125 m, but since they are mostly all most identical,
          ! for now I only use the values from 16.8 m. Only the measurements from year 2014 are good.
          ! For Aug I used the same input as for July, since no data is avail.
          ! According to IPPC then the atmospheric growth rate of methane has
          ! been 6 ppb yr-1 the past 10 years or so. unit of input: ppm.

          ! Read CH4 concentration in 2014 and add trend to obtain data in other years
          CH_CONS_all(:, ind_CH4) = linear_interp(montime, nxodrad, hyy_ch4 + (now_date(1)-2014) * 0.006_dp, dt_obs) * air(:) / 1.0e6_dp
        ELSE
          CH_CONS_all(:,ind_CH4) = (1.8 * air(:)) / 1.E6 !* 1.0562 !year 2050
        ENDIF

        CH_CONS_ALL(:,ind_H2)  = (0.5d0 * air(:)) * 1.0d-6

        ! Emissions are transfered to the correct KPP-form
        DO k = 1, kz
          ! The subroutine is different for different chemistry schemes, and you can
          ! write it as what you wish for a specific chemistry scheme
          CALL set_emissions_for_chemistry(CH_CONS_ALL(k, :), EM_Emi(k, :), dt_chem)
        END DO
        ! CH_CONS_ALL(:,ind_Emi1)  =  EM_Emi(:,1)   * dt_chem  ! C5H8 (Isoprene)
        ! CH_CONS_ALL(:,ind_Emi2)  =  EM_Emi(:,2)   * dt_chem  ! Myrcene
        ! CH_CONS_ALL(:,ind_Emi3)  =  EM_Emi(:,3)   * dt_chem  ! Sabinene
        ! CH_CONS_ALL(:,ind_Emi4)  =  EM_Emi(:,4)   * dt_chem  ! LIMONENE
        ! CH_CONS_ALL(:,ind_Emi5)  =  EM_Emi(:,5)   * dt_chem  ! Carene
        ! CH_CONS_ALL(:,ind_Emi6)  =  EM_Emi(:,6)   * dt_chem  ! Ocimene
        ! CH_CONS_ALL(:,ind_Emi7)  =  EM_Emi(:,7)   * dt_chem  ! Bpinene
        ! CH_CONS_ALL(:,ind_Emi8)  =  EM_Emi(:,8)   * dt_chem  ! Apinene
        ! CH_CONS_ALL(:,ind_Emi9)  =  EM_Emi(:,9)   * dt_chem  ! Other monoterpenes
        ! CH_CONS_ALL(:,ind_Emi10) =  EM_Emi(:,10)  * dt_chem  ! Farnesene
        ! CH_CONS_ALL(:,ind_Emi11) =  EM_Emi(:,11)  * dt_chem  ! BCARY (Beta-Carophyllene)
        ! CH_CONS_ALL(:,ind_Emi12) =  EM_Emi(:,12)  * dt_chem  ! Other sesquiterpenes
        ! CH_CONS_ALL(:,ind_Emi13) =  EM_Emi(:,13)  * dt_chem  ! MBO (2methyl-3buten-2ol)
        ! CH_CONS_ALL(:,ind_Emi14) =  EM_Emi(:,14)  * dt_chem  ! CH3OH (Methanol)
        ! CH_CONS_ALL(:,ind_Emi15) =  EM_Emi(:,15)  * dt_chem  ! CH3COCH3 (Aceton)
        ! CH_CONS_ALL(:,ind_Emi16) =  EM_Emi(:,16)  * dt_chem  ! CH4 (Methane)
        ! CH_CONS_ALL(:,ind_Emi17) =  EM_Emi(:,17)  * dt_chem  ! NO
        ! CH_CONS_ALL(:,ind_Emi18) =  EM_Emi(:,18)  * dt_chem  ! CH3CHO (Acetaldehyde)
        ! CH_CONS_ALL(:,ind_Emi19) =  EM_Emi(:,19)  * dt_chem  ! HCHO (Formaldehyde)
        ! CH_CONS_ALL(:,ind_Emi20) =  EM_Emi(:,20)  * dt_chem  ! CO
        ! CH_CONS_ALL(:,ind_Emi21) =  EM_Emi(:,21)  * dt_chem  ! Cineole (Eucalyptol) (not included in the new megan code - used is the same values as Sabinene)
        ! CH_CONS_ALL(:,ind_Emi22) =  EM_Emi(:,22)  * dt_chem  ! Linalool

        CALL debug_message('DEBUGING: sending and receiving chemistry data with MPI')
        DO k = 1, kz  ! loop over kz
          !#ifdef PARALLEL
          IF (mpi_sendcount >= mpi_nslaves) THEN
            ! receive data from a slave
            CALL MPI_RECV(mpi_recv_buffer,mpi_recv_buffer_size,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,MPI_ANY_TAG, &
              MPI_COMM_WORLD,mpi_status,mpi_rc)
            kz_tag = INT(mpi_recv_buffer(index_kz_tag))
            mpi_source_id = mpi_status(MPI_SOURCE)
            CH_RO2(kz_tag) = mpi_recv_buffer(index_RO2)
            CH_H2O(kz_tag) = mpi_recv_buffer(index_H2O_recv)
            CH_CONS = mpi_recv_buffer(index1_CONS_recv:index2_CONS_recv)

            CH_J_values_ALL(kz_tag,:) = mpi_recv_buffer(index1_J_values_recv:index2_J_values_recv)
            CH_K_values_ALL(kz_tag,:) = mpi_recv_buffer(index1_K_values_recv:index2_K_values_recv)
            CH_ACF_ALL(kz_tag,:) = mpi_recv_buffer(index1_ACF_recv:index2_ACF_recv)

            CH_CONS_ALL(kz_tag,:) = CH_CONS(:)

            mpi_recvcount = mpi_recvcount + 1
            mpi_dest_id = mpi_source_id
          ELSE
            mpi_dest_id = mpi_sendcount + 1
          END IF  ! mpi_sendcound >= mpi_nslaves


          ! send message "chemicals databuffer coming" to slave
          CALL MPI_SEND(mpi_do_chemistry_code,1,MPI_INTEGER,mpi_dest_id,mpi_task_code_tag,MPI_COMM_WORLD,mpi_rc)
          !#endif

          ! Prepare all things for sending data

          CH_TIME_kpp = daytime
          CH_END_kpp  = daytime + dt_chem


          IF (Soilflag == 1) THEN !This is to read in measured VOC soil emission
            !Currently we only have high resolution soil emission data for HUMPPA-COPEC - so parts of july and August
            !This is not completely true, cause Qingyang has more data. I will implement this (soon).
            IF (((mon .EQ. 7) .OR. (mon .EQ. 8)) .AND. (k .EQ. 2)) THEN
              ! read in soil emission data. Unit: molecules/cm^3/s
              open(unit=709,file = TRIM(ADJUSTL(input_dir_station_data))//'/soil_aaltonen.txt', &
                status='old')
              read(709,*)((EM_soil_voc(i20,j20),i20=1,13),j20=1,1488)
              close(unit=709)

              DO I=1,12 !we start at time 00.00
                EM_soil_emi(I)  = (EM_soil_voc(I+1,nxodrad) + (montime-(nxodrad-1)*dt_obs) *  &
                  (EM_soil_voc(I+1,nxodrad+1)-EM_soil_voc(I+1,nxodrad))/dt_obs)
                !Some emission values are negative, and since we already take deposition into account, we here exclude the negative values.
                IF (EM_soil_emi(I) .LT. 0.) THEN
                  EM_soil_emi(I) = 0.
                ENDIF
              ENDDO

              ! Set soil emissions for chemistry
              CALL set_soil_emissions_for_chemistry_1(CH_CONS_ALL(k, :), EM_soil_emi(:), &
                dt_chem, z(k)-z(k-1))
            ENDIF
          ENDIF  ! Soilflag==1

          IF (Soilflag == 2) THEN !This is in order to include Hermanni's parameterisation.
            !This should only be used for M05-M11
            IF (((mon .EQ. 5) .OR. (mon .EQ. 6) .OR. (mon .EQ. 7) .OR. (mon .EQ. 8) .OR. (mon .EQ. 9) .OR.  &
              (mon .EQ. 10) .OR. (mon .EQ. 11)) .AND. (k .EQ. 2)) THEN
              IF (ta1(temp_level) .GT. 278.15) THEN
                M137_soil(k) = (((0.3 * (((ta1(temp_level)-273.15) * SMOB)**2)) - &
                  (0.6 * ((ta1(temp_level)-273.15) * SMOB)) + 2) * conversion * 1/136) / ((z(k) - z(k-1))*100)
                M33_soil(k) = (((0.2 * (((ta1(temp_level)-273.15) * SMOH)**2)) - &
                  (0.35 * ((ta1(temp_level)-273.15) * SMOH)) + 0.02) * conversion * 1/32) / ((z(k) - z(k-1))*100)
                M69_soil(k) = (((0.005 * (((ta1(temp_level)-273.15) * SMOH)**2)) + &
                  (0.015 * ((ta1(temp_level)-273.15) * SMOH))) * conversion * 1/68) / ((z(k) - z(k-1))*100)
              ELSE
                M137_soil(k) = 0.
                M33_soil(k) = 0.
                M69_soil(k) = 0.
              ENDIF

              CALL set_soil_emissions_for_chemistry_2(CH_CONS_ALL(k, :), &
                M137_soil(k), M33_soil(k), M69_soil(k), dt_chem)
            ENDIF
          ENDIF  ! Soilflag==2

          IF (Treeflag == 4) THEN !CLEARCUT

            !Added night time effect:
            IF ((daytime .GE. 0) .AND. (daytime .LT. 5.0*60.0*60.0)) THEN
              sep_factor = 1.0/3.0
            ELSEIF ((daytime .GE. 5.0*60.0*60.0) .AND. (daytime .LE. 15.0*60.0*60.0)) THEN
              sep_factor = 2.0/(3.0*36000.0)*daytime
            ELSEIF ((daytime .GT. 15.0*60.0*60.0)  .AND. (daytime .LT. 24.0*60.0*60.0)) THEN
              sep_factor = (-2.0/97200.0 * daytime) + 2.1111
            ENDIF

            SEP_clear = SAMI(julian,3) * (1.0/1.0D6) * (1.0/1.0D4) * (1.0/3600.0) * sep_factor !* 1.1 !2050

            !Top canopy layer, assuming 30% of total emission:
            EM_Emi(12,8) = (0.3 * 0.437 * SEP_clear * Avog) / (136 * ((z(12)-z(11))* 100)) !A-pinene
            EM_Emi(12,7) = (0.3 * 0.090 * SEP_clear * Avog) / (136 * ((z(12)-z(11))* 100)) !b-pinene
            EM_Emi(12,5) = (0.3 * 0.396 * SEP_clear * Avog) / (136 * ((z(12)-z(11))* 100)) !carene
            EM_Emi(12,4) = (0.3 * 0.023 * SEP_clear * Avog) / (136 * ((z(12)-z(11))* 100)) !limonene
            EM_Emi(12,21) = (0.3 * 0.001 * SEP_clear * Avog) / (136 * ((z(12)-z(11))* 100)) !cineole
            EM_Emi(12,9) = (0.3 * 0.053 * SEP_clear * Avog) / (136 * ((z(12)-z(11))* 100)) !OMT
            CALL set_clearcut_emissions_for_chemistry(CH_CONS_ALL(12, :), EM_Emi(12, :), dt_chem)
            ! CH_CONS_ALL(12,ind_Emi8)  = dt_chem *EM_Emi(12,8)
            ! CH_CONS_ALL(12,ind_Emi7)  = dt_chem *EM_Emi(12,7)
            ! CH_CONS_ALL(12,ind_Emi5)  = dt_chem *EM_Emi(12,5)
            ! CH_CONS_ALL(12,ind_Emi4)  = dt_chem *EM_Emi(12,4)
            ! CH_CONS_ALL(12,ind_Emi21)  = dt_chem *EM_Emi(12,21)
            ! CH_CONS_ALL(12,ind_Emi9)  = dt_chem *EM_Emi(12,9)
            !Second top canopy layer, assuming 40% of total emission:
            EM_Emi(11,8) = (0.4 * 0.437 * SEP_clear * Avog) / (136 * ((z(11)-z(10))* 100)) !A-pinene
            EM_Emi(11,7) = (0.4 * 0.090 * SEP_clear * Avog) / (136 * ((z(11)-z(10))* 100)) !b-pinene
            EM_Emi(11,5) = (0.4 * 0.396 * SEP_clear * Avog) / (136 * ((z(11)-z(10))* 100)) !carene
            EM_Emi(11,4) = (0.4 * 0.023 * SEP_clear * Avog) / (136 * ((z(11)-z(10))* 100)) !limonene
            EM_Emi(11,21) = (0.4 * 0.001 * SEP_clear * Avog) / (136 * ((z(11)-z(10))* 100)) !cineole
            EM_Emi(11,9) = (0.4 * 0.053 * SEP_clear * Avog) / (136 * ((z(11)-z(10))* 100)) !OMT
            CALL set_clearcut_emissions_for_chemistry(CH_CONS_ALL(11, :), EM_Emi(11, :), dt_chem)
            ! CH_CONS_ALL(11,ind_Emi8)  = dt_chem *EM_Emi(11,8)
            ! CH_CONS_ALL(11,ind_Emi7)  = dt_chem *EM_Emi(11,7)
            ! CH_CONS_ALL(11,ind_Emi5)  = dt_chem *EM_Emi(11,5)
            ! CH_CONS_ALL(11,ind_Emi4)  = dt_chem *EM_Emi(11,4)
            ! CH_CONS_ALL(11,ind_Emi21)  = dt_chem *EM_Emi(11,21)
            ! CH_CONS_ALL(11,ind_Emi9)  = dt_chem *EM_Emi(11,9)
            !Third top canopy layer, assuming 30% of total emission:
            EM_Emi(10,8) = (0.3 * 0.437 * SEP_clear * Avog) / (136 * ((z(10)-z(9))* 100)) !A-pinene
            EM_Emi(10,7) = (0.3 * 0.090 * SEP_clear * Avog) / (136 * ((z(10)-z(9))* 100)) !b-pinene
            EM_Emi(10,5) = (0.3 * 0.396 * SEP_clear * Avog) / (136 * ((z(10)-z(9))* 100)) !carene
            EM_Emi(10,4) = (0.3 * 0.023 * SEP_clear * Avog) / (136 * ((z(10)-z(9))* 100)) !limonene
            EM_Emi(10,21) = (0.3 * 0.001 * SEP_clear * Avog) / (136 * ((z(10)-z(9))* 100)) !cineole
            EM_Emi(10,9) = (0.3 * 0.053 * SEP_clear * Avog) / (136 * ((z(10)-z(9))* 100)) !OMT
            CALL set_clearcut_emissions_for_chemistry(CH_CONS_ALL(10, :), EM_Emi(10, :), dt_chem)
            ! CH_CONS_ALL(10,ind_Emi8)  = dt_chem *EM_Emi(10,8)
            ! CH_CONS_ALL(10,ind_Emi7)  = dt_chem *EM_Emi(10,7)
            ! CH_CONS_ALL(10,ind_Emi5)  = dt_chem *EM_Emi(10,5)
            ! CH_CONS_ALL(10,ind_Emi4)  = dt_chem *EM_Emi(10,4)
            ! CH_CONS_ALL(10,ind_Emi21)  = dt_chem *EM_Emi(10,21)
            ! CH_CONS_ALL(10,ind_Emi9)  = dt_chem *EM_Emi(10,9)

            !If we only read in the emission near ground:
            ! EM_Emi(2,8) = (0.437 * SEP_clear * Avog) / (136 * ((z(2)-z(1))* 100)) !A-pinene
            ! EM_Emi(2,7) = (0.090 * SEP_clear * Avog) / (136 * ((z(2)-z(1))* 100)) !b-pinene
            ! EM_Emi(2,5) = (0.396 * SEP_clear * Avog) / (136 * ((z(2)-z(1))* 100)) !carene
            ! EM_Emi(2,4) = (0.023 * SEP_clear * Avog) / (136 * ((z(2)-z(1))* 100)) !limonene
            ! EM_Emi(2,21) = (0.001 * SEP_clear * Avog) / (136 *((z(2)-z(1)) * 100)) !cineole
            ! EM_Emi(2,9) = (0.053 * SEP_clear * Avog) / (136 * ((z(2)-z(1))* 100)) !OMT
            ! CH_CONS_ALL(2,ind_Emi8)  = dt_chem *EM_Emi(2,8)
            ! CH_CONS_ALL(2,ind_Emi7)  = dt_chem *EM_Emi(2,7)
            ! CH_CONS_ALL(2,ind_Emi5)  = dt_chem *EM_Emi(2,5)
            ! CH_CONS_ALL(2,ind_Emi4)  = dt_chem *EM_Emi(2,4)
            ! CH_CONS_ALL(2,ind_Emi21)  = dt_chem *EM_Emi(2,21)
            ! CH_CONS_ALL(2,ind_Emi9)  = dt_chem *EM_Emi(2,9)

          ENDIF  ! IF (Treeflag == 4) THEN  ! CLEARCUT

          CALL debug_message('DEBUGING: interpolating condensation sinks')
          CH_ALBEDO = ALBEDO(nxodrad)   + (montime-(nxodrad-1)*dt_obs) * (ALBEDO(nxodrad+1)  - ALBEDO(nxodrad))/dt_obs

          !         CH_CS     = cs_hyy(nxodrad)   + (montime-(nxodrad-1)*dt_obs) * (cs_hyy(nxodrad+1)  - cs_hyy(nxodrad))/dt_obs
          CH_CS2    = cs_hyy2(nxodrad)  + (montime-(nxodrad-1)*dt_obs) * (cs_hyy2(nxodrad+1) - cs_hyy2(nxodrad))/dt_obs

          PAR       = par_hyy(nxodrad)  + (montime-(nxodrad-1)*dt_obs) * (par_hyy(nxodrad+1) - par_hyy(nxodrad))/dt_obs

          ! Now the CH_CS (H2SO4) is calculated in uhma
          CH_CS = sink(:,ambient%sulfuric_acid_index)
          CH_RES1 = CH_CS(k)
          CH_RES2 = CH_CS2
          CH_CONS(:) = CH_CONS_ALL(k,:)

          !#ifdef PARALLEL

          mpi_send_buffer(index_kz_tag) = REAL(k,KIND=dp)
          mpi_send_buffer(index_time1) = CH_TIME_kpp
          mpi_send_buffer(index_time2) = CH_TIME_kpp + dt_chem
          mpi_send_buffer(index_tmcontr) = montime
          mpi_send_buffer(index_year) = REAL(now_date(1), dp)
          mpi_send_buffer(index_month) = REAL(now_date(2), dp)
          mpi_send_buffer(index_nxodrad) = REAL(nxodrad,KIND=dp)
          mpi_send_buffer(index_ta1) = ta1(k)
          mpi_send_buffer(index_pr1) = pres(k)
          mpi_send_buffer(index_beta) = Beta
          mpi_send_buffer(index_sun_par) = EM_SUN_PAR(k)
          mpi_send_buffer(index_res1) = CH_RES1
          mpi_send_buffer(index_res2) = CH_RES2
          mpi_send_buffer(index_qa1) = qa1(k)
          mpi_send_buffer(index_H2O) = CH_H2O(k)
          mpi_send_buffer(index1_CONS:index2_CONS) = CH_CONS
          mpi_send_buffer(index_albedo) = CH_ALBEDO
          mpi_send_buffer(index_glo) = GLO

          ! send chemicals databuffer to slave
          CALL MPI_SEND(mpi_send_buffer,mpi_send_buffer_size,MPI_DOUBLE_PRECISION,mpi_dest_id,mpi_buffer_tag, &
            MPI_COMM_WORLD,mpi_rc)

          mpi_sendcount = mpi_sendcount + 1

          !#else
          !
          !              ! Calculates the chemistry in current layer
          !              CALL CHEMISTRY(CH_CONS, CH_TIME_kpp, CH_TIME_kpp+dt_chem, montime, now_date(1), mon, nxodrad, &
          !                   ta1(k), pres(k), Beta, EM_SUN_PAR(k), CH_RES1, CH_RES2, qa1(k), CH_H2O(k), CH_RO2(k), &
          !                   CH_J_values, CH_K_values, GLO, STATION, CH_ALBEDO)
          !
          !              DO J = 1,NSPEC
          !                 CH_CONS_ALL(k,J) = CH_CONS(J)
          !              ENDDO
          !
          !              CH_J_values_ALL(k,:) = CH_J_values
          !              CH_K_values_ALL(k,:) = CH_K_values
          !
          !#endif

        ENDDO  ! loop over kz


        !#ifdef PARALLEL

        ! reveive data from the rest of the slaves
        DO slave_i = 1,mpi_nslaves
          ! receive data from a slave
          CALL MPI_RECV(mpi_recv_buffer,mpi_recv_buffer_size,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,MPI_ANY_TAG, &
            MPI_COMM_WORLD,MPI_STATUS_IGNORE,mpi_rc)

          kz_tag = INT(mpi_recv_buffer(index_kz_tag))
          CH_RO2(kz_tag) = mpi_recv_buffer(index_RO2)
          CH_H2O(kz_tag) = mpi_recv_buffer(index_H2O_recv)
          CH_CONS = mpi_recv_buffer(index1_CONS_recv:index2_CONS_recv)

          CH_J_values_ALL(kz_tag,:) = mpi_recv_buffer(index1_J_values_recv:index2_J_values_recv)
          CH_K_values_ALL(kz_tag,:) = mpi_recv_buffer(index1_K_values_recv:index2_K_values_recv)
          CH_ACF_ALL(kz_tag,:) = mpi_recv_buffer(index1_ACF_recv:index2_ACF_recv)

          CH_CONS_ALL(kz_tag, 1:NSPEC) = CH_CONS(1:NSPEC)  ! concentrations after chemistry reactions (already including emissions here!!!)
        END DO


        ! CALL CPU_TIME(wtime2)
        ! wwtime = wwtime + (wtime2-wtime1)
        ! wwcount = wwcount + 1
        !#endif

        ! Then process OH-reactivities
        IF (CH_oh_count > 0) THEN  ! only do this if we have rOH-pseudochemicals in the chemicals list
          CH_oh_cons3(:,:,1) = (CH_CONS_ALL(:,CH_oh_indices) - CH_oh_prev3(:,:,1)) / dt_chem
          CH_oh_prev3(:,:,1) = CH_CONS_ALL(:,CH_oh_indices)
          IF (MOD( INT(daytime), INT(CH_oh_step2*dt_chem) ) == 0) THEN
            CH_oh_cons3(:,:,2) = (CH_CONS_ALL(:,CH_oh_indices) - CH_oh_prev3(:,:,2)) / (CH_oh_step2 * dt_chem)
            CH_oh_prev3(:,:,2) = CH_CONS_ALL(:,CH_oh_indices)
          ENDIF
          IF (MOD( INT(daytime), INT(CH_oh_step3*dt_chem) ) == 0) THEN
            CH_oh_cons3(:,:,3) = (CH_CONS_ALL(:,CH_oh_indices) - CH_oh_prev3(:,:,3)) / (CH_oh_step3 * dt_chem)
            CH_oh_prev3(:,:,3) = CH_CONS_ALL(:,CH_oh_indices)
            CH_CONS_ALL(:,CH_oh_indices) = 0  ! this may be not necessary, maybe the numbers would not grow too large
            CH_oh_prev3 = 0
          ENDIF
        ENDIF

        ! Then process O3-reactivities
        IF (CH_o3_count > 0) THEN  ! only do this if we have rO3-pseudochemicals in the chemicals list
          CH_o3_cons3(:,:,1) = (CH_CONS_ALL(:,CH_o3_indices) - CH_o3_prev3(:,:,1)) / dt_chem
          CH_o3_prev3(:,:,1) = CH_CONS_ALL(:,CH_o3_indices)
          IF (MOD( INT(daytime), INT(CH_o3_step2*dt_chem) ) == 0) THEN
            CH_o3_cons3(:,:,2) = (CH_CONS_ALL(:,CH_o3_indices) - CH_o3_prev3(:,:,2)) / (CH_o3_step2 * dt_chem)
            CH_o3_prev3(:,:,2) = CH_CONS_ALL(:,CH_o3_indices)
          ENDIF
          IF (MOD( INT(daytime), INT(CH_o3_step3*dt_chem) ) == 0) THEN
            CH_o3_cons3(:,:,3) = (CH_CONS_ALL(:,CH_o3_indices) - CH_o3_prev3(:,:,3)) / (CH_o3_step3 * dt_chem)
            CH_o3_prev3(:,:,3) = CH_CONS_ALL(:,CH_o3_indices)
            CH_CONS_ALL(:,CH_o3_indices) = 0  ! this may be not necessary, maybe the numbers would not grow too large
            CH_o3_prev3 = 0               !
          ENDIF
        ENDIF

        ! Then process NO3-reactivities
        IF (CH_no3_count > 0) THEN  ! only do this if we have rNO3-pseudochemicals in the chemicals list
          CH_no3_cons3(:,:,1) = (CH_CONS_ALL(:,CH_no3_indices) - CH_no3_prev3(:,:,1)) / dt_chem
          CH_no3_prev3(:,:,1) = CH_CONS_ALL(:,CH_no3_indices)
          IF (MOD( INT(daytime), INT(CH_no3_step2*dt_chem) ) == 0) THEN
            CH_no3_cons3(:,:,2) = (CH_CONS_ALL(:,CH_no3_indices) - CH_no3_prev3(:,:,2)) / (CH_no3_step2 * dt_chem)
            CH_no3_prev3(:,:,2) = CH_CONS_ALL(:,CH_no3_indices)
          ENDIF
          IF (MOD( INT(daytime), INT(CH_no3_step3*dt_chem) ) == 0) THEN
            CH_no3_cons3(:,:,3) = (CH_CONS_ALL(:,CH_no3_indices) - CH_no3_prev3(:,:,3)) / (CH_no3_step3 * dt_chem)
            CH_no3_prev3(:,:,3) = CH_CONS_ALL(:,CH_no3_indices)
            CH_CONS_ALL(:,CH_no3_indices) = 0  ! this may be not necessary, maybe the numbers would not grow too large
            CH_no3_prev3 = 0               !
          ENDIF
        ENDIF

      ENDIF  ! should this be after the time_par? End of CHEMflag .EQ. 1.

      !***** Cumulative emissions *****!
      DO I=1, outemi_count
        Qemis(:, outemi_cheminds(I)) = Qemis(:, outemi_cheminds(I)) + EM_Emi(:, outemi_meganinds(I)) * dt_chem
      END DO
      ! Qemis(:,ind_C5H8)      =  Qemis(:,ind_C5H8)      + EM_Emi(:,1)  * dt_chem  ! C5H8 (Isoprene)
      ! Qemis(:,ind_Myrcene)   =  Qemis(:,ind_Myrcene)   + EM_Emi(:,2)  * dt_chem  ! Myrcene
      ! Qemis(:,ind_Sabinene)  =  Qemis(:,ind_Sabinene)  + EM_Emi(:,3)  * dt_chem  ! Sabinene
      ! Qemis(:,ind_LIMONENE)  =  Qemis(:,ind_LIMONENE)  + EM_Emi(:,4)  * dt_chem  ! LIMONENE
      ! Qemis(:,ind_Carene)    =  Qemis(:,ind_Carene)    + EM_Emi(:,5)  * dt_chem  ! Carene
      ! Qemis(:,ind_Ocimene)   =  Qemis(:,ind_Ocimene)   + EM_Emi(:,6)  * dt_chem  ! Ocimene
      ! Qemis(:,ind_BPINENE)   =  Qemis(:,ind_BPINENE)   + EM_Emi(:,7)  * dt_chem  ! Bpinene
      ! Qemis(:,ind_APINENE)   =  Qemis(:,ind_APINENE)   + EM_Emi(:,8)  * dt_chem  ! Apinene
      ! Qemis(:,ind_OMT)       =  Qemis(:,ind_OMT)       + EM_Emi(:,9)  * dt_chem  ! Other monoterpenes
      ! Qemis(:,ind_Farnesene) =  Qemis(:,ind_Farnesene) + EM_Emi(:,10) * dt_chem  ! Farnesene
      ! Qemis(:,ind_BCARY)     =  Qemis(:,ind_BCARY)     + EM_Emi(:,11) * dt_chem  ! BCARY (Beta-Carophyllene)
      ! Qemis(:,ind_OSQ)       =  Qemis(:,ind_OSQ)       + EM_Emi(:,12) * dt_chem  ! Other sesquiterpenes
      ! Qemis(:,ind_MBO)       =  Qemis(:,ind_MBO)       + EM_Emi(:,13) * dt_chem  ! MBO (2methyl-3buten-2ol)
      ! Qemis(:,ind_CH3OH)     =  Qemis(:,ind_CH3OH)     + EM_Emi(:,14) * dt_chem  ! CH3OH (Methanol)
      ! Qemis(:,ind_CH3COCH3)  =  Qemis(:,ind_CH3COCH3)  + EM_Emi(:,15) * dt_chem  ! CH3COCH3 (Aceton)
      ! Qemis(:,ind_CH4)       =  Qemis(:,ind_CH4)       + EM_Emi(:,16) * dt_chem  ! CH4 (Methane)
      ! Qemis(:,ind_NO)        =  Qemis(:,ind_NO)        + EM_Emi(:,17) * dt_chem  ! NO
      ! Qemis(:,ind_CH3CHO)    =  Qemis(:,ind_CH3CHO)    + EM_Emi(:,18) * dt_chem  ! CH3CHO (Acetaldehyde)
      ! Qemis(:,ind_HCHO)      =  Qemis(:,ind_HCHO)      + EM_Emi(:,19) * dt_chem  ! HCHO (Formaldehyde)
      ! Qemis(:,ind_CO)        =  Qemis(:,ind_CO)        + EM_Emi(:,20) * dt_chem  ! CO
      ! Qemis(:,ind_Cineole)   =  Qemis(:,ind_Cineole)   + EM_Emi(:,21) * dt_chem  ! Cineole (Eucalyptol) (not included in the new megan code - used is the same values as Sabinene)
      ! Qemis(:,ind_Linalool)  =  Qemis(:,ind_Linalool)  + EM_Emi(:,22) * dt_chem  ! Linalool

      !***** Cumulative chemistry sources and sinks *****!
      Qchem = Qchem + CH_CONS_ALL - CH_CONS_temp  ! remember that here the chemistry also includes the emissions
      ! substract Qemis is wrong since it is cumulative quantity, not the current one
      if (Emiflag==0)then
        CH_CONS_ALL(2,ind_APINENE) = linear_interp(montime, nxodrad, CH_VOC_hyy(1,:), dt_obs) * air(2)*1.0d-9*0.51
        CH_CONS_ALL(2,ind_BPINENE) = linear_interp(montime, nxodrad, CH_VOC_hyy(1,:), dt_obs) * air(2)*1.0d-9*0.12
        CH_CONS_ALL(2,ind_LIMONENE) = linear_interp(montime, nxodrad, CH_VOC_hyy(1,:), dt_obs) * air(2)*1.0d-9*0.09
        CH_CONS_ALL(2,ind_CARENE) = linear_interp(montime, nxodrad, CH_VOC_hyy(1,:), dt_obs) * air(2)*1.0d-9*0.28
        CH_CONS_ALL(2,ind_C5H8) = linear_interp(montime, nxodrad, CH_VOC_hyy(2,:), dt_obs) * air(2)*1.0d-9
        ! CH_CONS_ALL(2,ind_MVK) = linear_interp(montime, nxodrad, CH_VOC_hyy(3,:), dt_obs) * air(2)*1.0d-9
        !        CH_CONS_ALL(2,ind_MEK) = linear_interp(montime, nxodrad, CH_VOC_hyy(4,:), dt_obs) * air(2)*1.0d-9
        CH_CONS_ALL(2,ind_CH3OH) = linear_interp(montime, nxodrad, CH_VOC_hyy(5,:), dt_obs) * air(2)*1.0d-9
        CH_CONS_ALL(2,ind_CH3CHO) = linear_interp(montime, nxodrad, CH_VOC_hyy(6,:), dt_obs) * air(2)*1.0d-9
        !        CH_CONS_ALL(2,ind_C2H5OH) = linear_interp(montime, nxodrad, CH_VOC_hyy(7,:), dt_obs) * air(2)*1.0d-9
        CH_CONS_ALL(2,ind_CH3COCH3) = linear_interp(montime, nxodrad, CH_VOC_hyy(8,:), dt_obs) * air(2)*1.0d-9
        CH_CONS_ALL(2,ind_CH3CO2H) = linear_interp(montime, nxodrad, CH_VOC_hyy(9,:), dt_obs) * air(2)*1.0d-9
        CH_CONS_ALL(2,ind_H2SO4) = linear_interp(montime, nxodrad, CH_H2SO4_hyy, dt_obs)
      endif
    ENDIF  ! IF (MOD( INT(daytime), INT(dt_chem) ) == 0)

    !*******************************************************************************************************************!
    !
    !    GAS DRY DEPOSITION PART
    !
    !*******************************************************************************************************************!
    IF (Gasdrydepflag == 1) THEN
      CALL debug_message('DEBUGING: calculating gas dry deposition')
      LAIl_sl(2:nz-1) = LAIl(2:nz-1)*psn(2:nz-1)
      LAIl_sh(2:nz-1) = LAIl(2:nz-1) - LAIl_sh(2:nz-1)

      qnud_dep = qnud
      qnud_dep(1:11)=qnud_dep(12)
      Tc_dep = ta1-273.15d0
      RH_dep = qnud_dep*8.314d0/0.0180153d0*ta1 / &
        (a0+a1*Tc_dep+a2*Tc_dep**2+a3*Tc_dep**3+a4*Tc_dep**4+a5*Tc_dep**5+a6*Tc_dep**6) / 100.0d0

      !===== Method from Ullar et al. (2012, ACP) =====!
      ! taub = EXP(-0.7*LAIl_c*0.83/zenit)
      ! taud = EXP(-0.7*LAIl_c*0.83)
      ! PARl_sl(2:nz-1) = rsnt+taud(2:nz-1)*rskt
      ! PARl_sh(2:nz-1) = taud(2:nz-1)*rskt
      ! PARl(2:nz-1) = PARl_sl(2:nz-1)*taub(2:nz-1) + PARl_sh(2:nz-1)*(1.0d0-taub(2:nz-1))

      !===== Only consider the top radiation =====!
      ! PARl(2:nz-1) = rsnt*phsn + rskt*phsk
      ! PARl(2:nz-1) = rsnt*0.42+ rskt*0.60

      !===== Consider the PAR attenuation =====!
      !!!!! Consider the upward scattered PAR
      PARl(2:nz-1) = (rsnt*gl(2:nz-1)/zenit*phsn + rskt*psk(2:nz-1)*gd(2:nz-1)/zenit*phsk + fphd(2:nz-1) + fphu(2:nz-1))*psn(2:nz-1) &
        + (rskt*psk(2:nz-1)*gd(2:nz-1)/zenit*phsk + fphd(2:nz-1) + fphu(2:nz-1))*(1.0d0-psn(2:nz-1))
      !!!!! Do not consider the upward scattered PAR
      ! PARl(2:nz-1) = (rsnt*gl(2:nz-1)/zenit*phsn + rskt*psk(2:nz-1)*gd(2:nz-1)/zenit*phsk + fphd(2:nz-1) )*psn(2:nz-1) &
      !                + (rskt*psk(2:nz-1)*gd(2:nz-1)/zenit*phsk + fphd(2:nz-1))*(1.0d0-psn(2:nz-1))
      ! PARl(nz-1) = PARl(nz-2)
      ! PARl(nz-2) = PARl(nz-2) - 30.0d0
      ! PARl(2:nz-1) = PARl(2:nz-1) * (1.0d0-tphu-rphu)

      !!!!! Consider the scattered radiation from below
      PARl_sl(2:nz-1) = rsnt*gl(2:nz-1)/zenit*phsn + rskt*psk(2:nz-1)*gd(2:nz-1)/zenit*phsk + fphd(2:nz-1) + fphu(2:nz-1)
      PARl_sh(2:nz-1) = rskt*psk(2:nz-1)*gd(2:nz-1)/zenit*phsk + fphd(2:nz-1) + fphu(2:nz-1)
      !!!!! Do not consider the scattered radiation from below
      ! PARl_sl(2:nz-1) = rsnt*gl(2:nz-1)/zenit*phsn + rskt*psk(2:nz-1)*gd(2:nz-1)/zenit*phsk + fphu(2:nz-1)
      ! PARl_sh(2:nz-1) = rskt*psk(2:nz-1)*gd(2:nz-1)/zenit*phsk + fphu(2:nz-1)
      ! write(*,*) 'PARl:', PARl(1:nz)
      u_veg(2:nz-1) = sqrt(u1(2:nz-1)**2 + v1(2:nz-1)**2)
      rho_veg(2:nz-1) = roa
      Tc_veg(2:nz-1) = tsn1(2:nz-1)*psn(2:nz-1) + tsd1(2:nz-1)*(1.0d0-psn(2:nz-1)) ! Tc_veg is the averaged leaf temperature
      ! Tc_veg(2:nz-1) = ta1(2:nz-1)  ! Tc_veg = air T
      DO k=2,nz-1
        ! Henry(:,k) = HenryA * EXP(HenryB*(1.0d0/298.15d0 - 1.0d0/Tc_veg(k)))
        Henry(:,k) = HenryA(:)
      END DO

      CALL get_gas_dry_deposition_velocity(daytime, &
        nz-2, NSPEC, &  ! it starts from level 2 (level 1 is the ground, level nz is just over the canopy)
        l_drydep, l_vpd, l_wetskin, &
        frac_veg(2:nz-1), frac_ws(2:nz-1), &
        z(2:nz-1), hc, LAIl(2:nz-1), LAIl_sl(2:nz-1), LAIl_sh(2:nz-1), &
        PARl(2:nz-1), PARl_sl(2:nz-1), PARl_sh(2:nz-1), psn(2:nz-1), &
        u_veg(2:nz-1), ur(2:nz-1), rho_veg(2:nz-1), pres(2:nz-1), Tc_veg(2:nz-1), RH_dep(2:nz-1), &
        wg1(1), wgmax, wgwilt, &
        stomblock, &
        dvsn(2:nz-1), dvsd(2:nz-1), psn(2:nz-1)*dvsn(2:nz-1)+(1.0d0-psn(2:nz-1))*dvsd(2:nz-1), &
        gstm_h2o_sn(2:nz-1), gstm_h2o_sd(2:nz-1), psn(2:nz-1)*gstm_h2o_sn(2:nz-1)+(1.0d0-psn(2:nz-1))*gstm_h2o_sd(2:nz-1), &
        SPC_NAMES, molar_mass, SUM(Henry(:, 2:nz-1), DIM=2)/(nz-2.0d0), dryreac, ind_O3, ind_SO2, &  ! average of Henry at all levels
        vdep(2:nz-1, :), dep_output(2:nz-1,:,:))
      DO k=2, nz-1
        gasdep_flux(k,:) = -vdep(k,:)*CH_CONS_ALL(k,:)/( 0.5d0*(dz(k)+dz(k+1)) )
      END DO

      ! Update concentrations
      CH_CONS_ALL = CH_CONS_ALL + dt_mete*gasdep_flux

      !***** Cumulative deposition flux *****!
      Qdepo = Qdepo + dt_mete*gasdep_flux
    END IF  ! IF (Gasdrydepflag == 1) THEN


    CH_CONS_temp = CH_CONS_ALL

    CALL debug_message('DEBUGGING: mixing O3')
    !===== Get the input O3 concentration from input file when the chemistry is off =====!
    ! IF (CHEMflag == 1) THEN
    IF (.FALSE.) THEN
      o3_new = linear_interp(montime, nxodrad, CH_gas_hyy(2,:), dt_obs) * air(nz)*1.0d-9

      DO k=2,kz-1
        terma(k) = (1.0d0-o3_weight(k)) * dt_mete*(2.0d0*Kht(k)-w(k)*dz(k))/da(k)
        termc(k) = (1.0d0-o3_weight(k)) * dt_mete*(2.0d0*Khb(k)+w(k)*dz(k+1))/dc(k)
        termb(k) = (1.0d0-o3_weight(k)) * dt_mete*(2.0d0*(dz(k)*Kht(k)+ dz(k+1)*Khb(k))/(dz(k+1)+dz(k)) + &
          w(k)*(dz(k+1)-dz(k)))/db(k) + 1.0d0
      ENDDO

      DO k = 1,kz
        ! CH_CONS_VER(k) = CH_CONS_ALL(k,ind_O3) + (1.0d0-o3_weight(k))*dt_mete*gasdep_flux(k,ind_O3) &
        !                  + o3_weight(k)*(o3_new-CH_CONS_ALL(k,ind_O3))
        CH_CONS_VER(k) = CH_CONS_ALL(k,ind_O3) + o3_weight(k)*(o3_new-CH_CONS_ALL(k,ind_O3))
      ENDDO

      !!!!! calculate once for all layers !!!!!
      CALL gtri(terma, termc, termb, CH_CONS_VER, CH_CONS_VER_NEW, 2, 0.d0, 3.d0, 2, 0.d0, 3.d0, kz, 2)

      DO k=2,kz-1
        !!!!! kt: [m2 s-1], 100*kt: [cm m s-1], the flux unit is then [molec cm-2 s-1], upward is positive
        CH_CONS_FLUX(k,ind_O3) = -0.5*100.0d0*( Kht(k)*(CH_CONS_VER_NEW(k+1) - CH_CONS_VER_NEW(k))/dz(k+1) +  &
          Khb(k)*(CH_CONS_VER_NEW(k) - CH_CONS_VER_NEW(k-1))/dz(k) )
      ENDDO

      DO k=1,kz
        CH_CONS_ALL(k,ind_O3) = MAX(0.0d0, CH_CONS_VER_NEW(k))
      ENDDO
    END IF  ! mixing of ozone

    !*****************************************************************************************************************!
    !
    !    Mixing of chemicals
    !
    !*****************************************************************************************************************!
    CALL debug_message('DEBUGING: mixing compounds')

    IF (CHEMflag == 1) THEN
      !----------------------------------------------------------------
      !  solution for solving the vertical transport of the gas species
      !----------------------------------------------------------------
      DO k=2,kz-1
        terma(k) = dt_mete*(2.0d0*Kht(k)-w(k)*dz(k))/da(k)
        termc(k) = dt_mete*(2.0d0*Khb(k)+w(k)*dz(k+1))/dc(k)
        termb(k) = dt_mete*(2.0d0*(dz(k)*Kht(k) + dz(k+1)*Khb(k)) /(dz(k+1)+dz(k)) +w(k)*(dz(k+1)-dz(k)))/db(k)+ 1.0d0
      ENDDO

      DO J = 1, NSPEC
        IF (J==ind_O3) CYCLE  ! do not mix O3 since it is alreaday calculated above
        IF (CH_oh_flag(J)) CYCLE  ! do not mix rOH-chemicals, go back to start next do loop cycle right away
        IF (CH_o3_flag(J)) CYCLE  ! do not mix rO3-chemicals, go back to start next do loop cycle right away
        IF (CH_no3_flag(J)) CYCLE  ! do not mix rNO3-chemicals, go back to start next do loop cycle right away

        CH_CONS_VER = CH_CONS_ALL(:,J)  ! + dt_mete*gasdep_flux(:,J)

        CALL gtri(terma, termc, termb, CH_CONS_VER, CH_CONS_VER_NEW, 2, 0.0d0, 3.0d0, 2, 0.0d0, 3.0d0, kz, 2)

        DO k=2,kz-1
          !!!!! kt: [m2 s-1], 100*kt: [cm m s-1], so the unit of flux is [molec cm-2 s-1], upward is positive
          CH_CONS_FLUX(k,j) = -0.5d0*100.0d0*( Kht(k)*(CH_CONS_VER_NEW(k+1) - CH_CONS_VER_NEW(k))/dz(k+1) +  &
            Khb(k)*(CH_CONS_VER_NEW(k) - CH_CONS_VER_NEW(k-1))/dz(k) )
        ENDDO

        DO k=1,kz
          CH_CONS_ALL(k,J) = MAX(CH_CONS_VER_NEW(k), 0.0d0)
        ENDDO
      ENDDO
    ENDIF  ! CHEMflag == 1

    Qturb = Qturb + CH_CONS_ALL - CH_CONS_temp
    Qturb_now = CH_CONS_ALL - CH_CONS_temp  ! concentration change caused by turbulent transport at current time point.


    !***** Calculate the concentration change *****!
    ! CH_CONS_1 = CH_CONS_ALL
    ! Qconc = CH_CONS_1 - CH_CONS_0
    ! Qfluxdep = CH_CONS_1 - CH_CONS_temp
    ! write(*,*) 'Qconc(APINENE)', Qconc(1:20, ind_APINENE)

    !*******************************************************************************************************************!
    !
    !    AEROSOL PART
    !
    !*******************************************************************************************************************!

    CALL debug_message('DEBUGING: calculating aerosol processes')
    ! Calculation of nucleation rates by limononic acid and sulfuric acid

    IF (AEROFLAG == 1) THEN
      IF (MOD( INT(daytime), INT(dt_aero) ) == 0) THEN

        ! In the beginning of everyday or the first time loop, open a new dmps file
        ! (since they are saved as one file per day) to read the data.
        IF (first_time_loop .or. is_newday) THEN
          dmps_file_name = TRIM(ADJUSTL(input_dir_station)) &
            //'/dmps/'// year_str//'/dm'//year_str2//''//month_str//''//day_str//'.sum'
          ! write(*,*) '[Putian Debug] Reading new dmps file.', dmps_file_name
          line = 1
          options%month = mon

          ! try to find the number of sizebins (ccount) in the measurement
          open(12345, file = TRIM(ADJUSTL(dmps_file_name)))

          ! read the first line to dummy
          read(12345, '(A)') dummy_line
          close(12345)

          ! read values from dummy
          read(dummy_line, *, END = 8888) (val(ii), ii=1, maxcol)
          8888    continue

          ! Calculate number of sizebin from measurement ccount
          ii=3 ! since the first two numbers are zero.
          ccount = 0
          do while (val(ii)>0)
            ccount = ccount +1
            ii = ii +1
          end do
        END IF

        ! dmps data file time interval is 10 minutes
        IF (montime/600.0 == FLOOR(montime/(600.0))) THEN
          line=line+1
        ENDIF

        IF ( (now_date(4) < 1) .AND. (montime/600.0 == FLOOR(montime/600.0)) ) then

          call use_dmps(TRIM(ADJUSTL(dmps_file_name)), &
            ccount, line,particles,ambient%density,initial_dist%mfractions)

          ! The input particle concentration are made as 0.1
          ! of the measurements for layers above mixing height.
          abl_layer = 1
          do while (z(abl_layer) < hlayer)
            abl_layer = abl_layer+1
          end do

          Do k = 1, abl_layer
            N_CONC(k,:) = particles%n_conc
            Radius(k,:) = particles%radius
            Rdry(k,:)   = particles%rdry
            Core(k,:)   = particles%core
            Mass(k,:)   = particles%mass
            Vol_conc(k,:,:) = particles%vol_conc
          END DO

          DO k = abl_layer+1, kz
            N_CONC(k,:) = particles%n_conc * 0.1d0
            Radius(k,:) = particles%radius
            Rdry(k,:)   = particles%rdry
            Core(k,:)   = particles%core
            Mass(k,:)   = particles%mass * 0.1d0
            Vol_conc(k,:,:) = particles%vol_conc * 0.1d0
          END DO
        ENDIF

        ! call equilibrium_size(particles, ambient, options)

        time_aer = daytime
        end_aer = daytime + dt_aero

        if (use_parallel_aerosol) then
          call start_timer(timer_total_aero)
          mpi_sendcount = 0
          mpi_recvcount = 0

          ! Condensation sink name in chemistry is CH_CS, while in aerosol it's called ambient%sink(1)
          ! ambient%sink(1)=CH_CS
          ! do something

          DO k = 1, kz  ! loop over kz
            ! Assign environmental data
            call assign_environmental_data(ambient, ta1(k), rh(k), pres(k),NH3_hyy_ALL(k))

            ! Set temporary CH_CONS to the values in a specific layer
            ! CH_CONS = CH_CONS_ALL(k, :)

            IF (mpi_sendcount >= mpi_nslaves) THEN

              ! Start of timer of root receiving aerosol data from a slave
              call start_timer(root_recv_aero(k))

              ! receive data from a slave
              CALL MPI_RECV(mpi_recv_aer_buffer, mpi_recv_aer_buffer_size, &
                MPI_DOUBLE_PRECISION, MPI_ANY_SOURCE, &
                MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS, &
                mpi_rc)
              ! write(*,*) 'aerosol root pure recived time: ', real(sc_c1 - sc_c0)/real(sc_cr)

              mpi_source_id        = mpi_status(MPI_SOURCE)

              kz_tag               = INT(mpi_recv_aer_buffer(index_kz_tag))
              first_run(kz_tag)    = mpi_recv_aer_buffer(index_fcall_recv)
              first_run(kz_tag)    = mpi_recv_aer_buffer(index_fcall_recv)
              mpi_source_id        = mpi_status(MPI_SOURCE)

              kz_tag               = INT(mpi_recv_aer_buffer(index_kz_tag))
              first_run(kz_tag)    = mpi_recv_aer_buffer(index_fcall_recv)
              NUC_RATE(kz_tag)     = mpi_recv_aer_buffer(index_nuc_rate_recv)

              ION_NUC_RATE(kz_tag) = mpi_recv_aer_buffer(index_ion_nuc_rate_recv)
              N_CONC(kz_tag,:)     = mpi_recv_aer_buffer(index_ion_nuc_rate_recv+1:index_n_conc_recv)
              RADIUS(kz_tag,:)     = mpi_recv_aer_buffer(index_n_conc_recv+1:index_radius_recv)
              RDRY(kz_tag,:)       = mpi_recv_aer_buffer(index_radius_recv+1:index_rdry_recv)
              CORE(kz_tag,:)       = mpi_recv_aer_buffer(index_rdry_recv+1:index_core_recv)
              MASS(kz_tag,:)       = mpi_recv_aer_buffer(index_core_recv+1:index_mass_recv)
              GR(kz_tag,:)         = mpi_recv_aer_buffer(index_mass_recv+1:index_gr_recv)

              do cp = 1, uhma_compo
                do csec = 1, uhma_sections
                  VOL_CONC(kz_tag,csec, cp) = mpi_recv_aer_buffer(index_gr_recv + csec + (cp-1)*uhma_sections)
                end do
              end do

              ! Can somebody check this?
              ! SINK(kz_tag,:) = mpi_recv_aer_buffer(index_vap_conc_recv+1:index_sink_recv)/1E6

              VAPOR(kz_tag,:)= mpi_recv_aer_buffer(index_vol_conc_recv+1:index_vap_conc_recv)
              SINK(kz_tag,:) = mpi_recv_aer_buffer(index_vap_conc_recv+1:index_sink_recv)
              CH_CONS        = mpi_recv_aer_buffer(index1_CONS_aer_recv:index2_CONS_aer_recv)
              CH_CONS_ALL(kz_tag, :) = CH_CONS

              ! End of timer of root receiving aerosol data from a slave
              call end_timer(root_recv_aero(k))

              ! This line seems not used and unnecessary
              ambient%sink=sink(kz_tag,:)

              mpi_recvcount = mpi_recvcount + 1
              mpi_dest_id = mpi_source_id
            ELSE
              mpi_dest_id = mpi_sendcount + 1
            END IF

            ! Start of timer root_send_aero
            call start_timer(root_send_aero(k))

            ! send message "chemicals databuffer coming" to slave
            CALL MPI_SEND(mpi_do_aerosol_code, 1, MPI_INTEGER, &
              mpi_dest_id, mpi_task_code_tag, &
              MPI_COMM_WORLD, &
              mpi_rc)

            mpi_send_aer_buffer(index_kz_tag) = REAL(k, KIND=dp)
            mpi_send_aer_buffer(index_time1)  = TIME_aer
            mpi_send_aer_buffer(index_time2)  = END_aer
            mpi_send_aer_buffer(index_aer_ts) = dt_uhma
            mpi_send_aer_buffer(index_rh)     = ambient%rh
            mpi_send_aer_buffer(index_ta1)    = ambient%temp
            mpi_send_aer_buffer(index_pr1)    = ambient%pres
            mpi_send_aer_buffer(index_bh)     = ambient%boundaryheight
            mpi_send_aer_buffer(index_month)  = options%month
            mpi_send_aer_buffer(index_fcall)  = real(first_call)
            ! mpi_send_aer_buffer(index_sink_recv) = ambient%sink(ambient%sulfuric_acid_index)
            mpi_send_aer_buffer(index_nuc_rate) = nuc_rate(k)

            mpi_send_aer_buffer(index_ion_nuc_rate+1:index_n_conc) = N_CONC(k, :)
            mpi_send_aer_buffer(index_n_conc+1:index_radius)       = RADIUS(k, :)
            mpi_send_aer_buffer(index_radius+1:index_rdry)         = RDRY(k, :)
            mpi_send_aer_buffer(index_rdry+1:index_core)           = CORE(k, :)
            mpi_send_aer_buffer(index_core+1:index_mass)           = MASS(k, :)
            mpi_send_aer_buffer(index_mass+1:index_gr)             = GR(k, :)

            do cp = 1, uhma_compo
              do csec = 1, uhma_sections
                mpi_send_aer_buffer(index_gr+csec+(cp-1)*uhma_sections) = vol_conc(k,csec,cp)
              end do
            end do

            mpi_send_aer_buffer(index_vol_conc+1:index_vap_conc) = VAPOR(k, :)
            mpi_send_aer_buffer(index_day)     = julian !not real necessary
            mpi_send_aer_buffer(index_nh3_cm3) = ambient%nh3_cm3
            CH_CONS = CH_CONS_ALL(k, :)
            mpi_send_aer_buffer(index1_CONS_aer_send:index2_CONS_aer_send) = CH_CONS

            CALL MPI_SEND(mpi_send_aer_buffer, mpi_send_aer_buffer_size, &
              MPI_DOUBLE_PRECISION, &
              mpi_dest_id, mpi_buffer_tag, &
              MPI_COMM_WORLD, mpi_rc)

            ! End of timer root_send_aero
            call end_timer(root_send_aero(k))

            mpi_sendcount = mpi_sendcount + 1
          ENDDO  ! loop over kz

          ! do k = 1, kz
          !   write(*,*) '[Putian Debug] root_recv_aero time: ', &
          !     k, root_recv_aero(k)%used, root_send_aero(k)%used, &
          !     root_recv_aero(k)%last, root_send_aero(k)%last
          ! end do

          ! reveive data from the rest of the slaves
          DO slave_i = 1, mpi_nslaves
            ! receive data from a slave
            CALL MPI_RECV(mpi_recv_aer_buffer,mpi_recv_aer_buffer_size, &
              MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,MPI_ANY_TAG, &
              MPI_COMM_WORLD, MPI_STATUS,mpi_rc)

            kz_tag               = INT(mpi_recv_aer_buffer(index_kz_tag))
            mpi_source_id        = mpi_status(MPI_SOURCE)
            first_run(kz_tag)    = mpi_recv_aer_buffer(index_fcall_recv)
            NUC_RATE(kz_tag)     = mpi_recv_aer_buffer(index_nuc_rate_recv)

            ION_NUC_RATE(kz_tag) = mpi_recv_aer_buffer(index_ion_nuc_rate_recv)
            N_CONC(kz_tag,:)     = mpi_recv_aer_buffer(index_ion_nuc_rate_recv+1:index_n_conc_recv)
            RADIUS(kz_tag,:)     = mpi_recv_aer_buffer(index_n_conc_recv+1:index_radius_recv)
            RDRY(kz_tag,:)       = mpi_recv_aer_buffer(index_radius_recv+1:index_rdry_recv)
            CORE(kz_tag,:)       = mpi_recv_aer_buffer(index_rdry_recv+1:index_core_recv)
            MASS(kz_tag,:)       = mpi_recv_aer_buffer(index_core_recv+1:index_mass_recv)
            GR(kz_tag,:)         = mpi_recv_aer_buffer(index_mass_recv+1:index_gr_recv)
            VAPOR(kz_tag,:)      = mpi_recv_aer_buffer(index_vol_conc_recv+1:index_vap_conc_recv)
            SINK(kz_tag,:)       = mpi_recv_aer_buffer(index_vap_conc_recv+1:index_sink_recv)

            do cp = 1, uhma_compo
              do csec = 1, uhma_sections
                VOL_CONC(kz_tag,csec, cp) = mpi_recv_aer_buffer(index_gr_recv + csec + (cp-1)*uhma_sections)
              end do
            end do

            CH_CONS = mpi_recv_aer_buffer(index1_CONS_aer_recv:index2_CONS_aer_recv)
            CH_CONS_ALL(kz_tag, :) = CH_CONS
          END DO  ! slave_i = 1, mpi_nslaves
          call end_timer(timer_total_aero)
          ! write(*,*) '[Putian Debug] total used aerosol computation time ', timer_total_aero%last
        else  ! not use_parallel_aerosol
          do k = 1, kz  ! loop over kz
            call system_clock(sc_c0, sc_cr)
            ! Assign environmental data for level k
            call assign_environmental_data(ambient, ta1(k), rh(k), pres(k),NH3_hyy_ALL(k))
            ambient%vap_conc = VAPOR(k, :)

            ! Assign particles data for level k
            particles%nuc_rate = NUC_RATE(k)
            particles%n_conc   = N_CONC(k, :)
            particles%radius   = RADIUS(k, :)
            particles%rdry     = RDRY(k, :)
            particles%core     = CORE(k, :)
            particles%mass     = MASS(k, :)
            particles%gr       = GR(k, :)
            particles%vol_conc = VOL_CONC(k, :, :)

            call system_clock(sc_c1, sc_cr)
            ! write(*,*) 'aerosol set input: ', real(sc_c1-sc_c0)/real(sc_cr)

            call system_clock(sc_c0, sc_cr)

            call run_aerosol(particles, CH_CONS_ALL(kz, 1:NSPEC), ambient, options, time_aer, dt_aero, dt_uhma)

            call system_clock(sc_c1, sc_cr)
            ! write(*,*) 'aerosol computation: ', real(sc_c1-sc_c0)/real(sc_cr)

            call system_clock(sc_c0, sc_cr)

            ! Set new particles data back to layer k
            NUC_RATE(k)  = particles%nuc_rate
            N_CONC(k, :) = particles%n_conc
            RADIUS(k, :) = particles%radius
            RDRY(k, :)   = particles%rdry
            CORE(k, :)   = particles%core
            MASS(k, :)   = particles%mass
            GR(k, :)     = particles%gr
            VOL_CONC(k, :, :) = particles%vol_conc

            VAPOR(k, :) = ambient%vap_conc
            SINK(k, :) = ambient%sink

            call system_clock(sc_c1, sc_cr)
            ! write(*,*) 'aerosol set output: ', real(sc_c1-sc_c0)/real(sc_cr)
          end do  ! k = 1, kz for not use_parallel_aerosol
        end if  ! if (use_parallel_aerosol)
      ENDIF  ! IF (MOD( INT(daytime), INT(dt_aero) ) == 0) THEN

      ! Calculate aerosol deposition velocity
      DO k = 1, kz
        CALL DRY_DEPLAYER(k,ta1(k),pres(k),SQRT(u1(k)**2+v1(k)**2),ur(k),s1(k),face,Rk_vel(k,:),RADIUS(k,:))
      END DO

      !------------------------------------------------------------------------
      !solution for solving the vertical transport of particles
      !-------------------------------------------------------------------------
      CALL SCPARFLUX(kz,z,dz,dt_mete,alt,w1,kt,N_CONC,PAR_FLUX,VOL_CONC,Rk_vel,ta1,pres)
    END IF


    !===== Update time =====!
    CALL debug_message('DEBUGING: updating time')

    ! Update nsytki, maybe nsytki is not needed any more, need to check in future
    nsytki=2
    if (daytime > SECONDS_IN_ONE_DAY) nsytki=nsytki+1

    ! Update daytime
    daytime=daytime+dt_mete

    ! Update month and day
    IF (daytime >= SECONDS_IN_ONE_DAY) THEN  ! move to next day
      is_newday = .true.
      julian = julian + 1
      now_date(3) = now_date(3) + 1
      IF ( now_date(3) > month_length(now_date(2)) ) THEN  ! move to next month
        is_newmonth = .true.
        now_date(2) = now_date(2) + 1
        now_date(3) = 1
      ELSE
        is_newmonth = .false.
      END IF
      daytime = MOD(daytime, SECONDS_IN_ONE_DAY)
    ELSE
      is_newday = .false.
    END IF

    ! Update hour, minute and second
    now_date(4) = INT(daytime/3600.0d0)
    now_date(5) = INT((daytime-now_date(4)*3600.d0)/60.0d0)
    now_date(6) = INT(daytime-now_date(4)*3600.0d0-now_date(5)*60.0d0)

    ! Update montime
    montime=montime+dt_mete

    ! Write the output data
    IF (MOD(INT(daytime),1800) == 0) THEN
      CALL debug_message('DEBUGING: writing output data')
      CALL output_write()
      !***** Reset the cumulative quantities *****!
      Qemis = 0.0d0
      Qchem = 0.0d0
      Qdepo = 0.0d0
      Qturb = 0.0d0
    END IF

    ! Update time string
    ! Now the year time is not updated, need to be added in future
    WRITE(year_str,'(I4)') now_date(1)  ! '1999', '2000', '2001', ...
    WRITE(year_str2, '(I2.2)') MOD(now_date(1), 100)  ! '99', '00', '01', ...
    WRITE(month_str_Mxx, '(A1, I2.2)') 'M', now_date(2)  ! 'M01', 'M02', ..., 'M12'
    WRITE(month_str, '(I2.2)') now_date(2)  ! '01', '02', ..., '12'
    WRITE(day_str, '(I2.2)') now_date(3)  ! '01', '02', ..., '31'

    first_time_loop = .false.
  END DO  ! DO WHILE (time_end > montime)

  nmetro = 1
  nmx = 1
  !***************** Write the data for final time step, here the same as the second last actually ***************!
  IF (AEROFLAG .EQ. 1) THEN
    !!!!!   IF (MOD(INT(daytime),600) .EQ. 0) CALL WRITE_AEROSOL_OUTPUT
  END IF
  IF (MOD(INT(daytime), 1800) == 0) CALL output_write()

  call output_done()

  write(*,*)'balans  =',balans

  666 CONTINUE  ! (almost) the end of the program

  !#ifdef PARALLEL
  IF (my_id == master_id) THEN
    ! send message to slaves to stop, so the whole program ends cleanly
    DO slave_i = 1,mpi_nslaves
      CALL MPI_SEND(mpi_end_task_code,1,MPI_INTEGER,slave_i,mpi_task_code_tag,MPI_COMM_WORLD,mpi_rc)
    ENDDO
  END IF

  CALL MPI_Finalize(mpi_rc)
  !#endif


  ! **************************************** END OF PROGRAM ****************************************
CONTAINS

  !#ifdef PARALLEL
  SUBROUTINE slave_task
    ! This is where all other branches, except the master branch, do their work
    !
    ! And their work is: waiting for packets containing input for CHEMISTRY
    ! calling CHEMISTRY, and sending results back
    IMPLICIT NONE
    INTEGER :: task_code
    REAL(kind=dp) :: time1,time2, CH_H2O, CH_RO2, day_of_year, dt_uhma
    ! RO2 here will hide the visibility of the global variable RO2(kz)
    REAL(kind=dp) :: tmcontr_local
    REAL(kind=dp) :: ta1_local, pres1_local, Beta_local, sun_par_local
    ! variables local to this subroutine, to hold the received values from the corresponding variables from the main program
    INTEGER :: year_int, month_int
    REAL(kind=dp) :: qa1

    REAL(kind=dp), DIMENSION(NPHOT)    :: J_values_local
    REAL(kind=dp), DIMENSION(NKVALUES) :: K_values_local
    REAL(kind=dp), DIMENSION(75)       :: ACF_local


    CALL KPP_SetUp()
    DO
      !! MPI_RECV(BUF, COUNT, DATATYPE, SOURCE, TAG, COMM, STATUS, IERROR)
      CALL MPI_RECV(task_code,1,MPI_INTEGER,master_id,mpi_task_code_tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE,mpi_rc)

      IF (task_code == mpi_do_chemistry_code) THEN

        CALL MPI_RECV(mpi_send_buffer,mpi_send_buffer_size,MPI_DOUBLE_PRECISION,master_id,mpi_buffer_tag,MPI_COMM_WORLD, &
          MPI_STATUS_IGNORE,mpi_rc)

        kz_tag_dp     = mpi_send_buffer(index_kz_tag)
        time1         = mpi_send_buffer(index_time1)
        time2         = mpi_send_buffer(index_time2)
        tmcontr_local = mpi_send_buffer(index_tmcontr)
        nxodrad = INT(mpi_send_buffer(index_nxodrad))
        year_int = INT(mpi_send_buffer(index_year))
        month_int = INT(mpi_send_buffer(index_month))
        ta1_local = mpi_send_buffer(index_ta1)
        pres1_local = mpi_send_buffer(index_pr1)
        Beta_local = mpi_send_buffer(index_beta)
        sun_par_local = mpi_send_buffer(index_sun_par)
        CH_RES1 = mpi_send_buffer(index_res1)
        CH_RES2 = mpi_send_buffer(index_res2)
        qa1 = mpi_send_buffer(index_qa1)
        CH_H2O = mpi_send_buffer(index_H2O)
        CH_CONS = mpi_send_buffer(index1_CONS:index2_CONS)
        CH_ALBEDO = mpi_send_buffer(index_ALBEDO)
        GLO = mpi_send_buffer(index_glo)

        ! [Putian Debug] chemistry computation time
        CALL CHEMISTRY(CH_CONS, time1, time2, ta1_local+0.0d0, &
          pres1_local*1.0d-2, Beta_local, sun_par_local, CH_RES1, CH_RES2, qa1, CH_H2O, CH_RO2, &
          J_values_local, K_values_local,GLO, INPUT_DIR, STATION, CH_ALBEDO, ACF_local)


        mpi_recv_buffer(index_kz_tag) = kz_tag_dp
        mpi_recv_buffer(index_RO2) = CH_RO2
        mpi_recv_buffer(index_H2O_recv) = CH_H2O
        mpi_recv_buffer(index1_CONS_recv:index2_CONS_recv) = CH_CONS
        mpi_recv_buffer(index1_J_values_recv:index2_J_values_recv) = J_values_local
        mpi_recv_buffer(index1_K_values_recv:index2_K_values_recv) = K_values_local
        mpi_recv_buffer(index1_ACF_recv:index2_ACF_recv) = ACF_local

        CALL MPI_SEND(mpi_recv_buffer,mpi_recv_buffer_size,MPI_DOUBLE_PRECISION,master_id,mpi_buffer_tag, &
          MPI_COMM_WORLD,mpi_rc)

      ELSE IF (task_code == mpi_do_aerosol_code) THEN

        CALL MPI_RECV(mpi_send_aer_buffer,mpi_send_aer_buffer_size,MPI_DOUBLE_PRECISION,master_id,mpi_buffer_tag, &
          MPI_COMM_WORLD, MPI_STATUS_IGNORE,mpi_rc)

        kz_tag_dp = mpi_send_aer_buffer(index_kz_tag)
        time1     = mpi_send_aer_buffer(index_time1)
        time2     = mpi_send_aer_buffer(index_time2)
        dt_uhma = mpi_send_aer_buffer(index_aer_ts)
        ambient%rh = mpi_send_aer_buffer(index_rh)
        ambient%temp = mpi_send_aer_buffer(index_ta1)
        ambient%pres = mpi_send_aer_buffer(index_pr1)
        ambient%boundaryheight = mpi_send_aer_buffer(index_bh)
        options%month = mpi_send_aer_buffer(index_month)
        first_call = int(mpi_send_aer_buffer(index_fcall))
        ! ambient%sink(ambient%sulfuric_acid_index) = mpi_send_aer_buffer(index_sink_recv)
        particles%nuc_rate = mpi_send_aer_buffer(index_nuc_rate)

        particles%n_conc =  mpi_send_aer_buffer(index_ion_nuc_rate+1:index_n_conc)
        particles%radius = mpi_send_aer_buffer(index_n_conc+1:index_radius)
        particles%rdry = mpi_send_aer_buffer(index_radius+1:index_rdry)
        particles%core = mpi_send_aer_buffer(index_rdry+1:index_core)
        particles%mass =  mpi_send_aer_buffer(index_core+1:index_mass)
        particles%gr = mpi_send_aer_buffer(index_mass+1:index_gr)
        CH_CONS = mpi_send_aer_buffer(index1_CONS_aer_send:index2_CONS_aer_send)

        do cp = 1, uhma_compo
          do csec = 1, uhma_sections
            particles%vol_conc(csec, cp) = mpi_send_aer_buffer(index_gr + csec + (cp-1)*uhma_sections)
          end do
        end do
        ambient%vap_conc = mpi_send_aer_buffer(index_vol_conc+1:index_vap_conc)
        day_of_year = mpi_send_aer_buffer(index_day)

        ambient%nh3_cm3 = mpi_send_aer_buffer(index_nh3_cm3)


        IF (DebugFlag==1) THEN
          write(*,*) '[Putian Debug] other ids'
          write(*,*) '[Putian Debug] kz_tag_dp', kz_tag_dp
          write(*,*) '[Putian Debug] CH_CONS(ind_APINENE)', CH_CONS(ind_APINENE)
        ENDIF

        ! [Putian Debug] aerosol computation time
        call system_clock(sc_c0, sc_cr)
        ! do while (time1 < time2)
        ! call solve_gde(time1,dt_uhma,particles,ambient,options)    ! Solves the Grand Dynamic Equation
        call run_aerosol(particles, CH_CONS(1:NSPEC), ambient, options, time1, dt_aero, dt_uhma)
        ! time1=time1 + dt_uhma
        ! enddo
        call system_clock(sc_c1, sc_cr)
        ! write(*,*) 'aerosol computation: ', nint(kz_tag_dp), real(sc_c1-sc_c0)/real(sc_cr)

        mpi_recv_aer_buffer(index_kz_tag) = kz_tag_dp
        mpi_recv_aer_buffer(index_fcall_recv) = real(first_call)
        mpi_recv_aer_buffer(index_nuc_rate_recv) = particles%nuc_rate

        ! mpi_recv_aer_buffer(index_ion_nuc_rate_recv) = particles%ion_nuc_rate
        mpi_recv_aer_buffer(index_ion_nuc_rate_recv+1:index_n_conc_recv) = particles%n_conc
        mpi_recv_aer_buffer(index_n_conc_recv+1:index_radius_recv) = particles%radius
        mpi_recv_aer_buffer(index_radius_recv+1:index_rdry_recv) = particles%rdry
        mpi_recv_aer_buffer(index_rdry_recv+1:index_core_recv) = particles%core
        mpi_recv_aer_buffer(index_core_recv+1:index_mass_recv) = particles%mass
        mpi_recv_aer_buffer(index_mass_recv+1:index_gr_recv) = particles%gr

        do cp = 1, uhma_compo
          do csec = 1, uhma_sections
            mpi_recv_aer_buffer(index_gr_recv+csec+(cp-1)*uhma_sections) = particles%vol_conc(csec,cp)
          end do
        end do

        mpi_recv_aer_buffer(index_vol_conc_recv+1:index_vap_conc_recv) = ambient%vap_conc
        mpi_recv_aer_buffer(index_vap_conc_recv+1:index_sink_recv) = ambient%sink
        mpi_recv_aer_buffer(index1_CONS_aer_recv:index2_CONS_aer_recv) = CH_CONS

        CALL MPI_SEND(mpi_recv_aer_buffer,mpi_recv_aer_buffer_size,MPI_DOUBLE_PRECISION,master_id,mpi_buffer_tag, &
          MPI_COMM_WORLD,mpi_rc)

      ELSE  ! task_code must be mpi_end_task_code

        EXIT

      END IF
    END DO
  END SUBROUTINE slave_task

  !#endif


  !oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
  !
  !  Calculates the frequency of collision (1/cm3/s) from h2so4 and limononic acid based on Seinfeld page 152
  !
  !oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

  FUNCTION COLLISION(Temp)
    IMPLICIT NONE
    REAL(kind=dp) :: collision
    REAL(kind=dp) :: Temp, m_reduced, sigma, mass1, mass2, r_gas1, r_gas2
    REAL(kind=dp), PARAMETER :: k          = 5.67051e-8,               &  ! Boltzman constant (W/m2/K4)
      k2         = 1.3807d-23,               &  ! Stefan Boltzman constant (J/K)
      Avog       = 6.0221E23,                &  ! Avogadro-number (molecules / mol)
      molarmass1 = 98.08,                    &  ! Molar mass of H2SO4
      molarmass2 = 184.11,                   &  ! Molar mass of limononic acid
      molecvol1  = 8.804e-29,                &  ! Molecular volume of H2SO4 (m3)
      molecvol2  = 3.534e-28                    ! Molecular volume of limononic acid (m3)
    r_gas1 = (molecvol1*3./(4.*PI))**(1./3.)       ! Radius of one H2SO4 molecule (m)
    r_gas2 = (molecvol2*3./(4.*PI))**(1./3.)       ! Radius of one limononic acid molecules (m)
    ! Collision cross-section, sigma = in simple hard sphere collision theory the area of the circle with radius
    ! equal to the collision diameter. G.B. 56; see also 1996, 68, 158
    sigma = r_gas1**2 + r_gas2**2 * 0.4            ! collision cross section
    mass1 = molarmass1 / Avog                      ! mass of one sulfuric acid molecule in g
    mass2 = molarmass2 / Avog                      ! mass of one limononic acid molecule in g
    m_reduced = mass1 * mass2 / (mass1 + mass2)    ! reduced mass
    COLLISION =  (8 * k2 * 1E7 * Temp / PI / m_reduced )**0.5 * sigma*1E4 * PI
  END FUNCTION COLLISION


  !ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
  !
  !   Calculates the collision rate (m^3/s) of vapour molecules
  !
  !ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
  !
  FUNCTION COLLISION_RATE(TS7, PS7)
    IMPLICIT NONE

    !INTEGER, PARAMETER :: real_x = SELECTED_REAL_KIND(p=14, r=30)
    REAL(kind=dp) :: collision_rate
    ! INTEGER :: ii
    REAL(kind=dp) :: TS7, air_free_path, dif_vap1, dif_vap2, r_vap1, r_vap2, viscosity
    REAL(kind=dp) :: PS7
    !REAL(real_x), DIMENSION(sections) :: knudsen, corr, mean_path

    REAL(kind=dp), PARAMETER :: &
      molecvol1  = 8.804e-29,   &   ! Molecular volume of H2SO4
      molecvol2  = 3.534e-28,   &   ! Molecular volume of limononic acid
      molarmass1 = 98.08,       &   ! Molar mass of H2SO4
      molarmass2 = 184.11,      &   ! Molar mass of limononic acid
      diff_vol1  = 51.96,       &   ! diffusion volume of H2SO4
      diff_vol2  = 60.00            ! diffusion volume of limononic acid - assumption

    ! Air mean free path (m) and viscosity (kg s^-1 m^-1)
    air_free_path = (6.73d-8 * TS7 * (1. + 110.4 / TS7)) / (296. * PS7 * 100 / Patm * 1.373)

    viscosity     = (1.832d-5*406.4*TS7**1.5)/(5093*(TS7+110.4))

    r_vap1 = (molecvol1*3./(4.*PI))**(1./3.)  ! radius of condensable molecules (m)
    r_vap2 = (molecvol2*3./(4.*PI))**(1./3.)  ! radius of condensable molecules (m)

    ! molecular diffusion coefficient (m^2/s)
    dif_vap1 = DIFFUSION(TS7, PS7*100, molarmass1, diff_vol1)
    dif_vap2 = DIFFUSION(TS7, PS7*100, molarmass2, diff_vol2)

    ! mean free path (m)
    ! mean_path = 3*(dif_vap + dif_part)/sqrt(8*Sigma2*TS7/PI*(1./molecmass(jj) + 1./mass))

    !knudsen = mean_path/(radius + r_vap)
    !corr = knudsen + 1.      ! transitional correction factor (Fuchs and Sutugin, 1971)
    !corr = corr/(0.377*knudsen + 1 + 4./(3.*alpha(jj))*(knudsen+knudsen**2))

    collision_rate = 4*PI*(r_vap1 + r_vap1)*(dif_vap1 + dif_vap2)     ! (m^3/s)
  END FUNCTION COLLISION_RATE

  !ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
  !
  !   Evaluates molecular diffusion coefficient (m^2/s) for condensing vapour (see Reid et al. Properties of Gases and Liquids (1987))
  !
  !ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
  !
  REAL FUNCTION diffusion(TS7, PS7, molar_gas, d_gas)
    IMPLICIT NONE

    !INTEGER, PARAMETER :: real_x = SELECTED_REAL_KIND(p=14, r=30)
    REAL(kind=dp) :: PS7
    REAL(kind=dp) :: TS7, molar_gas, d_gas
    REAL(kind=dp) :: d_air

    REAL(kind=dp), PARAMETER :: MAir = 28.97  ! Molar mass of Air (Seinfeld page1292)

    d_air = 19.70  ! diffusion volume of air (???)

    diffusion = 1.e-7*TS7**1.75*SQRT(1./MAir + 1./molar_gas)
    diffusion = diffusion/((PS7/Patm)*(d_air**(1./3.)+d_gas**(1./3.))**2)
  END FUNCTION diffusion

  !cccccccccccccccccccccccccccccccccccccccccccccccccccccccc


  SUBROUTINE DRY_DEPLAYER(k,TS7,PS7,US7,USTR2,s1S7,face,Rk_veloc, Radius)

    IMPLICIT NONE
    !argments
    REAL(kind=dp), DIMENSION(uhma_sections), INTENT(OUT) :: Rk_veloc
    REAL(kind=dp),INTENT(IN) :: TS7,PS7,US7,USTR2,s1S7,face
    INTEGER,                            INTENT(IN) :: k
    ! local
    INTEGER :: ii
    REAL(kind=dp) :: Air_mean_path,Air_viscosity,Air_density,dneedle,Air_kinvis,Re_needle,CB,nB
    REAL(kind=dp), DIMENSION(uhma_sections) :: Corr_coeff,Diff_part,Schmidt,sherwood, IB, &
      taurel, betas, kx, stm, interc, impact, Brownian, Settling_veloc, Radius
    REAL(kind=dp), PARAMETER :: boltzmann = 1.3807d-23
    dneedle=2.d-3  ! diameter of a needle, 2 mm

    ! Ambient air properties
    Air_mean_path = (6.73d-8 * TS7 * (1. + 110.4 / TS7)) / (296. * PS7 / Patm * 1.373)

    Air_viscosity = (1.832d-5 * 406.4 * TS7**1.5) / (5093 * (TS7 + 110.4))

    Air_density   = 1.2929 * 273.15 * PS7 / (TS7 * Patm)  ! [kg/m^3]

    Air_kinvis=Air_viscosity/Air_density ! [m2 s-1]

    ! Cunningham correction factor (based on Allen and Raabe (1982))
    Corr_coeff = 1. + Air_mean_path / (2. * Radius) * &
      (2.514 + 0.8 * EXP(-0.55 * (2. * Radius / Air_mean_path)))

    ! Collection efficiency of particles due to Brownian diffusion
    Diff_part = boltzmann * TS7 * Corr_coeff / (6. * PI * Air_viscosity * Radius)  ! Diffusivity of particles [m^2/s]

    Schmidt   = Air_kinvis/ Diff_part         ! _Particle_ Schmidt number

    taurel=Corr_coeff * Mass(k,:) / (6. * PI * Air_viscosity * Radius) ! relaxation time

    if (s1s7==0.) then ! deposition to ground
      Brownian=3.*sqrt(3.)/(29*PI)*Schmidt**(-2./3.)*ustr2  ! no hay like hesston

      Settling_veloc = Corr_coeff * Mass(k,:) * grav/ (6. * PI * Air_viscosity * Radius)

    else
      Re_needle=us7*dneedle/Air_kinvis  ! Reynolds number for a needle

      if (Re_needle<4.d3) then
        CB=0.467
        nB=0.5
        IB=0.99  ! table 3, normal distr
      elseif ((Re_needle .lt. 4.d4) .and.( Re_needle .ge. 4.d3)) then
        CB=0.203
        nB=0.6
        IB=0.99  ! table 3, normal distr
      else
        CB=0.025
        nB=0.8
        IB=0.995  ! table 3, normal distr
      endif

      sherwood=CB*Schmidt**(1./3.)*Re_needle**nB

      Brownian=sherwood*Diff_part/dneedle

      Brownian=s1S7*face*IB*Brownian
      Settling_veloc = Corr_coeff * Mass(k,:) * grav/ (6. * PI * Air_viscosity * Radius)

      kx=0.27
      betas=0.6
      Interc=s1S7*face*2.*kx*2.*Radius/dneedle*us7

      Stm=us7*taurel/dneedle

      Impact=s1S7*face*us7*kx*Stm**2./(2.*betas**2.)*(1./(1.+2.*betas/stm)+log(1.+2.*betas/stm)-1.)

      Brownian=Brownian+Interc+Impact
    end if
    Rk_veloc = Settling_veloc+ Brownian


    ! NOTICE Rk_veloc(k=1) is deposition velocity (m/s).

    if (k==1) then !A2
      Rk_veloc=Rk_veloc/z(2)*2.  ! from deposition velocity (m/s) -> 1/s
    end if

  END SUBROUTINE DRY_DEPLAYER

  !===============================================================================
  SUBROUTINE SCPARFLUX(znmax,z,dz,tstep,alt,wup,kt,N_conc_s7,PAR_FLUX,Vol_conc_S7,Rk_vel,ta1,p)

    IMPLICIT NONE
    REAL(kind=dp), DIMENSION(kz,uhma_sections,uhma_compo), INTENT(INOUT) :: Vol_conc_S7
    REAL(kind=dp), DIMENSION(kz,uhma_sections),            INTENT(INOUT) :: N_conc_S7
    REAL(kind=dp), DIMENSION(kz,uhma_sections),              INTENT(OUT) :: PAR_FLUX

    REAL(kind=dp), DIMENSION(kz,uhma_sections), INTENT(IN) :: Rk_vel    ! m/s
    INTEGER,INTENT(IN) :: znmax
    REAL(kind=dp),DIMENSION(kz), INTENT(IN) :: z,dz,alt,kt,wup,ta1,p
    REAL(kind=dp), INTENT(IN) :: tstep

    ! local
    INTEGER :: J,k,I
    REAL(kind=dp),DIMENSION(kz):: a,b,c,d_N,d_V, da,db,dc,df
    REAL(kind=dp), DIMENSION(kz) :: PAR_VER,PAR_VER_NEW,VOL_VER,VOL_VER_NEW
    REAL(kind=dp) :: dkz1,daz,dkz,fktt,fktd

    !SIIRRETTY YLEMPaa, CANOPY-laskuista tahan
    df(1)=0.

    do k=2,znmax-1
      da(k)=dz(k+1)*(dz(k)+dz(k+1))
      db(k)=dz(k)*dz(k+1)
      dc(k)=dz(k)*(dz(k)+dz(k+1))
      df(k)=0.5
    enddo
    df(1)=df(2)

    do J = 1, uhma_sections
      PAR_VER(1)= N_conc_s7(2,J) - tstep * Rk_vel(1,J)*N_conc_s7(2,j)  ! deposition
      PAR_VER(znmax)= N_conc_s7(znmax,J)
      if (PAR_VER(1)<0.) then
        PAR_VER(1)=0.
      end if
      do k=2,znmax-1
        PAR_VER(k) = N_conc_s7(k,j)   &
          - tstep * Rk_vel(k,J)*N_conc_s7(k,J) !deposition


        fktt   = 2.*(df(k)*alt(k+1)*kt(k+1) +(1.-df(k))*alt(k)*kt(k))
        fktd   = 2.*((1.-df(k-1))*alt(k-1)*kt(k-1) +df(k-1)*alt(k)*kt(k))

        ! fktt   = df(k)*alt(k+1)*kt(k+1) +(1.-df(k))*alt(k)*kt(k)
        ! fktd   = (1.-df(k-1))*alt(k-1)*kt(k-1) +df(k-1)*alt(k)*kt(k)

        a(k)   = tstep*(fktt-wup(k)*dz(k))/da(k)
        c(k)   = tstep*(fktd+wup(k)*dz(k+1))/dc(k)
        b(k)   = tstep*((dz(k)*fktt+ dz(k+1)*fktd) /(dz(k+1)+dz(k)) &
          +wup(k)*(dz(k+1)-dz(k)))/db(k)+ 1.0d0  &
          - tstep * Rk_vel(k,J) !deposition
      enddo

      call gtri(a, c, b, PAR_VER, PAR_VER_NEW, 1, PAR_VER(1), 3.d0, 2, 0.d0, 3.d0, znmax, 2)

      ! No flux due to deposition at the lowest level. Flux=0.
      !       call gtri(a, c, b, PAR_VER, PAR_VER_NEW, 2,0.d0, 3.d0, 2, 0.d0, 3.d0, znmax, 2)

      do k=1,znmax
        if (PAR_VER_NEW(k) < 0.) PAR_VER_NEW(k) = 0. !JL
        N_conc_s7(k,J) = PAR_VER_NEW(k)
      enddo

      PAR_FLUX(1,J)=0.
      PAR_FLUX(znmax,J)=0.
      do  k=2,znmax-1
        fktt   = (df(k)*alt(k+1)*kt(k+1) +(1.-df(k))*alt(k)*kt(k))
        fktd   = ((1.-df(k-1))*alt(k-1)*kt(k-1) +df(k-1)*alt(k)*kt(k))
        !fktt   = 100*100*(df(k)*alt(k+1)*kt(k+1) +(1.-df(k))*alt(k)*kt(k))
        !fktd   = 100*100*((1.-df(k-1))*alt(k-1)*kt(k-1) +df(k-1)*alt(k)*kt(k))
        !JL mean of two levels, added minus, unit #/cm²/s
        PAR_FLUX(k,J)=-0.5*((fktt*(PAR_VER_NEW(k+1) - PAR_VER_NEW(k))   /dz(k+1)) +  &
          (fktd*(PAR_VER_NEW(k)   - PAR_VER_NEW(k-1)) /dz(k)))
      enddo

      do I =1, uhma_compo
        VOL_VER(1)= Vol_conc_s7(2,J,I) &
          - tstep * Rk_vel(1,J)*Vol_conc_s7(2,J,I)  ! deposition
        do k=2,znmax
          VOL_VER(k) = Vol_conc_s7(k,J,I) &
            - tstep * Rk_vel(k,J)*Vol_conc_s7(k,J,I) !deposition
        enddo
        call gtri(a, c, b, VOL_VER, VOL_VER_NEW, 1, VOL_VER(1), 3.d0, 2, 0.d0, 3.d0, znmax, 2)
        do k=1,znmax
          Vol_conc_s7(k,J,I) = VOL_VER_NEW(k) !_NEW(k) !JL
          if (Vol_conc_s7(k,J,I) < 0.) Vol_conc_s7(k,J,I) = 0. !JL
        enddo
      enddo

    enddo

  END SUBROUTINE SCPARFLUX


  ! subroutine for defining the vertical grid
  ! moved here from the beginning of the program by rosa, no changes made
  SUBROUTINE generate_grid(kz, method, hh, z, abl)
    INTEGER     , INTENT(IN) :: kz      ! number of layers
    CHARACTER(*), INTENT(IN) :: method  ! method to generate the grid blocks

    REAL(kind=dp), INTENT(OUT)  :: hh  ! [m], model domain height
    REAL(kind=dp), INTENT(OUT)  :: z(kz)  ! [m], vertical coordinate
    INTEGER      , INTENT(OUT)  :: abl    ! either 1 (linear grid) or 2 (logarithmic grid), used for other calculations

    REAL(kind=dp) :: dz    ! distance of the model levels in linear case
    ! REAL(kind=dp) :: hh    ! model domain height

    SELECT CASE ( TRIM(ADJUSTL(method)) )
    CASE ('LIN')  ! linear grids
      hh=23.5d0
      dz = hh/(kz-1)
      z(1) = 0.0_dp
      DO k=2,kz
        z(k)=(k-1)*dz
      END DO
      z(kz)=hh
    CASE ('LOG')  ! logarithmic grids
      hh = 3000.0_dp
      z(1) = 0.0_dp
      do k=2,kz
        z(k)=z(1)+exp(log(hh)*(k-1.0d0)/(kz-1.0d0))-1.0d0
      enddo
      z(kz)=hh
    CASE ('MAN_ASAM')  ! manually set grids, ASAM case
      z(1) = 0.0_dp
      z(2:kz) = (/2.0d0, 4.0d0, 6.0d0, 8.0d0, 10.0d0, &
        12.0d0, 14.0d0, 16.0d0, 18.0d0, 20.0d0, &
        23.595d0, 27.866d0, 32.879d0, 38.762d0, 45.667d0, &
        53.772d0, 63.284d0, 74.447d0, 87.549d0, 102.92d0, &
        120.97d0, 142.15d0, 167.01d0, 196.19d0, 230.44d0, &
        270.63d0, 317.80d0, 373.17d0, 438.14d0, 514.41d0, &
        603.91d0, 708.96d0, 832.26d0, 976.96d0, 1146.8d0, &
        1346.1d0, 1580.0d0, 1854.6d0, 2176.8d0, 2555.1d0, &
        3000.0d0 /)
    CASE ('MAN_1')  ! unknown grid settings
      z(1) = 0.0_dp
      z(2:kz) = (/2.5     , 7.5    , 12.5   , 17.5    , 22.75   , 28.525 , 34.88  , &
        41.87   , 49.555 , 58.01  , 67.31   , 77.54   , 88.795 , 101.175, &
        114.79  , 129.765, 146.24 , 164.365 , 184.305 , 206.235, 230.36 , &
        256.9   , 286.085, 318.19 , 353.51  , 392.365 , 435.105, 482.115, &
        533.825 , 590.705, 653.275, 722.1   , 797.81  , 881.095, 972.705, &
        1073.475, 1184.32, 1306.25, 1440.375, 1587.915, 1750.21, 1928.73 /)
    END SELECT

    abl = 2  ! set it to 2 currently, need to reorganize it in future
  END SUBROUTINE generate_grid


  SUBROUTINE init_aerosol()
    call set_aerosol_structures(particles, ambient, &
      initial_dist, options, &
      SPC_NAMES, trim(adjustl(input_dir_general)) // '/condensable_vapors')

    do k=1, kz
      N_CONC(k,:) = particles%n_conc
      Radius(k,:) = particles%radius
      Rdry(k,:)   = particles%rdry
      Core(k,:)   = particles%core
      Mass(k,:)   = particles%mass
      Vol_conc(k,:,:) = particles%vol_conc
    end do  ! k=1, kz
  END SUBROUTINE init_aerosol


  SUBROUTINE init_rOH()
    CH_oh_flag = .FALSE.
    CH_oh_count = 0

    DO CH_oh_i = 1, NSPEC
      IF (SPC_NAMES(CH_oh_i)(1:3) == 'rOH') THEN
        CH_oh_flag(CH_oh_i) = .TRUE.
        CH_oh_count = CH_oh_count + 1
      ENDIF
    ENDDO

    IF (CH_oh_count > 0) THEN
      ! collect indices of the OH-reactivity pseudochemicals
      ALLOCATE(CH_oh_indices(CH_oh_count))
      CH_oh_count = 0
      DO CH_oh_i = 1, NSPEC
        IF (SPC_NAMES(CH_oh_i)(1:3) == 'rOH') THEN
          CH_oh_count = CH_oh_count + 1
          CH_oh_indices(CH_oh_count) = CH_oh_i
        ENDIF
      ENDDO
      ALLOCATE(CH_oh_cons3(kz,CH_oh_count,3))
      ALLOCATE(CH_oh_prev3(kz,CH_oh_count,3))
      CH_oh_cons3 = 0
      CH_oh_prev3 = 0
    ENDIF
  END SUBROUTINE init_rOH


  SUBROUTINE init_rO3()
    CH_o3_flag = .FALSE.
    CH_o3_count = 0

    DO CH_o3_i = 1, NSPEC
      IF (SPC_NAMES(CH_o3_i)(1:3) == 'rO3') THEN
        CH_o3_flag(CH_o3_i) = .TRUE.
        CH_o3_count = CH_o3_count + 1
      ENDIF
    ENDDO

    IF (CH_o3_count > 0) THEN
      ! collect indices of the O3-reactivity pseudochemicals
      ALLOCATE(CH_o3_indices(CH_o3_count))
      CH_o3_count = 0
      DO CH_o3_i = 1, NSPEC
        IF (SPC_NAMES(CH_o3_i)(1:3) == 'rO3') THEN
          CH_o3_count = CH_o3_count + 1
          CH_o3_indices(CH_o3_count) = CH_o3_i
        ENDIF
      ENDDO
      ALLOCATE(CH_o3_cons3(kz,CH_o3_count,3))
      ALLOCATE(CH_o3_prev3(kz,CH_o3_count,3))
      CH_o3_cons3 = 0
      CH_o3_prev3 = 0
    ENDIF
  END SUBROUTINE init_rO3

  SUBROUTINE init_rNO3()
    CH_no3_flag = .FALSE.
    CH_no3_count = 0

    DO CH_no3_i = 1, NSPEC
      IF (SPC_NAMES(CH_no3_i)(1:4) == 'rNO3') THEN
        CH_no3_flag(CH_no3_i) = .TRUE.
        CH_no3_count = CH_no3_count + 1
      ENDIF
    ENDDO

    IF (CH_no3_count > 0) THEN
      ! collect indices of the NO3-reactivity pseudochemicals
      ALLOCATE(CH_no3_indices(CH_no3_count))
      CH_no3_count = 0
      DO CH_no3_i = 1, NSPEC
        IF (SPC_NAMES(CH_no3_i)(1:4) == 'rNO3') THEN
          CH_no3_count = CH_no3_count + 1
          CH_no3_indices(CH_no3_count) = CH_no3_i
        ENDIF
      ENDDO
      ALLOCATE(CH_no3_cons3(kz,CH_no3_count,3))
      ALLOCATE(CH_no3_prev3(kz,CH_no3_count,3))
      CH_no3_cons3 = 0
      CH_no3_prev3 = 0
    ENDIF
  END SUBROUTINE init_rNO3

END PROGRAM Sosa
